<?

class RatingWidget extends CWidget
{
    public $avg;
    public $elem_id;
    public $elem_type;

    public $selected_class = "activeStar";
    public $rating_text = "Оценка";


    public function run()
    {
        $function = "";
        $divFunction = "";
        if (!Yii::app()->user->isGuest) {
            $function = 'onclick="setRating(' . $this->elem_id . ',\'' . $this->elem_type . '\',$(this))" onmouseover="overstar($(this))"';
            $divFunction = 'onmouseout="out($(this))"';
        }
        $html = '<div class="ratingStarsView form-group" '.$divFunction.' >';
        $html .= RatingHelper::renderStars($this->avg,$function,$this->selected_class);
        $userText = '';
        $userRating = RatingHelper::getUserRating($this->elem_id,$this->elem_type);
        if ($userRating != 0) {
            $userText = ", Ваша ".$userRating;
        }
        $html .= "<span> ".substr($this->avg, 0, 4).$userText."</span>";
        $html .= '</div>';
        echo $html;
        //$this->render('copyright');
    }


}

?>