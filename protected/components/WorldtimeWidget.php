<?

class WorldtimeWidget extends CWidget
{
    public $elem_id;

    public function run()
    {
        if(Yii::app()->user->isGuest) {
            $worldtimes = Yii::app()->session['worldtimes'];
            if(count($worldtimes) > 5)
                $worldtimes = false;
            if (!$worldtimes) {
                $worldtimes = array();
                $timeItem = array();
                $timeItem['id'] = 0;
                $timeItem['city'] = 'Москва';
                $timeItem['country'] = 'Россия';
                $timeItem['diff'] = 0;
                array_push($worldtimes, $timeItem);
                $timeItem = array();
                $timeItem['id'] = 1;
                $timeItem['city'] = 'Нью-Йорк';
                $timeItem['country'] = 'США';
                $timeItem['diff'] = 7;
                array_push($worldtimes, $timeItem);
                $timeItem = array();
                $timeItem['id'] = 2;
                $timeItem['city'] = 'Лондон';
                $timeItem['country'] = 'Великобритания';
                $timeItem['diff'] = 2;
                array_push($worldtimes, $timeItem);
                $timeItem = array();
                $timeItem['id'] = 3;
                $timeItem['city'] = 'Токио';
                $timeItem['country'] = 'Япония';
                $timeItem['diff'] = -6;
                array_push($worldtimes, $timeItem);
                $timeItem = array();
                $timeItem['id'] = 4;
                $timeItem['city'] = 'Сидней';
                $timeItem['country'] = 'Австралия';
                $timeItem['diff'] = -7;
                array_push($worldtimes, $timeItem);
                Yii::app()->session['worldtimes'] = $worldtimes;
            }

            /*получаем время Москвы*/
            $urlMoscowTime = 'http://dateandtime.info/ru/city.php?id=524901';
            $moscow = file_get_contents($urlMoscowTime);
            preg_match('/class="citycolumn">(.*?)<\/time>/s', $moscow, $matches);
            preg_match('/class="hms_fulldate results"(.*?)>(.*?)<\/time>/s', $moscow, $matches);
            $moscowTime = explode('<br>', $matches[2])[0];
            $moscowHour = explode(':', $moscowTime)[0];
            $moscowMinute = explode(':', $moscowTime)[1];

            $mainArray = array();
            foreach ($worldtimes as $item) {
                $tempArray = array();
                $tempArray['id'] = $item['id'];
                $tempArray['city'] = $item['city'];
                $tempArray['country'] = $item['country'];
                $tempHour = $moscowHour - $item['diff'];
                if (strlen($tempHour) == 1)
                    $tempHour = '0' . $tempHour;
                $tempArray['time'] = $tempHour . ':' . $moscowMinute;
                $arrayDaysWeek = ['Вс', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Пн'];
                $dayOfWeek = intval(date('w'));
                if ($dayOfWeek == 0){
                    $dayOfWeek = 1;
                }

                $dayOfWeek = $arrayDaysWeek[$dayOfWeek - 1];
                $tempArray['dayOfWeek'] = $dayOfWeek;
                array_push($mainArray, $tempArray);

            }
            return $mainArray;
        } else {
            /*получаем время Москвы*/
            $urlMoscowTime = 'http://dateandtime.info/ru/city.php?id=524901';
            $moscow = file_get_contents($urlMoscowTime);
            preg_match('/class="citycolumn">(.*?)<\/time>/s', $moscow, $matches);
            preg_match('/class="hms_fulldate results"(.*?)>(.*?)<\/time>/s', $moscow, $matches);
            $moscowTime = explode('<br>', $matches[2])[0];
            $moscowHour = explode(':', $moscowTime)[0];
            $moscowMinute = explode(':', $moscowTime)[1];

            $worldtimes = Worldtime::model()->findByAttributes(array('userId'=>Yii::app()->user->id));

            //если нет ни одной записи
            if (!$worldtimes) {
                $worldtime = new Worldtime();
                $worldtime->city = 'Москва';
                $worldtime->country = 'Россия';
                $worldtime->diff = 0;
                $worldtime->userId = Yii::app()->user->id;
                $worldtime->save();
                $worldtime = new Worldtime();
                $worldtime->city = 'Нью-Йорк';
                $worldtime->country = 'США';
                $worldtime->diff = 7;
                $worldtime->userId = Yii::app()->user->id;
                $worldtime->save();
                $worldtime = new Worldtime();
                $worldtime->city = 'Лондон';
                $worldtime->country = 'Великобритания';
                $worldtime->diff = 2;
                $worldtime->userId = Yii::app()->user->id;
                $worldtime->save();
                $worldtime = new Worldtime();
                $worldtime->city = 'Токио';
                $worldtime->country = 'Япония';
                $worldtime->diff = -6;
                $worldtime->userId = Yii::app()->user->id;
                $worldtime->save();
                $worldtime = new Worldtime();
                $worldtime->city = 'Сидней';
                $worldtime->country = 'Австралия';
                $worldtime->diff = -7;
                $worldtime->userId = Yii::app()->user->id;
                $worldtime->save();

            }
            $worldtimes = Worldtime::model()->findAllByAttributes(array('userId'=>Yii::app()->user->id));
            $mainArray = array();
            foreach ($worldtimes as $item) {
                $tempArray = array();
                $tempArray['id'] = $item->id;
                $tempArray['city'] = $item->city;
                $tempArray['country'] = $item->country;
                $tempHour = $moscowHour - $item->diff;
                if (strlen($tempHour) == 1)
                    $tempHour = '0' . $tempHour;
                $tempArray['time'] = $tempHour . ':' . $moscowMinute;
                $arrayDaysWeek = ['Вс', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Пн'];
                $dayOfWeek = intval(date('w'));
                if ($dayOfWeek == 0){
                    $dayOfWeek = 1;
                }
                $dayOfWeek = $arrayDaysWeek[$dayOfWeek - 1];
                $tempArray['dayOfWeek'] = $dayOfWeek;
                array_push($mainArray, $tempArray);
            }
            return $mainArray;
        }

    }

}

?>