<?php
	//для лотерей в которых нужно указывать дату
	class Stoloto2Command extends CConsoleCommand{
		public function run($args) 
    	{
    		$WORK = TRUE;
		    $dates = array();
		    $ids = array();
        	$numbers = array();
		    $year = 2016;
    		
		    $url = $args[0];
    		$lid = $args[1];

	   		preg_match('/www.stoloto.ru\/(.*?)\/archive/s', $url, $matches);
    		$lotteryName = $matches[1];
    		do {

			    $patterns = array();
		        $patterns[0] = '/from=(.*?)\+/';
		        $patterns[1] = '/to=(.*?)\+/';
                $replacements = array();
                $year0 = $year-1;
		        $replacements[2] = 'from='.$year0.'-01-01+';
		        $replacements[1] = 'to='.$year.'-01-01+';
		        $url = 'http://www.stoloto.ru/'.$lotteryName.'/archive?from='.$year0.'-01-01+00%3A00%3A00&to='.$year.'-01-01+00%3A00%3A00&firstDraw=2261&lastDraw=2618&mode=date';
		        $url = preg_replace($patterns, $replacements, $url);
			    $page = file_get_contents($url);

			    preg_match_all('/ins class="pseudo">(.*?)<\/ins>/s', $page, $matches, PREG_PATTERN_ORDER);

			    $dates = array_merge($dates, $matches[1]);

			    preg_match_all('/"main"(.*?)class="numbers(.*?)>(.*?)<\/div/s', $page, $matches, PREG_PATTERN_ORDER);

			    for($i=0;$i<count($matches[3]);$i++){
			    	$temp = strip_tags($matches[3][$i]);
			    	$temp = trim($temp);
			    	$temp = preg_replace('/[\s,\t,\n]+/', '', $temp);
			    	$temp = preg_replace('/&nbsp;/', ',', $temp);
			    	//$temp = preg_replace('/([^\d\,])*/', '', $temp);
			    	$temp = substr($temp, 0, -1);
			    	//$temp = split("&nbsp;", $matches[3][$i]);
			    	array_push($numbers, $temp);
			    }

			    preg_match_all('/class="draw"(.*?)a href="(.*?)>(.*?)</s', $page, $matches, PREG_PATTERN_ORDER);
			    $temp = $matches[3];
			    $ids = array_merge($ids, $matches[3]);

			    if(empty($temp))
				    $WORK=FALSE;
			    $year--;

		    } while ($WORK == TRUE);

		    $tempNumbers;

			//запись в базу

			//удаляем все записи этой лотереи
			//ParsedLottery::	model()->deleteAll('idLottery = :lid AND id>0', array(':lid' => $lid));			
		    for($i=0;$i<count($ids);$i++){
			    $modelLottery = new ParsedLottery;
				$modelLottery->idLottery = $lid;
				$modelLottery->idDraw = $ids[$i];
				$modelLottery->drawName = $ids[$i];				
				$sqlResult = Users::model()->findBySql('select id from parsedLottery where idLottery=:id1 AND idDraw=:id2',array(':id1'=>$lid,':id2'=>$ids[$i]));
				$modelLottery->date = date('Y.m.d H:i:s',strtotime($dates[$i]));
				$tempNumbers = $numbers[$i];
				$pos = strripos($tempNumbers, 'суперприз');
				if($pos === FALSE)
					$modelLottery->isJackpot = 0;
					else
					$modelLottery->isJackpot = 1;
				$tempNumbers = preg_replace('/,суперприз[^\d\,]*/', '', $tempNumbers);
				$modelLottery->numbers = $tempNumbers;
				if($sqlResult == NULL)
					$modelLottery->save(false);
				//вывод
				/*
				echo $lid.'<br>';
				echo $ids[$i].'<br>';
				echo date('Y.m.d H:i:s',strtotime($dates[$i])).'<br>';
				echo $numbers[$i].'<br>';
				*/
			}

			/*BEGIN CALCULATE INTERVALS*/
    		
			$criteria = new CDbCriteria();
			$criteria->params = array(':lid'=>$lid);
			//$criteria->select = 'numbers';
			$criteria->condition = "idLottery = :lid";
			$criteria->order = "idDraw DESC";
			//$criteria->limit = "10";
			//$criteria->offset = "38";
			$sqlData = ParsedLottery::model()->findAll($criteria);
			$lotteryData = Lotteries::model()->findByPk($lid);
			$matrix = array();
			foreach ($sqlData as $key) {
				$tmp = explode(',',$key->numbers);
				if($lotteryData->extraNumbersCount >0){
					$tmpExtra = str_replace(" ", "", $key->extraNumbers);
					$tmp = array_merge($tmp, explode(',',$tmpExtra));
				}
				for($i=0;$i<count($tmp);$i++){
					if($tmp[$i] == -1 )
						continue;
					if($i+1<count($tmp)){
						for($j=$i+1;$j<count($tmp);$j++){
							if($tmp[$i]==$tmp[$j])
								$tmp[$j]=-1;
						}
					}
				}				
				array_push($matrix, array('id'=>$key->id, 'numbers'=>$tmp));
			}

			$i = 0;
			$n = 0;
			$sum = 0;
			$abc = 0;

			while($i<count($matrix)){
				$countOfArr = count($matrix[$i]['numbers']);
				$curElem = $matrix[$i]['numbers'][$n];
				$flag = 0;
				for($j=$i+1;$j<count($matrix);$j++){
					//echo "[$curElem]";
					for($k=0;$k<count($matrix[$j]['numbers']);$k++){
						//echo $matrix[$j]['numbers'][$k].'|';
						if($matrix[$j]['numbers'][$k] == -1){
							break;
						}
						if($matrix[$j]['numbers'][$k] == $curElem){
							if($j-$i-1 == 0){
								//if($flag == 2)
									$flag = 3;
								//else
									//$flag = 2;
								break;
							}
							$sum+=$j-$i-1;
							if($flag == 2)
								$sum -= 1;
							$flag = 1;
							break;
						}
					}
					if(($flag == 1)||($flag == 3))
						break;
				}
				$n++;
				//echo "[$n|$sum]";
				if($n >= $countOfArr){
					//$lottery = ParsedLottery::model()->findByPk($matrix[$i]['id']);
					//if($lottery->sumOfIntervals == -1){
						//echo '-1|';
						ParsedLottery::model()->updateByPk($matrix[$i]['id'], array('sumOfIntervals' => $sum));
					//}
					array_push($matrix[$i]['numbers'], $sum);
					$i++;
					$n=0;
					$sum = 0;
				}
				//if($abc > 22){
					//die();
				//}
				//$abc++;
			}

			/*END CALCULATE INTERVALS*/

    	}
	}
?>