<?php
	//для лотерей в которых не нужно указывать дату, а нужен только год
	class GetCitiesCommand extends CConsoleCommand{
		public function run($args){

            function transEngToRus($str){
                $totalStr = $str;
                $totalStr = str_replace("SCH", "Щ", $totalStr);$totalStr = str_replace("Sch", "Щ", $totalStr);$totalStr = str_replace("sch", "щ", $totalStr);
                $totalStr = str_replace("YOU", "Ю", $totalStr);$totalStr = str_replace("You", "Ю", $totalStr);$totalStr = str_replace("you", "ю", $totalStr);
                $totalStr = str_replace("CH", "Ч", $totalStr);$totalStr = str_replace("Ch", "Ч", $totalStr);$totalStr = str_replace("ch", "ч", $totalStr);
                $totalStr = str_replace("SH", "Ш", $totalStr);$totalStr = str_replace("Sh", "Ш", $totalStr);$totalStr = str_replace("sh", "ш", $totalStr);
                $totalStr = str_replace("ZH", "Ж", $totalStr);$totalStr = str_replace("Zh", "Ж", $totalStr);$totalStr = str_replace("zh", "ж", $totalStr);
                $totalStr = str_replace("OO", "У", $totalStr);$totalStr = str_replace("Oo", "У", $totalStr);$totalStr = str_replace("oo", "у", $totalStr);
                $totalStr = str_replace("YA", "Я", $totalStr);$totalStr = str_replace("Ya", "Я", $totalStr);$totalStr = str_replace("ya", "я", $totalStr);
                $totalStr = str_replace("YE", "Е", $totalStr);$totalStr = str_replace("Ye", "Е", $totalStr);$totalStr = str_replace("ye", "е", $totalStr);
                $totalStr = str_replace("YU", "Ю", $totalStr);$totalStr = str_replace("Yu", "Ю", $totalStr);$totalStr = str_replace("yu", "ю", $totalStr);
                $totalStr = str_replace("YO", "Ё", $totalStr);$totalStr = str_replace("Yo", "Ё", $totalStr);$totalStr = str_replace("yo", "ё", $totalStr);
                $totalStr = str_replace("A", "А", $totalStr);$totalStr = str_replace("a", "а", $totalStr);
                $totalStr = str_replace("B", "Б", $totalStr);$totalStr = str_replace("b", "б", $totalStr);
                $totalStr = str_replace("C", "Ц", $totalStr);$totalStr = str_replace("c", "ц", $totalStr);
                $totalStr = str_replace("D", "Д", $totalStr);$totalStr = str_replace("d", "д", $totalStr);
                $totalStr = str_replace("E", "Е", $totalStr);$totalStr = str_replace("e", "е", $totalStr);
                $totalStr = str_replace("F", "Ф", $totalStr);$totalStr = str_replace("f", "ф", $totalStr);
                $totalStr = str_replace("G", "Г", $totalStr);$totalStr = str_replace("g", "г", $totalStr);
                $totalStr = str_replace("H", "Х", $totalStr);$totalStr = str_replace("h", "х", $totalStr);
                $totalStr = str_replace("I", "И", $totalStr);$totalStr = str_replace("i", "и", $totalStr);
                $totalStr = str_replace("J", "Ж", $totalStr);$totalStr = str_replace("j", "ж", $totalStr);
                $totalStr = str_replace("K", "К", $totalStr);$totalStr = str_replace("k", "к", $totalStr);
                $totalStr = str_replace("L", "Л", $totalStr);$totalStr = str_replace("l", "л", $totalStr);
                $totalStr = str_replace("M", "М", $totalStr);$totalStr = str_replace("m", "м", $totalStr);
                $totalStr = str_replace("N", "Н", $totalStr);$totalStr = str_replace("n", "н", $totalStr);
                $totalStr = str_replace("O", "О", $totalStr);$totalStr = str_replace("o", "о", $totalStr);
                $totalStr = str_replace("P", "П", $totalStr);$totalStr = str_replace("p", "п", $totalStr);
                $totalStr = str_replace("Q", "К", $totalStr);$totalStr = str_replace("q", "к", $totalStr);
                $totalStr = str_replace("R", "Р", $totalStr);$totalStr = str_replace("r", "р", $totalStr);
                $totalStr = str_replace("S", "С", $totalStr);$totalStr = str_replace("s", "с", $totalStr);
                $totalStr = str_replace("T", "Т", $totalStr);$totalStr = str_replace("t", "т", $totalStr);
                $totalStr = str_replace("U", "У", $totalStr);$totalStr = str_replace("u", "у", $totalStr);
                $totalStr = str_replace("V", "В", $totalStr);$totalStr = str_replace("v", "в", $totalStr);
                $totalStr = str_replace("W", "В", $totalStr);$totalStr = str_replace("w", "в", $totalStr);
                $totalStr = str_replace("X", "Кс", $totalStr);$totalStr = str_replace("x", "кс", $totalStr);
                $totalStr = str_replace("Y", "И", $totalStr);$totalStr = str_replace("y", "и", $totalStr);
                $totalStr = str_replace("Z", "З", $totalStr);$totalStr = str_replace("z", "з", $totalStr);
                return $totalStr;
            }
            function transRusToEng($str){
                $totalStr = $str;
                $totalStr = str_replace("Щ", "SCH", $totalStr);$totalStr = str_replace("Щ", "Sch", $totalStr);$totalStr = str_replace("щ", "sch", $totalStr);
                $totalStr = str_replace("Ю", "YOU", $totalStr);$totalStr = str_replace("Ю", "You", $totalStr);$totalStr = str_replace("ю", "you", $totalStr);
                $totalStr = str_replace("Ч", "CH", $totalStr);$totalStr = str_replace("Ч", "Ch", $totalStr);$totalStr = str_replace("ч", "ch", $totalStr);
                $totalStr = str_replace("Ш", "SH", $totalStr);$totalStr = str_replace("Ш", "Sh", $totalStr);$totalStr = str_replace("ш", "sh", $totalStr);
                $totalStr = str_replace("Ж", "ZH", $totalStr);$totalStr = str_replace("Ж", "Zh", $totalStr);$totalStr = str_replace("ж", "zh", $totalStr);
                $totalStr = str_replace("Я", "YA", $totalStr);$totalStr = str_replace("Я", "Ya", $totalStr);$totalStr = str_replace("я", "ya", $totalStr);
                $totalStr = str_replace("Е", "YE", $totalStr);$totalStr = str_replace("Е", "Ye", $totalStr);$totalStr = str_replace("е", "ye", $totalStr);
                $totalStr = str_replace("Ю", "YU", $totalStr);$totalStr = str_replace("Ю", "Yu", $totalStr);$totalStr = str_replace("ю", "yu", $totalStr);
                $totalStr = str_replace("Ё", "YO", $totalStr);$totalStr = str_replace("Ё", "Yo", $totalStr);$totalStr = str_replace("ё", "yo", $totalStr);
                $totalStr = str_replace("А", "A", $totalStr);$totalStr = str_replace("а", "a", $totalStr);
                $totalStr = str_replace("Б", "B", $totalStr);$totalStr = str_replace("б", "b", $totalStr);
                $totalStr = str_replace("В", "V", $totalStr);$totalStr = str_replace("в", "v", $totalStr);
                $totalStr = str_replace("Г", "G", $totalStr);$totalStr = str_replace("г", "g", $totalStr);
                $totalStr = str_replace("Д", "D", $totalStr);$totalStr = str_replace("д", "d", $totalStr);
                $totalStr = str_replace("З", "Z", $totalStr);$totalStr = str_replace("з", "z", $totalStr);
                $totalStr = str_replace("И", "I", $totalStr);$totalStr = str_replace("и", "i", $totalStr);
                $totalStr = str_replace("Й", "I", $totalStr);$totalStr = str_replace("й", "i", $totalStr);
                $totalStr = str_replace("К", "K", $totalStr);$totalStr = str_replace("к", "k", $totalStr);
                $totalStr = str_replace("Л", "L", $totalStr);$totalStr = str_replace("л", "l", $totalStr);
                $totalStr = str_replace("М", "M", $totalStr);$totalStr = str_replace("м", "m", $totalStr);
                $totalStr = str_replace("Н", "N", $totalStr);$totalStr = str_replace("н", "n", $totalStr);
                $totalStr = str_replace("О", "O", $totalStr);$totalStr = str_replace("о", "o", $totalStr);
                $totalStr = str_replace("П", "P", $totalStr);$totalStr = str_replace("п", "p", $totalStr);
                $totalStr = str_replace("Р", "R", $totalStr);$totalStr = str_replace("р", "r", $totalStr);
                $totalStr = str_replace("С", "S", $totalStr);$totalStr = str_replace("с", "s", $totalStr);
                $totalStr = str_replace("Т", "T", $totalStr);$totalStr = str_replace("т", "t", $totalStr);
                $totalStr = str_replace("У", "U", $totalStr);$totalStr = str_replace("у", "u", $totalStr);
                $totalStr = str_replace("Ф", "F", $totalStr);$totalStr = str_replace("ф", "f", $totalStr);
                $totalStr = str_replace("Х", "H", $totalStr);$totalStr = str_replace("х", "h", $totalStr);
                $totalStr = str_replace("Ц", "C", $totalStr);$totalStr = str_replace("ц", "c", $totalStr);
                $totalStr = str_replace("Ъ", "", $totalStr);$totalStr = str_replace("ъ", "", $totalStr);
                $totalStr = str_replace("Ы", "Y", $totalStr);$totalStr = str_replace("ы", "y", $totalStr);
                $totalStr = str_replace("Ь", "", $totalStr);$totalStr = str_replace("ь", "", $totalStr);
                $totalStr = str_replace("Э", "e", $totalStr);$totalStr = str_replace("э", "e", $totalStr);
                return $totalStr;
            }

            $translit = array(
                'а' => 'a',   'б' => 'b',   'в' => 'v',
                'в' => 'w',
                'г' => 'g',   'д' => 'd',   'е' => 'e',
                'е' => 'ye',
                'ё' => 'yo',  'ж' => 'zh',  'з' => 'z',
                'и' => 'i',   'й' => 'j',   'к' => 'k',
                'л' => 'l',   'м' => 'm',   'н' => 'n',
                'о' => 'o',   'п' => 'p',   'р' => 'r',
                'с' => 's',   'т' => 't',   'у' => 'u',
                'у' => 'oo',
                'ф' => 'f',   'х' => 'x',   'ц' => 'c',
                'ч' => 'ch',  'ш' => 'sh',  'щ' => 'shh',
                'ь' => '\'',  'ы' => 'y',   'ъ' => '\'\'',
                'э' => 'e\'', 'ю' => 'yu',  'я' => 'ya',

                'А' => 'A',   'Б' => 'B',   'В' => 'V',
                'В' => 'W',
                'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
                'Ё' => 'YO',  'Ж' => 'Zh',  'З' => 'Z',
                'И' => 'I',   'Й' => 'J',   'К' => 'K',
                'Л' => 'L',   'М' => 'M',   'Н' => 'N',
                'О' => 'O',   'П' => 'P',   'Р' => 'R',
                'С' => 'S',   'Т' => 'T',   'У' => 'U',
                'Ф' => 'F',   'Х' => 'X',   'Ц' => 'C',
                'Ч' => 'CH',  'Ш' => 'SH',  'Щ' => 'SHH',
                'Ь' => '\'',  'Ы' => 'Y\'', 'Ъ' => '\'\'',
                'Э' => 'E\'', 'Ю' => 'YU',  'Я' => 'YA',
                'Я' => 'Ya', 'Е' => 'Ye', 'Е' => 'YE',
                'Ю' => 'You'
            );

            $countries = Countries::model()->findAll();
            $i = 0;
            foreach($countries as $country) {
                $countryId = $country->id;
                $codeAlpha = $country->codeAlpha;
                //$countryId = 253;
                //$codeAlpha = 'AU';

                //совместить русские и английские названия в один массив
                $arrayAllCities = array();
                //достаём русскоязычное названия города
                $url = 'http://dateandtime.info/ru/country.php?code='.$codeAlpha;

                $content = file_get_contents($url);
                preg_match_all('/id="city_table"(.*?)>(.*?)<\/table>/s', $content, $matches, PREG_PATTERN_ORDER);
                $tableWithCountries = null;

                $tableWithCountries = $matches[0][0];
                preg_match_all('/<tbody>(.*?)<\/tbody>/s', $tableWithCountries, $matches, PREG_PATTERN_ORDER);
                $tableBody = null;
                if (strlen(trim($matches[1][0])) > 0) {
                    $tableBody = $matches[1][0];
                    preg_match_all('/<td><a(.*?)>(.*?)<\/a>/s', $tableBody, $matches, PREG_PATTERN_ORDER);
                    $rusCities = array();
                    for ($i = 0; $i < count($matches[2]); $i++) {
                        $rusCities[$i]['rusName'] = $matches[2][$i];
                    }
                    /*
                    //print_r($rusCities);
                    //достаём англоязычного названия города
                    $url = 'http://dateandtime.info/country.php?code=' . $codeAlpha;
                    $content = file_get_contents($url);
                    preg_match_all('/id="city_table"(.*?)>(.*?)<\/table>/s', $content, $matches, PREG_PATTERN_ORDER);
                    $tableWithCountries = $matches[0][0];
                    preg_match_all('/<tbody>(.*?)<\/tbody>/s', $tableWithCountries, $matches, PREG_PATTERN_ORDER);
                    $tableBody = $matches[1][0];
                    preg_match_all('/<td><a(.*?)>(.*?)<\/a>/s', $tableBody, $matches, PREG_PATTERN_ORDER);
                    $engCities = array();
                    for ($i = 0; $i < count($matches[2]); $i++) {
                        $engCities[$i]['engName'] = $matches[2][$i];
                    }
                    //print_r($engCities);
                    //если нет города, то написать транслитом
                    if (count($rusCities) >= count($engCities)) {
                        for ($i = 0; $i < count($rusCities); $i++) {
                            $arrayAllCities[$i]['rusName'] = $rusCities[$i]['rusName'];

                            if (isset($engCities[$i]['engName']))
                                $arrayAllCities[$i]['engName'] = $engCities[$i]['engName'];
                            else
                                $arrayAllCities[$i]['engName'] = transRusToEng($rusCities[$i]['rusName']);
                                //$arrayAllCities[$i]['engName'] = strtr($rusCities[$i]['rusName'], $translit);
                        }
                    } else {
                        for ($i = 0; $i < count($engCities); $i++) {
                            $arrayAllCities[$i]['engName'] = $engCities[$i]['engName'];

                            if (isset($rusCities[$i]['rusName']))
                                $arrayAllCities[$i]['rusName'] = $rusCities[$i]['rusName'];
                            else
                                $arrayAllCities[$i]['rusName'] = transEngToRus($engCities[$i]['engName']);
                                //$arrayAllCities[$i]['rusName'] = strtr($engCities[$i]['engName'], array_flip($translit));
                        }
                    }

                    //print_r($arrayAllCities);

                    foreach($arrayAllCities as $item){
                        $city = new Cities();
                        $city->rusName = $item['rusName'];
                        $city->engName = $item['engName'];
                        $city->idCountry = $countryId;
                        $city->save();
                    }
                    //echo 'country:'.$i.'|';
                    //$i++;
                    */

                    foreach($rusCities as $item){
                        $city = new Cities();
                        $city->rusName = $item['rusName'];
                        $city->engName = transRusToEng($item['rusName']);
                        $city->idCountry = $countryId;
                        $city->save();
                    }

                }
            }
    	}
	}
?>