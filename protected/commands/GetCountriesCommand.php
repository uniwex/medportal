<?php
	//для лотерей в которых не нужно указывать дату, а нужен только год
	class GetCountriesCommand extends CConsoleCommand{
		public function run($args){
            $url = 'https://www.artlebedev.ru/tools/country-list/';
            $content = file_get_contents($url);
            preg_match_all('/id="TldCodes"(.*?)>(.*?)<\/table>/s', $content, $matches, PREG_PATTERN_ORDER);
            $tableWithCountrues = $matches[0][0];
            preg_match_all('/<tbody>(.*?)<\/tbody>/s', $tableWithCountrues, $matches, PREG_PATTERN_ORDER);
            $tbody = $matches[1][0];
            preg_match_all('/<tr(.*?)<\/tr>/s', $tbody, $matches, PREG_PATTERN_ORDER);
            $tr = $matches[1];
            foreach($tr as $item) {
                preg_match_all('/<td(.*?)<\/td>/s', $item, $matchesTd, PREG_PATTERN_ORDER);
                preg_match_all('/>(.*?)</s', $matchesTd[0][0], $matches, PREG_PATTERN_ORDER);
                $tdCountryRusName = $matches[1][0];
                preg_match_all('/>(.*?)</s', $matchesTd[0][3], $matches, PREG_PATTERN_ORDER);
                $tdCountryID = $matches[1][0];
                preg_match_all('/>(.*?)</s', $matchesTd[0][2], $matches, PREG_PATTERN_ORDER);
                $tdCountryEngName = $matches[1][0];
                $countryModel = new Countries();
                $countryModel->rusName = $tdCountryRusName;
                $countryModel->engName = $tdCountryEngName;
                $countryModel->codeAlpha = $tdCountryID;
                $countryModel->save();
            }
            echo 'SUCCESS';
    	}
	}
?>