<?php
	class TheLotterCommand extends CConsoleCommand{
		public function run($args) 
    	{
    		
    		$page = SendHelper::send($args[0]);
    		$lid = $args[1];
			preg_match_all('/lotteryRef":(.*?),/', $page, $matches);
			$lotteryRef = $matches[1][0];
			
			$postData = "{'lotteryRef':'$lotteryRef'}";
			//$page = SendHelper::send('https://www.thelotter.com/ru/rezultaty-loto/usa-powerball/?DrawNumber=112820');
			$host = 'www.thelotter.com';
			$string = "POST /__Ajax/__AsyncControls.asmx/GetDrawsValueNameList HTTP/1.1\r\n"
           	. "Host: $host\r\n"
           	. "Content-Type: application/json; charset=utf-8\r\n"
           	. "Content-Length: 19\r\n"
           	. "Cookie: ASP.NET_SessionId=4dx15ilhfpzezq4ozkiqdybg\r\n"
           	. "\r\n"
           	. $postData;

			$draws = SendHelper::socketSend($host,$string);
			preg_match_all('/DrawRef":(\d+)/', $draws, $matches);
			//get drawIds
			$drawIds = $matches[1];

			preg_match_all('/\|\s\d+\s\D+\d+/', $draws, $matches);
			$drawDates = $matches[0];
			$dateI = 0;
			//get drawDates
			foreach ($drawDates as $key) {
				$tempDate = $key;
				$tempDate = str_replace('| ', '', $tempDate);
				$tempDate = date('Y.m.d H:i:s',strtotime($tempDate)).'<br>';
				$drawDates[$dateI] = $tempDate;
				$dateI++;
			}

			//for($i=0;$i<count($drawIds);$i++) {
			for($i=0;$i<1;$i++) {
				//присваиваем текущий ИД тиража
				$drawId = $drawIds[$i];
				$drawDate = $drawDates[$i];
				$extraNumbers = 0;
				$isJackpot = 0;
				$page = SendHelper::send('https://www.thelotter.com/ru/rezultaty-loto/usa-powerball/?DrawNumber='.$drawId);
				//находим сколько дополнительных номеров в тираже
				preg_match('/results-ball-additional/s', $page, $matches);
				$extraNumbers = count($matches);
				//находим выпавшие числа
				preg_match_all('/class="result(.*?)>(\d+)/s', $page, $matches);
				$droppedNumbers = $matches[2];
				$extraNumber1='';$extraNumber2='';
				if($extraNumbers <= 2)
					$extraNumber1 = array_pop($droppedNumbers);
				if($extraNumbers == 2)
					$extraNumber2 = array_pop($droppedNumbers);
				//вычисляем был ли разыран Джекпот
				preg_match('/'.$drawId.'(.*?)LocalWinningAmountToDisplay(.*?)divPadding(.*?)>(.*?)</s', $page, $matches);
				$jackPot = $matches[4];
				$symbol = substr($jackPot, 0, 1);
				if($symbol == '$'){
					$jackPot = str_replace('$ ', '', $jackPot);
					if($jackPot[0] != '0')
						$isJackpot = 1;
				}
				else
					$isJackpot = 0;

				//запись в базу
				/*
				$modelLottery = new ParsedLottery;
				$modelLottery->idLottery = $lid;
				$modelLottery->idDraw = $drawId;
				$sqlResult = ParsedLottery::model()->findBySql('select id from parsedLottery where idLottery=:id1 AND idDraw=:id2',array(':id1'=>$lid,':id2'=>$drawId));
				$modelLottery->date = $drawDate;
				$modelLottery->isJackpot = $isJackpot;
				$modelLottery->numbers = implode(',', $droppedNumbers);
				$modelLottery->extraNumbers = "$extraNumber1 $extraNumber2";
				if(!$sqlResult)
					$modelLottery->save();
				*/
				
				//вывод данных для тестинга
				
				print_r('id:'.$drawId);
				echo '<br>';
				print_r('lid:'.$lid);
				echo '<br>';
				print_r('date:'.$drawDate);
				echo '<br>';
				print_r('numbers:'.$droppedNumbers);
				echo '<br>';
				print_r('extr1:'.$extraNumber1);
				echo '<br>';
				print_r('extr2:'.$extraNumber2);
				echo '<br>';
				print_r('jackpot:'.$isJackpot);
				echo '<br>-------<br>';
				
			};

    	}
	}
?>