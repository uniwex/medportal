<?php
	class TheLotterCommand extends CConsoleCommand{
		public function run($args) 
    	{
    		$page = SendHelper::send($args[0]);
    		$lid = $args[1];

			preg_match_all('/lotteryRef":(.*?),/', $page, $matches);
			$lotteryRef = $matches[1][0];
			
			$postData = "{'lotteryRef':'$lotteryRef'}";
			//$page = SendHelper::send('https://www.thelotter.com/ru/rezultaty-loto/usa-powerball/?DrawNumber=112820');
			$host = 'www.thelotter.com';
			$string = "POST /__Ajax/__AsyncControls.asmx/GetDrawsValueNameList HTTP/1.1\r\n"
           	. "Host: $host\r\n"
           	. "Content-Type: application/json; charset=utf-8\r\n"
           	. "Content-Length: ".strlen($postData)."\r\n"
           	. "X-Requested-With: XMLHttpRequest\r\n"
           	
           	. "Cookie: ASP.NET_SessionId=4dx15ilhfpzezq4ozkiqdybg\r\n"
           	. "\r\n"
           	. $postData;

			$draws = SendHelper::socketSend($host,$string);
			
			preg_match_all('/DrawRef":(\d+)/', $draws, $matches);
			$drawIds = $matches[1];

			preg_match_all('/DisplayText":"Draw No. (.*?)\|/', $draws, $matches);
			$drawNames = $matches[1];

			preg_match_all('/\|\s\d+\s\D+\d+/', $draws, $matches);
			$drawDates = $matches[0];
			
			$dateI = 0;
			//get drawDates
			foreach ($drawDates as $key) {
				$tempDate = $key;
				$tempDate = str_replace('| ', '', $tempDate);
				$tempDate = date('Y.m.d H:i:s',strtotime($tempDate)).'<br>';
				$drawDates[$dateI] = $tempDate;
				$dateI++;
			}

			//удаляем все записи этой лотереи
			//ParsedLottery::	model()->deleteAll('idLottery = :lid AND id>0', array(':lid' => $lid));
			for($i=0;$i<count($drawIds);$i++) {
				//присваиваем текущий ИД тиража
				$drawId = $drawIds[$i];
				$drawDate = $drawDates[$i];
				$drawName = $drawNames[$i];
				$extraNumbers = 0;
				$isJackpot = 0;
				$sqlResult = ParsedLottery::model()->findBySql('select id from parsedLottery where idLottery=:id1 AND idDraw=:id2',array(':id1'=>$lid,':id2'=>$drawId));
				if($sqlResult == NULL){
					$page = SendHelper::send($args[0].'?DrawNumber='.$drawId);
					//находим сколько дополнительных номеров в тираже
					preg_match_all('/results-ball-additional/s', $page, $matches);
					$extraNumbers = count($matches[0]);
					//находим выпавшие числа
					preg_match_all('/class="result(.*?)>(\d+)/s', $page, $matches);
					$droppedNumbers = $matches[2];
					
					$extraNumber1='';$extraNumber2='';
					if($extraNumbers == 1)
						$extraNumber1 = array_pop($droppedNumbers);
					if($extraNumbers == 2){
						$extraNumber1 = array_pop($droppedNumbers);
						$extraNumber2 = array_pop($droppedNumbers);
					}
					//вычисляем был ли разыран Джекпот
					preg_match('/'.$drawId.'(.*?)LocalWinningAmountToDisplay(.*?)divPadding(.*?)>(.*?)</s', $page, $matches);
					$jackPot = $matches[4];
					preg_match('/\d/', $jackPot, $matches);
					if(empty($matches))
						$isJackpot = 0;
					else
						$isJackpot = 1;

					//запись в базу
					
					$modelLottery = new ParsedLottery;
					$modelLottery->idLottery = $lid;
					$modelLottery->idDraw = $drawId;
					$modelLottery->drawName = trim($drawName);
					
					$modelLottery->date = $drawDate;
					$modelLottery->isJackpot = $isJackpot;
					$modelLottery->numbers = implode(',', $droppedNumbers);
					$modelLottery->extraNumbers = "";
					if($extraNumbers == 1)
						$modelLottery->extraNumbers = "$extraNumber1";
					if($extraNumbers == 2)
						$modelLottery->extraNumbers = "$extraNumber1,$extraNumber2";
					
					$modelLottery->save();
				}
				
				//вывод данных для тестинга
				/*
				print_r('id:'.$drawId);
				echo '<br>';
				print_r('id:'.$drawName);
				echo '<br>';
				print_r('lid:'.$lid);
				echo '<br>';
				print_r('date:'.$drawDate);
				echo '<br>';
				print_r('numbers:'.$droppedNumbers);
				echo '<br>';
				print_r('extr1:'.$extraNumber1);
				echo '<br>';
				print_r('extr2:'.$extraNumber2);
				echo '<br>';
				print_r('jackpot:'.$isJackpot);
				echo '<br>-------<br>';
				*/
			};

			/*BEGIN CALCULATE INTERVALS*/
    		
			$criteria = new CDbCriteria();
			$criteria->params = array(':lid'=>$lid);
			//$criteria->select = 'numbers';
			$criteria->condition = "idLottery = :lid";
			$criteria->order = "idDraw DESC";
			//$criteria->limit = "30";
			//$criteria->offset = "38";
			$sqlData = ParsedLottery::model()->findAll($criteria);
			$lotteryData = Lotteries::model()->findByPk($lid);
			$matrix = array();
			foreach ($sqlData as $key) {
				$tmp = explode(',',$key->numbers);
				if($lotteryData->extraNumbersCount >0){
					$tmpExtra = str_replace(" ", "", $key->extraNumbers);
					$tmp = array_merge($tmp, explode(',',$tmpExtra));
				}
				//повторяющиеся элементы заменяем на -1
				//отрицательных чисел не может быть в выпавших числах
				//таким образом мы сохраняем количество элементов в массиве чисел
				//и в циклах производим проверку, если попалось число -1, то мы знаем, что здесь было повторяющееся число, а значит это число считать не нужно,
				//после этого переходим на следующую итерацию или выходим из цикла				
				for($i=0;$i<count($tmp);$i++){
					if($tmp[$i] == -1 )
						continue;
					if($i+1<count($tmp)){
						for($j=$i+1;$j<count($tmp);$j++){
							if($tmp[$i]==$tmp[$j])
								$tmp[$j]=-1;
						}
					}
				}
				array_push($matrix, array('id'=>$key->id, 'numbers'=>$tmp));
			}

			$i = 0;
			$n = 0;
			$sum = 0;
			$abc = 0;

			while($i<count($matrix)){
				$countOfArr = count($matrix[$i]['numbers']);
				$curElem = $matrix[$i]['numbers'][$n];
				$flag = 0;
				for($j=$i+1;$j<count($matrix);$j++){
					//echo "[$curElem]";
					for($k=0;$k<count($matrix[$j]['numbers']);$k++){
						//echo $matrix[$j]['numbers'][$k].'|';
						if($matrix[$j]['numbers'][$k] == -1){
							break;
						}
						if($matrix[$j]['numbers'][$k] == $curElem){
							if($j-$i-1 == 0){
								//if($flag == 2)
									$flag = 3;
								//else
									//$flag = 2;
								break;
							}
							$sum+=$j-$i-1;
							if($flag == 2)
								$sum -= 1;
							$flag = 1;
							break;
						}
					}
					if(($flag == 1)||($flag == 3))
						break;
				}
				$n++;
				//echo "[$n|$sum]";
				if($n >= $countOfArr){
					//$lottery = ParsedLottery::model()->findByPk($matrix[$i]['id']);
					//if($lottery->sumOfIntervals == -1){
						//echo '-1|';
						ParsedLottery::model()->updateByPk($matrix[$i]['id'], array('sumOfIntervals' => $sum));
					//}
					array_push($matrix[$i]['numbers'], $sum);
					$i++;
					$n=0;
					$sum = 0;
				}
				//if($abc > 22){
					//die();
				//}
				//$abc++;
			}

			/*END CALCULATE INTERVALS*/

    	}
	}
?>