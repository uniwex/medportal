<?php

/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 30.11.2015
 * Time: 11:31
 */
class StatisticsCommand extends CConsoleCommand
{
    public function run($args)
    {
        $criteria = new CDbCriteria;
        $criteria->order = 'time desc';
        $date = date('Y-m-d');
        $statistic = Statistic::model()->find($criteria);
        if ($statistic->time != $date) {
            $stats = new Statistic;
            $stats->time = $date;
            $stats->newUser = User::getCountNewUser($date);
            $stats->user = User::getCountUser('');
            $stats->doctor = User::getCountUser('doctor');
            $stats->clinic = User::getCountUser('clinic');
            $stats->pacient = User::getCountUser('patient');
            $stats->nurse = User::getCountUser('nurse');
            $stats->payed = Pay::getDailyProfit($date);
            $stats->purchase = Event::getCountOrder($date);
            $stats->article = Article::getCountArticle($date);
            $stats->save();
        }
    }
}