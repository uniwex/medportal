<?php
	class UploadLotteriesCommand extends CConsoleCommand{
		public function run($args) 
    	{

    		$sqlData = Lotteries::model()->findAll();
    		foreach ($sqlData as $record) {
    			$url = $record->URL;
    			$lid = $record->id;
    		    		
			    $url = preg_replace('/\s/', '', $url);
			    if(substr($url, -1, 1) != '/')
			    	$url = $url.'/';
			    $params = array($url,$lid);

			    $parser = 0;
			    preg_match_all('/stoloto.ru(.*?)year/s', $url, $matches, PREG_PATTERN_ORDER);
			    if($matches[0]){
			    	$parser = 1;
			    }
			    if($parser==0){
				    preg_match_all('/stoloto.ru(.*?)archive/s', $url, $matches, PREG_PATTERN_ORDER);
				    if($matches[0]){
				    	$parser = 2;
				    }
				}
			    if($parser == 0){
			    	preg_match_all('/stoloto.ru(.*?)from(.*?)/s', $url, $matches, PREG_PATTERN_ORDER);
			    	if($matches[0])
			    		$parser = 2;
			    }
			    if($parser == 0){
			    	preg_match_all('/thelotter.com/s', $url, $matches, PREG_PATTERN_ORDER);
			    	if($matches[0])
			    		$parser = 3;
			    }

			    //print_r($parser);
			    if($parser == 0){
			    	echo json_encode(array('error'));
			    }

	   		    if($parser == 1){
				    
					$command = "/usr/bin/php -f ".Yii::app()->params['path']."www/protected/cron.php stoloto $params[0] $params[1]";
					//echo 'ok';
					//echo $command;
					//die();
					$process = new Process($command);
					$processId = $process->getPid();

					$sql = "SELECT COUNT(*) as lotteries FROM parsedLottery WHERE idLottery=$lid ORDER BY id ASC";
					$command = Yii::app()->db->createCommand($sql);
					$results = $command->queryAll();
					$numLotteries = (int)$results[0]["lotteries"];

					$responseArray = array('ok',$processId,$numLotteries);
					$response = json_encode($responseArray);
					echo 'cron: parser'.$parser . $response;
				}
				if($parser == 2){
				    
					$command = "/usr/bin/php -f ".Yii::app()->params['path']."www/protected/cron.php stoloto2 $params[0] $params[1]";
					//echo 'ok';
					//echo $command;
					//die();
					$process = new Process($command);
					$processId = $process->getPid();

					$sql = "SELECT COUNT(*) as lotteries FROM parsedLottery WHERE idLottery=$lid ORDER BY id ASC";
					$command = Yii::app()->db->createCommand($sql);
					$results = $command->queryAll();
					$numLotteries = (int)$results[0]["lotteries"];

					$responseArray = array('ok',$processId,$numLotteries);
					$response = json_encode($responseArray);
					echo 'cron: parser'.$parser . $response;
				}
				if($parser == 3){
					
					$command = "/usr/bin/php -f ".Yii::app()->params['path']."www/protected/cron.php thelotter $params[0] $params[1]";
					//echo 'ok';
					//echo $command;
					//die();
					$process = new Process($command);
					$processId = $process->getPid();

					$sql = "SELECT COUNT(*) as lotteries FROM parsedLottery WHERE idLottery=$lid ORDER BY id ASC";
					$command = Yii::app()->db->createCommand($sql);
					$results = $command->queryAll();
					$numLotteries = (int)$results[0]["lotteries"];

					$responseArray = array('ok',$processId,$numLotteries);
					$response = json_encode($responseArray);
					echo 'cron: parser'.$parser . $response;
				}

			}
    	}
	}
?>