<?php
class SendHelper 
{
    // SendHelper::send($url, 'param=1&param=2')
    public static function send($url,$param=null,$cookie=null,$referer=null,$body1=false,$proxy='',$proxyType='',$proxyAuth='')
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        if ($proxy!=''){
            curl_setopt($ch, CURLOPT_PROXY, "$proxy");
            curl_setopt($ch, CURLOPT_PROXYTYPE, "$proxyType");
            if ($proxyAuth!=''){
                curl_setopt($ch, CURLOPT_PROXYUSERPWD, "$proxyAuth");
            }
        }
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_REFERER, $referer);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_COOKIE, $cookie);  //
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.2; en-US; Valve Steam Tenfoot/1431729692; ) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.104 Safari/537.36');
        if (trim($param)!=''){
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
        } else {
            curl_setopt($ch, CURLOPT_POST, 0);
        }

        $result=curl_exec($ch);
        
        $header=substr($result,0,curl_getinfo($ch,CURLINFO_HEADER_SIZE));
        $body=substr($result,curl_getinfo($ch,CURLINFO_HEADER_SIZE));

        preg_match_all("/Set-Cookie: (.*?)=(.*?);/i",$header,$res);
        $cookie='';
        foreach ($res[1] as $key => $value) {
            $cookie.= $value.'='.$res[2][$key].'; ';
        };
        curl_close($ch);

        if($body1 == false) return $cookie."@@@".$result;
        elseif($body1 == true) return $body;
    }
}
?>