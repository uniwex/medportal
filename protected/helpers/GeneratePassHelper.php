<?php
class GeneratePassHelper
{

    public static function generatePassword(){
        $chars="qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP";
        $max=rand(7,10);
        $size=StrLen($chars)-1; 
        $password=null;
        while($max--)
            $password.=$chars[rand(0,$size)];
        return $password;
    }

}