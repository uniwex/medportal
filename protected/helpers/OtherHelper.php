<?php

class OtherHelper
{

    /**
     * Вывод ошибок при валидации модели
     * @param $array - Массив ошибок $model->getErrors()
     * @param $name - Имя модели
     * @return string - Описание ошибок
     */
    public static function printError($array, $name)
    {
        $model = array(
            'Clinic' => 'Клиника',
            'Doctor' => 'Доктор',
            'Nurse' => 'Сиделка',
            'Patient' => 'Пациент',
            'Schedule' => 'Расписание',
            'Station' => 'Станции',
            'User' => 'Пользователи'
        );
        if (!empty($array)) {
            $result = '<br>';
            $result .= $model[$name].': <br>';
            foreach ($array as $key => $value) {
                foreach ($value as $error) {
                    $result .= '<li>' . addslashes($error) . '</li>';
                }
            }
            return $result;
        }
    }
}