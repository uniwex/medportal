<?php
	class GetEventsHelper{

		public static function getEvents($year, $month, $day, $userId, $isFullMonth = false)
    	{
            $MONTHES = array('Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь');
            $DAYS_OF_WEEK = array('Понедельник','Вторник','Среда','Четверг','Пятница','Суббота','Воскресенье');

            $events=array();
            $criteria = new CDbCriteria;
            //get data from DataBase by user type and event type
            if  ((User::model()->findByPk(Yii::app()->user->id)->type != 'nurse')&&(User::model()->findByPk(Yii::app()->user->id)->type != 'clinic')) {
                if($isFullMonth == false) {
                    $criteria->condition = '(idOwner=:idOwner OR idExecuter=:idExecuter) AND eventDate=:eventDate AND type<>:type ORDER BY eventDate, eventTime ASC';
                    $criteria->params = array(':idOwner' => $userId, ':idExecuter' => $userId, ':eventDate' => $year . "-" . $month . "-" . $day, ':type'=>'nurse');
                }
                if($isFullMonth == true) {
                    $criteria->condition = '(idOwner=:idOwner OR idExecuter=:idExecuter) AND :eventDateBegin<=eventDate AND :eventDateEnd>=eventDate AND type<>:type ORDER BY eventDate, eventTime ASC';
                    $criteria->params = array(':idOwner' => $userId, ':idExecuter' => $userId, ':eventDateBegin' => $year . "-" . $month . "-01", ':eventDateEnd' => $year . "-" . $month . "-" . date('t'), ':type'=>'nurse');
                }
                $events = Event::model()->findAll($criteria);
                if($isFullMonth == false) {
                    $criteria->condition = '(idOwner=:idOwner OR idExecuter=:idExecuter) AND eventDate<=:eventDate AND :eventDate<=eventDateFinish AND type=:type';
                    $criteria->params = array(':idOwner' => $userId, ':idExecuter' => $userId, ':eventDate' => $year . "-" . $month . "-" . $day, ':type' => 'nurse');
                }
                if($isFullMonth == true) {
                    $criteria->condition = '(idOwner=:idOwner OR idExecuter=:idExecuter) AND eventDate<=:eventDate AND :eventDate<=eventDateFinish AND type=:type';
                    $criteria->params = array(':idOwner' => $userId, ':idExecuter' => $userId, ':eventDate' => $year . "-" . $month . "-" . $day, ':type' => 'nurse');
                }
                $events = array_merge(Event::model()->findAll($criteria), $events);
            }
            if (User::model()->findByPk(Yii::app()->user->id)->type == 'nurse'){
                $criteria->condition = '(idOwner=:idOwner OR idExecuter=:idExecuter) AND eventDate<=:eventDate AND :eventDate<=eventDateFinish AND type=:type';
                $criteria->params = array(':idOwner' => $userId, ':idExecuter' => $userId, ':eventDate' => $year . "-" . $month . "-" . $day, ':type'=>'nurse');
                $events = Event::model()->findAll($criteria);
                $criteria->condition = '(idOwner=:idOwner OR idExecuter=:idExecuter) AND eventDate=:eventDate AND type<>:type';
                $criteria->params = array(':idOwner' => $userId, ':idExecuter' => $userId, ':eventDate' => $year . "-" . $month . "-" . $day, ':type'=>'nurse');
                $events = array_merge(Event::model()->findAll($criteria), $events);
            }
            if (User::model()->findByPk(Yii::app()->user->id)->type == 'clinic') {
                $doctors = Schedule::model()->findAllByAttributes(array('idUser'=>Yii::app()->user->id));
                foreach($doctors as $doctor) {
                    $criteria->condition = '(idOwner=:idOwner OR idExecuter=:idExecuter) AND eventDate=:eventDate AND type<>:type';
                    $criteria->params = array(':idOwner' => $doctor->id, ':idExecuter' => $doctor->id, ':eventDate' => $year . "-" . $month . "-" . $day, ':type' => 'nurse');
                    $events = array_merge(Event::model()->findAll($criteria), $events);
                }
                $criteria->condition = 'idOwner=:idOwner AND eventDate=:eventDate AND type=:type';
                $criteria->params = array(':idOwner' => $userId, ':eventDate' => $year . "-" . $month . "-" . $day, ':type' => 'note');
                $events = array_merge(Event::model()->findAll($criteria), $events);
            }

            $monthName = $MONTHES[$month-1];
            if(($month == 3)||($month == 8))
                $monthName .= 'а';

            $dayOfWeek = date('N', strtotime($year . "-" . $month . "-" . $day));
            $dayOfWeekName = $DAYS_OF_WEEK[$dayOfWeek-1];

            //form data
            if($isFullMonth == true)
                $html = "";
            else
                $html = "<div>$day $monthName, $dayOfWeekName</div>";
            $eventDate = "";
            foreach($events as $event){
                if($isFullMonth == true) {
                    if ($eventDate != $event->eventDate) {
                        $eventDate = $event->eventDate;
                        $html .= "<div>$event->eventDate</div>";
                    }
                }
                if($event->type=='schedule') {
                    if(User::model()->findByPk(Yii::app()->user->id)->type == 'clinic'){
                        $patient = User::model()->findByPk($event->idOwner);
                        $doctor = Schedule::model()->findByPk($event->idExecuter);
                        $specialization = Specialization::model()->findByPk($doctor->specialization);
                        $html .= '<div class="calendar-event"><i class="fa fa-calendar-times-o event-remove" data-id="' . $event->id . '"></i><span style="margin-right:10px;">' . $event->eventTime . '</span><span>Приём пациента: ' . $patient->name . ' у врача: ' . $specialization->name . ', ' .$doctor->name.'</span></div>';
                    } else {
                        if ($event->idExecuter == $userId) {
                            $patient = User::model()->findByPk($event->idOwner);
                            $html .= '<div class="calendar-event"><i class="fa fa-calendar-times-o event-remove" data-id="' . $event->id . '"></i><span style="margin-right:10px;">' . $event->eventTime . '</span><span>Приём пациента: ' . $patient->name . '</span></div>';
                        } else {
                            $doctor = Schedule::model()->findByPk($event->idExecuter);
                            $specialization = Specialization::model()->findByPk($doctor->specialization);
                            $html .= '<div class="calendar-event"><i class="fa fa-calendar-times-o event-remove" data-id="' . $event->id . '"></i><span style="margin-right:10px;">' . $event->eventTime . '</span><span>Запись ко врачу: ' . $specialization->name . ", " . $doctor->name . '</span></div>';
                        }
                    }
                }
                if($event->type=='note') {
                    $html .= '<div class="calendar-event"><i class="fa fa-calendar-times-o event-remove" data-id="'.$event->id.'"></i><span style="margin-right:5px;">' . $event->eventTime . '</span><span>Напоминание: ' . $event->description . '</span></div>';
                }
                if($event->type=='nurse') {
                    if($event->idExecuter == $userId){
                        $patient = User::model()->findByPk($event->idOwner);
                        $html .= '<div class="calendar-event"><i class="fa fa-calendar-times-o event-remove" data-id="' . $event->id . '"></i><span style="margin-right:10px;">' . $event->eventTime . '</span><span>Вы сиделка у пациента: ' . $patient->name . '</span></div>';
                    }
                    else {
                        $nurse = User::model()->findByPk($event->idExecuter);
                        $html .= '<div class="calendar-event"><i class="fa fa-calendar-times-o event-remove" data-id="' . $event->id . '"></i><span>' . $event->eventTime . '</span><span>Сиделка: ' . $nurse->name . '</span></div>';
                    }
                }
            }

            return $html;
    	}

        public static function getEventsMonth($year, $month, $day, $userId){

            $MONTHES = array('Января','Февраля','Марта','Апреля','Мая','Июня','Июля','Августа','Сентября','Октября','Ноября','Декабря');
            $DAYS_OF_WEEK = array('Понедельник','Вторник','Среда','Четверг','Пятница','Суббота','Воскресенье');

            $countDays = date('t',strtotime($year . "-" . $month . "-01"));
            $criteria = new CDbCriteria();
            $events = array();
            $html = "";
            for($i=1;$i<=$countDays;$i++){
                $events = array();
                $dayOfWeek = date('N',strtotime($year . "-" . $month . "-" . $i));
                $tempHtml = "<div class=\"calendar-on-month\">$i ".$MONTHES[$month-1].", ".$DAYS_OF_WEEK[$dayOfWeek-1]."</div>";
                $isEvents = 0;
                if(User::model()->findByPk(Yii::app()->user->id)->type == 'patient'){
                    $criteria->condition = '(idOwner=:idOwner OR idExecuter=:idExecuter) AND :curDate>=eventDate AND :curDate<=eventDateFinish AND type=:type';
                    $criteria->params = array(':idOwner' => $userId, ':idExecuter' => $userId, ':curDate' => $year . "-" . $month . "-" . $i, ':type'=>'nurse');
                    $events = Event::model()->findAll($criteria);
                    if($events) {
                        $isEvents = 1;
                        foreach ($events as $key) {
                            $tempHtml .= "<span>Сиделка ". Nurse::model()->findByPk($key->idExecuter)->name."</span>";
                        }
                    }
                    $criteria->condition = '(idOwner=:idOwner OR idExecuter=:idExecuter) AND :curDate=eventDate AND type=:type ORDER BY eventTime ASC';
                    $criteria->params = array(':idOwner' => $userId, ':idExecuter' => $userId, ':curDate' => $year . "-" . $month . "-" . $i, ':type'=>'schedule');
                    $events = Event::model()->findAll($criteria);
                    if($events) {
                        $isEvents = 1;
                        foreach ($events as $key) {
                            $tempHtml .= "<div>" . $key->eventTime . " Приём у врача: " . Specialization::model()->findByPk(Schedule::model()->findByPk($key->idExecuter)->specialization)->name . " " . Schedule::model()->findByPk($key->idExecuter)->name."</div>";
                        }
                    }
                    $criteria->condition = '(idOwner=:idOwner OR idExecuter=:idExecuter) AND :curDate=eventDate AND type=:type ORDER BY eventTime ASC';
                    $criteria->params = array(':idOwner' => $userId, ':idExecuter' => $userId, ':curDate' => $year . "-" . $month . "-" . $i, ':type'=>'note');
                    $events = Event::model()->findAll($criteria);
                    if($events) {
                        $isEvents = 1;
                        foreach ($events as $key) {
                            $tempHtml .= "<div>" . $key->eventTime . " Заметка: ". $key->description."</div>";
                        }
                    }
                }
                if(User::model()->findByPk(Yii::app()->user->id)->type == 'nurse'){
                    $criteria->condition = '(idOwner=:idOwner OR idExecuter=:idExecuter) AND :curDate>=eventDate AND :curDate<=eventDateFinish AND type=:type';
                    $criteria->params = array(':idOwner' => $userId, ':idExecuter' => $userId, ':curDate' => $year . "-" . $month . "-" . $i, ':type'=>'nurse');
                    $events = Event::model()->findAll($criteria);
                    if($events) {
                        $isEvents = 1;
                        foreach ($events as $key) {
                            $tempHtml .= "<span>Пациент: ". User::model()->findByPk($key->idOwner)->name."</span>";
                        }
                    }
                    $criteria->condition = '(idOwner=:idOwner OR idExecuter=:idExecuter) AND :curDate=eventDate AND type=:type ORDER BY eventTime ASC';
                    $criteria->params = array(':idOwner' => $userId, ':idExecuter' => $userId, ':curDate' => $year . "-" . $month . "-" . $i, ':type'=>'note');
                    $events = Event::model()->findAll($criteria);
                    if($events) {
                        $isEvents = 1;
                        foreach ($events as $key) {
                            $tempHtml .= "<div>" . $key->eventTime . " Заметка: ". $key->description."</div>";
                        }
                    }
                }
                if(User::model()->findByPk(Yii::app()->user->id)->type == 'doctor'){
                    $criteria->condition = '(idOwner=:idOwner OR idExecuter=:idExecuter) AND :curDate=eventDate AND type=:type ORDER BY eventTime ASC';
                    $criteria->params = array(':idOwner' => $userId, ':idExecuter' => $userId, ':curDate' => $year . "-" . $month . "-" . $i, ':type'=>'schedule');
                    $events = Event::model()->findAll($criteria);
                    if($events) {
                        $isEvents = 1;
                        foreach ($events as $key) {
                            $tempHtml .= "<div>" . $key->eventTime . " пациент: ". User::model()->findByPk($key->idOwner)->name."</div>";
                        }
                    }
                    $criteria->condition = '(idOwner=:idOwner OR idExecuter=:idExecuter) AND :curDate=eventDate AND type=:type ORDER BY eventTime ASC';
                    $criteria->params = array(':idOwner' => $userId, ':idExecuter' => $userId, ':curDate' => $year . "-" . $month . "-" . $i, ':type'=>'note');
                    $events = Event::model()->findAll($criteria);
                    if($events) {
                        $isEvents = 1;
                        foreach ($events as $key) {
                            $tempHtml .= "<div>" . $key->eventTime . " Заметка: ". $key->description."</div>";
                        }
                    }
                }
                if(User::model()->findByPk(Yii::app()->user->id)->type == 'clinic'){
                    $doctors = Schedule::model()->findAllByAttributes(array('idUser'=>Yii::app()->user->id, 'type' => 'clinicDoc'));
                    foreach($doctors as $doctor) {
                        $criteria->condition = 'idExecuter=:idExecuter AND eventDate=:eventDate AND type=:type ORDER BY eventTime ASC';
                        $criteria->params = array(':idExecuter' => $doctor->id, ':eventDate' => $year . "-" . $month . "-" . $i, ':type' => 'schedule');
                        $events = array_merge(Event::model()->findAll($criteria), $events);
                    }
                    if($events) {
                        $isEvents = 1;
                        foreach ($events as $key) {
                            $tempHtml .= "<div>" . $key->eventTime . " Доктор: " . Specialization::model()->findByPk(Schedule::model()->findByPk($key->idExecuter)->specialization)->name . "-" . Schedule::model()->findByPk($key->idExecuter)->name . ", пациент: " . User::model()->findByPk($key->idOwner)->name . "</div>";
                        }
                    }
                    $criteria->condition = 'idOwner=:idOwner AND :curDate=eventDate AND type=:type ORDER BY eventTime ASC';
                    $criteria->params = array(':idOwner' => $userId, ':curDate' => $year . "-" . $month . "-" . $i, ':type'=>'note');
                    $events = Event::model()->findAll($criteria);
                    if($events) {
                        $isEvents = 1;
                        foreach ($events as $key) {
                            $tempHtml .= "<div>" . $key->eventTime . " Заметка: ". $key->description."</div>";
                        }
                    }
                }
                if($isEvents) {
                    $html .= $tempHtml;
                    $html .= "<hr>";
                }
            }

            return $html;
        }
	}
?>