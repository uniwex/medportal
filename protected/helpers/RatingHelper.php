<?php

class RatingHelper
{

    public static function getRating($idElem, $typeElem)
    {
        $criteria = new CDbCriteria;
        $criteria->select = 'avg(rating) as rating';
        $criteria->addCondition('id_elem = '.$idElem.' and type_elem=\''.$typeElem.'\'');
        $avg = Rating::model()->find($criteria)->rating;
        if (isset($avg))
            return $avg;
        else
            return 0;
    }

    public static function getUserRating($idElem,$typeElem){
        $id_user = Yii::app()->user->id;
        //return array('id_user' => $id_user, 'id_elem' => $idElem, 'type_elem' => $typeElem);
        $rating = Rating::model()->find('id_user = :id_user and id_elem = :id_elem and type_elem = :type_elem',
            ['id_user' => $id_user, 'id_elem' => $idElem, 'type_elem' => $typeElem]);
        if (isset($rating)) {
            return $rating->rating;
        } else return 0;
    }

    public static function setAvgElemRating($idElem,$typeElem) {
        $avgRating = self::getRating($idElem,$typeElem);
        if ($typeElem == 'article') {
            $article = Article::model()->findByPk($idElem);
            $article->avg_rating = (float)$avgRating;
            $article->save();
        }
        if($typeElem == "digest") {
            $digest = Digest::model()->findByPk($idElem);
            $digest->avg_rating = (float)$avgRating;
            $digest->save();
        }
    }

    public static function setRating($idElem, $typeElem, $value)
    {
        $id_user = Yii::app()->user->id;
        $rating = Rating::model()->find('id_user = :id_user and id_elem = :id_elem and type_elem = :type_elem',
            ['id_user' => $id_user, 'id_elem' => $idElem, 'type_elem' => $typeElem]);
        self::setAvgElemRating($idElem,$typeElem);
        if (isset($rating)) {
            $rating->rating = intval($value);
            if ($rating->save()) return true; else return false;
        } else {
            $rating = new Rating();
            $rating->id_user = $id_user;
            $rating->id_elem = $idElem;
            $rating->type_elem = $typeElem;
            $rating->rating = intval($value);
            if ($rating->save()) return true; else return var_dump($rating->getErrors());
        }
    }

    public static function renderStars($selected,$function = false, $selected_class = 'activeStar') {
        $html = '';
        $count = 5;
        for($i=0;$i<round($selected);$i++){
            if ($i < 6) {
                /*
                if ($function == false){
                    $html .= '<img class="' . $selected_class . '" data-test="1">';
                } else {
                    $html .= '<img class="' . $selected_class . '"' . $function . ' data-test="2">';
                }
                */
                if ($function == false){
                    $html .= "<span>" . Yii::app()->params['goldStarSmall'] . "</span>";
                } else {
                    $html .= "<span $function>" . Yii::app()->params['goldStarSmall'] . "</span>";
                }
            }
            $count --;
        }
        for($i=0;$i<$count;$i++) {
            /*
            if ($function == false) {
                $html .= ' <img class="" data-test="3">';
            } else {
                $html .= ' <img class="" ' . $function . ' data-test="4">';
            }
            */
            if ($function == false) {
                $html .= "<span>" . Yii::app()->params['grayStarSmall'] . "</span>";
            } else {
                $html .= "<span $function>" . Yii::app()->params['grayStarSmall'] . "</span>";
            }
        }
        return $html;
    }
}