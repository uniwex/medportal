<?php
class SendHelper
{

    public static function send($url,$param=null,$cookie=null,$referer=null,$proxy='',$proxyType='',$proxyAuth=''){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        if ($proxy!=''){
            curl_setopt($ch, CURLOPT_PROXY, "$proxy");
            curl_setopt($ch, CURLOPT_PROXYTYPE, "$proxyType");
            if ($proxyAuth!=''){
                curl_setopt($ch, CURLOPT_PROXYUSERPWD, "$proxyAuth");
            }
        }
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_REFERER, $referer);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_COOKIE, $cookie);  //
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.0.1) Gecko/2008070208');
        if (trim($param)!=''){
            curl_setopt($ch, CURLOPT_POST, 1);
            //curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8','','{\'lotteryRef\'=\'25\'}'));
            curl_setopt($ch, CURLOPT_POSTFIELDS, '');

        } else {
            curl_setopt($ch, CURLOPT_POST, 0);
        }

        $result=curl_exec($ch);

        $header=substr($result,0,curl_getinfo($ch,CURLINFO_HEADER_SIZE));
        $body=substr($result,curl_getinfo($ch,CURLINFO_HEADER_SIZE));
        //echo $header;
        preg_match_all("/Set-Cookie: (.*?)=(.*?);/i",$header,$res);
        $cookie='';
        foreach ($res[1] as $key => $value) {
            $cookie.= $value.'='.$res[2][$key].'; ';
        };
        curl_close($ch);

        return $result;
        //return $cookie."@@@".$result;
    }

    public static function socketSend($host, $string){
        $fp = fsockopen("ssl://$host", 443);
        fputs($fp, $string);
        
        $temp = fread($fp, 100000);
        $responseLength = strlen($temp);
        $html = $temp;

        while(strlen($temp) == $responseLength){
            $temp = fread($fp, 100000);
            $html .= $temp;
        }
        fclose($fp);
        return $html;
    }

}