<?php
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.helpers.*',
	),

	// application components
	'components'=>array(

		'user'=>array(
			// enable cookie-based authentication
			//'allowAutoLogin'=>true,
		),

		// uncomment the following to enable URLs in path-format
		
		// database settings are configured in database.php

		
		'db'=>array(
            'connectionString' => 'mysql:host=localhost;dbname=user_lottery',
            'emulatePrepare' => true,
            'username' => 'user_root1',
            'password' => '1234567',
            'charset' => 'utf8',
            'tablePrefix' => '',
        ),
       
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
		'path' => '/var/www/user/data/www/loto.tovarhouse.ru'
	),
);
