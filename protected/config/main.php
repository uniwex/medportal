<?php
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.helpers.*',
        'application.extensions.*',
    ),

    'modules' => array(
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => '111',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
//            'ipFilters'=>array('88.86.82.189','::1'),
            'ipFilters' => array('127.0.0.1', '*'),
        ),

    ),

    'theme'=>'default',

    'preload' => array('log'),
    // application components
    'components' => array(


        'user' => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
        ),

        // uncomment the following to enable URLs in path-format

        'urlManager' => array(
            'showScriptName' => false,
            'urlFormat' => 'path',
            'rules' => array(
                'articles/<id:[\d]+>' => '/articles/view',
                '<controller:\w+>/<id:\d+>' => '<controller>/',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                'site/doctor/<id:[\d]+>' => '/site/doctor',
                'site/clinic/<id:[\d]+>' => '/site/clinic',
                'site/nurse/<id:[\d]+>' => '/site/nurse',
                'articles/view/<id:[\d]+>' => '/articles/view',
                'articles/edit/<id:[\d]+>' => '/articles/edit',
                'articles/remove/<id:[\d]+>' => '/articles/remove',
                'digest/view/<id:[\d]+>' => '/digest/view',
                'digest/edit/<id:[\d]+>' => '/digest/edit',
                'digest/remove/<id:[\d]+>' => '/digest/remove',
                'tickets/view/<id:[\d]+>' => '/tickets/view',
                'visit/clinic/<id:[\d]+>' => '/visit/clinic',
                'articles/list/<id:[\d]+>' => '/articles/list',
                'scienceworks/download/<id:[\d]+>' => '/site/scienceworks/download',
            ),
        ),

        //remote
        /*
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=u0101182_default',
            'emulatePrepare' => true,
            'username' => 'doctor',
            'password' => 'V3e9J1j9',
            'charset' => 'utf8',
            'tablePrefix' => '',
        ),
        */
        //local
        'errorHandler' => [
            'errorAction' => '/site/error',
        ],
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CWebLogRoute',
                    'categories' => 'application',
                    'levels'=>'error, warning, trace, profile, info',
                ),
            ),
        ),
        'db' => array(
            // включаем профайлер
            'enableProfiling'=>true,
            // показываем значения параметров
            'enableParamLogging' => true,
            'connectionString' => 'mysql:host=localhost;dbname=u0101182_default',
            'emulatePrepare' => true,
            'username' => 'doctor',
            'password' => 'V3e9J1j9',
            'charset' => 'utf8',
            'tablePrefix' => '',
        ),

        /*'clientScript' => array(
            'scriptMap' => array(
                'jquery.js' => Yii::app()->request->baseUrl . '/assets/scripts/jquery/jquery-2.1.4.js',
                'jquery.min.js' => Yii::app()->request->baseUrl . '/assets/scripts/jquery/jquery-2.1.4.min.js',
            )
        ),*/

        'sms' => array(
            'class' => 'application.extensions.Sms',
            'api_id' => 'ce408444-264c-1b94-91e7-d498531ac495',
            'login' => '89115180238',
            'password' => 'uniwex123'
        ),

        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                // 'levels'=>'error, warning, trace, profile, info',
                array(
                    'class' => 'CWebLogRoute',
                    'categories' => 'application',
                    'levels' => 'error',
                    'enabled' => 'true',
                ),

                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error',
                ),
            ),
        ),

    ),
    'language' => 'ru',
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'webmaster@example.com',
        'path' => '/var/www/loto2.contrastfoto.ru/',
        'socialNetLink' => 'medportal',
        'socialNetworks' => array(
            'vkontakte' => 'http://vk.com/',
            'odnoklassniki' => 'http://odnoklassniki.ru/',
            'facebook' => 'http://facebook.com/',
            'twitter' => 'http://twitter.com/',
            'instagram' => 'http://instagram.com/',
        ),
        'goldStarSmall' => '<svg height="20" width="20"><polygon points="10,1 4,19 19,8 1,8 16,19" style="fill:#EE7600;stroke:#EE7600;stroke-width:1;fill-rule:nonzero;"/>SVG n-a</svg>',
        'grayStarSmall' => '<svg height="20" width="20"><polygon points="10,1 4,19 19,8 1,8 16,19" style="fill:#B2BDC4;stroke:#B2BDC4;stroke-width:1;fill-rule:nonzero;"/>SVG n-a</svg>',
        'goldStarBig' => '<svg height="30" width="30"><polygon points="15,1 6,28 28,12 1,12 24,28" style="fill:#EE7600;stroke:#EE7600;stroke-width:1;fill-rule:nonzero;"/>SVG n-a</svg>',
        'grayStarBig' => '<svg height="30" width="30"><polygon points="15,1 6,28 28,12 1,12 24,28" style="fill:#B2BDC4;stroke:#B2BDC4;stroke-width:1;fill-rule:nonzero;"/>SVG n-a</svg>',
        'typeHuman' => array(
            1 => 'Дети',
            2 => 'Взрослые',
            3 => 'Все'
        ),
        'status' => array(
            0 => 'В обработке',
            1 => 'Ожидает Вашего ответа',
            2 => 'Закрыто'
        ),
        'statusLabel' => array(
            0 => 'label label-warning',
            1 => 'label label-primary',
            2 => 'label label-success'
        ),
        'statusModer' => array(
            0 => 'Нужна помощь',
            1 => 'Ожидает ответа пользователя',
            2 => 'Закрыто'
        ),
        'typeAction' => array(
            'in' => 'Пополнение',
            'out' => 'Списание'
        ),
        'typePay' => array(
            'manual' => 'Ручное',
            'auto' => 'Авто'
        ),
        'typeUser' => array(
            'doctor' => 'Доктор',
            'clinic' => 'Клиника',
            'patient' => 'Пациент',
            'nurse' => 'Сиделка'
        ),
        'robokassa' => array(
            0 => 'veta­grand',
            1 => 'admin',
            2 => 'gs37WBk7a7'
        )

    )



);
