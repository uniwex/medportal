<?
	return array(
		'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
		'name'=>'Cron',
		'params'=>array(
			'adminEmail'=>'webmaster@example.com',
			'path' =>'/var/www/user/data/www/zapiskdoktoru.ru'
		),
		'preload'=>array('log'),

		'import'=>array(
			'application.components.*',
			'application.models.*',
			'application.commands.*',
			'application.helpers.*',
		),
		'components'=>array(
			'db'=>array(
	            'connectionString' => 'mysql:host=localhost;dbname=u0101182_default',
	            'emulatePrepare' => true,
	            'username' => 'doctor',
	            'password' => 'V3e9J1j9',
	            'charset' => 'utf8',
	            'tablePrefix' => '',
	        ),
		),
	);
?>