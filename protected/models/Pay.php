<?php

/**
 * This is the model class for table "pay".
 *
 * The followings are the available columns in table 'pay':
 * @property integer $id
 * @property string $action
 * @property string $type
 * @property integer $count
 * @property integer $idUser
 * @property string $time
 */
class Pay extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'pay';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('action, type, count, idUser, time', 'required'),
            array('count, idUser', 'numerical', 'integerOnly' => true),
            array('action', 'length', 'max' => 3),
            array('type', 'length', 'max' => 6),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, action, type, count, idUser, time', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'idUser')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'Код операции',
            'action' => 'Действие',
            'type' => 'Тип оплаты',
            'count' => 'Сумма операции',
            'idUser' => 'Пользователь',
            'time' => 'Дата операции',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('action', $this->action, true);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('count', $this->count);
        $criteria->compare('idUser', $this->idUser);

        if ($this->time) {
            $date = explode('-', $this->time);
            $dateFrom = trim(str_replace('/', '-', $date[0])) . ' 00:00:00';
            if (count($date) == 2) $dateTo = trim(str_replace('/', '-', $date[1])) . ' 23:59:59';
            else $dateTo = trim(str_replace('/', '-', $date[0])) . ' 23:59:59';
            $criteria->addBetweenCondition('time', $dateFrom, $dateTo);
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Pay the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function getDailyProfit($date) {
        $criteria = new CDbCriteria;
        $criteria->select = 'sum(count) as count';
        $criteria->compare('time', $date, true);
        $sum = self::model()->find($criteria)->count;
        return $sum == null ? 0 : $sum;
    }
}
