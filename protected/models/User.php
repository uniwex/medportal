<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $phone
 * @property string $password
 * @property double $money
 * @property string $email
 * @property integer $emailEnable
 * @property string $type
 * @property string $dateRegistration
 * @property string $vipFinish
 * @property string $status
 * @property string $restoreCode
 * @property string $city
 * @property string $country
 * @property int $isConfirm
 */
class User extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public $statistic;
    public $referal;
    public $countReferal;


    public function tableName()
    {
        return 'user';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('email, name', 'required', 'message' => 'Поле {attribute} не может быть пустым', 'except' => 'login'),
            array('phone', 'required', 'message' => 'Поле {attribute} не может быть пустым'),
            array('type', 'required', 'message' => 'Поле {attribute} не может быть пустым'),
            array('password', 'required', 'message' => 'Поле {attribute} не может быть пустым', 'on' => 'insert, login'),


            /*array('email, name, country, city', 'required', 'message' => 'Поле {attribute} не может быть пустым', 'except' => 'login'),
            //array('email, name', 'required', 'message' => 'Поле {attribute} не может быть пустым1', 'except' => 'login'),
            array('phone', 'required', 'message' => 'Поле {attribute} не может быть пустым'),
            array('password', 'required', 'message' => 'Поле {attribute} не может быть пустым', 'on' => 'insert, login'),

            //array('city, country', 'length', 'min' => 2, 'max' => 255, 'tooShort' => 'Поле {attribute} не может быть пустым', 'tooLong' => 'Поле {attribute} не может быть пустым'),

            array('money, emailEnable, isConfirm', 'numerical', 'message' => 'Поле {attribute} должно быть числовым'),

            // Телефон должен соответствовать шаблону
            array('phone', 'match', 'pattern' => '/^\+[\d+]{11}$/', 'message' => 'Ошибка валидации телефона'),
            array('phone, email', 'unique', 'message' => 'Такой {attribute} уже используется', 'on' => 'insert', 'except' => 'login'),

            // E-mail должен соответствовать шаблону
            array('email', 'email', 'message' => 'Неверный {attribute}'),

            // Длина пароля не менее 6 символов
            array('password, restoreCode', 'length', 'min' => 6, 'max' => 32, 'tooShort' => Yii::t("translation", "Длина поля {attribute} должна быть от 6 до 30 символов"),
                'tooLong' => Yii::t("translation", "Длина поля {attribute} должна быть от 6 до 30 символов"), 'on' => 'insert'),
            array('type', 'length', 'min' => 1, 'tooShort' => Yii::t("translation", "{attribute} слишком короткий, минимум 6 символов"), 'tooLong' => Yii::t("translation", "{attribute} слишком длинный, максимум 30 символов")),
            array('phone, email', 'length', 'max' => 50, 'min' => 5, 'tooShort' => 'Длина поля {attribute} должна быть от 5 до 50 символов', 'tooLong' => 'Длина поля {attribute} должна быть от 5 до 50 символов'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id,statistic, referal, phone, password, money, email, emailEnable, type, city, country, dateRegistration, vipFinish, status, isConfirm', 'safe', 'on' => 'search'),*/
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'ticket' => array(self::HAS_MANY, 'Ticket', 'idUser'),
            'ticketMessage' => array(self::HAS_MANY, 'TicketMessages', 'id'),
            'review' => array(self::HAS_MANY, 'Review', 'idUserTo', 'on'=>'review.moderate=1'),
            'station' => array(self::HAS_ONE, 'Station', 'idUser'),
            'schedule' => array(self::HAS_MANY, 'Schedule', 'idUser'),
            'clinic' => array(self::HAS_ONE, 'Clinic', 'idUser'),
            'doctor' => array(self::HAS_ONE, 'Doctor', 'idUser'),
            'nurse' => array(self::HAS_ONE, 'Nurse', 'idUser'),
            'visitorsNurse' => array(self::HAS_MANY, 'Visitors', 'idUser'),
            'visitorsPatient' => array(self::HAS_MANY, 'Visitors', 'idPatient'),
            'pay' => array(self::HAS_MANY, 'Pay', 'idUser'),
            'partner' => array(self::HAS_MANY, 'Partner', 'idReferal'),
            'referrer' => array(self::HAS_MANY, 'Referral', 'idReferer')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Имя и фамилия',
            'email' => 'Электронная почта',
            'phone' => 'Телефон',
            'password' => 'Пароль',
            'money' => 'Баланс',
            'emailEnable' => 'Показывать e-mail',
            'type' => 'Тип пользователя',
            'restoreCode' => 'Код восстановление',
            'dateRegistration' => 'Дата регистрации',
            'vipFinish' => 'Vip Finish',
            'status' => 'Статус',
            'city' => 'Город',
            'country' => 'Страна',
            'isConfirm' => 'Модерация',
            'statistic' => 'Статистика оплат',
            'referal' => 'Реферальная система',
            'countReferal' => 'Кол-во рефералов'

        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('phone', $this->phone, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('money', $this->money);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('emailEnable', $this->emailEnable);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('restoreCode', $this->restoreCode, true);
        $criteria->compare('dateRegistration', $this->dateRegistration, true);
        $criteria->compare('vipFinish', $this->vipFinish, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('city', $this->city, true);
        $criteria->compare('country', $this->country, true);
        $criteria->compare('isConfirm', $this->isConfirm, true);
        $criteria->compare('countReferal', $this->countReferal, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function countReferal($k)
    {
        $model = Partner::model()->find('idReferal = :id', array(':id' => $k));
        if (isset($model)) {
            $count = $model->money;
            return $count;
        } else
            return 0;
    }


    public static function typeUser($k)
    {
        $doctorType = array(
            'doctor' => 'Доктор',
            'clinic' => 'Клиника',
            'patient' => 'Пациент',
            'nurse' => 'Сиделка'
        );
        if ($k == 'doctor') {
            return $doctorType['doctor'];
        }
        if ($k == 'clinic') {
            return $doctorType['clinic'];
        }
        if ($k == 'patient') {
            return $doctorType['patient'];
        }
        if ($k == 'nurse') {
            return $doctorType['nurse'];
        }

    }

    public static function getName($k)
    {
        $model = User::model()->find('id = :id', array(':id' => $k));
        if (isset($model))
        {
            return $model->name;
        }
    }

    public function updatePassword()
    {
        $password = $this->getPassword();
        $this->password = md5($password);
        $this->update();
        return $password;
    }

    public static function getPassword($length = 8, $number = true, $lettersLow = true, $lettersUp = true)
    {
        $res = '';
        $array = array();

        if ($lettersLow) $array[] = 'qwertyuiopasdfghjklzxcvbnm';
        if ($lettersUp) $array[] = 'QWERTYUIOPASDFGHJKLZXCVBNM';
        if ($number) $array[] = '0123456789';

        $size = count($array) - 1;
        for ($i = 0; $i < $length; $i++) {
            $str = rand(0, $size);
            $val = rand(0, iconv_strlen($array[$str]) - 1);
            $res .= $array[$str][$val];
        }
        return $res;
    }

    public function resetCode()
    {
        $this->restoreCode = '';
        $this->update();
    }

    public function login()
    {
        $this->phone = trim($this->phone);
        $this->password = trim($this->password);
        $model = User::model()->find(
            'phone = :phone and password = :pass',
            array(':phone' => $this->phone, ':pass' => md5($this->password))
        );
        if (count($model) > 0) return $model;
        else return false;
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function sendConfirm()
    {
        $this->isConfirm = User::getPassword();
        $confirm = md5($this->isConfirm);
        $subject = 'Подтверждение регистрации';
        $body = 'Для завершения регистрации перейдите по ссылке';

    }

    protected function beforeSave()
    {
        if (parent::beforeSave()) {
            if ($this->isNewRecord) {
                $this->password = md5($this->password);
                $this->money = '0';
                $this->status = 'moderate';
            }
            return true;
        }
        return false;
    }

    public function getPrice()
    {
        $min = 0;
        $max = 0;
        for ($i = 1; $i < count($this->schedule); $i++) {
            if ($i == 1) {
                $min = $this->schedule[$i]->price;
                $max = $this->schedule[$i]->price;
            } else {
                if ($max < $this->schedule[$i]->price) $max = $this->schedule[$i]->price;
                if ($min > $this->schedule[$i]->price) $min = $this->schedule[$i]->price;
            }
        }
        return array($min, $max);
    }

    public static function isAdmin()
    {
        $user = User::model()->findByPk(Yii::app()->user->id);
        if (isset($user)) {
            if ($user->role == 'admin') return true;
            else return false;
        } else return false;
    }


    public static function getCountNewUser($date)
    {
        return self::model()->count('dateRegistration = :date', array(':date' => $date));
    }

    public static function getCountUser($type)
    {
        $criteria = new CDbCriteria;
        if ($type) {
            $criteria->condition = 'type = :type';
            $criteria->params = array(':type' => $type);
        }
        return self::model()->count($criteria);
    }
}