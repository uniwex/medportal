<?php

/**
 * This is the model class for table "doctor".
 *
 * The followings are the available columns in table 'doctor':
 * @property integer $idUser
 * @property integer $idSpecialization
 * @property integer $idClinic
 * @property string $name
 * @property string $expirience
 * @property integer $expirienceTime
 * @property string $license
 * @property string $description
 * @property string $merit
 * @property string $specialization
 * @property string $education
 * @property integer $timeSize
 * @property integer $price
 * @property integer $rate
 * @property integer $isPhototherapy
 * @property integer $enabled
 * @property integer $typeHuman
 */
class Doctor extends CActiveRecord
{
    public $image;
    const IMAGE_PATH = '/upload/';
    const DEFAULT_IMAGE = '/assets/images/profiles-overlay.png';

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Doctor the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'doctor';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name', 'required', 'message' => 'Выберите {attribute} из списка', 'on' => 'visit'),
            array('name', 'length', 'min'=>0, 'max'=>255, 'tooShort' => 'Выберите {attribute} из списка', 'tooLong' => 'Выберите {attribute} из списка', 'on' => 'visit'),

            array('expirience, education, specialization, typeHuman', 'required', 'message' => 'Поле {attribute} не может быть пустым'),
            array('image', 'required', 'message' => 'Поле {attribute} не может быть пустым', 'on' => 'insert'),

            array('idSpecialization, expirienceTime,idClinic, price, rate, enabled', 'numerical', 'integerOnly' => true, 'message' => 'Поле {attribute} должно быть числовым'),
            array('license', 'length', 'max' => 255, 'min' => 4, 'tooShort' => Yii::t("translation", "Длина поля {attribute} должна быть от 4 до 255 символов"), 'tooLong' => Yii::t
            ("translation", "Длина поля {attribute} должна быть от 4 до 255 символов")),
            array('description, merit, specialization, education', 'length', 'max' => 65535, 'tooLong' => Yii::t("translation", "Длина поля {attribute} должна быть до 65535
			символов")),

            array('image', 'file', 'types' => 'jpg, gif, png, ico', 'maxSize' => 5242880, 'wrongType' => '{attribute} "{file}" не загружено. Неверный тип изображения. Поддерживаются только следующие типы файлов: {extensions}', 'tooLarge' => '{attribute} превышает максимально допустимый размер', 'on' => 'insert'),
            array('image', 'file', 'types' => 'jpg, gif, png, ico', 'maxSize' => 5242880, 'wrongType' => '{attribute} "{file}" не загружено. Неверный тип изображения. Поддерживаются только следующие типы файлов: {extensions}', 'tooLarge' => '{attribute} превышает максимально допустимый размер', 'allowEmpty' => true, 'on' => 'update'),
            array('pic, timeSize', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('idUser, idSpecialization, typeHuman, isPhototherapy, name, expirience, description, merit, specialization, education, price, enabled, rate', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'idUser'),
            'spec' => array(self::HAS_ONE, 'Specialization', 'id'),
            'getSpecialization' => array(self::BELONGS_TO, 'Specialization', 'idSpecialization'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'idUser' => 'Пользователь',
            'idSpecialization' => 'Специальность',
            'idClinic' => 'Клиника, в которой работает врач',
            'name' => 'ФИО врача',
            'expirience' => 'Опыт работы',
            'expirienceTime' => 'Опыт работы (лет)',
            'license' => 'Лицензия',
            'description' => 'Описание',
            'merit' => 'Заслуги',
            'specialization' => 'Специализация',
            'education' => 'Образование',
            'timeSize' => 'Время приёма',
            'price' => 'Цена',
            'typeHuman' => 'Возрастной приём',
            'isPhototherapy' => 'Практикует ли фотодинамческую терапию',
            'rate' => 'Рейтинг',
            'enabled' => 'Активность врача',
            'pic' => 'Фотография'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idUser', $this->idUser);
        $criteria->compare('idSpecialization', $this->idSpecialization);
        $criteria->compare('idClinic',$this->idClinic);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('expirience', $this->expirience, true);
        $criteria->compare('expirienceTime',$this->expirienceTime);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('merit', $this->merit, true);
        $criteria->compare('specialization', $this->specialization, true);
        $criteria->compare('education', $this->education, true);
        $criteria->compare('price', $this->timeSize);
        $criteria->compare('price', $this->price);
        $criteria->compare('rate', $this->rate);
        $criteria->compare('isPhototherapy', $this->isPhototherapy);
        $criteria->compare('typeHuman', $this->typeHuman);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getImage($img)
    {
        if ($img) {
            return self::IMAGE_PATH . $img;
        } else {
            return self::DEFAULT_IMAGE;
        }
    }

    protected function beforeSave()
    {
        if (parent::beforeSave()) {
            if ($this->isNewRecord) {
                $this->rate = '5';
            }
            return true;
        }
        return false;
    }

    public static function isDoctor()
    {
        $model = Doctor::model()->findByPk(Yii::app()->user->id);
        if ($model) return true;
        else return false;
    }


}