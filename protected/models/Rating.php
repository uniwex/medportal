<?php

/**
* This is the model class for table "rating".
*
* The followings are the available columns in table 'rating':
    * @property integer $id_user
    * @property integer $id_elem
    * @property string $type_elem
    * @property integer $rating
    * @property integer $id_rating
*/
class Rating extends CActiveRecord
{
/**
* @return string the associated database table name
*/
public function tableName()
{
return 'rating';
}

/**
* @return array validation rules for model attributes.
*/
public function rules()
{
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
return array(
    array('id_user, id_elem, type_elem, rating', 'required'),
    array('id_user, id_elem, rating', 'numerical', 'integerOnly'=>true),
    array('type_elem', 'length', 'max'=>255),
// The following rule is used by search().
// @todo Please remove those attributes that should not be searched.
array('id_user, id_elem, type_elem, rating, id_rating', 'safe', 'on'=>'search'),
);
}

/**
* @return array relational rules.
*/
public function relations()
{
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
return array(
);
}

/**
* @return array customized attribute labels (name=>label)
*/
public function attributeLabels()
{
return array(
    'id_user' => 'Ид пользователя',
    'id_elem' => 'Ид элемента оценки',
    'type_elem' => 'Тип элемента оценки',
    'rating' => 'Оценка',
    'id_rating' => 'Id Rating',
);
}

/**
* Retrieves a list of models based on the current search/filter conditions.
*
* Typical usecase:
* - Initialize the model fields with values from filter form.
* - Execute this method to get CActiveDataProvider instance which will filter
* models according to data in model fields.
* - Pass data provider to CGridView, CListView or any similar widget.
*
* @return CActiveDataProvider the data provider that can return the models
* based on the search/filter conditions.
*/
public function search()
{
// @todo Please modify the following code to remove attributes that should not be searched.

$criteria=new CDbCriteria;

		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('id_elem',$this->id_elem);
		$criteria->compare('type_elem',$this->type_elem,true);
		$criteria->compare('rating',$this->rating);
		$criteria->compare('id_rating',$this->id_rating);

return new CActiveDataProvider($this, array(
'criteria'=>$criteria,
));
}

/**
* Returns the static model of the specified AR class.
* Please note that you should have this exact method in all your CActiveRecord descendants!
* @param string $className active record class name.
* @return Rating the static model class
*/
public static function model($className=__CLASS__)
{
return parent::model($className);
}
}
