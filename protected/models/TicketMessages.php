<?php

/**
 * This is the model class for table "ticketMessages".
 *
 * The followings are the available columns in table 'ticketMessages':
 * @property integer $id
 * @property integer $idTicket
 * @property integer $idUser
 * @property string $message
 * @property string $messageDate
 */
class TicketMessages extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'ticketMessages';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('idTicket, idUser, message, messageDate', 'required', 'message' => 'Поле {attribute} не может быть пустым'),
            array('idTicket, idUser', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, idTicket, idUser, message, messageDate', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'ticket' => array(self::BELONGS_TO, 'Ticket', 'idTicket'),
            'user' => array(self::BELONGS_TO, 'User', 'idUser'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'Код комментария',
            'idTicket' => 'Код тикета',
            'idUser' => 'Код пользователя отправителя',
            'message' => 'Текст',
            'messageDate' => 'Дата',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('idTicket', $this->idTicket);
        $criteria->compare('idUser', $this->idUser);
        $criteria->compare('message', $this->message, true);
        $criteria->compare('messageDate', $this->messageDate, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TicketMessages the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeSave() {
        if($this->isNewRecord) {
            $user = User::model()->findByPk($this->idUser);
            $ticket = Ticket::model()->findByPk($this->idTicket);
            if($ticket->status != 3) {
                if($user->role == 'admin') Ticket::model()->updateByPk($this->idTicket, array('status' => 1));
                else Ticket::model()->updateByPk($this->idTicket, array('status' => 0));
            }
        }
        return true;
    }
}
