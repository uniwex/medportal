<?php

/**
 * Created by PhpStorm.
 * User: админ
 * Date: 07.12.2015
 * Time: 15:08
 */
class Search extends CFormModel
{
    const ALL = 0;
    const DOCTOR = 1;
    const CLINIC = 2;
    const NURSE = 3;
    const ARTICLE = 4;

    const IMAGE_PATH = "/upload/";

    public $search_text = '';
    public $filter = self::ALL;
    public $result = array();
    public $count = 0;

    public function getResult()
    {

        if ($this->filter == 0 || $this->filter == self::DOCTOR) {
            $this->getDoctor();
        }
        if ($this->filter == 0 || $this->filter == self::CLINIC) {
            $this->getClinic();
        }
        if ($this->filter == 0 || $this->filter == self::NURSE) {
            $this->getNurse();
        }
        if ($this->filter == 0 || $this->filter == self::ARTICLE) {
            $this->getArticle();
        }
        return $this->result;
    }

    private function getDoctor()
    {
        $specialization = Specialization::model()->find("name like :text",array(':text'=>'%' . $this->search_text . '%'));
        if (isset($specialization)) {
            $result = Doctor::model()->findAll("name like :text or specialization like :text or description like :text or idSpecialization = :idSpec",
                array(
                    ':text' => '%' . $this->search_text . '%',
                    ':idSpec' => $specialization->id));
        } else {
            $result = Doctor::model()->findAll("name like :text or specialization like :text or description like :text",
                array(':text' => '%' . $this->search_text . '%'));
        }
        foreach ($result as $item) {
            $this->result[$this->count]['id'] = $item->idUser;
            $this->result[$this->count]['name'] = $item->name;
            $this->result[$this->count]['image'] = self::IMAGE_PATH.$item->pic;
            $this->result[$this->count]['description'] = $item->description;
            $this->result[$this->count]['rating'] = $item->rate;
            $this->result[$this->count]['type'] = 'doctor';
            $this->count++;
        }
    }

    private function getClinic()
    {
        $result = Clinic::model()->findAll("name like :text or specialization like :text or description like :text", array(':text' => '%' . $this->search_text . '%'));
        foreach ($result as $item) {
            $this->result[$this->count]['id'] = $item->idUser;
            $this->result[$this->count]['name'] = $item->name;
            $this->result[$this->count]['image'] = self::IMAGE_PATH.$item->pic;
            $this->result[$this->count]['description'] = $item->description;
            $this->result[$this->count]['rating'] = $item->rate;
            $this->result[$this->count]['type'] = 'clinic';
            $this->count++;
        }
    }

    private function getNurse() {
        $result = Nurse::model()->findAll("name like :text or specialization like :text or description like :text", array(':text' => '%' . $this->search_text . '%'));
        foreach ($result as $item) {
            $this->result[$this->count]['id'] = $item->idUser;
            $this->result[$this->count]['name'] = $item->name;
            $this->result[$this->count]['image'] = self::IMAGE_PATH.$item->pic;
            $this->result[$this->count]['description'] = $item->description;
            $this->result[$this->count]['rating'] = $item->rate;
            $this->result[$this->count]['type'] = 'nurse';
            $this->count++;
        }
    }

    private function getArticle()
    {
        $result = Article::model()->findAll("name like :text or text like :text", array(':text' => '%' . $this->search_text . '%'));
        foreach ($result as $item) {
            $this->result[$this->count]['id'] = $item->id;
            $this->result[$this->count]['name'] = $item->name;
            $this->result[$this->count]['image'] = $item->getImage();
            $this->result[$this->count]['description'] = $item->text;
            $this->result[$this->count]['rating'] = $item->avg_rating;
            $this->result[$this->count]['type'] = 'article';
            $this->count++;
        }
    }
}