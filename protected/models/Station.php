<?php

/**
 * This is the model class for table "station".
 *
 * The followings are the available columns in table 'station':
 * @property integer $id
 * @property integer $idUser
 * @property string $name
 * @property string $city
 */
class Station extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Station the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'station';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name', 'required', 'message' => 'Выберите станцию'),
            array('id, idUser', 'numerical', 'integerOnly' => true),
            array('city', 'length', 'min' => 1, 'max' => 255),
            array('name', 'length', 'min' => 0, 'max' => 255, 'tooShort' => 'Выберите станцию', 'tooLong' => 'Выберите станцию'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, idUser, name, city', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::HAS_ONE, 'User', 'idUser')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'Код станции',
            'idUser' => 'Код пользователя',
            'name' => 'Станция',
            'city' => 'Город',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('idUser', $this->idUser);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('city', $this->city, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /*
    protected function beforeSave()
    {
         if(parent::beforeSave())
         {
            if($this->isNewRecord)
            {
                $this->city = 'test city';
            }

            return true;
         }

        return false;
    }
    */

    protected function beforeValidate() {
        if(is_array($this->name)) {
            $this->name = implode(',', $this->name);
        }
        return parent::beforeValidate();
    }

}