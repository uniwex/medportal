<?php

/**
 * This is the model class for table "clinic".
 *
 * The followings are the available columns in table 'clinic':
 * @property integer $idUser
 * @property string $name
 * @property string $contact
 * @property string $site
 * @property string $license
 * @property string $description
 * @property string $specialization
 * @property string $merit
 * @property string $contactAdvanced
 * @property integer $rate
 * @property string $virtual
 * @property string $panorama
 */
class Clinic extends CActiveRecord
{
    public $image;
    const IMAGE_PATH = '/upload/';
    const DEFAULT_IMAGE = '/assets/images/profiles-overlay.png';

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Clinic the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'clinic';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name', 'required', 'message' => 'Поле {attribute} не может быть пустым'),
            array('image', 'required', 'message' => 'Поле {attribute} не может быть пустым', 'except' => 'update'),
            array('idUser, rate', 'numerical', 'integerOnly' => true, 'message' => 'Поле {attribute} должно быть числовым'),
            array('name, contact, site, license, virtual, panorama', 'length', 'max' => 255, 'tooLong' => Yii::t("translation", "Длина поля {attribute} должна быть до 255 символов")),
            array('description, merit, contactAdvanced, specialization', 'length', 'max' => 65535, 'tooLong' => Yii::t("translation", "Длина поля {attribute} должна быть до 65535 символов")),
            array(
                'image',
                'file',
                'types' => 'jpg, gif, png',
                'wrongType' => '{attribute} "{file}" не загружено. Неверный тип изображения. Поддерживаются только следующие типы файлов: {extensions}',
                'tooLarge' => '{attribute} превышает максимально допустимый размер',
                'maxSize' => 5242880,
                'except' => 'update'
            ),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('idUser, name, contact, site, license, description, merit, specialization, contactAdvanced, rate', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::HAS_ONE, 'User', 'id'),
            'hours' => array(self::HAS_MANY, 'VisitingHours', 'idClinic'),
            'info' => array(self::HAS_MANY, 'AddInfo', 'idClinic'),
            'media' => array(self::HAS_MANY, 'ClinicMedia', 'idClinic'),
            'doctors' => array(self::HAS_MANY, 'Doctor', 'idClinic'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'idUser' => 'ИД пользователя',
            'name' => 'Название',
            'contact' => 'Телефон',
            'site' => 'Сайт',
            'license' => 'Лицензия',
            'description' => 'Описание',
            'specialization' => 'Специализация',
            'merit' => 'Заслуги',
            'contactAdvanced' => 'Доп. контакты',
            'rate' => 'Rate',
            'image' => 'Фото',
            'virtual' => 'Ссылка на вирт. тур',
            'panorama' => 'Ключ youtube-видео',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idUser', $this->idUser);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('contact', $this->contact, true);
        $criteria->compare('site', $this->site, true);
        $criteria->compare('license', $this->license, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('merit', $this->merit, true);
        $criteria->compare('specialization', $this->specialization, true);
        $criteria->compare('contactAdvanced', $this->contactAdvanced, true);
        $criteria->compare('rate', $this->rate);
        $criteria->compare('virtual',$this->virtual,true);
        $criteria->compare('panorama',$this->panorama,true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getImage($img)
    {
        if ($img) {
            return self::IMAGE_PATH . $img;
        } else {
            return self::DEFAULT_IMAGE;
        }
    }

    protected function beforeSave()
    {
        if (parent::beforeSave()) {
            if ($this->isNewRecord) $this->contact = 1;

            return true;
        }

        return false;
    }

    public static function isClinic()
    {
        $model = Clinic::model()->findByPk(Yii::app()->user->id);
        if ($model) return true;
        else return false;
    }

    public function formatText($attribute) {
        $text = explode("\r\n", $this->$attribute);
        $result = '';
        for($i = 0; $i < count($text); $i++) {
            $result .= '<p>- '.$text[$i].'</p>';
        }
        return $result;
    }
}