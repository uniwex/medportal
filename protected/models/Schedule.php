<?php

/**
 * This is the model class for table "schedule".
 *
 * The followings are the available columns in table 'schedule':
 * @property integer $id
 * @property integer $idUser
 * @property string $type
 * @property string $name
 * @property string $specialization
 * @property string $monday
 * @property string $tuesday
 * @property string $wednesday
 * @property string $thursday
 * @property string $friday
 * @property string $saturday
 * @property string $sunday
 * @property integer $price
 */
class Schedule extends CActiveRecord
{
    public $timework, $timeworkClinic,$idSpecialization,$expirience,$license,$education,$rate,$typeHuman,$isPhototherapy;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Schedule the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'schedule';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            //array('monday, tuesday, wednesday, thursday, friday, saturday, sunday', 'required', 'message'=>'Для поля {attribute} не задано расписание'),
            array('name', 'required', 'message' => 'Поле {attribute} является обязательным полем'),
            array('id, idUser, price', 'numerical', 'integerOnly' => true),
            array('name, type, specialization, monday, tuesday, wednesday, thursday, friday, saturday, sunday', 'length', 'max' => 255),
            array('timework, timeworkClinic', 'length', 'allowEmpty' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, idUser, name, type, specialization, price, timework, timeworkClinic', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'idUser'),
            'spec' => array(self::BELONGS_TO, 'Specialization', 'specialization'),
            'visitors' => array(self::HAS_MANY, 'Visitors', 'idSchedule'),
            'getClinic' => array(self::BELONGS_TO, 'Clinic', 'idUser')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'idUser' => 'Id User',
            'type' => 'Тип пользователя',
            'name' => 'Имя',
            'specialization' => 'Специализация',
            'monday' => 'Понедельник',
            'tuesday' => 'Вторник',
            'wednesday' => 'Среда',
            'thursday' => 'Четверг',
            'friday' => 'Пятница',
            'saturday' => 'Суббота',
            'sunday' => 'Воскресенье',
            'price' => 'Цена',
            'timeSize' => 'Время приёма'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('idUser', $this->idUser);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('type', $this->type);
        $criteria->compare('specialization', $this->specialization, true);
        $criteria->compare('monday', $this->monday, true);
        $criteria->compare('tuesday', $this->tuesday, true);
        $criteria->compare('wednesday', $this->wednesday, true);
        $criteria->compare('thursday', $this->thursday, true);
        $criteria->compare('friday', $this->friday, true);
        $criteria->compare('saturday', $this->saturday, true);
        $criteria->compare('sunday', $this->sunday, true);
        $criteria->compare('price', $this->price);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getSchedule()
    {
        $dayWeek = array(
            'monday',
            'tuesday',
            'wednesday',
            'thursday',
            'friday',
            'saturday',
            'sunday'
        );
        $schedule = array();
        $record = 0;
        for ($i = 0; $i < count($dayWeek); $i++) {
            $temp[$i] = explode(',', $this->$dayWeek[$i]);
            if (count($temp[$i]) > $record) $record = count($temp[$i]);
        }

        for ($i = 0; $i < $record; $i++) {
            $schedule[] = array();
        }

        for ($i = 0; $i < count($temp); $i++) {
            if (count($temp[$i]) < $record) array_push($temp[$i], '');
            for ($j = 0; $j < count($temp[$i]); $j++) {
                if ($temp[$i][$j] == 'Выходной') $temp[$i][$j] = '';
            }
        }

        for ($i = 0; $i < $record; $i++) {
            for ($j = 0; $j < count($temp); $j++) {
                for ($k = 0; $k < count($temp[$j]); $k++) {
                    if ($temp[$j][$k] == '') {
                        $schedule[$k][$j] = array('', '');
                    } else {
                        $timeRange = explode('-', $temp[$j][$k]);
                        $schedule[$k][$j] = array($timeRange[0], $timeRange[1]);
                    }
                }
            }
        }
        return json_encode($schedule);
    }

    public static function getScheduleClinic($id) {
        $model = Schedule::model()->find(
            'idUser = :id and type = :type',
            array(
                ':id' => $id,
                ':type' => 'clinic'
            )
        );
        if(!$model) return 0;
        $minHour = 24;
        $maxHour = 0;
        $dayWeek = array(
            'monday',
            'tuesday',
            'wednesday',
            'thursday',
            'friday',
        );
        $weekend = array(
            'saturday' => 'Суббота',
            'sunday' => 'Воскресенье'
        );
        $result = '<table class="table"><tr><td>Будни</td>';
        foreach($dayWeek as $value) {
            $time = explode('-', $model->$value);
            if(isset($time[0]) && isset($time[1])) {
                if($time[0] < $minHour) $minHour = trim($time[0]);
                if($time[1] > $maxHour) $maxHour = trim($time[1]);
            }
        }

        if($minHour != 24 && $maxHour != 0) $result .= '<td>c '.$minHour.' до '.$maxHour.'</td></tr>';
        else $result .= '<td>Выходной</td></tr>';

        foreach($weekend as $key => $value) {
            // 0 - min, 1 - max
            $result .= '<tr><td>'.$weekend[$key].'</td>';
            $time = explode('-', $model->$key);
            if(isset($time[0]) && isset($time[1])) {
                $minHour = trim($time[0]);
                $maxHour = trim($time[1]);
                $result .= '<td>c '.$minHour.' до '.$maxHour.'</td></tr>';
            }
            else $result .= '<td>Выходной</td></tr>';
        }
        $result .= '</table>';

        return $result;
    }

    public function getIdClinic()
    {
        $model = Schedule::model()->find('type = :type and idUser = :id', array(':type' => 'clinic', ':id' => $this->idUser));
        return (int)$model->id;
    }

    // Убирает лишние слова "Выходной"
    public function clearSchedule($day)
    {
        $day = explode(',', $day);
        if (count($day) > 1) {
            for ($i = 0; $i < count($day); $i++) {
                if ($day[$i] == 'Выходной') unset($day[$i]);
            }
            $result = implode(',', $day);
            return $result;
        } else return $day[0];
    }

    public function deleteSchedule()
    {
        $this->monday = '';
        $this->tuesday = '';
        $this->wednesday = '';
        $this->thursday = '';
        $this->friday = '';
        $this->saturday = '';
        $this->sunday = '';
    }

    /**
     * @param string $schedule - Расписание вида [[["",""],["",""],["",""],["",""],["",""],["",""],["",""]]]
     */
    public function setSchedule($schedule)
    {
        $dayWeek = array(
            'monday',
            'tuesday',
            'wednesday',
            'thursday',
            'friday',
            'saturday',
            'sunday'
        );
        $schedule = json_decode($schedule);

        for ($j = 0; $j < count($schedule); $j++) {
            for ($k = 0; $k < count($dayWeek); $k++) {
                $time = implode('-', $schedule[$j][$k]);

                if ($time != '-') {
                    if (!empty($this->$dayWeek[$k])) $this->$dayWeek[$k] .= ',' . $time;
                    else $this->$dayWeek[$k] .= $time;
                } else {
                    if (empty($this->$dayWeek[$k])) $this->$dayWeek[$k] = 'Выходной';
                }
            }
        }
    }
}