<?php

/**
* This is the model class for table "social".
*
* The followings are the available columns in table 'social':
    * @property integer $id
    * @property integer $idUser
    * @property string $vkontakte
    * @property string $odnoklassniki
    * @property string $facebook
    * @property string $twitter
    * @property string $instagram
*/
class Social extends CActiveRecord
{
/**
* @return string the associated database table name
*/
public function tableName()
{
return 'social';
}

/**
* @return array validation rules for model attributes.
*/
public function rules()
{
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
return array(
    array('idUser', 'required'),
    array('idUser', 'numerical', 'integerOnly'=>true),
    array('vkontakte, odnoklassniki, facebook, twitter, instagram', 'length', 'max'=>255),
// The following rule is used by search().
// @todo Please remove those attributes that should not be searched.
array('id, idUser, vkontakte, odnoklassniki, facebook, twitter, instagram', 'safe', 'on'=>'search'),
);
}

/**
* @return array relational rules.
*/
public function relations()
{
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
return array(
);
}

/**
* @return array customized attribute labels (name=>label)
*/
public function attributeLabels()
{
return array(
    'id' => 'Id социальной сети',
    'idUser' => 'id юзера в системе (связь с таблицей user по полю id)',
    'vkontakte' => 'Профиль вконтакте',
    'odnoklassniki' => 'Профиль на одноклассниках',
    'facebook' => 'Профиль на facebook',
    'twitter' => 'Профиль на twitter',
    'instagram' => 'Профиль на instagram',
);
}

/**
* Retrieves a list of models based on the current search/filter conditions.
*
* Typical usecase:
* - Initialize the model fields with values from filter form.
* - Execute this method to get CActiveDataProvider instance which will filter
* models according to data in model fields.
* - Pass data provider to CGridView, CListView or any similar widget.
*
* @return CActiveDataProvider the data provider that can return the models
* based on the search/filter conditions.
*/
public function search()
{
// @todo Please modify the following code to remove attributes that should not be searched.

$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idUser',$this->idUser);
		$criteria->compare('vkontakte',$this->vkontakte,true);
		$criteria->compare('odnoklassniki',$this->odnoklassniki,true);
		$criteria->compare('facebook',$this->facebook,true);
		$criteria->compare('twitter',$this->twitter,true);
		$criteria->compare('instagram',$this->instagram,true);

return new CActiveDataProvider($this, array(
'criteria'=>$criteria,
));
}

/**
* Returns the static model of the specified AR class.
* Please note that you should have this exact method in all your CActiveRecord descendants!
* @param string $className active record class name.
* @return Social the static model class
*/
public static function model($className=__CLASS__)
{
return parent::model($className);
}
}
