<?php

/**
 * This is the model class for table "review".
 *
 * The followings are the available columns in table 'review':
 * @property integer $id
 * @property integer $idUserFrom
 * @property integer $idUserTo
 * @property string $name
 * @property string $text
 * @property integer $cure
 * @property integer $regard
 * @property integer $qualification
 * @property string $time
 * @property string $pics
 */
class Review extends CActiveRecord
{
    public $idUser='';

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Review the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'review';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, text, cure, regard, qualification', 'required', 'message' => 'Поле {attribute} не может быть пустым'),
            array('idUserFrom, idUserTo, cure, regard, qualification', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, idUserFrom, idUserTo, text, cure, moderate, regard, qualification, time', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'Id комментария',
            'idUserFrom' => 'Автор комментария',
            'idUserTo' => 'Получатель коментария',
            'name' => 'Ваше имя',
            'text' => 'Текст отзыва',
            'cure' => 'Лечение',
            'regard' => 'Отношение',
            'qualification' => 'Квалификация',
            'time' => 'Время',
            'pics' => 'Фотографии',
            'moderate' => 'Модерация'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('idUserFrom', $this->idUserFrom);
        $criteria->compare('idUserTo', $this->idUserTo);
        $criteria->compare('text', $this->text, true);
        $criteria->compare('cure', $this->cure);
        $criteria->compare('regard', $this->regard);
        $criteria->compare('qualification', $this->qualification);
        $criteria->compare('time', $this->time, true);
        $criteria->compare('pics', $this->pics, true);
        $criteria->compare('moderate', $this->moderate, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /*
    protected function beforeSave()
    {
        if (parent::beforeSave()) {
            if ($this->isNewRecord) {
                $this->idUserFrom = 0;
                $this->idUserTo = 0;
            }
            return true;
        }
        return false;
    }
    */

}