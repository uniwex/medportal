<?php

/**
* This is the model class for table "scienceWorks".
*
* The followings are the available columns in table 'scienceWorks':
    * @property integer $id
    * @property string $name
    * @property string $description
    * @property integer $idAuthor
    * @property string $link
    * @property string $picture
*/
class ScienceWorks extends CActiveRecord
{
/**
* @return string the associated database table name
*/

	public $image;
	public $pdf;
	public function tableName()
	{
		return 'scienceWorks';
	}

	/**
	* @return array validation rules for model attributes.
	*/
	public function rules()
	{
	// NOTE: you should only define rules for those attributes that
	// will receive user inputs.
		return array(
			array('name, description, idAuthor', 'required'),
			array('link, picture', 'required', 'on'=>'create'),
			array('idAuthor', 'numerical', 'integerOnly'=>true),
			array(
				'picture',
				'file',
				'types' => 'jpg, gif, png',
				'wrongType' => '{attribute} "{file}" не загружено. Неверный тип изображения. Поддерживаются только следующие типы файлов: {extensions}',
				'tooLarge' => '{attribute} превышает максимально допустимый размер',
				'maxSize' => 15242880,
				'except'=>array('update','create-user'),
			),
			array(
				'link',
				'file',
				'types' => 'pdf',
				'wrongType' => '{attribute} "{file}" не загружено. Неверный тип изображения. Поддерживаются только следующие типы файлов: {extensions}',
				'tooLarge' => '{attribute} превышает максимально допустимый размер',
				'except'=>array('update'),
			),
		// The following rule is used by search().
		// @todo Please remove those attributes that should not be searched.
		array('id, name, description, idAuthor, link, picture', 'safe', 'on'=>'search'),
		array('picture','safe', 'on' => 'create-user'),
		);
	}

	/**
	* @return array relational rules.
	*/
	public function relations()
	{
	// NOTE: you may need to adjust the relation name and the related
	// class name for the relations automatically generated below.
		return array(
			'user' => array(self::HAS_ONE, 'User', 'id'),
		);
	}

	/**
	* @return array customized attribute labels (name=>label)
	*/
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'picture' => 'Изображение',
			'name' => 'Название работы',
			'description' => 'Описание',
			'idAuthor' => 'id автора',
			'link' => 'pdf файл',

		);
	}

	/**
	* Retrieves a list of models based on the current search/filter conditions.
	*
	* Typical usecase:
	* - Initialize the model fields with values from filter form.
	* - Execute this method to get CActiveDataProvider instance which will filter
	* models according to data in model fields.
	* - Pass data provider to CGridView, CListView or any similar widget.
	*
	* @return CActiveDataProvider the data provider that can return the models
	* based on the search/filter conditions.
	*/
	public function search()
	{
	// @todo Please modify the following code to remove attributes that should not be searched.

	$criteria=new CDbCriteria;

			$criteria->compare('id',$this->id);
			$criteria->compare('name',$this->name,true);
			$criteria->compare('description',$this->description,true);
			$criteria->compare('idAuthor',$this->idAuthor);
			$criteria->compare('link',$this->link,true);
			$criteria->compare('picture',$this->picture,true);

	return new CActiveDataProvider($this, array(
	'criteria'=>$criteria,
	));
	}



	/**
	* Returns the static model of the specified AR class.
	* Please note that you should have this exact method in all your CActiveRecord descendants!
	* @param string $className active record class name.
	* @return ScienceWorks the static model class
	*/
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeDelete() {
		if (file_exists(Yii::getPathOfAlias('webroot') . '/upload/'.$this->link)) {
			unlink(Yii::getPathOfAlias('webroot') . '/upload/'.$this->link);
		}
		if (file_exists(Yii::getPathOfAlias('webroot') . '/upload/'.$this->picture)) {
			unlink(Yii::getPathOfAlias('webroot') . '/upload/'.$this->picture);
		}
		return parent::beforeDelete();
	}
}
