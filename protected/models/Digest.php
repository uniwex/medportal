<?php

/**
 * This is the model class for table "digest".
 *
 * The followings are the available columns in table 'digest':
 * @property integer $id
 * @property integer $idCategory
 * @property string $name
 * @property string $text
 * @property string $time
 * @property string $status
 * @property string $image
 * @property double $avg_rating
 */
class Digest extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Digest the static model class
     */
    const IMAGE_PATH = '/upload/digets/';
    const DEFAULT_IMAGE = '/assets/images/articles-overlay.png';

    public $minRatingFilter = 0;
    public $maxRatingFilter = 5;
    public $id_category = '';
    public $dateStart = '';
    public $dateEnd = '';


    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'digest';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('name, text', 'required', 'message' => 'Поле {attribute} не может быть пустым'),
            array('name, time, image, status', 'length', 'max' => 255),
            array('avg_rating', 'numerical'),

            array('id, idCategory, name, text, time, status, avg_rating, maxRatingFilter, minRatingFilter, id_category,
             dateStart, dateEnd, onlyMe', 'safe',  'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'idCategory' => 'Id Category',
            'name' => 'Имя',
            'text' => 'Текст',
            'time' => 'Дате',
            'status' => 'Статус',
            'image' => 'Изображение',
            'avg_rating' => 'Среднему рейтингу',
            'maxRatingFilter' => 'Максимальный рейтинг',
            'minRatingFilter' => 'Минимальный рейтинг',
            'id_category' => 'Категория'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('idCategory', $this->idCategory);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('text', $this->text, true);
        $criteria->compare('time', $this->time, true);
        $criteria->compare('status', $this->status, true);

        if($this->id_category != '')
            $criteria->addCondition("idCategory LIKE '%". $this->id_category . "%'");
        $params = array(
            'min_rating' => $this->minRatingFilter,
            'max_rating' => $this->maxRatingFilter,
        );
        if ($this->dateStart != "") {
            $criteria->addCondition('time >= :dateStart');
            $params['dateStart'] = $this->dateStart;
        }
        if ($this->dateEnd != "") {
            $criteria->addCondition('time <= :dateEnd');
            $params['dateEnd']= $this->dateEnd;
        }
        $criteria->addCondition('avg_rating >= :min_rating');
        $criteria->addCondition('avg_rating <= :max_rating');
        $criteria->params = $params;
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getImage()
    {
        if (($this->image)&&(file_exists($this->image))) {
            return self::IMAGE_PATH . $this->image;
        } else {
            return self::DEFAULT_IMAGE;
        }
    }

    protected function beforeSave()
    {
        if (parent::beforeSave()) {
            if ($this->isNewRecord) {
                $this->status = 'public';
            }
            return true;
        }
        return false;
    }

}
