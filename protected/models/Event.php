<?php

/**
 * This is the model class for table "event".
 *
 * The followings are the available columns in table 'event':
 * @property integer $id
 * @property integer $idOwner
 * @property integer $idExecuter
 * @property string $type
 * @property string $eventDate
 * @property string $eventTime
 * @property string $eventDateFinish
 * @property string $description
 */
class Event extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'event';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('description', 'required', 'on' => 'note', 'message' => 'Поле {attribute} не может быть пустым'),
            array('eventDate, eventDateFinish', 'required', 'on' => 'nurse'),
            array(
                'eventDate, eventDateFinish',
                'match', 'pattern' => '/^\d\d\d\d-\d\d-\d\d$/',
                'message' => 'Введите корректную дату.',
                'on' => 'nurse'
            ),
            array('idOwner, type, eventDate', 'required', 'message' => 'Поле {attribute} не может быть пустым'),
            array('idOwner', 'numerical', 'integerOnly' => true),
            array('type', 'length', 'max' => 8),
            array('eventTime', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, idOwner, idExecuter, type, eventDate, eventTime, eventDateFinish, description', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'idOwner' => 'Id Owner',
            'idExecuter' => 'Id Executer',
            'type' => 'Type',
            'eventDate' => 'Дата',
            'eventTime' => 'Время',
            'eventDateFinish' => 'Дата окончания',
            'description' => 'Описание',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('idOwner', $this->idOwner);
        $criteria->compare('idExecuter', $this->idExecuter);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('eventDate', $this->eventDate, true);
        $criteria->compare('eventTime', $this->eventTime, true);
        $criteria->compare('eventDateFinish', $this->eventDateFinish, true);
        $criteria->compare('description', $this->description, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Event the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function getCountOrder($date) {
        $criteria = new CDbCriteria;
        $criteria->condition = 'type <> :type and eventDate = :date';
        $criteria->params = array(':type' => 'note', ':date' => $date);
        return self::model()->count($criteria);
    }
}
