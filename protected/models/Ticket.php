<?php

/**
 * This is the model class for table "ticket".
 *
 * The followings are the available columns in table 'ticket':
 * @property integer $id
 * @property integer $idUser
 * @property string $type
 * @property string $text
 * @property string $subject
 * @property integer $status
 * @property string $date
 */
class Ticket extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'ticket';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('idUser, type, text, subject, status, date', 'required', 'message' => 'Поле {attribute} не может быть пустым'),
            array('idUser, status', 'numerical', 'integerOnly' => true),
            array('type', 'length', 'max' => 12),
            array('subject', 'length', 'max' => 255, 'tooLong' => 'Длина поля {attribute} не может быть больше 255 символов'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, idUser, type, text, subject, status, date', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'idUser'),
            'message' => array(self::HAS_MANY, 'TicketMessages', 'id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'Код тикета',
            'idUser' => 'Автор',
            'type' => 'Тип тикета',
            'text' => 'Текст тикета',
            'subject' => 'Тема тикета',
            'status' => 'Статус тикета',
            'date' => 'Дата создания',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('idUser', $this->idUser);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('text', $this->text, true);
        $criteria->compare('subject', $this->subject, true);
        $criteria->compare('status', $this->status);

        if ($this->date) {
            $date = explode('-', $this->date);
            $dateFrom = trim(str_replace('/', '-', $date[0])) . ' 00:00:00';
            if (count($date) == 2) $dateTo = trim(str_replace('/', '-', $date[1])) . ' 23:59:59';
            else $dateTo = trim(str_replace('/', '-', $date[0])) . ' 23:59:59';
            $criteria->addBetweenCondition('date', $dateFrom, $dateTo);
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Ticket the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function checkOpenTicket($id, $ticket) {
        $model = Ticket::model()->findByPk($ticket);
        if($model && $model->idUser == $id || User::isAdmin()) return $model;
        else return false;
    }

    public static function closeTicket($id) {
        if(self::model()->updateByPk($id, array('status' => 2))) return true;
        else return false;
    }
}
