<?php

/**
 * This is the model class for table "section".
 *
 * The followings are the available columns in table 'section':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $image
 *
 */
class Section extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */

    public function tableName()
    {
        return 'section';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
        return array(
            array('name', 'required', 'message' => 'Поле {attribute} не может быть пустым'),



    // The following rule is used by search().
    // @todo Please remove those attributes that should not be searched.
            array('name', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(//'user' => array(self::HAS_ONE, 'User', 'id'),
            'category' => array(self::HAS_MANY, 'Category', 'idSection'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'id',
            'name' => 'Название раздела',
            'description' => 'Описание раздела',
            'image' => 'Изображение',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('text', $this->text, true);
        $criteria->compare('idCategory', $this->idCategory);
        $criteria->compare('time', $this->time, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('idUser', $this->idUser);

// todo Переделать это говно
        if($this->id_category != '')
            $criteria->addCondition("idCategory LIKE '%". $this->id_category . "%'");
        $params = array(
            'min_rating' => $this->minRatingFilter,
            'max_rating' => $this->maxRatingFilter,
        );
        if ($this->onlyMe != 0) {
            $criteria->addCondition('idUser = :idUser');
            $params['idUser'] = Yii::app()->user->id;
        }
        if ($this->dateStart != "") {
            $criteria->addCondition('time >= :dateStart');
            $params['dateStart'] = $this->dateStart;
        }
        if ($this->dateEnd != "") {
            $criteria->addCondition('time <= :dateEnd');
            $params['dateEnd']= $this->dateEnd;
        }

        $criteria->addCondition('avg_rating >= :min_rating');
        $criteria->addCondition('avg_rating <= :max_rating');
        $criteria->params = $params;

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Article the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    protected function beforeSave()
    {
        if (parent::beforeSave()) {
            if ($this->isNewRecord) {
                $this->status = 'public';
            }
            return true;
        }
        return false;
    }

    public function getImage()
    {
        if (($this->image)&&(file_exists($this->image))) {
            return self::IMAGE_PATH . $this->image;
        } else {
            return self::DEFAULT_IMAGE;
        }
    }


    public static function getCountArticle($date)
    {
        $criteria = new CDbCriteria;
        $criteria->compare('time', $date, true);
        $count = self::model()->find($criteria)->count;
        return $count == null ? 0 : $count;
    }
}