<?php

/**
 * This is the model class for table "infohub".
 *
 * The followings are the available columns in table 'infohub':
 * @property integer $id
 * @property string $header
 * @property string $description
 * @property string $link
 * @property string $pic
 */
class Infohub extends CActiveRecord
{
    public $picture;
    const IMAGE_PATH = '/upload/infohub';
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'infohub';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('header, description, link, pic', 'required'),
// The following rule is used by search().
// @todo Please remove those attributes that should not be searched.
            array('id, header, description, link, pic', 'safe', 'on' => 'search'),
            array('picture', 'required','on' => 'create'),
            array(
                'picture',
                'file',
                'types' => 'jpg, gif, png',
                'wrongType' => '{attribute} "{file}" не загружено. Неверный тип изображения. Поддерживаются только следующие типы файлов: {extensions}',
                'tooLarge' => '{attribute} превышает максимально допустимый размер',
                'maxSize' => 5242880,
                'except' => 'search',
                //'on' => 'image',
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'header' => 'Заголовок',
            'description' => 'Описание',
            'link' => 'Ссылка',
            'pic' => 'Изображение',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('header', $this->header, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('link', $this->link, true);
        $criteria->compare('pic', $this->pic, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Infohub the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function getImage($img)
    {
        if ($img) {
            return self::IMAGE_PATH . $img;
        }
    }
    public function formatText($attribute) {
        $text = explode("\r\n", $this->$attribute);
        $result = '';
        for($i = 0; $i < count($text); $i++) {
            $result .= '<p>- '.$text[$i].'</p>';
        }
        return $result;
    }
}
