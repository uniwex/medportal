<?php

/**
 * This is the model class for table "visitors".
 *
 * The followings are the available columns in table 'visitors':
 * @property integer $id
 * @property integer $idPatient
 * @property integer $idUser
 * @property integer $idSchedule
 * @property string $visitDate
 * @property string $visitTime
 */
class Visitors extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'visitors';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('idPatient, visitDate, visitTime', 'required', 'message' => 'Поле {attribute} не может быть пустым'),
            array(
                'visitTime',
                'match', 'pattern' => '/^(\d){1,2}:\d\d-(\d){1,2}:\d\d$/',
                'message' => 'Введите корректное время.',
            ),
            array(
                'visitDate',
                'match', 'pattern' => '/^\d\d\d\d-\d\d-\d\d$/',
                'message' => 'Введите корректную дату.',
            ),
            array('idPatient, idSchedule', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, idPatient, idUser, idSchedule, visitDate, visitTime', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'schedule' => array(self::BELONGS_TO, 'Schedule', 'idSchedule'), // для докторов и докторов клиник
            'user' => array(self::BELONGS_TO, 'User', 'idUser') // для сиделок и пациентов
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'Код записи',
            'idPatient' => 'Код пациента',
            'idUser' => 'Код пользователя',
            'idSchedule' => 'Код врача',
            'visitDate' => 'Дата',
            'visitTime' => 'Время',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('idPatient', $this->idPatient);
        $criteria->compare('idUser', $this->idUser);
        $criteria->compare('idSchedule', $this->idSchedule);
        $criteria->compare('visitDate', $this->date, true);
        $criteria->compare('visitTime', $this->time, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Visitors the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
