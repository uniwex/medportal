<?php

/**
 * This is the model class for table "patient".
 *
 * The followings are the available columns in table 'patient':
 * @property integer $idUser
 * @property string $birthday
 * @property string $oms
 * @property string $dms
 */
class Patient extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Patient the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'patient';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('birthday, oms, dms', 'required', 'message'=>'Поле {attribute} не может быть пустым'),
			array('idUser', 'numerical', 'integerOnly'=>true, 'message'=>'Поле {attribute} должно быть числовым'),
			array('oms, dms', 'length', 'max' => 255, 'tooLong' => Yii::t("translation", "Длина поля {attribute} должна быть до 255 символов")),
			array('birthday', 'match', 'pattern'=>'/\d{4}-\d{2}-\d{2}$/', 'message'=>'Ошибка валидации даты'),
			
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idUser, birthday, oms, dms', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idUser' => 'ИД пациента',
			'birthday' => 'Дата рождения',
			'oms' => 'ОМС',
			'dms' => 'ДМС',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idUser',$this->idUser);
		$criteria->compare('birthday',$this->birthday,true);
		$criteria->compare('oms',$this->oms,true);
		$criteria->compare('dms',$this->dms,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/*
	protected function beforeSave()
    {
         if(parent::beforeSave())
         {
            if($this->isNewRecord)
            {
            	//
            }

            return true;
         }

        return false;
    }
    */

	public static function isPatient()
	{
		$model = Patient::model()->findByPk(Yii::app()->user->id);
		if ($model) return true;
		else return false;
	}
}