<?php

/**
 * This is the model class for table "nurse".
 *
 * The followings are the available columns in table 'nurse':
 * @property integer $idUser
 * @property string $name
 * @property string $expirience
 * @property string $description
 * @property string $specialization
 * @property string $education
 * @property integer $priceDay
 * @property integer $priceMonth
 * @property integer $priceHour
 * @property integer $rate
 * @property integer $residence
 * @property integer $food
 * @property string $categorySick
 */
class Nurse extends CActiveRecord
{
    public $image;
    const IMAGE_PATH = '/upload/';
    const DEFAULT_IMAGE = '/assets/images/profiles-overlay.png';

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Nurse the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'nurse';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('food, residence, expirience, specialization, education', 'required', 'message' => 'Поле {attribute} не может быть пустым'),
            array('image', 'required', 'message' => 'Поле {attribute} не может быть пустым', 'on' => 'insert'),
            array('idUser, food, residence, priceDay, priceMonth, priceHour, rate', 'numerical', 'integerOnly' => true),
            array('name, categorySick', 'length', 'max' => 255),
            array('expirience, description, specialization, education', 'length', 'max' => 65535, 'tooLong' => Yii::t("translation", "Длина поля {attribute} должна быть до 65535
			символов")),

            array('image', 'file', 'types' => 'jpg, gif, png', 'maxSize' => 5242880, 'on' => 'insert'),
            array('image', 'file', 'types' => 'jpg, gif, png', 'maxSize' => 5242880, 'allowEmpty' => true, 'on' => 'update'),

            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('idUser, name, food, residence, expirience, description, specialization, education, priceDay, priceMonth, rate', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::HAS_ONE, 'User', 'idUser'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'idUser' => 'ИД пользователя',
            'name' => 'ФИО',
            'expirience' => 'Опыт работы',
            'description' => 'Описание',
            'specialization' => 'Специализация',
            'education' => 'Образование',
            'priceDay' => 'Стоимость дня',
            'priceMonth' => 'Стоимость месяца',
            'priceHour' => 'Стоимость часа',
            'rate' => 'Рейтинг',
            'image' => 'Фото',
            'food' => 'Питание',
            'residence' => 'Проживание',
            'categorySick' => 'Категория больных',
            'pic' => 'Фотография'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idUser', $this->idUser);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('expirience', $this->expirience, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('specialization', $this->specialization, true);
        $criteria->compare('education', $this->education, true);
        $criteria->compare('priceDay', $this->priceDay);
        $criteria->compare('priceMonth', $this->priceMonth);
        $criteria->compare('priceHour', $this->priceHour);
        $criteria->compare('rate', $this->rate);
        $criteria->compare('food', $this->food);
        $criteria->compare('residence', $this->residence);
        $criteria->compare('categorySick', $this->categorySick);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getImage($img)
    {
        if ($img) {
            return self::IMAGE_PATH . $img;
        } else {
            return self::DEFAULT_IMAGE;
        }
    }

    protected function beforeSave()
    {
        if (parent::beforeSave()) {
            if ($this->isNewRecord) {
                $this->rate = '5';
            }
            return true;
        }
        return false;
    }

    public static function isNurse()
    {
        $model = Nurse::model()->findByPk(Yii::app()->user->id);
        if ($model) return true;
        else return false;
    }
}