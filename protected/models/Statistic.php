<?php

/**
 * This is the model class for table "statistic".
 *
 * The followings are the available columns in table 'statistic':
 * @property string $time
 * @property integer $newUser
 * @property integer $user
 * @property integer $doctor
 * @property integer $clinic
 * @property integer $pacient
 * @property integer $nurse
 * @property integer $payed
 * @property integer $purchase
 * @property integer $article
 */
class Statistic extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public $idReferal;
    public function tableName()
    {
        return 'statistic';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('time, newUser, user, doctor, clinic, pacient, nurse, payed, purchase, article', 'required'),
            array('newUser, user, doctor, clinic, pacient, nurse, payed, purchase, article', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('time, newUser, user, doctor, clinic, pacient, nurse, payed, purchase, article', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'time' => 'Сегодняшняя дата',
            'newUser' => 'Кол-во новых пользователей',
            'user' => 'Общее кол-во пользователей',
            'doctor' => 'Кол-во врачей',
            'clinic' => 'Кол-во клиник',
            'pacient' => 'Кол-во пациентов',
            'nurse' => 'Кол-во сиделок',
            'payed' => 'Сумма пополнений',
            'purchase' => 'Кол-во заказов',
            'article' => 'Кол-во статей',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('newUser', $this->newUser);
        $criteria->compare('user', $this->user);
        $criteria->compare('doctor', $this->doctor);
        $criteria->compare('clinic', $this->clinic);
        $criteria->compare('pacient', $this->pacient);
        $criteria->compare('nurse', $this->nurse);
        $criteria->compare('payed', $this->payed);
        $criteria->compare('purchase', $this->purchase);
        $criteria->compare('article', $this->article);

        if ($this->time) {
            $date = explode('-', $this->time);
            $dateFrom = trim(str_replace('/', '-', $date[0]));
            if (count($date) == 2) $dateTo = trim(str_replace('/', '-', $date[1]));
            else $dateTo = trim(str_replace('/', '-', $date[0]));
            $criteria->addBetweenCondition('time', $dateFrom, $dateTo);
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Statistic the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
