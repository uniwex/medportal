<?php

/**
 * This is the model class for table "winners".
 *
 * The followings are the available columns in table 'winners':
 * @property integer $id
 * @property integer $idQuiz
 * @property string $prize
 */
class Winners extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'winners';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('id, idQuiz, prize', 'required'),
            array('id, idQuiz', 'numerical', 'integerOnly' => true),
            array('id', 'unique', 'message'=>'Такой победитель уже задан'),
// The following rule is used by search().
// @todo Please remove those attributes that should not be searched.
            array('id, idQuiz, prize', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'users' => array(self::HAS_ONE, 'User', 'id'),
            'quiz' => array(self::BELONGS_TO, 'Quiz', 'idQuiz'),
            'clinic' => array(self::HAS_ONE, 'Clinic', 'idUser'),
            'doctor' => array(self::HAS_ONE, 'Doctor', 'idUser'),
            'nurse' => array(self::HAS_ONE, 'Nurse', 'idUser'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'Победитель',
            'idQuiz' => 'Розыгрыш',
            'prize' => 'Приз',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('idQuiz', $this->idQuiz);
        $criteria->compare('prize', $this->prize, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Winners the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
