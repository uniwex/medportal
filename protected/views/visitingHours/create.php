<?php
/* @var $this VisitingHoursController */
/* @var $model VisitingHours */

$this->breadCrumbs = array(
    'Режим работы' => array('index'),
    'Создать',
);

$this->menu = array(
    array('label' => 'Режимы работы', 'url' => array('index')),
    array('label' => 'Управление режимами работы', 'url' => array('admin')),
);
?>

    <h1>Создать режим работы</h1>

<?php $this->renderPartial('_form', array('model' => $model, 'clinic' => $clinic, 'days' => $days,)); ?>