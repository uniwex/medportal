<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id'=>'visiting-hours-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    )); ?>

    <div class="row">
        <div class="col-md-6 createGood">
            <div id="box-hours" class="box box-primary">
                <div class="box-header with-border">
                </div>
                <div class="box-body">
                    <?php echo $form->errorSummary($model); ?>

                    <div id="clinicName" class="form-group">
                        <?php echo $form->labelEx($model,'idClinic'); ?><br>
                        <?php echo $form->dropDownList($model,'idClinic',$clinic ); ?>
                        <?php echo $form->error($model,'idClinic'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'day'); ?>
                        <br>
                        <?php echo $form->dropDownList($model,'day',$days ); ?>
                        <?php echo $form->error($model, 'day'); ?>
                    </div>
                    <p>Чтобы обозначить выходной, оставьте</p>
                    <p>поля "С" и "ДО" пустыми.</p>
                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'timeFrom'); ?>
                        <br>
                        <?php echo $form->numberField($model, 'timeFrom',array('max'=>23,'min'=>0)); ?>
                        <?php echo $form->error($model, 'timeFrom'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'timeTo'); ?>
                        <br>
                        <?php echo $form->numberField($model, 'timeTo',array('max'=>23,'min'=>0)); ?>
                        <?php echo $form->error($model, 'timeTo'); ?>
                    </div>

                    <div class="row buttons text-center">
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->