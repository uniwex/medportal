<?php
/* @var $this VisitingHoursController */
/* @var $model VisitingHours */

$this->breadCrumbs = array(
    'Режимы работы' => array('index'),
    'Управление',
);

$this->menu = array(
    array('label' => 'Список режимов работы', 'url' => array('index')),
    array('label' => 'Создать режим работы', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$('#visiting-hours-grid').yiiGridView('update', {
data: $(this).serialize()
});
return false;
});
");
?>

<form action="/visitingHours/create">
    <button type="submit">Создать новый режим работы</button>
</form>
<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search', array(
        'model' => $model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'visiting-hours-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'idClinic',
        'day',
        'timeFrom',
        'timeTo',
        array(
            'class' => 'CButtonColumn',
        ),
    ),
)); ?>
