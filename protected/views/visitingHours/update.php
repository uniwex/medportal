<?php
    /* @var $this VisitingHoursController */
    /* @var $model VisitingHours */

$this->breadCrumbs=array(
	'Режим работы'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

    $this->menu=array(
    array('label'=>'Режимы работы', 'url'=>array('index')),
    array('label'=>'Создать режим работы', 'url'=>array('create')),
    array('label'=>'Просмотр режимов работы', 'url'=>array('view', 'id'=>$model->id)),
    array('label'=>'Управление режимами работы', 'url'=>array('admin')),
    );
    ?>

    <h1>Обновить режим работы <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'clinic'=>$clinic,'days' => $days,)); ?>