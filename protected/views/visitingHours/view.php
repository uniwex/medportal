<?php
/* @var $this VisitingHoursController */
/* @var $model VisitingHours */

$this->breadCrumbs=array(
	'Visiting Hours'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List VisitingHours', 'url'=>array('index')),
array('label'=>'Create VisitingHours', 'url'=>array('create')),
array('label'=>'Update VisitingHours', 'url'=>array('update', 'id'=>$model->id)),
array('label'=>'Delete VisitingHours', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage VisitingHours', 'url'=>array('admin')),
);
?>

<h1>View VisitingHours #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
'data'=>$model,
'attributes'=>array(
		'id',
		'idClinic',
		'day',
		'timeFrom',
		'timeTo',
),
)); ?>
