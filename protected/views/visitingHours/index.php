<?php
/* @var $this VisitingHoursController */
/* @var $dataProvider CActiveDataProvider */

$this->breadCrumbs = array(
    'Visiting Hours',
);

$this->menu = array(
    array('label' => 'Добавить режим работы', 'url' => array('create')),
    array('label' => 'Управление режимами работы', 'url' => array('admin')),
);
?>

<h1>Режимы работы</h1>

<?php $this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_view',
)); ?>
