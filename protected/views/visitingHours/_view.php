<?php
/* @var $this VisitingHoursController */
/* @var $data VisitingHours */
?>

<div class="view">

    	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idClinic')); ?>:</b>
	<?php echo CHtml::encode($data->idClinic); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('day')); ?>:</b>
	<?php echo CHtml::encode($data->day); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('timeFrom')); ?>:</b>
	<?php echo CHtml::encode($data->timeFrom); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('timeTo')); ?>:</b>
	<?php echo CHtml::encode($data->timeTo); ?>
	<br />


</div>