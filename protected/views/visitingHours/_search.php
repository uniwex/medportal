<?php
/* @var $this VisitingHoursController */
/* @var $model VisitingHours */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

                    <div class="row">
            <?php echo $form->label($model,'id'); ?>
            <?php echo $form->textField($model,'id'); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'idClinic'); ?>
            <?php echo $form->textField($model,'idClinic'); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'day'); ?>
            <?php echo $form->textArea($model,'day',array('rows'=>6, 'cols'=>50)); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'timeFrom'); ?>
            <?php echo $form->textField($model,'timeFrom'); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'timeTo'); ?>
            <?php echo $form->textField($model,'timeTo'); ?>
        </div>

        <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->