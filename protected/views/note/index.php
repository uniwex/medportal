<div class="col-md-10" data-type="ticket">
    <div class="row indent">
        <div class="col-md-12">
            <h1 class="userInformationHead2 no-margin">Создание заметки</h1>
        </div>
    </div>
    <?php if($step==1){ ?>
    <?php $formNote = $this->beginWidget("CActiveForm", array(
        'id' => 'add-note',
        'action' => Yii::app()->request->baseUrl . '/note',
        'method' => 'post',
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true
        ),
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
            'autocomplete' => 'off',
            'onsubmit' => 'crutchForCKEditor();return true',
        ),
    ));
    ?>
    <div class="form-group row">
        <div class="col-md-2">
            Дата и время:
        </div>
        <div class="col-md-8">
            <?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
            $this->widget('CJuiDateTimePicker',array(
                'model'=>$eventModel, //Model object
                'attribute'=>'eventDate', //attribute name
                'mode'=>'datetime', //use "time","date" or "datetime" (default)
                'language'=>'ru',
                'options'=>array(
                    'dateFormat'=> 'yy-mm-dd',
                    'hourText'=>'Часы',
                    'minuteText'=>'Минуты',
                    'timeText'=>'Время'
                )
            ));
            ?>
            <?= $formNote->error($eventModel, 'eventDate', array('class' => 'text-red')) ?>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-2">
            Текст заметки:
        </div>
        <div class="col-md-8">
            <?= $formNote->textArea($eventModel, 'description', array('class' => 'fieldInput')); ?>
            <?= $formNote->error($eventModel, 'description', array('class' => 'text-red')) ?>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <?= CHtml::submitButton('Сохранить заметку', array('class' => 'button1 button1reg')); ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>
    <?php } ?>
    <?php if($step==2){ ?>
    <div class="form-group row">
        <div class="col-md-12">
            Заметка создана!
        </div>
    </div>
    <?php } ?>
</div>
<script src="//cdn.ckeditor.com/4.5.4/standard/ckeditor.js"></script>
<script>
    /*CKEDITOR.replace('Event[description]');
    var crutchForCKEditor = function(){
        var abc = document.getElementsByTagName('iframe')[0].contentDocument;
        var abc2 = abc.childNodes[1];
        var text = abc2.childNodes[1].childNodes[0].innerHTML;
        if(text != '<br>')
            $('#Event_description').val(text);
    }*/
</script>