<div class="right-menu-relative">
    <? require ("protected/views/site/rightMenu.php"); $vote = true; ?>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<div class="center-relative">
    <div class="question-desc">
        <p class="text-polls polls-name-text"><?= $poll['name']?></a></p>
        <div class="polls-prize">+<?= $poll['prize']?> баллов</div>
        <p class="text-polls text-polls-desc"><?= $poll['description']?></p>
    </div>
    <form action="finish" method="post">
        <div class="panel-group" id="accordion">
            <?foreach ($questions as $key=>$item) { ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$item->idQuestion;?>">
                            Вопрос <?=$key+1;?>. <?=$item->text;?>
                        </a>
                    </h4>
                </div>
                <div id="collapse<?=$item->idQuestion;?>" class="panel-collapse collapse <?= ($key == 0) ? 'in':'';?> ">
                    <div class="panel-body">
                        <?=$item->description;?>
                        <div class="answers-margin">
                            <? foreach ($item->answers as $answer) {?>
                                <?
                                if (isset($answers[$answer->idAnswer])) {
                                    $vote = false;
                                    echo '<span class="radio-align">'.$answer->text.' - '.round($answers[$answer->idAnswer],2).'%</span><br><br>';
                                } else {
                                ?>
                                <input required type="radio" name="Answers[<?=$item->idQuestion;?>]" value="<?=$answer->idAnswer;?>">
                                <span class="radio-align"><?=$answer->text;?></span>
                                <br>
                            <?
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <?}?>
        </div>
        <div class="vote-btn">
            <input type="hidden" name="idPoll" value="<?= $poll['id']?>">
            <?
            if ($vote) {
            ?> <button type="submit" class="btn btn-default">Голосовать!</button>
            <?}?>
        </div>
    </form>
</div>
