<? if (!Yii::app()->user->id) { ?>
<div class="polls">
<? } else { ?>
<div>
<? } ?>
    <p class="text-polls text-polls-desc"><?php echo CHtml::encode($data->datePoll); ?></p>

    <p class="text-polls polls-name-text"><a
            href="polls/questions?idPoll=<?php echo CHtml::encode($data->id); ?>"><?php echo CHtml::encode($data->name); ?></a>
    </p>

    <div class="polls-prize">+<?php echo CHtml::encode($data->prize); ?> баллов</div>
    <div class="polls-desc">
        <?= $data->description; ?>
    </div>
</div>