<div class="right-menu-relative" xmlns="http://www.w3.org/1999/html">
    <? require ("protected/views/site/rightMenu.php"); ?>
</div>
<div class="center-relative">
    <?if (!Yii::app()->user->id) {?>
        <div id="polls-info">
            <p class="text-relative">Для участия в голосованиях Вы должны быть зарегистрированы на</p>
            <p class="text-relative">портале. В настоящий момент Вы можете только ознакомиться с</p>
            <p class="text-relative">текущими опросами. <a href="/site/registration">Зарегистрироваться.</a></p>
        </div>
    <?}?>
    <div class="polls-it-works">
        <p class="text-relative"><h4>Как это работает?</h4></p>
        </br>
        <p class="text-relative polls-it-works-text">Ежемесячно мы</p>
        <p class="text-relative polls-it-works-text">предлагаем пользователям</p>
        <p class="text-relative polls-it-works-text">нашего портала обсудить</p>
        <p class="text-relative polls-it-works-text">важные вопросы,</p>
        <p class="text-relative polls-it-works-text">касающиеся медицины</p>
        <p class="text-relative polls-it-works-text">За прохождение каждого</p>
        <p class="text-relative polls-it-works-text">голосования Вам</p>
        <p class="text-relative polls-it-works-text">начисляются баллы.</p>
        <p class="text-relative polls-it-works-text">Раз в квартал мы проводим</p>
        <p class="text-relative polls-it-works-text">розыгрыш призов среди</p>
        <p class="text-relative polls-it-works-text">наиболее активных</p>
        <p class="text-relative polls-it-works-text">пользователей.</p>
        <p class="text-relative polls-it-works-text">Для участия в розыгрыше</p>
        <p class="text-relative polls-it-works-text">Вам необходимо набрать</p>
        <p class="text-relative polls-it-works-text">100 и более баллов.</p>
        </br>
        <a href="/polls/winners" class="text-relative polls-it-works-text">Победители прошедших розыгрышей</a>
    </div>
    <div id="polls-tabpane">
        <ul class="nav nav-pills" id="pills-first">
            <li class="active polls-info-btn" data-toggle="tab"><a href="#active">Активные</a></li>
            <li class="polls-info-btn" data-toggle="tab"><a href="#inactive">Завершенные</a></li>
        </ul>

        <div class="tab-content tab-content-polls">
            <div class="tab-pane active" id="active">
                </br>
                <?=(isset($_GET['message'])) ? '<div class="alert alert-success">Ваш голос учтен</div>' : ''; ?>
                <div>
                    <?php $this->widget('zii.widgets.CListView', array(
                        'dataProvider'=>$dataProviderActive,
                        'itemView'=>'_polls', // представление для одной записи
                        'ajaxUpdate'=>false, // отключаем ajax поведение
                        'emptyText'=>'Нет опросов.',
                        'summaryText'=>"{start}&mdash;{end} из {count}",
                        'template'=>'{summary} {sorter} {items} <hr> {pager}',
                        'pager'=>array(
                            'class'=>'CLinkPager',
                            'header'=>false,
                            'cssFile'=>'/css/pager.css', // устанавливаем свой .css файл
                            'htmlOptions'=>array('class'=>'pager'),
                        ),
                    )); ?>
                </div>
                </br>
            </div>
            <div class="tab-pane" id="inactive">
                </br>
                <div>
                    <?php $this->widget('zii.widgets.CListView', array(
                        'dataProvider'=>$dataProviderUnactive,
                        'itemView'=>'_pollsClosed', // представление для одной записи
                        'ajaxUpdate'=>false, // отключаем ajax поведение
                        'emptyText'=>'Нет опросов.',
                        'summaryText'=>"{start}&mdash;{end} из {count}",
                        'template'=>'{summary} {sorter} {items} <hr> {pager}',
                        'pager'=>array(
                            'class'=>'CLinkPager',
                            'header'=>false,
                            'cssFile'=>'/css/pager.css', // устанавливаем свой .css файл
                            'htmlOptions'=>array('class'=>'pager'),
                        ),
                    )); ?>
                </div>
                </br>
            </div>
        </div>
    </div>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.2/js/bootstrap.min.js"></script>
    <script>
        $('#pills-first a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        });
    </script>
</div>
