<div class="winners-container">
    <div class="winners-img">
        <? if ($data->users->type == 'clinic') {?>
            <img src="/upload/<?php echo CHtml::encode($data->clinic->pic); ?>" alt="">
        <?}?>
        <? if ($data->users->type == 'doctor') {?>
            <img src="/upload/<?php echo CHtml::encode($data->doctor->pic); ?>" alt="">
        <?}?>
        <? if ($data->users->type == 'nurse') {?>
            <img src="/upload/<?php echo CHtml::encode($data->nurse->pic); ?>" alt="">
        <?}?>
        <? if ($data->users->type == 'patient') {?>
            <img src="/upload/ab665b3533fd9ac3cb50e1efb64cd99f.png" alt="">
        <?}?>
    </div>

    <p class="text-polls winners-text-desc">
        <?=substr(CHtml::encode($data->users->name), 0, 60)?><?if(strlen($data->users->name)>60) echo '...';?>
    </p>
    <p class="text-polls winners-text-desc">(<?php echo CHtml::encode($data->users->city); ?>)</p>
    <p class="text-polls winners-text-desc">приз - <?=$data->prize; ?></p>
</div>
