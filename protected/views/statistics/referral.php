<?php
/* @var $this StatisticsController */
/* @var $model Partner */
/* @var $partners Partner */
/* @var $partner Partner */
/* @var $dataProvider CActiveDataProvider */
?>

    <div class="row">
        <div class="col-md-12">

            <div class="box box-info">
                <div class="box-header with-border ui-sortable-handle">
                    <h3 class="box-title">Статистика реферальной системы</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove">
                            <i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <? $this->widget('zii.widgets.grid.CGridView', array(
                        'dataProvider' => $model->search(),
                        'filter' => $model,
                        'cssFile' => Yii::app()->request->getBaseUrl(true) . '/assets/admin/css/gridview.css',
                        'itemsCssClass' => 'table no-margin',
                        'htmlOptions' => array(
                            'class' => 'table-responsive'
                        ),
                        'summaryText' => 'Показано записей: {start} - {end} из {count}',
                        'emptyText' => 'Результаты не найдены',
                        'columns' => array(
                            array(
                                'name' => 'idReferal',
                                'value' => ' CHtml::link(User::getName($data->idReferal), "", array(
                                "data-toggle" => "modal",
                                "data-target" => "#myModal",
                                "class" => "grid-ref",
                                "data-id" => $data->idReferal,
                                "onclick" => "viewRef(this)"
                            ))',
                                'filter' => CHtml::listData(
                                    $partners,
                                    'idReferal',
                                    function ($partner) {
                                        if (isset($partner->user->name)) {
                                            return $partner->user->name;
                                        }
                                    }),
                                'type' => 'raw'
                            ),
                            'money'
                        ),
                        'pager' => array(
                            'class' => 'CLinkPager',
                            'header' => '',
                            'nextPageLabel' => 'Следующая',
                            'prevPageLabel' => 'Предыдущая'
                        )
                    )); ?>
                </div>
                <div class="box-footer clearfix"></div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Приглашенные пользователи</h4>
                </div>
                <div class="modal-body">
                    <div id="preloader" class="text-center" style="display: none">
                        <img src="<?= Yii::app()->request->getBaseUrl(true) ?>/assets/admin/img/preloader.gif" alt="">
                    </div>
                    <? $this->renderPartial('list', array('dataProvider' => $dataProvider), false, true); ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        function viewRef(el) {
            var id = '';
            if (typeof(el) == 'number') id = el;
            else id = $(el).data('id');
            $.fn.yiiGridView.update('grid', {
                url: '/statistics/getReferral',
                data: {
                    id: id
                }
            })
        }
    </script>

<?
$script = <<<JS
    var url = location.search,
        regex = new RegExp(/.*?id=([\d]+)/).exec(url);
        if(regex) {
            viewRef(Number(regex[1]));
            $('#myModal').modal('show');
        }
JS;
Yii::app()->clientScript->registerScript('showRef', $script, CClientScript::POS_READY);
?>