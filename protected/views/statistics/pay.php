<?php
/* @var $this StatisticsController */
/* @var $model Pay */
/* @var $payments Pay */

$afterAjaxUpdate = <<<JS
function() { $('[name="Pay[time]"]').daterangepicker(); }
JS;
?>

    <div class="row">
        <div class="col-md-12">

            <div class="box box-info">
                <div class="box-header with-border ui-sortable-handle">
                    <h3 class="box-title">Статистика оплат</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove">
                            <i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <? $this->widget('zii.widgets.grid.CGridView', array(
                        'dataProvider' => $model->search(),
                        'filter' => $model,
                        'cssFile' => Yii::app()->request->getBaseUrl(true) . '/assets/admin/css/gridview.css',
                        'itemsCssClass' => 'table no-margin',
                        'htmlOptions' => array(
                            'class' => 'table-responsive'
                        ),
                        'afterAjaxUpdate' => $afterAjaxUpdate,
                        'summaryText' => 'Показано записей: {start} - {end} из {count}',
                        'emptyText' => 'Результаты не найдены',
                        'columns' => array(
                            'id',
                            array(
                                'name' => 'idUser',
                                'filter' => CHtml::listData(
                                    $payments,
                                    'idUser',
                                    function ($payment) {
                                        if (isset($payment->user->name)) {
                                            return $payment->user->name;
                                        }
                                    }),
                                'value' => 'User::getName($data->idUser)'
                            ),
                            array(
                                'name' => 'action',
                                'filter' => Yii::app()->params["typeAction"],
                                'value' => 'CHtml::label(Yii::app()->params["typeAction"]["$data->action"], "", array("class" => $data->action == "in" ? "label label-success" : "label label-danger"))',
                                'type' => 'raw'
                            ),
                            array(
                                'name' => 'type',
                                'filter' => Yii::app()->params["typePay"],
                                'value' => 'Yii::app()->params["typePay"]["$data->type"]'
                            ),
                            array(
                                'name' => 'count',
                                'header' => 'Сумма операции'
                            ),
                            'time'
                        ),
                    )); ?>
                </div>
                <div class="box-footer clearfix"></div>
            </div>

        </div>
    </div>

<?
$script = <<<JS
    $('[name="Pay[time]"]').daterangepicker();
JS;

Yii::app()->clientScript->registerScript('date', $script, CClientScript::POS_READY);
?>