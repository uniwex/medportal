<?php
/* @var $this AddInfoController */
/* @var $model AddInfo */

$this->breadCrumbs = array(
    'Доп. информация' => array('index'),
    'Управление',
);

$this->menu = array(
    array('label'=>'Доп. информация', 'url'=>array('index')),
    array('label'=>'Добавить доп. информацию', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$('#add-info-grid').yiiGridView('update', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Управление доп. информацией</h1>

<form action="/addInfo/create">
    <button type="submit">Создать новую доп. информацию</button>
</form>
<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search', array(
        'model' => $model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'add-info-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'idClinic',
        'name',
        'value',
        array(
            'class' => 'CButtonColumn',
        ),
    ),
)); ?>
