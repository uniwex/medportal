<?php
/* @var $this AddInfoController */
/* @var $model AddInfo */

$this->breadCrumbs = array(
    'Доп. информация' => array('index'),
    $model->name => array('view', 'id' => $model->id),
    'Изменить запись',
);

$this->menu = array(
    array('label' => 'Доп. информация', 'url' => array('index')),
    array('label' => 'Добавить доп. информацию', 'url' => array('create')),
    array('label' => 'Изменить доп. информацию', 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Управление доп. информацией', 'url' => array('admin')),
);
?>

    <h1>Изменить доп. информацию <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model' => $model, 'clinic' => $clinic)); ?>