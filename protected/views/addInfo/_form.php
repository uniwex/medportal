<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'add-info-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    )); ?>

    <div class="row">
        <div class="col-md-6 createGood">
            <div id="box-hours" class="box box-primary">
                <div class="box-header with-border">
                </div>
                <div class="box-body">

                    <?php echo $form->errorSummary($model); ?>

                    <div id="clinicName" class="form-group">
                        <?php echo $form->labelEx($model,'idClinic'); ?><br>
                        <?php echo $form->dropDownList($model,'idClinic',$clinic ); ?>
                        <?php echo $form->error($model,'idClinic'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'name'); ?>
                        <br>
                        <?php echo $form->textField($model, 'name', array('rows' => 6, 'cols' => 50)); ?>
                        <?php echo $form->error($model, 'name'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'value'); ?>
                        <br>
                        <?php echo $form->textField($model, 'value', array('rows' => 6, 'cols' => 50)); ?>
                        <?php echo $form->error($model, 'value'); ?>
                    </div>

                    <div class="row buttons text-center">
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать запись' : 'Сохранить'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->