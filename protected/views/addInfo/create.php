<?php
/* @var $this AddInfoController */
/* @var $model AddInfo */

$this->breadCrumbs = array(
    'Доп. информация' => array('index'),
    'Создать',
);

$this->menu = array(
    array('label' => 'Добавить доп. информацию', 'url' => array('create')),
    array('label' => 'Управление доп. информацией', 'url' => array('admin')),
);
?>

    <h1>Добавить доп. информацию</h1>
    <p>Данная информаци отобразится в разделе контакты</p>
    <p>на странице клиники</p>

<?php $this->renderPartial('_form', array('model' => $model, 'clinic' => $clinic)); ?>