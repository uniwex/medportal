<?php
/* @var $this AddInfoController */
/* @var $model AddInfo */

$this->breadCrumbs=array(
	'Доп. информация'=>array('index'),
	$model->name,
);

$this->menu=array(
array('label'=>'Доп. информация', 'url'=>array('index')),
array('label'=>'Добавить доп. информацию', 'url'=>array('create')),
array('label'=>'Изменить доп. информацию', 'url'=>array('update', 'id'=>$model->id)),
array('label'=>'Удалить доп. информацию', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Вы уверены, что хотите удалить данную запись?')),
array('label'=>'Управление доп. информацией', 'url'=>array('admin')),
);
?>

<h1>Просмотр доп. информации #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
'data'=>$model,
'attributes'=>array(
		'id',
		'idClinic',
		'name',
		'value',
),
)); ?>
