<?php
/* @var $this AddInfoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadCrumbs = array(
    'Доп. информация',
);

$this->menu = array(
    array('label' => 'Добавить доп. информацию', 'url' => array('create')),
    array('label' => 'Управление доп. информацией', 'url' => array('admin')),
);
?>

<h1>Дополнительная информация</h1>

<?php $this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_view',
)); ?>
