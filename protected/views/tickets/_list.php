<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 27.10.2015
 * Time: 12:13
 */
?>

<div class="col-md-12 search-block">
    <div class="row">
        <div class="col-md-2 text-center">
            <? if($data->type == 'Вопрос') { ?>
                <i class="fa fa-5x fa-question-circle text-blue"></i>
            <? } elseif($data->type == 'Жалоба') { ?>
                <i class="fa fa-5x fa-exclamation-triangle text-blue"></i>
            <? } ?>
        </div>
        <div class="col-md-6">
            <div class="userInformationHead2 no-margin">
                <a href="/tickets/view/<?= $data->id ?>"><?= $data->subject ?></a>
            </div>
            <div class="userInformationHead1 no-margin">
                <?= $data->user['name']; ?>
            </div>
            <p><?= $data->text ?></p>
        </div>
        <div class="col-md-4 text-right">
            <p>Дата создания: <b><?= $data->date ?></b></p>
            <? if(count($data->message)) {
                $lastMess = $data->message[0];
                echo '<p>Дата изменения: <b>'.$lastMess->messageDate.'</b></p>';
            } ?>
            <p>Статус: <?= Yii::app()->params['status'][$data->status] ?></p>
        </div>
    </div>
</div>
