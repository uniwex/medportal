<?
Yii::app()->clientScript->registerScript('search',
    "
$('#filter').on('click', function(){
    $.fn.yiiListView.update('list', {
            url: '" . CController::createUrl('tickets/') . "',
            data: 'type='+$(\"[id=Ticket_type]\").val()+'&date='+$(\"[id=Ticket_date]\").val()
        }
    )
});");
?>

<div id="postAdvert" class="col-md-10" data-type="ticket">
    <div class="row">
        <div class="col-md-3 col-xs-6">
            <div class="form-group">
                <?= CHtml::activeLabel(Ticket::model(), 'type') ?>
                <?= CHtml::activeDropDownList(Ticket::model(), 'type', array('' => 'Любой', 'Вопрос' => 'Вопрос', 'Жалоба' => 'Жалоба'), array('class' => 'fieldInput')); ?>
            </div>
        </div>
        <div class="col-md-3 col-xs-6">
            <div class="form-group">
                <?= CHtml::activeLabel(Ticket::model(), 'date') ?>
                <?
                $year = date('y');
                $month = date('m');
                $day = date('d');
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => Ticket::model(),
                    'attribute' => 'date',
                    'language' => 'ru',
                    'options' => array(
                        'dateFormat' => 'yy-mm-dd',
                        'showAnim' => 'fold',
                    ),
                    'htmlOptions' => array(
                        'class' => 'fieldInput',
                    ),
//                    'value' => $year . '-' . $month . '-' . $day
                )); ?>
            </div>
        </div>
        <div class="col-md-3 col-xs-6 text-center top-padding">
            <input class="button1 buttonFilter bg-blue" type="button" id="filter" value="Фильтровать">
        </div>
        <div class="col-md-3 col-xs-6 text-center top-padding">
            <input class="button1 buttonFilter bg-blue" type="button" value="Добавить" onclick="location.href='/tickets/new'">
        </div>
    </div>
    <div class="row">
        <? $this->renderPartial('list', array('dataProvider' => $dataProvider), false, true); ?>
    </div>
</div>