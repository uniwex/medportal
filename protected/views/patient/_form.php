<?php
/* @var $this PatientController */
/* @var $model Patient */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'patient-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'errorMessageCssClass' => 'control-label',
        'clientOptions' => array(
            'validateOnChange' => true,
            'validateOnSubmit' => true,
            'errorCssClass' => 'has-error',
            'successCssClass' => 'has-success'
        ),
    )); ?>


    <?php echo $form->errorSummary($model, 'Пожалуйста, исправьте следующие ошибки:', '', array('class' => 'callout callout-danger')); ?>

    <div class="box-body">

        <div class="form-group">
            <?php echo $form->labelEx($model, 'birthday'); ?>
            <?php echo $form->textField($model, 'birthday', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'birthday'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'oms'); ?>
            <?php echo $form->textField($model, 'oms', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'oms'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'dms'); ?>
            <?php echo $form->textField($model, 'dms', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'dms'); ?>
        </div>

        <div class="box-footer">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array('class' => 'btn btn-primary pull-right')); ?>
        </div>


        <?php $this->endWidget(); ?>

    </div>
</div><!-- form -->