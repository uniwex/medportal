<?php
/* @var $this PatientController */
/* @var $data Patient */
?>

<div class="view">

    	<b><?php echo CHtml::encode($data->getAttributeLabel('idUser')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idUser), array('view', 'id'=>$data->idUser)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('birthday')); ?>:</b>
	<?php echo CHtml::encode($data->birthday); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('oms')); ?>:</b>
	<?php echo CHtml::encode($data->oms); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dms')); ?>:</b>
	<?php echo CHtml::encode($data->dms); ?>
	<br />


</div>