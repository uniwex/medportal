<?php
/* @var $this DiseasesManageController */
/* @var $model Diseases */

$this->breadCrumbs = array(
    'Заболевания' => array('index'),
    'Управление',
);

$this->menu = array(
    array('label' => 'Список заболеваний', 'url' => array('index')),
    array('label' => 'Создать новое', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$('#diseases-grid').yiiGridView('update', {
data: $(this).serialize()
});
return false;
});
");
?>

<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search', array(
        'model' => $model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'diseases-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'idDirection',
        'name',
        'letter',
        'text',
        array(
            'class' => 'CButtonColumn',
        ),
    ),
)); ?>
