<?php
/* @var $this DiseasesManageController */
/* @var $model Diseases */

$this->breadCrumbs = array(
    'Заболевания' => array('index'),
    $model->id,
);

$this->menu = array(
    array('label' => 'Список заболеваний', 'url' => array('index')),
    array('label' => 'Добавить новое', 'url' => array('create')),
    array('label' => 'Изменить заболевание', 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Удалить заболевание', 'url' => '#',
        'linkOptions' => array(
            'submit' => array(
                'delete',
                'id' => $model->id
            ),
            'confirm' => 'Вы уверены, что хотите удалить данное заболевание?'
        )
    ),
    array('label' => 'Управление заболеваниями', 'url' => array('admin')),
);
?>

<h1>Просмотр заболевания #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'idDirection',
        'name',
        'letter',
        'text',
    ),
)); ?>
