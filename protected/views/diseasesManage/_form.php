<?php
/* @var $this DiseasesManageController */
/* @var $model Diseases */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'diseases-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    )); ?>
    <div class="row">
        <div class="col-md-6 createGood">
            <div class="box box-primary">
                <div class="box-header with-border">
                </div>
                <div class="box-body">


                    <?php echo $form->errorSummary($model); ?>

                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'idDirection'); ?>
                        <?php echo $form->dropDownList($model,'idDirection',$directions, array('class' => 'form-control')); ?>
                        <?php echo $form->error($model, 'idDirection'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'name'); ?><br>
                        <?php echo $form->textField($model, 'name'); ?>
                        <?php echo $form->error($model, 'name'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'text'); ?>
                        <?php echo $form->textArea($model, 'text', array('rows' => 6, 'cols' => 50)); ?>
                        <?php echo $form->error($model, 'text'); ?>
                    </div>

                    <div id="Diseases_button" class="row buttons text-center">
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>

</div><!-- form -->
<script src="//cdn.ckeditor.com/4.5.4/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('Diseases[text]');
    var crutchForCKEditor = function () {
        var abc = document.getElementsByTagName('iframe')[0].contentDocument;
        var abc2 = abc.childNodes[1];
        var text = abc2.childNodes[1].childNodes[0].innerHTML;
        if (text != '<br>')
            $('#Diseases_text').val(text);
    }
</script>