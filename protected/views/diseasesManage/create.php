<?php
/* @var $this DiseasesManageController */
/* @var $model Diseases */

$this->breadCrumbs = array(
    'Заболевания' => array('index'),
    'Добавить новое',
);

$this->menu = array(
    array('label' => 'Список заболеваний', 'url' => array('index')),
    array('label' => 'Управление заболеваниями', 'url' => array('admin')),
);
?>

    <h1>Добавить заболевание</h1>

<?php $this->renderPartial('_form', array('model' => $model, 'directions' => $directions)); ?>