<?php
/* @var $this DiseasesManageController */
/* @var $data Diseases */
?>

<div class="view">

    	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idDirection')); ?>:</b>
	<?php echo CHtml::encode($data->idDirection); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('letter')); ?>:</b>
	<?php echo CHtml::encode($data->letter); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('text')); ?>:</b>
	<?php echo CHtml::encode($data->text); ?>
	<br />


</div>