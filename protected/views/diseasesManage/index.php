<?php
/* @var $this DiseasesManageController */
/* @var $dataProvider CActiveDataProvider */

$this->breadCrumbs = array(
    'Заболенивания',
);

$this->menu = array(
    array('label' => 'Добавить новое', 'url' => array('create')),
    array('label' => 'Управление заболеваниями', 'url' => array('admin')),
);
?>

<h1>Заболевания</h1>

<?php $this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_view',
)); ?>
