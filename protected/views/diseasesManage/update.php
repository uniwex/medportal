<?php
/* @var $this DiseasesManageController */
/* @var $model Diseases */

$this->breadCrumbs = array(
    'Заболевания' => array('index'),
    $model->id => array('view', 'id' => $model->id),
    'Изменить',
);

$this->menu = array(
    array('label' => 'Список заболеваний', 'url' => array('index')),
    array('label' => 'Добавить новое', 'url' => array('create')),
    array('label' => 'Просмотр заболевания', 'url' => array('view', 'id' => $model->id)),
    array('label' => 'Управление заболеваниями', 'url' => array('admin')),
);
?>

    <h1>Изменить заболевание <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model' => $model, 'directions' => $directions)); ?>