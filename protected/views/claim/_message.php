<?
/* @var $data TicketMessages */

$counter = Yii::app()->session['counter'];
if ($data->idUser == Yii::app()->user->id) {
    $class = 'right';
    $left = 'pull-left';
    $right = 'pull-right';
} else {
    $class = '';
    $left = 'pull-right';
    $right = 'pull-left';
}
$image='no-user.jpg';
$type = '';
if (isset($data->user->type)) {
    $type = $data->user->type;
}

if ($type != 'patient') {
    if (isset($data->user->$type->pic)) {
        $image = $data->user->$type->pic;
    } else {
        $image = 'no-user.jpg';
    }
}
?>
<div class="direct-chat-msg <?= $class ?>">
    <div class="direct-chat-info clearfix">
        <span class="direct-chat-name <?= $right ?>"><? if (isset($data->user->name)) echo $data->user->name ?></span>
        <span
            class="direct-chat-timestamp <?= $left ?>"><? if (isset($data->messageDate)) echo $data->messageDate ?></span>
    </div>
    <img class="direct-chat-img"
         src="<?= Yii::app()->request->getBaseUrl(true) ?>/upload/<?= $image ?>"
         alt="Message User Image">

    <div class="direct-chat-text"><? if (isset($data->message)) echo $data->message?></div>
</div>