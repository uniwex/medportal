<div class="right-menu-relative" xmlns="http://www.w3.org/1999/html">
    <? require("protected/views/site/rightMenu.php"); ?>
</div>
<div class="center-overview">
    <p id="desease-text-list">Список заболеваний</p>
    <ul class="nav nav-pills" id="inset-desease">
        <li class="active desease-info-btn" data-toggle="tab"><a href="#active">По алфавиту</a></li>
        <li class="desease-info-btn" data-toggle="tab"><a href="#inactive">По направлениям</a></li>
    </ul>
    <div class="tab-content tab-content-disease">
        <div class="tab-pane active" id="active">
            </br>
            <div id="alphabet-disease">
                    <a name="А" href="javascript:void(0)">А</a>
                    <a name="Б" href="javascript:void(0)">Б</a>
                    <a name="В" href="javascript:void(0)">В</a>
                    <a name="Г" href="javascript:void(0)">Г</a>
                    <a name="Д" href="javascript:void(0)">Д</a>
                    <a name="Е" href="javascript:void(0)">Е</a>
                    <a name="Ж" href="javascript:void(0)">Ж</a>
                    <a name="З" href="javascript:void(0)">З</a>
                    <a name="И" href="javascript:void(0)">И</a>
                    <a name="Й" href="javascript:void(0)">Й</a>
                    <a name="К" href="javascript:void(0)">К</a>
                    <a name="Л" href="javascript:void(0)">Л</a>
                    <a name="М" href="javascript:void(0)">М</a>
                    <a name="Н" href="javascript:void(0)">Н</a>
                    <a name="О" href="javascript:void(0)">О</a>
                    <a name="П" href="javascript:void(0)">П</a>
                    <a name="Р" href="javascript:void(0)">Р</a>
                    <a name="С" href="javascript:void(0)">С</a>
                    <a name="Т" href="javascript:void(0)">Т</a>
                    <a name="У" href="javascript:void(0)">У</a>
                    <a name="Ф" href="javascript:void(0)">Ф</a>
                    <a name="Х" href="javascript:void(0)">Х</a>
                    <a name="Ц" href="javascript:void(0)">Ц</a>
                    <a name="Ч" href="javascript:void(0)">Ч</a>
                    <a name="Ш" href="javascript:void(0)">Ш</a>
                    <a name="Щ" href="javascript:void(0)">Щ</a>
                    <a name="Э" href="javascript:void(0)">Э</a>
                    <a name="Ю" href="javascript:void(0)">Ю</a>
                    <a name="Я" href="javascript:void(0)">Я</a>
            </div>
            <div id="column-disease-alphabet">
                <?$this->renderPartial('_listView', array('dataProvider' => $dataProvider));?>
            </div>
            <script>
                $('#alphabet-disease > a').on('click', function () {
                    $.ajax({
                        type: "GET",
                        url: "/disease/search?sort=" + encodeURI($(this).attr('name'))+ "&type=ajax"
                    }).done(function (data) {
                        $('#column-disease-alphabet').html(data);
                    })
                });
            </script>
            </br>
        </div>
        <div class="tab-pane" id="inactive">
            </br>
            <div id="column-disease">
                <? foreach ($directions as $value) {?>
                    <a href="/disease/direction/<?=$value->id?>"><?=$value->name?></a>
                <?}?>
            </div>
            </br>
        </div>
    </div>
</div>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.2/js/bootstrap.min.js"></script>
<script>
    $('#inset-desease a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    });
</script>
<script>
    $(document).on('click','.pager-disease  li  a', function () {
        $.ajax({
            type: "GET",
            url: $(this).attr("href")+"&type=ajax"
        }).done(function (data) {
            $('#column-disease-alphabet').html(data);
        });
        return false;
    });
</script>