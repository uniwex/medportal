<div class="right-menu-relative" xmlns="http://www.w3.org/1999/html">
    <? require("protected/views/site/rightMenu.php"); ?>
</div>
<div class="center-overview">
    <div class="col-lg-12 col-md-12 no-padding">
        <div class="col-lg-6 col-md-6 no-left-padding">
            <input id="search-disease-form" class="form-control select-clinic" placeholder="Поиск по названию"
                   type="text">
        </div>

        <script type="text/javascript">
            $('#search-disease-form').keyup(function () {
                if ($('#search-disease-form').val() == '') {
                    if ($('#change-direction > option:selected').val() == 'null') {
                        $.ajax({
                            type: "GET",
                            url: "/disease/changeDirection/?id=" + <?=$direction->id?>
                        }).done(function (data) {
                            $('#directions-disease-list').html(data);
                        });
                    } else {
                        $.ajax({
                            type: "GET",
                            url: "/disease/changeDirection/?id=" + $('#change-direction > option:selected').val()
                        }).done(function (data) {
                            $('#directions-disease-list').html(data);
                        });
                    }
                } else {
                    $.ajax({

                        type: "GET",
                        url: "/disease/searchDisease/?q=" + $('#search-disease-form').val()
                    }).done(function (data) {
                        $('#directions-disease-list').html(data);
                    });
                }
            });
        </script>
        <div id="drop-directions" class="col-lg-6 col-md-6">
            <div class="form-group">
                <select class="form-control select-clinic" id="change-direction">
                    <option value="null">Все направления</option>
                    <? foreach ($allDirections as $key => $value) { ?>
                        <option value="<?= $key + 1 ?>"><?= $value->name ?></option>
                    <? } ?>
                </select>
                <script>
                    $('#change-direction').on('change', function () {
                        if ($('#change-direction > option:selected').val() == 'null') {
                            $.ajax({
                                type: "GET",
                                url: "/disease/changeDirection/?id=" + <?=$direction->id?>
                            }).done(function (data) {
                                $('#directions-disease-list').html(data);
                            });
                        } else {
                            $.ajax({
                                type: "GET",
                                url: "/disease/changeDirection/?id=" + $('#change-direction > option:selected').val()
                            }).done(function (data) {
                                $('#directions-disease-list').html(data);
                            });
                        }
                    });
                </script>
            </div>
        </div>
    </div>
    <div id="directions-disease-list">
        <? if($direction) {
            $this->renderPartial('_directions', array('direction' => $direction));
        }
        if ($diseases) {
            $this->renderPartial('_searchDisease', array('diseases' => $diseases));
        }
        ?>
    </div>
</div>