<div class="right-menu-disease" xmlns="http://www.w3.org/1999/html">
    <? require("protected/views/site/rightMenu.php"); ?>
</div>
<div id="diseaseView" class="center-overview">
    <?= $disease->text ?>
    <? if ($clinics) {?>
        <p><strong>Клиники, занимающиеся данным заболеванием:</strong></p>
        <div id="disease-clinic">
            <?foreach ($clinics as $value) {?>
                <div class="col-md-3 col-lg-3 no-padding">
                    <img class="disease-clinic-pic" src="/upload/<?=$value->pic?>" alt="">
                </div>
                <div class="col-md-9 col-lg-9 no-padding">
                    <a href="/clinicView/<?=$value->idUser?>"><?=$value->name?></a>
                    <div id="disease-clinic-spec">
                        <?=$value->description?>
                    </div>
                </div>
            <?}?>
        </div>
    <?}?>
    <? if ($articles) { ?>
        <p><strong>Статьи на тему:</strong></p>
        <? foreach ($articles as $value) { ?>
            <a href="/articles/<?=$value->id?>"><?=$value->name?></a><br>
        <?
        }
    } ?>
</div>

