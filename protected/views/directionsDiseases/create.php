<?php
/* @var $this DirectionsDiseasesController */
/* @var $model DirectionsDiseases */

$this->breadCrumbs=array(
	'Направления заболеваний'=>array('index'),
	'Добавить направление',
);

$this->menu=array(
array('label'=>'Список направлений', 'url'=>array('index')),
array('label'=>'Управление направлениями', 'url'=>array('admin')),
);
?>

<h1>Добавить новое направление заболеваний</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>