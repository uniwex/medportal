<?php
/* @var $this DirectionsDiseasesController */
/* @var $model DirectionsDiseases */

$this->breadCrumbs = array(
    'Направления заболеваний' => array('index'),
    'Управление',
);

$this->menu = array(
    array('label' => 'Список направлений', 'url' => array('index')),
    array('label' => 'Добавить направление', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$('#directions-diseases-grid').yiiGridView('update', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Управление направлениями заболеваний</h1>


<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search', array(
        'model' => $model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'directions-diseases-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'name',
        array(
            'class' => 'CButtonColumn',
        ),
    ),
)); ?>
