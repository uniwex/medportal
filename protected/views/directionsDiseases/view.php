<?php
/* @var $this DirectionsDiseasesController */
/* @var $model DirectionsDiseases */

$this->breadCrumbs = array(
    'Направления заболеваний' => array('index'),
    $model->name,
);

$this->menu = array(
    array('label' => 'Список направлений', 'url' => array('index')),
    array('label' => 'Добавить направление', 'url' => array('create')),
    array('label' => 'Изменить направление', 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Удалить направление', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Управление направлениями', 'url' => array('admin')),
);
?>

<h1>Просмотр направления #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'name',
    ),
)); ?>
