<?php
/* @var $this DirectionsDiseasesController */
/* @var $model DirectionsDiseases */

$this->breadCrumbs = array(
    'Направления заболеваний' => array('index'),
    $model->name => array('view', 'id' => $model->id),
    'Изменить',
);

$this->menu = array(
    array('label' => 'Список направлений', 'url' => array('index')),
    array('label' => 'Добавить направление', 'url' => array('create')),
    array('label' => 'Просмотр направления', 'url' => array('view', 'id' => $model->id)),
    array('label' => 'Управление направлениями', 'url' => array('admin')),
);
?>

    <h1>Изменить направление <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model' => $model)); ?>