<?php
/* @var $this DirectionsDiseasesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadCrumbs = array(
    'Направления заболеваний',
);

$this->menu = array(
    array('label' => 'Добавить направление', 'url' => array('create')),
    array('label' => 'Управление направлениями', 'url' => array('admin')),
);
?>

<h1>Направления заболеваний</h1>

<?php $this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_view',
)); ?>
