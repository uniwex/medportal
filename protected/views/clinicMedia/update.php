<?php
/* @var $this ClinicMediaController */
/* @var $model ClinicMedia */

$this->breadCrumbs = array(
    'Фото/видео' => array('index'),
    $model->name => array('view', 'id' => $model->id),
    'Изменить',
);

$this->menu = array(
    array('label' => 'Фото/видео', 'url' => array('index')),
    array('label' => 'Добавить фото/видео', 'url' => array('create')),
    array('label' => 'Просмотр фото/видео', 'url' => array('view', 'id' => $model->id)),
    array('label' => 'Управление фото/видео', 'url' => array('admin')),


);
?>

    <h1>Изменение фото/видео <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model' => $model, 'clinic' => $clinic, 'types' => $types)); ?>