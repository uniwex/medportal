<?php
/* @var $this ClinicMediaController */
/* @var $model ClinicMedia */

$this->breadCrumbs = array(
    'Фото/видео' => array('index'),
    'Управление',
);

$this->menu = array(
    array('label' => 'Фото/видео', 'url' => array('index')),
    array('label' => 'Добавить фото/видео', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$('#clinic-media-grid').yiiGridView('update', {
data: $(this).serialize()
});
return false;
});
");
?>

<form action="/clinicMedia/create">
    <button type="submit">Создать новую доп. информацию</button>
</form>

<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search', array(
        'model' => $model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'clinic-media-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'idClinic',
        'type',
        'name',
        'src',
        array(
            'class' => 'CButtonColumn',
        ),
    ),
)); ?>
