<?php
/* @var $this ClinicMediaController */
/* @var $model ClinicMedia */

$this->breadCrumbs=array(
	'Фото/видео'=>array('index'),
	$model->name,
);

$this->menu=array(
array('label'=>'List ClinicMedia', 'url'=>array('index')),
array('label'=>'Create ClinicMedia', 'url'=>array('create')),
array('label'=>'Update ClinicMedia', 'url'=>array('update', 'id'=>$model->id)),
array('label'=>'Delete ClinicMedia', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage ClinicMedia', 'url'=>array('admin')),
);
?>

<h1>Просмотр записей о фото/видео #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
'data'=>$model,
'attributes'=>array(
		'id',
		'idClinic',
		'type',
		'name',
		'src',
),
)); ?>
