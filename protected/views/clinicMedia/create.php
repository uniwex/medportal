<?php
/* @var $this ClinicMediaController */
/* @var $model ClinicMedia */

$this->breadCrumbs = array(
    'Фото/видео' => array('index'),
    'Добавление',
);

$this->menu = array(
    array('label' => 'Фото/видео', 'url' => array('index')),
    array('label' => 'Управление фото/видео', 'url' => array('admin')),
);
?>

    <h1>Добавить фото/видео</h1>

<?php $this->renderPartial('_form', array('model' => $model, 'clinic' => $clinic, 'types' => $types)); ?>