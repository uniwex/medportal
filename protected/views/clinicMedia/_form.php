<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'clinic-media-form',
        'htmlOptions' => ['method' => 'POST', 'enctype' => 'multipart/form-data'],
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    )); ?>

    <div class="row">
        <div class="col-md-6 createGood">
            <div id="box-hours" class="box box-primary">
                <div class="box-header with-border">
                </div>
                <div class="box-body">

                    <?php echo $form->errorSummary($model); ?>

                    <div id="clinicName" class="form-group">
                        <?php echo $form->labelEx($model, 'idClinic'); ?>
                        <?php echo $form->dropDownList($model, 'idClinic', $clinic); ?>
                        <?php echo $form->error($model, 'idClinic'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'type'); ?>
                        <?php echo $form->dropDownList($model, 'type', $types); ?>
                        <?php echo $form->error($model, 'type'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'name'); ?>
                        <?php echo $form->textField($model, 'name', array('rows' => 6, 'cols' => 50)); ?>
                        <?php echo $form->error($model, 'name'); ?>
                    </div>

                    <div id="photo" class="form-group">
                        <?php echo $form->labelEx($model, 'picture'); ?>
                        <?php echo $form->fileField($model, 'picture', array('class' => 'form-control')); ?>
                        <?php echo $form->error($model, 'picture'); ?>
                    </div>

                    <div id="video" class="form-group invisible">
                        <?php echo $form->labelEx($model, 'src'); ?>
                        <?php echo $form->textField($model, 'src'); ?>
                        <?php echo $form->error($model, 'src'); ?>
                    </div>

                    <div class="row buttons text-center">
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(function () {
        $('#ClinicMedia_type').on('change', function () {
            var type = $('option:selected', this).attr('value');
            $(".invisible").removeClass('invisible');
            if (type == 'video') {
                $("#photo").hide();
                $("#video").show();
            }
            if (type == 'photo') {
                $("#photo").show();
                $("#video").hide();
            }
        });
    });
    $(function () {
        $(".buttons").on('click', function () {
            var type = $('option:selected', '#ClinicMedia_type').attr('value');
            if (type == 'photo') {
                $("#ClinicMedia_src").attr("value", "image");
            }
        })
    })
</script>