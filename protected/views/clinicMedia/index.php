<?php
/* @var $this ClinicMediaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadCrumbs = array(
    'Фото/видео',
);

$this->menu = array(
    array('label' => 'Фото/видео', 'url' => array('index')),
    array('label' => 'Управление фото/видео', 'url' => array('admin')),
);
?>

<h1>Фото/видео</h1>

<?php $this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_view',
)); ?>
<?= $this->renderPartial('rightMenu') ?>