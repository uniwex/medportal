<?php
    /* @var $this WinnersController */
    /* @var $model Winners */

$this->breadCrumbs=array(
	'Winners'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

    $this->menu=array(
    array('label'=>'List Winners', 'url'=>array('index')),
    array('label'=>'Create Winners', 'url'=>array('create')),
    array('label'=>'View Winners', 'url'=>array('view', 'id'=>$model->id)),
    array('label'=>'Manage Winners', 'url'=>array('admin')),
    );
    ?>

    <h1>Редактировать победителей <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'users' => $userArray, 'quiz' => $quizArray,)); ?>