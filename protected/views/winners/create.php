<?php
/* @var $this WinnersController */
/* @var $model Winners */

$this->breadCrumbs=array(
	'Winners'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'Список победителей', 'url'=>array('index')),
array('label'=>'Управление победителями', 'url'=>array('admin')),
);
?>

<h1>Создать победителей</h1>
<?php $this->renderPartial('_form', array('model'=>$model, 'usersA' => $usersA, 'quizA' => $quizA,)); ?>