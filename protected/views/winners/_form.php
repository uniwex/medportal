<?php
/* @var $this WinnersController */
/* @var $model Winners */
/* @var $form CActiveForm */
?>

<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'winners-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    )); ?>
    <div class="row">
        <div class="col-md-6 createGood">
            <div class="box box-primary">
                <div class="box-header with-border">
                </div>
                <div class="box-body">

                    <?php echo $form->errorSummary($model); ?>

                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'id'); ?><br>
                        <?php echo $form->dropDownList($model,'id',$usersA,['style'=>'width: 475px;']); ?>
                        <?php echo $form->error($model, 'id'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'idQuiz'); ?><br>
                        <?php echo $form->dropDownList($model,'idQuiz',$quizA); ?>
                        <?php echo $form->error($model, 'idQuiz'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'prize'); ?><br>
                        <?php echo $form->textArea($model, 'prize', array('rows' => 6, 'cols' => 50)); ?>
                        <?php echo $form->error($model, 'prize'); ?>
                    </div>

                    <div class="form-group buttons text-center">
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array('class' => 'btn btn-primary save')); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <?php $this->endWidget(); ?>

</div><!-- form -->


<script src="//cdn.ckeditor.com/4.5.4/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('Winners[prize]');
    var crutchForCKEditor = function(){
        var abc = document.getElementsByTagName('iframe')[0].contentDocument;
        var abc2 = abc.childNodes[1];
        var text = abc2.childNodes[1].childNodes[0].innerHTML;
        if(text != '<br>')
            $('#Winners_prize').val(text);
    }
</script>