<?php
/* @var $this WinnersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadCrumbs=array(
	'Winners',
);

$this->menu=array(
array('label'=>'Create Winners', 'url'=>array('create')),
array('label'=>'Manage Winners', 'url'=>array('admin')),
);
?>

<h1>Победители</h1>

<?php $this->widget('zii.widgets.CListView', array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
