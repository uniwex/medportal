<div class="col-md-5 auth">
    <? $form = $this->beginWidget('CActiveForm', array(
        'id' => 'login-form',
        'action' => Yii::app()->request->baseUrl . '/site/login',
        'method' => 'post',
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmmit' => true,
            'validateOnChange' => true
        )
    )) ?>
    <h3>Авторизация</h3>
    <div id="error" class="text-red form-group hidden"></div>
    <div class="form-group">
        <? $this->widget('CMaskedTextField', array(
            'model' => $model,
            'attribute' => 'phone',
            'mask' => '+99999999999',
            'placeholder' => '*',
            'htmlOptions' => array(
                'class' => 'form-control',
                'placeholder' => 'Телефон *'
            )
        )); ?>
        <?= $form->error($model, 'phone') ?>
    </div>

    <div class="form-group">
        <?= $form->passwordField($model, 'password', array('class' => 'form-control', 'placeholder' => 'Пароль *')) ?>
        <?= $form->error($model, 'password') ?>
    </div>
    <div class="form-group">
        <?= CHtml::ajaxSubmitButton('Войти', Yii::app()->request->baseUrl . '/site/login', array(
            'success' => 'function(data){
                    if(/Ошибка авторизации/.test(data)) {
                        $("#error").removeClass("hidden");
                        $("#error").html(data);
                    } else if(data == 1) {
                        location.href = "' . Yii::app()->request->getBaseUrl(true) . '";
                    }
                }'
        ), array('class' => 'col-md-5')) ?>
        <a name="restore" class="col-md-5 col-md-offset-2" href="<?= Yii::app()->request->getBaseUrl(true) ?>/site/restore">Забыли пароль?</a>
    </div>
    <? $this->endWidget(); ?>
</div>
