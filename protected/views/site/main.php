<?php
if (isset($systemMsg)) { ?>
    <script>
        $(document).ready(function() {
            $('#myModalLabel').html('Регистрация');
            $('#modal-body').html('<?= $systemMsg; ?>');
            $('#myModal').modal();
        })
    </script>
<? } ?>

<div class="col-md-5 col-xs-12 indent" data-type="task">
    <div class="infoBlock">
        <div class="row">
            <div class="col-md-7 col-xs-7">
                <div class="infoForDoctors">
                    <div class="title">для врачей и клиник</div>
                    <div class="description">1 - Зарегистрируйтесь<br>
                        2 - Совершенно бесплатно разместите своё объявление<br>
                        3 - Заполните свой профиль<br>
                        4 - Оцените свой приём<br>
                        5 - Продвиньте своё объявление в поиске
                    </div>
                </div>
                <a style="text-decoration:none;" href="/site/inProcess"><div class="addAd">Подать объявление</div></a>
            </div>
            <div class="col-md-5 col-xs-5">
                <div class="registrationIcon">
                    <img src="<?= Yii::app()->request->baseUrl ?>/assets/images/iconRegistration1.png">
                    <div class="regText"><a href="/site/registration">Регистрация</a></div>
                </div>
            </div>
            <img src="<?= Yii::app()->request->baseUrl ?>/assets/images/doctors1.png" class="picDoctors">
        </div>
    </div>
</div>

<div class="col-md-5 col-xs-12 indent" data-type="task">
    <div class="infoBlock">
        <div class="row">
            <div class="col-md-7 col-xs-7">
                <div class="infoForClients">
                    <div class="title">для пациентов</div>
                    <div class="description">1 - Вы можете найти врача и клинику<br>
                        2 - Ознакомиться с рейтингом врача и узнать стоимость приёма<br>
                        3 - Выбрать врача по параметрам: рейтинг, отзывы, цена приёма<br>
                        4 - Быстро записаться на приём<br>
                        5 - Оставить отзывы о приёме
                    </div>
                </div>
                <?php if (!Yii::app()->user->isGuest) { ?><a style="text-decoration:none;" href="/visit"><?php } ?>
                <?php if (Yii::app()->user->isGuest) { ?><a style="text-decoration:none;" href="/site/registration"><?php } ?>
                    <div class="addAd" style="background-color: #EE7600; color: #FFF;">Записаться</div>
                </a>
            </div>
            <div class="col-md-5 col-xs-5">
                <div class="registrationIcon">
                    <img src="<?= Yii::app()->request->baseUrl ?>/assets/images/iconRegistration1.png">
                    <div class="regText">
                        <a href="/site/registration">Регистрация</a>
                    </div>
                </div>
            </div>
            <img src="<?= Yii::app()->request->baseUrl ?>/assets/images/doctors2.png" class="picDoctors">
        </div>
    </div>
</div>

<div class="col-md-10 section">
    <ul>
        <li class="usefulSection">
            <a href="/medbook"><img src='<?= Yii::app()->request->baseUrl ?>/assets/images/medIcon1.png'></a>
            <span><a href="/medbook">Медицинский справочник</a></span>
        </li>
        <li class="usefulSection">
            <a href="/site/inProcess"><img src='<?= Yii::app()->request->baseUrl ?>/assets/images/medIcon2.png'></a>
            <a href="/site/inProcess"><span>Что болит?</span></a>
        </li>
        <li class="usefulSection">
            <a href="/site/inProcess"><img src='<?= Yii::app()->request->baseUrl ?>/assets/images/medIcon4.png'></a>
            <a href="/site/inProcess"><span>Для инвалидов</span></a>
        </li>
        <li class="usefulSection">
            <a href="/site/inProcess"><img src='<?= Yii::app()->request->baseUrl ?>/assets/images/medIcon3.png'></a>
            <a href="/site/inProcess"><span>Кардио</span></a>
        </li>
        <li class="usefulSection">
            <a href="/site/inProcess"><img src='<?= Yii::app()->request->baseUrl ?>/assets/images/medIcon6.png'></a>
            <a href="/site/inProcess"><span>Транспортировка больных</span></a>
        </li>
        <li class="usefulSection">
            <a href="/articles"><img src='<?= Yii::app()->request->baseUrl ?>/assets/images/medIcon5.png'></a>
            <span><a href="/articles">Полезные статьи</a></span>
        </li>
    </ul>
</div>

<div class="col-md-5 news indent">
    <div id="news" class="infoMenuItem">
        <div class="title">Новости</div>
        <div class="infoMenuNewsContent">
            Запись <span style="color:#1FA9D9;">к</span> доктору - сайт бесплатных объявлений. Миллионы людей в месяц посещают наш сайт. Ежедневно
            подаются сотни тысяч новых объявлений.
            Здесь легко найти нужный товар или услугу всего за несколько минут.
        </div>
    </div>
</div>

<div class="col-md-5 news indent">
    <div id="service" class="infoMenuItem">
        <div class="title">Патронажная служба</div>
        <div class="infoMenuNewsContent">
            <ul>
                <li>
                    <div class="li-info">Если Вы работаете в данной сфере разместите своё объявление</div>
                    <a href="/site/inProcess"><div class="addAd">Подать объявление</div></a>
                </li>
                <li>
                    <div class="li-info">Здесь Вы можете найти помощника себе и своим близким</div>
                    <? $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'search-form',
                        'action' => Yii::app()->request->baseUrl . '/search/',
                    )) ?>
                    <?= CHtml::hiddenField('type', 'nurse') ?>
                    <input class="addAd" type="submit" name="submitNurse" style="background-color:#F7793E;color:#FFF;border:none;" value="Найти сиделку">
                    <?
                    $city = CHtml::listData(User::model()->findAll(), 'city', 'city');
                    asort($city);
                    $city = array_merge(array('' => 'Все города'), $city);
                    ?>
                    <?= CHtml::dropDownList(
                        'city',
                        isset($_POST['city']) ? $_POST['city'] : '',
                        $city,
                        array('class' => 'form-control hidden')
                    ) ?>
                    <?
                    $country = CHtml::listData(User::model()->findAll(), 'country', 'country');
                    asort($country);
                    $country = array_merge(array('' => 'Все страны'), $country);
                    ?>
                    <?= CHtml::dropDownList(
                        'country',
                        isset($_POST['country']) ? $_POST['country'] : '',
                        $country,
                        array('class' => 'form-control hidden')
                    ) ?>
                    <? $this->endWidget(); ?>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body" id="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>