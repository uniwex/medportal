<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 14.09.2015
 * Time: 15:58
 */
?>
<div class="col-md-offset-2 col-md-5 auth">
    <? $form = $this->beginWidget('CActiveForm', array(
        'id' => 'restore-form',
        'action' => Yii::app()->request->baseUrl . '/site/restore',
        'method' => 'post',
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmmit' => true,
            'validateOnChange' => true
        )
    )) ?>
    <h3>Восстановление пароля</h3>

    <div id="error" class="text-red form-group hidden"></div>

    <div class="form-group">
        <? $this->widget('CMaskedTextField', array(
            'model' => $model,
            'attribute' => 'phone',
            'mask' => '+79999999999',
            'placeholder' => '*',
            'htmlOptions' => array(
                'class' => 'form-control',
                'placeholder' => 'Телефон *'
            )
        )); ?>
        <?= $form->error($model, 'password') ?>
    </div>
    <div id="code" class="form-group hidden"></div>
    <div class="form-group">
        <?= CHtml::ajaxSubmitButton('Восстановить пароль', Yii::app()->request->baseUrl . '/site/restore', array(
            'success' => 'function(data){
                var response = JSON.parse(data);
                console.log(response.code);
                switch (response.code) {
                    case 0:
                        $($("form#restore-form").find(".form-group")[1]).after(
                        "<div class=\"form-group\">" +
                            "<label>Введите код восстановления:</label>" +
                            "<input type=\"text\" name=\"User[restoreCode]\" class=\"form-control\">" +
                        "</div>");
                        $("#error").addClass("hidden");
                        break;
                    case 1:
                    case 3:
                        $("#error").removeClass("hidden").html(response.status);
                        $("[name=\"User[restoreCode]\"]").parent().remove();
                        $("[name=\"User[phone]\"]").val("");
                        break;
                    case 2:
                        $("#error").addClass("hidden");
                        $("form [name=\"User[restoreCode]\"]").parent().remove();
                        $("form [name=\"User[phone]\"]").parent().remove();
                        $("form input[type=submit]").parent().remove();
                        $("#code").removeClass("hidden").html(
                            "<label>" + response.status + "</label>" +
                            "<input class=\"form-control\" type=\"text\" value=" + response.password + " readonly>");
                        break;
                }
            }'
        ), array('class' => 'col-md-5')) ?>
    </div>
    <? $this->endWidget(); ?>
</div>
