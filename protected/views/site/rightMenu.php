<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 right-menu">
    <div class="ulBox">
        <h3>Медицинский справочник</h3>
        <ul>
            <li>Аптеки</li>
            <li>Лекарства</li>
            <li>Лекарственные травы</li>
            <li><a href="/medcatalog/profdisease">Профзаболевания</a></li>
            <li><a href="/medcatalog/parapharmaceutic">Парафармацевтика</a></li>
            <li>Центры реабилитации</li>
            <li>Лечебные курорты</li>
        </ul>
    </div>
    <div class="ulBox">
        <h3>Симптомы</h3>
    </div>
    <div class="ulBox">
        <h3>Диагностика</h3>
        <ul>
            <li>Анализы</li>
            <li>Биометрия</li>
            <li>Кардио</li>
            <li>Инфо-эко</li>
        </ul>
    </div>
    <div class="ulBox">
        <h3>Для инвалидов</h3>
        <ul>
            <li>Маркет</li>
            <li>Доступная среда</li>
            <li>Аутизм-коммуникатор</li>
            <li>Вторичный рынок</li>
            <li>Аренда</li>
        </ul>
    </div>
    <div class="ulBox">
        <h3>Транспортировка больных</h3>
    </div>
    <div class="ulBox">
        <h3>Скорая помощь</h3>
    </div>
    <div class="ulBox">
        <h3>Страхование</h3>
    </div>
    <div class="ulBox">
        <h3>Правовая помощь</h3>
        <ul>
            <li>Публикации</li>
            <li>Подать жалобу</li>
        </ul>
    </div>
    <div class="ulBox">
        <a href="/sections"><h3>Статьи</h3></a>
    </div>
    <div class="ulBox">
        <h3>Научные работы</h3>
    </div>
    <div class="ulBox">
        <h3>Программы</h3>
        <ul>
            <li>Расходы на лечение</li>
            <li>Счётчик каллорий</li>
        </ul>
    </div>
    <div class="ulBox">
        <h3>Услуги</h3>
        <ul>
            <li>Шоферские медкомиссии</li>
            <li>Доставка еды</li>
            <li>Детские клоуны</li>
            <li>Репетиторы</li>
            <li>Священники</li>
        </ul>
    </div>
    <div id="degrees360">
        <img src="/assets/images/360degrees.png">
        <a href="/overview"><span>Клиники изнутри</span></a>
    </div>
    <div id="clinicNavigation">
        <a href="/site/inProcess"><span>GPS GLONASS<br>навигация</span></a>
        <img src="/assets/images/mini-map.png" style="width:100%;">
    </div>
</div>