<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 27.10.2015
 * Time: 12:13
 */

?>

<?php if(isset($data->specialization)) { ?>
<div class="col-md-12">
    <div class="row form-group">
        <div class="col-md-4">Имя: <?= $data->name; ?></div>
        <div class="col-md-4">Специальность: <?= $data->spec->name; ?></div>
    </div>
    <div class="row form-group">
        <div class="col-md-12">
            <div class="tabTitle">График работы</div>
            <table class="timetable">
                <tr>
                    <th>Понедельник</th>
                    <th>Вторник</th>
                    <th>Среда</th>
                    <th>Четверг</th>
                    <th>Пятница</th>
                    <th>Суббота</th>
                    <th>Воскресенье</th>
                </tr>
                <tr>
                    <th><?= $data->monday; ?></th>
                    <th><?= $data->tuesday; ?></th>
                    <th><?= $data->wednesday; ?></th>
                    <th><?= $data->thursday; ?></th>
                    <th><?= $data->friday; ?></th>
                    <th><?= $data->saturday; ?></th>
                    <th><?= $data->sunday; ?></th>
                </tr>
            </table>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-2">
            <?= CHtml::submitButton('Записаться', array('class' => 'button1 button1reg', 'onclick'=>'(location.host != "localhost") ? location.href="/visit?step=5&docId='.$data->id.'" : location.href="/medportal/visit?step=5&docId='.$data->id.'"')); ?>
        </div>
    </div>
</div>
<?php } ?>