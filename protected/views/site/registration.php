<div class="col-md-8" id="postAdvert">
    <div class="form-group text-black" style="font-size:12pt;">
        Перед добавлением на сайт каждое объявление проверяется на соответствие правилам
    </div>
    <!------------------------------------------------------------------>
    <!------------------------------------------------------------------>
    <?php
    $formPatient = $this->beginWidget("CActiveForm", array(
        'id' => 'patient-registration',
        'action' => Yii::app()->request->baseUrl . '/site/regUser',
        'method' => 'post',
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
        ),
        'htmlOptions' => array(
            'autocomplete' => 'off'
        ),
    )); ?>
    <span class="displayPatient">
        <div class="form-group">
            <?= $formPatient->errorSummary($userModel, 'Пожалуйста, исправьте следующие ошибки:', '', array('class' => 'text-red')) ?>
            <?= $formPatient->errorSummary($patientModel, '', '', array('class' => 'text-red')) ?>
            <?= $formPatient->errorSummary($stationModel, '', '', array('class' => 'text-red')) ?>
        </div>
        <div class="form-group row">
            <div class="col-md-8">
                <?= $formPatient->radioButtonList($userModel, 'type', array('0' => 'Пациент', '1' => 'Клиника', '2' => 'Сиделка', '3' => 'Частный врач'), array('onclick' => 'switchUserType(this)', 'data-ut' => 'patient')); ?>
                <?= $formPatient->error($userModel, 'type', array('class' => 'text-red')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formPatient->labelEx($userModel, 'name') ?>
            </div>
            <div class="col-md-8">
                <?= $formPatient->textField($userModel, 'name', array('class' => 'fieldInput marginForInput', 'placeholder' => 'Иванов И.И.')); ?>
                <?= $formPatient->error($userModel, 'name', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formPatient->labelEx($userModel, 'email') ?>
            </div>
            <div class="col-md-8">
                <?= $formPatient->textField($userModel, 'email', array('class' => 'fieldInput marginForInput', 'placeholder' => 'sample@mail.ru')); ?>
                <?= $formPatient->checkBox($userModel, 'emailEnable', array('id' => 'User_emailEnable_4')); ?>
                <?= $formPatient->labelEx($userModel, 'emailEnable', array('for' => 'User_emailEnable_4')) ?>
                <?= $formPatient->error($userModel, 'email', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formPatient->labelEx($userModel, 'password') ?>
            </div>
            <div class="col-md-8">
                <?= $formPatient->passwordField($userModel, 'password', array('class' => 'fieldInput marginForInput')); ?>
                <?= $formPatient->error($userModel, 'password', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formPatient->labelEx($userModel, 'phone') ?>
            </div>
            <div class="col-md-8">
                <?= $formPatient->textField($userModel, 'phone', array('class' => 'fieldInput marginForInput', 'placeholder' => '+79000000000')) ?>
                <?= $formPatient->error($userModel, 'phone', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formPatient->labelEx($patientModel, 'birthday') ?>
            </div>
            <div class="col-md-8">
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'name' => 'birthday',
                    'model' => $patientModel,
                    'attribute' => 'birthday',
                    'language' => 'ru',
                    'options' => array(
                        'dateFormat' => 'yy-mm-dd',
                        'class' => 'fieldInput',
                        'showAnim' => 'fold',
                    ),
                    'htmlOptions' => array(
                        'class' => 'fieldInput marginForInput',
                        'id' => 'Patient_birthday',
                    ),
                )); ?>
                <?= $formPatient->error($patientModel, 'birthday', array('class' => 'text-red marginForInput')); ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formPatient->labelEx($userModel, 'country') ?>
            </div>
            <div class="col-md-8">
                <?= $formPatient->dropDownList($userModel, 'country', array(0 => 'Выберите страну', 1 => 'Россия'), array('class' => 'fieldInput marginForInput', 'data-alias' => 'country')); ?>
                <?= $formPatient->error($userModel, 'country', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formPatient->labelEx($userModel, 'city') ?>
            </div>
            <div class="col-md-4 col-xs-6">
                <?= $formPatient->textField($userModel, 'city', array('class' => 'fieldInput inputForSearchCity marginForInput')); ?>
                <?= $formPatient->error($userModel, 'city', array('class' => 'text-red marginForInput')) ?>
                <select class="dropDownCities marginForInput" type="select" size="5" data-alias="city">
                    <option>Выберите город</option>
                </select>
            </div>
            <div class="col-md-4 col-xs-6">
                <?= $formPatient->dropDownList($stationModel, 'name', array('0' => 'Выберите станцию метро', 'Нет метро' => 'Нет метро'), array('class' => 'fieldInput marginForInput', 'data-alias' => 'metro', 'multiple' => 'multiple', 'style' => 'width:100%')); ?>
                <?= $formPatient->error($stationModel, 'name', array('class' => 'text-red marginForInput')) ?>
                <div class="selectButton"></div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formPatient->labelEx($patientModel, 'oms') ?>
            </div>
            <div class="col-md-8">
                <?= $formPatient->textField($patientModel, 'oms', array('class' => 'fieldInput marginForInput')); ?>
                <?= $formPatient->error($patientModel, 'oms', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formPatient->labelEx($patientModel, 'dms') ?>
            </div>
            <div class="col-md-8">
                <?= $formPatient->textField($patientModel, 'dms', array('class' => 'fieldInput marginForInput')); ?>
                <?= $formPatient->error($patientModel, 'dms', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <?= CHtml::submitButton('Зарегистрироваться', array('class' => 'button1 button1reg marginForInput')); ?>
            </div>
        </div>
    </span>
    <?php $this->endWidget(); ?>

    <!------------------------------------------------------------------>
    <!------------------------------------------------------------------>
    <?php $formClinic = $this->beginWidget("CActiveForm", array(
        'id' => 'clinic-registration',
        'action' => Yii::app()->request->baseUrl . '/site/regUser',
        'method' => 'post',
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true
        ),
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
            'autocomplete' => 'off',
            'onsubmit' => 'crutchForClinicCKEditor();return true',
        ),
    )); ?>
    <span class="displayClinic">
        <div class="form-group">
            <?= $formClinic->errorSummary($userModel, 'Пожалуйста, исправьте следующие ошибки:', '', array('class' => 'text-red')) ?>
            <?= $formClinic->errorSummary($clinicModel, '', '', array('class' => 'text-red')) ?>
            <?= $formClinic->errorSummary($scheduleModel, '', '', array('class' => 'text-red')) ?>
            <?= $formClinic->errorSummary($specializationModel, '', '', array('class' => 'text-red')) ?>
        </div>
        <span class="displayClinicStep1">
            <div class="form-group row">
                <div class="col-md-8">
                    <?= $formClinic->radioButtonList($userModel, 'type', array('0' => 'Пациент', '1' => 'Клиника', '2' => 'Сиделка', '3' => 'Частный врач'), array('onclick' => 'switchUserType(this)', 'data-ut' => 'clinic')); ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($userModel, 'name') ?>
                </div>
                <div class="col-md-8">
                    <?= $formClinic->textField($userModel, 'name', array('class' => 'fieldInput marginForInput', 'placeholder' => 'Вишневского')); ?>
                    <?= $formClinic->error($userModel, 'name', array('class' => 'text-red marginForInput')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($clinicModel, 'site') ?>
                </div>
                <div class="col-md-8">
                    <?= $formClinic->textField($clinicModel, 'site', array('class' => 'fieldInput marginForInput', 'placeholder' => 'www.fotoditazin.com')); ?>
                    <?= $formClinic->error($clinicModel, 'site', array('class' => 'text-red marginForInput')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($userModel, 'email') ?>
                </div>
                <div class="col-md-8">
                    <?= $formClinic->textField($userModel, 'email', array('class' => 'fieldInput marginForInput', 'placeholder' => 'sample@mail.ru')); ?>
                    <?= $formClinic->checkBox($userModel, 'emailEnable', array('id' => 'User_emailEnable_2')); ?>
                    <?= $formClinic->labelEx($userModel, 'emailEnable', array('for' => 'User_emailEnable_2')); ?>
                    <?= $formClinic->error($userModel, 'email', array('class' => 'text-red marginForInput')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($userModel, 'password') ?>
                </div>
                <div class="col-md-8">
                    <?= $formClinic->passwordField($userModel, 'password', array('class' => 'fieldInput marginForInput')); ?>
                    <?= $formClinic->error($userModel, 'password', array('class' => 'text-red marginForInput')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($userModel, 'phone') ?>
                </div>
                <div class="col-md-8">
                    <?= $formClinic->textField($userModel, 'phone', array('class' => 'fieldInput marginForInput', 'placeholder' => '+79000000000')) ?>
                    <?= $formClinic->error($userModel, 'phone', array('class' => 'text-red marginForInput')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($userModel, 'country') ?>
                </div>
                <div class="col-md-8">
                    <?= $formClinic->dropDownList($userModel, 'country', array(0 => 'Выберите страну', 1 => 'Россия'), array('class' => 'fieldInput marginForInput', 'data-alias' => 'country')); ?>
                    <?= $formClinic->error($userModel, 'country', array('class' => 'text-red marginForInput')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($userModel, 'city') ?>
                </div>
                <div class="col-md-4 col-xs-6">
                    <?= $formClinic->textField($userModel, 'city', array('class' => 'fieldInput inputForSearchCity marginForInput')); ?>
                    <?= $formClinic->error($userModel, 'city', array('class' => 'text-red marginForInput')) ?>
                    <select class="dropDownCities marginForInput" type="select" size="5" data-alias="city">
                        <option>Выберите город</option>
                    </select>
                </div>
                <div class="col-md-4 col-xs-6">
                    <?= $formClinic->dropDownList($stationModel, 'name', array('0' => 'Выберите станцию метро', 'Нет метро' => 'Нет метро'), array('class' => 'fieldInput marginForInput', 'data-alias' => 'metro', 'multiple' => 'multiple', 'style' => 'width: 100%')); ?>
                    <?= $formClinic->error($stationModel, 'name', array('class' => 'text-red marginForInput')) ?>
                    <div class="selectButton"></div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($scheduleModel, 'График работы клиники') ?>
                </div>
                <div class="col-md-9 marginForInput" style="width:100px;">
                    <div class="tabTitle">График работы</div>
                    <table class="timetable" data-id="1" data-type="timeclinic" onclick="openSchedule(this)">
                        <tr>
                            <th>Пн</th>
                            <th>Вт</th>
                            <th>Ср</th>
                            <th>Чт</th>
                            <th>Пт</th>
                            <th>Сб</th>
                            <th>Вс</th>
                        </tr>
                        <tr>
                            <th class="weekend"><input value="выходной"></th>
                            <th class="weekend"><input value="выходной"></th>
                            <th class="weekend"><input value="выходной"></th>
                            <th class="weekend"><input value="выходной"></th>
                            <th class="weekend"><input value="выходной"></th>
                            <th class="weekend"><input value="выходной"></th>
                            <th class="weekend"><input value="выходной"></th>
                        </tr>
                    </table>
                    <input data-id="1" data-type="timeclinic" name="Schedule[timeworkClinic]"
                           id="Schedule_timeworkClinic"
                           type="hidden"/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($clinicModel, 'specialization') ?>
                </div>
                <div class="col-md-9">
                    <?= $formClinic->textArea($clinicModel, 'specialization', array('class' => 'fieldInput marginForInput', 'style' => 'width:100%;height:100px;')); ?>
                    <i class="marginForInput">* Для лучшего форматирования текста начинайте каждый пункт с новой строки</i>
                    <?= $formClinic->error($clinicModel, 'specialization', array('class' => 'text-red marginForInput')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($clinicModel, 'license') ?>
                </div>
                <div class="col-md-8">
                    <?= $formClinic->textField($clinicModel, 'license', array('class' => 'fieldInput marginForInput')); ?>
                    <?= $formClinic->error($clinicModel, 'license', array('class' => 'text-red marginForInput')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($clinicModel, 'merit') ?>
                </div>
                <div class="col-md-8">
                    <?= $formClinic->textField($clinicModel, 'merit', array('class' => 'fieldInput marginForInput')); ?>
                    <?= $formClinic->error($clinicModel, 'merit', array('class' => 'text-red marginForInput')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($clinicModel, 'contactAdvanced') ?>
                </div>
                <div class="col-md-8">
                    <?= $formClinic->textField($clinicModel, 'contactAdvanced', array('class' => 'fieldInput marginForInput')); ?>
                    <?= $formClinic->error($clinicModel, 'contactAdvanced', array('class' => 'text-red marginForInput')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($clinicModel, 'description') ?>
                </div>
                <div class="col-md-9">
                    <?= $formClinic->textArea($clinicModel, 'description', array('class' => 'fieldInput marginForInput', 'style' => 'width:100%;height:100px;')); ?>
                    <?= $formClinic->error($clinicModel, 'description', array('class' => 'text-red marginForInput')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($clinicModel, 'image') ?>
                </div>
                <div class="col-md-8">
                    <div class="fieldInput marginForInput" id="image">
                        <?= $formClinic->fileField($clinicModel, 'image'); ?>
                    </div>
                    <?= $formClinic->labelEx($clinicModel, '* Размер картинки не может превышать 5 MB', array('class'=>'marginForInput')) ?>
                    <?= $formClinic->error($clinicModel, 'image', array('class' => 'text-red marginForInput')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($socialModel, 'vkontakte'); ?>
                </div>
                <div class="col-md-8">
                    <?= $formClinic->textField($socialModel, 'vkontakte', array('class' => 'fieldInput marginForInput')); ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($socialModel, 'odnoklassniki'); ?>
                </div>
                <div class="col-md-8">
                    <?= $formClinic->textField($socialModel, 'odnoklassniki', array('class' => 'fieldInput marginForInput')); ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($socialModel, 'facebook'); ?>
                </div>
                <div class="col-md-8">
                    <?= $formClinic->textField($socialModel, 'facebook', array('class' => 'fieldInput marginForInput')); ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($socialModel, 'twitter'); ?>
                </div>
                <div class="col-md-8">
                    <?= $formClinic->textField($socialModel, 'twitter', array('class' => 'fieldInput marginForInput')); ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($socialModel, 'instagram'); ?>
                </div>
                <div class="col-md-8">
                    <?= $formClinic->textField($socialModel, 'instagram', array('class' => 'fieldInput marginForInput')); ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <input id="regClinicToStep2" type="button" class="button1 button1reg marginForInput" value="Перейти к шагу 2">
                </div>
            </div>
        </span>
        <span class="displayClinicStep2">
            <div class="form-group row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <input id="regClinicToStep1" type="button" class="button1 button1reg marginForInput" value="Назад к шагу 1">
                </div>
            </div>
            <span id="doctorsInClinic">
                <div class="form-doctor">
                    <div class="form-group row">
                        <div class="col-md-2">
                            <?= $formClinic->labelEx($scheduleModel, 'name') ?>
                        </div>
                        <div class="col-md-8">
                            <?= $formClinic->textField($scheduleModel, 'name', array('class' => 'fieldInput marginForInput', 'name' => 'Schedule[name][]')); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <?= $formClinic->labelEx($scheduleModel, 'specialization', array('data-spec' => 'clinic')) ?>
                        </div>
                        <div class="col-md-8">
                            <?= $formClinic->dropDownList($scheduleModel, 'specialization', CHtml::listData(Specialization::model()->findAll(), 'id', 'name'), array('class' => 'fieldInput marginForInput', 'name' => 'Specialization[name][]')); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <?= $formClinic->labelEx($scheduleModel, 'timeSize') ?>
                        </div>
                        <div class="col-md-8">
                            <?= $formClinic->numberField($scheduleModel, 'timeSize', array('class' => 'fieldInput marginForInput', 'name' => 'Schedule[timeSize][]')); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <?= $formClinic->labelEx($scheduleModel, 'price') ?>
                        </div>
                        <div class="col-md-8">
                            <?= $formClinic->numberField($scheduleModel, 'price', array('class' => 'fieldInput marginForInput', 'name' => 'Schedule[price][]')); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <?= $formClinic->labelEx($scheduleModel, 'График работы врача') ?>
                        </div>
                        <div class="col-md-9 marginForInput" style="width:100px;">
                            <div class="tabTitle">Время приёма</div>
                            <table class="timetable" data-id="1" data-type="clinic" onclick="openSchedule(this)">
                                <tr>
                                    <th>Пн</th>
                                    <th>Вт</th>
                                    <th>Ср</th>
                                    <th>Чт</th>
                                    <th>Пт</th>
                                    <th>Сб</th>
                                    <th>Вс</th>
                                </tr>
                                <tr>
                                    <th class="weekend"><input value="выходной"></th>
                                    <th class="weekend"><input value="выходной"></th>
                                    <th class="weekend"><input value="выходной"></th>
                                    <th class="weekend"><input value="выходной"></th>
                                    <th class="weekend"><input value="выходной"></th>
                                    <th class="weekend"><input value="выходной"></th>
                                    <th class="weekend"><input value="выходной"></th>
                                </tr>
                            </table>
                            <input data-id="1" data-type="clinic" name="Schedule[timework][]" id="Schedule_timework"
                                   type="hidden" />
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <input class="button1 btnAddDoctor marginForInput" type="button" value="Добавить ещё врача">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <?= CHtml::submitButton('Зарегистрироваться', array('class' => 'button1 button1reg marginForInput', 'click' => 'alert()')); ?>
                    </div>
                </div>
            </span>
        </span>
    </span>
    <?php $this->endWidget(); ?>
    <!------------------------------------------------------------------>
    <!------------------------------------------------------------------>
    <?php $formNurse = $this->beginWidget("CActiveForm", array(
        'id' => 'nurse-registration',
        'action' => Yii::app()->request->baseUrl . '/site/regUser',
        'method' => 'post',
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
        ),
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
            'autocomplete' => 'off',
            'onsubmit' => 'crutchForNurseCKEditor();return true',
        ),
    )); ?>
    <span class="displayNurse">
        <div class="form-group">
            <?= $formNurse->errorSummary($userModel, 'Пожалуйста, исправьте следующие ошибки:', '', array('class' => 'text-red')) ?>
            <?= $formNurse->errorSummary($nurseModel, '', '', array('class' => 'text-red')) ?>
            <?= $formNurse->errorSummary($stationModel, '', '', array('class' => 'text-red')) ?>
        </div>
        <div class="form-group row">
            <div class="col-md-8">
                <?= $formNurse->radioButtonList($userModel, 'type', array('0' => 'Пациент', '1' => 'Клиника', '2' => 'Сиделка', '3' => 'Частный врач'), array('onclick' => 'switchUserType(this)', 'data-ut' => 'nurse')); ?>
                <?= $formNurse->error($userModel, 'type', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($userModel, 'name') ?>
            </div>
            <div class="col-md-8">
                <?= $formNurse->textField($userModel, 'name', array('class' => 'fieldInput marginForInput', 'placeholder' => 'Иванов И.И.')); ?>
                <?= $formNurse->error($userModel, 'name', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($userModel, 'email') ?>
            </div>
            <div class="col-md-8">
                <?= $formNurse->textField($userModel, 'email', array('class' => 'fieldInput marginForInput', 'placeholder' => 'sample@mail.ru')); ?>
                <?= $formNurse->checkBox($userModel, 'emailEnable', array('id' => 'User_emailEnable_3')); ?>
                <?= $formNurse->labelEx($userModel, 'emailEnable', array('for' => 'User_emailEnable_3')) ?>
                <?= $formNurse->error($userModel, 'email', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($userModel, 'password') ?>
            </div>
            <div class="col-md-8">
                <?= $formNurse->passwordField($userModel, 'password', array('class' => 'fieldInput marginForInput')); ?>
                <?= $formNurse->error($userModel, 'password', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($userModel, 'phone') ?>
            </div>
            <div class="col-md-8">
                <?= $formNurse->textField($userModel, 'phone', array('class' => 'fieldInput marginForInput', 'placeholder' => '+79000000000')) ?>
                <?= $formNurse->error($userModel, 'phone', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($userModel, 'country') ?>
            </div>
            <div class="col-md-8">
                <?= $formNurse->dropDownList($userModel, 'country', array(0 => 'Выберите страну', 1 => 'Россия'), array('class' => 'fieldInput marginForInput', 'data-alias' => 'country')); ?>
                <?= $formNurse->error($userModel, 'country', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($userModel, 'city') ?>
            </div>
            <div class="col-md-4 col-xs-6">
                <?= $formNurse->textField($userModel, 'city', array('class' => 'fieldInput inputForSearchCity marginForInput')); ?>
                <?= $formNurse->error($userModel, 'city', array('class' => 'text-red marginForInput')) ?>
                <select class="dropDownCities marginForInput" type="select" size="5" data-alias="city">
                    <option>Выберите город</option>
                </select>
            </div>
            <div class="col-md-4 col-xs-6">
                <?= $formNurse->dropDownList($stationModel, 'name', array('0' => 'Выберите станцию метро', 'Нет метро' => 'Нет метро'), array('class' => 'fieldInput marginForInput', 'data-alias' => 'metro', 'style' => 'width:100%', 'multiple' => 'multiple')); ?>
                <?= $formNurse->error($stationModel, 'name', array('class' => 'text-red marginForInput')) ?>
                <div class="selectButton"></div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($nurseModel, 'education') ?>
            </div>
            <div class="col-md-9">
                <?= $formNurse->textArea($nurseModel, 'education', array('class' => 'fieldInput marginForInput', 'style' => 'width:100%;height:100px;')); ?>
                <i class="marginForInput">* Для лучшего форматирования текста начинайте каждый пункт с новой строки</i>
                <?= $formNurse->error($nurseModel, 'education', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($nurseModel, 'specialization') ?>
            </div>
            <div class="col-md-9">
                <?= $formNurse->textArea($nurseModel, 'specialization', array('class' => 'fieldInput marginForInput', 'style' => 'width:100%;height:100px;')); ?>
                <i class="marginForInput">* Для лучшего форматирования текста начинайте каждый пункт с новой строки</i>
                <?= $formNurse->error($nurseModel, 'specialization', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($nurseModel, 'expirience') ?>
            </div>
            <div class="col-md-9">
                <?= $formNurse->textArea($nurseModel, 'expirience', array('class' => 'fieldInput marginForInput', 'style' => 'width:100%;height:100px;')); ?>
                <i class="marginForInput">* Для лучшего форматирования текста начинайте каждый пункт с новой строки</i>
                <?= $formNurse->error($nurseModel, 'expirience', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($nurseModel, 'categorySick') ?>
            </div>
            <div class="col-md-8">
                <?= $formNurse->textField($nurseModel, 'categorySick', array('class' => 'fieldInput marginForInput')); ?>
                <?= $formNurse->error($nurseModel, 'categorySick', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($nurseModel, 'residence') ?>
            </div>
            <div class="col-md-8 marginForInput">
                <?= $formNurse->radioButtonList($nurseModel, 'residence', array(0 => 'Без проживания', 1 => 'С проживанием')); ?>
                <?= $formNurse->error($nurseModel, 'residence', array('class' => 'text-red')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($nurseModel, 'food') ?>
            </div>
            <div class="col-md-8 marginForInput">
                <?= $formNurse->radioButtonList($nurseModel, 'food', array(0 => 'Без приготовления еды', 1 => 'С приготовлением еды')); ?>
                <?= $formNurse->error($nurseModel, 'food', array('class' => 'text-red')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($nurseModel, 'priceHour') ?>
            </div>
            <div class="col-md-8">
                <?= $formNurse->numberField($nurseModel, 'priceHour', array('class' => 'fieldInput marginForInput')); ?>
                <?= $formNurse->error($nurseModel, 'priceHour', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($nurseModel, 'priceDay') ?>
            </div>
            <div class="col-md-8">
                <?= $formNurse->numberField($nurseModel, 'priceDay', array('class' => 'fieldInput marginForInput')); ?>
                <?= $formNurse->error($nurseModel, 'priceDay', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($nurseModel, 'priceMonth') ?>
            </div>
            <div class="col-md-8">
                <?= $formNurse->numberField($nurseModel, 'priceMonth', array('class' => 'fieldInput marginForInput')); ?>
                <?= $formNurse->error($nurseModel, 'priceMonth', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($nurseModel, 'description') ?>
            </div>
            <div class="col-md-9">
                <?= $formNurse->textArea($nurseModel, 'description', array('class' => 'fieldInput marginForInput', 'style' => 'width:100%;height:100px;')); ?>
                <?= $formNurse->error($nurseModel, 'description', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($nurseModel, 'image') ?>
            </div>
            <div class="col-md-8">
                <div class="fieldInput marginForInput" id="image">
                    <?= $formNurse->fileField($nurseModel, 'image'); ?>
                </div>
                <?= $formNurse->labelEx($nurseModel, '* Размер картинки не может превышать 5 MB', array('class'=>'marginForInput')) ?>
                <?= $formNurse->error($nurseModel, 'image', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($socialModel, 'vkontakte'); ?>
            </div>
            <div class="col-md-8">
                <?= $formNurse->textField($socialModel, 'vkontakte', array('class' => 'fieldInput marginForInput')); ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($socialModel, 'odnoklassniki'); ?>
            </div>
            <div class="col-md-8">
                <?= $formNurse->textField($socialModel, 'odnoklassniki', array('class' => 'fieldInput marginForInput')); ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($socialModel, 'facebook'); ?>
            </div>
            <div class="col-md-8">
                <?= $formNurse->textField($socialModel, 'facebook', array('class' => 'fieldInput marginForInput')); ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($socialModel, 'twitter'); ?>
            </div>
            <div class="col-md-8">
                <?= $formNurse->textField($socialModel, 'twitter', array('class' => 'fieldInput marginForInput')); ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($socialModel, 'instagram'); ?>
            </div>
            <div class="col-md-8">
                <?= $formNurse->textField($socialModel, 'instagram', array('class' => 'fieldInput marginForInput')); ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <?= CHtml::submitButton('Зарегистрироваться', array('class' => 'button1 button1reg marginForInput')); ?>
            </div>
        </div>
    </span>
    <?php $this->endWidget(); ?>
    <!------------------------------------------------------------------>
    <!------------------------------------------------------------------>
    <?php $formDoctor = $this->beginWidget("CActiveForm", array(
        'id' => 'doctor-registration',
        'action' => Yii::app()->request->baseUrl . '/site/regUser',
        'method' => 'post',
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
        ),
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
            'autocomplete' => 'off',
            'onsubmit' => 'crutchForDoctorCKEditor();return true',
        ),
    )); ?>
    <span class="displayDoctor">
        <div class="form-group">
            <?= $formDoctor->errorSummary($userModel, 'Пожалуйста, исправьте следующие ошибки:', '', array('class' => 'text-red')) ?>
            <?= $formDoctor->errorSummary($doctorModel, '', '', array('class' => 'text-red')) ?>
            <?= $formDoctor->errorSummary($specializationModel, '', '', array('class' => 'text-red')) ?>
        </div>
        <div class="form-group row">
            <div class="col-md-8">
                <?= $formDoctor->radioButtonList($userModel, 'type',
                    array('0' => 'Пациент', '1' => 'Клиника', '2' => 'Сиделка', '3' => 'Частный врач'),
                    array('onclick' => 'switchUserType(this)', 'data-ut' => 'doctor')); ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formDoctor->labelEx($userModel, 'name') ?>
            </div>
            <div class="col-md-8">
                <?= $formDoctor->textField($userModel, 'name', array('class' => 'fieldInput marginForInput')); ?>
                <?= $formDoctor->error($userModel, 'name', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formDoctor->labelEx($userModel, 'email') ?>
            </div>
            <div class="col-md-8">
                <?= $formDoctor->textField($userModel, 'email', array('class' => 'fieldInput marginForInput', 'placeholder' => 'sample@mail.ru')); ?>
                <?= $formDoctor->checkBox($userModel, 'emailEnable', array('id' => 'User_emailEnable_1')); ?>
                <?= $formDoctor->labelEx($userModel, 'emailEnable', array('for' => 'User_emailEnable_1')); ?>
                <?= $formDoctor->error($userModel, 'email', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formDoctor->labelEx($userModel, 'password') ?>
            </div>
            <div class="col-md-8">
                <?= $formDoctor->passwordField($userModel, 'password', array('class' => 'fieldInput marginForInput')); ?>
                <?= $formDoctor->error($userModel, 'password', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formDoctor->labelEx($userModel, 'phone') ?>
            </div>
            <div class="col-md-8">
                <?= $formDoctor->textField($userModel, 'phone', array('class' => 'fieldInput marginForInput', 'placeholder' => '+79000000000')) ?>
                <?= $formDoctor->error($userModel, 'phone', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formDoctor->labelEx($userModel, 'country') ?>
            </div>
            <div class="col-md-8">
                <?= $formDoctor->dropDownList(
                    $userModel,
                    'country',
                    array(0 => 'Выберите страну', 1 => 'Россия'),
                    array('class' => 'fieldInput marginForInput', 'data-alias' => 'country')); ?>
                <?= $formDoctor->error($userModel, 'country', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formDoctor->labelEx($userModel, 'city'); ?>
            </div>
            <div class="col-md-4 col-xs-6">
                <?= $formDoctor->textField($userModel, 'city', array('class' => 'fieldInput inputForSearchCity marginForInput')); ?>
                <?= $formDoctor->error($userModel, 'city', array('class' => 'text-red marginForInput')) ?>
                <select class="dropDownCities marginForInput" type="select" size="5" data-alias="city">
                    <option>Выберите город</option>
                </select>
            </div>
            <div class="col-md-4 col-xs-6">
                <?= $formDoctor->dropDownList($stationModel, 'name', array('' => 'Выберите станцию метро', 'Нет метро' => 'Нет метро'), array('class' => 'fieldInput marginForInput', 'data-alias' => 'metro', 'style' => 'width: 100%', 'multiple' => 'multiple')); ?>
                <?= $formDoctor->error($stationModel, 'name', array('class' => 'text-red marginForInput')) ?>
                <div class="selectButton"></div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formDoctor->labelEx($doctorModel, 'typeHuman') ?>
            </div>
            <div class="col-md-8">
                <?= $formDoctor->dropDownList($doctorModel, 'typeHuman', array('Дети' => 'Дети', 'Взрослые' => 'Взрослые', 'Все' => 'Все'), array('class' => 'fieldInput marginForInput')) ?>
                <?= $formDoctor->error($doctorModel, 'typeHuman', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formDoctor->labelEx($doctorModel, 'specialization'); ?>
            </div>
            <div class="col-md-9">
                <?= $formDoctor->textArea($doctorModel, 'specialization', array('class' => 'fieldInput marginForInput', 'style' => 'width:100%;height:100px;')); ?>
                <i class="marginForInput">* Для лучшего форматирования текста начинайте каждый пункт с новой строки</i>
                <?= $formDoctor->error($doctorModel, 'specialization', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formDoctor->labelEx($doctorModel, 'expirience') ?>
            </div>
            <div class="col-md-9">
                <?= $formDoctor->textArea($doctorModel, 'expirience', array('class' => 'fieldInput marginForInput', 'style' => 'width:100%;height:100px;')); ?>
                <i class="marginForInput">* Для лучшего форматирования текста начинайте каждый пункт с новой строки</i>
                <?= $formDoctor->error($doctorModel, 'expirience', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formDoctor->labelEx($doctorModel, 'education') ?>
            </div>
            <div class="col-md-9">
                <?= $formDoctor->textArea($doctorModel, 'education', array('class' => 'fieldInput marginForInput', 'style' => 'width:100%;height:100px;')); ?>
                <i class="marginForInput">* Для лучшего форматирования текста начинайте каждый пункт с новой строки</i>
                <?= $formDoctor->error($doctorModel, 'education', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <!--..-->
        <div class="form-group row">
            <div class="col-md-2">
            </div>
            <div class="col-md-8 marginForInput">
                <?= $formDoctor->checkBox($doctorModel, 'isPhototherapy'); ?>
                <?= $formDoctor->labelEx($doctorModel, 'isPhototherapy') ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formDoctor->labelEx($specializationModel, 'name') ?>
            </div>
            <div class="col-md-8">
                <?= $formDoctor->dropDownList($specializationModel, 'name', CHtml::listData(Specialization::model()->findAll(), 'id', 'name'), array('class' => 'fieldInput marginForInput')); ?>
                <?= $formDoctor->error($specializationModel, 'name', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formDoctor->labelEx($doctorModel, 'license'); ?>
            </div>
            <div class="col-md-8">
                <?= $formDoctor->textField($doctorModel, 'license', array('class' => 'fieldInput marginForInput')); ?>
                <?= $formDoctor->error($doctorModel, 'license', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formDoctor->labelEx($doctorModel, 'merit'); ?>
            </div>
            <div class="col-md-8">
                <?= $formDoctor->textField($doctorModel, 'merit', array('class' => 'fieldInput marginForInput')); ?>
                <?= $formDoctor->error($doctorModel, 'merit', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <label>График работы</label>
            </div>
            <div class="col-md-9 marginForInput" style="width:100px;">
                <div class="tabTitle">Время приёма</div>
                <table class="timetable" data-id="1" data-type="doc" onclick="Schedule(this)">
                    <tr>
                        <th>Пн</th>
                        <th>Вт</th>
                        <th>Ср</th>
                        <th>Чт</th>
                        <th>Пт</th>
                        <th>Сб</th>
                        <th>Вс</th>
                    </tr>
                    <tr>
                        <th class="weekend"><input value="выходной"></th>
                        <th class="weekend"><input value="выходной"></th>
                        <th class="weekend"><input value="выходной"></th>
                        <th class="weekend"><input value="выходной"></th>
                        <th class="weekend"><input value="выходной"></th>
                        <th class="weekend"><input value="выходной"></th>
                        <th class="weekend"><input value="выходной"></th>
                    </tr>
                </table>
                <input data-id="1" data-type="doc" name="Schedule[timework]" id="Schedule_timework" type="hidden"/>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formDoctor->labelEx($doctorModel, 'description'); ?>
            </div>
            <div class="col-md-9">
                <?= $formDoctor->textArea($doctorModel, 'description', array('class' => 'fieldInput marginForInput', 'style' => 'width:100%;height:100px;')); ?>
                <?= $formDoctor->error($doctorModel, 'description', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                Длительность приёма
            </div>
            <div class="col-md-8">
                <?= $formDoctor->numberField($scheduleModel, 'timeSize', array('class' => 'fieldInput marginForInput')); ?>
                <?= $formDoctor->error($scheduleModel, 'tiemSize', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formDoctor->labelEx($doctorModel, 'price'); ?>
            </div>
            <div class="col-md-8">
                <?= $formDoctor->numberField($doctorModel, 'price', array('class' => 'fieldInput marginForInput')); ?>
                <?= $formDoctor->error($doctorModel, 'price', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formDoctor->labelEx($doctorModel, 'image'); ?>
            </div>
            <div class="col-md-8">
                <div id="image" class="fieldInput marginForInput">
                    <?= $formDoctor->fileField($doctorModel, 'image', array('class' => 'fieldInput')); ?>
                </div>
                <?= $formDoctor->labelEx($doctorModel, '* Размер картинки не может превышать 5 MB', array('class' => 'marginForInput')) ?>
                <?= $formDoctor->error($doctorModel, 'image', array('class' => 'text-red marginForInput')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formDoctor->labelEx($socialModel, 'vkontakte'); ?>
            </div>
            <div class="col-md-8">
                <?= $formDoctor->textField($socialModel, 'vkontakte', array('class' => 'fieldInput marginForInput')); ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formDoctor->labelEx($socialModel, 'odnoklassniki'); ?>
            </div>
            <div class="col-md-8">
                <?= $formDoctor->textField($socialModel, 'odnoklassniki', array('class' => 'fieldInput marginForInput')); ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formDoctor->labelEx($socialModel, 'facebook'); ?>
            </div>
            <div class="col-md-8">
                <?= $formDoctor->textField($socialModel, 'facebook', array('class' => 'fieldInput marginForInput')); ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formDoctor->labelEx($socialModel, 'twitter'); ?>
            </div>
            <div class="col-md-8">
                <?= $formDoctor->textField($socialModel, 'twitter', array('class' => 'fieldInput marginForInput')); ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formDoctor->labelEx($socialModel, 'instagram'); ?>
            </div>
            <div class="col-md-8">
                <?= $formDoctor->textField($socialModel, 'instagram', array('class' => 'fieldInput marginForInput')); ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <?= CHtml::submitButton('Зарегистрироваться', array('class' => 'button1 button1reg marginForInput')); ?>
            </div>
        </div>
    </span>
    <?php $this->endWidget(); ?>
    <!------------------------------------------>
    <!------------------------------------------>
    <div style="margin-top:20px;margin-left:145px;display:none;">
        <div class="button1" style="background-color: rgb(0, 108, 167);">Бесплатное размещение</div>
        <div class="button1" style="margin-left:15px;background-color:rgb(247, 121, 62);">VIP размещение</div>
        <div id="vipDescription">VIP размещение гарантирует Вам в течение 7 дней демонстрацию Вашего объявления в правой
            колонке выделенного окна
        </div>
    </div>
</div>
<div id="aboutRules" class="col-md-2 no-padding">
    <div id="rules">
        <div class="point">1</div>
        <div class="text">Заполните форму Вашего объявления.<br> Данная информация будет опубликована на нашем сайте.
        </div>
        <div class="point">2</div>
        <div class="text">Вы можете редактировать Ваш профиль</div>
        <div class="point">3</div>
        <div class="text">В личном кабинете Вы сможете увидеть информацию о записи пациентов</div>
    </div>
    <a href="/site/inProcess"><div id="moreRules">Подробнее о правилах ></div></a>
    <div id="sellQuickly">
        <div class="howSell">Как продать быстрее?</div>
        <ul>
            <li><p>Указывайте конкурентную цену приёма</p></li>
            <li><p>Подробно опишите Вашу услугу</p></li>
            <li><p>Выберите пакет</p>

                <a href="/site/inProcess"><p style="color:#F7793E;margin-top:-10px;">VIP продажа</p></a></li>
        </ul>
    </div>
</div>
<link rel="stylesheet" href="<?= Yii::app()->request->getBaseUrl(true); ?>/assets/admin/bootstrap/css/bootstrap-multiselect.css">
<script src="<?= Yii::app()->request->getBaseUrl(true); ?>/assets/admin/bootstrap/js/bootstrap-multiselect.js"></script>
<script src="//cdn.ckeditor.com/4.5.4/standard/ckeditor.js"></script>
<script type="text/javascript">
    //CKEDITOR INITIALIZATION
    //DOCTOR {
    CKEDITOR.replace('Doctor[specialization]');
    CKEDITOR.replace('Doctor[expirience]');
    CKEDITOR.replace('Doctor[education]');
    //call when form submit
    var crutchForDoctorCKEditor = function(){
        var iframesDoctor = $('.displayDoctor iframe');
        for(var i=0;i<iframesDoctor.length;i++) {
            var text = iframesDoctor[i].contentDocument.childNodes[1].childNodes[1].childNodes[0].innerHTML;
            if (text != '<br>') {
                $(iframesDoctor[i].parentNode.parentNode.parentNode.parentNode).find('textarea').val(text);
            } else
                $(iframesDoctor[i].parentNode.parentNode.parentNode.parentNode).find('textarea').val('');
        }
    }
    // }
    //NURSE {
    CKEDITOR.replace('Nurse[education]');
    CKEDITOR.replace('Nurse[specialization]');
    CKEDITOR.replace('Nurse[expirience]');
    //call when form submit
    var crutchForNurseCKEditor = function(){
        var iframesNurse = $('.displayNurse iframe');
        for(var i=0;i<iframesNurse.length;i++) {
            var text = iframesNurse[i].contentDocument.childNodes[1].childNodes[1].childNodes[0].innerHTML;
            if (text != '<br>') {
                $(iframesNurse[i].parentNode.parentNode.parentNode.parentNode).find('textarea').val(text);
            } else
                $(iframesNurse[i].parentNode.parentNode.parentNode.parentNode).find('textarea').val('');
        }
    }
    // }
    //CLINIC {
    CKEDITOR.replace('Clinic[specialization]');
    //call when form submit
    var crutchForClinicCKEditor = function(){
        var iframesClinic = $('.displayClinic iframe');
        for(var i=0;i<iframesClinic.length;i++) {
            var text = iframesClinic[i].contentDocument.childNodes[1].childNodes[1].childNodes[0].innerHTML;
            if (text != '<br>') {
                $(iframesClinic[i].parentNode.parentNode.parentNode.parentNode).find('textarea').val(text);
            } else
                $(iframesClinic[i].parentNode.parentNode.parentNode.parentNode).find('textarea').val('');
        }
    }
    // }

    window.onload = function () {
        var inputs = document.querySelectorAll('#user-registration input')
        for (i = 0; i < inputs.length; i++) {
            if ((inputs[i].className.indexOf('button') == -1) && (inputs[i].type != 'radio'))
                inputs[i].value = '';
        }
        /*$('[data-alias=metro]').multiselect({
            includeSelectAllOption: true
        });*/
    }
</script>
