<div id="postAdvert" class="col-md-10">
    <div class="row form-group">
        <div class="col-md-2">
            Категория
        </div>
        <div class="col-md-push-1 col-md-2">
            Дата
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-2">
            <?php
            echo CHtml::dropDownList('categories', '', CHtml::listData(Category::model()->findAll(), 'id', 'name', 'category'), array(
                'id' => 'inputCategories',
                'class' => 'fieldInput',
                'prompt'=>'Не выбрана'
            ));
            ?>
        </div>
        <div class="col-md-push-1 col-md-2">
            <?php
            $year = date('Y');
            $month = date('m');
            $day = date('d');
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'name' => 'time',
                'model' => $medbooks,
                'attribute' => 'time',
                'language' => 'ru',
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                    'class' => 'fieldInput',
                    'showAnim' => 'fold',
                ),
                'htmlOptions' => array(
                    'class' => 'fieldInput',
                    'id' => 'dateMedbook',
                ),
                'value' => $year . '-'.$month.'-'.$day
            )); ?>
        </div>
        <div class="col-md-push-2 col-md-2">
            <input class="button1 buttonFilter bg-blue" type="button" value="Фильтровать" onclick="loadMedbook();">
        </div>
        <?php if(!Yii::app()->user->isGuest && User::model()->findByPk(Yii::app()->user->id)->role == 'admin') { ?>
        <div class="col-md-push-2 col-md-2">
            <input class="button1 buttonFilter bg-blue" type="button" value="Добавить" onclick="addMedbook();">
        </div>
        <?php } ?>
    </div>
	<span id="listOfMedbook">
	<?php foreach($medbooks as $key){
        $day = date('d',strtotime($key->time));
        $month = date('M',strtotime($key->time));
        $year = date('Y',strtotime($key->time));
        switch ($month) {
            case 'Jan':
                $month = 'января';
                break;
            case 'Feb':
                $month = 'февраля';
                break;
            case 'Mar':
                $month = 'марта';
                break;
            case 'Apr':
                $month = 'апреля';
                break;
            case 'May':
                $month = 'мая';
                break;
            case 'Jun':
                $month = 'июня';
                break;
            case 'Jul':
                $month = 'июля';
                break;
            case 'Aug':
                $month = 'августа';
                break;
            case 'Sep':
                $month = 'сентября';
                break;
            case 'Oct':
                $month = 'октября';
                break;
            case 'Nov':
                $month = 'ноября';
                break;
            case 'Dec':
                $month = 'декабря';
                break;
        }
        $name = 'Администратор';
        ?>
        <div class="row article" id="medbook<?= $key->id; ?>" data-id="<?= $key->id; ?>" onclick="showMedbook(event, this)">
            <div class="col-md-3">
                <?php if($key->image) $image = Yii::app()->request->baseUrl.'/upload/medbook/' . $key->image; else $image = Yii::app()->request->baseUrl.'/assets/images/blankPhoto1.png'; ?>
                <img class="article-img" src="<?= $image; ?>">
            </div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-11 article-author"><?php echo $name; ?></div>
                    <div class="col-md-1 article-author"><?php if(!Yii::app()->user->isGuest && User::model()->findByPk(Yii::app()->user->id)->role == 'admin') { ?><i class="fa fa-pencil-square-o" data-id="<?= $key->id; ?>" onclick="editMedbook(event, this)"></i><i data-id="<?= $key->id; ?>" class="fa fa-times lml" onclick="removeMedbook(event, this)"></i><?php } ?></div>
                </div>
                <div class="row form-group">
                    <div class="col-md-8 article-head no-padding"><?php echo $key->name; ?></div>
                    <div class="col-md-4 text-r"><?php echo $day . ' ' . $month . ' ' . $year; ?></div>
                </div>
                <div class="row article-text"><?php echo $key->text; ?></div>
            </div>
        </div>
    <?php } ?>
	</span>
</div>
<input id="userId" type="hidden" value=<? if(!Yii::app()->user->isGuest) echo User::model()->findByPk(Yii::app()->user->id)->role; ?>>
<script>
    var fixPath = (location.href.indexOf('localhost')!=-1) ? 'medportal/' : '' ;

    function showMedbook(event, el){
        var id = el.getAttribute('data-id');
        location.href = 'http://' + location.hostname + '/' + fixPath + 'medbook/' + id;
    }

    function addMedbook(){
        location.href = 'http://' + location.hostname + '/' + fixPath + 'medbook/add';
    }

    function editMedbook(event, el){
        event = event || window.event;
        event.stopPropagation();
        var id = el.getAttribute('data-id');
        location.href = 'http://' + location.hostname + '/' + fixPath + 'medbook/edit/' + id;
    }

    function removeMedbook(event, el){
        event = event || window.event;
        event.stopPropagation();
        var id = el.getAttribute('data-id');
        $('#preload').css('display','block');
        $.ajax({
            url : 'http://' + location.hostname + '/' + fixPath + 'medbook/remove/'+ id,
            method : 'GET',
            success : function(response){
                $('#medbook'+id).fadeOut();
            },
            error : function(data){
                console.log(data);
            },
            complete : function(){
                $('#preload').css('display','none');
            }
        });
    }

    function loadMedbook(){
        var idCategory = $('#inputCategories').val();
        var medbookDate = $('#dateMedbook').val();
        if(!idCategory)
            idCategory = -1;
        $('#preload').css('display','block');
        $.ajax({
            url : 'http://' + location.hostname + '/' + fixPath + 'medbook/getMedbook',
            method : 'POST',
            data : 'idCategory=' + idCategory + '&medbookDate=' + medbookDate,
            success : function(responce){
                $('#listOfMedbook').html('');
                if(!responce){
                    return;
                }
                var data = JSON.parse(responce);
                var html = '';
                if(data.length == 0){
                    html += '<div class="row">';
                    html += '<div class="col-md-12 text-center article-head">Ничего не найдено</div>';
                    html += '</div>';
                }
                for(var i=0;i<data.length;i++){
                    var medbookDate = data[i].time;
                    medbookDate = medbookDate.split(' ')[0];
                    var medbookDateYear = medbookDate.split('-')[0];
                    var medbookDateMonth = medbookDate.split('-')[1];
                    var medbookDateDay = medbookDate.split('-')[2];
                    switch (medbookDateMonth) {
                        case '01':
                            medbookMonth = 'января'
                            break;
                        case '02':
                            medbookMonth = 'февраля'
                            break;
                        case '03':
                            medbookMonth = 'марта'
                            break;
                        case '04':
                            medbookMonth = 'апреля'
                            break;
                        case '05':
                            medbookMonth = 'мая'
                            break;
                        case '06':
                            medbookMonth = 'июня'
                            break;
                        case '07':
                            medbookMonth = 'июля'
                            break;
                        case '08':
                            medbookMonth = 'августа'
                            break;
                        case '09':
                            medbookMonth = 'сентября'
                            break;
                        case '10':
                            medbookMonth = 'октября'
                            break;
                        case '11':
                            medbookMonth = 'ноября'
                            break;
                        case '12':
                            medbookMonth = 'декабря'
                            break;
                    }
                    html += '<div class="row article" id="medbook'+ data[i].id +'" data-id="'+ data[i].id +'" onclick="showMedbook(event, this)">';
                    html += '<div class="col-md-3">';
                    var pic = 'http://' + location.hostname + '/' + fixPath + 'assets/images/blankPhoto1.png';
                    if(data[i].image != '')
                        pic = 'http://' + location.hostname + '/' + fixPath + 'upload/medbook/' + data[i].image;
                    html += '<img class="article-img" src="' + pic + '">';
                    html += '</div>';
                    html += '<div class="col-md-9">';
                    html += '<div class="row">';
                    html += '<div class="col-md-11 article-author">' + data[i].author + '</div>';
                    if($('#userId').val() == 'admin')
                        html += '<div class="col-md-1 article-author"><i class="fa fa-pencil-square-o" data-id="'+data[i].id+'" onclick="editMedbook(event, this)"></i><i data-id="'+data[i].id+'" class="fa fa-times lml" onclick="removeMedbook(event, this)"></i></div>';
                    else
                        html += '<div class="col-md-1 article-author"></div>';
                    html += '</div>';
                    html += '<div class="row form-group">';
                    html += '<div class="col-md-8 article-head no-padding">'+ data[i].name +'</div>';
                    html += '<div class="col-md-4 text-r">'+ medbookDateDay + ' ' + medbookMonth + ' ' + medbookDateYear + '</div>';
                    html += '</div>';
                    html += '<div class="row article-text">'+ data[i].text +'</div>';
                    html += '</div>';
                    html += '</div>';
                }
                $('#listOfMedbook').html(html);

            },
            error : function(data){
                console.log(data);
            },
            complete : function(){
                $('#preload').css('display','none');
            }
        });
    }

</script>