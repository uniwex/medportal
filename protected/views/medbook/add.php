<div id="postAdvert" class="col-md-10">
    <?php $formMedbook = $this->beginWidget("CActiveForm", array(
        'id' => 'add-medbook',
        'action' => Yii::app()->request->baseUrl . '/medbook/saveNew',
        'method' => 'post',
//        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true
        ),
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
            'autocomplete' => 'off',
            'onsubmit' => 'crutchForCKEditor();return true',
        ),
    )); ?>
    <div class="form-group row">
        <div class="col-md-2">
            <?= $formMedbook->labelEx($medbookModel, 'name') ?>
        </div>
        <div class="col-md-8">
            <?= $formMedbook->textField($medbookModel, 'name', array('class' => 'fieldInput', 'placeholder' => 'Новая статья')); ?>
            <?= $formMedbook->error($medbookModel, 'name', array('class' => 'text-red')) ?>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-2">
            <?= $formMedbook->labelEx($medbookModel, 'image'); ?>
        </div>
        <div class="col-md-8">
            <div id="image" class="fieldInput">
                <?= $formMedbook->fileField($medbookModel, 'image'); ?>
            </div>
            <?= $formMedbook->labelEx($medbookModel, 'Размер картинки не может превышать 5 MB') ?>
            <?= $formMedbook->error($medbookModel, 'image', array('class' => 'text-red')) ?>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-2">
            Категория
        </div>
        <div class="col-md-8">
            <?= $formMedbook->dropDownList($categoryModel, 'name', CHtml::listData(Category::model()->findAll(), 'id', 'name', 'category'), array('class' => 'fieldInput', 'multiple' => 'multiple')); ?>
            <?= $formMedbook->error($categoryModel, 'name', array('class' => 'text-red')) ?>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-2">
            <?= $formMedbook->labelEx($medbookModel, 'text') ?>
        </div>
        <div class="col-md-8">
            <?= $formMedbook->textArea($medbookModel, 'text', array('class' => 'fieldInput')); ?>
            <?= $formMedbook->error($medbookModel, 'text', array('class' => 'text-red')) ?>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <?= CHtml::submitButton('Сохранить статью', array('class' => 'button1 button1reg', 'id'=>'medbookSubmit')); ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
<script src="//cdn.ckeditor.com/4.5.4/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('Medbook[text]');
    var crutchForCKEditor = function(){
        var abc = document.getElementsByTagName('iframe')[0].contentDocument;
        var abc2 = abc.childNodes[1];
        var text = abc2.childNodes[1].childNodes[0].innerHTML;
        if(text != '<br>')
            $('#Medbook_text').val(text);
    }
</script>