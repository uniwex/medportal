<div id="postAdvert" class="col-md-10">
    <?php $formMedbook = $this->beginWidget("CActiveForm", array(
        'id' => 'edit-medbook',
        'action' => Yii::app()->request->baseUrl . '/medbook/update',
        'method' => 'post',
//        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true
        ),
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
            'autocomplete' => 'off',
//            'onsubmit' => 'return true',
        ),
    )); ?>
    <div class="form-group row">
        <div class="col-md-2">
            <?= $formMedbook->labelEx($medbookModel, 'name') ?>
        </div>
        <div class="col-md-8">
            <?= $formMedbook->textField($medbookModel, 'name', array('class' => 'fieldInput', 'placeholder' => 'О сайте')); ?>
            <?= $formMedbook->error($medbookModel, 'name', array('class' => 'text-red')) ?>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-2">
            <?= $formMedbook->labelEx($medbookModel, 'image'); ?>
        </div>
        <div class="col-md-8">
            <?php
            if($medbookModel->image) $image = Yii::app()->request->baseUrl.'/upload/medbook/' . $medbookModel->image; else $image = Yii::app()->request->baseUrl.'/assets/images/blankPhoto1.png';
            ?>
            <div id="image" class="fieldInput" <?= 'style="background-image:url('.$image.')"'; ?>)>
                <?= $formMedbook->fileField($medbookModel, 'image'); ?>
            </div>
            <?= $formMedbook->labelEx($medbookModel, 'Размер картинки не может превышать 5 MB') ?>
            <?= $formMedbook->error($medbookModel, 'image', array('class' => 'text-red')) ?>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-2">
            Категория
        </div>
        <div class="col-md-8">
            <?php
            array($medbookModel->idCategory => array('selected'=>true));

            $arrayIdCategories = explode(',',$medbookModel->idCategory);
            $arrayCategories = array();

            for($i=0;$i<count($arrayIdCategories);$i++){
                $arrayCategories[$arrayIdCategories[$i]] = array('selected'=>true);
            }
            ?>
            <?= $formMedbook->dropDownList($categoryModel, 'name', CHtml::listData(Category::model()->findAll(), 'id', 'name', 'category'), array('class' => 'fieldInput', 'multiple' => 'multiple', 'options' => $arrayCategories)); ?>
            <?= $formMedbook->error($categoryModel, 'name', array('class' => 'text-red')) ?>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-2">
            <?= $formMedbook->labelEx($medbookModel, 'text') ?>
        </div>
        <div class="col-md-8">
            <?= $formMedbook->textArea($medbookModel, 'text', array('class' => 'fieldInput')); ?>
            <?= $formMedbook->error($medbookModel, 'text', array('class' => 'text-red')) ?>
        </div>
    </div>
    <?= $formMedbook->hiddenField($medbookModel, 'id', array('class' => 'fieldInput', 'value'=>$medbookModel->id)); ?>
    <div class="form-group row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <?= CHtml::submitButton('Сохранить статью', array('class' => 'button1 button1reg')); ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
<script src="//cdn.ckeditor.com/4.5.4/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('Medbook[text]');
</script>