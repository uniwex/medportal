<div class="right-menu-relative" xmlns="http://www.w3.org/1999/html">
    <? require ("protected/views/site/rightMenu.php"); ?>
</div>
<div class="center-overview">
    <div class="over-desc">
        <p>Виртуальный тур позволит Вам совершить ознакомительную прогулку по клиникам, не выходя из</p>
        <p>дома. Ощутите эффект присутствия, рассмотрите в деталях кабинеты врачей, узнайте о </p>
        <p>подробностях внутреннего обитания. Вашему вниманию как привычные 3D-туры, так и</p>
        <p>панорамные видео-экскурсии. Используйте уникальную возможность виртуально посетить</p>
        <p>клиники прямо сейчас!</p>
    </div>
    <div id="over-table">
        <table class="table">
            <tr id="over-table-colls">
                <th>Клиники, в которых доступны 3D-туры</th>
                <th>Виртуальный тур</th>
                <th>Панорамная экскурсия</th>
            </tr>
            <tbody>
                <? foreach ($model as $key=>$item) { ?>
                    <tr>
                        <td id="over-clinics">
                            <a href="/clinicView/<?=$item->idUser;?>" class="over-table-text"><?=$key+1;?>. <?=$item->name;?></a>
                        </td>
                        <td id="ddd-tour" class="over-variable">
                            <a href="#" data-src="<?=$item->virtual;?>" class="over-table-text"><div class="ddd-icon"></div>открыть</a>
                        </td>
                        <td id="video-tour" class="over-variable">
                            <a href="#" data-videosrc="https://www.youtube.com/embed/<?=$item->panorama;?>" class="over-table-text"><div class="play-icon"></div>воспроизвести</a>
                        </td>
                    </tr>
                <?}?>
            </tbody>
        </table>
        <div class="modal fade bs-example-modal-lg" id="youtube-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <iframe width="100%" height="480" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(function() {
                $('#video-tour>a').on('click', function() {
                    var src = $(this).data('videosrc');
                    $('#youtube-modal iframe').attr('src', src);
                    $('#youtube-modal').modal('show');
                });
            });
            $('#youtube-modal').on('hidden.bs.modal', function () {
                $('#youtube-modal iframe').removeAttr('src');
            })
        </script>
        <div class="modal fade bs-example-modal-lg" id="ddd-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <iframe width="100%" height="480" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(function() {
                $('#ddd-tour>a').on('click', function() {
                    var src = $(this).data('src');
                    $('#ddd-modal iframe').attr('src', src);
                    $('#ddd-modal').modal('show');
                });
            });
            $('ddd-modal').on('hidden.bs.modal', function () {
                $('#ddd-modal iframe').removeAttr('src');
            })
        </script>
    </div>
</div>