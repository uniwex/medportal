<?php
if(isset($data)) {
    ?>

    <div class="row article">
        <div class="col-md-2 col-xs-2 no-padding">
            <?php
            $image = Yii::app()->request->baseUrl.'/assets/images/articles-overlay.png';
            if(isset($data->image))
                if($data->image != '')
                    $image = Yii::app()->request->baseUrl.'/upload/categories/' . $data->image;
            ?>
            <div class="article-img" style="background-image:url('<?= $image; ?>');background-size: 160% auto;"></div>
        </div>
        <div class="col-md-10 col-xs-8">
            <div class="row form-group">
                <a href="/articles/list/<?=$data->id?>">
                    <div class="col-md-12 col-xs-12 article-head no-padding" style="padding-left: 14px;">
                        <?= $data->name ?>
                    </div>
                </a>
            </div>
            <div class="row">
                <div class="col-md-12 col-xs-12 category-text no-padding" style="padding-left: 14px;">
                    <?= $data->description ?>
                </div>
            </div>
            <div class="row">
                <a href="/articles/list/<?=$data->id?>">
                    <div class="col-md-12 col-xs-12 article-head no-padding text-r" style="font-size: 14px;">
                        Перейти в раздел >
                    </div>
                </a>
            </div>
        </div>
    </div>

<?php } ?>