<?php
/* @var $this QuizController */
/* @var $model Quiz */

$this->breadCrumbs=array(
	'Quizs'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Quiz', 'url'=>array('index')),
array('label'=>'Manage Quiz', 'url'=>array('admin')),
);
?>

<h1>Добавить розыгрыш</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>