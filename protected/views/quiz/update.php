<?php
    /* @var $this QuizController */
    /* @var $model Quiz */

$this->breadCrumbs=array(
	'Quizs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

    $this->menu=array(
    array('label'=>'Список розыгрышей', 'url'=>array('index')),
    array('label'=>'Создать розыгрыш', 'url'=>array('create')),
    array('label'=>'Просмотр розыгрышей', 'url'=>array('view', 'id'=>$model->id)),
    array('label'=>'Управление розыгрышами', 'url'=>array('admin')),
    );
    ?>

    <h1>Обновить розыгрыш <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>