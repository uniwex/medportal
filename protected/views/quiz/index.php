<?php
/* @var $this QuizController */
/* @var $dataProvider CActiveDataProvider */

$this->breadCrumbs=array(
	'Quizs',
);

$this->menu=array(
array('label'=>'Создать розыгрыш', 'url'=>array('create')),
array('label'=>'Управление розыгрышами', 'url'=>array('admin')),
);
?>

<h1>Розыгрыши</h1>

<?php $this->widget('zii.widgets.CListView', array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
