<?php
/* @var $this QuizController */
/* @var $model Quiz */

$this->breadCrumbs=array(
	'Quizs'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'Список розыгрышей', 'url'=>array('index')),
array('label'=>'Создать розыгрыш', 'url'=>array('create')),
array('label'=>'Редактировать розыгрыш', 'url'=>array('update', 'id'=>$model->id)),
array('label'=>'Удалить розыгрыш', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Управление розыгрышами', 'url'=>array('admin')),
);
?>

<h1>Просмотр розыгрыша #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
'data'=>$model,
'attributes'=>array(
		'id',
		'date',
),
)); ?>
