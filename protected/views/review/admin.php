<?php
/* @var $this ReviewController */
/* @var $model Review */


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$('#review-grid').yiiGridView('update', {
data: $(this).serialize()
});
return false;
});
");
?>


<div class="row">
    <div class="col-md-12">

        <div class="box box-info">
            <div class="box-header with-border ui-sortable-handle">
                <h3 class="box-title">Список врачей</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <?php $this->widget('zii.widgets.grid.CGridView', array(
                    'id' => 'review-grid',
                    'dataProvider' => $model->search(),
                    'filter' => $model,
                    'cssFile' => Yii::app()->request->getBaseUrl(true) . '/assets/admin/css/gridview.css',
                    'itemsCssClass' => 'table no-margin',
                    'htmlOptions' => array(
                        'class' => 'table-responsive'
                    ),
                    'summaryText' => 'Показано записей: {start} - {end} из {count}',
                    'emptyText' => 'Результаты не найдены',
                    'columns' => array(
                        'id',

                        array(
                            'name' => 'idUserFrom',
                            'value' => 'User::getName($data->idUserFrom)'
                        ),
                        array(
                            'name' => 'idUserTo',
                            'value' => 'User::getName($data->idUserFrom)'
                        ),
                        'name',
                        'text',
                        'cure',
                        array(
                            'name' => 'moderate',
                            'value' => '$data->moderate == 1 ? CHtml::link(CHtml::encode("отклонить"),
                            array("/review/modered?status=".$data->moderate."&id=".$data->id)):
                            CHtml::link(CHtml::encode("одобрить"),
                            array("/review/modered?status=".$data->moderate."&id=".$data->id))',

                            'cssClassExpression'=>'$data->moderate != 1 ? "btn btn-warning stat" : "btn btn-info stat"',
                            'type' => 'raw',
                            'filter' => array(
                                '0' => 'Не одобренные',
                                '1' => 'Одобренные',
                            )
                        ),
                        /*
                        'regard',
                        'qualification',
                        'time',
                        'pics',
                        */

                        array(
                            'header' => 'Операции',
                            'class' => 'CButtonColumn',
                        ),
                    ),
                )); ?>
            </div>
            <div class="box-footer clearfix"></div>
        </div>
    </div>
</div>