<?php
/* @var $this ReviewController */
/* @var $model Review */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    )); ?>

    <div class="row">
        <?php echo $form->label($model, 'id'); ?>
        <?php echo $form->textField($model, 'id'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'idUserFrom'); ?>
        <?php echo $form->textField($model, 'idUserFrom'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'idUserTo'); ?>
        <?php echo $form->textField($model, 'idUserTo'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'name'); ?>
        <?php echo $form->textArea($model, 'name', array('rows' => 6, 'cols' => 50)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'text'); ?>
        <?php echo $form->textArea($model, 'text', array('rows' => 6, 'cols' => 50)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'cure'); ?>
        <?php echo $form->textField($model, 'cure'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'regard'); ?>
        <?php echo $form->textField($model, 'regard'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'qualification'); ?>
        <?php echo $form->textField($model, 'qualification'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'time'); ?>
        <?php echo $form->textField($model, 'time'); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'moderate'); ?>
        <?php echo $form->textArea($model, 'moderate', array('rows' => 6, 'cols' => 50)); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'pics'); ?>
        <?php echo $form->textArea($model, 'pics', array('rows' => 6, 'cols' => 50)); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->