<div class="right-menu-relative">
    <? require ("protected/views/site/rightMenu.php"); ?>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<div class="center-relative">
    <p class="text-relative">В предложенной ниже форме выберите страны, чтобы сравнить их между собой</p>
    <div class="col-md-6">
        <select id="left-country" class="country-choose">
            <option selected id="0">Выбрать страну...</option>
            <?
            $result = LightArticle::model()->findAll();

            foreach ($result as $item) {
                ?><option about="<?print_r($item->idCountry);?>" id="<?print_r($item->id);?>"><?print_r($item->name);?></option> <?
            }
            ?>
        </select>
        <div id="left-article" class="country-article"></div>
    </div>
    <div class="col-md-6">
        <select id="right-country" class="country-choose">
            <option selected id="0">Выбрать страну...</option>
            <?
            $result = LightArticle::model()->findAll();

            foreach ($result as $item) {
                ?><option about="<?print_r($item->idCountry);?>" id="<?print_r($item->id);?>"><?print_r($item->name);?></option> <?
            }
            ?>
        </select>
        <div id="right-article" class="country-article"></div>
    </div>
    <div class="col-md-12 desc-relative">
        <p class="text-relative">Для удобства поиска Вы можете воспользоваться выбором страны
            <a href="/relative/alphabet">по алфавиту</a>.</p>
        <p class="text-relative">Если Вас интересует, в какой стране лечат конкретное заболевание, используйте поиск</p>
        <p class="text-relative">по <a href="/relative/directions">популярным направлениям медицинского туризма</a>.</p>
    </div>
</div>
<script type="text/javascript" src="../../../assets/scripts/relative.js"></script>