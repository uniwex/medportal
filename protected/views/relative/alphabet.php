<div class="right-menu-relative">
    <? require ("protected/views/site/rightMenu.php"); ?>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<div class="center-relative">
    <p class="text-relative">Выбрать страну по алфавиту</p>
    <div class="col-md-3 one-string">
        <p class="text-relative alphabet-string">А</p>
        <p><a href="#" class="text-relative">Австралия</a></p>
        <p><a href="/articles/7" class="text-relative">Австрия</a></p>
        <p><a href="/articles/105" class="text-relative">Азербайджан</a></p>
        <p><a href="/articles/102" class="text-relative">Албания</a></p>
        <p><a href="#" class="text-relative">Андорра</a></p>
        <p><a href="/articles/62" class="text-relative">Аргентина</a></p>
        <p><a href="/articles/104" class="text-relative">Армения</a></p>

        <p class="text-relative alphabet-string">Б</p>
        <p><a href="/articles/89" class="text-relative">Беларусь</a></p>
        <p><a href="/articles/96" class="text-relative">Бельгия</a></p>
        <p><a href="/articles/95" class="text-relative">Болгария</a></p>
        <p><a href="/articles/99" class="text-relative">Босния и Герцеговина</a></p>
        <p><a href="/articles/63" class="text-relative">Бразилия</a></p>

        <p class="text-relative alphabet-string">В</p>
        <p><a href="/articles/8" class="text-relative">Великобритания</a></p>
        <p><a href="/articles/10" class="text-relative">Венгрия</a></p>
        <p><a href="#" class="text-relative">Венесуэла</a></p>
        <p><a href="/articles/106" class="text-relative">Вьетнам</a></p>

        <p class="text-relative alphabet-string">Г</p>
        <p><a href="/articles/9" class="text-relative">Германия</a></p>
        <p><a href="/articles/94" class="text-relative">Греция</a></p>
        <p><a href="/articles/103" class="text-relative">Грузия</a></p>

        <p class="text-relative alphabet-string">Д</p>
        <p><a href="/articles/81" class="text-relative">Дания</a></p>
        <p><a href="/articles/50" class="text-relative">Доминикана</a></p>
    </div>
    <div class="col-md-3 one-string">


        <p class="text-relative alphabet-string">И</p>
        <p><a href="/articles/11" class="text-relative">Израиль</a></p>
        <p><a href="/articles/64" class="text-relative">Индия</a></p>
        <p><a href="#" class="text-relative">Индонезия</a></p>
        <p><a href="/articles/101" class="text-relative">Ирландия</a></p>
        <p><a href="/articles/82" class="text-relative">Исландия</a></p>
        <p><a href="/articles/65" class="text-relative">Испания</a></p>
        <p><a href="/articles/91" class="text-relative">Италия</a></p>

        <p class="text-relative alphabet-string">К</p>
        <p><a href="/articles/26" class="text-relative">Казахстан</a></p>
        <p><a href="/articles/66" class="text-relative">Канада</a></p>
        <p><a href="/articles/12" class="text-relative">Китай</a></p>
        <p><a href="#" class="text-relative">Колумбия</a></p>
        <p><a href="/articles/56" class="text-relative">Коста-Рика</a></p>
        <p><a href="#" class="text-relative">Кипр</a></p>
        <p><a href="/articles/43" class="text-relative">Куба</a></p>

        <p class="text-relative alphabet-string">Л</p>
        <p><a href="/articles/84" class="text-relative">Латвия</a></p>
        <p><a href="/articles/83" class="text-relative">Литва</a></p>

        <p class="text-relative alphabet-string">М</p>
        <p><a href="/articles/100" class="text-relative">Македония</a></p>
        <p><a href="/articles/67" class="text-relative">Малайзия</a></p>
        <p><a href="#" class="text-relative">Мальта</a></p>
        <p><a href="/articles/68" class="text-relative">Марокко</a></p>
        <p><a href="/articles/25" class="text-relative">Мексика</a></p>
    </div>
    <div class="col-md-3 one-string">
        <p class="text-relative alphabet-string">Н</p>
        <p><a href="/articles/97" class="text-relative">Нидерланды</a></p>
        <p><a href="#" class="text-relative">Новая Зеландия</a></p>
        <p><a href="/articles/85" class="text-relative">Норвегия</a></p>

        <p class="text-relative alphabet-string">О</p>
        <p><a href="/articles/69" class="text-relative">ОАЭ</a></p>

        <p class="text-relative alphabet-string">П</p>
        <p><a href="#" class="text-relative">Панама</a></p>
        <p><a href="/articles/57" class="text-relative">Польша</a></p>
        <p><a href="/articles/93" class="text-relative">Португалия</a></p>

        <p class="text-relative alphabet-string">Р</p>
        <p><a href="/articles/92" class="text-relative">Румыния</a></p>

        <p class="text-relative alphabet-string">С</p>
        <p><a href="/articles/13" class="text-relative">Сингапур</a></p>
        <p><a href="/articles/98" class="text-relative">Словакия</a></p>
        <p><a href="/articles/14" class="text-relative">США</a></p>

        <p class="text-relative alphabet-string">Т</p>
        <p><a href="#" class="text-relative">Тайвань</a></p>
        <p><a href="/articles/42" class="text-relative">Таиланд</a></p>
        <p><a href="/articles/70" class="text-relative">Тунис</a></p>
        <p><a href="/articles/15" class="text-relative">Турция</a></p>

        <p class="text-relative alphabet-string">У</p>
        <p><a href="/articles/90" class="text-relative">Украина</a></p>
        <p><a href="#" class="text-relative">Уругвай</a></p>

        <p class="text-relative alphabet-string">Ф</p>
        <p><a href="/articles/20" class="text-relative">Филиппины</a></p>
        <p><a href="/articles/79" class="text-relative">Финляндия</a></p>
        <p><a href="/articles/19" class="text-relative">Франция</a></p>
    </div>
    <div class="col-md-3 one-string">
        <p class="text-relative alphabet-string">Х</p>
        <p><a href="/articles/71" class="text-relative">Хорватия</a></p>

        <p class="text-relative alphabet-string">Ч</p>
        <p><a href="#" class="text-relative">Чехия</a></p>
        <p><a href="#" class="text-relative">Чили</a></p>

        <p class="text-relative alphabet-string">Ш</p>
        <p><a href="/articles/16" class="text-relative">Швейцария</a></p>
        <p><a href="/articles/80" class="text-relative">Швеция</a></p>

        <p class="text-relative alphabet-string">Э</p>
        <p><a href="/articles/88" class="text-relative">Эстония</a></p>

        <p class="text-relative alphabet-string">Ю</p>
        <p><a href="/articles/72" class="text-relative">ЮАР</a></p>
        <p><a href="/articles/17" class="text-relative">Южная Корея</a></p>

        <p class="text-relative alphabet-string">Я</p>
        <p><a href="/articles/18" class="text-relative">Япония</a></p>
    </div>
</div>