<div class="right-menu-relative">
    <? require ("protected/views/site/rightMenu.php"); ?>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<div class="center-relative">
    <p class="text-relative left-none">География медицинского и оздоровительного туризма все больше расширяется с каждым</p>
    <p class="text-relative left-none">годом. Увеличивается выбор стран, медицинских и оздоровительных учреждений, а так же</p>
    <p class="text-relative left-none">предоставляемых ими услуг. Однако на сегодняшний день уже можно выделить самые </p>
    <p class="text-relative left-none">популярные направления и страны в индустрии медицинского туризма</p>
    <div class="table-relative">
        <table class="table">
            <caption class="text-relative">Онкология</caption>
            <tbody>
                <tr>
                    <td class="td-relative">
                        Ранняя диагностика и уточнение диагноза
                    </td>
                    <td class="td-relative">
                        <a href="/articles/11" class="text-relative">Израиль</a>
                        <a href="/articles/9" class="text-relative">Германия</a>
                        <a href="/articles/7" class="text-relative">Австрия</a>
                        <a href="/articles/14" class="text-relative">США</a>
                        <a href="/articles/67" class="text-relative">Малайзия</a>
                        <a href="/articles/13" class="text-relative">Сингапур</a>
                        <a href="/articles/66" class="text-relative">Канада</a>
                        <a href="/articles/80" class="text-relative">Швеция</a>
                        <a href="/articles/69" class="text-relative">ОАЭ</a>
                        <a href="/articles/17" class="text-relative">Южная Корея</a>
                        <a href="/articles/18" class="text-relative">Япония</a>
                        <a href="#" class="text-relative">Словения</a>
                        <a href="/articles/10" class="text-relative">Венгрия</a>
                    </td>
                </tr>
                <tr>
                    <td class="td-relative">
                        Хирургическое лечение злокачественных новообразований (в различных органах и тканях)
                    </td>
                    <td class="td-relative">
                        <a href="/articles/11" class="text-relative">Израиль</a>
                        <a href="/articles/67" class="text-relative">Малайзия</a>
                        <a href="/articles/14" class="text-relative">США</a>
                        <a href="/articles/43" class="text-relative">Куба</a>
                        <a href="/articles/56" class="text-relative">Коста-Рика</a>
                        <a href="/articles/13" class="text-relative">Сингапур</a>
                        <a href="/articles/80" class="text-relative">Швеция</a>
                        <a href="/articles/7" class="text-relative">Австрия</a>
                        <a href="/articles/20" class="text-relative">Филиппины</a>
                        <a href="/articles/17" class="text-relative">Южная Корея</a>
                        <a href="/articles/42" class="text-relative">Таиланд</a>
                        <a href="/articles/15" class="text-relative">Турция</a>
                        <a href="#" class="text-relative">Словения</a>
                        <a href="/articles/10" class="text-relative">Венгрия</a>
                    </td>
                </tr>
                <tr>
                    <td class="td-relative">
                        Детская онкология
                    </td>
                    <td class="td-relative">
                        <a href="/articles/11" class="text-relative">Израиль</a>
                        <a href="/articles/43" class="text-relative">Куба</a>
                        <a href="/articles/17" class="text-relative">Южная Корея</a>
                        <a href="/articles/20" class="text-relative">Филиппины</a>
                        <a href="/articles/67" class="text-relative">Малайзия</a>
                        <a href="/articles/14" class="text-relative">США</a>
                        <a href="/articles/56" class="text-relative">Коста-Рика</a>
                        <a href="/articles/7" class="text-relative">Австрия</a>
                        <a href="/articles/13" class="text-relative">Сингапур</a>
                        <a href="/articles/80" class="text-relative">Швеция</a>
                        <a href="#" class="text-relative">Словения</a>
                    </td>
                </tr>
                <tr>
                    <td class="td-relative">
                        Лечений онкологических заболеваний крови (онкогематология)
                    </td>
                    <td class="td-relative">
                        <a href="/articles/11" class="text-relative">Израиль</a>
                        <a href="/articles/43" class="text-relative">Куба</a>
                        <a href="/articles/17" class="text-relative">Южная Корея</a>
                        <a href="/articles/20" class="text-relative">Филиппины</a>
                        <a href="/articles/14" class="text-relative">США</a>
                        <a href="/articles/7" class="text-relative">Австрия</a>
                        <a href="/articles/80" class="text-relative">Швеция</a>
                        <a href="#" class="text-relative">Словения</a>
                    </td>
                </tr>
                <tr>
                    <td class="td-relative">
                        Радиология и химиотерапия
                    </td>
                    <td class="td-relative">
                        <a href="/articles/11" class="text-relative">Израиль</a>
                        <a href="/articles/42" class="text-relative">Таиланд</a>
                        <a href="/articles/17" class="text-relative">Южная Корея</a>
                        <a href="/articles/43" class="text-relative">Куба</a>
                        <a href="/articles/7" class="text-relative">Австрия</a>
                        <a href="/articles/15" class="text-relative">Турция</a>
                        <a href="#" class="text-relative">Словения</a>
                        <a href="#" class="text-relative">Новая Зеландия</a>
                        <a href="/articles/13" class="text-relative">Сингапур</a>
                        <a href="#" class="text-relative">Мальта</a>
                        <a href="/articles/69" class="text-relative">ОАЭ</a>
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="table table-next-relative">
            <caption class="text-relative">Стоматология</caption>
            <tbody>
                <tr>
                    <td class="td-relative">
                        Эстетическая стоматология (металлокерамическое протезирование, виниры, вкладки и др.)
                    </td>
                    <td class="td-relative">
                        <a href="/articles/10" class="text-relative">Венгрия</a>
                        <a href="/articles/56" class="text-relative">Коста-Рика</a>
                        <a href="/articles/42" class="text-relative">Таиланд</a>
                        <a href="/articles/15" class="text-relative">Турция</a>
                        <a href="/articles/67" class="text-relative">Малайзия</a>
                        <a href="#" class="text-relative">Колумбия</a>
                        <a href="/articles/57" class="text-relative">Польша</a>
                        <a href="/articles/98" class="text-relative">Словакия</a>
                        <a href="#" class="text-relative">Кипр</a>
                        <a href="/articles/93" class="text-relative">Португалия</a>
                        <a href="/articles/65" class="text-relative">Испания</a>
                        <a href="#" class="text-relative">Словения</a>
                        <a href="/articles/90" class="text-relative">Украина</a>
                        <a href="#" class="text-relative">Черногория</a>
                        <a href="/articles/16" class="text-relative">Швейцария</a>
                        <a href="/articles/69" class="text-relative">ОАЭ</a>
                        <a href="/articles/84" class="text-relative">Латвия</a>
                        <a href="/articles/83" class="text-relative">Литва</a>
                        <a href="/articles/100" class="text-relative">Македония</a>
                    </td>
                </tr>
                <tr>
                    <td class="td-relative">
                        Хирургическая стоматология. Имплантология
                    </td>
                    <td class="td-relative">
                        <a href="/articles/10" class="text-relative">Венгрия</a>
                        <a href="/articles/56" class="text-relative">Коста-Рика</a>
                        <a href="/articles/62" class="text-relative">Аргентина</a>
                        <a href="/articles/42" class="text-relative">Таиланд</a>
                        <a href="/articles/13" class="text-relative">Сингапур</a>
                        <a href="/articles/17" class="text-relative">Южная Корея</a>
                        <a href="/articles/57" class="text-relative">Польша</a>
                        <a href="/articles/15" class="text-relative">Турция</a>
                        <a href="/articles/71" class="text-relative">Хорватия</a>
                        <a href="/articles/100" class="text-relative">Македония</a>
                        <a href="#" class="text-relative">Кипр</a>
                        <a href="/articles/90" class="text-relative">Украина</a>
                        <a href="/articles/83" class="text-relative">Литва</a>
                        <a href="/articles/84" class="text-relative">Латвия</a>
                        <a href="#" class="text-relative">Мальта</a>
                        <a href="/articles/81" class="text-relative">Дания</a>
                        <a href="/articles/97" class="text-relative">Нидерланды</a>
                        <a href="#" class="text-relative">Новая Зеландия</a>
                    </td>
                </tr>
                <tr>
                    <td class="td-relative">
                        Эндодонтическое лечение
                    </td>
                    <td class="td-relative">
                        <a href="/articles/10" class="text-relative">Венгрия</a>
                        <a href="/articles/56" class="text-relative">Коста-Рика</a>
                        <a href="/articles/25" class="text-relative">Мексика</a>
                        <a href="/articles/81" class="text-relative">Дания</a>
                        <a href="/articles/42" class="text-relative">Таиланд</a>
                        <a href="/articles/57" class="text-relative">Польша</a>
                        <a href="/articles/83" class="text-relative">Литва</a>
                        <a href="/articles/90" class="text-relative">Украина</a>
                        <a href="#" class="text-relative">Кипр</a>
                        <a href="#" class="text-relative">Мальта</a>
                    </td>
                </tr>
                <tr>
                    <td class="td-relative">
                        Профилактика стоматологическая
                    </td>
                    <td class="td-relative">
                        <a href="/articles/16" class="text-relative">Швейцария</a>
                        <a href="/articles/10" class="text-relative">Венгрия</a>
                        <a href="/articles/7" class="text-relative">Австрия</a>
                        <a href="/articles/56" class="text-relative">Коста-Рика</a>
                        <a href="/articles/81" class="text-relative">Дания</a>
                        <a href="/articles/96" class="text-relative">Бельгия</a>
                        <a href="/articles/83" class="text-relative">Литва</a>
                        <a href="/articles/84" class="text-relative">Латвия</a>
                        <a href="#" class="text-relative">Мальта</a>
                        <a href="#" class="text-relative">Словения</a>
                        <a href="#" class="text-relative">Новая Зеландия</a>
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="table table-next-relative">
            <caption class="text-relative">Пластическая и реконструктивная хирургия</caption>
            <tbody>
                <tr>
                    <td class="td-relative">
                        Эндопротезирование груди
                    </td>
                    <td class="td-relative">
                        <a href="/articles/42" class="text-relative">Таиланд</a>
                        <a href="/articles/56" class="text-relative">Коста-Рика</a>
                        <a href="/articles/62" class="text-relative">Аргентина</a>
                        <a href="/articles/63" class="text-relative">Бразилия</a>
                        <a href="/articles/67" class="text-relative">Малайзия</a>
                        <a href="/articles/13" class="text-relative">Сингапур</a>
                        <a href="#" class="text-relative">Колумбия</a>
                        <a href="/articles/11" class="text-relative">Израиль</a>
                        <a href="/articles/57" class="text-relative">Польша</a>
                        <a href="/articles/17" class="text-relative">Южная Корея</a>
                        <a href="#" class="text-relative">Словения</a>
                        <a href="/articles/10" class="text-relative">Венгрия</a>
                        <a href="/articles/98" class="text-relative">Словакия</a>
                        <a href="/articles/16" class="text-relative">Швейцария</a>
                        <a href="/articles/90" class="text-relative">Украина</a>
                    </td>
                </tr>
                <tr>
                    <td class="td-relative">
                        Реконструктивные операции на груди
                    </td>
                    <td class="td-relative">
                        <a href="/articles/11" class="text-relative">Израиль</a>
                        <a href="/articles/42" class="text-relative">Таиланд</a>
                        <a href="/articles/63" class="text-relative">Бразилия</a>
                        <a href="/articles/17" class="text-relative">Южная Корея</a>
                        <a href="/articles/7" class="text-relative">Австрия</a>
                        <a href="/articles/9" class="text-relative">Германия</a>
                        <a href="/articles/15" class="text-relative">Турция</a>
                        <a href="#" class="text-relative">Словения</a>
                        <a href="#" class="text-relative">Новая Зеландия</a>
                        <a href="/articles/13" class="text-relative">Сингапур</a>
                        <a href="/articles/16" class="text-relative">Швейцария</a>
                        <a href="/articles/90" class="text-relative">Украина</a>
                    </td>
                </tr>
                <tr>
                    <td class="td-relative">
                        Липосакция (липоаспирация) в различных участках тела
                    </td>
                    <td class="td-relative">
                        <a href="/articles/11" class="text-relative">Израиль</a>
                        <a href="/articles/63" class="text-relative">Бразилия</a>
                        <a href="/articles/67" class="text-relative">Малайзия</a>
                        <a href="/articles/42" class="text-relative">Таиланд</a>
                        <a href="/articles/56" class="text-relative">Коста-Рика</a>
                        <a href="/articles/13" class="text-relative">Сингапур</a>
                        <a href="/articles/80" class="text-relative">Швеция</a>
                        <a href="/articles/7" class="text-relative">Австрия</a>
                        <a href="/articles/20" class="text-relative">Филиппины</a>
                        <a href="/articles/17" class="text-relative">Южная Корея</a>
                        <a href="/articles/15" class="text-relative">Турция</a>
                        <a href="#" class="text-relative">Словения</a>
                        <a href="/articles/90" class="text-relative">Украина</a>
                    </td>
                </tr>
                <tr>
                    <td class="td-relative">
                        Абдоминопластика
                    </td>
                    <td class="td-relative">
                        <a href="/articles/11" class="text-relative">Израиль</a>
                        <a href="/articles/42" class="text-relative">Таиланд</a>
                        <a href="/articles/63" class="text-relative">Бразилия</a>
                        <a href="/articles/62" class="text-relative">Аргентина</a>
                        <a href="/articles/17" class="text-relative">Южная Корея</a>
                        <a href="/articles/13" class="text-relative">Сингапур</a>
                        <a href="/articles/7" class="text-relative">Австрия</a>
                        <a href="/articles/16" class="text-relative">Швейцария</a>
                        <a href="/articles/15" class="text-relative">Турция</a>
                        <a href="#" class="text-relative">Словения</a>
                        <a href="#" class="text-relative">Новая Зеландия</a>
                        <a href="/articles/90" class="text-relative">Украина</a>
                    </td>
                </tr>
                <tr>
                    <td class="td-relative">
                        Ринопластика (коррекция размера и формы носа) и отопластика (коррекция ушных раковин)
                    </td>
                    <td class="td-relative">
                        <a href="/articles/62" class="text-relative">Аргентина</a>
                        <a href="/articles/63" class="text-relative">Бразилия</a>
                        <a href="/articles/56" class="text-relative">Коста-Рика</a>
                        <a href="#" class="text-relative">Колумбия</a>
                        <a href="/articles/42" class="text-relative">Таиланд</a>
                        <a href="/articles/13" class="text-relative">Сингапур</a>
                        <a href="/articles/17" class="text-relative">Южная Корея</a>
                        <a href="#" class="text-relative">Панама</a>
                        <a href="/articles/11" class="text-relative">Израиль</a>
                        <a href="/articles/9" class="text-relative">Германия</a>
                        <a href="#" class="text-relative">Словения</a>
                        <a href="/articles/16" class="text-relative">Швейцария</a>
                        <a href="/articles/91" class="text-relative">Италия</a>
                        <a href="#" class="text-relative">Мальта</a>
                        <a href="#" class="text-relative">Новая Зеландия</a>
                        <a href="/articles/90" class="text-relative">Украина</a>
                    </td>
                </tr>
                <tr>
                    <td class="td-relative">
                        Блефаропластика (коррекция век) и ментапластика (коррекция подбородка)
                    </td>
                    <td class="td-relative">
                        <a href="/articles/11" class="text-relative">Израиль</a>
                        <a href="/articles/42" class="text-relative">Таиланд</a>
                        <a href="/articles/63" class="text-relative">Бразилия</a>
                        <a href="/articles/62" class="text-relative">Аргентина</a>
                        <a href="/articles/17" class="text-relative">Южная Корея</a>
                        <a href="/articles/16" class="text-relative">Швейцария</a>
                        <a href="/articles/15" class="text-relative">Турция</a>
                        <a href="#" class="text-relative">Словения</a>
                        <a href="#" class="text-relative">Новая Зеландия</a>
                        <a href="/articles/13" class="text-relative">Сингапур</a>
                        <a href="#" class="text-relative">Мальта</a>
                        <a href="/articles/90" class="text-relative">Украина</a>
                        <a href="/articles/83" class="text-relative">Литва</a>
                        <a href="/articles/84" class="text-relative">Латвия</a
                    </td>
                </tr>
                <tr>
                    <td class="td-relative">
                        Хирургическое удаление шрамов
                    </td>
                    <td class="td-relative">
                        <a href="/articles/11" class="text-relative">Израиль</a>
                        <a href="/articles/42" class="text-relative">Таиланд</a>
                        <a href="/articles/17" class="text-relative">Южная Корея</a>
                        <a href="/articles/15" class="text-relative">Турция</a>
                        <a href="/articles/57" class="text-relative">Польша</a>
                        <a href="#" class="text-relative">Словения</a>
                        <a href="/articles/91" class="text-relative">Италия</a>
                        <a href="#" class="text-relative">Новая Зеландия</a>
                        <a href="/articles/13" class="text-relative">Сингапур</a>
                        <a href="#" class="text-relative">Мальта</a>
                        <a href="/articles/7" class="text-relative">Австрия</a>
                    </td>
                </tr>
                <tr>
                    <td class="td-relative">
                        Хирургия врожденных пороков у детей
                        (расщепление неба, раздвоение губы, пороки развития лицевых костей и др.)
                    </td>
                    <td class="td-relative">
                        <a href="/articles/11" class="text-relative">Израиль</a>
                        <a href="/articles/42" class="text-relative">Таиланд</a>
                        <a href="/articles/17" class="text-relative">Южная Корея</a>
                        <a href="/articles/9" class="text-relative">Германия</a>
                        <a href="/articles/7" class="text-relative">Австрия</a>
                        <a href="/articles/13" class="text-relative">Сингапур</a>
                        <a href="/articles/43" class="text-relative">Куба</a>
                        <a href="#" class="text-relative">Словения</a>
                        <a href="/articles/80" class="text-relative">Швеция</a>
                        <a href="#" class="text-relative">Новая Зеландия</a>
                        <a href="/articles/15" class="text-relative">Турция</a>
                        <a href="#" class="text-relative">Мальта</a>
                    </td>
                </tr>
                <tr>
                    <td class="td-relative">
                        Микрохирургические реконструктивные операции
                    </td>
                    <td class="td-relative">
                        <a href="/articles/42" class="text-relative">Таиланд</a>
                        <a href="/articles/11" class="text-relative">Израиль</a>
                        <a href="/articles/17" class="text-relative">Южная Корея</a>
                        <a href="/articles/7" class="text-relative">Австрия</a>
                        <a href="/articles/16" class="text-relative">Швейцария</a>
                        <a href="/articles/62" class="text-relative">Аргентина</a>
                        <a href="/articles/80" class="text-relative">Швеция</a>
                        <a href="/articles/56" class="text-relative">Коста-Рика</a>
                        <a href="/articles/14" class="text-relative">США</a>
                        <a href="#" class="text-relative">Словения</a>
                        <a href="#" class="text-relative">Новая Зеландия</a>
                        <a href="/articles/13" class="text-relative">Сингапур</a>
                    </td>
                </tr>
                <tr>
                    <td class="td-relative">
                        Коррекции ягодиц, голени и др.
                    </td>
                    <td class="td-relative">
                        <a href="/articles/11" class="text-relative">Израиль</a>
                        <a href="/articles/42" class="text-relative">Таиланд</a>
                        <a href="/articles/17" class="text-relative">Южная Корея</a>
                        <a href="/articles/7" class="text-relative">Австрия</a>
                        <a href="/articles/15" class="text-relative">Турция</a>
                        <a href="#" class="text-relative">Словения</a>
                        <a href="#" class="text-relative">Новая Зеландия</a>
                        <a href="/articles/13" class="text-relative">Сингапур</a>
                        <a href="/articles/57" class="text-relative">Польша</a>
                        <a href="#" class="text-relative">Мальта</a>
                    </td>
                </tr>
                <tr>
                    <td class="td-relative">
                        Трансплантация волос
                    </td>
                    <td class="td-relative">
                        <a href="#" class="text-relative">Колумбия</a>
                        <a href="/articles/56" class="text-relative">Коста-Рика</a>
                        <a href="/articles/62" class="text-relative">Аргентина</a>
                        <a href="/articles/63" class="text-relative">Бразилия</a>
                        <a href="/articles/11" class="text-relative">Израиль</a>
                        <a href="/articles/17" class="text-relative">Южная Корея</a>
                        <a href="/articles/13" class="text-relative">Сингапур</a>
                        <a href="/articles/42" class="text-relative">Таиланд</a>
                        <a href="#" class="text-relative">Мальта</a>
                        <a href="/articles/96" class="text-relative">Бельгия</a>
                        <a href="/articles/16" class="text-relative">Швейцария</a>
                    </td>
                </tr>
                <tr>
                    <td class="td-relative">
                        Пластика половых органов и операции по изменению пола
                    </td>
                    <td class="td-relative">
                        <a href="/articles/42" class="text-relative">Таиланд</a>
                        <a href="/articles/56" class="text-relative">Коста-Рика</a>
                        <a href="/articles/62" class="text-relative">Аргентина</a>
                        <a href="/articles/11" class="text-relative">Израиль</a>
                        <a href="/articles/9" class="text-relative">Германия</a>
                        <a href="/articles/17" class="text-relative">Южная Корея</a>
                        <a href="#" class="text-relative">Словения</a>
                        <a href="/articles/13" class="text-relative">Сингапур</a>
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="table table-next-relative">
            <caption class="text-relative">Репродуктивная медицина</caption>
            <tbody>
                <tr>
                    <td class="td-relative">
                        Диагностика и лечение бесплодия.
                        Программы с применением вспомогательных репродуктивных технологий (ЭКО, ИКСИ)
                    </td>
                    <td class="td-relative">
                        <a href="/articles/8" class="text-relative">Великобритания</a>
                        <a href="/articles/14" class="text-relative">США</a>
                        <a href="/articles/81" class="text-relative">Дания</a>
                        <a href="/articles/9" class="text-relative">Германия</a>
                        <a href="/articles/11" class="text-relative">Израиль</a>
                        <a href="#" class="text-relative">Кипр</a>
                        <a href="/articles/64" class="text-relative">Индия</a>
                        <a href="/articles/90" class="text-relative">Украина</a>
                        <a href="#" class="text-relative">Чехия</a>
                        <a href="/articles/98" class="text-relative">Словакия</a>
                        <a href="/articles/15" class="text-relative">Турция</a>
                        <a href="/articles/25" class="text-relative">Мексика</a>
                        <a href="/articles/42" class="text-relative">Таиланд</a>
                        <a href="/articles/16" class="text-relative">Швейцария</a>
                        <a href="#" class="text-relative">Мальта</a>
                        <a href="/articles/91" class="text-relative">Италия</a>
                        <a href="#" class="text-relative">Новая Зеландия</a>
                        <a href="/articles/72" class="text-relative">ЮАР</a>
                        <a href="/articles/18" class="text-relative">Япония</a>
                        <a href="/articles/65" class="text-relative">Испания</a>
                    </td>
                </tr>
                <tr>
                    <td class="td-relative">
                        Программы ЭКО с суррогатным материнством и/или донорством яйцеклеток (спермы)
                    </td>
                    <td class="td-relative">
                        <a href="/articles/64" class="text-relative">Индия</a>
                        <a href="/articles/14" class="text-relative">США</a>
                        <a href="/articles/90" class="text-relative">Украина</a>
                        <a href="#" class="text-relative">Кипр</a>
                        <a href="/articles/42" class="text-relative">Таиланд</a>
                        <a href="/articles/103" class="text-relative">Грузия</a>
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="table table-next-relative">
            <caption class="text-relative">Акушерство</caption>
            <tbody>
                <tr>
                    <td class="td-relative">
                        Роды за рубежом
                    </td>
                    <td class="td-relative">
                        <a href="/articles/9" class="text-relative">Германия</a>
                        <a href="/articles/14" class="text-relative">США</a>
                        <a href="/articles/7" class="text-relative">Австрия</a>
                        <a href="/articles/16" class="text-relative">Швейцария</a>
                        <a href="/articles/11" class="text-relative">Израиль</a>
                        <a href="/articles/13" class="text-relative">Сингапур</a>
                        <a href="#" class="text-relative">Чехия</a>
                        <a href="/articles/80" class="text-relative">Швеция</a>
                        <a href="/articles/17" class="text-relative">Южная Корея</a>
                        <a href="#" class="text-relative">Словения</a>
                        <a href="#" class="text-relative">Мальта</a>
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="table table-next-relative">
            <caption class="text-relative">Офтальмология</caption>
            <tbody>
                <tr>
                    <td class="td-relative">
                        Лазная коррекция зрения
                    </td>
                    <td class="td-relative">
                        <a href="/articles/11" class="text-relative">Израиль</a>
                        <a href="/articles/9" class="text-relative">Германия</a>
                        <a href="/articles/7" class="text-relative">Австрия</a>
                        <a href="/articles/67" class="text-relative">Малайзия</a>
                        <a href="/articles/13" class="text-relative">Сингапур</a>
                        <a href="/articles/69" class="text-relative">ОАЭ</a>
                        <a href="/articles/10" class="text-relative">Венгрия</a>
                        <a href="/articles/57" class="text-relative">Польша</a>
                        <a href="/articles/17" class="text-relative">Южная Корея</a>
                        <a href="#" class="text-relative">Словения</a>
                        <a href="/articles/98" class="text-relative">Словакия</a>
                        <a href="#" class="text-relative">Чехия</a>
                        <a href="#" class="text-relative">Мальта</a>
                        <a href="/articles/90" class="text-relative">Украина</a>
                        <a href="/articles/84" class="text-relative">Латвия</a>
                        <a href="/articles/83" class="text-relative">Литва</a>
                    </td>
                </tr>
                <tr>
                    <td class="td-relative">
                        Микрохирургия глаза
                    </td>
                    <td class="td-relative">
                        <a href="/articles/42" class="text-relative">Таиланд</a>
                        <a href="/articles/11" class="text-relative">Израиль</a>
                        <a href="/articles/13" class="text-relative">Сингапур</a>
                        <a href="/articles/67" class="text-relative">Малайзия</a>
                        <a href="/articles/17" class="text-relative">Южная Корея</a>
                        <a href="/articles/9" class="text-relative">Германия</a>
                        <a href="/articles/7" class="text-relative">Австрия</a>
                        <a href="/articles/56" class="text-relative">Коста-Рика</a>
                        <a href="/articles/14" class="text-relative">США</a>
                        <a href="#" class="text-relative">Словения</a>
                        <a href="#" class="text-relative">Новая Зеландия</a>
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="table table-next-relative">
            <caption class="text-relative">Дерматология</caption>
            <tbody>
                <tr>
                    <td class="td-relative">
                        Лечение псориаза
                    </td>
                    <td class="td-relative">
                        <a href="/articles/11" class="text-relative">Израиль</a>
                        <a href="#" class="text-relative">Иордания</a>
                        <a href="/articles/15" class="text-relative">Турция</a>
                        <a href="/articles/10" class="text-relative">Венгрия</a>
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="table table-next-relative">
            <caption class="text-relative">Хирургия</caption>
            <tbody>
                <tr>
                    <td class="td-relative">
                        Кардиохирургия
                    </td>
                    <td class="td-relative">
                        <a href="/articles/9" class="text-relative">Германия</a>
                        <a href="/articles/11" class="text-relative">Израиль</a>
                        <a href="/articles/7" class="text-relative">Австрия</a>
                        <a href="/articles/67" class="text-relative">Малайзия</a>
                        <a href="/articles/13" class="text-relative">Сингапур</a>
                        <a href="/articles/80" class="text-relative">Швеция</a>
                        <a href="/articles/17" class="text-relative">Южная Корея</a>
                        <a href="/articles/14" class="text-relative">США</a>
                        <a href="/articles/16" class="text-relative">Швейцария</a>
                    </td>
                </tr>
                <tr>
                    <td class="td-relative">
                        Нейрохирургия
                    </td>
                    <td class="td-relative">
                        <a href="/articles/11" class="text-relative">Израиль</a>
                        <a href="/articles/9" class="text-relative">Германия</a>
                        <a href="/articles/7" class="text-relative">Австрия</a>
                        <a href="/articles/13" class="text-relative">Сингапур</a>
                        <a href="/articles/17" class="text-relative">Южная Корея</a>
                        <a href="/articles/10" class="text-relative">Венгрия</a>
                        <a href="#" class="text-relative">Словения</a>
                        <a href="/articles/14" class="text-relative">США</a>
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="table table-next-relative">
            <caption class="text-relative">Ортопедия и травматология</caption>
            <tbody>
                <tr>
                    <td class="td-relative">
                        Протезирование и лечение суставов
                    </td>
                    <td class="td-relative">
                        <a href="/articles/11" class="text-relative">Израиль</a>
                        <a href="/articles/9" class="text-relative">Германия</a>
                        <a href="/articles/7" class="text-relative">Австрия</a>
                        <a href="/articles/80" class="text-relative">Швеция</a>
                        <a href="/articles/96" class="text-relative">Бельгия</a>
                        <a href="/articles/67" class="text-relative">Малайзия</a>
                        <a href="/articles/13" class="text-relative">Сингапур</a>
                        <a href="/articles/17" class="text-relative">Южная Корея</a>
                        <a href="#" class="text-relative">Словения</a>
                    </td>
                </tr>
                <tr>
                    <td class="td-relative">
                        Травматология и спортивная медицина
                    </td>
                    <td class="td-relative">
                        <a href="/articles/9" class="text-relative">Германия</a>
                        <a href="/articles/7" class="text-relative">Австрия</a>
                        <a href="/articles/11" class="text-relative">Израиль</a>
                        <a href="/articles/17" class="text-relative">Южная Корея</a>
                        <a href="/articles/13" class="text-relative">Сингапур</a>
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="table table-next-relative">
            <caption class="text-relative">Неврология и лечение зависимостей</caption>
            <tbody>
                <tr>
                    <td class="td-relative">
                        Синдром хронической усталости
                    </td>
                    <td class="td-relative">
                        <a href="/articles/11" class="text-relative">Израиль</a>
                        <a href="/articles/9" class="text-relative">Германия</a>
                        <a href="/articles/13" class="text-relative">Сингапур</a>
                        <a href="/articles/17" class="text-relative">Южная Корея</a>
                        <a href="#" class="text-relative">Новая Зеландия</a>
                        <a href="/articles/10" class="text-relative">Венгрия</a>
                        <a href="/articles/25" class="text-relative">Мексика</a>
                        <a href="#" class="text-relative">Чехия</a>
                    </td>
                </tr>
                <tr>
                    <td class="td-relative">
                        Лечение алкогольной и наркологической зависимостей
                    </td>
                    <td class="td-relative">
                        <a href="/articles/11" class="text-relative">Израиль</a>
                        <a href="/articles/42" class="text-relative">Таиланд</a>
                        <a href="/articles/17" class="text-relative">Южная Корея</a>
                        <a href="/articles/7" class="text-relative">Австрия</a>
                        <a href="/articles/15" class="text-relative">Турция</a>
                        <a href="#" class="text-relative">Словения</a>
                        <a href="#" class="text-relative">Новая Зеландия</a>
                        <a href="/articles/13" class="text-relative">Сингапур</a>
                        <a href="/articles/56" class="text-relative">Коста-Рика</a>
                        <a href="/articles/8" class="text-relative">Великобритания</a>
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="table table-next-relative">
            <caption class="text-relative">Диагностика</caption>
            <tbody>
                <tr>
                    <td class="td-relative">
                        Комплексная диагностика
                    </td>
                    <td class="td-relative">
                        <a href="/articles/9" class="text-relative">Германия</a>
                        <a href="/articles/7" class="text-relative">Австрия</a>
                        <a href="/articles/11" class="text-relative">Израиль</a>
                        <a href="/articles/17" class="text-relative">Южная Корея</a>
                        <a href="/articles/13" class="text-relative">Сингапур</a>
                        <a href="/articles/16" class="text-relative">Швейцария</a>
                        <a href="/articles/10" class="text-relative">Венгрия</a>
                        <a href="#" class="text-relative">Мальта</a>
                        <a href="/articles/15" class="text-relative">Турция</a>
                        <a href="/articles/69" class="text-relative">ОАЭ</a>
                    </td>
                </tr>
                <tr>
                    <td class="td-relative">
                        Диагностика по направлениям
                    </td>
                    <td class="td-relative">
                        <a href="/articles/9" class="text-relative">Германия</a>
                        <a href="/articles/7" class="text-relative">Австрия</a>
                        <a href="/articles/11" class="text-relative">Израиль</a>
                        <a href="/articles/17" class="text-relative">Южная Корея</a>
                        <a href="/articles/13" class="text-relative">Сингапур</a>
                        <a href="#" class="text-relative">Мальта</a>
                        <a href="#" class="text-relative">Чехия</a>
                        <a href="/articles/69" class="text-relative">ОАЭ</a>
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="table table-next-relative">
            <caption class="text-relative">Народная И Нетрадиционная Медицина</caption>
            <tbody>
                <tr>
                    <td class="td-relative">
                        Аюрведа
                    </td>
                    <td class="td-relative">
                        <a href="/articles/64" class="text-relative">Индия</a>
                        <a href="/articles/67" class="text-relative">Малайзия</a>
                        <a href="/articles/13" class="text-relative">Сингапур</a>
                        <a href="#" class="text-relative">Индонезия</a>
                    </td>
                </tr>
                <tr>
                    <td class="td-relative">
                        Народные и нетрадиционные методики
                    </td>
                    <td class="td-relative">
                        <a href="/articles/12" class="text-relative">Китай</a>
                        <a href="/articles/20" class="text-relative">Филиппины</a>
                        <a href="/articles/67" class="text-relative">Малайзия</a>
                        <a href="/articles/42" class="text-relative">Таиланд</a>
                        <a href="#" class="text-relative">Индонезия</a>
                        <a href="/articles/64" class="text-relative">Индия</a>
                        <a href="/articles/106" class="text-relative">Вьетнам</a>
                        <a href="/articles/25" class="text-relative">Мексика</a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>