<?php
/* @var $this InfohubController */
/* @var $model Infohub */

$this->breadCrumbs = array(
    'Инфохаб' => array('index'),
    'Создать',
);

$this->menu = array(
    array('label' => 'Список слайдеров', 'url' => array('index')),
    array('label' => 'Управление инфохабом', 'url' => array('admin')),
);
?>

    <h1>Добавить новый слайдер</h1>

<?php $this->renderPartial('_form', array('model' => $model)); ?>