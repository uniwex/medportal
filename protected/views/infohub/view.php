<?php
/* @var $this InfohubController */
/* @var $model Infohub */

$this->breadCrumbs = array(
    'Инфохаб' => array('index'),
    $model->id,
);

$this->menu = array(
    array('label' => 'List Infohub', 'url' => array('index')),
    array('label' => 'Create Infohub', 'url' => array('create')),
    array('label' => 'Update Infohub', 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Delete Infohub', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage Infohub', 'url' => array('admin')),
);
?>

<h1>Просмотр слайда #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'header',
        'description',
        'link',
        'pic',
    ),
)); ?>
