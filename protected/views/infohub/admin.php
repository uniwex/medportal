<?php
/* @var $this InfohubController */
/* @var $model Infohub */

$this->breadCrumbs = array(
    'Инфохаб' => array('index'),
    'Управление',
);

$this->menu = array(
    array('label' => 'Список слайдеров', 'url' => array('index')),
    array('label' => 'Добавить новый', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$('#infohub-grid').yiiGridView('update', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Управление слайдерами</h1>

<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search', array(
        'model' => $model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'infohub-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'header',
        'description',
        'link',
        'pic',
        array(
            'class' => 'CButtonColumn',
        ),
    ),
)); ?>
