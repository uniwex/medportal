<?php
/* @var $this InfohubController */
/* @var $dataProvider CActiveDataProvider */

$this->breadCrumbs = array(
    'Инфохаб',
);

$this->menu = array(
    array('label' => 'Добавить новый', 'url' => array('create')),
    array('label' => 'Управление инфохабом', 'url' => array('admin')),
);
?>

<h1>Слайдеры инфохаба</h1>

<?php $this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_view',
)); ?>
