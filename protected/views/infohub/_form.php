<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'infohub-form',
        'htmlOptions' => ['method' => 'POST', 'enctype' => 'multipart/form-data'],
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    )); ?>
    <div class="row">
        <div class="col-md-6 createGood">
            <div class="box box-primary">
                <div class="box-header with-border">
                </div>
                <div class="box-body">
                    <?php echo $form->errorSummary($model); ?>


                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'header'); ?><br>
                        <?php echo $form->textField($model, 'header'); ?>
                        <?php echo $form->error($model, 'header'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'description'); ?>
                        <?php echo $form->textArea($model, 'description', array('rows' => 6, 'cols' => 50)); ?>
                        <?php echo $form->error($model, 'description'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'link'); ?><br>
                        <?php echo $form->textField($model, 'link'); ?>
                        <?php echo $form->error($model, 'link'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'pic'); ?><br>
                        <?php echo $form->fileField($model, 'picture', array('class' => 'form-control')); ?>
                        <?php echo $form->error($model, 'pic'); ?>
                    </div>

                    <div class="row buttons text-center">
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>

</div><!-- form -->
<script src="//cdn.ckeditor.com/4.5.4/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('Infohub[description]');
    var crutchForCKEditor = function(){
        var abc = document.getElementsByTagName('iframe')[0].contentDocument;
        var abc2 = abc.childNodes[1];
        var text = abc2.childNodes[1].childNodes[0].innerHTML;
        if(text != '<br>')
            $('Infohub_description').val(text);
    }
</script>
