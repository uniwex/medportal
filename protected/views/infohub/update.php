<?php
/* @var $this InfohubController */
/* @var $model Infohub */

$this->breadCrumbs = array(
    'Инфохаб' => array('index'),
    $model->id => array('view', 'id' => $model->id),
    'Обновить',
);

$this->menu = array(
    array('label' => 'Список слайдеров', 'url' => array('index')),
    array('label' => 'Добавить новый', 'url' => array('create')),
    array('label' => 'Просмотр слайдера', 'url' => array('view', 'id' => $model->id)),
    array('label' => 'Управление инфохабом', 'url' => array('admin')),
);
?>

    <h1>Обновить слайдер <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model' => $model)); ?>