<?php
/* @var $this SpecialProjectController */
/* @var $model SpecialProject */

$this->breadCrumbs = array(
    'Спец. проект' => array('index'),
    'Создать',
);

$this->menu = array(
    array('label' => 'Спец. проект', 'url' => array('index')),
    array('label' => 'Спец. проект', 'url' => array('admin')),
);
?>

    <h1>Добавить спец. проект</h1>

<?php $this->renderPartial('_form', array('model' => $model)); ?>