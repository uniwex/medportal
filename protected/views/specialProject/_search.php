<?php
/* @var $this SpecialProjectController */
/* @var $model SpecialProject */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

                    <div class="row">
            <?php echo $form->label($model,'id'); ?>
            <?php echo $form->textField($model,'id'); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'link'); ?>
            <?php echo $form->textArea($model,'link',array('rows'=>6, 'cols'=>50)); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'pic'); ?>
            <?php echo $form->textArea($model,'pic',array('rows'=>6, 'cols'=>50)); ?>
        </div>

        <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->