<?php
/* @var $this SpecialProjectController */
/* @var $model SpecialProject */

$this->breadCrumbs = array(
    'Спец. проект' => array('index'),
    'Управление',
);

$this->menu = array(
    array('label' => 'Спец. проект', 'url' => array('index')),
    array('label' => 'Добавить спец. проект', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$('#special-project-grid').yiiGridView('update', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Управление спец. проектом</h1>


<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search', array(
        'model' => $model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'special-project-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'link',
        'pic',
        array(
            'class' => 'CButtonColumn',
        ),
    ),
)); ?>
