<?php
/* @var $this SpecialProjectController */
/* @var $dataProvider CActiveDataProvider */

$this->breadCrumbs=array(
	'Special Projects',
);

$this->menu=array(
array('label'=>'Создать спец. проект', 'url'=>array('create')),
array('label'=>'Спец. проект', 'url'=>array('admin')),
);
?>

<h1>Special Projects</h1>

<?php $this->widget('zii.widgets.CListView', array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
