<?php
    /* @var $this SpecialProjectController */
    /* @var $model SpecialProject */

$this->breadCrumbs=array(
	'Special Projects'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

    $this->menu=array(
    array('label'=>'Спец. проект', 'url'=>array('index')),
    array('label'=>'Создать спец. проект', 'url'=>array('create')),
    array('label'=>'Просмот спец. проекта', 'url'=>array('view', 'id'=>$model->id)),
    array('label'=>'Спец. проект', 'url'=>array('admin')),
    );
    ?>

    <h1>Редактировать спец. проект <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>