<?php
/* @var $this SpecialProjectController */
/* @var $model SpecialProject */

$this->breadCrumbs = array(
    'Спец. проект' => array('index'),
    $model->id,
);

$this->menu = array(
    array('label' => 'Спец. проект', 'url' => array('index')),
    array('label' => 'Создать спец. проект', 'url' => array('create')),
    array('label' => 'Редактировать спец. проект', 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Удалить спец. проект', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Спец. проект', 'url' => array('admin')),
);
?>

<h1>Просмотр спец. проекта #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'link',
        'pic',
    ),
)); ?>
