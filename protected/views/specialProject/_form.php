<?php
/* @var $this SpecialProjectController */
/* @var $model SpecialProject */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'special-project-form',
        'htmlOptions' => ['method' => 'POST', 'enctype' => 'multipart/form-data'],
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    )); ?>
    <div class="row">
        <div class="col-md-6 createGood">
            <div class="box box-primary">
                <div class="box-header with-border">
                </div>
                <div class="box-body">
                    <?php echo $form->errorSummary($model); ?>

                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'link'); ?>
                        <?php echo $form->textField($model, 'link'); ?>
                        <?php echo $form->error($model, 'link'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'pic'); ?>
                        <?php echo $form->fileField($model, 'picture', array('class' => 'form-control')); ?>
                        <?php echo $form->error($model, 'pic'); ?>
                    </div>

                    <div class="row buttons text-center">
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>

</div><!-- form -->