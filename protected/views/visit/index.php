
<div class="col-md-10" data-type="ticket">
    <div class="row indent">
        <div class="col-md-12">
            <h1 class="userInformationHead2 no-margin">Запись на прием</h1>
        </div>
    </div>
    <?php
    if($step == 1) {
        ?>
        <div class="row indent">
            <div class="col-md-12">
                <h1 class="userInformationHead2 no-margin text-orange">Шаг 1</h1>
            </div>
        </div>
        <div class="row indent">
            <div class="col-md-12">
                <span>К кому хотите записаться: доктор/сиделка ?</span>
            </div>
        </div>
        <span id="isNurseNotSelected">
            <?php $formVisiting = $this->beginWidget("CActiveForm", array(
                'id' => 'specialization-registration',
                'action' => Yii::app()->request->baseUrl . '/visit/index',
                'method' => 'post',
                'enableAjaxValidation' => true,
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                    'validateOnChange' => true,
                ),
                'htmlOptions' => array(
                    'autocomplete' => 'off',
                    'onsubmit' => ''
                ),
            ));  ?>
            <div class="form-group row">
                <div class="col-md-8">
                    <?= $formVisiting->radioButtonList($userModel, 'type',
                        array('0' => 'Доктор', '1' => 'Сиделка'),
                        array('onclick' => 'visit_switchIsNurse(this)', 'class' => 'isNurse-false', 'name' => 'isNurse[type][0]')); ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <label for="specialization">Выбор специалиста</label>
                </div>
                <div class="col-md-10">
                    <?
                    $specializations = Specialization::model()->findAll();
                    ?>
                    <?= $formVisiting->dropDownList(
                        $specializationModel,
                        'name',
                        array_merge(
                            array('' => 'Выберите специализацию'),
                            CHtml::listData(
                                $specializations, 'id', 'name'
                            )
                        ),
                        array('class' => 'fieldInput')
                    ) ?>
                    <?= $formVisiting->error($specializationModel, 'name', array('class' => 'text-red')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <?= CHtml::submitButton('Следующий шаг', array('class' => 'button1 button1reg')); ?>
                </div>
            </div>
            <input name="step" type="hidden" value="2"/>
            <?php $this->endWidget(); ?>
        </span>
        <span id="isNurseSelected">
            <?php $formVisiting = $this->beginWidget("CActiveForm", array(
                'id' => 'nurse-registration',
                'action' => Yii::app()->request->baseUrl . '/visit/index',
                'method' => 'post',
                'enableAjaxValidation' => true,
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                    'validateOnChange' => true,
                ),
                'htmlOptions' => array(
                    'autocomplete' => 'off',
                    'onsubmit' => ''
                ),
            ));  ?>
            <div class="form-group row">
                <div class="col-md-8">
                    <?= $formVisiting->radioButtonList($userModel, 'type',
                        array('0' => 'Доктор', '1' => 'Сиделка'),
                        array('onclick' => 'visit_switchIsNurse(this)', 'class' => 'isNurse-true', 'name' => 'isNurse[type][0]')); ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <label for="specialization">Выбор сиделки</label>
                </div>
                <div class="col-md-10">
                    <?
                    $nurses = Nurse::model()->findAll();
                    $nurseOptions = array();
                    foreach($nurses as $key){
                        $nurseOptions[$key->idUser] = $key->name;
                    }
                    ?>
                    <?= $formVisiting->dropDownList(
                        $nurseModel,
                        'name',
                        $nurseOptions,
                        array('empty' => 'Выберите сиделку', 'class' => 'fieldInput')
                    ) ?>
                    <?= $formVisiting->error($nurseModel, 'name', array('class' => 'text-red')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <?= CHtml::submitButton('Следующий шаг', array('class' => 'button1 button1reg')); ?>
                </div>
            </div>
            <input name="step" type="hidden" value="8"/>
            <?php $this->endWidget(); ?>
        </span>
        <?php
        }
    ?>
    <?php
    if($step == 3) {
        ?>
        <div class="row indent">
            <div class="col-md-12">
                <h1 class="userInformationHead2 no-margin text-orange">Шаг 2</h1>
            </div>
        </div>
        <div class="row indent">
            <div class="col-md-12">
                <span>Выбрана специальность: <?= $specialization; ?></span>
            </div>
        </div>
        <div class="row indent">
            <div class="col-md-12">
                <span>К кому хотите записаться: врач/клиника ?</span>
            </div>
        </div>
        <span id="displayVisitDocClinic">
            <?php $formDoctorVisiting = $this->beginWidget("CActiveForm", array(
                'id' => 'doctor-visiting',
                'action' => Yii::app()->request->baseUrl . '/visit/index',
                'method' => 'post',
                'enableAjaxValidation' => true,
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                    'validateOnChange' => true,
                ),
                'htmlOptions' => array(
                    'autocomplete' => 'off'
                ),
            ));  ?>
            <div class="form-group row">
                <div class="col-md-8">
                    <?= $formDoctorVisiting->radioButtonList($userModel, 'type',
                        array('0' => 'Частный врач', '1' => 'Клиника'),
                        array('onclick' => 'visit_switchUserType(this)', 'class' => 'ut-doctor', 'name' => 'User[type][0]')); ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-8">
                    <?
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'idSpecialization = :idSpecialization AND timeSize > :timeSize AND enabled = :enabled';
                    $criteria->params = array(':idSpecialization' => $idSpecialization, ':timeSize' => '0', ':enabled' => '1');
                    $doctors = Doctor::model()->findAll($criteria);
                    /*
                    echo '____<br>';
                    foreach($doctors as $key){
                        echo $key->id . "<br>";
                    }
                    die();
                    */
                    $doctorOptions = array();
                    if(isset($doctors)) {
                        foreach ($doctors as $key) {
                            $doctorOptions[$key->idUser] = User::model()->findByPk($key->idUser)->name;
                        }
                    }
                    ?>
                    <?= $formDoctorVisiting->dropDownList(
                        $doctorModel,
                        'name',
                        $doctorOptions,
                        array('empty' => 'Выберите доктора', 'class' => 'fieldInput')
                    ); ?>
                    <?= $formDoctorVisiting->error($doctorModel, 'name', array('class' => 'text-red')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <?= CHtml::submitButton('Следующий шаг', array('class' => 'button1 button1reg')); ?>
                </div>
            </div>
            <input name="step" type="hidden" value="4"/>
            <input id="docType" name="docType" type="hidden" value="0"/>
            <?php $this->endWidget(); ?>
        </span>
        <span id="displayVisitClinic">
            <?php $formClinicDocVisiting = $this->beginWidget("CActiveForm", array(
                'id' => 'clinicDoc-visiting',
                'action' => Yii::app()->request->baseUrl . '/visit/index',
                'method' => 'post',
                'enableAjaxValidation' => true,
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                    'validateOnChange' => true,
                ),
                'htmlOptions' => array(
                    'autocomplete' => 'off'
                ),
            ));  ?>
            <div class="form-group row">
                <div class="col-md-8">
                    <?= $formClinicDocVisiting->radioButtonList($userModel, 'type',
                        array('0' => 'Частный врач', '1' => 'Клиника'),
                        array('onclick' => 'visit_switchUserType(this)', 'class' => 'ut-clinicDoc', 'name' => 'User[type][1]')); ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-8">
                    <?
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'specialization = :idSpecialization and type = :type AND timeSize > :timeSize';
                    $criteria->params = array(':idSpecialization' => $idSpecialization, ':type' => 'clinicDoc', ':timeSize' => '0');
                    $schedules = Schedule::model()->findAll($criteria);
                    ?>
                    <?
                        if(isset($schedules)){
                            echo $formClinicDocVisiting->dropDownList(
                                $scheduleModel,
                                'name',
                                array_merge(
                                    array('' => 'Выберите доктора'),
                                    CHtml::listData(
                                        $schedules, 'id', 'name',
                                        function($schedule) {
                                            if(isset($schedule))
                                                if(isset($schedule->getClinic->name))
                                                    return "Клиника: ".$schedule->getClinic->name;
                                        }
                                    )
                                ),
                                array('class' => 'fieldInput')
                            );
                        } ?>
                    <?= $formClinicDocVisiting->error($scheduleModel, 'name', array('class' => 'text-red')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <?= CHtml::submitButton('Следующий шаг', array('class' => 'button1 button1reg')); ?>
                </div>
            </div>
            <input name="step" type="hidden" value="4"/>
            <input id="docType" name="docType" type="hidden" value="0"/>
            <?php $this->endWidget(); ?>
        </span>
    <?php
    }
    ?>
    <?php

    if($step == 5) {?>
        <div class="row indent">
            <div class="col-md-12">
                <h1 class="userInformationHead2 no-margin text-orange">Шаг 3</h1>
            </div>
        </div>
        <div class="row indent">
            <div class="col-md-12">
                <span>Запись ко врачу: <?= ($docName); ?></span>
            </div>
        </div>
        <?php
        $scheduleModel = Schedule::model()->findByPk($docId);
        if($scheduleModel->type == 'doctor')
            $docTimeSize = Doctor::model()->findByPk($scheduleModel->idUser)->timeSize;
        $userModel = User::model()->findByPk($scheduleModel->idUser);
        if(User::model()->findByPk(Yii::app()->user->id)->id != $userModel->id){
        if($userModel->money - $userModel->tax >= 0) {
        ?>
        <div class="row indent">
            <div class="col-md-12">
                <span>Длительность приёма: <?= ($docTimeSize) . " минут"; ?></span>
            </div>
        </div>
        <div class="row indent">
            <div class="col-md-12">
                <span>Выберите дату приёма:</span>
            </div>
        </div>
        <?php $formTimeVisiting = $this->beginWidget("CActiveForm", array(
            'id' => 'timeVisiting-visiting',
            'action' => Yii::app()->request->baseUrl . '/visit/index',
            'method' => 'post',
            'enableAjaxValidation' => true,
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => true,
            ),
            'htmlOptions' => array(
                'autocomplete' => 'off',
                'onsubmit'=>'return checkTimes()'
            ),
        ));  ?>
        <div class="form-group row">
            <div class="col-md-2">
                Дата приёма*
            </div>
            <div class="col-md-8">
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'name' => 'eventDate',
                    'model' => $eventModel,
                    'attribute' => 'eventDate',
                    'language' => 'ru',
                    'options' => array(
                        'dateFormat' => 'yy-mm-dd',
                        'class' => 'fieldInput',
                        'showAnim' => 'fold'
                    ),
                    'htmlOptions' => array(
                        'class' => 'fieldInput',
                        'id' => 'visit_date',
                        'onChange' => 'visit_getTime(this.value)'
                    )
                )); ?>
                <?= $formTimeVisiting->error($eventModel, 'eventDate', array('class' => 'text-red')); ?>
                <div id="docNotWork" class="text-red" style="display:none;">В этот день врач не работает</div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                Время приёма*
            </div>
            <div class="col-md-8">
                <?= $formTimeVisiting->dropDownList($eventModel, 'eventTime', array('0' => 'Выберите время'), array('class' => 'fieldInput')); ?>
                <?= $formTimeVisiting->error($eventModel, 'eventTime', array('class' => 'text-red')); ?>
            </div>
        </div>
        <input name="step" type="hidden" value="6"/>
        <?= "<input type='hidden' name='hiddenPatientId' id='hiddenPatientId' value='".Yii::app()->user->id."'>"; ?>
        <?= "<input type='hidden' name='hiddenDocId' id='hiddenDocId' value='$docId'>"; ?>
        <?= "<input type='hidden' id='hiddenTimeSize' value='$docTimeSize'>"; ?>
        <div class="form-group row">
            <div class="col-md-12">
                <?= CHtml::submitButton('Следующий шаг', array('class' => 'button1 button1reg')); ?>
            </div>
        </div>
        <?php $this->endWidget(); ?>
            <?php } else { ?>
                <div class="form-group row">
                    <div class="col-md-12">
                        Извините, в данный момент вы не можете записаться к этому врачу.
                    </div>
                </div>
            <?php } ?>
            <?php } else { ?>
                <div class="form-group row">
                    <div class="col-md-12">
                        Вы не можете записаться к самому себе.
                    </div>
                </div>
            <?php } ?>
    <?php } ?>
    <?php if($step == 7) {?>
        Запись выполнена успешно!
        <div class="form-group row">
            <div class="col-md-12">
                Вы записаны на <?= $visitDate; ?>, время приёма <?= $visitTime; ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-12">
                Ваш лечащий врач: <?= $visitDocSpecialization . " " . $docName; ?>
            </div>
        </div>
    <?php } ?>
    <?php if($step == 11) {?>
        Запись выполнена успешно!
        <div class="form-group row">
            <div class="col-md-12">
                Вы записаны к сиделке <?=$docName;?> с <?= $eventModel->eventDate; ?> по <?= $eventModel->eventDateFinish; ?>
            </div>
        </div>
    <?php } ?>
    <?php if($step == 9) {?>
        <div class="row indent">
            <div class="col-md-12">
                <h1 class="userInformationHead2 no-margin text-orange">Шаг 2</h1>
            </div>
        </div>
        <div class="row indent">
            <div class="col-md-12">
                <span>Запись к сиделке: <?= ($docName); ?></span>
            </div>
        </div>

        <div class="row indent">
            <div class="col-md-12">
                <span>Выберите на какой период записаться</span>
            </div>
        </div>
        <?php $formTimeVisiting = $this->beginWidget("CActiveForm", array(
            'id' => 'nurse-visiting',
            'action' => Yii::app()->request->baseUrl . '/visit/index',
            'method' => 'post',
            'enableAjaxValidation' => true,
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => true,
            ),
            'htmlOptions' => array(
                'autocomplete' => 'off',
                'onsubmit'=>'return getGlobalDateFree()'
            ),
        ));  ?>
        <div class="form-group row">
            <div class="col-md-2">
                С даты:
            </div>
            <div class="col-md-8">
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'name' => 'eventDate',
                    'model' => $eventModel,
                    'attribute' => 'eventDate',
                    'language' => 'ru',
                    'options' => array(
                        'dateFormat' => 'yy-mm-dd',
                        'class' => 'fieldInput',
                        'showAnim' => 'fold'
                    ),
                    'htmlOptions' => array(
                        'class' => 'fieldInput',
                        'id' => 'visit_dateBegin',
                        'onChange' => 'visit_checkDate(\'begin\',this.value)'
                    )
                )); ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                По дату:
            </div>
            <div class="col-md-8">
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'name' => 'eventDateFinish',
                    'model' => $eventModel,
                    'attribute' => 'eventDateFinish',
                    'language' => 'ru',
                    'options' => array(
                        'dateFormat' => 'yy-mm-dd',
                        'class' => 'fieldInput',
                        'showAnim' => 'fold'
                    ),
                    'htmlOptions' => array(
                        'class' => 'fieldInput',
                        'id' => 'visit_dateEnd',
                        'onChange' => 'visit_checkDate(\'end\',this.value)'
                    )
                )); ?>
                <div id="nurseDateError" class="text-red" style="display:none;">На эту дату уже есть запись</div>
            </div>
        </div>
        <input id="idNurse" name="idNurse" type="hidden" value="<?= $idNurse; ?>"/>
        <input name="step" type="hidden" value="8"/>
        <div class="form-group row">
            <div class="col-md-12">
                <?= CHtml::submitButton('Записаться', array('class' => 'button1 button1reg')); ?>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    <?php } ?>
</div>
<script>
    globalIsDateFree = false;

    function getGlobalDateFree(){
        return globalIsDateFree;
    }

    function checkTimes() {
        if ($('#Event_eventTime').val() == 'Выберите время')
            return false;
        return true;
    }

    $(document).ready(function(){
        if(document.getElementsByClassName('ut-doctor')[0])
            document.getElementsByClassName('ut-doctor')[0].click();
        if(document.getElementsByClassName('isNurse-false')[0])
            document.getElementsByClassName('isNurse-false')[0].click();
    });

    var errorsMessages = [
        'В этот день врач не работает.',
        'В этот день все свободные часы приёма уже заняты.'
    ]

    function visit_switchUserType(el){
        var type;
        console.log('test');
        if(el.className == 'ut-doctor'){
            if(el.value == 0){
                type = 'Частный врач';
                $('#displayVisitClinic').css('display','none');
                $('#displayVisitDocClinic').css('display','block');
                $('#docType').val('0');
            }
            if(el.value == 1){
                type = 'Клиника';
                $('#displayVisitClinic').css('display','block');
                $('#displayVisitDocClinic').css('display','none');
                $('#docType').val('1');
                document.getElementsByClassName('ut-clinicDoc')[1].click();
            }
        }
        if(el.className == 'ut-clinicDoc'){
            if(el.value == 0){
                type = 'Частный врач';
                $('#displayVisitClinic').css('display','none');
                $('#displayVisitDocClinic').css('display','block');
                $('#docType').val('0');
                document.getElementsByClassName('ut-doctor')[0].click();
            }
            if(el.value == 1){
                type = 'Клиника';
                $('#displayVisitClinic').css('display','block');
                $('#displayVisitDocClinic').css('display','none');
                $('#docType').val('1');
            }
        }
    }

    function visit_switchIsNurse(el) {
        var type;
        console.log('test1');
        console.log("value" + el.value);
        if(el.className == 'isNurse-true') {
            if (el.value == 0) {
                type = 'Доктор';
                $('#isNurseNotSelected').css('display', 'block');
                $('#isNurseSelected').css('display', 'none');
                document.getElementsByClassName('isNurse-false')[0].click();
            }
            if (el.value == 1) {
                type = 'Сиделка';
                $('#isNurseNotSelected').css('display', 'none');
                $('#isNurseSelected').css('display', 'block');
            }
        }
        if(el.className == 'isNurse-false'){
            if (el.value == 0) {
                type = 'Доктор';
                $('#isNurseNotSelected').css('display', 'block');
                $('#isNurseSelected').css('display', 'none');
            }
            if (el.value == 1) {
                type = 'Сиделка';
                $('#isNurseNotSelected').css('display', 'none');
                $('#isNurseSelected').css('display', 'block');
                document.getElementsByClassName('isNurse-true')[1].click();
            }
        }
    }

    function visit_getTime(visitDate){
        var fixPath = (location.host == "localhost") ? '/medportal' : '';
        var url = 'http://' + location.host + fixPath + '/visit/getTime';
        var timeSize = parseInt($('#hiddenTimeSize').val());
        var docId = $('#hiddenDocId').val();
        var MINUTES_IN_HOUR = 60;
        $.ajax({
            url : url,
            method : 'POST',
            data : 'visitDate=' + visitDate + '&docId=' + docId,
            success : function(responce){
                var data = JSON.parse(responce);
                var time = data.time;
                $('#docNotWork').css('display','none');
                if(time == 'Выходной') {
                    $('#docNotWork').html(errorsMessages[0]);
                    $('#docNotWork').css('display','block');
                    $('#Visitors_visitTime').html('<option>Выберите время</option>');
                    return;
                }
                var schedule = time.split(',');
                var minTime = schedule[0].split('-')[0];
                var maxTime = schedule[schedule.length-1].split('-')[1];
                var minMinutes = 0;
                var maxMinutes = visit_timeToMinutes(minTime, maxTime);
                //var testTime = minutesToTime(minTime, 610);
                var arrayOfBreaks = [];
                if(data.timeIntervals.length > 0)
                    arrayOfBreaks = data.timeIntervals;
                //form array of time breaks
                for(var i=0;i<schedule.length;i++){
                    if(schedule[i+1])
                        arrayOfBreaks.push(schedule[i].split('-')[1]+'-'+schedule[i+1].split('-')[0])
                }
                //sort
                for(var i=0;i<arrayOfBreaks.length;i++){
                    for(var j=i+1;j<arrayOfBreaks.length;j++){
                        var temp;
                        var hours1 = arrayOfBreaks[i].split('-')[0].split(':')[0];
                        var hours2 = arrayOfBreaks[j].split('-')[0].split(':')[0];
                        var minutes1 = arrayOfBreaks[i].split('-')[0].split(':')[1];
                        var minutes2 = arrayOfBreaks[j].split('-')[0].split(':')[1];
                        if(hours1 > hours2) {
                            temp = arrayOfBreaks[i];
                            arrayOfBreaks[i] = arrayOfBreaks[j];
                            arrayOfBreaks[j] = temp;
                        }
                        if((hours1 == hours2)&&(minutes1>minutes2)) {
                            temp = arrayOfBreaks[i];
                            arrayOfBreaks[i] = arrayOfBreaks[j];
                            arrayOfBreaks[j] = temp;
                        }
                    }
                }

                options = '<option>Выберите время</option>';
                var timesVisit = 0;
                if(arrayOfBreaks.length > 0) {
                    for (i = 0; i <= arrayOfBreaks.length; i++) {
                        var tempMinTime;
                        var tempMaxMinutes;
                        if (i == 0)
                            tempMinTime = minTime;
                        if ((i > 0) && (i < arrayOfBreaks.length))
                            tempMinTime = arrayOfBreaks[i - 1].split('-')[1];
                        if (i == arrayOfBreaks.length)
                            tempMinTime = arrayOfBreaks[arrayOfBreaks.length - 1].split('-')[1];
                        if (!arrayOfBreaks[i])
                            tempMaxMinutes = visit_timeToMinutes(arrayOfBreaks[arrayOfBreaks.length - 1].split('-')[1], maxTime);
                        else
                            tempMaxMinutes = visit_timeToMinutes(tempMinTime, arrayOfBreaks[i].split('-')[0]);
                        for (var j = 0; j <= tempMaxMinutes - timeSize; j++) {
                            if ((j % timeSize == 0) || (j == 0)) {
                                timesVisit++;
                                options += '<option>' + visit_minutesToTime(tempMinTime, j) + '-' + visit_minutesToTime(tempMinTime, j + timeSize) + '</option>';
                            }
                        }
                    }
                } else {
                    var tempMinTime = minTime;
                    var tempMaxMinutes;
                    tempMaxMinutes = visit_timeToMinutes(minTime, maxTime);
                    for (var j = 0; j <= tempMaxMinutes - timeSize; j++) {
                        if ((j % timeSize == 0) || (j == 0)) {
                            timesVisit++;
                            options += '<option>' + visit_minutesToTime(tempMinTime, j) + '-' + visit_minutesToTime(tempMinTime, j + timeSize) + '</option>';
                        }
                    }
                }

                if(timesVisit == 0) {
                    $('#docNotWork').html(errorsMessages[1]);
                    $('#docNotWork').css('display', 'block');
                }

                $('#Event_eventTime').html(options);
            },
            error : function(data){
                console.log(data);
            }
        });
    }

    function visit_checkDate(type,eventDate){
        var dateBegin = $('#visit_dateBegin').val();
        var dateEnd = $('#visit_dateEnd').val();
        var fixPath = (location.host == "localhost") ? '/medportal' : '';
        var url = 'http://' + location.host + fixPath + '/visit/checkDate';
        if((dateBegin)&&(dateEnd)){
            $.ajax({
                url: url,
                method: 'POST',
                data: 'dateBegin=' + dateBegin + '&dateEnd=' + dateEnd + '&idNurse='+$('#idNurse').val(),
                success: function (responce) {
                    var data = JSON.parse(responce);
                    if(data.message == 'ok') {
                        globalIsDateFree = true;
                        $('#nurseDateError').css('display','none');
                    }
                    else {
                        $('#nurseDateError').html(data.message);
                        $('#nurseDateError').css('display','block');
                    }
                },
                error: function(data){
                    console.log(data);
                }
            });
        }
    }

    function visit_timeToMinutes(beginTime, curTime){
        var beginHour = beginTime.split(':')[0];
        var beginMinutes = beginTime.split(':')[1];
        var curHour = curTime.split(':')[0];
        var curMinutes = curTime.split(':')[1];
        var totalMinutes = 0;
        var tempMinutes = 0;
        if(parseInt(beginMinutes) > 0) {
            totalMinutes += 60 - beginMinutes;
            beginHour++;
        }
        totalMinutes += 60 * (curHour - beginHour);
        totalMinutes += parseInt(curMinutes);
        return totalMinutes;
    }

    function visit_minutesToTime(beginTime, minutes){
        var beginHour = parseInt(beginTime.split(':')[0]);
        var beginMinutes = parseInt(beginTime.split(':')[1]);
        var endHour = Math.floor(minutes / 60);
        var endMinutes = minutes - endHour * 60;
        endHour = beginHour + endHour;
        endMinutes = beginMinutes + endMinutes;
        if(endMinutes == 60)
            endHour++;
        if(endMinutes > 60){
            var tempMinutes = endMinutes - 60;
            endMinutes = tempMinutes;
            endHour++;
        }
        if((endMinutes == 0)||(endMinutes == 60))
            endMinutes = '00';
        return endHour+':'+endMinutes;
    }

</script>