<?php
/* @var $this DoctorController */
/* @var $model Doctor */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    )); ?>

    <div class="row">
        <?php echo $form->label($model, 'idUser'); ?>
        <?php echo $form->textField($model, 'idUser'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'idSpecialization'); ?>
        <?php echo $form->textField($model, 'idSpecialization'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'name'); ?>
        <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'expirience'); ?>
        <?php echo $form->textArea($model, 'expirience', array('rows' => 6, 'cols' => 50)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'license'); ?>
        <?php echo $form->textField($model, 'license', array('size' => 60, 'maxlength' => 255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'description'); ?>
        <?php echo $form->textArea($model, 'description', array('rows' => 6, 'cols' => 50)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'merit'); ?>
        <?php echo $form->textArea($model, 'merit', array('rows' => 6, 'cols' => 50)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'specialization'); ?>
        <?php echo $form->textArea($model, 'specialization', array('rows' => 6, 'cols' => 50)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'education'); ?>
        <?php echo $form->textArea($model, 'education', array('rows' => 6, 'cols' => 50)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'price'); ?>
        <?php echo $form->textField($model, 'price'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'rate'); ?>
        <?php echo $form->textField($model, 'rate'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'typeHuman'); ?>
        <?php echo $form->textField($model, 'typeHuman', array('size' => 16, 'maxlength' => 16)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'isPhototherapy'); ?>
        <?php echo $form->textField($model, 'isPhototherapy'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'pic'); ?>
        <?php echo $form->textArea($model, 'pic', array('rows' => 6, 'cols' => 50)); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->