<?php
/* @var $this DoctorController */
/* @var $model Doctor */

?>

<h1><?php echo $model->name; ?></h1>
<div class="box box-info text-left">
    <?php $this->widget('zii.widgets.CDetailView', array(
        'data' => $model,
        'attributes' => array(
            array(
                'type'=>'raw',
                'value'=> CHtml::image(Yii::app()->request->baseUrl.'/upload/'.$model->pic,'', array('style'=>'width: 30%;')),
            ),
            'idUser',
            'idSpecialization',
            'name',
            'license',
            'merit',
            array(
                'label' => 'Опыт работы',
                'type'=>'raw',
                'value'=> strip_tags($model->expirience),
            ),
            'description',
            array(
                'label' => 'Специализация',
                'type'=>'raw',
                'value'=> strip_tags($model->specialization),
            ),
            array(
                'label' => 'Образование',
                'type'=>'raw',
                'value'=> strip_tags($model->education),
            ),
            array(
                'label' => 'Время приёма',
                'type'=>'raw',
                'value'=> strip_tags($model->timeSize).' минут',
            ),
            array(
                'label' => 'Активность врача',
                'type'=>'raw',
                'value'=> ($model->enabled) ? ' Активен' : 'Не активен',
            ),
            'price',
            'rate',
            'typeHuman',
            'isPhototherapy',

        ),
    )); ?>
</div>
