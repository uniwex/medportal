<?php
/* @var $this NurseController */
/* @var $model Nurse */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

                    <div class="row">
            <?php echo $form->label($model,'idUser'); ?>
            <?php echo $form->textField($model,'idUser'); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'name'); ?>
            <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'expirience'); ?>
            <?php echo $form->textArea($model,'expirience',array('rows'=>6, 'cols'=>50)); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'description'); ?>
            <?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'specialization'); ?>
            <?php echo $form->textArea($model,'specialization',array('rows'=>6, 'cols'=>50)); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'education'); ?>
            <?php echo $form->textArea($model,'education',array('rows'=>6, 'cols'=>50)); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'priceDay'); ?>
            <?php echo $form->textField($model,'priceDay'); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'priceMonth'); ?>
            <?php echo $form->textField($model,'priceMonth'); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'rate'); ?>
            <?php echo $form->textField($model,'rate'); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'residence'); ?>
            <?php echo $form->textField($model,'residence'); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'food'); ?>
            <?php echo $form->textField($model,'food'); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'categorySick'); ?>
            <?php echo $form->textField($model,'categorySick',array('size'=>60,'maxlength'=>255)); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'pic'); ?>
            <?php echo $form->textArea($model,'pic',array('rows'=>6, 'cols'=>50)); ?>
        </div>

        <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->