<?php
/* @var $this NurseController */
/* @var $model Nurse */
/* @var $form CActiveForm */
?>

<div class="form adminEditNurse">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'nurse-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'errorMessageCssClass' => 'control-label',
        'clientOptions' => array(
            'validateOnChange' => true,
            'validateOnSubmit' => true,
            'errorCssClass' => 'has-error',
            'successCssClass' => 'has-success'
        ),
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
            'onsubmit' => 'crutchForNurseCKEditor();return true',
        ),
    )); ?>

    <div class="box-body">
               <?php echo $form->errorSummary($model, 'Пожалуйста, исправьте следующие ошибки:', '', array('class' => 'callout callout-danger')); ?>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'idUser'); ?>
            <?php echo $form->dropDownList($model, 'idUser', CHtml::listData(User::model()->findAll('type = :id', array(':id' => 'nurse')), 'id', 'name'), array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'idUser'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'name'); ?>
            <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'name'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'expirience'); ?>
            <?php echo $form->textArea($model, 'expirience', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'expirience'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'description'); ?>
            <?php echo $form->textArea($model, 'description', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'description'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'specialization'); ?>
            <?php echo $form->textArea($model, 'specialization', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'specialization'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'education'); ?>
            <?php echo $form->textArea($model, 'education', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'education'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'priceHour'); ?>
            <?php echo $form->textField($model, 'priceHour', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'priceHour'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'priceDay'); ?>
            <?php echo $form->textField($model, 'priceDay', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'priceDay'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'priceMonth'); ?>
            <?php echo $form->textField($model, 'priceMonth', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'priceMonth'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'rate'); ?>
            <?php echo $form->dropDownList($model, 'rate', array(0 => '0',1 => '1', 2 => '2', 3 => '3',4 => '4',5 => '5'), array('class' => 'form-control'));  ?>
            <?php echo $form->error($model, 'rate'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'residence'); ?>
            <?php echo $form->dropDownList($model, 'residence', array(0 => 'Без проживания', 1 => 'С проживанием'), array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'residence'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'food'); ?>
            <?php echo $form->dropDownList($model, 'food', array(0 => 'Без питания', 1 => 'С питанием'), array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'food'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'categorySick'); ?>
            <?php echo $form->textField($model, 'categorySick', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'categorySick'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'image'); ?>
            <?php echo $form->fileField($model, 'image', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'image'); ?>
        </div>


        <div class="box-footer">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array('class' => 'btn btn-primary pull-right')); ?>
        </div>

        <?php $this->endWidget(); ?>

    </div>
    <!-- form -->
</div>
<script src="//cdn.ckeditor.com/4.5.4/standard/ckeditor.js"></script>
<script type="text/javascript">
    //CKEDITOR INITIALIZATION
    //NURSE
    CKEDITOR.replace('Nurse[education]');
    CKEDITOR.replace('Nurse[specialization]');
    CKEDITOR.replace('Nurse[expirience]');
    //call when form submit
    var crutchForNurseCKEditor = function(){
        var iframesNurse = $('.adminEditNurse iframe');
        for(var i=0;i<iframesNurse.length;i++) {
            var text = iframesNurse[i].contentDocument.childNodes[1].childNodes[1].childNodes[0].innerHTML;
            if (text != '<br>') {
                $(iframesNurse[i].parentNode.parentNode.parentNode.parentNode).find('textarea').val(text);
            } else
                $(iframesNurse[i].parentNode.parentNode.parentNode.parentNode).find('textarea').val('');
        }
    }
</script>