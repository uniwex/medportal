<?php
/* @var $this UserController */
/* @var $model User */

/*$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->name,
);*/

?>

<h1><?php echo $model->name; ?></h1>
<div class="box box-info text-left">
    <?php $this->widget('zii.widgets.CDetailView', array(
        'data' => $model,
        'attributes' => array(
            'id',
            'name',
            'phone',
            'password',
            'money',
            'city',
            'country',
            'email',
            'emailEnable',
            'type',
            'dateRegistration',
            'vipFinish',
            'status',
            'restoreCode',
            'isConfirm',
            'role',
        ),
    )); ?>
</div>
