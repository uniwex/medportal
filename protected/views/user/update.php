<?php
/* @var $this UserController */
/* @var $model User */

/*$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);*/
?>
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Изменение-<?= $model->name; ?></h3>
            </div>
            <?php $this->renderPartial('_form', array('model' => $model)); ?>
        </div>
    </div>
</div>