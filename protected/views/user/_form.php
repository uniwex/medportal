<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'user-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'errorMessageCssClass' => 'control-label',
        'clientOptions' => array(
            'validateOnChange' => true,
            'validateOnSubmit' => true,
            'errorCssClass' => 'has-error',
            'successCssClass' => 'has-success'
        ),

    )); ?>

    <div class="box-body">
        <?php echo $form->errorSummary($model, 'Пожалуйста, исправьте следующие ошибки:', '', array('class' => 'callout callout-danger')); ?>
        <div class="form-group">
            <?php echo $form->labelEx($model, 'name'); ?>
            <?php echo $form->textArea($model, 'name', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'name'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'phone'); ?>
            <?php echo $form->textField($model, 'phone', array('size' => 60, 'class' => 'form-control', 'maxlength' => 255)); ?>
            <?php echo $form->error($model, 'phone'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'password'); ?>
            <?php echo $form->passwordField($model, 'password', array('size' => 32, 'class' => 'form-control', 'maxlength' => 32)); ?>
            <?php echo $form->error($model, 'password'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'money'); ?>
            <?php echo $form->textField($model, 'money', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'money'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'city'); ?>
            <?php echo $form->textField($model, 'city', array('size' => 60, 'class' => 'form-control', 'maxlength' => 255)); ?>
            <?php echo $form->error($model, 'city'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'country'); ?>
            <?php echo $form->textField($model, 'country', array('size' => 60, 'class' => 'form-control', 'maxlength' => 255)); ?>
            <?php echo $form->error($model, 'country'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'email'); ?>
            <?php echo $form->textField($model, 'email', array('size' => 60, 'class' => 'form-control', 'maxlength' => 255)); ?>
            <?php echo $form->error($model, 'email'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'emailEnable'); ?>
            <?php echo $form->dropDownList($model, 'emailEnable', array(0 => 'Не показывать', 1 => 'Показывать',), array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'emailEnable'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'type'); ?>
            <?php echo $form->dropDownList($model, 'type', array('doctor' => 'Доктор', 'clinic' => 'Клиника', 'patient' => 'Пациент', 'nurse' => 'Сиделка'), array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'type'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'dateRegistration'); ?>
            <?php echo $form->textField($model, 'dateRegistration', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'dateRegistration'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'vipFinish'); ?>
            <?php echo $form->textField($model, 'vipFinish', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'vipFinish'); ?>
        </div>


        <div class="form-group">
            <?php echo $form->labelEx($model, 'restoreCode'); ?>
            <?php echo $form->textField($model, 'restoreCode', array('size' => 32, 'class' => 'form-control', 'maxlength' => 32)); ?>
            <?php echo $form->error($model, 'restoreCode'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'isConfirm'); ?>
            <?php echo $form->dropDownList($model, 'isConfirm', array(0 => 'Не одобренный', 1 => 'Одобренный',), array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'isConfirm'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'role'); ?>
            <?php echo $form->dropDownList($model, 'role', array('user' => 'Пользователь', 'admin' => 'Администратор',), array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'role'); ?>
        </div>

        <div class="box-footer">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array('class' => 'btn btn-primary pull-right')); ?>
        </div>

        <?php $this->endWidget(); ?>


        <!-- form -->
    </div>
</div>