<?php
/* @var $this UserController */
/* @var $model User */

/*$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Manage',
);*/

/*$this->menu=array(
array('label'=>'List User', 'url'=>array('index')),
array('label'=>'Create User', 'url'=>array('create')),
);*/

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$('#user-grid').yiiGridView('update', {
data: $(this).serialize()
});
return false;
});
");
?>

    <div class="box box-info">
        <div class="box-header with-border ui-sortable-handle">
            <h3 class="box-title">Список пользователей</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
            <?php $this->widget('zii.widgets.grid.CGridView', array(
                'id' => 'user-grid',
                'dataProvider' => $model->search(),
                'filter' => $model,
                'cssFile' => Yii::app()->request->getBaseUrl(true) . '/assets/admin/css/gridview.css',
                'itemsCssClass' => 'table no-margin',
                'htmlOptions' => array(
                    'class' => 'table-responsive'
                ),
                'summaryText' => 'Показано записей: {start} - {end} из {count}',
                'emptyText' => 'Результаты не найдены',
                'columns' => array(
                    'id',
                    'name',
                    array(
                        'name' => 'type',
                        'value' => 'User::typeUser("$data->type")',
                        'filter' => array(

                            'doctor' => 'Доктор',
                            'clinic' => 'Клиника',
                            'patient' => 'Пациент',
                            'nurse' => 'Сиделка'
                        )
                    ),

                    array(
                        'name' => 'isConfirm',
                        'value' => '$data->isConfirm == 1 ? CHtml::link(CHtml::encode("заблокировать"),
                            array("/user/modered?status=".$data->isConfirm."&id=".$data->id)):
                            CHtml::link(CHtml::encode("одобрить"),
                            array("/user/modered?status=".$data->isConfirm."&id=".$data->id))',
                            'htmlOptions' => array(

                        ),
                        'cssClassExpression'=>'$data->isConfirm != 1 ? "btn btn-warning stat" : "btn btn-info stat"',
                        'type' => 'raw',
                        'filter' => array(
                            '0' => 'не одобренные',
                            '1' => 'одобренные',
                        )
                    ),
                    'city',
                    array(
                        'name' => 'statistic',
                        'value' => 'CHtml::link(CHtml::encode("просмотр"),
                            array("/statistics/pay?Pay[idUser]=".$data->id."&ajax=yw0"))',
                        'htmlOptions' => array(
                            'class' => 'btn btn-success stat'
                        ),
                        'type' => 'raw',
                    ),


                    array(
                        'name' => 'countReferal',
                        'value' => 'User::CountReferal($data->id)',

                    ),

                    array(
                        'name' => 'referal',
                        'type' => 'raw',
                        'value' => 'CHtml::link(CHtml::encode("просмотр"),
                            array("/statistics/referral?id=".$data->id))',
                        'htmlOptions' => array(
                            'class' => 'btn btn-success stat'
                        ),

                    ),


                    /*array(
                        'name' => 'isConfirm',
                        'filter' => array(
                            '' => 'Все',
                            0 => 'Проверенные',
                            1 => 'Не проверенные'
                        )
                    ),*/

                    /*
                    'country',
                    'email',
                    'emailEnable',
                    'type',
                    'dateRegistration',
                    'vipFinish',
                    'status',
                    'restoreCode',
                    'isConfirm',
                    'role',
                    */
                    array(
                        'class'=>'CButtonColumn',
                        'template'=>'{view}{update}',
                    ),
                ),
            )); ?>
        </div>
        <div class="box-footer clearfix"></div>
    </div>


