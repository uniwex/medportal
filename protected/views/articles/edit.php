<div id="postAdvert" class="col-md-10">
    <?php $formArticle = $this->beginWidget("CActiveForm", array(
        'id' => 'edit-article',
        'action' => Yii::app()->request->baseUrl . '/articles/update',
        'method' => 'post',
//        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true
        ),
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
            'autocomplete' => 'off',
//            'onsubmit' => 'return true',
        ),
    )); ?>
    <div class="form-group row">
        <div class="col-md-2">
            <?= $formArticle->labelEx($articleModel, 'name') ?>
        </div>
        <div class="col-md-8">
            <?= $formArticle->textField($articleModel, 'name', array('class' => 'fieldInput', 'placeholder' => 'О сайте')); ?>
            <?= $formArticle->error($articleModel, 'name', array('class' => 'text-red')) ?>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-2">
            <?= $formArticle->labelEx($articleModel, 'image'); ?>
        </div>
        <div class="col-md-8">
            <?php
                if($articleModel->image) $image = Yii::app()->request->baseUrl.'/upload/articles/' . $articleModel->image; else $image = Yii::app()->request->baseUrl.'/assets/images/blankPhoto1.png';
            ?>
            <div id="image" class="fieldInput" <?= 'style="background-image:url('.$image.')"'; ?>)>
                <?= $formArticle->fileField($articleModel, 'image'); ?>
            </div>
            <?= $formArticle->labelEx($articleModel, 'Размер картинки не может превышать 5 MB') ?>
            <?= $formArticle->error($articleModel, 'image', array('class' => 'text-red')) ?>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-2">
            Категория
        </div>
        <div class="col-md-8">
            <?php
            array($articleModel->idCategory => array('selected'=>true));

            $arrayIdCategories = explode(',',$articleModel->idCategory);
            $arrayCategories = array();

            for($i=0;$i<count($arrayIdCategories);$i++){
                $arrayCategories[$arrayIdCategories[$i]] = array('selected'=>true);
            }
            ?>
            <?= $formArticle->dropDownList($categoryModel, 'name', CHtml::listData(Category::model()->findAll(), 'id', 'name', 'category'), array('class' => 'fieldInput', 'multiple'=>'multiple', 'options' => $arrayCategories)); ?>
            <?= $formArticle->error($categoryModel, 'name', array('class' => 'text-red')) ?>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-2">
            <?= $formArticle->labelEx($articleModel, 'text') ?>
        </div>
        <div class="col-md-8">
            <?= $formArticle->textArea($articleModel, 'text', array('class' => 'fieldInput')); ?>
            <?= $formArticle->error($articleModel, 'text', array('class' => 'text-red')) ?>
        </div>
    </div>
    <?= $formArticle->hiddenField($articleModel, 'id', array('class' => 'fieldInput', 'value'=>$articleModel->id)); ?>
    <div class="form-group row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <?= CHtml::submitButton('Сохранить статью', array('class' => 'button1 button1reg')); ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
<script src="//cdn.ckeditor.com/4.5.4/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('Article[text]');
</script>