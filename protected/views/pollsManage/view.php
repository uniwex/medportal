<?php
/* @var $this PollsManageController */
/* @var $model Polls */

$this->breadCrumbs=array(
	'Polls'=>array('index'),
	$model->name,
);

$this->menu=array(
array('label'=>'List Polls', 'url'=>array('index')),
array('label'=>'Create Polls', 'url'=>array('create')),
array('label'=>'Update Polls', 'url'=>array('update', 'id'=>$model->id)),
array('label'=>'Delete Polls', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Polls', 'url'=>array('admin')),
);
?>

<h1>Просмотр опросов #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
'data'=>$model,
'attributes'=>array(
		'id',
		'datePoll',
		'name',
		'description',
		'prize',
),
)); ?>
