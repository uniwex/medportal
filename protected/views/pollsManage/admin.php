<?php
/* @var $this PollsController */
/* @var $model Polls */

$this->breadCrumbs = array(
	'Опросы'=>array('index'),
	'Manage',
);
?>
<p>
	При поиске вы можете использоать знаки(<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
	или <b>=</b>) .
</p>
<?
$this->menu = array(
	array('label'=>'List Polls', 'url'=>array('index')),
	array('label'=>'Create Polls', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
	$('.search-button').click(function(){
		$('.search-form').toggle();
		return false;
	});
	$('.search-form form').submit(function(){
		$('#polls-grid').yiiGridView('update', {
			data: $(this).serialize()
		});
		return false;
	});
	");
?>

<h1>Управление опросами</h1>


<?php echo CHtml::link('Расширенный поиск','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
		'model'=>$model,
	)); ?>
</div><!-- search-form -->
<div class="row">
	<div class="col-md-12">
		<div class="box box-info">
			<div class="box-header with-border ui-sortable-handle">
				<h3 class="box-title">Список опросов</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove">
						<i class="fa fa-times"></i></button>
				</div>
			</div>
			<div class="box-body">
				<?php $this->widget('zii.widgets.grid.CGridView', array(
					'id'=>'polls-grid',
					'dataProvider'=>$model->search(),
					'filter'=>$model,
					'cssFile' => Yii::app()->request->getBaseUrl(true) . '/assets/admin/css/gridview.css',
					'itemsCssClass' => 'table no-margin',
					'htmlOptions' => array(
						'class' => 'table-responsive'
					),
					'columns'=>array(
						'id',
						'datePoll',
						'name',
						'description',
						'prize',
						array(
							'class'=>'CButtonColumn',
							'template'=>'{addQ}',
							'buttons'=>array(
								'addQ'=> array (
									'label'=>'Вопросы',
									'url'=>'Yii::app()->createUrl("/questions/admin", array("id"=>$data->id))',
								),
							),
						),
						array(
							'class'=>'CButtonColumn',
						),
					),
				)); ?>
			</div>
		</div>
	</div>
</div>
