<?php
/* @var $this PollsManageController */
/* @var $model Polls */

$this->breadCrumbs=array(
	'Polls'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'Список опросов', 'url'=>array('index')),
array('label'=>'Управление опросами', 'url'=>array('admin')),
);
?>

<h1>Создать опросы</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>