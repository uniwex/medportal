<?php
    /* @var $this PollsManageController */
    /* @var $model Polls */

$this->breadCrumbs=array(
	'Polls'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

    $this->menu=array(
    array('label'=>'List Polls', 'url'=>array('index')),
    array('label'=>'Create Polls', 'url'=>array('create')),
    array('label'=>'View Polls', 'url'=>array('view', 'id'=>$model->id)),
    array('label'=>'Manage Polls', 'url'=>array('admin')),
    );
    ?>

    <h1>Обновить опросы <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>