<?php
/* @var $this PollsManageController */
/* @var $dataProvider CActiveDataProvider */

$this->breadCrumbs=array(
	'Polls',
);

$this->menu=array(
array('label'=>'Create Polls', 'url'=>array('create')),
array('label'=>'Manage Polls', 'url'=>array('admin')),
);
?>

<h1>Опросы</h1>

<?php $this->widget('zii.widgets.CListView', array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
