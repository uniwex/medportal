<div class="doctor-block">
    <div class="col-lg-3 col-md-3 no-padding">
        <img src="/upload/<?echo CHtml::encode($doctorsList->pic); ?>" alt="" class="clinic-doctor-img">
    </div>
    <div class="col-lg-9 col-md-9 no-padding doctors-desc">
        <div class="doctor-header">
            <div class="col-lg-8 col-md-8">
                <a href="/site/doctor/<?=$doctorsList->idUser?>" class="doctor-name"><?echo CHtml::encode($doctorsList->name); ?></a>

                <p class="doctors-desc-text"><?echo CHtml::encode($specialization[$doctorsList->idSpecialization - 1]);?>,
                    стаж <?echo CHtml::encode($doctorsList->expirienceTime); ?> лет</p>
            </div>
            <div class="col-lg-4 col-md-4">
                <button type="button"  data-src="<?=$doctorsList->idUser?>" class="btn btn-order-doctor">Записаться</button>
            </div>
        </div>
        <div id="doctor-<?=$doctorsList->idUser;?>" class="doctor-body">
            <?=$doctorsList->expirience;?>
            <a id="a-expand-<?=$doctorsList->idUser;?>" class="a-expand"><b>Подробнее...</b></a>
        </div>
        <div class="modal fade bs-example-modal-lg exp-modal" id="exp-modal-<?=$doctorsList->idUser;?>" tabindex="-1" role="dialog"
             aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <p class="doctor-name"><?=$doctorsList->name?></p>
                        <?=$doctorsList->expirience;?>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $('#a-expand-<?=$doctorsList->idUser;?>').on('click', function () {
                $id = <?=$doctorsList->idUser;?>;
                $('#exp-modal-<?=$doctorsList->idUser;?>').modal('show');
            });
        </script>
    </div>
</div>
