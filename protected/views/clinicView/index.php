<link rel="stylesheet" href="../../../assets/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<script type="text/javascript" src="../../../assets/datetimepicker/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="../../../assets/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

<div class="right-menu-relative" xmlns="http://www.w3.org/1999/html">
    <? require("protected/views/site/rightMenu.php"); ?>
</div>
<div class="center-clinic">
    <div class="clinic-header">
        <div class="col-md-4 col-lg-4 no-padding">
            <div class="clinic-pic" style="background-image:url('/upload/<?= $model->pic; ?>')">
            </div>
            <div class="col-md-12 col-sm-12 profile-stars">
                <?php
                for ($i = 0; $i < $model->rate; $i++) {
                    echo Yii::app()->params['goldStarBig'];
                }
                for ($i = 0; $i < 5 - $model->rate; $i++) {
                    echo Yii::app()->params['grayStarBig'];
                } ?>
            </div>
        </div>
        <div class="col-md-8 col-lg-8 no-padding clinic-desc">
            <p id="clinic-name"><?= $model->name; ?></p>

            <p><?= $model->contactAdvanced; ?></p>

            <p><a href="#">Показать на карте</a></p>
            <br>

            <p><?= $model->description ?></p>
        </div>
    </div>
    <ul class="nav nav-tabs clinic-tab">
        <li class="active"><a data-toggle="tab" href="#panel1">Специализация</a></li>
        <li><a data-toggle="tab" href="#panel2">Запись на прием</a></li>
        <li><a data-toggle="tab" href="#panel3">Врачи клиники</a></li>
        <li><a data-toggle="tab" href="#panel4">Фото и видео</a></li>
        <li><a data-toggle="tab" href="#panel5">Часы работы</a></li>
        <li><a data-toggle="tab" href="#panel6">Контакты</a></li>
    </ul>

    <div class="tab-content clinic-tab-content">
        <div id="panel1" class="tab-pane fade in active">
            <p class="panel-header">Клиника предоставляет услуги по следующим направлениям:</p>

            <div id="panel1-line"></div>
            <div id="panel1-text" class="column">
                <?= $model->specialization ?>
            </div>
        </div>
        <div id="panel2" class="tab-pane fade">
            <p>Запись к доктору</p>

            <div class="order-block">
                <div class="col-lg-4 col-md-4">
                    <div class="form-group">
                        <label class="select-label" for="sel1">Выберите специализацию:</label>
                        <select class="form-control select-clinic order-doctor" id="sel1">
                            <option value="null"></option>
                            <? foreach ($specialization as $key => $value) { ?>
                                <option value="<?= $key + 1 ?>"><?= $value ?></option>
                            <? } ?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="form-group">
                        <label class="select-label" for="sel2">Выберите возрастной прием:</label>
                        <select class="form-control select-clinic order-doctor" id="sel2">
                            <option value="null"></option>
                            <option value="Все">Все</option>
                            <option value="Взрослые">Взрослые</option>
                            <option value="Дети">Дети</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="form-group">
                        <label class="select-label" for="sel3">Выберите врача:</label>
                        <select class="form-control select-clinic" id="sel3">
                            <option value="null"></option>
                            <? foreach ($model->doctors as $value) {
                                $this->renderPartial('_orderDoctor', array('value' => $value));
                            } ?>
                        </select>
                    </div>
                    <script>
                        $('.order-doctor').on('change', function () {
                            $.ajax({
                                type: "GET",
                                url: "doctorChange/?specialization=" + $('#sel1 > option:selected').val()
                                + "&typeHuman=" + $('#sel2 > option:selected').val() + "&idClinic=" +<?=$idClinic?>,
                            }).done(function (data) {
                                $('#sel3').html(data);
                            });
                        });
                    </script>
                </div>
                <div class="col-lg-4 col-md-4">
                    <p>Дата:</p>

                    <div class="input-group date col-lg-12 col-md-12">
                        <input type="text" id="datetimepicker1" class="form-control"/>
                    </div>
                    <script type="text/javascript">
                        $(function () {
                            $('#datetimepicker1').datetimepicker({
                                minDate: moment(),
                                pickTime: false,
                                language: 'ru',
                                format: 'YYYY-MM-DD',
                            });
                        });
                    </script>
                </div>
                <div class="col-lg-4 col-md-4">
                    <p>Время:</p>

                    <div class="input-group date col-lg-12 col-md-12">
                        <input type="text" id="datetimepicker2" class="form-control"/>
                    </div>
                    <script type="text/javascript">
                        $(function () {
                            $('#datetimepicker2').datetimepicker({pickDate: false, language: 'ru'});
                        });
                    </script>
                </div>
                <div class="col-lg-4 col-md-4">
                    <button type="submit" class="btn button-checkin-doctors">Записаться</button>
                </div>
                <div class="modal fade bs-example-modal-md" id="order-modal" tabindex="-1" role="dialog"
                     aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-md" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                                <p id="order-modal-text">Вы успешно записались на прием к доктору.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <script>
                    $('.button-checkin-doctors').on('click', function () {
                        $auth = '<?=Yii::app()->user->isGuest?>';
                        if ($auth) {
                            location.href='/site/login';
                        } else {
                            if (($('#datetimepicker1').val() == '') || ($('#datetimepicker2').val() == '') || ($('#sel3 > option:selected').val() == 'null') ) {
                                alert("Не все поля заполнены. Выберите врача, укажите дату и время");
                            } else {
                                $.ajax({
                                    type: "GET",
                                    url: "order/?idExecuter=" + <?=$idClinic?> + "&eventDate=" + $('#datetimepicker1').val()
                                    + "&eventTime=" + $('#datetimepicker2').val() + "&idDoctor="+$('#sel3 > option:selected').val(),
                                });
                                $('#order-modal').modal('show');
                            }
                        }
                    });
                </script>
            </div>
        </div>
        <div id="panel3" class="tab-pane fade">
            <div class="col-lg-12 col-md-12 no-padding">
                <div class="col-lg-3 col-md-3 no-left-padding">
                    <div class="form-group">
                        <label class="select-label" for="doctorName">ФИО Врача</label>
                        <select class="form-control select-clinic" id="doctorName">
                            <option value="null"></option>
                            <? foreach ($model->doctors as $value) { ?>
                                <option value="<?= $value->idUser ?>"><?= $value->name ?></option>
                            <? } ?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 no-left-padding">
                    <div class="form-group">
                        <label class="select-label" for="doctorSpec">Специальность врача</label>
                        <select class="form-control select-clinic" id="doctorSpec">
                            <option value="null"></option>
                            <? foreach ($specialization as $key => $value) { ?>
                                <option value="<?= $key + 1 ?>"><?= $value ?></option>
                            <? } ?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 no-left-padding">
                    <div class="form-group">
                        <label class="select-label" for="doctorHuman">Возрастной прием</label>
                        <select class="form-control select-clinic" id="doctorHuman">
                            <option value="null"></option>
                            <option value="Все">Все</option>
                            <option value="Взрослые">Взрослые</option>
                            <option value="Дети">Дети</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 no-left-padding">
                    <button type="button" id="find-doctors" class="btn button-find-doctors"><span>Найти</span></button>
                </div>
            </div>
            <script>
                $('#find-doctors').on('click', function () {
                    $.ajax({
                        type: "GET",
                        url: "search/?idUser=" + $('#doctorName > option:selected').val() + "&specialization=" + $('#doctorSpec > option:selected').val()
                        + "&typeHuman=" + $('#doctorHuman > option:selected').val() + "&idClinic=" +<?=$idClinic?>,
                    }).done(function (data) {
                        $('#doctors-list').html(data);
                    });
                });
            </script>
            <div id="doctors-list" class="col-lg-12 col-md-12 no-padding">
                <? foreach ($doctorsList as $value) {
                    $this->renderPartial('_doctors', array('doctorsList' => $value, 'specialization' => $specialization));
                } ?>
            </div>

            <div class="modal fade bs-example-modal-lg" id="order-doctors-modal" data-src="" tabindex="-1" role="dialog"
                 aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="order-block">
                                <div class="col-lg-4 col-md-4">
                                    <p>Дата:</p>

                                    <div class="input-group date col-lg-12 col-md-12">
                                        <input type="text" id="datetimepicker3" class="form-control"/>
                                    </div>
                                    <script type="text/javascript">
                                        $(function () {
                                            $('#datetimepicker3').datetimepicker({
                                                minDate: moment(),
                                                pickTime: false,
                                                language: 'ru',
                                                format: 'YYYY-MM-DD',
                                            });
                                        });
                                    </script>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <p>Время:</p>

                                    <div class="input-group date col-lg-12 col-md-12">
                                        <input type="text" id="datetimepicker4" class="form-control"/>
                                    </div>
                                    <script type="text/javascript">
                                        $(function () {
                                            $('#datetimepicker4').datetimepicker({pickDate: false, language: 'ru'});
                                        });
                                    </script>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <button type="submit" id="order-final" class="btn">Записаться</button>
                                </div>
                                <script>
                                    $('#order-final').on('click', function () {
                                        if (($('#datetimepicker3').val() == '') || ($('#datetimepicker4').val() == '') || ($('#sel6 > option:selected').val() == 'null') ) {
                                            alert("Не все поля заполнены. Укажите дату и время");
                                        } else {
                                            $.ajax({
                                                type: "GET",
                                                url: "order/?idExecuter=" + <?=$idClinic?> + "&eventDate=" + encodeURIComponent($('#datetimepicker3').val())
                                                + "&eventTime=" + encodeURIComponent($('#datetimepicker4').val()) + "&idDoctor="+$('#order-doctors-modal').attr('data-src'),
                                            });
                                            $('#order-doctors-modal').modal('hide');
                                            alert("Вы успешно записались к доктору!");
                                        }
                                    });
                                </script>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                $('.btn-order-doctor').on('click', function () {
                    $auth = '<?=Yii::app()->user->isGuest?>';
                    if ($auth) {
                        location.href='/site/login';
                    } else {
                        $('#order-doctors-modal').attr('data-src', $(this).attr('data-src'));
                        $('#order-doctors-modal').modal('show');
                    }
                });
            </script>
        </div>
        <div id="panel4" class="tab-pane fade">
            <? if ($model->virtual != '') { ?>
                <div class="col-md-6 col-lg-6">
                    <div id="ddd" class="media-container">
                        <p class="panel-header">Виртуальный тур по клинике:</p>
                        <? preg_match('/([\w]+)\.html/', $model->virtual, $str);
                        $path = preg_replace('/([\w]+)\.html/', '', $model->virtual);
                        $path = $path . $str[1] . 'data/thumbnail.jpg'
                        ?>
                        <a href="javascript:void(0);" data-src="<?= $model->virtual; ?>" class="clinic-link"></a>
                        <img class="clinic-img" src="<?= $path ?>" alt="">
                    </div>
                </div>
            <? } ?>
            <? if ($model->panorama != '') { ?>
                <div class="col-md-6 col-lg-6">
                    <div class="media-container clinic-video">
                        <p class="panel-header">Панорамная экскурсия 360&#176;</p>

                        <div class="clinic-play"></div>
                        <a href="javascript:void(0);"
                           data-videosrc="https://www.youtube.com/embed/<?= $model->panorama; ?>"
                           class="clinic-link"></a>
                        <img class="clinic-img" src="http://img.youtube.com/vi/<?= $model->panorama ?>/hqdefault.jpg"
                             alt="">
                    </div>
                </div>
            <? } ?>
            <? foreach ($model->media as $value) {
                if ($value->type == 'video') { ?>
                    <div class="col-md-6 col-lg-6">
                        <div class="media-container clinic-video">
                            <p class="panel-header"><?= $value->name; ?></p>

                            <div class="clinic-play"></div>
                            <a href="javascript:void(0);"
                               data-videosrc="https://www.youtube.com/embed/<?= $value->src; ?>"
                               class="clinic-link"></a>
                            <img class="clinic-img" src="http://img.youtube.com/vi/<?= $value->src ?>/hqdefault.jpg"
                                 alt="">
                        </div>
                    </div>
                    <?
                }
                if ($value->type == 'photo') { ?>
                    <div class="col-md-6 col-lg-6">
                        <div class="media-container clinic-photo">
                            <p class="panel-header"><?= $value->name; ?></p>
                            <a href="javascript:void(0);" class="clinic-link"></a>
                            <img class="clinic-img" src="../../../upload/clinic/<?= $value->src ?>" alt="">
                        </div>
                    </div>
                    <?
                }
            }
            ?>
            <div class="modal fade bs-example-modal-lg" id="youtube-modal" tabindex="-1" role="dialog"
                 aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <iframe width="100%" height="480" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                $(function () {
                    $('.clinic-video>a').on('click', function () {
                        var src = $(this).data('videosrc');
                        $('#youtube-modal iframe').attr('src', src);
                        $('#youtube-modal').modal('show');
                    });
                });
                $('#youtube-modal').on('hidden.bs.modal', function () {
                    $('#youtube-modal iframe').removeAttr('src');
                })
            </script>
            <div class="modal fade bs-example-modal-lg" id="ddd-modal" tabindex="-1" role="dialog"
                 aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <iframe width="100%" height="480" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                $(function () {
                    $('#ddd>a').on('click', function () {
                        var src = $(this).data('src');
                        $('#ddd-modal iframe').attr('src', src);
                        $('#ddd-modal').modal('show');
                    });
                });
                $('ddd-modal').on('hidden.bs.modal', function () {
                    $('#ddd-modal iframe').removeAttr('src');
                })
            </script>
            <div class="modal fade bs-example-modal-lg" id="imagemodal" tabindex="-1" role="dialog"
                 aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <img src="" class="imagepreview" style="max-width: 100%; width: auto;"/>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                $(function () {
                    $('.clinic-photo>a').on('click', function () {
                        $('.imagepreview').attr('src', $(this).next().attr('src'));
                        $('#imagemodal').modal('show');
                    });
                });
            </script>
        </div>
        <div id="panel5" class="tab-pane fade">
            <p class="panel-header">Часы приема:</p>

            <div id="clinic-hours">
                <div class="col-md-6 col-lg-6 time-block">
                    <? foreach ($model->hours as $value) {
                        ?><p><?= $value->day ?></p>
                        <?
                    } ?>
                </div>
                <div class="col-md-6 col-lg-6 time-block">
                    <? foreach ($model->hours as $value) {
                        if (($value->timeFrom == '00:00:00') || ($value->timeTo == '00:00:00')) {
                            ?><p class="clinic-time">Выходной</p><?
                        } else {
                            ?><p class="clinic-time">c <?= substr($value->timeFrom, 0, -3); ?>
                            по <?= substr($value->timeTo, 0, -3); ?></p><?
                        }
                    } ?>
                </div>
            </div>
        </div>
        <div id="panel6" class="tab-pane fade">
            <p class="panel-header">Контакты клиники:</p>

            <div id="clinic-contacts">
                <div class="col-md-6 col-lg-6 time-block">
                    <? foreach ($model->info as $value) {
                        ?><p><?= $value->name ?>:</p>
                        <?
                    } ?>
                    <? if ($model->site != '') { ?>
                        <p>Сайт:</p>
                    <? } ?>
                </div>
                <div class="col-md-6 col-lg-6 time-block">
                    <? foreach ($model->info as $value) {
                        ?><p class="clinic-time"><?= $value->value ?></p>
                        <?
                    } ?>
                    <? if ($model->site != '') { ?>
                        <a href="<?= $model->site ?>" class="clinic-time"><?= $model->site ?></a>
                    <? } ?>
                </div>
            </div>
        </div>
    </div>
</div>

