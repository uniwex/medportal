<?php
/* @var $this LightArticleController */
/* @var $dataProvider CActiveDataProvider */

$this->breadCrumbs=array(
	'Light Articles',
);

$this->menu=array(
array('label'=>'Create LightArticle', 'url'=>array('create')),
array('label'=>'Manage LightArticle', 'url'=>array('admin')),
);
?>

<h1>Облегченные статьи</h1>

<?php $this->widget('zii.widgets.CListView', array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
