<?php
/* @var $this LightArticleController */
/* @var $model LightArticle */

$this->breadCrumbs=array(
	'Light Articles'=>array('index'),
	$model->name,
);

$this->menu=array(
array('label'=>'List LightArticle', 'url'=>array('index')),
array('label'=>'Create LightArticle', 'url'=>array('create')),
array('label'=>'Update LightArticle', 'url'=>array('update', 'id'=>$model->id)),
array('label'=>'Delete LightArticle', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Вы точно хотите удалить эту запись?')),
array('label'=>'Manage LightArticle', 'url'=>array('admin')),
);
?>

<h1>Просмотр облегченных статей<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
'data'=>$model,
'attributes'=>array(
		'id',
		'name',
		'text',
		'idCountry',
),
)); ?>
