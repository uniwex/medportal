<?php
/* @var $this LightArticleController */
/* @var $model LightArticle */

$this->breadCrumbs=array(
	'Облегченные статьи'=>array('index'),
	'Manage',
);
?>
<p>
При поиске вы можете использоать знаки(<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
    or <b>=</b>) .
</p>
<?
$this->menu=array(
array('label'=>'List LightArticle', 'url'=>array('index')),
array('label'=>'Create LightArticle', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$('#light-article-grid').yiiGridView('update', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Управление облегченными статьями</h1>

<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<div class="row">
	<div class="col-md-12">

		<div class="box box-info">
			<div class="box-header with-border ui-sortable-handle">
				<h3 class="box-title">Список облегченных статей</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove">
						<i class="fa fa-times"></i></button>
				</div>
			</div>
			<div class="box-body">
<?php $this->widget('zii.widgets.grid.CGridView', array(
'id'=>'light-article-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'cssFile' => Yii::app()->request->getBaseUrl(true) . '/assets/admin/css/gridview.css',
'itemsCssClass' => 'table no-margin',
'htmlOptions' => array(
	'class' => 'table-responsive'
),
'columns'=>array(
		'id',
		'name',
		'text',
		'idCountry',
array(
'class'=>'CButtonColumn',
),
),
)); ?>
			</div>
		</div>
	</div>
</div>