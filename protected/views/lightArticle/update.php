<?php
    /* @var $this LightArticleController */
    /* @var $model LightArticle */

$this->breadCrumbs=array(
	'Light Articles'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

    $this->menu=array(
    array('label'=>'List LightArticle', 'url'=>array('index')),
    array('label'=>'Create LightArticle', 'url'=>array('create')),
    array('label'=>'View LightArticle', 'url'=>array('view', 'id'=>$model->id)),
    array('label'=>'Manage LightArticle', 'url'=>array('admin')),
    );
    ?>

    <h1>Обновить облегченные статьи <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'countries'=>$countries)); ?>