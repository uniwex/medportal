<?php
/* @var $this LightArticleController */
/* @var $model LightArticle */

$this->breadCrumbs=array(
	'Light Articles'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List LightArticle', 'url'=>array('index')),
array('label'=>'Manage LightArticle', 'url'=>array('admin')),
);
?>

<h1>Добавить облегченную статью</h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'countries'=>$countries)); ?>