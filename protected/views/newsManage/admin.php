<?php
/* @var $this NewsManageController */
/* @var $model News */

$this->breadCrumbs = array(
    'Новости' => array('index'),
    'Управление',
);

$this->menu = array(
    array('label' => 'Список новостей', 'url' => array('index')),
    array('label' => 'Добавить новость', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$('#news-grid').yiiGridView('update', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Управление списком новостей</h1>

<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search', array(
        'model' => $model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'news-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'date',
        'header',
        'text',
        array(
            'class' => 'CButtonColumn',
        ),
    ),
)); ?>
