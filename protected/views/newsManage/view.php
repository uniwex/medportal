<?php
/* @var $this NewsManageController */
/* @var $model News */

$this->breadCrumbs = array(
    'Новости' => array('index'),
    $model->id,
);

$this->menu = array(
    array('label' => 'Список новостей', 'url' => array('index')),
    array('label' => 'Добавить новость', 'url' => array('create')),
    array('label' => 'Обновить новость', 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Удалить новость', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Управление новостями', 'url' => array('admin')),
);
?>

<h1>Просмотр новости #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'date',
        'header',
        'text',
    ),
)); ?>
