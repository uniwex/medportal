<?php
/* @var $this NewsManageController */
/* @var $model News */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'news-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    )); ?>

    <div class="row">
        <div class="col-md-6 createGood">
            <div class="box box-primary">
                <div class="box-header with-border">
                </div>
                <div class="box-body">

                    <?php echo $form->errorSummary($model); ?>

                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'date'); ?><br>
                        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'name' => 'date',
                            'model' => $model,
                            'attribute' => 'date',
                            'language' => 'ru',
                            'options' => array(
                                'showAnim' => 'fold',
                                'dateFormat' => 'yy-mm-dd',
                            ),
                            'htmlOptions' => array(
                                'style' => 'height:26px;'
                            ),
                        ));?>
                        <?php echo $form->error($model, 'date'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'header'); ?><br>
                        <?php echo $form->textField($model, 'header', array('rows' => 6, 'cols' => 50)); ?>
                        <?php echo $form->error($model, 'header'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'text'); ?>
                        <?php echo $form->textArea($model, 'text', array('rows' => 6, 'cols' => 50)); ?>
                        <?php echo $form->error($model, 'text'); ?>
                    </div>

                    <div class="row buttons text-center">
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>

</div><!-- form -->
<script src="//cdn.ckeditor.com/4.5.4/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('News[text]');
    var crutchForCKEditor = function () {
        var abc = document.getElementsByTagName('iframe')[0].contentDocument;
        var abc2 = abc.childNodes[1];
        var text = abc2.childNodes[1].childNodes[0].innerHTML;
        if (text != '<br>')
            $('#newsManage_text').val(text);
    }
</script>