<?php
    /* @var $this NewsManageController */
    /* @var $model News */

$this->breadCrumbs=array(
	'Новости'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Обновить',
);

    $this->menu=array(
    array('label'=>'Список новостей', 'url'=>array('index')),
    array('label'=>'Добавить новость', 'url'=>array('create')),
    array('label'=>'Просмотр новостей', 'url'=>array('view', 'id'=>$model->id)),
    array('label'=>'Управление новостями', 'url'=>array('admin')),
    );
    ?>

    <h1>Обновить новость <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>