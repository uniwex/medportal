<div class="col-md-12 search-block-vip">
    <?
    $typeHuman = array(
        'Дети' => 'Ведет прием взрослых',
        'Взрослые' => 'Ведет прием взрослых и детей'
    );
    $user = User::model()->findByPk($data->id);
    $views = $user->views;
    $user->views = $views+1;
    $user->update(array('views'));
    ?>
    <div class="row">
        <div class="col-md-3 text-left search-profiles no-padding">


            <a href="/site/<?=$related?>/<?= $data->id ?>">
                <?php
                $img = '/assets/images/articles-overlay.png';
                if ($data->$related->pic) {
                    if ($data->$related->pic != '')
                        $img = '/upload/' . $data->$related->pic;
                }
                ?>
                <div class="pictureBlock" style="background-image: url('<?= $img; ?>');"></div>
            </a>

            <? for ($i = 0; $i < 5; $i++) { ?>
                <? if($i > $data->$related->rate - 1) {
                    echo Yii::app()->params['grayStar'];
                } else {
                    echo Yii::app()->params['goldStar'];
                }
            } ?>

        </div>
        <div class="col-md-6" style="text-overflow:ellipsis;overflow:hidden;height:190px;">
            <div class="userInformationHead1 no-margin">
                <a href="/site/<?=$related?>/<?= $data->id ?>"><?= $data->name ?></a>
            </div>
            <div>
                <?
                    if($data->type == 'clinic') echo '<p><a href="http://'.$data->$related->site.'" target="_blank">'.$data->$related->site.'</a></p>';
                    if($data->type == 'doctor') {
                        $idSpecialization = Doctor::model()->findByAttributes(array('idUser'=>$data->id))->idSpecialization;
                        echo Specialization::model()->findByPk($idSpecialization)->name.', '.$data->$related->expirience;
                    }
                    if($data->type == 'nurse') echo $data->$related->specialization.', '.$data->$related->expirience.'.';
                ?>
            </div>
            <? if($data->type == 'doctor') { ?>
            <div>
                <h5><b><?= $typeHuman[$data->$related->typeHuman] ?></b></h5>
            </div>
            <? } ?>
            <div class="indent text-orange">
                <?
                    $station = explode(',', $data->station->name);
                    for($i = 0; $i < count($station); $i++) {
                        echo '<div class="searchOfMetro iconMetro2">М</div> '.$station[$i].' ';
                    }
                ?>
            </div>
            <div>
                <?= $data->$related->description ?>
            </div>
        </div>
        <?php
            $userType = $data->type;
            $dataId = null;
            $timeSize = '';
            $price = '';
            if(($userType == 'clinic')||($userType == 'nurse')) {
                $dataId = $data->$related->idUser;
            }
            if($userType == 'doctor') {
                $dataId = Schedule::model()->findByAttributes(array('idUser'=>$data->id))->id;
                $timeSize = Schedule::model()->findByAttributes(array('idUser'=>$data->id))->timeSize;
                $price = Schedule::model()->findByAttributes(array('idUser'=>$data->id))->price;
            }
            $buttonClass = '';
            if($userType == 'clinic')
                $buttonClass = 'clinicDoctor';
            if($userType == 'doctor')
                $buttonClass = 'privateDoctor';
            if($userType == 'nurse')
                $buttonClass = 'nurse';

        ?>
        <div class="col-md-3 text-center">
            <h1 class="text-blue no-margin"><b><?= count($data->review) ?></b></h1>
            <p class="text-blue">отзывов</p>
            <?php
            if($userType == 'doctor'){
                if($doctorIsEnabled == false){
                    echo "<div class='button1 indent' data-id='$dataId' style='cursor: default;background-color:#EE7600;font-size:17px;width:127px;'>Врач временно не ведёт приём</div>";
                } else {
                    if ($timeSize == 0) {
                        echo '';
                    } else {
                        if ((!Yii::app()->user->isGuest)) {
                            if (Yii::app()->user->id != $data->id) {
                                echo "<div class='button1 indent $buttonClass' data-id='$dataId' style='background-color:#EE7600;font-size:17px;width:127px;'><i class='fa fa-plus'></i> Записаться</div>";
                            }
                        } ?>
                        <?php if (Yii::app()->user->isGuest) { ?>
                            <a href="/site/registration">
                                <div class="button1 indent"
                                     style="background-color: #EE7600;font-size: 17px;width: 127px;">
                                    <i class="fa fa-plus"></i> Записаться
                                </div>
                            </a>
                        <?php
                        }
                    }
                }
            }?>
            <?php if(($userType == 'doctor')&&($price == 0)) {
                echo '';
            } else { ?>
                <div>
                    <span class="priem">Прием:</span><b class="text-orange">
                        <?
                        $postCountry = "";
                        if(isset($_POST['country']))
                            $postCountry = $_POST['country'];
                        if($data->type == 'doctor') {
                            echo $data->$related->price . ' р';
                            if ($postCountry == 'Израиль')
                                echo ' (' . $data->$related->price * (0.0584) . ' шекелей)';
                        }
                        elseif($data->type == 'nurse') {
                            echo $data->$related->priceDay.' / '.$data->$related->priceMonth;
                            if ($postCountry == 'Израиль')
                                echo ' (' . $data->$related->priceDay * (0.0584) . ' / ' . $data->$related->priceMonth * (0.0584) . ' шекелей)';
                        }
                        elseif($data->type == 'clinic') {
                            $price = $data->getPrice();
                            echo $price[0].' - '.$price[1] . ' р';
                            if ($postCountry == 'Израиль')
                                echo ' (' . $price[0] * (0.0584) . ' - ' . $price[1] * (0.0584) . ' шекелей)';
                        }
                        ?>
                    </b>
                </div>
            <?php } ?>
        </div>

    </div>
</div>