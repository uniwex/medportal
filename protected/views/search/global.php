<?php
/* @var $this TestController */
/* @var $dataProvider CActiveDataProvider */
?>
<link href='<?= Yii::app()->request->baseUrl ?>/assets/styles/rating.css' media="screen" rel="stylesheet"
      type="text/css"/>
<div id="postAdvert" class="col-md-10 no-padding">
    <div class="row">
        <div class="col-md-12 indent">
            <p>Показать только</p>
            <? echo CHtml::dropDownList('view_type', Yii::app()->getRequest()->getParam('type'),
                array(
                    Search::ALL => 'Всех',
                    Search::DOCTOR => 'Докторов',
                    Search::CLINIC => 'Клинки',
                    Search::NURSE => 'Сиделок',
                    Search::ARTICLE => 'Статьи'), array(
                'class' => 'fieldInput',
            )); ?>
        </div>
        <div class="col-md-12">
            <?php $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $dataProvider,
                'itemView' => '_view',
                'template' => '{sorter}{items}{pager}',
                'id' => 'globallistview',
            )); ?>
        </div>
    </div>
</div>
<input type="hidden" id="search_value" value="<?=Yii::app()->getRequest()->getParam('text')?>">
<script>
    $('#view_type').on('change', function() {
        document.location.href = "/search/global?type="+encodeURIComponent($('#view_type').val())
            +"&text="+encodeURIComponent($('#search_value').val());
    });
</script>
