<?php
/* @var $this QuestionsController */
/* @var $data Questions */
?>

<div class="view">

    	<b><?php echo CHtml::encode($data->getAttributeLabel('idQuestion')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idQuestion), array('view', 'id'=>$data->idQuestion)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idPoll')); ?>:</b>
	<?php echo CHtml::encode($data->idPoll); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('text')); ?>:</b>
	<?php echo CHtml::encode($data->text); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />


</div>