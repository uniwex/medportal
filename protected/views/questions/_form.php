<?php
/* @var $this QuestionsController */
/* @var $model Questions */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'questions-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <div class="row">
        <div class="col-md-6 createGood">
            <div class="box box-primary">
                <div class="box-header with-border">
                </div>
                <div class="box-body">

                    <?php echo $form->errorSummary($model); ?>

                    <div class="form-group">
                        <?php echo $form->hiddenField($model,'idPoll',['value'=>$id]); ?>
                        <?php echo $form->error($model,'idPoll'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($model,'text'); ?><br>
                        <?php echo $form->textArea($model,'text',array('rows'=>6, 'cols'=>50)); ?>
                        <?php echo $form->error($model,'text'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($model,'description'); ?><br>
                        <?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
                        <?php echo $form->error($model,'description'); ?>
                    </div>

                    <button onclick="addQuestion()" id="addAnswer" type="button">Добавить ответ</button><br><br>
                    <div id="allAnswers">
                        <? if (isset($answers)) {
                            foreach ($answers as $data) {?>
                                <label class="answer<?=$data->idAnswer;?>">Ответ</label>
                                <div class="form-group">
                                    <input class="answer<?=$data->idAnswer;?>" name="Answers[answer<?=$data->idAnswer;?>]" type="text" value="<?=$data->text;?>"/>
                                    <a onclick="removeAnswer(this)" id="answer<?=$data->idAnswer;?>" class="fa fa-ban delete-fa"></a>
                                </div>
                            <?}
                        } else { ?>
                            <label class="answer1">Ответ</label>
                            <div class="form-group">
                                <input class="answer1" name="Answers[answer1]" type="text"/>
                                <a onclick="removeAnswer(this)" id="answer1" class="fa fa-ban delete-fa"></a>
                            </div>
                        <?}?>
                    </div>
                    <div class="row buttons text-center">
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>

</div><!-- form -->
<script src="//cdn.ckeditor.com/4.5.4/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('Questions[description]');
    var crutchForCKEditor = function(){
        var abc = document.getElementsByTagName('iframe')[0].contentDocument;
        var abc2 = abc.childNodes[1];
        var text = abc2.childNodes[1].childNodes[0].innerHTML;
        if(text != '<br>')
            $('#Questions_description').val(text);
    }
    <? if (isset($answers)) {?>
        var iAnswer = parseInt("<? echo($data->idAnswer)?>") + 1;
    <?} else {?>
        var iAnswer = 2;
    <?}?>
    function removeAnswer(object) {
        $('.'+$(object).attr('id')).remove();
        $(object).remove();
    }
    function addQuestion() {
        $('#allAnswers').append('<label class="answer'+iAnswer+'">Ответ</label> <div class="form-group"> <input  class="answer'+iAnswer+'" name="Answers[answer'+iAnswer+']" type="text" > <a onclick="removeAnswer(this)" id="answer'+iAnswer+'" class="fa fa-ban delete-fa"></a> </div>   ');
        iAnswer++;
    }
</script>