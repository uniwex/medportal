<?php
    /* @var $this QuestionsController */
    /* @var $model Questions */

$this->breadCrumbs=array(
	'Questions'=>array('index'),
	$model->idQuestion=>array('view','id'=>$model->idQuestion),
	'Update',
);

    $this->menu=array(
    array('label'=>'Список вопросов', 'url'=>array('index')),
    array('label'=>'Создать вопрос', 'url'=>array('create')),
    array('label'=>'Просмотр вопросов', 'url'=>array('view', 'id'=>$model->idQuestion)),
    array('label'=>'Управление вопросоами', 'url'=>array('admin')),
    );
    ?>

    <h1>Изменить вопросы для <?php echo $model->text; ?></h1>

<? $this->renderPartial('_form', array('model'=>$model, 'answers'=>$answers, 'id'=>$id)); ?>