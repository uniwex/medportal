<?php
/* @var $this QuestionsController */
/* @var $model Questions */

$this->breadCrumbs=array(
	'Questions'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'Список вопросов', 'url'=>array('index')),
array('label'=>'Управление вопросами', 'url'=>array('admin')),
);
?>

<h1>Создать вопросы для "<? echo $name?>"</h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'id'=>$id)); ?>