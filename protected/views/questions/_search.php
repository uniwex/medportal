<?php
/* @var $this QuestionsController */
/* @var $model Questions */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

                    <div class="row">
            <?php echo $form->label($model,'idQuestion'); ?>
            <?php echo $form->textField($model,'idQuestion'); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'idPoll'); ?>
            <?php echo $form->textField($model,'idPoll'); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'text'); ?>
            <?php echo $form->textArea($model,'text',array('rows'=>6, 'cols'=>50)); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'description'); ?>
            <?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
        </div>

        <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->