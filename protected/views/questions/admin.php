<?php
/* @var $this QuestionsController */
/* @var $model Questions */

$this->breadCrumbs=array(
	'Questions'=>array('index'),
	'Manage',
);
$this->menu=array(
array('label'=>'List Questions', 'url'=>array('index')),
array('label'=>'Create Questions', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$('#questions-grid').yiiGridView('update', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Управление вопросами</h1>

<div class="row">
	<div class="col-md-12">
		<div class="box box-info">
			<div class="box-header with-border ui-sortable-handle">
				<h3 class="box-title">Список вопросов для <?='"'.$name.'"';?></h3><br><br>
				<a href="../create/<?=$id;?>">Создать новый вопрос</a>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove">
						<i class="fa fa-times"></i></button>
				</div>
			</div>
			<div class="box-body">
				<?php $this->widget('zii.widgets.grid.CGridView', array(
					'id'=>'questions-grid',
					'dataProvider'=>$model->search(),
					'filter'=>$model,
					'cssFile' => Yii::app()->request->getBaseUrl(true) . '/assets/admin/css/gridview.css',
					'itemsCssClass' => 'table no-margin',
					'htmlOptions' => array(
						'class' => 'table-responsive'
					),
					'columns'=>array(
						'idQuestion',
						'idPoll',
						'text',
						'description',
						array(
							'class'=>'CButtonColumn',
							'buttons'=>array(
								'update'=> array (
									'options'=>array('class'=>'update'),
									'imageUrl'=>'/assets/f086fb1a/gridview/update.png',
									'url'=>'Yii::app()->createUrl("/questions/update", array("id"=>$data->idQuestion, "idPoll"=>$data->idPoll))',
								),
							),
						),
					),
				)); ?>
			</div>
		</div>
	</div>
</div>
