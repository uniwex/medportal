<?php
/* @var $this QuestionsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadCrumbs=array(
	'Questions',
);

$this->menu=array(
array('label'=>'Создать вопросы', 'url'=>array('create')),
array('label'=>'Управление вопросами', 'url'=>array('admin')),
);
?>

<h1>Вопросы</h1>

<?php $this->widget('zii.widgets.CListView', array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
