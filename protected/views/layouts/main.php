<?php
if(!Yii::app()->user->isGuest) {
    if(User::model()->findByPk(Yii::app()->user->id) == false){
        $this->redirect("/site/logout");
    }
}
?>
<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Сайт по поиску врачей</title>

    <link href='<?= Yii::app()->request->baseUrl ?>/assets/styles/jquery-ui.css' media="screen" rel="stylesheet"
          type="text/css"/>
    <link href='<?= Yii::app()->request->baseUrl ?>/assets/bootstrap/css/bootstrap.css' media="screen" rel="stylesheet"
          type="text/css"/>
    <link href='<?= Yii::app()->request->baseUrl ?>/assets/styles/main.css' media="screen" rel="stylesheet"
          type="text/css"/>
    <link rel="shortcut icon" href=<?= '"'.Yii::app()->request->baseUrl . "/assets/images/favicon.ico\""; ?> type="image/x-icon" >
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <? Yii::app()->getClientScript()->registerCoreScript('jquery'); ?>
</head>
<body>
<div id="preload" class="overlay">
    <div id="spinner"></div>
</div>
<div class="overlay" id="overlaySchedule">
    <div class="modal-window">
        <div class="modal-content">
            <div class="modal-table">
                <table id="schedule">
                    <tr>
                        <th>Пн</th>
                        <th>Вт</th>
                        <th>Ср</th>
                        <th>Чт</th>
                        <th>Пт</th>
                        <th>Сб</th>
                        <th>Вс</th>
                    </tr>
                    <tr>
                        <th>с <input autocomplete="off" type="text"><br>до
                            <input autocomplete="off" type="text"></th>
                        <th>с <input autocomplete="off" type="text"><br>до
                            <input autocomplete="off" type="text"></th>
                        <th>с <input autocomplete="off" type="text"><br>до
                            <input autocomplete="off" type="text"></th>
                        <th>с <input autocomplete="off" type="text"><br>до
                            <input autocomplete="off" type="text"></th>
                        <th>с <input autocomplete="off" type="text"><br>до
                            <input autocomplete="off" type="text"></th>
                        <th>с <input autocomplete="off" type="text"><br>до
                            <input autocomplete="off" type="text"></th>
                        <th>с <input autocomplete="off" type="text"><br>до
                            <input autocomplete="off" type="text"></th>
                    </tr>
                </table>
            </div>
            <div class="modal-btn">
                <input type="button" class="button1" value="+ время" id="addTime">
                <input type="button" class="button1 disabled" disabled="disabled" value="- время" id="removeTime">
                <input type="button" class="button1" value="Сохранить" id="saveSchedule" onclick="SaveSchedule()">
                <input type="button" class="button1" value="Закрыть"
                       onclick="$('#overlaySchedule').css('display', 'none');">
            </div>
        </div>
    </div>
</div>

<div class="overlay" id="overlay-calendar">
    <div class="modal-window">
        <div class="close-button">X</div>
        <?
        if (!Yii::app()->user->isGuest) {
            if (User::model()->findByPk(Yii::app()->user->id)->type == 'patient') { ?>
                <div class="calendar-appointment">
                <span>
                    <a href="<?= Yii::app()->request->baseUrl . "/visit" ?>">
                        <i class="fa fa-calendar-plus-o"><span style="font-family: MyriadProRegular;font-weight: 100;font-size: 12pt;">Записаться к врачу</span></i>
                    </a>
                </span>
                </div>
            <?php }
        }
        ?>

        <div class="calendar-appointment"><span><a href="<?= Yii::app()->request->baseUrl . "/note" ?>"><i
                        class="fa fa-calendar-plus-o"><span style="font-family: MyriadProRegular;font-weight: 100;font-size: 12pt;">Создать заметку</span></i></a></span></div>
        <hr class="modal-hr">
        <div id="calendar-on-day">
        </div>
    </div>
</div>

<nav class="navbar navbar-inverse" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="hidden-xs hidden-sm phone-header col-md-3 no-padding" style="max-width:250px;">
            <span id="firstSite">Первый сайт по поиску врачей</span>
        </div>
        <div class="collapse navbar-collapse col-md-9 pull-right" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right" id="navMenu">
                <li><a href="/site/eyeimpired" style="color:#333">Слабовидящим</a></li>
                <? if (Yii::app()->user->id) { ?>
                    <li><a href="/tickets">Вопрос - Ответ</a></li>
                <?php } ?>
                <li><a href='<?= Yii::app()->request->baseUrl ?>/digest'>Дайджест соцсетей</a></li>
                <li><a href='<?= Yii::app()->request->baseUrl ?>/sections'>Статьи</a></li>
                <li><a href="/site/inProcess">Форум</a></li>
                <li><a href="http://vk.com/vrach_help">Онлайн консультация</a></li>
                <?php if(User::isAdmin()) { ?>
                <li><a href="/clinic/admin">Админ.панель</a></li>
                <?php } ?>
                <?php
                if (!Yii::app()->user->id) {
                    ?>
                    <li><a href='<?= Yii::app()->request->baseUrl ?>/site/registration'>Регистрация</a></li>
                    <li><a href="<?= Yii::app()->request->baseUrl ?>/site/login">Вход</a></li>
                    <?php
                } else {
                    ?>
                    <?php if (User::model()->findByPk(Yii::app()->user->id)->type != 'patient') { ?>
                        <li><a id="toggle-user-menu">Личный кабинет</a>
                            <ul id="user-menu" style="display:none;">
                                <li>
                                    <a href="<?= Yii::app()->request->baseUrl . '/site/' . User::model()->findByPk(Yii::app()->user->id)->type . '/' . Yii::app()->user->id; ?>">Профиль</a>
                                </li>
                                <li><a href="<?= Yii::app()->request->baseUrl ?>/site/logout/">Выход</a></li>
                            </ul>
                        </li>
                    <?php } else { ?>
                        <li><a href="<?= Yii::app()->request->baseUrl ?>/site/logout">Выход</a></li>
                    <?php } ?>
                    <?php
                }
                ?>
            </ul>
        </div>
    </div>
</nav>

<div class="container">
    <div class="row">
        <div class="col-md-12">

            <div class="pull-left" id="blueCross">
                <a href="/">
                    <img class="pull-left" src='<?= Yii::app()->request->baseUrl ?>/assets/images/blueCross.png'>
                    <div class="pull-right" id="regToDoctor">ЗАПИСЬ К ДОКТОРУ</div>
                </a>
            </div>

            <div class="pull-right" id="contacts">
                <div class="row">
                    <a href="/site/inProcess" style="text-decoration:none;"><div class="order">
                        <img class="phoneIcon" src='<?= Yii::app()->request->baseUrl ?>/assets/images/phone1.png'>
                            Заказать звонок
                        </div>
                    </a>
                    <div class="phoneNumber">8 (495) 777 77 00</div>
                </div>
                <div class="row">
                    <input class="pull-right" id="siteSearch" placeholder="Поиск по сайту..">
                    <span id="searchButton"><i class="fa fa-search"></i></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row indent search">
        <div class="col-md-8 col-md-offset-2">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#searchDoctor" data-toggle="tab">Поиск врача <i
                            class="fa fa-caret-down"></i></a></li>
                <li><a href="#searchClinic" data-toggle="tab">Поиск клиники <i class="fa fa-caret-down"></i></a></li>
                <li style="margin-bottom:-2px;"><a href="#searchNurse" data-toggle="tab">Поиск сиделки <i class="fa fa-caret-down"></i></a></li>
            </ul>
            <? $form = $this->beginWidget('CActiveForm', array(
                'id' => 'search-form',
                'action' => Yii::app()->request->baseUrl . '/search/',
            )) ?>
            <?= CHtml::hiddenField('type', '') ?>
            <div class="tab-content">
                <div class="tab-pane active" id="searchDoctor">
                    <input type="text" name="doctor" placeholder="Запись к доктору...">
                    <input type="submit" name="submitDoctor" style="width:120px;margin-right:10px;" value="Найти">
                </div>

                <div class="tab-pane" id="searchClinic">
                    <input type="text" name="clinic" placeholder="Запись к доктору...">
                    <input type="submit" name="submitClinic" style="width:120px;margin-right:10px;" value="Найти">
                </div>

                <div class="tab-pane" id="searchNurse">
                    <input type="text" name="nurse" placeholder="Запись к доктору...">
                    <input type="submit" name="submitNurse" style="width:120px;margin-right:10px;" value="Найти">
                </div>

                <div class="row search-detail">
                    <div class="col-md-5 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Найдите врача в своем городе</label>
                            <?
                            $city = CHtml::listData(User::model()->findAll(), 'city', 'city');
                            asort($city);
                            $city = array_merge(array('' => 'Все города'), $city);
                            ?>
                            <?= CHtml::dropDownList(
                                'city',
                                isset($_POST['city']) ? $_POST['city'] : '',
                                $city,
                                array('class' => 'form-control')
                            ) ?>
                            <div class="height visible-xs visible-sm"></div>
                        </div>
                    </div>

                    <div class="col-md-2 hidden-sm hidden-xs text-center no-padding" style="margin-left:-20px;">
                        <div class="form-group">
                            <ul>
                                <li><a href="#"
                                       onclick="$('#stationModal').modal('show')"
                                       data-id="metro"
                                       title="Поиск по метро">М</a></li>
<!--                                <li><a href="#" data-id="map" data-toggle="tooltip" data-placement="bottom"-->
<!--                                       title="Поиск по карте">К</a></li>-->
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-5 col-xs-12" style="margin-top: -28px;">
                        <div class="form-group">
                            <label class="control-label">Найдите врача в другой стране</label>
                            <?
                            $country = CHtml::listData(User::model()->findAll(), 'country', 'country');
                            asort($country);
                            $country = array_merge(array('' => 'Все страны'), $country);
                            ?>
                            <?= CHtml::dropDownList(
                                'country',
                                isset($_POST['country']) ? $_POST['country'] : '',
                                $country,
                                array('class' => 'form-control')
                            ) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="stationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="modalLabel">Выберите станцию метро</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row" data-empty="metro" style="display: none">
                                <div class="col-md-12">
                                    <p>Для этого города нет станций метро</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4" data-col="0"></div>
                                <div class="col-md-4" data-col="1"></div>
                                <div class="col-md-4" data-col="2"></div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Сохранить изменения
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <? $this->endWidget(); ?>
        </div>
        <div class="hidden-sm hidden-xs col-md-2" id="calendar">
            <?php
            $year = date('Y');
            $month = date('m');
            ?>
            <span id="calendar-month" style="display:none;"><?= $month; ?></span>
            <h4 class="text-center"><a id="calendar-link" href="<?= Yii::app()->request->baseUrl . '/events'; ?>">Предстоящие
                    события</a></h4>
            <hr>
            <div class="month"><span id="calendar-month-left" style="padding-left:0px;">«</span><span
                    id="calendar-month-name"><?php $month = date('m');
                    switch ($month) {
                        case 1:
                            echo "Январь";
                            break;
                        case 2:
                            echo "Февраль";
                            break;
                        case 3:
                            echo "Март";
                            break;
                        case 4:
                            echo "Апрель";
                            break;
                        case 5:
                            echo "Май";
                            break;
                        case 6:
                            echo "Июнь";
                            break;
                        case 7:
                            echo "Июль";
                            break;
                        case 8:
                            echo "Август";
                            break;
                        case 9:
                            echo "Сентябрь";
                            break;
                        case 10:
                            echo "Октябрь";
                            break;
                        case 11:
                            echo "Ноябрь";
                            break;
                        case 12:
                            echo "Декабрь";
                            break;
                    }
                    ?></span><span id="calendar-year"><?= date('Y'); ?></span><span id="calendar-month-right">»</span>
            </div>
            <table class="week">
                <tr>
                    <td>Пн</td>
                    <td>Вт</td>
                    <td>Ср</td>
                    <td>Чт</td>
                    <td>Пт</td>
                    <td class="weekend">Сб</td>
                    <td class="weekend">Вс</td>
                </tr>
            </table>
            <span id="calendar-content">
            <?php
            //GENERATING CALENDAR
            $beginOfMonth = strtotime($year . "-" . $month . "-01");
            $firstDayOfWeek = date('N', $beginOfMonth);
            $daysFirstWeek = 8 - $firstDayOfWeek;
            $lastDayOfMonth = date('t', strtotime($year . "-" . $month . "-01"));
            $endOfMonth = strtotime($year . "-" . $month . "-" . $lastDayOfMonth);
            $daysLastWeek = date('N', $endOfMonth);
            $countFullWeeks = ($lastDayOfMonth - $daysFirstWeek - $daysLastWeek) / 7;
            $day = 1;

            $criteria = new CDbCriteria;
            $userId = Yii::app()->user->id;
            if (!Yii::app()->user->isGuest) {

                $userType = User::model()->findByPk(Yii::app()->user->id)->type;
                $events = array();
                //get data from database
                if ($userType == 'patient') {
                    $criteria->condition = 'idOwner=:idOwner';
                    $criteria->params = array(':idOwner' => Yii::app()->user->id);
                    $events = Event::model()->findAll($criteria);
                }
                if ($userType == 'doctor') {
                    $criteria->condition = '(idOwner=:idOwner OR idExecuter=:idExecuter) AND type<>:type';
                    $criteria->params = array(':idOwner' => Yii::app()->user->id, ':idExecuter' => Yii::app()->user->id, 'type' => 'nurse');
                    $events = Event::model()->findAll($criteria);
                }
                if ($userType == 'nurse') {
                    $criteria->condition = '(idOwner=:idOwner OR idExecuter=:idExecuter) AND type<>:type';
                    $criteria->params = array(':idOwner' => Yii::app()->user->id, ':idExecuter' => Yii::app()->user->id, 'type' => 'schedule');
                    $events = Event::model()->findAll($criteria);
                }
                if ($userType == 'clinic') {
                    $doctors = Schedule::model()->findAllByAttributes(array('idUser' => Yii::app()->user->id));
                    foreach ($doctors as $doctor) {
                        $criteria->condition = '(idOwner=:idOwner OR idExecuter=:idExecuter) AND type=:type';
                        $criteria->params = array(':idOwner' => $doctor->id, ':idExecuter' => $doctor->id, 'type' => 'schedule');
                        $events = array_merge(Event::model()->findAll($criteria), $events);
                    }
                    $criteria->condition = 'idOwner=:idOwner AND type=:type';
                    $criteria->params = array(':idOwner' => Yii::app()->user->id, 'type' => 'note');
                    $events = array_merge(Event::model()->findAll($criteria), $events);
                }
            }
            //display calendar
            echo '<table class="days"><tr>';
            //first week
            $tempI;
            for ($i = 1; $i <= 7; $i++) {
                if ($i >= $firstDayOfWeek) {
                    $weekend = "";
                    $wrote = false;//if number in calendar already highlighted
                    if($i > 5)
                        $weekend = " weekend";
                    $class = "class=\"";
                    if ($userId) {
                        foreach ($events as $key) {
                            if ($key->type != 'nurse') {
                                ($day < 10) ? $tempI = '0'.$day : $tempI = $day;
                                if (($key->eventDate == $year . "-" . $month . "-" . $tempI)&&($wrote == false)) {
                                    $wrote = true;
                                    $class .= "busyDay";
                                }
                            }
                            if ($key->type == 'nurse') {
                                $beginDateSeconds = strtotime($key->eventDate);
                                $endDateSeconds = strtotime($key->eventDateFinish);
                                $curDateSeconds = strtotime($year . "-" . $month . "-" . $day);
                                if (($beginDateSeconds <= $curDateSeconds) && ($curDateSeconds <= $endDateSeconds)&&($wrote == false)) {
                                    $wrote = true;
                                    $class .= "busyDay";
                                }
                            }
                        }
                    }
                    $class .= $weekend . "\"";
                    echo '<td ' . $class . '>' . $day . '</td>';
                    $day++;
                } else {
                    echo '<td></td>';
                }
            }
            echo '</tr></table>';
            $tempDay = $day;
            //full weeks
            for ($i = 1; $i <= $countFullWeeks; $i++) {
                echo '<table class="days"><tr>';
                for ($j = 1; $j <= 7; $j++) {
                    $weekend = "";
                    $wrote = false;//if number in calendar already highlighted
                    if($j > 5)
                        $weekend = " weekend";
                    $class = "class=\"";
                    if ($userId) {
                        foreach ($events as $key) {
                            if ($key->type != 'nurse') {
                                ($day < 10) ? $tempI = '0'.$day : $tempI = $day;
                                if (($key->eventDate == $year . "-" . $month . "-" . $tempI)&&($wrote == false)) {
                                    $class .= "busyDay";
                                    $wrote = true;
                                }
                            }
                            if ($key->type == 'nurse') {
                                $beginDateSeconds = strtotime($key->eventDate);
                                $endDateSeconds = strtotime($key->eventDateFinish);
                                $curDateSeconds = strtotime($year . "-" . $month . "-" . $day);
                                if (($beginDateSeconds <= $curDateSeconds) && ($curDateSeconds <= $endDateSeconds)&&($wrote == false)) {
                                    $class .= "busyDay";
                                    $wrote = true;
                                }
                            }
                        }
                    }
                    $class .= $weekend . "\"";
                    echo '<td ' . $class . '>' . $day . '</td>';
                    $day++;
                }
                echo '</tr></table>';
            }
            //last week
            echo '<table class="days"><tr>';
            for ($i = 1; $i <= 7; $i++) {
                if ($i <= $daysLastWeek) {
                    $weekend = "";
                    $wrote = false;//if number in calendar already highlighted
                    if($i > 5)
                        $weekend = " weekend";
                    $class = "class=\"";
                    if ($userId) {
                        foreach ($events as $key) {
                            if ($key->type != 'nurse') {
                                ($day < 10) ? $tempI = '0'.$day : $tempI = $day;
                                if (($key->eventDate == $year . "-" . $month . "-" . $tempI)&&($wrote == false)) {
                                    $class .= "busyDay";
                                    $wrote = true;
                                }
                            }
                            if ($key->type == 'nurse') {
                                $beginDateSeconds = strtotime($key->eventDate);
                                $endDateSeconds = strtotime($key->eventDateFinish);
                                $curDateSeconds = strtotime($year . "-" . $month . "-" . $day);
                                if (($beginDateSeconds <= $curDateSeconds) && ($curDateSeconds <= $endDateSeconds)&&($wrote == false)) {
                                    $class .= "busyDay";
                                    $wrote = true;
                                }
                            }
                        }
                    }
                    $class .= $weekend . "\"";
                    echo '<td ' . $class . '>' . $day . '</td>';
                    $day++;
                } else {
                    echo '<td></td>';
                }
            }
            echo '</tr></table>';
            ?>
            </span>
        </div>
    </div>
    <div class="row indent">
        <div class="col-md-2 col-xs-12 specialty">
            <span>Специальность</span><i class="fa fa-caret-down" style="margin-left:22px;line-height:1;"></i>
        </div>
        <div class="col-md-7 col-xs-6 breadCrumbs">
            <? $this->widget('zii.widgets.CBreadcrumbs', array(
                'homeLink' => CHtml::link('Главная', '/site/index'),
                'links' => $this->breadCrumbs
            )); ?>
        </div>
        <div class="col-md-3 col-xs-6 pull-right" style="padding: 4px 0px;">
            <div class="pull-right social" style="margin-right:-5px;">
                <a href="<?= "http://vk.com/" . Yii::app()->params['socialNetLink']; ?>"><img class="arrowRight" src="<?= Yii::app()->request->baseUrl ?>/assets/images/icon_vkontakte.png"></a>
                <a href="<?= "http://odnoklassniki.ru/" . Yii::app()->params['socialNetLink']; ?>"><img class="arrowRight" src="<?= Yii::app()->request->baseUrl ?>/assets/images/icon_odnoklassniki.png"></a>
                <a href="<?= "http://facebook.com/" . Yii::app()->params['socialNetLink']; ?>"><img class="arrowRight" src="<?= Yii::app()->request->baseUrl ?>/assets/images/icon_facebook.png"></a>
                <a href="<?= "http://twitter.com/" . Yii::app()->params['socialNetLink']; ?>"><img class="arrowRight" src="<?= Yii::app()->request->baseUrl ?>/assets/images/icon_twitter.png"></a>
                <a href="<?= "http://instagram.com/" . Yii::app()->params['socialNetLink']; ?>"><img class="arrowRight" src="<?= Yii::app()->request->baseUrl ?>/assets/images/icon_instagram.png"></a>
                <a href="<?= "http://linkedin.com/" . Yii::app()->params['socialNetLink']; ?>"><img class="arrowRight" src="<?= Yii::app()->request->baseUrl ?>/assets/images/linkedin1.png"></a>
                <a href="http://zapiskdoktoru.ru" title="medportal" rel="sidebar"><img class="arrowRight" src="<?= Yii::app()->request->baseUrl ?>/assets/images/plus1.png"></a>
            </div>
        </div>
    </div>

    <div id="baseBlock" class="row indent" style="margin-right:-30px;">
        <div class="col-md-2 menuSpeciality indent" style="display: none;">
            <ul>
                <?php
                $specData = Specialization::model()->findAll();
                foreach ($specData as $key) {
                    $doctors = Doctor::model()->findAllByAttributes(array('idSpecialization' => $key->id));
                    $img = '';
                    if ($doctors) {
                        $img = '<img src="' . Yii::app()->request->baseUrl . '/assets/images/blueArrowRight.png">';
                        echo '<li>' . $img . '<a href="/search?specialization='.$key->id.'">'.$key->name.'</a>';
                    } else
                        echo '<li>' . $img . $key->name;
                    echo '</li>';
                }
                ?>
            </ul>
            <div class="entireList indent">
                <div class="pull-left" style="padding:2px 0px;">Весь список</div>
                <div class="pull-right"><i class="fa fa-caret-down" style="line-height:0.85;"></i></div>
            </div>

            <div class="help-home">
                Помощь на дому
            </div>
        </div>
        <?= $content; ?>
    </div>

    <div class="row partner" style="margin-left:-30px;margin-right:-30px;">
        <div class="col-md-2">
            <a href="/site/inProcess">
                <img src="<?= Yii::app()->request->baseUrl ?>/assets/images/bag1.png">
                <span class="our-partners">Наши партнёры</span>
            </a>
        </div>
        <div class="col-md-10">
            <div class="row">
                <div class="col-md-4">
                    <a href="http://normoprotein.ru/">
                        <h5 class="desc-partner">Лечебное питание для больниц:</h5>
                        <img src="<?= Yii::app()->request->baseUrl ?>/assets/images/partner1.png">
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="http://фотодитазин.рф">
                        <h5 class="desc-partner">Фотодинамическая терапия:</h5>
                        <img src="<?= Yii::app()->request->baseUrl ?>/assets/images/partner2.png">
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="http://markagoda.ru/">
                        <h5 class="desc-partner">Добровольная сертификация:</h5>
                        <img src="<?= Yii::app()->request->baseUrl ?>/assets/images/partner3.png">
                    </a>
                </div>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript" src='<?= Yii::app()->request->baseUrl ?>/assets/scripts/jquery-ui.js'></script>
<script type="text/javascript" src='<?= Yii::app()->request->baseUrl ?>/assets/bootstrap/js/bootstrap.js'></script>
<script type="text/javascript" src='<?= Yii::app()->request->baseUrl ?>/assets/scripts/main2.js'></script>
<script type="text/javascript" src='<?= Yii::app()->request->baseUrl ?>/assets/scripts/globalSearch.js'></script>
</body>
</html>