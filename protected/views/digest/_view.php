<?php
/* @var $this TestController */
/* @var $data Article */
?>
<div class="row article" id="digest-<?= $data->id; ?>">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-11 article-author"><? ?></div>
            <div class="col-md-1 article-author"></div>
        </div>
        <div class="row form-group">
            <a href="/digest/<?=$data->id?>">
                <div class="col-md-10 article-head no-padding"><?= $data->name ?></div>
                <div class="col-md-2 text-r no-padding"><?= date('d-m-Y',strtotime($data->time)) ?></div>
            </a>
            <?php
            if(isset(Yii::app()->user->id)) {
            if( User::model()->findByPk(Yii::app()->user->id)->role == 'admin') {
                ?>
                <div class="col-md-1 text-r"><a href="/digest/edit/<?= $data->id; ?>"><i class="fa fa-pencil-square-o"></i></a>
                    <i class="fa fa-times" onclick="removeArticle(this, 1)" data-id="<?= $data->id; ?>"></i></div>
            <?php
            } }
            ?>
        </div>
        <div class="row article-text">
            <div class="col-md-9 no-padding">
                <?= $data->text ?>
            </div>
            <!--<div class="col-md-2 rating-div" style="text-align: left; margin-left: 17px;" onclick="return false;">-->
                <? //$this->widget('RatingWidget',['elem_id' => $data->id,'elem_type' => 'digest','avg' => RatingHelper::getRating($data->id, 'digest')]); ?>
            <div class="col-md-2">
            </div>
        </div>
    </div>
</div>
<script src="/assets/scripts/articles.js"></script>