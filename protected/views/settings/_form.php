<?php
/* @var $this SettingsController */
/* @var $model PartnerPercent */
/* @var $form CActiveForm */
?>
<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'partner-percent-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'errorMessageCssClass' => 'control-label',
    'clientOptions' => array(
        'validateOnChange' => true,
        'validateOnSubmit' => true,
        'errorCssClass' => 'has-error',
        'successCssClass' => 'has-success'
    )
)); ?>
    <div class="box-body">

        <?php echo $form->errorSummary($model, 'Пожалуйста, исправьте следующие ошибки:', '', array('class' => 'callout callout-danger')); ?>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'count'); ?>
            <?php echo $form->numberField($model, 'count', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'count'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'percent'); ?>
            <?php echo $form->numberField($model, 'percent', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'percent'); ?>
        </div>
    </div>
    <div class="box-footer">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', array('class' => 'btn btn-primary pull-right')); ?>
    </div>
<?php $this->endWidget(); ?>