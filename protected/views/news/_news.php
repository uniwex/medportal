<div class="col-xs-12 col-md-12 col-sm-12 no-padding">
    <p class="news">
        <span><?php echo Yii::app()->dateFormatter->format('d MMMM yyyy', CHtml::encode($data->date)); ?></span> <?echo CHtml::encode($data->header); ?>
        <br>
    </p>
    <div class="news-desc">
        <?=CHtml::encode($data->text); ?>
    </div>
</div>