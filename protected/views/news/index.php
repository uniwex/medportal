<div class="right-menu-relative">
    <? require ("protected/views/site/rightMenu.php"); ?>
</div>
<div class="center-overview">
    <?php $this->widget('zii.widgets.CListView', array(
        'dataProvider'=>$dataProvider,
        'itemView'=>'_news', // представление для одной записи
        'ajaxUpdate'=>false, // отключаем ajax поведение
        'emptyText'=>'Нет новостей.',
        'template'=>'{sorter} {items} <hr> {pager}',
        'sorterHeader'=>'Сортировать по',
        'sortableAttributes'=>array('date'),
        'pager'=>array(
            'class'=>'CLinkPager',
            'header'=>false,
            'cssFile'=>'/css/pager.css', // устанавливаем свой .css файл
            'htmlOptions'=>array('class'=>'pager'),
        ),
    )); ?>
</div>
