<?php

/**
 * Created by PhpStorm.
 * User: димон
 * Date: 23.05.2016
 * Time: 13:18
 */
class SciencedoctorController extends CController
{
    public $breadCrumbs;
    public $breadCrumbsHome;


    public function beforeAction() {
        $user = User::model()->findByPk(Yii::app()->user->id);
        if (isset($user)) {
            if ($user->type == 'doctor')
                return true;
            else $this->redirect(array('/site/main'));
        } else $this->redirect(array('/site/main'));
    }

    public function actionIndex(){
        $param = [];
        
        $this->breadCrumbs = array('Мои научные работы','');
        
        $criteria = new CDbCriteria();
        $criteria->limit = 3;
        $criteria->addCondition('idAuthor ='.Yii::app()->user->id);
        $criteria->order = 'id desc';

        $count = ScienceWorks::model()->count($criteria);
        $pages = new CPagination($count);
        $pages->setPageSize(3);
        $pages->applyLimit($criteria);
        $result = ScienceWorks::model()->findAll($criteria);
        $dataProvider = new CArrayDataProvider($result);
        
        $param['dataProvider'] = $dataProvider;
        $param['pages'] = $pages;
        $this->render('index', $param);
    }


    public function actionCreate()
    {
        $model=new ScienceWorks;
        $model->setScenario('create-user');
        if(isset($_REQUEST['ScienceWorks']))
        {
            $model->attributes=$_REQUEST['ScienceWorks'];

            if (isset($model->link)) {

                $model->pdf = CUploadedFile::getInstance($model, 'link');
                $filenamep = md5(time() . rand(1000, 10000)) . '.' . $model->pdf->getExtensionName();
                $model->link = $filenamep;
                $model->pdf->saveAs(Yii::getPathOfAlias('webroot') . '/upload/' . $filenamep);

                $filename = md5(time() . rand(1000, 10000)) . '.jpeg';
                $model->image = '123';
                $model->idAuthor = Yii::app()->user->id;
                $model->picture = $filename;

                $obPdf = new Imagick(Yii::getPathOfAlias('webroot') . '/upload/' . $filenamep.'[0]'); #Открываем наш PDF и указываем обработчику на первую страницу
                $obPdf->setImageColorspace(255); #устанавливаем цветовую палитру
                $obPdf->setImageBackgroundColor('white');

                $obPdf = $obPdf->flattenImages(); // Use this instead.

                $obPdf->setCompression(Imagick::COMPRESSION_JPEG); #Устанавливаем компрессор
                $obPdf->setCompressionQuality(100); #И уровень сжатия
                $obPdf->setImageFormat('jpeg'); #С форматом не заморачиваемся — пусть будет JPEG.
                #При необходимости сделать превью ресайзим изображение
                $obPdf->resizeImage(154, 220, Imagick::FILTER_LANCZOS, 1);
                #Ну и конечно же пишем в jpg-файл.
                $obPdf->writeImage(Yii::getPathOfAlias('webroot') . '/upload/' . $filename);
                $obPdf->clear();
                $obPdf->destroy();
            }

            if($model->save())
                $this->redirect(array('index'));
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }

    public function actionUpdate($id){
        $science = ScienceWorks::model()->findByPk($id,'idAuthor = :idAuthor',[':idAuthor'=>Yii::app()->user->id]);
        if (isset($science)) {
            if(isset($_REQUEST['ScienceWorks'])) {
                $model = $science;
                $model->setScenario('update');
                $model->attributes = $_REQUEST['ScienceWorks'];
                if (!isset($model->link)) $model->link = $science->link; else {
                    if ($model->pdf = CUploadedFile::getInstance($model, 'link')){
                        if (file_exists(Yii::getPathOfAlias('webroot') . '/upload/'.$this->link)) {
                            unlink(Yii::getPathOfAlias('webroot') . '/upload/'.$this->link);
                        }
                        $filename = md5(time() . rand(1000, 10000)) . '.' . $model->pdf->getExtensionName();
                        $model->link = $filename;
                        $model->pdf->saveAs(Yii::getPathOfAlias('webroot') . '/upload/' . $filename);
                    }
                }
                if (!isset($model->picture)) $model->picture = $science->picture; else {
                    if ($model->image = CUploadedFile::getInstance($model, 'picture')) {
                        if (file_exists(Yii::getPathOfAlias('webroot') . '/upload/'.$science->picture)) {
                            unlink(Yii::getPathOfAlias('webroot') . '/upload/'.$science->picture);
                        }
                        $filename = md5(time() . rand(1000, 10000)) . '.' . $model->image->getExtensionName();
                        $model->picture = $filename;
                        $model->image->saveAs(Yii::getPathOfAlias('webroot') . '/upload/' . $filename);
                    }
                }
                if($model->save())
                    $this->redirect(array('index'));
            }
            $this->render('update',array(
                'model' => $science
            ));
        } else {
            $this->redirect(array('index'));
        }
    }

    public function actionDelete($id) {
        $science = ScienceWorks::model()->findByPk($id,'idAuthor = :idAuthor',[':idAuthor'=>Yii::app()->user->id]);
        if (isset($science)) {
            $science->delete();
            $this->redirect(array('index'));
        } else {
            $this->redirect(array('index'));
        }
    }

    public function actionDownload(){
        if (isset($_GET['id'])){
            $name = ScienceWorks::model()->findByPk(intval($_GET['id']))->link;
            $file = (Yii::getPathOfAlias('webroot') . "/upload/" . $name);
            header ("Content-Type: application/octet-stream");
            header ("Accept-Ranges: bytes");
            header ("Content-Length: ".filesize($file));
            header ("Content-Disposition: attachment; filename=".$name);
            readfile($file);
        }
    }

}