<?php

class SiteController extends CController
{
    public $breadCrumbs;
    public $breadCrumbsHome;
    public $MONTHES = array('Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь');
    public $DAYS_OF_WEEK = array('Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье');

    public function actionDoctor()
    {
        $id = Yii::app()->request->getQuery('id');
        if (!empty($id)) {
            $this->breadCrumbs = array('Доктор');
            $user = User::model()->findByPk($id);
            $doctor = Doctor::model()->findByPk($id);
            if ($doctor) {
                $reviewModel = new Review;
                $this->performAjaxValidation($reviewModel, 'doctor-review');
                $param = array(
                    'docId' => $id,
                    'reviewModel' => $reviewModel,
                    'user' => $user,
                    'doctor' => $doctor,
                    'curProfileId' => $id
                );
                if (isset($_POST['Review'])) {
                    $reviewModel->attributes = $_POST['Review'];
                    $reviewModel->pics = 1;
                    if ($reviewModel->validate()) {
                        $reviewModel->time = date("Y-m-d H:i:s");
                        $reviewModel->idUserFrom = Yii::app()->user->id;
                        $reviewModel->save();
                        $param['com'] = 1;
                    } else $param['com'] = 0;
                }
                $this->render('doctor', $param);
            } else $this->redirect(Yii::app()->request->baseUrl . '/site/index');
        } else $this->redirect(Yii::app()->request->baseUrl . '/site/index');
    }

    protected function performAjaxValidation($model, $form)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === $form) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionClinic()
    {

        $id = Yii::app()->request->getQuery('id');
        if (!empty($id)) {
            $this->breadCrumbs = array('Клиника');
            $clinic = Clinic::model()->findByPk($id);
            if ($clinic) {
                $user = User::model()->findByPk($id);
                $reviewModel = new Review();
                $this->performAjaxValidation($reviewModel, 'clinic-review');

                $param = array(
                    'reviewModel' => $reviewModel,
                    'user' => $user,
                    'clinic' => $clinic,
                    'curProfileId' => $id,
                );

                if (isset($_POST['Review'])) {
                    $reviewModel->attributes = $_POST['Review'];
                    if ($reviewModel->validate()) {
                        $reviewModel->time = date("Y-m-d H:i:s");
                        $reviewModel->idUserFrom = Yii::app()->user->id;
                        $reviewModel->save();
                        $path = Yii::getPathOfAlias('webroot') . '/upload/' . $reviewModel->id . '1.jpg';
                        $pics = '';
                        if ($_FILES['image1']['tmp_name'] != '') {
                            move_uploaded_file($_FILES['image1']['tmp_name'], $path);
                            $pics = $reviewModel->id . '1.jpg_';
                        }
                        $path = Yii::getPathOfAlias('webroot') . '/upload/' . $reviewModel->id . '2.jpg';
                        if ($_FILES['image2']['tmp_name'] != '') {
                            move_uploaded_file($_FILES['image2']['tmp_name'], $path);
                            $pics .= $reviewModel->id . '2.jpg_';
                        }
                        $path = Yii::getPathOfAlias('webroot') . '/upload/' . $reviewModel->id . '3.jpg';
                        if ($_FILES['image3']['tmp_name'] != '') {
                            move_uploaded_file($_FILES['image3']['tmp_name'], $path);
                            $pics .= $reviewModel->id . '3.jpg_';
                        }
                        $pics = substr($pics, 0, strlen($pics) - 1);
                        Review::model()->updateByPk($reviewModel->id, array('pics' => $pics));
                        $param['systemMsg'] = 'Отзыв добавлен';
                    }
                }

                $criteria = new CDbCriteria();

                $criteria->condition = 'type = :type and idUser = :id';
                $criteria->params = array(':type' => "clinicDoc", ':id' => $id);

                $dataProvider = new CActiveDataProvider('Schedule', array(
                    'criteria' => $criteria,
                    'pagination' => array(
                        'pageSize' => 15,
                    ),
                ));

                $param['dataProvider'] = $dataProvider;

                $this->render('clinic', $param);
            } else $this->redirect(Yii::app()->request->baseUrl . '/site/');
        } else $this->redirect(Yii::app()->request->baseUrl . '/site/');
    }

    public function actionNurse()
    {
        $id = Yii::app()->request->getQuery('id');
        if (!empty($id)) {
            $this->breadCrumbs = array('Сиделка');
            $nurse = Nurse::model()->findByPk($id);
            $reviewModel = new Review();
            if ($nurse) {
                $param = array(
                    'reviewModel' => $reviewModel,
                    'nurse' => $nurse,
                    'nurseId' => $id,
                    'curProfileId' => $id
                );

                $this->performAjaxValidation($reviewModel, 'nurse-review');

                if (isset($_POST['Review'])) {
                    $reviewModel->attributes = $_POST['Review'];
                    if ($reviewModel->validate()) {
                        $reviewModel->time = date("Y-m-d H:i:s");
                        $reviewModel->idUserFrom = Yii::app()->user->id;
                        $reviewModel->save();
                        $this->layout = 'main';
                        $param['com'] = 1;
                    } else $param['com'] = 0;
                }
                $this->render('nurse', $param);
            } else $this->redirect(Yii::app()->request->baseUrl . '/site/index');
        } else $this->redirect(Yii::app()->request->baseUrl . '/site/index');
    }

    public function actionIndex()
    {
        $this->layout = 'outOfOrder';
        $this->render('outOfOrder');
    }

    public function actionMain()
    {
        $this->breadCrumbsHome = 'Наши задачи:';
        $infohub = Infohub::model()->findAll();
        $specCriteria = new CDbCriteria(array(
            'order' => 'id DESC',
            'limit' => 1
        ));
        $specProject = SpecialProject::model()->find($specCriteria);
        $worldtimes = WorldtimeHelper::getAllTimes();
        $newsCriteria = new CDbCriteria(array(
            'order' => 'date DESC',
            'limit' => 2
        ));
        $news = News::model()->findAll($newsCriteria);
        $this->layout = 'main';
        $this->render('main', array('infohub' => $infohub, 'specProject' => $specProject, 'news' => $news));
    }

    public function actionGetCountries()
    {
        $countries = file_get_contents('http://api.vk.com/method/database.getCountries?v=5.5&need_all=0');
        /*
        if(!Yii::app()->user->isGuest) {
            $model = User::model()->findByPk(Yii::app()->user->id);
            $stations = explode(',', $model->station->name);
            $countries = substr($countries, 0, -1) . ', "country":"'.$model->country.'", "station":'.json_encode($stations).'}';
        }
        */
        echo $countries;
    }

    public function actionGetCityById()
    {
        $idCountry = Yii::app()->request->getParam('idCountry');
        $cities = Cities::model()->findAllBySql("SELECT * FROM cities WHERE `idCountry`=".$idCountry." ORDER BY rusName ASC");
        $allCities = array();
        foreach($cities as $item){
            $city = array('id'=>$item->id, 'name'=>$item->rusName);
            array_push($allCities, $city);
        }
        print_r(json_encode($allCities));
    }

    public function actionGetCityByIdInVk()
    {
        $idCountry = Yii::app()->request->getParam('idCountry');
        $queryString = Yii::app()->request->getParam('queryString');
        $needAll = Yii::app()->request->getParam('needAll');
        if(!isset($needAll))
            $needAll = 0;
        $lang = 0; // russian
        $headerOptions = array(
            'http' => array(
                'method' => "GET",
                'header' => "Accept-language: en\r\n" .
                    "Cookie: remixlang=$lang\r\n"
            )
        );
        $streamContext = stream_context_create($headerOptions);
        $methodUrl = 'http://api.vk.com/method/database.getCities?v=5.50&need_all='.$needAll.'&country_id=' . $idCountry . '&count=100&q=' . urlencode($queryString);
        $cities = file_get_contents($methodUrl, false, $streamContext);
        echo $cities;
    }

    public function actionGetStations()
    {
        $methodUrl = 'https://api.hh.ru/metro';
        $stations = SendHelper::send($methodUrl);
        echo $stations;
    }

    public function actionTest()
    {
        $idCountry = Yii::app()->request->getParam('idCountry');
        $queryString = Yii::app()->request->getParam('queryString');
        $lang = 0; // russian
        $headerOptions = array(
            'http' => array(
                'method' => "GET",
                'header' => "Accept-language: en\r\n" .
                    "Cookie: remixlang=$lang\r\n"
            )
        );
        $streamContext = stream_context_create($headerOptions);
        $methodUrl = 'http://api.vk.com/method/database.getCities?v=5.44&need_all=0&country_id=' . $idCountry . '&count=100&q=' . urlencode($queryString);
        $cities = file_get_contents($methodUrl, false, $streamContext);
        $citiesParsed = json_decode($cities);
        foreach($citiesParsed->response->items as $city){
            if(!isset($city->area))
                print_r($city);
        }
    }

    public function actionGetStationsByCityId()
    {
        $cityId = Yii::app()->request->getParam('cityId');
        $methodUrl = 'https://api.hh.ru/metro/' . $cityId;
        $stations = SendHelper::send($methodUrl);
        $stationParsed = json_decode($stations);

        $stationNames = array();
        foreach($stationParsed->lines as $line){
            foreach($line->stations as $station) {
                array_push($stationNames, $station->name);
            }
        }
        sort($stationNames);
        echo json_encode($stationNames);
    }

    public function actionGetSpecializations()
    {
        $specializations = Specialization::model()->findAll();
        $arr = array();
        foreach ($specializations as $sp) {
            array_push($arr, array('id' => $sp->id, 'name' => $sp->name));
        }
        echo json_encode($arr);
    }

    // Авторизация, регистрация, восстановление, выход

    public function actionRegistration()
    {
        if (Yii::app()->user->isGuest) {
            $this->breadCrumbs = array('Регистрация');
            $userModel = new User();
            $doctorModel = new Doctor();
            $clinicModel = new Clinic();
            $nurseModel = new Nurse();
            $patientModel = new Patient();
            $stationModel = new Station();
            $scheduleModel = new Schedule();
            $specializationModel = new Specialization();
            $socialModel = new Social();
            $userModel->type = '0';

            $this->performAjaxValidation($userModel, 'user-registration');
            $this->performAjaxValidation($doctorModel, 'doctor-registration');
            $this->performAjaxValidation($clinicModel, 'clinic-registration');
            $this->performAjaxValidation($nurseModel, 'nurse-registration');
            $this->performAjaxValidation($patientModel, 'patient-registration');
            $this->performAjaxValidation($stationModel, 'station-registration');
            $this->performAjaxValidation($scheduleModel, 'schedule-registration');
            $this->performAjaxValidation($specializationModel, 'specialization-registration');
            $this->performAjaxValidation($socialModel, 'social-registration');

            $this->layout = 'main';
            $this->render('registration', array('model' => $userModel, 'doctorModel' => $doctorModel, 'clinicModel' => $clinicModel, 'nurseModel' => $nurseModel, 'patientModel' => $patientModel, 'stationModel' => $stationModel, 'scheduleModel' => $scheduleModel, 'specializationModel' => $specializationModel, 'socialModel' => $socialModel));
        } else $this->redirect(Yii::app()->homeUrl);
    }

    public function actionRegUser()
    {
        if (Yii::app()->user->isGuest) {
            $user = new User();
            if(empty($_POST['User'])) {

                $this->render('registration', array('model' => $user));


            } else {

                $user->attributes = $_POST['User'];
                if($user->validate()) {
                   /* echo '<pre>';
                    print_r($_POST);
                    echo '</pre>';*/

                    if ($user->save()){
                        $user->password = '';
                        $this->render('login', ['model' => $user, 'error' => '']);
                    }
                    else{
                        echo 'no save';
                    }
                }
                else{
                    $this->render('registration', ['model' => $user, 'error' => 'не верные данные']);
                }
            }
            //echo 'ggg';

            $this->render('main', ['userModel' => $user, 'error' => '']);
        }
    }

    public function actionRestore()
    {
        $this->breadCrumbs = array('Восстановление пароля');
        if (Yii::app()->user->isGuest) {
            $log = array();
            $model = new User;
            $this->performAjaxValidation($model, 'restore-form');
            if (isset($_POST['User'])) {
                $phone = $_POST['User']['phone'];
                $restoreCode = null;
                if(isset($_POST['User']['restoreCode']))
                    $restoreCode = $_POST['User']['restoreCode'];
                $user = User::model()->find('phone = :phone', array(':phone' => $phone));
                if ($phone && !isset($_POST['User']['restoreCode'])) {
                    if (count($user) > 0) {
                        $code = User::getPassword(5);
                        $user->restoreCode = $code;
                        $user->update();
                        $link = 'Код восстановления: ' . $code;
                        Yii::app()->sms->sms_send($phone, $link);
                        $log['code'] = 0;
                    } else {
                        $log['status'] = 'Аккаунт не найден';
                        $log['code'] = 1;
                    }
                } elseif (isset($_POST['User']['restoreCode'])) {
                    if ($user->restoreCode == $restoreCode) {
                        $password = $user->updatePassword();
                        $log['status'] = 'Новый пароль:';
                        $log['password'] = $password;
                        $log['code'] = 2;
                    } else {
                        $log['status'] = 'Неверный код восстановления';
                        $log['code'] = 3;
                    }
                    $user->resetCode();
                }
                echo json_encode($log);
            } else $this->render('restore', array('model' => $model));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true));
    }

    public function actionLogin()
    {
        $this->breadCrumbs = array('Авторизация');
        if (Yii::app()->user->isGuest) {
            $user = new User;
            if (isset($_POST['User'])) {
                $user->attributes = $_POST['User'];
                $user = $user->login();
                $this->performAjaxValidation($user, 'login-form');
                if ($user) {
                    $identity = new UserIdentity($user->id, $user->phone);
                    $identity->Id = $user->id;
                    $identity->Login = $user->phone;
                    $identity->authenticate();
                    Yii::app()->user->login($identity);
                    echo '1';
                } else echo 'Ошибка авторизации';
            } else $this->render('login', array('model' => $user));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true));
    }

    public function actionLogout()
    {
        $theme = Yii::app()->session['theme'];
        Yii::app()->user->logout();
        if($theme == 'eyeimpired')
            $this->redirect('/site/eyeimpired');
        else
            $this->redirect(Yii::app()->homeUrl);
    }

    public function actionReviews()
    {
        $idUser = Yii::app()->request->getPost('idUser');
        $reviews = Review::model()->findAllByAttributes(array('idUserTo' => $idUser, 'moderate'=>1));
        $revs = array();
        foreach ($reviews as $key) {
            $time = strtotime($key->time);
            $review = array(
                'name' => $key->name,
                'text' => $key->text,
                'cure' => $key->cure,
                'regard' => $key->regard,
                'qualification' => $key->qualification,
                'time' => $time,
                'pics' => $key->pics,
            );
            array_push($revs, $review);
        }
        echo json_encode($revs);
    }

    //get calendar with events
    public function actionGetCalendar(){

        if (Yii::app()->request->isAjaxRequest) {

            $year = Yii::app()->getRequest()->getParam('year');
            $month = Yii::app()->getRequest()->getParam('month');

            $html = '';

            $beginOfMonth = strtotime($year . "-" . $month . "-01");
            $firstDayOfWeek = date('N', $beginOfMonth);
            $daysFirstWeek = 8 - $firstDayOfWeek;
            $lastDayOfMonth = date('t',strtotime($year . "-" . $month . "-01"));
            $endOfMonth = strtotime($year . "-" . $month . "-" . $lastDayOfMonth);
            $daysLastWeek = date('N', $endOfMonth);
            $countFullWeeks = ($lastDayOfMonth - $daysFirstWeek - $daysLastWeek) / 7;
            $day = 1;

            if (!Yii::app()->user->isGuest) {
                $criteria = new CDbCriteria;
                $userType = User::model()->findByPk(Yii::app()->user->id)->type;
                $events = array();
                if ($userType == 'patient') {
                    $criteria->condition = 'idOwner=:idOwner';
                    $criteria->params = array(':idOwner' => Yii::app()->user->id);
                    $events = Event::model()->findAll($criteria);
                }
                if ($userType == 'doctor') {
                    $criteria->condition = '(idOwner=:idOwner OR idExecuter=:idExecuter) AND type<>:type';
                    $criteria->params = array(':idOwner' => Yii::app()->user->id, ':idExecuter' => Yii::app()->user->id, 'type' => 'nurse');
                    $events = Event::model()->findAll($criteria);
                }
                if ($userType == 'nurse') {
                    $criteria->condition = '(idOwner=:idOwner OR idExecuter=:idExecuter) AND type<>:type';
                    $criteria->params = array(':idOwner' => Yii::app()->user->id, ':idExecuter' => Yii::app()->user->id, 'type' => 'schedule');
                    $events = Event::model()->findAll($criteria);
                }
                if ($userType == 'clinic') {
                    $doctors = Schedule::model()->findAllByAttributes(array('idUser' => Yii::app()->user->id));
                    foreach ($doctors as $doctor) {
                        $criteria->condition = '(idOwner=:idOwner OR idExecuter=:idExecuter) AND type=:type';
                        $criteria->params = array(':idOwner' => $doctor->id, ':idExecuter' => $doctor->id, 'type' => 'schedule');
                        $events = array_merge(Event::model()->findAll($criteria), $events);
                    }
                    $criteria->condition = 'idOwner=:idOwner AND type=:type';
                    $criteria->params = array(':idOwner' => Yii::app()->user->id, 'type' => 'note');
                    $events = array_merge(Event::model()->findAll($criteria), $events);
                }
            }

            //$events = Event::model()->findAllByAttributes(array('idOwner'=>Yii::app()->user->id, 'type'=>'schedule'));
            $html .= '<table class="days"><tr>';
            //first week
            for ($i = 1; $i <= 7; $i++) {
                if($i>=$firstDayOfWeek) {
                    $weekend = "";
                    $wrote = false;//if number in calendar already highlighted
                    if($i > 5)
                        $weekend = " weekend";
                    $class = "class=\"";
                    if (!Yii::app()->user->isGuest) {
                        foreach ($events as $key) {
                            if ($key->type != 'nurse') {
                                if (($key->eventDate == $year . "-" . $month . "-" . $day)&&($wrote == false)){
                                    $class .= "busyDay";
                                    $wrote = true;
                                }
                            }
                            if ($key->type == 'nurse') {
                                $beginDateSeconds = strtotime($key->eventDate);
                                $endDateSeconds = strtotime($key->eventDateFinish);
                                $curDateSeconds = strtotime($year . "-" . $month . "-" . $day);
                                if (($beginDateSeconds <= $curDateSeconds) && ($curDateSeconds <= $endDateSeconds)&&($wrote == false)) {
                                    $class .= "busyDay";
                                    $wrote = true;
                                }
                            }
                        }
                    }
                    $class .= $weekend . "\"";
                    $html .= '<td '.$class.'>' . $day . '</td>';
                    $day++;
                } else {
                    $html .= '<td></td>';
                }
            }
            $html .= '</tr></table>';
            $tempDay = $day;
            //full weeks
            for($i=1;$i<=$countFullWeeks;$i++) {
                $html .= '<table class="days"><tr>';
                for ($j = 1; $j <= 7; $j++) {
                    $weekend = "";
                    $wrote = false;//if number in calendar already highlighted
                    if($j > 5)
                        $weekend = " weekend";
                    $class = "class=\"";
                    if (!Yii::app()->user->isGuest) {
                        foreach ($events as $key) {
                            if ($key->type != 'nurse') {
                                if (($key->eventDate == $year . "-" . $month . "-" . $day)&&($wrote == false)){
                                    $class .= "busyDay";
                                    $wrote = true;
                                }
                            }
                            if ($key->type == 'nurse') {
                                $beginDateSeconds = strtotime($key->eventDate);
                                $endDateSeconds = strtotime($key->eventDateFinish);
                                $curDateSeconds = strtotime($year . "-" . $month . "-" . $day);
                                if (($beginDateSeconds <= $curDateSeconds) && ($curDateSeconds <= $endDateSeconds)&&($wrote == false)) {
                                    $class .= "busyDay";
                                    $wrote = true;
                                }
                            }
                        }
                    }
                    $class .= $weekend . "\"";
                    $html .= '<td '.$class.'>' . $day . '</td>';
                    $day++;
                }
                $html .= '</tr></table>';
            }
            //last week
            $html .= '<table class="days"><tr>';
            for ($i = 1; $i <= 7; $i++) {
                if($i<=$daysLastWeek) {
                    $weekend = "";
                    $wrote = false;//if number in calendar already highlighted
                    if($i > 5)
                        $weekend = " weekend";
                    $class = "class=\"";
                    if (!Yii::app()->user->isGuest) {
                        foreach ($events as $key) {
                            if ($key->type != 'nurse') {
                                if (($key->eventDate == $year . "-" . $month . "-" . $day)&&($wrote == false)) {
                                    $class .= "busyDay";
                                    $wrote = true;
                                }
                            }
                            if ($key->type == 'nurse') {
                                $beginDateSeconds = strtotime($key->eventDate);
                                $endDateSeconds = strtotime($key->eventDateFinish);
                                $curDateSeconds = strtotime($year . "-" . $month . "-" . $day);
                                if (($beginDateSeconds <= $curDateSeconds) && ($curDateSeconds <= $endDateSeconds)&&($wrote == false)) {
                                    $class .= "busyDay";
                                    $wrote = true;
                                }
                            }
                        }
                    }
                    $class .= $weekend . "\"";
                    $html .= '<td '.$class.'>' . $day . '</td>';
                    $day++;
                } else {
                    $html .= '<td></td>';
                }
            }
            $html .= '</tr></table>';

            $monthName = $this->MONTHES[$month-1];

            echo json_encode(array('html' => $html, 'year'=>$year, 'month'=>$month, 'monthName'=>$monthName));

            //$this->renderPartial('list', array('year'=>$year, 'month'=>$month), false, true);
        }

    }

    //get events on selected day
    public function actionGetDayEvents(){

        if (!Yii::app()->user->isGuest) {
            $year = Yii::app()->getRequest()->getParam('year');
            $month = Yii::app()->getRequest()->getParam('month');
            $day = Yii::app()->getRequest()->getParam('day');
            $userId = Yii::app()->user->id;
            $html = GetEventsHelper::getEvents($year, $month, $day, $userId);
            echo json_encode(array('html'=>$html));
        }

    }

    public function actionRemoveEvent(){

        if (!Yii::app()->user->isGuest) {
            if (Yii::app()->request->isAjaxRequest) {
                $eventId = Yii::app()->getRequest()->getParam('eventId');
                Event::model()->deleteByPk($eventId);
                echo 'ok';
            }
        }
    }

    public function actionGetMetro($city) {
        if(Yii::app()->request->isAjaxRequest) {
            if(!empty($city)) {
                $criteria = new CDbCriteria;
                $criteria->select = 'name';
                $criteria->distinct = true;
                $criteria->condition = 'city = :city and name <> :name and name <> :name2';
                $criteria->params = array(
                    ':city' => $city,
                    ':name' => '0',
                    ':name2' => 'Нет метро'
                );
                $model = Station::model()->findAll($criteria);
                if($model) {
                    $result = '';
                    for($i = 0; $i < count($model); $i++) {
                        $result .= $model[$i]->name;
                        if($i != count($model) - 1) $result .= ',';
                    }
                    $result = explode(',', $result);
                    $result = array_unique($result);
                    echo json_encode($result);
                }
            }
        }
    }

    public function actionBuyVip() {
        $idRate = Yii::app()->request->getPost('vip-type');
        $user = User::model()->findByPk(Yii::app()->user->id);
        $user->idRate = $idRate;
        $vipTimeEnd = strtotime(date('Y-m-d H:i'));
        if($idRate == 1)
            $vipTimeEnd += 1*86400;
        if($idRate == 2)
            $vipTimeEnd += 7*86400;
        if($idRate == 3)
            $vipTimeEnd += 30*86400;
        $user->vipTimeEnd = date('Y-m-d H:i', $vipTimeEnd);
        $user->save();
        $this->redirect(Yii::app()->request->baseUrl . '/site/'.$user->type.'/'.$user->id);
    }

    public function actionPersonal(){
        if (Yii::app()->user->isGuest) {
            $this->render('personal');
        } else {
            if(User::model()->findByPk(Yii::app()->user->id)->type == 'patient')
                $this->redirect('/site/inProcess');
            else
                $this->redirect(User::model()->findByPk(Yii::app()->user->id)->type . '/' . Yii::app()->user->id);
        }
    }

    public function actionInProcess() {
        $this->breadCrumbs = array('Страница недоступна');
        $this->render('inProcess');
    }

    public function actionEyeimpired() {
        Yii::app()->session['theme'] = 'eyeimpired';
        $this->redirect(Yii::app()->request->baseUrl . '/');
    }

    public function actionDefault() {
        Yii::app()->session['theme'] = 'default';
        $this->redirect(Yii::app()->request->baseUrl . '/');
    }

    public function actionUpdateAjax() {
        $data = array();
        $data["myValue"] = "Content updated in AJAX";

        $this->renderPartial('_ajaxContent', $data, false, true);
    }

    public function actionAddCityTime(){

        $timeCity = Yii::app()->request->getPost('time-city');
        $timeCountry = Yii::app()->request->getPost('time-country');

        /*получаем время Москвы*/
        $urlMoscowTime = 'http://dateandtime.info/ru/city.php?id=524901';
        $moscow = file_get_contents($urlMoscowTime);
        preg_match('/class="citycolumn">(.*?)<\/time>/s', $moscow, $matches);
        preg_match('/class="hms_fulldate results"(.*?)>(.*?)<\/time>/s', $moscow, $matches);
        $moscowTime = explode('<br>', $matches[2])[0];
        $moscowHour = explode(':', $moscowTime)[0];

        /*получаем время выбранного города*/
        $urlCity = 'http://dateandtime.info/ru/citysearch.php?city='.$timeCountry.'&country='.$timeCity;
        $cityInfo = file_get_contents($urlCity);
        $cityTime = null;
        $cityHour = null;
        preg_match('/class="citycolumn">(.*?)<\/time>/s', $cityInfo, $matches);
        if(empty($matches) == 1){
            preg_match('/class="hms_fulldate results"(.*?)>(.*?)<\/time>/s', $cityInfo, $matches);
            $cityTime = explode('<br>', $matches[2])[0];
            $cityHour = explode(':', $cityTime)[0];
        } else {
            preg_match('/<time(.*?)>(.*?)<\/time>/s', $matches[0], $matches1);
            if(empty($matches1) == 1) {
                $mainArray['msg'] = 'fail';
                $mainArray = json_encode($mainArray);
                echo $mainArray;
                die();
            }
            preg_match('/class="dhms"(.*?)>(.*?)<\/time>/s', $matches1[0], $matches2);
            if(empty($matches2) == 1) {
                $mainArray['msg'] = 'fail';
                $mainArray = json_encode($mainArray);
                echo $mainArray;
                die();
            }
            $cityTime = explode(' ',$matches2[2])[1];
            $cityHour = explode(':', $cityTime)[0];
        }
        $mainArray['city'] = $timeCountry;
        $mainArray['country'] = $timeCity;
        $mainArray['time'] = $cityTime;
        $arrayDaysWeek = ['Пн','Вт','Ср','Чт','Пт','Сб','Вс'];
        $dayOfWeek = intval(date('w'));
        $dayOfWeek = $arrayDaysWeek[$dayOfWeek-1];
        $mainArray['dayOfWeek'] = $dayOfWeek;
        $mainArray['msg'] = 'ok';
        $difference = $moscowHour - $cityHour;

        if(!Yii::app()->user->isGuest) {
            $worldtime = new Worldtime();
            $worldtime->city = $timeCountry;
            $worldtime->country = $timeCity;
            $worldtime->diff = $difference;
            $worldtime->userId = Yii::app()->user->id;
            $worldtime->save();

            $mainArray['id'] = $worldtime->id;
        } else {

            $worldtimes = Yii::app()->session['worldtimes'];
            if(!$worldtimes)
                $worldtimes = array();
            $maxId = count($worldtimes);
            foreach($worldtimes as $item){
                if($item['id'] > $maxId)
                    $maxId = $item['id'];
            }
            $timeItem = array();
            $timeItem['city'] = $timeCity;
            $timeItem['country'] = $timeCountry;
            $timeItem['diff'] = $difference;
            $timeItem['id'] = $maxId;

            array_push($worldtimes, $timeItem);
            Yii::app()->session['worldtimes'] = $worldtimes;

            $mainArray['id'] = $maxId;

        }

        $mainArray = json_encode($mainArray);
        echo $mainArray;
        /*
        $url = 'http://dateandtime.info/ru/citysearch.php?city='.$timeCity.'&country='.$timeCountry;
        $content = file_get_contents($url);
        */
    }

    public function actionRemoveCityTime(){

        $idTimeCity = Yii::app()->request->getPost('idTimeCity');
        if(!Yii::app()->user->isGuest) {
            Worldtime::model()->deleteByPk($idTimeCity);
        } else {
            $worldtimes = Yii::app()->session['worldtimes'];
            for($i=0;$i<count($worldtimes);$i++){
                if($worldtimes[$i]['id'] == $idTimeCity) {
                    unset($worldtimes[$i]);
                }
            }
            Yii::app()->session['worldtimes'] = $worldtimes;
        }

    }

    public function actionError()
    {

        if('404'==Yii::app()->errorHandler->error['code']) {
            $this->render('error404', []);
        }

        $exception = Yii::app()->errorHandler->exception;
        if ($exception !== null) {
            return $this->render('error', ['exception' => $exception]);
        }
    }

}