<?php

class EditController extends CController
{
    public $breadCrumbs;

    public function actionDoctor()
    {
        if (!Yii::app()->user->isGuest && Doctor::isDoctor()) {
            unset(Yii::app()->session['phoneHash']);
            unset(Yii::app()->session['newPhone']);
            $id = Yii::app()->user->id;
            $this->breadCrumbs = array(
                'Доктор' => array('/site/doctor/', 'id' => $id),
                'Редактирование профиля'
            );

            /* Model */
            $doctorModel = Doctor::model()->findByPk($id);
            $userModel = User::model()->findByPk($id);
            $stationModel = $userModel->station;
            $scheduleModel = $userModel->schedule[0];
            $scheduleModel->timework = $scheduleModel->getSchedule();
            $socialModel = Social::model()->findByPk($id);
            if(!$socialModel) {
                $socialModel = new Social();
                $socialModel->idUser = $id;
                $socialModel->save();
            }
            /* End Model */

            /* Var */
            $password = $userModel->password;
            $phone = $userModel->phone;
            $link = $doctorModel->pic;
            $image = 0;
            $filename = '';
            $path = Yii::getPathOfAlias('webroot') . '/upload/';
            $params = array(
                'doctorModel' => $doctorModel,
                'userModel' => $userModel,
                'stationModel' => $stationModel,
                'scheduleModel' => $scheduleModel,
                'socialModel' => $socialModel,
            );
            $data = array(
                'Doctor' => $doctorModel,
                'User' => $userModel,
                'Station' => $stationModel,
                'Schedule' => $scheduleModel,
                'Social' => $socialModel,
            );
            $dayWeek = array(
                'monday',
                'tuesday',
                'wednesday',
                'thursday',
                'friday',
                'saturday',
                'sunday'
            );
            /* End Var */

            if (isset($_POST['User'])) {
                foreach ($_POST as $key => $value) {
                    if (!isset($data[$key])) continue;

                    // Присваиваем входящие данные модели
                    $data[$key]->attributes = $value;

                    // Далее изменяем специфичные атрибуты каждой модели
                    if ($key == 'User') { // Если $_POST['User']
                        if ($value['phone'] != $phone) $data[$key]->phone = $phone;
                        if (empty($value['password']) || md5($value['password']) == $password) $data[$key]->password = $password;
                        else $data[$key]->password = md5($value['password']);
                    }
                    elseif ($key == 'Doctor') { // Если $_POST['Doctor']
                        $data[$key]->image = CUploadedFile::getInstance($data[$key], 'image');
                        if ($data[$key]->image) {
                            $filename = md5($data[$key]->idUser . time() . rand()) . '.' . $data[$key]->image->getExtensionName();
                            $data[$key]->pic = $filename;
                            $image = 1;
                        }

                    }
                    elseif ($key == 'Schedule') { // Если $_POST['Schedule']
                        $data[$key]->deleteSchedule();
                        $timework = json_decode($value['timework']);
                        for ($j = 0; $j < count($timework); $j++) {
                            for ($k = 0; $k < count($timework[$j]); $k++) {
                                $time = implode('-', $timework[$j][$k]);
                                if (strlen($time) > 2) {
                                    if (!empty($data[$key]->$dayWeek[$k])) $data[$key]->$dayWeek[$k] .= ',' . $time;
                                    else $data[$key]->$dayWeek[$k] .= $time;
                                } else {
                                    if (empty($data[$key]->$dayWeek[$k])) $data[$key]->$dayWeek[$k] = 'Выходной';
                                }
                            }
                        }
                    }
                    elseif ($key == 'Station') {
                        if($value['name'][0] == '0') $data[$key]->name = 'Нет метро';
                    }

                    if ($data[$key]->validate()) {
                        if ($key == 'Doctor' && $image) {
                            if (file_exists($path . $link)) unlink($path . $link);
                            $fullpath = $path . $filename;
                            $data[$key]->image->saveAs($fullpath);
                        }
                        $data[$key]->update();
                    }
                }

                $this->redirect('/site/doctor/'.$id);
            }
            $this->render('doctor', $params);
        } else $this->redirect(Yii::app()->homeUrl);
    }

    public function actionPatient()
    {
        if (!Yii::app()->user->isGuest && Patient::isPatient()) {
            unset(Yii::app()->session['phoneHash']);
            unset(Yii::app()->session['newPhone']);

            $id = Yii::app()->user->id;
            $this->breadCrumbs = array(
                'Редактирование профиля'
            );
            $patientModel = Patient::model()->findByPk($id);
            $userModel = User::model()->findByPk($id);
            $stationModel = $userModel->station;

            $password = $userModel->password;

            $data = array(
                'Patient' => $patientModel,
                'User' => $userModel,
                'Station' => $stationModel,
            );

            if (isset($_POST['User'])) {
                foreach ($_POST as $key => $value) {
                    if (!isset($data[$key])) continue;
                    $data[$key]->attributes = $value;
                    if ($key == 'User') {
                        if (empty($value['password']) || md5($value['password']) == $password) $data[$key]->password = $password;
                        else $data[$key]->password = md5($value['password']);
                    }
                    elseif ($key == 'Station') {
                        if($value['name'][0] == '0') $data[$key]->name = 'Нет метро';
                    }

                    if ($data[$key]->validate()) {
                        $data[$key]->update();
                    }
                }
            }
            $params = array(
                'patientModel' => $patientModel,
                'userModel' => $userModel,
                'stationModel' => $stationModel,
            );
            $this->render('patient', $params);
        } else $this->redirect(Yii::app()->homeUrl);
    }

    public function actionNurse()
    {
        if (!Yii::app()->user->isGuest && Nurse::isNurse()) {
            unset(Yii::app()->session['phoneHash']);
            unset(Yii::app()->session['newPhone']);

            $id = Yii::app()->user->id;
            $this->breadCrumbs = array(
                'Сиделка' => array('/site/nurse/', 'id' => $id),
                'Редактирование профиля'
            );

            // Model
            $nurseModel = Nurse::model()->findByPk($id);
            $userModel = User::model()->findByPk($id);
            $socialModel = Social::model()->findByPk($id);
            if(!$socialModel) {
                $socialModel = new Social();
                $socialModel->idUser = $id;
                $socialModel->save();
            }
            $stationModel = $userModel->station;
            // Old data
            $password = $userModel->password;
            $link = $nurseModel->pic;
            // For upload image
            $image = 0;
            $filename = '';
            $path = Yii::getPathOfAlias('webroot') . '/upload/';

            $data = array(
                'Nurse' => $nurseModel,
                'User' => $userModel,
                'Station' => $stationModel,
                'Social' => $socialModel
            );

            if (isset($_POST['User'])) {
                foreach ($_POST as $key => $value) {
                    if (!isset($data[$key])) continue;
                    $data[$key]->attributes = $value;
                    if ($key == 'User') {
                        if (empty($value['password']) || md5($value['password']) == $password) $data[$key]->password = $password;
                        else $data[$key]->password = md5($value['password']);
                    } elseif ($key == 'Nurse') {
                        $data[$key]->image = CUploadedFile::getInstance($data[$key], 'image');
                        if ($data[$key]->image) {
                            $filename = md5($data[$key]->idUser . time() . rand()) . '.' . $data[$key]->image->getExtensionName();
                            $data[$key]->pic = $filename;
                            $image = 1;
                        }
                    }
                    elseif ($key == 'Station') {
                        if($value['name'][0] == '0') $data[$key]->name = 'Нет метро';
                    }

                    if ($data[$key]->validate()) {
                        if ($key == 'Nurse' && $image) {
                            if (file_exists($path . $link)) unlink($path . $link);
                            $fullpath = $path . $filename;
                            $data[$key]->image->saveAs($fullpath);
                        }
                        $data[$key]->update();
                    }
                }
            }
            $params = array(
                'nurseModel' => $nurseModel,
                'userModel' => $userModel,
                'stationModel' => $stationModel,
                'socialModel' => $socialModel
            );
            $this->render('nurse', $params);
        } else $this->redirect(Yii::app()->homeUrl);
    }

    public function actionClinic() {
        if (!Yii::app()->user->isGuest && Clinic::isClinic()) {
            unset(Yii::app()->session['phoneHash']);
            unset(Yii::app()->session['newPhone']);
            $id = Yii::app()->user->id;
            $this->breadCrumbs = array(
                'Клиника' => array('/site/clinic/', 'id' => $id),
                'Редактирование клиники'
            );

            $criteria = new CDbCriteria;
            $criteria->with = array(
                'station',
                'schedule' => array(
                    'with' => array(
                        'spec'
                    )
                ),
                'clinic'
            );
            $criteria->condition = 't.id = :id';
            $criteria->params = array(':id' => $id);
            $userModel = User::model()->find($criteria);
            $socialModel = Social::model()->findByPk($id);
            if(!$socialModel) {
                $socialModel = new Social();
                $socialModel->idUser = $id;
                $socialModel->save();
            }

//            for($i = 0; $i < count($userModel->schedule); $i++) {
//                $userModel->schedule[$i]->timework = htmlspecialchars($userModel->schedule[$i]->getSchedule());
//            }

            foreach($userModel->schedule as $schedule) {
                $schedule->timework = htmlspecialchars($schedule->getSchedule());
            }

            /* Var */
            $password = $userModel->password;
            $link = $userModel->clinic->pic;
            $image = 0;
            $filename = '';
            $path = Yii::getPathOfAlias('webroot') . '/upload/';
            $params = array(
//                'clinicModel' => $clinicModel,
                'userModel' => $userModel,
                'socialModel' => $socialModel,
//                'stationModel' => $stationModel,
//                'scheduleModel' => $scheduleModel,
//                'specializationModel' => $specializationModel
            );
            $data = array(
                'Clinic' => $userModel->clinic,
                'User' => $userModel,
                'Station' => $userModel->station,
                'Schedule' => $userModel->schedule,
                'Social' => $socialModel
            );
            /* End Var */

            if (isset($_POST['User'])) {
                foreach ($_POST as $key => $value) {
                    if (!isset($data[$key])) continue;
                    if ($key == 'Station') {
                        if($value['name'][0] == '0') $data[$key]->name = 'Нет метро';
                        else
                            $data[$key]->name = $value['name'];
                    }
                    elseif ($key == 'Schedule') {
                        $v = 0;
                        // Обновляем расписание клиники и уже имеющихся докторов
                        for($i = 0; $i < count($data[$key]); $i++) {
                            // Очищаем старое расписание, чтобы убрать повторы при записи нового
                            $data[$key][$i]->deleteSchedule();
                            // Расписание для клиники
                            if ($data[$key][$i]->type == 'clinic') {
                                $timework = $value['timeworkClinic'];
                                $data[$key][$i]->specialization = 0;
                            }
                            // Расписание для докторов клиники
                            else {
                                $timework = $value['timework'][$v];
                                $data[$key][$i]->specialization = (int) $_POST['Specialization']['name'][$v];
                                $data[$key][$i]->name = $value['name'][$v];
                                $data[$key][$i]->price = (int) $value['price'][$v];
                                $v++;
                            }
                            // Парсим расписание
                            $data[$key][$i]->setSchedule($timework);

                            if ($data[$key][$i]->validate()) $data[$key][$i]->update();
                        }

                        // Создаем вновь добавленных докторов
                        if (count($_POST['Schedule']['name']) > count($data[$key]) - 1) {
                            for($i = 0; $i < count($_POST['Schedule']['name']) - $v; $i++) {
                                $schedule = new Schedule();
                                $schedule->idUser = $id;
                                $schedule->type = 'clinicDoc';
                                $schedule->name = $_POST['Schedule']['name'][$v];
                                $schedule->setSchedule($_POST['Schedule']['timework'][$v]);
                                $schedule->price = $_POST['Schedule']['price'][$v];
                                $schedule->specialization = $_POST['Specialization']['name'][$v];
                                $schedule->save();
                                $v++;
                            }
                        }
                    }
                    elseif ($key == 'User') {
                        $data[$key]->attributes = $value;
                        if (empty($value['password']) || md5($value['password']) == $password) $data[$key]->password = $password;
                        else $data[$key]->password = md5($value['password']);
                    }
                    elseif ($key == 'Clinic') {
                        $data[$key]->attributes = $value;
                        $data['User']->name = $data[$key]->name;
                        $data[$key]->image = CUploadedFile::getInstance($data[$key], 'image');

                        if ($data[$key]->image) {
                            $filename = md5($data[$key]->idUser . time() . rand()) . '.' . $data[$key]->image->getExtensionName();
                            $data[$key]->pic = $filename;
                            $image = 1;
                        }
                    }
                    elseif ($key == 'Social') {
                     //   echo 'TEST';
                    }

                    if ($key == 'Schedule') continue;

                    if ($data[$key]->validate()) {
                        if ($key == 'Clinic' && $image) {
                            if (file_exists($path . $link)) unlink($path . $link);
                            $fullpath = $path . $filename;
                            $data[$key]->image->saveAs($fullpath);
                        }
                        $data[$key]->update();
                    }
                }
                $this->redirect('/site/clinic/'.$id);
            }
            {
                //die(var_dump($params));
                $this->render('clinic', $params);
            }
        } else $this->redirect(Yii::app()->homeUrl);
    }

    public function actionDelete() {
        if(!Yii::app()->user->isGuest && Yii::app()->request->isAjaxRequest && Clinic::isClinic()) {
            $hash = Yii::app()->request->getParam('hash');
            $model = Schedule::model()->findAll('idUser = :id', array(':id' => Yii::app()->user->id));
            for($i = 0; $i < count($model); $i++) {
                if($hash == md5(Yii::app()->params['salt'].$model[$i]->id)) {
                    $model[$i]->delete();
                    break;
                }
            }
        } else $this->redirect(Yii::app()->homeUrl);
    }

    public function actionChangePhone($code = null, $phone = null) {
        if(!Yii::app()->user->isGuest && Yii::app()->request->isAjaxRequest) {
            $session = Yii::app()->session['phoneHash'];
            $id = Yii::app()->user->id;
            if($session == null) {
                if(preg_match('/^\+[\d+]{11}$/', $phone)) {
                    $model = User::model()->findByPk($id);
                    $hash = User::getPassword(5, true, false,false);
                    Yii::app()->session['phoneHash'] = $hash;
                    Yii::app()->session['newPhone'] = $phone;
                    $link = 'Код для смены телефона: ' . $hash;
                    Yii::app()->sms->sms_send($model->phone, $link);
                    echo json_encode(array('phone' => $phone));
                }
                else {
                    unset(Yii::app()->session['newPhone']);
                    unset(Yii::app()->session['phoneHash']);
                }
            }
            else {
                if($code == $session) {
                    $phone = Yii::app()->session['newPhone'];
                    $model = User::model()->find('phone = :phone', array(':phone' => $phone));
                    if(count($model) == 0) {
                        User::model()->updateByPk(
                            $id,
                            array(
                                'phone' => $phone
                            )
                        );
                        echo json_encode(array('status' => 'success', 'phone' => $phone));
                    }
                    else echo json_encode(array('status' => 'fail', 'msg' => 'Такой телефон уже используется'));
                }
                else echo json_encode(array('status' => 'fail', 'msg' => 'Неверный код'));
                unset(Yii::app()->session['newPhone']);
                unset(Yii::app()->session['phoneHash']);
            }
        }
        else {
            $ref = Yii::app()->request->urlReferrer;
            if(!empty($ref)) $this->redirect($ref);
            else $this->redirect(Yii::app()->request->getBaseUrl(true));
        }
    }
}