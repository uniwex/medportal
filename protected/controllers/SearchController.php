<?php

/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 26.10.2015
 * Time: 11:01
 */
class SearchController extends CController
{
    public $breadCrumbs;

    public function actionTest() {
        $criteria = new CDbCriteria;
        $criteria->compare('name', 'Новогиреево', true, 'or');
        $criteria->compare('name', 'Коломенская', true, 'or');
        $criteria->compare('name', 'Лубянка', true, 'or');

        $model = Station::model()->findAll($criteria);

        var_dump($model);
    }

    public function actionIndex()
    {
        $type = array(
            'submitDoctor' => array(
                'doctor',
                'доктора'
            ),
            'submitNurse' => array(
                'nurse',
                'сиделки'
            ),
            'submitClinic' => array(
                'clinic',
                'клиники'
            )
        );
        $param = array();
        if (isset($_POST) && count($_POST)) {
            if(isset($_POST['User']['country']))
                $_POST['country'] = $_POST['User']['country'];
            if(isset($_POST['User']['city']))
                $_POST['city'] = $_POST['User']['city'];
            if(isset($_POST['Station']))
                $_POST['metro'] = $_POST['Station']['name'][0];
            if($_POST['country'] == '0')
                $_POST['country'] = '';
            if(($_POST['city'] == '0')||($_POST['city'] == 'Выберите город'))
                $_POST['city'] = '';
            if(isset($_POST['metro']))
                if($_POST['metro'] == '0')
                    $_POST['metro'] = '';
            foreach ($type as $key => $value) {
                if (array_key_exists($key, $_POST)) {
                    $this->breadCrumbs = array('Поиск ' . $value[1]);
                    $criteria = new CDbCriteria;
                    $criteria->with = array(
                        'station' => array(
                            'alias' => 'station'
                        ),
                        $value[0] => array(
                            'alias' => $value[0]
                        )
                    );

                    $criteria->condition = 't.type = :type AND t.vipTimeEnd < :curTime';
                    $criteria->order = 'dateRegistration ASC';
                    $criteria->params = array(
                        ':type' => $value[0],
                        ':curTime' => date('Y-m-d H:i')
                    );

                    if ($key == 'submitDoctor') {
                        if (!empty($_POST['type'])) {
                            $criteria->with['doctor'] = array(
                                'alias' => 'doctor'
                            );
                            $criteria->compare('doctor.typeHuman', $_POST['type']);
                        }
                        if (isset($_POST['specialization'])) {
                            if(($_POST['specialization']) != ''){
                                $criteriaSpec = new CDbCriteria;
                                $criteriaSpec->addCondition('doctor.idSpecialization = :spec');
                                $criteriaSpec->params[':spec'] = $_POST['specialization']+1;
                                $criteria->mergeWith($criteriaSpec);
                            }
                        }
                    }
                    if (($key == 'submitNurse')&&(isset($_POST['Nurse']))) {
                        $criteriaSpec = new CDbCriteria;
                        $criteriaSpec->addCondition('nurse.residence = :residence');
                        $criteriaSpec->addCondition('nurse.food = :food');
                        $criteriaSpec->params[':residence'] = $_POST['Nurse']['residence'];
                        $criteriaSpec->params[':food'] = $_POST['Nurse']['food'];
                        $criteria->mergeWith($criteriaSpec);
                    }
                    if (isset($_POST['metro'])) {
                        $metro = $_POST['metro'];
                        $criteriaMetro = new CDbCriteria;
                        if(is_array($metro)){
                            for ($i = 0; $i < count($metro); $i++) {
                                $criteriaMetro->compare('station.name', $metro[$i], true, 'or');
                            }
                        } else {
                            if ($metro != 'Выбрать метро') {
                                $criteriaMetro->compare('station.name', $metro, true, 'or');
                                $criteria->mergeWith($criteriaMetro);
                            }
                        }
                    }
                    if (isset($_POST['city']) && !empty($_POST['city'])) {
                        $criteria->addCondition('t.city = :city');
                        $criteria->params[':city'] = $_POST['city'];
                    }
                    if (isset($_POST['country']) && !empty($_POST['country'])) {
                        $criteria->addCondition('t.country = :country');
                        $criteria->params[':country'] = $_POST['country'];
                    }
                    if (isset($_POST[$value[0]])) {
                        $criteria2 = new CDbCriteria;
                        $criteria2->compare($value[0] . '.description', $_POST[$value[0]], true, 'or');
                        $criteria2->compare($value[0] . '.specialization', $_POST[$value[0]], true, 'or');
                        $criteria2->compare('t.name', $_POST[$value[0]], true, 'or');
                        $criteria->mergeWith($criteria2);
                    }

                    $dataProvider = new CActiveDataProvider('User', array(
                        'criteria' => $criteria
                    ));
                    //var_dump($dataProvider);
                    $param['dataProvider'] = $dataProvider;
                    $param['related'] = $value[0];

                    $criteria3 = new CDbCriteria;
                    $criteria3->addCondition('type = :type AND t.vipTimeEnd >= :curTime');
                    $criteria3->params[':type'] = $value[0];
                    $criteria3->params[':curTime'] = date('Y-m-d H:i');
                    $criteria3->limit = '3';
                    $criteria3->order = 'views ASC';
                    $dataProvider2 = new CActiveDataProvider('User', array(
                        'criteria' => $criteria3,
                        'pagination' => false
                    ));
                    $param['dataProvider2'] = $dataProvider2;
                    break;
                }
            }
        } else {
            $this->breadCrumbs = array('Поиск врачей');
            if(isset($_GET['specialization'])){
                $criteria = new CDbCriteria;
                $criteria->addCondition('doctor.idSpecialization = :spec');
                $criteria->params[':spec'] = $_GET['specialization'];
                $criteria->with=array(
                    'doctor',
                );
                $dataProvider = new CActiveDataProvider('User', array(
                    'criteria' => $criteria,
                ));
                $param['dataProvider'] = $dataProvider;
                $param['related'] = 'doctor';
            } else {
                $criteria = new CDbCriteria;
                $criteria->addCondition('t.type = :type AND t.vipTimeEnd < :curTime');
                $criteria->order = 'dateRegistration ASC';
                $criteria->params[':type'] = 'doctor';
                $criteria->params[':curTime'] = date('Y-m-d H:i');
                $dataProvider = new CActiveDataProvider('User', array(
                    'criteria' => $criteria,
                ));
                $param['dataProvider'] = $dataProvider;
                $param['related'] = 'doctor';

                $criteria2 = new CDbCriteria;
                $criteria2->addCondition('type = :type AND t.vipTimeEnd >= :curTime');
                $criteria2->params[':type'] = 'doctor';
                $criteria2->params[':curTime'] = date('Y-m-d H:i');
                $criteria2->limit = '3';
                $criteria2->order = 'views ASC';

                $dataProvider2 = new CActiveDataProvider('User', array(
                    'criteria' => $criteria2,
                    'pagination' => false
                ));
                $param['dataProvider2'] = $dataProvider2;
            }
        }
        $this->render('index', $param);
    }

    public function actionGetDoctors()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $clinicId = Yii::app()->getRequest()->getParam('clinicId');
            $criteria = new CDbCriteria();

            $criteria->condition = 'type = :type and idUser = :id';
            $criteria->params = array(':type' => "clinicDoc", ':id' => $clinicId);

            $dataProvider = new CActiveDataProvider('Schedule', array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => 15,
                ),
            ));

            $param['dataProvider'] = $dataProvider;

            $this->renderPartial('list', $param, false, true);
        }
    }

    public function actionGlobal() {
        $text = "";
        $this->layout = 'main';
        if (Yii::app()->getRequest()->getParam('text')) {
            $text = Yii::app()->getRequest()->getParam('text');
        }
        $model = new Search();
        $model->search_text = $text;

        if (Yii::app()->getRequest()->getParam('type')){
            $model->filter = Yii::app()->getRequest()->getParam('type');
        } else {
            $model->filter = Search::ALL;
        }

        $lowerText = $this->strtolower_ru($text);

        if ($lowerText == "сиделка" || $lowerText == "сиделки") {
            $model->search_text = "";
            $model->filter = Search::NURSE;
        }

        if ($lowerText == "доктор" || $lowerText == "доктора" || $lowerText == "докторы") {
            $model->search_text = "";
            $model->filter = Search::DOCTOR;
        }

        if ($lowerText == "клиника" || $lowerText == "клиники") {
            $model->search_text = "";
            $model->filter = Search::CLINIC;
        }

        if ($lowerText == "статья" || $lowerText == "статьи") {
            $model->search_text = "";
            $model->filter = Search::ARTICLE;
        }


        $result = $model->getResult();
        //
        $dataProvider=new CArrayDataProvider($result, array(
            'pagination'=>array(
                'pageSize'=>20,
            ),
        ));
// $dataProvider->getData()
        $this->render('global',array(
                'dataProvider'=> $dataProvider
            )
        );
    }

    function strtolower_ru($text) {
        $alfavitlover = array('ё','й','ц','у','к','е','н','г', 'ш','щ','з','х','ъ','ф','ы','в', 'а','п','р','о','л','д','ж','э', 'я','ч','с','м','и','т','ь','б','ю');
        $alfavitupper = array('Ё','Й','Ц','У','К','Е','Н','Г', 'Ш','Щ','З','Х','Ъ','Ф','Ы','В', 'А','П','Р','О','Л','Д','Ж','Э', 'Я','Ч','С','М','И','Т','Ь','Б','Ю');
        return str_replace($alfavitupper,$alfavitlover,$text);
    }
}