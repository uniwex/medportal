<?php

class ClaimController extends CController
{
    public $layout = '//layouts/admin',
        $nameController = 'Тикеты',
        $breadCrumbs;

    public function actionIndex()
    {
        $this->breadCrumbs = array(
            'Тикеты'
        );

        $criteria = new CDbCriteria;
        $criteria->distinct = true;
        $tickets = Ticket::model()->findAll($criteria);
        $dataProvider = new CActiveDataProvider('TicketMessages');

        $reply = new TicketMessages();
        $model = new Ticket('search');
        $model->unsetAttributes();
        if(isset($_GET['Ticket'])) $model->attributes = $_GET['Ticket'];
        $this->render('index', array('model' => $model, 'tickets' => $tickets, 'dataProvider' => $dataProvider, 'reply' => $reply));
    }

    public function actionUpdate($id)
    {
        $this->breadCrumbs = array(
            'Тикеты' => array('/claim'),
            'Изменение тикета '.$id
        );
        $this->render('update');
    }

    public function loadModel($id)
    {
        $model = Ticket::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionGetMail($id) {
        if (Yii::app()->request->isAjaxRequest) {
            $dataProvider = new CActiveDataProvider('TicketMessages', array(
                'criteria' => array(
                    'condition' => 'idTicket = :id',
                    'params' => array(':id' => $id)
                )
            ));
            $this->renderPartial('list', array('dataProvider' => $dataProvider), false, true);
        }
    }

    public function actionReply() {
        if(Yii::app()->request->isAjaxRequest) {
            $model = new TicketMessages;
            $this->performAjaxValidation($model, 'ticketMessage');
            $model->attributes = $_POST['TicketMessages'];
            $model->idUser = Yii::app()->user->id;
            $model->messageDate = date('Y-m-d H:i:s');
            if($model->save()) {
                $model->ticket->status = 1;
                $model->ticket->update();
                echo json_encode(array('status' => 'success', 'id' => $model->idTicket));
            }
            else echo json_encode($model->getErrors());
        }
    }

    public function actionClose($id) {

        if(Ticket::closeTicket($id)) {
            Yii::app()->user->setFlash('success', 'Тикет закрыт!');
        }
        else {
            Yii::app()->user->setFlash('danger', 'Не удалось закрыть тикет! Возможно он уже закрыт.');
        }
        $this->redirect(Yii::app()->request->UrlReferrer);
    }

    /**
     * Performs the AJAX validation.
     * @param Clinic $model the model to be validated
     */
    protected function performAjaxValidation($model, $form)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === $form) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}