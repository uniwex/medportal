<?php

class OverviewController extends CController {
    public $breadCrumbsHome;
    public $breadCrumbs;

    public function actionIndex() {
        $this->layout = 'main';
        $this->breadCrumbs = array('Клиники изнутри');
        $clinic = Clinic::model()->findAll('virtual <> \'\'');
        $this->render('index', array('model'=>$clinic));
    }
}