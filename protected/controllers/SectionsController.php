<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 29.01.2016
 * Time: 12:48
 */
class SectionsController extends CController {

    public $breadCrumbs;

    public function actionIndex(){

        //$idCategory = Yii::app()->request->getQuery('id');

        $this->breadCrumbs = array('Разделы');

        $criteria = new CDbCriteria;
        $dataProvider = new CActiveDataProvider('Section');
        $param['dataProvider'] = $dataProvider;

        //send model object for search
        $this->render('index', $param);

    }

}