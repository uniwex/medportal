<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 29.01.2016
 * Time: 12:48
 */
class CategoriesController extends CController {

    public $breadCrumbs;

    public function actionIndex(){

        $idCategory = Yii::app()->request->getQuery('id');

        $this->breadCrumbs = array('Категории');

        if(isset($idCategory)){

        } else {
            $criteria = new CDbCriteria;
            $criteria->addCondition('id = :id1 OR id = :id2 OR id = :id3');
            $criteria->params[':id1'] = '77';
            $criteria->params[':id2'] = '81';
            $criteria->params[':id3'] = '82';
            $dataProvider = new CActiveDataProvider('Category',array(
                    'criteria' => $criteria
                ));
            $param['dataProvider'] = $dataProvider;

            //send model object for search
            $this->render('index', $param);
        }

    }

}