<?php

class NurseController extends CController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/admin';
    public $breadCrumbs,
        $menu,
        $nameController = 'Клиники';
    /**
     * @return array action filters
     */


    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        if (User::isAdmin()) {
            $this->breadCrumbs = array('Просмотр сиделки');
            $this->render('view', array(
                'model' => $this->loadModel($id),
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        if (User::isAdmin()) {
            $this->breadCrumbs = array('Создание сиделки');
            $model = new Nurse;

            if (isset($_POST['Nurse'])) {
                $model->attributes = $_POST['Nurse'];
                if (isset($model->image)) {
                    $model->image = CUploadedFile::getInstance($model, 'image');
                    $filename = md5(time() . rand(1000, 10000)) . '.' . $model->image->getExtensionName();
                    $model->pic = $filename;
                    $uploaded = $model->image->saveAs(Yii::getPathOfAlias('webroot') . '/upload/' . $filename);
                }
                $model->idUser = $_POST['Nurse']['idUser'];
                if ($model->save())
                    $this->redirect(array('view', 'id' => $model->idUser));
            }

            $this->render('create', array(
                'model' => $model,
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        if (User::isAdmin()) {
            $this->breadCrumbs = array('Изменение сиделки');
            $model = $this->loadModel($id);

            if (isset($_POST['Nurse'])) {
                $model->attributes = $_POST['Nurse'];
                $model->image = CUploadedFile::getInstance($model, 'image');
                if ($model->image) {
                    $filename = md5(time() . rand(1000, 10000)) . '.' . $model->image->getExtensionName();
                    if ($model->pic) {
                        if (file_exists(Yii::getPathOfAlias('webroot') . '/upload/' . $model->pic)) {
                            unlink(Yii::getPathOfAlias('webroot') . '/upload/' . $model->pic);
                        }

                    }
                    $model->pic = $filename;
                    $model->image->saveAs(Yii::getPathOfAlias('webroot') . '/upload/' . $filename);
                    $model->update();

                }

                if ($model->save())
                    $this->redirect(array('view', 'id' => $model->idUser));
            }


            $this->render('update', array(
                'model' => $model,
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        if (User::isAdmin()) {

            if($id != Yii::app()->user->id) {
                $partner = Partner::model()->findAll('idReferal = :id', array(':id' => $id));
                if (isset($partner)) {
                    Partner::model()->deleteAll('idReferal = :id', array(':id' => $id));
                }

                $referral = Referral::model()->findAll('idReferal = :id', array(':id' => $id));
                if (isset($referral)) {
                    Referral::model()->deleteAll('idReferal = :id', array(':id' => $id));
                }
                $executer = Event::model()->findAll('idExecuter = :id', array(':id' => $id));
                if (isset($executer)) {
                    Event::model()->deleteAll('idExecuter = :id', array(':id' => $id));
                }
                $Owner = Event::model()->findAll('idOwner = :id', array(':id' => $id));
                if (isset($Owner)) {
                    Event::model()->deleteAll('idOwner = :id', array(':id' => $id));
                }

                if (isset($social)) {
                    Social::model()->deleteAll('idUser = :id', array(':id' => $id));
                }
                $station = Station::model()->findAll('idUser = :id', array(':id' => $id));
                if (isset($station)) {
                    Station::model()->deleteAll('idUser = :id', array(':id' => $id));
                }
                $user = User::model()->findAll('id = :id', array(':id' => $id));
                if (isset($user)) {
                    User::model()->deleteAll('id = :id', array(':id' => $id));
                }

                $this->loadModel($id)->delete();

            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(Yii::app()->request->urlReferrer);
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        if (User::isAdmin()) {
            $this->breadCrumbs = array('Сиделки');
            $dataProvider = new CActiveDataProvider('Nurse');
            $this->render('index', array(
                'dataProvider' => $dataProvider,
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        if (User::isAdmin()) {
            $this->breadCrumbs = array('Список сиделок');
            $model = new Nurse('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['Nurse']))
                $model->attributes = $_GET['Nurse'];

            $this->render('admin', array(
                'model' => $model,
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Nurse the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        if (User::isAdmin()) {
            $model = Nurse::model()->findByPk($id);
            if ($model === null)
                throw new CHttpException(404, 'The requested page does not exist.');
            return $model;
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }


    /**
     * Performs the AJAX validation.
     * @param Nurse $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (User::isAdmin()) {
            if (isset($_POST['ajax']) && $_POST['ajax'] === 'nurse-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }
}
