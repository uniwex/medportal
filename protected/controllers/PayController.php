<?php

class PayController extends CController
{
    public function actionPayment()
    {

        $userModel = User::model()->findByPk(Yii::app()->user->id);
        $payed = 1000;
        $userModel->money += $payed;
        $userModel->update();
        $referralModel = Referral::model()->findByAttributes(array('idReferer'=>$userModel->id));
        $idReferal = null;
        if(isset($referralModel))
            $idReferal = $referralModel->idReferal;

        $percents = PartnerPercent::model()->findAll();

        $percentSum = 0;

        for ($i = 0; $i < count($percents); $i++) {
            if (isset($percents[$i + 1])) {
                if (($payed >= $percents[$i]->count) && ($payed < $percents[$i + 1]->count)) {
                    $percentSum = ($payed / 100) * $percents[$i]->percent;
                    break;
                }
            } else {
                $percentSum = ($payed / 100) * $percents[count($percents)-1]->percent;
            }
        }

        $partner = Partner::model()->findByPk($idReferal);
        if(!isset($partner)) {
            $partner = new Partner();
            $partner->money = $partner->money + $percentSum;
            $partner->save();
        } else {
            $partner->money = $partner->money + $percentSum;
            $partner->update();
        }
        if(isset($referralModel)) {
            $referal = Referral::model()->findByAttributes(array('idReferal' => $idReferal, 'idReferer' => $userModel->id));
            $referal->money += $percentSum;
            $referal->update();
        }

        $this->redirect("/site/".User::model()->findByPk(Yii::app()->user->id)->type."/".Yii::app()->user->id);
        die();

        //$name = Yii::app()->request->getPost('name');

        $oid = 7;

        $totalPrice = PriceForVip::model()->findByPk(1)->price;
        $name = User::model()->findByPk(Yii::app()->user->id)->type;
        $mrh_login = Yii::app()->params['robokassa'][0];
        $mrh_pass1 = Yii::app()->params['robokassa'][1];
        $inv_id = 0;
        $inv_desc = 'Покупка VIP на medportal';
        $def_sum = $totalPrice;
        $culture = 'ru';
        $encoding = 'utf-8';
        $crc = md5("$mrh_login:$def_sum:$inv_id:$mrh_pass1:shpName=$oid");
        //echo "https://auth.robokassa.ru/Merchant/Index.aspx?" . "MrchLogin=$mrh_login&OutSumm=$def_sum&InvId=$inv_id" . "&Desc=$inv_desc&SignatureValue=$crc" . "";
        $this->redirect("https://auth.robokassa.ru/Merchant/Index.aspx?" . "MrchLogin=$mrh_login&OutSum=$def_sum&InvId=$inv_id" . "&Desc=$inv_desc&SignatureValue=$crc&shpName=$oid" . "");
    }

    /* Оплата WebMoney */
    public function actionOpenPP()
    {
        if (!empty($_POST['rand']) && !empty($_POST['price']) && !empty($_POST['hash']) && !empty($_POST['id'])
            && (md5('id=' . trim($_POST['id']) . 'type=' . trim($_POST['type']) . 'rand=' . trim($_POST['rand']) . 'price=' . trim($_POST['price']) . 'wm='
                    . trim($_POST['wm']) . Yii::app()->params['openpp']['secret_key']) == trim($_POST['hash']))
        ) {
            $pay = new Pay;
            $pay->user_id = $_POST['id'];
            $pay->type = 1;
            $pay->type_pay = 0;
            $pay->sum = $_POST['price'];
            $pay->date = date('Y-m-d H:i');
            $pay->save();

            $user = User::model()->findByPk($_POST['id']);
            $user->userBalance += $_POST['price'];
            $user->save();
        }
    }

    /* Редирект покупателя на сайт Робокассы для оплаты предмета */
    public function actionPay()
    {
        $price = PriceForVip::model()->findByPk(1)->price;
        $name = $_REQUEST['shpName'];
        $mrh_login = Yii::app()->params['robokassa'][0];
        $mrh_pass1 = Yii::app()->params['robokassa'][1];
        $inv_id = 0;
        $inv_desc = 'Покупка VIP на medportal';
        $def_sum = $price;
        $culture = 'ru';
        $encoding = 'utf-8';
        $crc = md5("$mrh_login:$def_sum:$inv_id:$mrh_pass1:shpName=$name");
        $this->redirect("https://auth.robokassa.ru/Merchant/Index.aspx?" . "MrchLogin=$mrh_login&OutSum=$def_sum&InvId=$inv_id" . "&Desc=$inv_desc&SignatureValue=$crc" . "");
    }

    /* Проверка оплаты, запись истории */
    public function actionRobokassa()
    {
        $mrh_pass2 = Yii::app()->params['robokassa'][2];

        // HTTP parameters:
        $out_sum = $_REQUEST["OutSum"];
        $inv_id = $_REQUEST["InvId"];
        $crc = $_REQUEST["SignatureValue"];
        $crc = strtoupper($crc);

        $name = $_REQUEST['shpName'];

        $my_crc = strtoupper(md5("$out_sum:$inv_id:$mrh_pass2:shpName=$name"));
        //file_put_contents('robokassa.txt', $my_crc . "~~src:~" . $crc);
        //file_put_contents('robokassa1.txt', var_export($_REQUEST,1));

        if (strtoupper($my_crc) != strtoupper($crc)) {
            //file_put_contents('robokassa1.txt', 'crc != my_crc');
            //echo "bad sign\n";
            $this->render('/site/'.User::model()->findByPk(Yii::app()->user->id)->type.'/'.Yii::app()->user->id, array('res'=>1));
            exit();
        }
        echo "OK$inv_id\n";

        //file_put_contents('robokassa1.txt', 'crc == my_crc');

        $pay = new Pay();
        $pay->action = 'out';
        $pay->type = 'manual';
        $pay->sum = $out_sum;
        $pay->idUser = Yii::app()->user->id;
        $pay->time = date('Y-m-d H:i:s');
        $pay->save();

    }

    public function actionYandex()
    {
        $r = array(
            'notification_type' => Yii::app()->getRequest()->getParam('notification_type'),
            'operation_id' => Yii::app()->getRequest()->getParam('operation_id'),
            'amount' => Yii::app()->getRequest()->getParam('amount'),
            'currency' => Yii::app()->getRequest()->getParam('currency'),
            'datetime' => Yii::app()->getRequest()->getParam('datetime'),
            'sender' => Yii::app()->getRequest()->getParam('sender'),
            'codepro' => Yii::app()->getRequest()->getParam('codepro'),
            'label' => Yii::app()->getRequest()->getParam('label'),
            'sha1_hash' => Yii::app()->getRequest()->getParam('sha1_hash'),
        );

        if (sha1($r['notification_type'] . '&' .
                $r['operation_id'] . '&' .
                $r['amount'] . '&' .
                $r['currency'] . '&' .
                $r['datetime'] . '&' .
                $r['sender'] . '&' .
                $r['codepro'] . '&' .
                Yii::app()->params['yandex']['secret'] . '&' .
                $r['label']) == $r['sha1_hash']
        ) {
            //получаем данные
            $userId = $r['label'];
            $amount = floatval(Yii::app()->getRequest()->getParam('withdraw_amount'));
            $pay = new Pay;
            $pay->user_id = $userId;
            $pay->type = 1;
            $pay->type_pay = 1;
            $pay->sum = $amount;
            $pay->date = date('Y-m-d H:i:s');
            $pay->save();

            $user = User::model()->findByPk($userId);
            $user->userBalance += $amount;
            $user->save(false);
        }
    }

    /* Уведомление в случае успешного платежа */
    public function actionSuccess()
    {
        if (isset($_REQUEST['OutSum'])) {
            // as a part of SuccessURL script
            // your registration data
            $mrh_pass1 = Yii::app()->params['robokassa'][1];  // merchant pass1 here

            // HTTP parameters:
            $out_sum = $_REQUEST["OutSum"];
            $inv_id = $_REQUEST["InvId"];
            $crc = $_REQUEST["SignatureValue"];
            $crc = strtoupper($crc);  // force uppercase

            $name = $_REQUEST['shpName'];

            // build own CRC
            $my_crc = strtoupper(md5("$out_sum:$inv_id:$mrh_pass1:shpName=$name"));
            if (strtoupper($my_crc) != strtoupper($crc)) {
                echo $my_crc .' '.$crc.' ';
                echo "bad sign\n";
                exit();
            }

            // you can check here, that resultURL was called
            // (for better security)
            // OK, payment proceeds
            $this->render('/site/'.User::model()->findByPk(Yii::app()->user->id)->type.'/'.Yii::app()->user->id, array('res'=>2));
            //$this->redirect(Yii::app()->request->baseUrl.'/site/index', array('res'=>2,'sum'=>$out_sum));
        } else {
            $this->render('/site/'.User::model()->findByPk(Yii::app()->user->id)->type.'/'.Yii::app()->user->id, array('res'=>3));
            //$this->redirect(Yii::app()->request->baseUrl.'/site/index', array('res'=>2));
        }
    }

    /* Уведомление в случае отказа от платежа */
    public function actionFail()
    {
        $this->render('/site/'.User::model()->findByPk(Yii::app()->user->id)->type.'/'.Yii::app()->user->id, array('res'=>3));
        //$this->redirect(Yii::app()->request->baseUrl.'/site/index', array('res'=>3));
    }
}