<?php

class DoctorController extends CController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/admin',
        $breadCrumbs,
        $nameController = 'Частные врачи';

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        if (User::isAdmin()) {
            $this->render('view', array(
                'model' => $this->loadModel($id),
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');

    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        if (User::isAdmin()) {
            $model = new Doctor;

            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);
            $result = Clinic::model()->findAll();
            $clinicArray = [];
            $clinicArray [0] = '';
            foreach ($result as $item) {
                $clinicArray [$item->idUser] = $item->name;
            }
            if (isset($_POST['Doctor'])) {
                $model->attributes = $_POST['Doctor'];
                if (isset($model->image)) {
                    $model->image = CUploadedFile::getInstance($model, 'image');
                    $filename = md5(time() . rand(1000, 10000)) . '.' . $model->image->getExtensionName();
                    $model->pic = $filename;
                    $uploaded = $model->image->saveAs(Yii::getPathOfAlias('webroot') . '/upload/' . $filename);
                    $model->idUser=$_POST['Doctor']['idUser'];
                }
                $shedule = new Schedule();
                $shedule->idUser = $_POST['Doctor']['idUser'];
                $shedule->name = $_POST['Doctor']['name'];
                $shedule->specialization = $_POST['Doctor']['specialization'];
                $shedule->monday = '08:00-18:00';
                $shedule->tuesday = '08:00-16:00';
                $shedule->wednesday = '08:00-14:00';
                $shedule->thursday = '08:00-14:00';
                $shedule->friday = '08:00-14:00';
                $shedule->saturday = 'Выходной';
                $shedule->sunday = 'Выходной';
                $shedule->price = '1500';
                $shedule->type = 'doctor';
                $shedule->timeSize = '30';
                $shedule->save();
                $model->name = $_POST['Doctor']['name'];
                if ($model->save())
                    $this->redirect(array(
                        'view',
                        'id' => $model->idUser
                    ));
            }
            $this->render('create', array(
                'model' => $model,
                'clinic' => $clinicArray,
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');

    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        if (User::isAdmin()) {
            $this->breadCrumbs = array(
                'Частные врачи' => array('/doctor/admin'),
                'Обновление информации'
            );
            $result = Clinic::model()->findAll();
            $clinicArray = [];
            $clinicArray [0] = '';
            foreach ($result as $item) {
                $clinicArray [$item->idUser] = $item->name;
            }
            $specializations = Specialization::model()->findAll();
            $model = $this->loadModel($id);
            if (isset($_POST['Doctor'])) {

                $model->attributes = $_POST['Doctor'];
                $model->image = CUploadedFile::getInstance($model, 'image');
                if ($model->image) {
                    $filename = md5(time() . rand(1000, 10000)) . '.' . $model->image->getExtensionName();
                    if ($model->pic) {
                        if (file_exists(Yii::getPathOfAlias('webroot') . '/upload/' . $model->pic)) {
                            unlink(Yii::getPathOfAlias('webroot') . '/upload/' . $model->pic);
                        }
                    }
                    $model->pic = $filename;
                    $model->image->saveAs(Yii::getPathOfAlias('webroot') . '/upload/' . $filename);
                    $model->update();
                }
                $model->name = $_POST['Doctor']['name'];
                $model->isPhototherapy = $_POST['Doctor']['isPhototherapy'];
                if ($model->save()) {

                    $this->redirect(array(
                        'view',
                        'id' => $model->idUser,
                    ));
                }
            }

            $this->render('update', array(
                'model' => $model,
                'specializations' => $specializations,
                'clinic' => $clinicArray,
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');

    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        if (User::isAdmin()) {

            if($id != Yii::app()->user->id) {
                $partner = Partner::model()->findAll('idReferal = :id', array(':id' => $id));
                if (isset($partner)) {
                    Partner::model()->deleteAll('idReferal = :id', array(':id' => $id));
                }
                $schedule = Schedule::model()->findAll('idUser = :id', array(':id' => $id));
                $referral = Referral::model()->findAll('idReferal = :id', array(':id' => $id));
                if (isset($referral)) {
                    Referral::model()->deleteAll('idReferal = :id', array(':id' => $id));
                }
                $executer = Event::model()->findAll('idExecuter = :id', array(':id' => $id));
                if (isset($executer)) {
                    Event::model()->deleteAll('idExecuter = :id', array(':id' => $id));
                }
                $Owner = Event::model()->findAll('idOwner = :id', array(':id' => $id));
                if (isset($Owner)) {
                    Event::model()->deleteAll('idOwner = :id', array(':id' => $id));
                }
                if (isset($schedule)) {
                    Schedule::model()->deleteAll('idUser = :id', array(':id' => $id));
                }
                if (isset($social)) {
                    Social::model()->deleteAll('idUser = :id', array(':id' => $id));
                }
                $station = Station::model()->findAll('idUser = :id', array(':id' => $id));
                if (isset($station)) {
                    Station::model()->deleteAll('idUser = :id', array(':id' => $id));
                }
                $user = User::model()->findAll('id = :id', array(':id' => $id));
                if (isset($user)) {
                    User::model()->deleteAll('id = :id', array(':id' => $id));
                }

                $this->loadModel($id)->delete();

            }

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        if (User::isAdmin()) {
            $dataProvider = new CActiveDataProvider('Doctor');
            $this->render('index', array(
                'dataProvider' => $dataProvider,
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        if (User::isAdmin()) {
            $this->breadCrumbs = array(
                'Частные врачи'
            );
            $model = new Doctor('search');
            $model->unsetAttributes();  // clear any default values

            $specializations = Specialization::model()->findAll();

            if (isset($_GET['Doctor']))
                $model->attributes = $_GET['Doctor'];

            $this->render('admin', array(
                'model' => $model,
                'specializations' => $specializations
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Doctor the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        if (User::isAdmin()) {
            $model = Doctor::model()->findByPk($id);
            if ($model === null)
                throw new CHttpException(404, 'The requested page does not exist.');
            return $model;
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Performs the AJAX validation.
     * @param Doctor $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (User::isAdmin()) {
            if (isset($_POST['ajax']) && $_POST['ajax'] === 'doctor-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }
}
