<?php

/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 03.11.2015
 * Time: 12:37
 */
class EventsController extends CController
{
    public $breadCrumbs;

    public function actionIndex()
    {
        if (!Yii::app()->user->isGuest) {
            $this->breadCrumbs = array('События');
            $year = Yii::app()->getRequest()->getParam('year');
            $month = Yii::app()->getRequest()->getParam('month');
            $day = Yii::app()->getRequest()->getParam('day');
            if(!$year)
                $year = date('Y');
            if(!$month)
                $month = date('m');
            if(!$day)
                $day = date('d');
            $userId = Yii::app()->user->id;

            $html = GetEventsHelper::getEventsMonth($year, $month, $day, $userId);

            $this->render('index', array('html'=>$html));
        } else $this->redirect('/');
    }

    protected function performAjaxValidation($model, $form){
        if (isset($_POST['ajax']) && $_POST['ajax'] === $form) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}