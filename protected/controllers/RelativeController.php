<?php

class RelativeController extends CController {

    public $breadCrumbs;
    public $breadCrumbsHome;

    public function actionIndex() {
        $this->layout = 'main';
        $this->breadCrumbs = array('Сравнительный анализ лечения');
        $this->render('index');
    }
    public function actionAlphabet() {
        $this->layout = 'main';
        $this->breadCrumbs = array('Сравнительный анализ' => Yii::app()->request->baseUrl . '/relative', 'Страны по алфавиту');
        $this->render('alphabet');
    }
    public function actionDirections() {
        $this->layout = 'main';
        $this->breadCrumbs = array('Сравнительный анализ' => Yii::app()->request->baseUrl . '/relative', 'Направления медицинского туризма');
        $this->render('directions');
    }
    public function actionGetArticle($id,$country) {
        $result = Countries::model()->findByPk($country);
        $article = LightArticle::model()->findByPk($id);
        if (empty($result)) {
            echo $article->text;
        } else {
            echo $article->text . '<p><a href = "../articles/'.$result->idArticle.'">Читать полный обзор по стране</a></p>';
        }
    }
}