<?php

class StatisticsController extends CController
{
    public $breadCrumbs,
        $menu,
        $nameController = 'Статистика',
        $layout = '//layouts/admin';

    public function actionIndex()
    {
        if (User::isAdmin()) {
            $this->redirect('/statistics/general');
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    public function actionGeneral()
    {
        if (User::isAdmin()) {
            $this->breadCrumbs = array(
                'Общая статистика'
            );

            $model = new Statistic('search');
            $model->unsetAttributes();
            if (isset($_GET['Statistic'])) $model->attributes = $_GET['Statistic'];
            $this->render('general', array('model' => $model));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    public function actionPay()
    {
        if (User::isAdmin()) {
            $this->breadCrumbs = array(
                'Статистика оплат'
            );

            $criteria = new CDbCriteria;
            $criteria->distinct = true;

            $payments = Pay::model()->findAll($criteria);
            $model = new Pay('search');
            $model->unsetAttributes();
            if (isset($_GET['Pay'])) $model->attributes = $_GET['Pay'];
            $this->render('pay', array('model' => $model, 'payments' => $payments));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    public function actionReferral()
    {
        if (User::isAdmin()) {
            $this->breadCrumbs = array(
                'Статистика реферальной системы'
            );

            $model = new Partner('search');
            $model->unsetAttributes();
            if (isset($_GET['Partner'])) $model->attributes = $_GET['Partner'];
            $dataProvider = new CActiveDataProvider('Referral');
            $partners = Partner::model()->with(array('user'))->findAll();
            $this->render('referral', array('model' => $model, 'partners' => $partners, 'dataProvider' => $dataProvider));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    public function actionGetReferral($id)
    {
        if (User::isAdmin()) {
            if (Yii::app()->request->isAjaxRequest) {
                $dataProvider = new CActiveDataProvider('Referral', array(
                    'criteria' => array(
                        'condition' => 'idReferal = :id',
                        'params' => array(':id' => $id)
                    )
                ));
                $this->renderPartial('list', array('dataProvider' => $dataProvider), false, true);
            }
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }
}