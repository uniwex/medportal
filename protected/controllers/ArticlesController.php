<?php

class ArticlesController extends CController
{
    public $breadCrumbs;

    public function actionTest()
    {
        $this->breadCrumbs = array('Статьи');
        $this->layout = 'main';
        $dataProvider = new CActiveDataProvider('Article');
        $this->render('test/index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionIndex(){

        $idCategory = Yii::app()->request->getQuery('category');

        $this->breadCrumbs = array('Статьи');

        if(isset($idCategory)){

            $criteria = new CDbCriteria;
            $criteria->addCondition('idCategory = :idCategory');
            $criteria->params[':idCategory'] = $idCategory;
            $dataProvider = new CActiveDataProvider('Article',array(
                'criteria' => $criteria
            ));

            $param['dataProvider'] = $dataProvider;

            //send model object for search
            $this->render('index', $param);

        } else {
            $model = new Article('search');
            $model->unsetAttributes();  // clear any default values
            //if(isset($_GET['Article']))
            //$model->attributes=$_GET['Article'];

            //send model object for search
            $this->render('index', array(
                    'dataProvider' => $model->search(),
                    'model' => $model)
            );
        }
    }

    public function actionList(){

        $idSection = Yii::app()->request->getQuery('id');

        $this->breadCrumbs = array('Статьи');

        if(isset($idSection)){

            $section=Section::model()->findByPk($idSection);
            $categories = $section->category;

            $articles = $categories[0]->getArticles();

            $criteria = new CDbCriteria;
            $criteria->addCondition('idSection = '.$idSection);
            //$criteria->param(array(':idSection'=> $idSection));

            $section = new CActiveDataProvider('Section',array(
                'criteria' => $criteria
            ));

            $article_ids = array();
            foreach($articles as $article){
                $article_ids[] = $article->id;
            }

            $criteria2 = new CDbCriteria;
            $criteria2->addCondition('id in ('.implode(',',$article_ids).')');

            $articles = new CActiveDataProvider('Article',array(
                'criteria' => $criteria2
            ));

            //die(var_dump($articles));

            $param['articles'] = $articles;

            //send model object for search
            $this->render('index', $param);
        }
    }

    public function actionView(){

        $idArticle = Yii::app()->request->getQuery('id');
        if($idArticle) {
            $this->breadCrumbs = array('Статьи' => Yii::app()->request->baseUrl . '/sections', 'Просмотр статьи');
            $article = Article::model()->findByPk($idArticle);
            $this->layout = 'main';
            $this->render('view', array('article' => $article));
        } else {
            $this->breadCrumbs = array('Статьи');
            $articles = Article::model()->findAll();
            $this->layout = 'main';
            $this->render('index', array('articles' => $articles));
        }

    }

    public function actionEdit(){

        $idArticle = Yii::app()->request->getQuery('id');
        if($idArticle) {
            $this->breadCrumbs = array('Статьи' => Yii::app()->request->baseUrl . '/sections', 'Редактирование статьи');
            $articleModel = Article::model()->findByPk($idArticle);
            if(($articleModel->idUser == Yii::app()->user->id)||( User::model()->findByPk(Yii::app()->user->id)->role == 'admin')) {
                $categoryModel = new Category();
                $this->performAjaxValidation($articleModel, 'edit-article');
                $this->performAjaxValidation($categoryModel, 'edit-article');
                $this->layout = 'main';
                $this->render('edit', array('articleModel' => $articleModel, 'categoryModel' => $categoryModel));
            } else {
                $this->redirect(Yii::app()->request->getBaseUrl(true).'/articles');
            }
        } else {
            $this->breadCrumbs = array('Статьи');
            $articles = Article::model()->findAll();
            $this->layout = 'main';
            $this->render('index', array('articles' => $articles));
        }

    }

    public function actionUpdate(){

        if (!Yii::app()->user->isGuest) {
            $articleModel = new Article();
            $categoryModel = new Category();
            $this->performAjaxValidation($articleModel, 'edit-article');
            if  (($_POST['Article'])&&($_POST['Category'])) {
                $articleModel->attributes = $_POST['Article'];
                $categoryModel->attributes = $_POST['Category'];
                $tmpImage = CUploadedFile::getInstance($articleModel, 'image');
                $filename = '';
                if($tmpImage) {
                    $filename = md5(time() . rand()) . '.' . $tmpImage->getExtensionName();
                    $articleModel->image = $filename;
                    $path = Yii::getPathOfAlias('webroot') . '/upload/articles/' . $filename;
                }
                $articleModel->idUser = Yii::app()->user->id;
                $articleModel->time = date('Y-m-d H:i:s');
                $idsCategory = $_POST['Category'];
                $categoriesString = array();
                foreach($idsCategory['name'] as $idCategory){
                    array_push($categoriesString, '@'. $idCategory . '@');
                }
                $idsCategory = implode(',',$categoriesString);
                $articleModel->idCategory = $idsCategory;
                if ($articleModel->validate()) {
                    if($tmpImage) {
                        Article::model()->updateByPk($_POST['Article']['id'], array('name' => $articleModel->name, 'text' => $articleModel->text, 'image' => $filename, 'idCategory' => $articleModel->idCategory));
                        $tmpImage->saveAs($path);
                    }
                    else {
                        Article::model()->updateByPk($_POST['Article']['id'], array('name'=>$articleModel->name,'text'=>$articleModel->text, 'idCategory' => $articleModel->idCategory));
                    }
                    $this->redirect(Yii::app()->request->getBaseUrl(true).'/sections');
                } else
                    print_r($articleModel->getErrors());
            } else
                echo 'error2';
        }
        else{
            $this->redirect(Yii::app()->homeUrl);
        }

    }

    public function actionAdd(){

        if (!Yii::app()->user->isGuest) {
            $articleModel = new Article();
            $categoryModel = new Category();
            $this->performAjaxValidation($articleModel, 'add-article');
            $this->performAjaxValidation($categoryModel, 'add-article');
            $this->breadCrumbs = array('Статьи' => '/sections', 'Добавление новой статьи');
            $this->layout = 'main';
            $this->render('add', array('articleModel' => $articleModel, 'categoryModel' => $categoryModel));
        }
        else{
            $this->redirect(Yii::app()->homeUrl);
        }

    }

    public function actionSaveNew(){

        if (!Yii::app()->user->isGuest) {
            $articleModel = new Article();
            $categoryModel = new Category();
            $this->performAjaxValidation($articleModel, 'add-article');
            if (($_POST['Article'])&&($_POST['Category'])) {
                $articleModel->attributes = $_POST['Article'];
                $categoryModel->attributes = $_POST['Category'];
                $tmpImage = CUploadedFile::getInstance($articleModel, 'image');
                if($tmpImage) {
                    $filename = md5(time() . rand()) . '.' . $tmpImage->getExtensionName();
                    $articleModel->image = $filename;
                    $path = Yii::getPathOfAlias('webroot') . '/upload/articles/' . $filename;
                }
                $articleModel->idUser = Yii::app()->user->id;
                $articleModel->time = date('Y-m-d H:i:s');
                $idsCategory = array();
                foreach($categoryModel->name as $categoryName){
                    array_push($idsCategory, '@' . $categoryName . '@');
                }
                $idsCategoryStr = implode(',',$idsCategory);
                $articleModel->idCategory = $idsCategoryStr;
                if ($articleModel->validate()) {
                    $articleModel->save();
                    if($tmpImage)
                        $tmpImage->saveAs($path);
                    $this->redirect(Yii::app()->request->getBaseUrl(true).'/sections');
                } else
                    print_r($articleModel->getErrors());
            } else
                echo 'error code: 2';
        }
        else{
            $this->redirect(Yii::app()->homeUrl);
        }

    }

    //function remove the article by id
    //input parameters:
    ////id
    public function actionRemove(){

        if (!Yii::app()->user->isGuest) {
            $idArticle = Yii::app()->request->getPOST('id');
            Article::model()->deleteByPk($idArticle);
        }
    }

    public function actionGetArticles(){

        //if (!Yii::app()->user->isGuest) {

            $idCategory = Yii::app()->request->getPOST('idCategory');
            $userId = Yii::app()->request->getPOST('userId');
            $articleDate = Yii::app()->request->getPOST('articleDate');
            $sorting = Yii::app()->request->getPOST('sorting');
            $criteria = new CDbCriteria;
            $condition = '';

            if($idCategory != -1)
                $condition .= "idCategory LIKE '%". $idCategory . "%' AND";
            if($userId != -1)
                $condition .= " idUser = ". $userId . " AND";
            if($articleDate != '')
                $condition .= " time LIKE '". $articleDate . "%' AND";

            $condition = preg_replace('/AND$/','',$condition);

            $criteria->condition=$condition;
            if ($sorting == 0) {
                $criteria->order = "time DESC";
            } elseif($sorting == 1) {
                $criteria->order = "avg_rating DESC";
            } elseif($sorting == 2) {
                $criteria->order = "avg_rating ASC";
            }


            $articles = Article::model()->findAll($criteria);
            $articlesForJson = array();
            foreach ($articles as $key) {
                $name = User::model()->findByPk($key->idUser)->name;
                $article = array(
                    'id' => $key->id,
                    'idUser' => $key->idUser,
                    'author' => $name,
                    'name' => $key->name,
                    'text' => $key->text,
                    'time' => $key->time,
                    'image' => $key->image,
                );
                array_push($articlesForJson, $article);
            }
            echo json_encode($articlesForJson);
        //}
    }

    protected function performAjaxValidation($model, $form)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === $form) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}