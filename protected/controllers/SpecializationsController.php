<?php

class SpecializationsController extends CController {

    public $breadCrumbs;
    public $breadCrumbsHome;


    public function actionIndex() {
        $this->layout = 'main';
        $this->breadCrumbs = array('Все специализации');

        $spec = Specialization::model()->findAll();
        $this->render('index', array('specializations' => $spec));
    }
}