<?php

/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 03.11.2015
 * Time: 12:37
 */
class NoteController extends CController
{
    public $breadCrumbs;

    public function actionIndex()
    {
        if (!Yii::app()->user->isGuest) {
            $this->breadCrumbs = array('Создание заметки');
            $step = 1;
            $eventModel = new Event();
            $eventModel->setScenario('note');
            $eventModel->idOwner = Yii::app()->user->id;
            $this->performAjaxValidation($eventModel, 'add-note');
            if (isset($_POST['Event'])) {
                $eventModel->attributes = $_POST['Event'];
                $eventModel->type = 'note';
                $eventDateTime = $_POST['Event']['eventDate'];
                $eventDateTime = explode(' ', $eventDateTime);
                $eventModel->eventDate = $eventDateTime[0];
                $eventModel->eventTime = $eventDateTime[1];
                if ($eventModel->validate()) {
                    $eventModel->save();
                    $step = 2;
                }

            }
            $this->render('index', array('step'=>$step, 'eventModel'=>$eventModel));
        } else $this->redirect('/');
    }

    protected function performAjaxValidation($model, $form){
        if (isset($_POST['ajax']) && $_POST['ajax'] === $form) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}