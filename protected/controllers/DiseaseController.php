<?php

class DiseaseController extends CController
{
    public $breadCrumbsHome;
    public $breadCrumbs;

    public function actionIndex()
    {
        $this->layout = 'main';
        $this->breadCrumbs = array('Заболевания');
        $directions = DirectionsDiseases::model()->findAll();
        $allDiseases = Diseases::model()->findAll();
        $criteria = new CDbCriteria(array(
            'condition' => 'letter =:letter',
            'params' => array(':letter' => 'А')
        ));
        $dataProvider = new CActiveDataProvider('Diseases', array(
            'pagination' => array(
                'pageSize' => 33,
            ),
            'criteria' => $criteria,
        ));
        if (isset($_GET['type']) == "ajax") {
            $this->renderPartial('_listView', array('dataProvider' => $dataProvider));
        } else {
            $this->render('index', array(
                    'dataProvider' => $dataProvider,
                    'allDiseases' => $allDiseases,
                    'directions' => $directions
                )
            );
        }

    }

    public function actionView($id)
    {
        $this->layout = 'main';
        $disease = Diseases::model()->findByPk($id);
        $this->breadCrumbs = array('Заболевания' => Yii::app()->request->baseUrl . '/disease', $disease->name);
        $direction = DirectionsDiseases::model()->findByPk($disease->idDirection);
        $clinics = Clinic::model()->findAll("SPECIALIZATION like :direction", array(':direction' => '%' . $direction->name . '%'));
        $articles = Article::model()->findAll("TEXT like :name", array(':name' => '%' . $disease->name . '%'));
        $this->render('view', array('disease' => $disease, 'articles' => $articles, 'clinics' => $clinics));
    }

    public function actionSearch()
    {
        $letter = $_GET['sort'];
        $directions = DirectionsDiseases::model()->findAll();
        $criteria = new CDbCriteria(array(
            'condition' => 'letter =:letter',
            'params' => array(':letter' => $letter)
        ));
        $dataProvider = new CActiveDataProvider('Diseases', array(
            'pagination' => array(
                'pageSize' => 33,
            ),
            'criteria' => $criteria,
        ));
        if (isset($_GET['type']) == "ajax") {
            $this->renderPartial('_listView', array('dataProvider' => $dataProvider));
        } else
            if (isset($_GET['Diseases_page'])) {
                $this->layout = 'main';
                $this->breadCrumbs = array('Заболевания');
                $directions = DirectionsDiseases::model()->findAll();
                $allDiseases = Diseases::model()->findAll();
                $this->render('index', array(
                        'dataProvider' => $dataProvider,
                        'allDiseases' => $allDiseases,
                        'directions' => $directions
                    )
                );
            } else {
                $this->layout = 'main';
                $this->breadCrumbs = array('Заболевания');
                $directions = DirectionsDiseases::model()->findAll();
                $allDiseases = Diseases::model()->findAll();
                $this->render('index', array(
                    'dataProvider' => $dataProvider,
                    'allDiseases' => $allDiseases,
                    'directions' => $directions
                ));
            }
    }

    public function actionDirection($id)
    {
        $this->layout = 'main';
        $this->breadCrumbs = array('Заболевания' => Yii::app()->request->baseUrl . '/disease', 'Направления заболеваний');
        $direction = DirectionsDiseases::model()->with('diseases')->findByPk($id);
        $allDirections = DirectionsDiseases::model()->findAll();
        $this->render('direction', array('direction' => $direction, 'allDirections' => $allDirections, 'diseases' => null));
    }

    public function actionChangeDirection()
    {
        $id = $_GET['id'];
        $direction = DirectionsDiseases::model()->with('diseases')->findByPk($id);
        $this->renderPartial('_directions', array('direction' => $direction));
    }

    public function actionSearchDisease()
    {
        $diseases = Diseases::model()->findAll("NAME like :request", array(':request' => '%' . $_GET['q'] . '%'));
        $this->renderPartial('_searchDisease', array('diseases' => $diseases));
    }
}