<?php

class NewsController extends CController {

    public $breadCrumbs;
    public $breadCrumbsHome;


    public function actionIndex() {
        $this->layout = 'main';
        $this->breadCrumbs = array('Новости');

        $sort = new CSort();
        $sort->attributes = array(
            'date'=>array(
                'asc'=>'date',
                'desc'=>'date desc',
                'default'=>'date DESC',
                'label'=>'дате',
            ),
        );


        $dataProvider = new CActiveDataProvider('News',
            array(
                'sort'=>$sort,
                'pagination'=>array(
                    'pageSize'=>6,
                ),
            )
        );

        $dataProvider->sort->defaultOrder='date DESC';
        $this->render('index', array('dataProvider' => $dataProvider));
    }
}
