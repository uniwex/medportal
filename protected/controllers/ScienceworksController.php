<?php

class ScienceworksController extends CController
{
    public $breadCrumbs;
    public $breadCrumbsHome;


    public function actionIndex(){
        $param = [];


        $criteria = new CDbCriteria();

        if (isset($_GET['id'])){
            $param['model'] = ScienceWorks::model()->findByPk(intval($_GET['id']));
            $name =  (strlen($param['model']->name)>83) ? substr($param['model']->name, 0, 83).'...' : substr($param['model']->name, 0, 83) ;
            $this->breadCrumbs = array('Научные работы',$name);
            $this->render('details', $param);
        } else {
            $this->breadCrumbs = array('Научные работы');
            $criteria->limit = 3;
            $count = ScienceWorks::model()->count($criteria);

            $pages = new CPagination($count);
            $pages->setPageSize(3);
            $pages->applyLimit($criteria);
            $result = ScienceWorks::model()->findAll($criteria);
            $dataProvider = new CArrayDataProvider($result);
            $param['dataProvider'] = $dataProvider;
            $param['pages'] = $pages;
            $this->render('index', $param);
        }

    }
    public function actionDownload(){
        if (isset($_GET['id'])){
            $name = ScienceWorks::model()->findByPk(intval($_GET['id']))->link;
            $file = (Yii::getPathOfAlias('webroot') . "/upload/" . $name);
            header ("Content-Type: application/octet-stream");
            header ("Accept-Ranges: bytes");
            header ("Content-Length: ".filesize($file));
            header ("Content-Disposition: attachment; filename=".$name);
            readfile($file);
        }
    }
}