<?php

class VisitingHoursController extends CController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/admin';
    public $breadCrumbs,
        $menu,
        $nameController = 'Режим работы';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        if (User::isAdmin()) {
            $this->render('view', array(
                'model' => $this->loadModel($id),
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        if (User::isAdmin()) {
            $result = Clinic::model()->findAll();
            $clinicArray = [];
            foreach ($result as $item) {
                $clinicArray [$item->idUser] = $item->name;
            }
            $days = [];
            $days += ['Понедельник'=>'Понедельник'];
            $days += ['Вторник'=>'Вторник'];
            $days += ['Среда'=>'Среда'];
            $days += ['Четверг'=>'Четверг'];
            $days += ['Пятница'=>'Пятница'];
            $days += ['Суббота'=>'Суббота'];
            $days += ['Воскресенье'=>'Воскресенье'];



            $model = new VisitingHours;

    // Uncomment the following line if AJAX validation is needed
    // $this->performAjaxValidation($model);

            if (isset($_POST['VisitingHours'])) {
                $model->attributes = $_POST['VisitingHours'];
                if ($model->timeFrom < 10) {
                    $model->timeFrom = '0'.$model->timeFrom;
                }
                if ($model->timeTo < 10) {
                    $model->timeTo = '0'.$model->timeTo;
                }
                $model->timeFrom = $model->timeFrom.':00:00';
                $model->timeTo = $model->timeTo.':00:00';
                if ($model->save())
                    $this->redirect(array('admin'));
            }

            $this->render('create', array(
                'model' => $model,
                'clinic' => $clinicArray,
                'days' => $days,
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        if (User::isAdmin()) {
            $result = Clinic::model()->findAll();
            $clinicArray = [];
            foreach ($result as $item) {
                $clinicArray [$item->idUser] = $item->name;
            }
            $days = [];
            $days += ['Понедельник'=>'Понедельник'];
            $days += ['Вторник'=>'Вторник'];
            $days += ['Среда'=>'Среда'];
            $days += ['Четверг'=>'Четверг'];
            $days += ['Пятница'=>'Пятница'];
            $days += ['Суббота'=>'Суббота'];
            $days += ['Воскресенье'=>'Воскресенье'];

            $model = $this->loadModel($id);
            $model->timeFrom = str_replace(':00:00','',$model->timeFrom);
            $model->timeTo = str_replace(':00:00','',$model->timeTo);

    // Uncomment the following line if AJAX validation is needed
    // $this->performAjaxValidation($model);

            if (isset($_POST['VisitingHours'])) {
                $model->attributes = $_POST['VisitingHours'];
                if ($model->timeFrom < 10) {
                    $model->timeFrom = '0'.$model->timeFrom;
                }
                if ($model->timeTo < 10) {
                    $model->timeTo = '0'.$model->timeTo;
                }
                $model->timeFrom = $model->timeFrom.':00:00';
                $model->timeTo = $model->timeTo.':00:00';
                if ($model->save())
                    $this->redirect(array('admin'));
            }

            $this->render('update', array(
                'model' => $model,
                'clinic' => $clinicArray,
                'days' => $days,
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        if (User::isAdmin()) {
            $this->loadModel($id)->delete();

    // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }


    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        if (User::isAdmin()) {
            $dataProvider = new CActiveDataProvider('VisitingHours');
            $this->render('index', array(
                'dataProvider' => $dataProvider,
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        if (User::isAdmin()) {
            $model = new VisitingHours('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['VisitingHours']))
                $model->attributes = $_GET['VisitingHours'];

            $this->render('admin', array(
                'model' => $model,
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return VisitingHours the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = VisitingHours::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param VisitingHours $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'visiting-hours-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
