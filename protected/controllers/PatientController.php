<?php

class PatientController extends CController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/admin',
        $breadCrumbs,
        $nameController = 'Пациенты';
    /**
     * @return array action filters
     */


    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        if (User::isAdmin()) {
            $this->breadCrumbs = array(
                'Просмотр пациента'
            );
            $this->render('view', array(
                'model' => $this->loadModel($id),
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        if (User::isAdmin()) {
            $this->breadCrumbs = array(
                'Создание пациента'
            );
            $model = new Patient;

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

            if (isset($_POST['Patient'])) {

                $model->attributes = $_POST['Patient'];
                if ($model->save())
                    $this->redirect(array('view', 'id' => $model->idUser));
            }

            $this->render('create', array(
                'model' => $model,
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        if (User::isAdmin()) {
            $model = $this->loadModel($id);
            $this->breadCrumbs = array(
                'Изменение данных пациента'
            );

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

            if (isset($_POST['Patient'])) {
                $model->attributes = $_POST['Patient'];
                if ($model->save())
                    $this->redirect(array('view', 'id' => $model->idUser));
            }

            $this->render('update', array(
                'model' => $model,
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        if (User::isAdmin()) {
            if($id != Yii::app()->user->id) {
                $event = Event::model()->findAll('idOwner = :id', array(':id' => $id));
                if (isset($event)) {
                    Event::model()->deleteAll('idOwner = :id', array(':id' => $id));
                }

                $social = Social::model()->findAll('idUser = :id', array(':id' => $id));
                if (isset($social)) {
                    Social::model()->deleteAll('idUser = :id', array(':id' => $id));
                }
                $station = Station::model()->findAll('idUser = :id', array(':id' => $id));
                if (isset($station)) {
                    Station::model()->deleteAll('idUser = :id', array(':id' => $id));
                }
                $user = User::model()->findAll('id = :id', array(':id' => $id));
                if (isset($user)) {
                    User::model()->deleteAll('id = :id', array(':id' => $id));
                }
                $this->loadModel($id)->delete();
            }

            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        if (User::isAdmin()) {
            $dataProvider = new CActiveDataProvider('Patient');
            $this->render('index', array(
                'dataProvider' => $dataProvider,
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        if (User::isAdmin()) {
            $this->breadCrumbs = array(
                'Пациенты'
            );
            $model = new Patient('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['Patient']))
                $model->attributes = $_GET['Patient'];

            $this->render('admin', array(
                'model' => $model,
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Patient the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        if (User::isAdmin()) {
            $model = Patient::model()->findByPk($id);
            if ($model === null)
                throw new CHttpException(404, 'The requested page does not exist.');
            return $model;
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Performs the AJAX validation.
     * @param Patient $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'patient-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
