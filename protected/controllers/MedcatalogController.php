<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 29.01.2016
 * Time: 12:48
 */
class MedcatalogController extends CController {

    public $breadCrumbs;

    public function actionProfdisease(){

        $this->breadCrumbs = array('Медицинский справочник » Профзаболевания');

        //send model object for search
        $this->render('profdisease');

    }

    public function actionParapharmaceutic(){

        $this->breadCrumbs = array('Медицинский справочник » Парафармацевтика');

        //send model object for search
        $this->render('parapharmaceutic');

    }

}