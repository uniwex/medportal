<?php

class QuestionsController extends CController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/admin';
    public $breadCrumbs,
        $menu,
        $nameController = 'Вопросы';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        if (User::isAdmin()) {
            $this->render('view', array(
                'model' => $this->loadModel($id),
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        if (User::isAdmin()) {
            if (isset($_GET['id'])) {
                $id = $_GET['id'];
                $poll = Polls::model()->findByPk($id);
                $name = $poll['name'];
            }
            $model = new Questions;

            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if (isset($_POST['Questions'])) {
                $model->attributes = $_POST['Questions'];
                if ($model->save()) {
                    if (isset($_POST['Answers'])) {
                        foreach ($_POST['Answers'] as $value) {
                            $answers = new PossibleAnswer();
                            $answers->idQuestion = $model->idQuestion;
                            $answers->text = $value;
                            $answers->save();
                        }
                    }
                    $this->redirect(array('admin', 'id' => $model->idPoll));
                }

            }


            $this->render('create', array(
                'model' => $model,
                'name' => $name,
                'id' => $id,
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id,$idPoll)
    {
        if (User::isAdmin()) {
            $model = $this->loadModel($id);
            $answers = PossibleAnswer::model()->findAll('idQuestion=:idQuestion', array(':idQuestion'=>$id));
            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if (isset($_POST['Questions'])) {
                $model->attributes = $_POST['Questions'];
                if ($model->save()){
                    if (isset($_POST['Answers'])) {
                        PossibleAnswer::model()->deleteAllByAttributes(array('idQuestion'=>$id));
                        foreach ($_POST['Answers'] as $value) {
                            $answers = new PossibleAnswer();
                            $answers->idQuestion = $model->idQuestion;
                            $answers->text = $value;
                            $answers->save();
                        }
                    }
                    $this->redirect(array('admin', 'id' => $idPoll));
                }

            }
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
        $this->render('update', array(
            'model' => $model,
            'answers'=>$answers,
            'id' => $idPoll,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        if (User::isAdmin()) {
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        if (User::isAdmin()) {
            $dataProvider = new CActiveDataProvider('Questions');
            $this->render('index', array(
                'dataProvider' => $dataProvider,
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        if (User::isAdmin()) {
            if (isset($_GET['id'])) {
                $id = $_GET['id'];
                $poll = Polls::model()->findByPk($id);
                $name = $poll['name'];
            }
            $model = new Questions('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['Questions']))
                $model->attributes = $_GET['Questions'];
            $model->idPoll = $id;
            $this->render('admin', array(
                'model' => $model, 'name' => $name, 'id' => $id,
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Questions the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Questions::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Questions $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'questions-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
