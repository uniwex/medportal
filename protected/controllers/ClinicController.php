<?php

class ClinicController extends CController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/admin';
    public $breadCrumbs,
        $menu,
        $nameController = 'Клиники';
    /**
     * @return array action filters
     */


    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        if (User::isAdmin()) {
            $this->breadCrumbs = array('Просмотр клиники');
            $this->render('view', array(
                'model' => $this->loadModel($id),
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Clinic the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        if (User::isAdmin()) {
            $model = Clinic::model()->findByPk($id);
            if ($model === null)
                throw new CHttpException(404, 'The requested page does not exist.');
            return $model;
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        if (User::isAdmin()) {
            $this->breadCrumbs = array('Создание клиники');
            $model = new Clinic;

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

            if (isset($_POST['Clinic'])) {
                $model->attributes = $_POST['Clinic'];
                if (isset($model->image)) {
                    $model->image = CUploadedFile::getInstance($model, 'image');
                    $filename = md5(time() . rand(1000, 10000)) . '.' . $model->image->getExtensionName();
                    $model->pic = $filename;
                    $uploaded = $model->image->saveAs(Yii::getPathOfAlias('webroot') . '/upload/' . $filename);
                }
                if ($model->save())
                    $this->redirect(array('view', 'id' => $model->idUser));
            }

            $this->render('create', array(
                'model' => $model,
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        if (User::isAdmin()) {
            $this->breadCrumbs = array('Изменение клиники');
            $model = $this->loadModel($id);
            $doctors = Schedule::model()->findAll($id);
            if (isset($_POST['Clinic'])) {
                $model->attributes = $_POST['Clinic'];
                $model->image = CUploadedFile::getInstance($model, 'image');
                if ($model->image) {
                    $filename = md5(time() . rand(1000, 10000)) . '.' . $model->image->getExtensionName();
                    if ($model->pic) {
                        if (file_exists(Yii::getPathOfAlias('webroot') . '/upload/' . $model->pic)) {
                            unlink(Yii::getPathOfAlias('webroot') . '/upload/' . $model->pic);
                        }

                    }
                    $model->pic = $filename;
                    $model->image->saveAs(Yii::getPathOfAlias('webroot') . '/upload/' . $filename);
                    $model->update();

                }

                if ($model->save())
                    $this->redirect(array('view', 'id' => $model->idUser));
            }

            $this->render('update', array(
                'model' => $model,
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionAddDoctor()
    {
        echo '123';
    }





    public function actionDelete($id)
    {

        if (User::isAdmin()) {
            if($id != Yii::app()->user->id) {
                $user = User::model()->findAll('id = :id', array(':id' => $id));
                if (isset($user)) {
                    User::model()->deleteAll('id = :id', array(':id' => $id));
                }
                $partner = Partner::model()->findAll('idReferal = :id', array(':id' => $id));
                if (isset($partner)) {
                    Partner::model()->deleteAll('idReferal = :id', array(':id' => $id));
                }
                $schedule = Schedule::model()->findAll('idUser = :id', array(':id' => $id));
                $referral = Referral::model()->findAll('idReferal = :id', array(':id' => $id));
                if (isset($referral)) {
                    Referral::model()->deleteAll('idReferal = :id', array(':id' => $id));
                }
                for ($i = 0; $i < count($schedule); $i++) {
                    $idDoctor = $schedule[$i]['id'];
                    $executer = Event::model()->findAll('idExecuter = :id', array(':id' => $idDoctor));
                    if (isset($executer)) {
                        Event::model()->deleteAll('idExecuter = :id', array(':id' => $idDoctor));
                    }
                    $Owner = Event::model()->findAll('idOwner = :id', array(':id' => $idDoctor));
                    if (isset($Owner)) {
                        Event::model()->deleteAll('idOwner = :id', array(':id' => $idDoctor));
                    }
                }
                if (isset($schedule)) {
                    Schedule::model()->deleteAll('idUser = :id', array(':id' => $id));
                }
                if (isset($social)) {
                    Social::model()->deleteAll('idUser = :id', array(':id' => $id));
                }
                $station = Station::model()->findAll('idUser = :id', array(':id' => $id));
                if (isset($station)) {
                    Station::model()->deleteAll('idUser = :id', array(':id' => $id));
                }
                $user = User::model()->findAll('id = :id', array(':id' => $id));
                if (isset($user)) {
                    User::model()->deleteAll('id = :id', array(':id' => $id));
                }
                $this->loadModel($id)->delete();
            }
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {

        if (User::isAdmin()) {
            $dataProvider = new CActiveDataProvider('Clinic');
            $this->render('index', array(
                'dataProvider' => $dataProvider,
            ));
        } else  $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }


    public
    function actionUpdateDetail()
    {
        if (User::isAdmin()) {
            $massDoc = array();
            $massDoctorTime = array();
            $id = $_POST['specialization'];

            if (isset($id)) {
                for ($i = 0; $i < count($id); $i++) {
                    $readyMas = array();
                    $massDoc[] = $_POST['day' . $id[$i]];
                    $doctors = Schedule::model()->find('id = :id', array(':id' => $id[$i]));
                    $massCount = array(
                        '0' => $doctors->monday,
                        '1' => $doctors->tuesday,
                        '2' => $doctors->wednesday,
                        '3' => $doctors->thursday,
                        '4' => $doctors->friday,
                        '5' => $doctors->saturday,
                        '6' => $doctors->sunday,
                    );
                    $massCountResponse = array(
                        'monday',
                        'tuesday',
                        'wednesday',
                        'thursday',
                        'friday',
                        'saturday',
                        'sunday',
                    );
                    $maxCount = 0;
                    for ($j = 0; $j < count($massCount); $j++) {
                        $massAll = explode(',', $massCount[$j]);
                        $massDoctorTime[$j] = $massAll;
                        $countMas = count($massAll);
                        if (($countMas) > $maxCount)
                            $maxCount = $countMas;
                    }
                    $masSite = $massDoc[$i]; // массив данных с сайта по пользователю (все значениея полей со временем)
                    $countInBase = $maxCount; // максимальное колличество времен в поле базы
                    $countInSite = count($massDoc[$i]) / 7; // максимальное колличество времен с сайта
                    $countAll = count($massDoc[$i]); // колличество записей переданных с формы (неотсортированные по дням недели
                    if ($countInBase == $countInSite)    // если одинаковые то обновляем данные
                    {
                        for ($d = 0; $d < 7; $d++) {    // формирование отсортированного списка времен по дням недели как в базе
                            for ($t = $d; $t < $countAll; $t += 7) {
                                $readyMas[$d][] = $masSite[$t];
                            }
                        }
                        $strMas = array();
                        for ($p = 0; $p < count($readyMas); $p++) {
                            $str = '';
                            for ($y = 0; $y < count($readyMas[$p]); $y++) {
                                $str .= $readyMas[$p][$y] . ',';
                                $strMas[$p][] = $str;
                            }
                        }
                        $fullReadymass = array();
                        for ($o = 0; $o < count($strMas); $o++) {

                            $fullReadymass[$o] = preg_replace('/Выходной(,)/', '', $strMas[$o][$countInBase - 1]);
                            if ($fullReadymass[$o] != '') {
                                $fullReadymass[$o] = substr($fullReadymass[$o], 0, -1);
                            }
                            if ($fullReadymass[$o] == '') {
                                $fullReadymass[$o] = 'Выходной';
                            }
                        }
                        for ($e = 0; $e < count($massCount); $e++) {
                            //if (preg_match('/^([0-1][0-9]|[2][0-3]):([0-5][0-9])$/si', $subject, $regs)) {
                            //  $result = $regs[0];
//                    } else {
//                        $result = "";
//                    }
                            $doctors->$massCountResponse[$e] = $fullReadymass[$e];
                        }
                        if ($doctors->update()) {
                            echo 'good 1';
                        }

                    }
                    if ($countInBase < $countInSite)    // если значения разные (возможно добавлено поле)
                    {
                        for ($d = 0; $d < 7; $d++) {    // формирование отсортированного списка времен по дням недели как в базе
                            for ($t = $d; $t < $countAll; $t += 7) {
                                $readyMas[$d][] = $masSite[$t];
                            }
                        }
                        $strMas = array();
                        for ($p = 0; $p < count($readyMas); $p++) {
                            $str = '';
                            for ($y = 0; $y < count($readyMas[$p]); $y++) {
                                $str .= $readyMas[$p][$y] . ',';
                                $strMas[$p][] = $str;
                            }
                        }
                        $fullReadymass = array();
                        for ($o = 0; $o < count($strMas); $o++) {
                            $fullReadymass[$o] = preg_replace('/Выходной(,)/', '', $strMas[$o][$countInSite - 1]);
                            if ($fullReadymass[$o] != '') {
                                $fullReadymass[$o] = substr($fullReadymass[$o], 0, -1);
                            }
                            if ($fullReadymass[$o] == '') {
                                $fullReadymass[$o] = 'Выходной';
                            }
                        }
                        for ($v = 0; $v < count($fullReadymass); $v++) {
                            if (substr($fullReadymass[$v], -1) == ',') {
                                $fullReadymass[$v] = substr($fullReadymass[$v], 0, -1);
                            }
                        }
                        for ($e = 0; $e < count($massCount); $e++) {
                            $doctors->$massCountResponse[$e] = $fullReadymass[$e];
                        }

                        if ($doctors->update()) {
                            echo 'good 2';

                        }
                    }
                }
            }
            if (isset($_POST["id"])) {
                for ($i = 0; $i < count($_POST["id"]); $i++) {
                    $modelDetailUpdate = Schedule::model()->find('id = :id', array(':id' => $_POST['id'][$i]));
                    $modelDetailUpdate['name'] = $_POST["name"][$i];
                    $modelDetailUpdate['specialization'] = $_POST["specializationDoctor"][$i];
                    $modelDetailUpdate['price'] = $_POST["price"][$i];
                    $modelDetailUpdate['timeSize'] = $_POST["timeSize"][$i];
                    $modelDetailUpdate->update();
                }
            }

            $this->redirect(Yii::app()->request->urlReferrer);
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }


    public
    function actionDeleteDoctor()
    {
        if (User::isAdmin()) {
            $idDelete = explode('_', $_GET['idDelete']);
            $readyMas = array();
            $strMas = array();
            $fullReadymass = array();
            $id = $idDelete[0]; //id врача

            $line = $idDelete[1]; // линия

            $massDoc = $_GET['day' . $id];
            $doctors = Schedule::model()->find('id = :id', array(':id' => $id));

            $massCount = array(
                '0' => $doctors->monday,
                '1' => $doctors->tuesday,
                '2' => $doctors->wednesday,
                '3' => $doctors->thursday,
                '4' => $doctors->friday,
                '5' => $doctors->saturday,
                '6' => $doctors->sunday,
            );
            $massCountResponse = array(
                'monday',
                'tuesday',
                'wednesday',
                'thursday',
                'friday',
                'saturday',
                'sunday',
            );
            $maxCount = 0;
            for ($j = 0; $j < count($massCount); $j++) {
                $massAll = explode(',', $massCount[$j]);
                $massDoctorTime[$j] = $massAll;
                $countMas = count($massAll);
                if (($countMas) > $maxCount)
                    $maxCount = $countMas;
            }
            $masSite = $massDoc; // массив данных с сайта по пользователю (все значениея полей со временем)
            $countInBase = $maxCount; // максимальное колличество времен в поле базы
            $countInSite = count($massDoc) / 7; // максимальное колличество времен с сайта
            $countAll = count($massDoc); // колличество записей переданных с формы (неотсортированные по дням недели
            if ($countInBase == $countInSite)    // если одинаковые то обновляем данные
            {
                for ($d = 0; $d < 7; $d++) {    // формирование отсортированного списка времен по дням недели как в базе
                    for ($t = $d; $t < $countAll; $t += 7) {
                        $readyMas[$d][] = $masSite[$t];
                    }
                }
                for ($h = 0; $h < count($readyMas); $h++) {
                    $readyMas[$h][$line] = '';
                }
                for ($p = 0; $p < count($readyMas); $p++) {
                    $str = '';
                    for ($y = 0; $y < count($readyMas[$p]); $y++) {
                        $str .= $readyMas[$p][$y] . ',';
                        $strMas[$p][] = $str;
                    }
                }
                for ($o = 0; $o < count($strMas); $o++) {
                    if ($strMas[$o][$countInBase - 1][0] == ',') {  // если удален первый элемент
                        echo "если удален первый элемент";
                        $k = substr($strMas[$o][$countInBase - 1], 0, -1);
                        $fullReadymass[$o] = substr($k, 1);
                    }
                    if (substr($strMas[$o][$countInBase - 1], -2) == ',,') { // если удален ПОСЛЕДНИЙ
                        echo 'если удален ПОСЛЕДНИЙ';
                        $fullReadymass[$o] = substr($strMas[$o][$countInBase - 1], 0, -2);
                    }
                    preg_match('/,,(.?)/', $strMas[$o][$countInBase - 1], $result);
                    if ($result[1] != '') {    // если удален в середине
                        echo 'если удален в середине';

                        $k = str_replace(",,", ",", $strMas[$o][$countInBase - 1]);
                        $k = substr($k, 0, -1);
                        $fullReadymass[$o] = $k;
                    }
                }
                for ($e = 0; $e < count($massCount); $e++) {
                    if (isset($doctors->$massCountResponse[$e])) {
                        $doctors->$massCountResponse[$e] = $fullReadymass[$e];
                    }
                }
                if ($doctors->update()) {
                    echo 'good';
                }
            }
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }


    public
    function actionDoctorUpdate()
    {
        if (User::isAdmin()) {
            $id = substr($_GET['id'], 0, strlen($_GET['id']) - 1);
            $id = explode('_', $id);
            $doctors = Schedule::model()->findAllByPk($id);
            $massAll = array();
            for ($k = 0; $k < count($doctors); $k++) {
                $doctor = array(
                    'doctor' => array(
                        'id' => $doctors[$k]->id,
                        'name' => $doctors[$k]->name,
                        'specialization' => $doctors[$k]->specialization,
                        'price' => $doctors[$k]->price,
                        'timeSize' => $doctors[$k]->timeSize,
                    )
                );
                $massCount = array(
                    '0' => $doctors[$k]->monday,
                    '1' => $doctors[$k]->tuesday,
                    '2' => $doctors[$k]->wednesday,
                    '3' => $doctors[$k]->thursday,
                    '4' => $doctors[$k]->friday,
                    '5' => $doctors[$k]->saturday,
                    '6' => $doctors[$k]->sunday,

                );
                for ($j = 0; $j < count($massCount); $j++) {
                    $massAll[$doctors[$k]->id][] = explode(',', $massCount[$j]);

                }
                foreach ($massAll as $key => $value) {
                    $max = DayHelper::maxField($value);

                    $massAll[$key] = DayHelper::enterField($value, $max);
                    $massAll[$doctors[$k]->id][] = array(
                        'doctor' => array(
                            'id' => $doctors[$k]->id,
                            'name' => $doctors[$k]->name,
                            'specialization' => $doctors[$k]->specialization,
                            'price' => $doctors[$k]->price,
                            'timeSize' => $doctors[$k]->timeSize,
                        )
                    );
                }
            }
            echo json_encode($massAll);
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        if (User::isAdmin()) {
            $this->breadCrumbs = array('Просмотр клиник');
            $model = new Clinic('search');
            $model->unsetAttributes();
            if (isset($_GET['Clinic']))
                $model->attributes = $_GET['Clinic'];
            $this->render('admin', array(
                'model' => $model,
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }


    /**
     * Performs the AJAX validation.
     * @param Clinic $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (User::isAdmin()) {
            if (isset($_POST['ajax']) && $_POST['ajax'] === 'clinic-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }
}
