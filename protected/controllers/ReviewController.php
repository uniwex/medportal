<?php

class ReviewController extends CController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */


    /**
     * @return array action filters
     */
    public $layout = '//layouts/admin',
        $breadCrumbs,
        $nameController = 'Отзывы';


    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        if (User::isAdmin()) {
            $this->breadCrumbs = array('Просмотр отзыва');
            $this->render('view', array(
                'model' => $this->loadModel($id),
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        if (User::isAdmin()) {
            $this->breadCrumbs = array('Создание отзыва');
            $model = new Review;
            if (isset($_POST['Review'])) {
                $model->attributes = $_POST['Review'];
                if ($model->save())
                    $this->redirect(array('view', 'id' => $model->id));
            }
            $this->render('create', array(
                'model' => $model,
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        if (User::isAdmin()) {
            $this->breadCrumbs = array('Изменение отзыва');
            $model = $this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

            if (isset($_POST['Review'])) {
                $model->attributes = $_POST['Review'];
                if ($model->save())
                    $this->redirect(array('view', 'id' => $model->id));
            }

            $this->render('update', array(
                'model' => $model,
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        if (User::isAdmin()) {
            $this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }


    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        if (User::isAdmin()) {
            $this->breadCrumbs = array('тзывы');
            $dataProvider = new CActiveDataProvider('Review');
            $this->render('index', array(
                'dataProvider' => $dataProvider,
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        if (User::isAdmin()) {
            $this->breadCrumbs = array('Отзывы');
            $model = new Review('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['Review']))
                $model->attributes = $_GET['Review'];

            $this->render('admin', array(
                'model' => $model,
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Review the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        if (User::isAdmin()) {
            $model = Review::model()->findByPk($id);
            if ($model === null)
                throw new CHttpException(404, 'The requested page does not exist.');
            return $model;
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }


    public function actionModered()
    {
        if (User::isAdmin()) {
            $id = Yii::app()->getRequest()->getParam('id');
            $status = Yii::app()->getRequest()->getParam('status');
            $model = Review::model()->find('id = :id', array(':id' => $id));
            if (isset($model) && $status == '0') {
                $model->moderate = '1';
                $model->update();
                $this->redirect(Yii::app()->getRequest()->getBaseUrl(true) . '/review/admin');
            }
            if (isset($model) && $status == '1') {
                $model->moderate = '0';
                $model->update();
                $this->redirect(Yii::app()->getRequest()->getBaseUrl(true) . '/review/admin');
            }
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');

    }

    public function actionDeleteItem()
    {
        if (User::isAdmin()) {
            $id = Yii::app()->getRequest()->getParam('id');
            $pics = explode('_', $id);
            $idPic = $pics[0];
            $numPic = $pics[1];
            $image = Review::model()->find('id = :id', array(':id' => $idPic));
            $AllPics = explode('_', $image->pics);
            $str = $image->pics;

            $str = str_replace($AllPics[$numPic], "", $str);
            if (substr($str, -1) == '_') // если удален ПОСЛЕДНИЙ
            {
                $str = substr($str, 0, -1);
                $image->pics = $str;
            }
            preg_match('/__(.?)/', $str, $result);
            if (isset($result[1])) {    // если удален в середине
                if ($result[1] != '') {
                    $str = str_replace("__", "_", $str);
                }
            }

            if ($str!='') {
                if ($str[0] == '_') {   // если удален первый элемент
                    $str = substr($str, 1);
                }
            }
            $image->pics = $str;
            if ($str=='')
            {
                $image->pics='1';
            }
            if ($image->update()) {
                echo 'good';
            }
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/login');
    }

    /**
     * Performs the AJAX validation.
     * @param Review $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'review-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
