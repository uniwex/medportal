<?php

/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 27.10.2015
 * Time: 16:11
 */
class MedbookController extends CController
{
    public $breadCrumbs;

    public function actionIndex()
    {
        $this->breadCrumbs = array('Медицинский справочник');
        $medbooks = Medbook::model()->findAll();
        $this->layout = 'main';
        $this->render('index', array('medbooks' => $medbooks));
    }

    public function actionView(){
        $idmedbook = Yii::app()->request->getQuery('id');
        if($idmedbook) {
            $this->breadCrumbs = array('Медицинский справочник' => Yii::app()->request->baseUrl . '/medbook', 'Просмотр статьи');
            $medbook = Medbook::model()->findByPk($idmedbook);
            $this->layout = 'main';
            $this->render('view', array('medbook' => $medbook));
        } else {
            $this->breadCrumbs = array('Медицинский справочник');
            $medbooks = Medbook::model()->findAll();
            $this->layout = 'main';
            $this->render('index', array('medbooks' => $medbooks));
        }
    }

    public function actionEdit(){
        $idmedbook = Yii::app()->request->getQuery('id');
        if($idmedbook && User::isAdmin()) {
            $this->breadCrumbs = array('Медицинский справочник' => Yii::app()->request->baseUrl . '/medbook', 'Редактирование статьи');
            $medbookModel = Medbook::model()->findByPk($idmedbook);
            if(User::model()->findByPk(Yii::app()->user->id)->role == 'admin') {
                $categoryModel = new Category();
                $this->performAjaxValidation($medbookModel, 'edit-medbook');
                $this->performAjaxValidation($categoryModel, 'edit-medbook');
                $this->layout = 'main';
                $this->render('edit', array('medbookModel' => $medbookModel, 'categoryModel' => $categoryModel));
            } else {
                $this->redirect(Yii::app()->request->getBaseUrl(true).'/medbook');
            }
        } else {
            $this->breadCrumbs = array('Медицинский справочник');
            $medbook = Medbook::model()->findAll();
            $this->layout = 'main';
            $this->render('index', array('medbook' => $medbook));
        }
    }

    public function actionUpdate(){
        if (!Yii::app()->user->isGuest && User::isAdmin()) {
            $medbookModel = new Medbook();
            $categoryModel = new Category();
            $this->performAjaxValidation($medbookModel, 'edit-medbook');
            if  (($_POST['Medbook'])&&($_POST['Category'])) {
                $medbookModel->attributes = $_POST['Medbook'];
                $categoryModel->attributes = $_POST['Category'];
                $tmpImage = CUploadedFile::getInstance($medbookModel, 'image');
                $filename = '';
                if($tmpImage) {
                    $filename = md5(time() . rand()) . '.' . $tmpImage->getExtensionName();
                    $medbookModel->image = $filename;
                    $path = Yii::getPathOfAlias('webroot') . '/upload/medbook/' . $filename;
                }
                $medbookModel->time = date('Y-m-d H:i:s');
                $idsCategory = implode(',',$categoryModel->name);
                $medbookModel->idCategory = $idsCategory;
                if ($medbookModel->validate()) {
                    if($tmpImage) {
                        Medbook::model()->updateByPk($_POST['Medbook']['id'], array('name' => $medbookModel->name, 'text' => $medbookModel->text, 'image' => $filename, 'idCategory' => $medbookModel->idCategory));
                        $tmpImage->saveAs($path);
                    }
                    else {
                        Medbook::model()->updateByPk($_POST['Medbook']['id'], array('name'=>$medbookModel->name,'text'=>$medbookModel->text, 'idCategory' => $medbookModel->idCategory));
                    }
                    $this->redirect(Yii::app()->request->getBaseUrl(true).'/medbook');
                } else
                    print_r($medbookModel->getErrors());
            } else
                echo 'error2';
        }
        else{
            $this->redirect(Yii::app()->homeUrl);
        }

    }

    public function actionAdd(){

        if ((!Yii::app()->user->isGuest)&&(User::model()->findByPk(Yii::app()->user->id)->role == 'admin')) {
            $medbookModel = new Medbook();
            $categoryModel = new Category();
            $this->performAjaxValidation($medbookModel, 'add-medbook');
            $this->performAjaxValidation($categoryModel, 'add-medbook');
            $this->breadCrumbs = array('Медицинский справочник' => Yii::app()->request->baseUrl . '/medbook', 'Добавление новой статьи');
            $this->layout = 'main';
            $this->render('add', array('medbookModel' => $medbookModel, 'categoryModel' => $categoryModel));
        }
        else{
            $this->redirect(Yii::app()->homeUrl);
        }

    }

    public function actionSaveNew(){

        if ((!Yii::app()->user->isGuest)&&(User::model()->findByPk(Yii::app()->user->id)->role == 'admin')) {

            $medbookModel = new Medbook();
            $categoryModel = new Category();
            $this->performAjaxValidation($medbookModel, 'add-medbook');
            if (($_POST['Medbook']) && ($_POST['Category'])) {
                $medbookModel->attributes = $_POST['Medbook'];
                $categoryModel->attributes = $_POST['Category'];
                $tmpImage = CUploadedFile::getInstance($medbookModel, 'image');
                if ($tmpImage) {
                    $filename = md5(time() . rand()) . '.' . $tmpImage->getExtensionName();
                    $medbookModel->image = $filename;
                    $path = Yii::getPathOfAlias('webroot') . '/upload/medbook/' . $filename;
                }
                $medbookModel->time = date('Y-m-d H:i:s');
                $idsCategory = implode(',',$categoryModel->name);
                $medbookModel->idCategory = $idsCategory;
                if ($medbookModel->validate()) {
                    $medbookModel->save();
                    if ($tmpImage)
                        $tmpImage->saveAs($path);
                    $this->redirect(Yii::app()->request->getBaseUrl(true) . '/medbook');
                } else
                    print_r($medbookModel->getErrors());
            } else
                echo 'error code: 2';
        }
        else{
            $this->redirect(Yii::app()->homeUrl);
        }

    }

    public function actionRemove(){

        if ((!Yii::app()->user->isGuest)&&(User::model()->findByPk(Yii::app()->user->id)->role == 'admin')) {
            $idmedbook = Yii::app()->request->getQuery('id');
            Medbook::model()->deleteByPk($idmedbook);
        }
    }

    public function actionGetmedbook(){
        $idCategory = Yii::app()->request->getPOST('idCategory');
        $medbookDate = Yii::app()->request->getPOST('medbookDate');

        //$idCategory = -1;
        //$medbookDate = "2015-10-14";

        $criteria = new CDbCriteria;
        $condition = '';
        if($idCategory != -1)
            $condition .= "idCategory = ". $idCategory . " AND";
        if($medbookDate != '')
            $condition .= " time LIKE '". $medbookDate . "%'";

        $condition = preg_replace('/AND$/','',$condition);

        $criteria->condition=$condition;

        $medbook = Medbook::model()->findAll($criteria);
        $medbookForJson = array();

        foreach ($medbook as $key) {
            $name = 'Администратор';
            $medbook = array(
                'id' => $key->id,
                'author' => $name,
                'name' => $key->name,
                'text' => $key->text,
                'time' => $key->time,
                'image' => $key->image,
            );
            array_push($medbookForJson, $medbook);
        }
        echo json_encode($medbookForJson);
    }

    protected function performAjaxValidation($model, $form){
        if (isset($_POST['ajax']) && $_POST['ajax'] === $form) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}