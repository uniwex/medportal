<?php

class PollsController extends CController
{
    public $breadCrumbsHome;
    public $breadCrumbs;

    public function actionIndex() {
        $this->layout = 'main';
        $this->breadCrumbs = array('Опросы');
        $today = date("Y-m-d");
        $polls = Polls::model()->findAll();

        if(!$polls)
            throw new CHttpException(404, 'Такой страницы не существует.');

        $dataProviderActive = new CActiveDataProvider('Polls',
            array(
                'criteria'=>array(
                    'condition'=>'datePoll >= :datePoll',
                    'params'=>array(':datePoll'=>$today)
                ),
                'pagination'=>array(
                    'pageSize'=>5,
                ),
            )
        );
        $dataProviderUnactive = new CActiveDataProvider('Polls',
            array(
                'criteria'=>array(
                    'condition'=>'datePoll < :datePoll',
                    'params'=>array(':datePoll'=>$today)
                ),
                'pagination'=>array(
                    'pageSize'=>5,
                ),
            )
        );
        $this->render('index', array('model'=>$polls, 'dataProviderActive'=>$dataProviderActive, 'dataProviderUnactive'=>$dataProviderUnactive));


    }
    public function actionQuestions() {
        if (Yii::app()->user->isGuest) {
            $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
        }
        $this->layout = 'main';
        $this->breadCrumbs = array('Опросы' => Yii::app()->request->baseUrl . '/polls', 'Вопросы');
        $id = $_GET['idPoll'];
        $idUser = Yii::app()->user->id;
        $answersData = AnswersData::model()->find('idPoll=:idPoll AND idUser=:idUser', array(
            ':idPoll'=>$id,
            ':idUser'=>$idUser,
        ));
        $questions = Questions::model()->with('answers')->findAll('idPoll=:idPoll', array(':idPoll'=>$id));
        $amount = [];
        $answersArray = [];
        if ($answersData !== NULL) {
            //Считаем суммы
            foreach ($questions as $key=>$item) {
                $amount[$key] = 0;
                foreach ($item->answers as $answer) {
                    $answersArray[$answer->idAnswer] = AnswersData::model()->count('idPoll=:idPoll AND idAnswer=:idAnswer',
                    array(
                        ':idPoll'=>$id,
                        ':idAnswer'=>$answer->idAnswer,
                    ));
                    $amount[$key] += $answersArray[$answer->idAnswer];
                }
            }
            //Считаем проценты ответов
            foreach ($questions as $key=>$item) {
                foreach ($item->answers as $answer) {
                    if ($amount[$key] <= 0)
                        $answersArray[$answer->idAnswer] = 0;
                    else
                        $answersArray[$answer->idAnswer] = $answersArray[$answer->idAnswer]*100/$amount[$key];
                }
            }
        }
        $today = date("Y-m-d");
        $poll = Polls::model()->findByPk($id);
        if(!$questions)
            throw new CHttpException(404, 'Нет данных по такому запросу.');
        if ($poll['datePoll'] < $today) {
            $this->redirect('/polls');
        } else {
            $this->render('questions',array(
                'poll' => $poll,
                'questions'=> $questions,
                'answers' => $answersArray,
            ));
        }
    }
    public function actionFinish() {
        if (isset ($_POST)){
            $id = Yii::app()->user->id;
            $idPoll = ($_POST['idPoll']);
            $poll = Polls::model()->findByPk($idPoll);
            $questions = Questions::model()->count('idPoll=:idPoll', array(':idPoll'=>$idPoll));

            if ($questions != count($_POST['Answers'])){
                $this->redirect('/polls');
            }
            if (!empty($id)) {
                $user = User::model()->findByPk($id);
                $user->points += $poll['prize'];
                $user->save();
                foreach ($_POST['Answers'] as $key) {
                    $answers = new AnswersData();
                    $answers->idPoll = $idPoll;
                    $answers->idAnswer = $key;
                    $answers->idUser = $id;
                    $answers->save();
                }
            }
        }
        $this->redirect('/polls?message=1');
    }
    public function actionWinners() {
        $this->layout = 'main';
        $this->breadCrumbs = array('Опросы' => Yii::app()->request->baseUrl . '/polls', 'Победители розыгрышей');

        $criteria = new CDbCriteria();
        $count = Quiz::model()->count($criteria);
        $pages = new CPagination($count);
        $pages->pageSize=1;
        $pages->applyLimit($criteria);

        $sort=new CSort('Quiz');
        $sort->defaultOrder='date DESC';
        $sort->applyOrder($criteria);

        $quiz = Quiz::model()->findAll($criteria);
        foreach ($quiz as $item) {
            $dataProvider[] = new CActiveDataProvider('Winners',
                array(
                    'criteria' => array(
                        'with' => array('users', 'quiz'=>array(
                            'condition'=>'idQuiz='.$item->id),
                            'clinic', 'doctor', 'nurse'),
                    ),
                )
            );
        }
        $this->render('winners', array('model'=>$quiz, 'dataProvider'=>$dataProvider, 'pages' => $pages));
    }
}