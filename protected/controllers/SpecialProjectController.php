<?php

class SpecialProjectController extends CController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/admin';
    public $breadCrumbs,
        $menu,
        $nameController = 'Спец. проект';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }


    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        if (User::isAdmin()) {
            $this->render('view', array(
                'model' => $this->loadModel($id),
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        if (User::isAdmin()) {
            $model = new SpecialProject;

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

            if (isset($_POST['SpecialProject'])) {
                $model->attributes = $_POST['SpecialProject'];
                if (isset($model->picture)){
                    $model->picture = CUploadedFile::getInstance($model, 'picture');
                    $filename = md5(time() . rand(1000, 10000)) . '.' . $model->picture->getExtensionName();
                    $model->pic = $filename;
                    $uploaded = $model->picture->saveAs(Yii::getPathOfAlias('webroot') . '/upload/infohub/' . $filename);
                }
                if ($model->save())
                    $this->redirect(array('view', 'id' => $model->id));
            }

            $this->render('create', array(
                'model' => $model,
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        if (User::isAdmin()) {
            $model = $this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

            if (isset($_POST['SpecialProject'])) {
                $model->attributes = $_POST['SpecialProject'];
                $model->picture = CUploadedFile::getInstance($model, 'image');
                if ($model->picture) {
                    $filename = md5(time() . rand(1000, 10000)) . '.' . $model->picture->getExtensionName();
                    if ($model->pic) {
                        if (file_exists(Yii::getPathOfAlias('webroot') . '/upload/infohub' . $model->pic)) {
                            unlink(Yii::getPathOfAlias('webroot') . '/upload/infohub' . $model->pic);
                        }

                    }
                    $model->pic = $filename;
                    $model->picture->saveAs(Yii::getPathOfAlias('webroot') . '/upload/infohub' . $filename);
                    $model->update();

                }
                if ($model->save())
                    $this->redirect(array('view', 'id' => $model->id));
            }

            $this->render('update', array(
                'model' => $model,
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        if (User::isAdmin()) {
            $this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        if (User::isAdmin()) {
            $dataProvider = new CActiveDataProvider('SpecialProject');
            $this->render('index', array(
                'dataProvider' => $dataProvider,
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        if (User::isAdmin()) {
            $model = new SpecialProject('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['SpecialProject']))
                $model->attributes = $_GET['SpecialProject'];

            $this->render('admin', array(
                'model' => $model,
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return SpecialProject the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = SpecialProject::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param SpecialProject $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'special-project-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
