<?php

class WinnersController extends CController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/admin';
    public $breadCrumbs,
        $menu,
        $nameController = 'Победители розыгрышей';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }


    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        if (User::isAdmin()) {
            $this->render('view', array(
                'model' => $this->loadModel($id),
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        if (User::isAdmin()) {
            $model = new Winners;

            $result = User::model()->findAll();
            $userArray = [];
            foreach ($result as $item) {
                $userArray [$item->id] = $item->name;
            }

            $result = Quiz::model()->findAll();
            $quizArray = [];
            foreach ($result as $item) {
                $quizArray [$item->id] = $item->date;
            }


            if (isset($_POST['Winners'])) {
                $model->attributes = $_POST['Winners'];
                if ($model->save())
                    $this->redirect(array('view', 'id' => $model->id));
            }

            $this->render('create', array(
                'model' => $model,
                'usersA' => $userArray,
                'quizA' => $quizArray,
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        if (User::isAdmin()) {
            $model = $this->loadModel($id);

            $result = User::model()->findAll();
            $userArray = [];
            foreach ($result as $item) {
                $userArray [$item->id] = $item->name;
            }

            $result = Quiz::model()->findAll();
            $quizArray = [];
            foreach ($result as $item) {
                $quizArray [$item->id] = $item->date;
            }

            if (isset($_POST['Winners'])) {
                $model->attributes = $_POST['Winners'];
                if ($model->save())
                    $this->redirect(array('view', 'id' => $model->id));
            }

            $this->render('update', array(
                'model' => $model,
                'usersA' => $userArray,
                'quizA' => $quizArray,
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        if (User::isAdmin()) {
            $this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        if (User::isAdmin()) {
            $dataProvider = new CActiveDataProvider('Winners');
            $this->render('index', array(
                'dataProvider' => $dataProvider,
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        if (User::isAdmin()) {
            $model = new Winners('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['Winners']))
                $model->attributes = $_GET['Winners'];

            $this->render('admin', array(
                'model' => $model,
            ));
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Winners the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        if (User::isAdmin()) {
            $model = Winners::model()->findByPk($id);
            if ($model === null)
                throw new CHttpException(404, 'The requested page does not exist.');
            return $model;
        } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login');
    }

    /**
     * Performs the AJAX validation.
     * @param Winners $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'winners-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
