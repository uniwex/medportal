<?php

class DigestController extends CController
{
    public $breadCrumbs;

    public function actionIndex()
    {

        $this->breadCrumbs = array('Дайджест соцсетей');
        $model=new Digest('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Digest']))
            $model->attributes=$_GET['Digest'];

        //send model object for search
        $this->render('index',array(
                'dataProvider'=>$model->search(),
                'model'=>$model)
        );

    }

    public function actionView(){

        $idDigest = Yii::app()->request->getQuery('id');
        if($idDigest) {
            $this->breadCrumbs = array('Дайджест соцсетей' => Yii::app()->request->baseUrl . '/digest', 'Просмотр статьи');
            $digest = Digest::model()->findByPk($idDigest);
            $this->layout = 'main';
            $this->render('view', array('digest' => $digest));
        } else {
            $this->breadCrumbs = array('Дайджест соцсетей');
            $digests = Digest::model()->findAll();
            $this->layout = 'main';
            $this->render('index', array('digests' => $digests));
        }

    }

    public function actionEdit(){

        $idDigest = Yii::app()->request->getQuery('id');
        if($idDigest) {
            $this->breadCrumbs = array('Дайджест' => Yii::app()->request->baseUrl . '/digest', 'Редактирование статьи');
            $digestModel = Digest::model()->findByPk($idDigest);
            if(User::model()->findByPk(Yii::app()->user->id)->role == 'admin') {
                $categoryModel = new Category();
                $this->performAjaxValidation($digestModel, 'edit-digest');
                $this->performAjaxValidation($categoryModel, 'edit-digest');
                $this->layout = 'main';
                $this->render('edit', array('digestModel' => $digestModel, 'categoryModel' => $categoryModel));
            } else {
                $this->redirect(Yii::app()->request->getBaseUrl(true).'/digest');
            }
        } else {
            $this->breadCrumbs = array('Дайджест');
            $digest = Digest::model()->findAll();
            $this->layout = 'main';
            $this->render('index', array('digest' => $digest));
        }

    }

    public function actionUpdate(){

        if (!Yii::app()->user->isGuest) {
            $digestModel = new Digest();
            $categoryModel = new Category();
            $this->performAjaxValidation($digestModel, 'edit-digest');
            if  (($_POST['Digest'])&&($_POST['Category'])) {
                $digestModel->attributes = $_POST['Digest'];
                $categoryModel->attributes = $_POST['Category'];
                $tmpImage = CUploadedFile::getInstance($digestModel, 'image');
                $filename = '';
                if($tmpImage) {
                    $filename = md5(time() . rand()) . '.' . $tmpImage->getExtensionName();
                    $digestModel->image = $filename;
                    $path = Yii::getPathOfAlias('webroot') . '/upload/digest/' . $filename;
                }
                $digestModel->time = date('Y-m-d H:i:s');
                $idsCategory = implode(',',$categoryModel->name);
                $digestModel->idCategory = $idsCategory;
                if ($digestModel->validate()) {
                    if($tmpImage) {
                        Digest::model()->updateByPk($_POST['Digest']['id'], array('name' => $digestModel->name, 'text' => $digestModel->text, 'image' => $filename, 'idCategory' => $digestModel->idCategory));
                        $tmpImage->saveAs($path);
                    }
                    else {
                        Digest::model()->updateByPk($_POST['Digest']['id'], array('name'=>$digestModel->name,'text'=>$digestModel->text, 'idCategory' => $digestModel->idCategory));
                    }
                    $this->redirect(Yii::app()->request->getBaseUrl(true).'/digest');
                } else
                    print_r($digestModel->getErrors());
            } else
                echo 'error2';
        }
        else{
            $this->redirect(Yii::app()->homeUrl);
        }

    }

    public function actionAdd(){

        if ((!Yii::app()->user->isGuest)&&(User::model()->findByPk(Yii::app()->user->id)->role == 'admin')) {
            $digestModel = new Digest();
            $categoryModel = new Category();
            $this->performAjaxValidation($digestModel, 'add-digest');
            $this->performAjaxValidation($categoryModel, 'add-digest');
            $this->breadCrumbs = array('Дайджест' => Yii::app()->request->baseUrl . '/digest', 'Добавление новой статьи');
            $this->layout = 'main';
            $this->render('add', array('digestModel' => $digestModel, 'categoryModel' => $categoryModel));
        }
        else{
            $this->redirect(Yii::app()->homeUrl);
        }

    }

    public function actionSaveNew(){

        if ((!Yii::app()->user->isGuest)&&(User::model()->findByPk(Yii::app()->user->id)->role == 'admin')) {

            $digestModel = new Digest();
            $categoryModel = new Category();
            $this->performAjaxValidation($digestModel, 'add-digest');
            if (($_POST['Digest']) && ($_POST['Category'])) {
                $digestModel->attributes = $_POST['Digest'];
                $categoryModel->attributes = $_POST['Category'];
                $tmpImage = CUploadedFile::getInstance($digestModel, 'image');
                if ($tmpImage) {
                    $filename = md5(time() . rand()) . '.' . $tmpImage->getExtensionName();
                    $digestModel->image = $filename;
                    $path = Yii::getPathOfAlias('webroot') . '/upload/digest/' . $filename;
                }
                $digestModel->time = date('Y-m-d H:i:s');
                $idsCategory = implode(',',$categoryModel->name);
                $digestModel->idCategory = $idsCategory;
                if ($digestModel->validate()) {
                    $digestModel->save();
                    if ($tmpImage)
                        $tmpImage->saveAs($path);
                    $this->redirect(Yii::app()->request->getBaseUrl(true) . '/digest');
                } else
                    print_r($digestModel->getErrors());
            } else
                echo 'error code: 2';
        }
        else{
            $this->redirect(Yii::app()->homeUrl);
        }

    }

    //function remove the article by id
    //input parameters:
    ////id
    public function actionRemove(){

        if (!Yii::app()->user->isGuest) {
            if (User::model()->findByPk(Yii::app()->user->id)->role == 'admin') {
                $idDigest = Yii::app()->request->getPOST('id');
                Digest::model()->deleteByPk($idDigest);
            }
        }
    }

    public function actionGetDigest(){

        //if (!Yii::app()->user->isGuest) {

        $idCategory = Yii::app()->request->getPOST('idCategory');
        $digestDate = Yii::app()->request->getPOST('digestDate');

        //$idCategory = -1;
        //$digestDate = "2015-10-14";

        $criteria = new CDbCriteria;
        $condition = '';
        if($idCategory != -1)
            $condition .= "idCategory = ". $idCategory . " AND";
        if($digestDate != '')
            $condition .= " time LIKE '". $digestDate . "%'";

        $condition = preg_replace('/AND$/','',$condition);

        $criteria->condition=$condition;

        $digest = Digest::model()->findAll($criteria);
        $digestForJson = array();

        foreach ($digest as $key) {
            $name = 'Администратор';
            $digest = array(
                'id' => $key->id,
                'author' => $name,
                'name' => $key->name,
                'text' => $key->text,
                'time' => $key->time,
                'image' => $key->image,
            );
            array_push($digestForJson, $digest);
        }
        echo json_encode($digestForJson);
        //}

    }

    protected function performAjaxValidation($model, $form){
        if (isset($_POST['ajax']) && $_POST['ajax'] === $form) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionSetrating() {
        if (Yii::app()->request->isAjaxRequest){
            $id_elem = Yii::app()->request->getParam('id_elem');
            $type_elem = Yii::app()->request->getParam('type_elem');
            $value = Yii::app()->request->getParam('value');
            RatingHelper::setRating($id_elem,$type_elem,$value);
            echo $this->renderPartial('_rating',['id'=>$id_elem,'type_elem'=>$type_elem]);
        }
    }

}