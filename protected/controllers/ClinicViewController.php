<?php

class ClinicViewController extends CController
{
    public $breadCrumbsHome;
    public $breadCrumbs;

    public function actionIndex($id)
    {
        $this->layout = 'main';
        $this->breadCrumbs = array('Клиники');
        $clinic = Clinic::model()->with('hours', 'info', 'media', 'doctors')->findByPk($id);
        $doctorsList = $clinic->doctors;
        $doctorsArray = [];
        foreach ($clinic->doctors as $value) {
            $doctorsArray += [$value->idUser, $value->name];
        }
        $specialization = Specialization::model()->findAll();
        $specializationArray = [];
        foreach ($specialization as $value) {
            array_push($specializationArray, $value->name);

        }
        $this->render('index', array(
            'model' => $clinic,
            'specialization' => $specializationArray,
            'doctors' => $doctorsArray,
            'doctorsList' => $doctorsList,
            'idClinic' => $id,
        ));
    }

    public function actionSearch()
    {
        if (isset($_GET)) {
            $idUser = $_GET['idUser'];
            $spec = $_GET['specialization'];
            $typeHuman = $_GET['typeHuman'];
            $idClinic = $_GET['idClinic'];

            $specialization = Specialization::model()->findAll();
            $specializationArray = [];
            foreach ($specialization as $value) {
                array_push($specializationArray, $value->name);
            }


            if ($idUser != 'null') {
                $doctorsList = Doctor::model()->findByPk($idUser);
                $this->renderPartial('_doctors', array('doctorsList' => $doctorsList, 'specialization' => $specializationArray), false, false);
                $spec = 'null';
                $typeHuman = 'null';
            }
            if (($spec != 'null') or ($typeHuman != 'null')) {
                if (($spec != 'null') && ($typeHuman != 'null')) {
                    $doctorsList = Doctor::model()->findAll('idSpecialization=:idSpecialization AND typeHuman=:typeHuman AND idClinic=:idClinic', array(
                        ':idSpecialization' => $spec,
                        ':typeHuman' => $typeHuman,
                        ':idClinic' => $idClinic,
                    ));
                }
                if (($spec == 'null') && ($typeHuman != 'null')) {
                    $doctorsList = Doctor::model()->findAll('typeHuman=:typeHuman AND idClinic=:idClinic', array(
                        ':typeHuman' => $typeHuman,
                        ':idClinic' => $idClinic,
                    ));
                }
                if (($spec != 'null') && ($typeHuman == 'null')) {
                    $doctorsList = Doctor::model()->findAll('idSpecialization=:idSpecialization AND idClinic=:idClinic', array(
                        ':idSpecialization' => $spec,
                        ':idClinic' => $idClinic,
                    ));
                }
                foreach ($doctorsList as $value) {
                    $this->renderPartial('_doctors', array('doctorsList' => $value, 'specialization' => $specializationArray));
                }
            }
            if (($spec == 'null') && ($typeHuman == 'null') && ($idUser == 'null')) {
                $doctorsList = Doctor::model()->findAll('idClinic=:idClinic', array(
                    ':idClinic' => $idClinic,
                ));
                foreach ($doctorsList as $value) {
                    $this->renderPartial('_doctors', array('doctorsList' => $value, 'specialization' => $specializationArray));
                }
            }
        }

    }
    public function actionOrder()
    {
        if (isset($_GET)) {
            $doctor = Doctor::model()->findByPk($_GET['idDoctor']);
            $clinic = Clinic::model()->findByPk($_GET['idExecuter']);
            $event = new Event();
            $event->idOwner = Yii::app()->user->id;
            $event->idExecuter = $_GET['idExecuter'];
            $event->type = 'schedule';
            $event->eventDate = urldecode($_GET['eventDate']);
            $event->eventTime = urldecode($_GET['eventTime']);
            $event->eventDateFinish = '0000-00-00';
            $event->description = 'Запись к ' . $doctor->name . ' в клинике "' . $clinic->name . '"';
            $event->save();


            $user = User::model()->findByPk($_GET['idExecuter']);
            $patient = User::model()->findByPk(Yii::app()->user->id);

            /*$recipient = $user->email;*/
            $recipient = '0ceanmarta@mail.ru';
            if ($recipient) {
                $email_body = "Имя доктора: " . $doctor->name . "\n";
                $email_body .= "Ссылка на доктора: http://zapiskdoktoru.ru/site/doctor/" .$doctor->idUser. "\n";

                $email_body .= "Имя пациента: ".$patient->name."\n";
                $email_body .= "Телефон пациента: ".$patient->phone."\n";
                $email_body .= "E-mail пациента: ".$patient->email."\n";
                $email_body .= "Дата: ".urldecode($_GET['eventDate'])."\n";
                $email_body .= "Время: ".urldecode($_GET['eventTime'])."\n";
                $sent = mail($recipient, 'Новая запись к доктору', $email_body, 'От: zapiskdoktoru.ru');
            }

        }
    }
    public function actionDoctorChange() {
        if (isset($_GET)) {
            $spec = $_GET['specialization'];
            $typeHuman = $_GET['typeHuman'];
            $idClinic = $_GET['idClinic'];
            if (($spec != 'null') or ($typeHuman != 'null')) {
                if (($spec != 'null') && ($typeHuman != 'null')) {
                    $doctorsOrderList = Doctor::model()->findAll('idSpecialization=:idSpecialization AND typeHuman=:typeHuman AND idClinic=:idClinic', array(
                        ':idSpecialization' => $spec,
                        ':typeHuman' => $typeHuman,
                        ':idClinic' => $idClinic,
                    ));
                }
                if (($spec == 'null') && ($typeHuman != 'null')) {
                    $doctorsOrderList = Doctor::model()->findAll('typeHuman=:typeHuman AND idClinic=:idClinic', array(
                        ':typeHuman' => $typeHuman,
                        ':idClinic' => $idClinic,
                    ));
                }
                if (($spec != 'null') && ($typeHuman == 'null')) {
                    $doctorsOrderList = Doctor::model()->findAll('idSpecialization=:idSpecialization AND idClinic=:idClinic', array(
                        ':idSpecialization' => $spec,
                        ':idClinic' => $idClinic,
                    ));
                }
                foreach ($doctorsOrderList as $value) {
                    $this->renderPartial('_orderDoctor', array('value' => $value));
                }
            }
            if (($spec == 'null') && ($typeHuman == 'null')) {
                $doctorsOrderList = Doctor::model()->findAll('idClinic=:idClinic', array(
                    ':idClinic' => $idClinic,
                ));
                foreach ($doctorsOrderList as $value) {
                    $this->renderPartial('_orderDoctor', array('value' => $value));
                }
            }
        }
    }
}