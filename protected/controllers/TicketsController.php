<?php

class TicketsController extends CController
{
    public $breadCrumbs;

    public function actionIndex()
    {
        if (!Yii::app()->user->isGuest) {
            $this->breadCrumbs = array('Вопрос-ответ');
            $user = User::model()->findByPk(Yii::app()->user->id);
            $criteria = new CDbCriteria();
            $criteria->with = array('user');
            $criteria->order = 't.id desc';
            if (Yii::app()->request->isAjaxRequest) {
                $type = Yii::app()->getRequest()->getParam('type');
                $date = Yii::app()->getRequest()->getParam('date');
                if($user->role == 'user') {
                    $criteria->condition = 't.type like :type and idUser = :id';
                    $criteria->params = array(':type' => '%'.$type.'%', ':id' => $user->id);
                    if($date) $criteria->addSearchCondition('date', date('Y-m-d', strtotime($date)));
                }
                elseif ($user->role == 'admin') {
                    $criteria->condition = 't.type like :type';
                    $criteria->params = array(':type' => '%'.$type.'%');
                    if($date) $criteria->addSearchCondition('date', date('Y-m-d', strtotime($date)));
                }
                $dataProvider = new CActiveDataProvider('Ticket', array(
                    'criteria' => $criteria,
                    'pagination' => array(
                        'pageSize' => 15,
                    ),
                ));
                $this->renderPartial('list', array('dataProvider' => $dataProvider), false, true);
            } else {
                if($user->role == 'user') {
                    $criteria->condition = 'idUser = :id';
                    $criteria->params = array(':id' => $user->id);
                    $dataProvider = new CActiveDataProvider('Ticket', array(
                        'criteria' => $criteria,
                        'pagination' => array(
                            'pageSize' => 15,
                        ),
                    ));
                }
                elseif ($user->role == 'admin') {
                    $dataProvider = new CActiveDataProvider('Ticket', array(
                        'pagination' => array(
                            'pageSize' => 15,
                        ),
                    ));
                }
                $this->render('index', array('dataProvider' => $dataProvider));
            }
        } else $this->redirect(Yii::app()->request->getBaseUrl(true));
    }

    public function actionView()
    {
        if (!Yii::app()->user->isGuest) {
            $idTicket = Yii::app()->request->getQuery('id');
            $id = Yii::app()->user->id;
            $ticket = Ticket::checkOpenTicket($id, $idTicket);
            $param['ticket'] = $ticket;
            if($ticket) {
                $this->breadCrumbs = array(
                    'Вопрос-ответ' => array('/tickets'),
                    $ticket->subject
                );
                $dataProvider = new CActiveDataProvider('TicketMessages', array(
                    'criteria' => array(
                        'order' => 'id desc',
                        'condition' => 'idTicket = :id',
                        'params' => array(':id' => $idTicket)
                    ),
                ));
                $param['dataProvider'] = $dataProvider;
                if($ticket->status != 2) {
                    $message = new TicketMessages;
                    $param['model'] = $message;
                    if(isset($_POST['TicketMessages'])) {
                        $message->attributes = $_POST['TicketMessages'];
                        $message->idTicket = $idTicket;
                        $message->idUser = $id;
                        $message->messageDate = date('Y-m-d H:i:s');
                        if($message->validate()) {
                            $message->save();
                            if(User::isAdmin() && isset($_POST['close']) && $_POST['close'] == 1) {
                                $message->ticket->status = 2;
                                $message->ticket->update();
                            }
                            $this->refresh();
                        }
                    }
                }
                $this->render('view', $param);
            } else $this->redirect('/tickets');
        } else $this->redirect(Yii::app()->homeUrl);
    }

    public function actionNew()
    {
        if (!Yii::app()->user->isGuest) {
            $this->breadCrumbs = array(
                'Вопрос-ответ' => array('/tickets'),
                'Новый тикет'
            );
            $id = Yii::app()->user->id;
            $ticket = new Ticket();
            if(isset($_POST['Ticket'])) {
                $ticket->attributes = $_POST['Ticket'];
                $ticket->idUser = $id;
                $ticket->status = 0;
                $ticket->date = date('Y-m-d H:i:s');
                if($ticket->validate()) {
                    $ticket->save();
                    $tm = new TicketMessages;
                    $tm->idTicket = $ticket->id;
                    $tm->idUser = $id;
                    $tm->message = $ticket->text;
                    $tm->messageDate = date('Y-m-d H:i:s');
                    $tm->save();
                    $this->redirect('/tickets');
                }
            }
            $this->render('new', array('model' => $ticket));
        } else $this->redirect(Yii::app()->homeUrl);
    }

    protected function performAjaxValidation($model, $form)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === $form) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
