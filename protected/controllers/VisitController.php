<?php

/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 03.11.2015
 * Time: 12:37
 */
class VisitController extends CController
{
    public $breadCrumbs;

    public function actionTest() {
        $id = 27;

        $model = Schedule::model()->findAll('idUser = :id and type = :type', array(':id' => $id, ':type' => 'clinicDoc'));
        var_dump($model[1]->spec->name);
    }

    public function actionIndex()
    {
        if (!Yii::app()->user->isGuest) {

            $this->breadCrumbs = array('Запись на прием');
            $step = 1;
            $specializationModel = new Specialization();
            $userModel = new User();
            $nurseModel = new Nurse();
            $this->performAjaxValidation($specializationModel, 'specialization-registration');
            $this->performAjaxValidation($nurseModel, 'nurse-registration');
            if (isset($_GET['step'])) {
                if ($_GET['step'] == 5) {
                    $docId = $_GET['docId'];
                    $doc = Schedule::model()->findByPk($docId);
                    $docName = $doc->name;
                    $timeSize = 0;
                    if($doc->type == 'doctor') {
                        $timeSize = Doctor::model()->findByPk($doc->idUser)->timeSize;
                    } else {
                        $timeSize = $doc->timeSize;
                    }
                    $visitorsModel = new Visitors();
                    $eventModel = new Event();
                    $step = 5;
                    $this->render('index', array('step' => $step, 'docName' => $docName, 'docTimeSize' => $timeSize, 'visitorsModel' => $visitorsModel, 'docId' => $docId, 'eventModel' => $eventModel));
                } else {
                    if ($_GET['step'] == 9) {
                        $nurseId = $_GET['docId'];
                        $nurse = User::model()->findByPk($nurseId);
                        $nurseName = $nurse->name;
                        $eventModel = new Event();
                        $step = 9;
                        $this->render('index', array('step' => $step, 'docName' => $nurseName, 'idNurse' => $nurseId, 'eventModel' => $eventModel));
                    }
                }
            } else
                if (isset($_POST['step'])) {
                    if ($_POST['step'] == 2) {
                        if (isset($_POST['Specialization'])) {
                            //if($_POST['isNurse'])

                            $userModel = new User();
                            $scheduleModel = new Schedule();
                            $doctorModel = new Doctor();
                            $doctorModel->setScenario('visit');
                            $this->performAjaxValidation($doctorModel, 'doctor-visiting');
                            $this->performAjaxValidation($scheduleModel, 'clinicDoc-visiting');
                            $idSpecialization = $_POST['Specialization']['name'] + 1;
                            $specialization = Specialization::model()->findByPk($idSpecialization)->name;
                            $step = 3;
                            $this->render('index', array('step' => $step, 'userModel' => $userModel, 'idSpecialization' => $idSpecialization, 'specialization' => $specialization, 'doctorModel' => $doctorModel, 'scheduleModel' => $scheduleModel));
                        }
                    } else
                        if ($_POST['step'] == 4) {
                            $timeSize = 0;//minutes
                            if (isset($_POST['Doctor'])) {
                                $doctorModel = new Doctor();
                                $this->performAjaxValidation($doctorModel, 'doctor-visiting');
                                $step = 5;
                                $docUserId = $_POST['Doctor']['name'];
                                $docId = Schedule::model()->findByAttributes(array('idUser' => $docUserId))->id;
                                $docName = User::model()->findByPk($docUserId)->name;
                                $timeSize = Schedule::model()->findByAttributes(array('idUser' => $docUserId))->timeSize;
                                //echo $docUserId;
                                //echo $docId;
                                //die();
                                $eventModel = new Event();
                                $this->render('index', array('step' => $step, 'docName' => $docName, 'docTimeSize' => $timeSize, 'eventModel' => $eventModel, 'docId' => $docId));
                            }
                            if (isset($_POST['Schedule'])) {
                                $scheduleModel = new Schedule();
                                $this->performAjaxValidation($scheduleModel, 'clinicDoc-visiting');
                                $step = 5;
                                $docId = $_POST['Schedule']['name'];
                                $doc = Schedule::model()->findByPk($docId);
                                $docName = $doc->name;
                                $timeSize = $doc->timeSize;
                                $eventModel = new Event();

                                $this->render('index', array('step' => $step, 'docName' => $docName, 'docTimeSize' => $timeSize, 'eventModel' => $eventModel, 'docId' => $docId));
                            }
                        } else
                            if ($_POST['step'] == 6) {
                                if (isset($_POST['Event'])) {
                                    $eventModel = new Event();
                                    $eventModel->attributes = $_POST['Event'];
                                    $this->performAjaxValidation($eventModel, 'timeVisiting-visiting');
                                    $step = 5;

                                    $eventModel->attributes = $_POST['Event'];
                                    $eventModel->idOwner = $_POST['hiddenPatientId'];
                                    $docId = $_POST['hiddenDocId'];
                                    $docName = Schedule::model()->findByPk($docId)->name;
                                    $docTimeSize = 0;
                                    $docType = Schedule::model()->findByPk($docId)->type;
                                    $specialization = "";
                                    if ($docType == 'doctor') {
                                        $userId = Schedule::model()->findByPk($docId)->idUser;
                                        $docTimeSize = Doctor::model()->findByPk($userId)->timeSize;
                                        $idSpecialization = Doctor::model()->findByAttributes(array('idUser' => $userId))->idSpecialization;
                                        $specialization = Specialization::model()->findByPk($idSpecialization)->name;
                                    }
                                    if ($docType == 'clinicDoc') {
                                        $docTimeSize = Schedule::model()->findByPk($docId)->timeSize;
                                        $idSpecialization = Schedule::model()->findByPk($docId)->specialization;
                                        $specialization = Specialization::model()->findByPk($idSpecialization)->name;
                                    }

                                    $eventModel->idExecuter = $docId;
                                    $eventModel->type = 'schedule';

                                    $eventDate = $_POST['Event']['eventDate'];
                                    $eventModel->eventDate = $eventDate;
                                    $eventTime = $_POST['Event']['eventTime'];
                                    $eventModel->eventTime = $eventTime;

                                    if ($eventModel->validate()) {
                                        $scheduleModel = Schedule::model()->findByPk($docId);
                                        $userDoc = User::model()->findByPk($scheduleModel->idUser);
                                        $phone = $userDoc->phone;
                                        $userDoc->money = $userDoc->money - $userDoc->tax;
                                        $userDoc->update();

                                        $message = "";
                                        if ($docType == 'doctor') {
                                            $message = 'Записись на дату:' . $eventDate . ', время: ' . $eventTime . ', пациент: ' . User::model()->findByPk(Yii::app()->user->id)->name;
                                        }
                                        if ($docType == 'clinicDoc') {
                                            $message = 'Записись к доктору ' . $docName . ' на дату:' . $eventDate . ', время: ' . $eventTime . ', пациент: ' . User::model()->findByPk(Yii::app()->user->id)->name;
                                        }
                                        //Yii::app()->sms->sms_send($phone, $message);
                                        $eventModel->save();
                                        $step = 7;
                                    }

                                    $this->render('index', array('step' => $step, 'eventModel' => $eventModel, 'docId' => $docId, 'docTimeSize' => $docTimeSize, 'docName' => $docName, 'visitDate' => $eventDate, 'visitTime' => $eventTime, 'visitDocSpecialization' => $specialization));
                                }
                            } else
                                if ($_POST['step'] == 8) {
                                    $step = 9;
                                    $idNurse = null;
                                    if (!isset($_POST['Nurse']['name'])) {
                                        $idNurse = $_POST['idNurse'];
                                    } else
                                        $idNurse = $_POST['Nurse']['name'];

                                    $nurse = Nurse::model()->findByAttributes(array('idUser' => $idNurse));
                                    $userNurse = User::model()->findByPk($idNurse);

                                    User::model()->updateByPk($idNurse, array('money' => $userNurse->money - 50));

                                    $eventModel = new Event();
                                    $eventModel->setScenario('nurse');
                                    if(isset($_POST['Event']))
                                        $eventModel->attributes = $_POST['Event'];

                                    $eventModel->idOwner = Yii::app()->user->id;
                                    $eventModel->idExecuter = $nurse->idUser;
                                    $eventModel->type = 'nurse';

                                    if ($eventModel->validate()) {
                                        //Yii::app()->sms->sms_send($userNurse->phone, "К вам только что записался пациент");
                                        $eventModel->save();
                                        $step = 11;
                                    }
                                    $this->render('index', array('step' => $step, 'docName' => $nurse->name, 'idNurse' => $nurse->idUser, 'eventModel' => $eventModel));
                                }
                } else {
                    $this->render('index', array('step' => $step, 'specializationModel' => $specializationModel, 'userModel' => $userModel, 'nurseModel' => $nurseModel));
                }
        } else $this->redirect('/');
    }

    public function actionGetTime() {

        if (!Yii::app()->user->isGuest) {
            $visitDate = $_POST['visitDate'];
            $docId = $_POST['docId'];
            $type = 'schedule';

            //$visitDate = "2015-11-23";
            //$docId = 20;

            $dayOfWeek = date('N', strtotime($visitDate));
            //echo $docId . ' ' . $dayOfWeek . ' ';
            $scheduleModel = Schedule::model()->find("id = :id", array(':id' => $docId));
            $arrayTime = array();
            $time = null;
            $timeIntervals = array();

            $visitors = Event::model()->findAllByAttributes(array('idExecuter'=>$docId, 'eventDate'=>$visitDate, 'type'=>$type));
            foreach($visitors as $key){
                array_push($timeIntervals, $key->eventTime);
            }

            //print_r($timeIntervals);
            //die();

            switch ($dayOfWeek) {
                case 1:
                    $time = $scheduleModel->monday;
                    break;
                case 2:
                    $time = $scheduleModel->tuesday;
                    break;
                case 3:
                    $time = $scheduleModel->wednesday;
                    break;
                case 4:
                    $time = $scheduleModel->thursday;
                    break;
                case 5:
                    $time = $scheduleModel->friday;
                    break;
                case 6:
                    $time = $scheduleModel->saturday;
                    break;
                case 7:
                    $time = $scheduleModel->sunday;
                    break;
            }

            $arrayTime = array('time' => $time, 'timeIntervals' => $timeIntervals);
            echo json_encode($arrayTime);

        }

    }

    public function actionCheckDate(){

        $dateBegin = $_POST['dateBegin'];
        $dateEnd = $_POST['dateEnd'];
        $idNurse = $_POST['idNurse'];
        $timeBegin = strtotime($dateBegin);
        $timeEnd = strtotime($dateEnd);
        if($timeBegin >= $timeEnd){
            echo json_encode(array('message'=>'дата окончания должна быть больше даты начала'));
            return;
        }
        $events = Event::model()->findAllByAttributes(array('idExecuter' => $idNurse));
        $arrayTimes = array();
        foreach($events as $key){
            array_push($arrayTimes, array( strtotime($key->eventDate), strtotime($key->eventDateFinish) ));
        }
        $flag = true;
        for($i=0;$i<count($arrayTimes);$i++){
            if(($timeBegin >= $arrayTimes[$i][0])&&($timeBegin <= $arrayTimes[$i][1])){
                $flag = false;
            }
            if($flag){
                if(($timeEnd >= $arrayTimes[$i][0])&&($timeEnd <= $arrayTimes[$i][1])){
                    $flag = false;
                }
            }
            if($flag){
                if(($timeBegin < $arrayTimes[$i][0])&&($timeEnd > $arrayTimes[$i][1])){
                    $flag = false;
                }
            }
        }
        if($flag)
            echo json_encode(array('message'=>'ok', 'dateBegin'=>$dateBegin, 'dateEnd'=>$dateEnd));
        else
            echo json_encode(array('message'=>'эта дата уже занята'));
    }

    protected function performAjaxValidation($model, $form){
        if (isset($_POST['ajax']) && $_POST['ajax'] === $form) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}