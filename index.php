<?
defined('YII_DEBUG') or define('YII_DEBUG',true);
define('YII_ENABLE_ERROR_HANDLER', true);
require_once(dirname(__FILE__).'/framework/yii.php');
$configFile= include(dirname(__FILE__).'/protected/config/main.php');

Yii::createWebApplication($configFile)->run();
?>