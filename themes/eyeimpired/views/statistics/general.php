<?php
/* @var $this StatisticsController */
?>

<div class="row">
    <div class="col-md-12">

        <div class="box box-info">
            <div class="box-header with-border ui-sortable-handle">
                <h3 class="box-title">Общая статистика</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <? $this->widget('zii.widgets.grid.CGridView', array(
                    'dataProvider' => $model->search(),
                    'filter' => $model,
                    'cssFile' => Yii::app()->request->getBaseUrl(true) . '/assets/admin/css/gridview.css',
                    'itemsCssClass' => 'table no-margin',
                    'htmlOptions' => array(
                        'class' => 'table-responsive'
                    ),
                    'afterAjaxUpdate' => "function() {
                        jQuery('[name=\"Statistic[time]\"]').daterangepicker();
                    }",
                    'summaryText' => 'Показано записей: {start} - {end} из {count}',
                    'emptyText' => 'Результаты не найдены',
                    'columns' => array(
                        'time',
                        'newUser',
                        'user',
                        'doctor',
                        'clinic',
                        'pacient',
                        'nurse',
                        'payed',
                        'purchase',
                        'article'
                    ),
                )); ?>
            </div>
            <div class="box-footer clearfix"></div>
        </div>

    </div>
</div>

<?
$script = <<<JS
    $('[name="Statistic[time]"]').daterangepicker();
JS;

Yii::app()->clientScript->registerScript('date', $script, CClientScript::POS_READY);
?>

