<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 26.11.2015
 * Time: 13:27
 */
$beforeAjaxUpdate = <<<JS
    function(){
        $('#preloader').css('display', 'block');
        $('#grid').css('display', 'none');
    }
JS;

$afterAjaxUpdate = <<<JS
    function(){
        $('#preloader').css('display', 'none');
        $('#grid').css('display', 'block');
    }
JS;

$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'grid',
    'ajaxUpdate' => true,
    'dataProvider' => $dataProvider,
    'beforeAjaxUpdate' => $beforeAjaxUpdate,
    'afterAjaxUpdate' => $afterAjaxUpdate,
    'cssFile' => Yii::app()->request->getBaseUrl(true) . '/assets/admin/css/gridview.css',
    'itemsCssClass' => 'table no-margin',
    'htmlOptions' => array(
        'class' => 'table-responsive',
        'style' => 'display:none'
    ),
    'summaryText' => 'Показаны пользователи: {start} - {end} из {count}',
    'emptyText' => 'Результаты не найдены',
    'columns' => array(
        array(
            'name' => 'idReferer',
            'value' => 'User::getName($data->idReferal)'
        ),
        'money'
    ),
    'pager' => array(
        'class' => 'CLinkPager',
        'header' => '',
        'nextPageLabel' => 'Следующая',
        'prevPageLabel' => 'Предыдущая'
    )
));
