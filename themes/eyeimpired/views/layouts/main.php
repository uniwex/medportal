<?php
if(!Yii::app()->user->isGuest) {
    if(User::model()->findByPk(Yii::app()->user->id) == false){
        $this->redirect("/site/logout");
    }
}
?>
<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Сайт по поиску врачей</title>

    <link href='<?= Yii::app()->request->baseUrl ?>/assets/styles/jquery-ui.css' media="screen" rel="stylesheet"
          type="text/css"/>
    <link href='<?= Yii::app()->request->baseUrl ?>/assets/bootstrap/css/bootstrap.css' media="screen" rel="stylesheet"
          type="text/css"/>
    <link href='<?= Yii::app()->request->baseUrl ?>/assets/styles/main.css' media="screen" rel="stylesheet" type="text/css"/>
    <link href='<?= Yii::app()->request->baseUrl ?>/assets/styles/eyeimpired.css' media="screen" rel="stylesheet" type="text/css"/>
    <link rel="shortcut icon" href=<?= '"'.Yii::app()->request->baseUrl . "/assets/images/favicon.ico\""; ?> type="image/x-icon" >
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <? Yii::app()->getClientScript()->registerCoreScript('jquery'); ?>
</head>
<body>
<div id="preload" class="overlay">
    <div id="spinner"></div>
</div>
<div class="overlay" id="overlaySchedule">
    <div class="modal-window">
        <div class="modal-content">
            <div class="modal-table">
                <table id="schedule">
                    <tr>
                        <th>Пн</th>
                        <th>Вт</th>
                        <th>Ср</th>
                        <th>Чт</th>
                        <th>Пт</th>
                        <th>Сб</th>
                        <th>Вс</th>
                    </tr>
                    <tr>
                        <th>с <input autocomplete="off" type="text"><br>до
                            <input autocomplete="off" type="text"></th>
                        <th>с <input autocomplete="off" type="text"><br>до
                            <input autocomplete="off" type="text"></th>
                        <th>с <input autocomplete="off" type="text"><br>до
                            <input autocomplete="off" type="text"></th>
                        <th>с <input autocomplete="off" type="text"><br>до
                            <input autocomplete="off" type="text"></th>
                        <th>с <input autocomplete="off" type="text"><br>до
                            <input autocomplete="off" type="text"></th>
                        <th>с <input autocomplete="off" type="text"><br>до
                            <input autocomplete="off" type="text"></th>
                        <th>с <input autocomplete="off" type="text"><br>до
                            <input autocomplete="off" type="text"></th>
                    </tr>
                </table>
            </div>
            <div class="modal-btn">
                <input type="button" class="button1" value="+ время" id="addTime">
                <input type="button" class="button1 disabled" disabled="disabled" value="- время" id="removeTime">
                <input type="button" class="button1" value="Сохранить" id="saveSchedule" onclick="SaveSchedule()">
                <input type="button" class="button1" value="Закрыть"
                       onclick="$('#overlaySchedule').css('display', 'none');">
            </div>
        </div>
    </div>
</div>

<div class="overlay" id="overlay-calendar">
    <div class="modal-window">
        <div class="close-button">X</div>
        <?
        if (!Yii::app()->user->isGuest) {
            if (User::model()->findByPk(Yii::app()->user->id)->type == 'patient') { ?>
                <div class="calendar-appointment">
                <span>
                    <a href="<?= Yii::app()->request->baseUrl . "/visit" ?>">
                        <i class="fa fa-calendar-plus-o"><span style="font-family: MyriadProRegular;font-weight: 100;font-size: 12pt;">Записаться к врачу</span></i>
                    </a>
                </span>
                </div>
            <?php }
        }
        ?>

        <div class="calendar-appointment"><span><a href="<?= Yii::app()->request->baseUrl . "/note" ?>"><i
                        class="fa fa-calendar-plus-o"><span style="font-family: MyriadProRegular;font-weight: 100;font-size: 12pt;">Создать заметку</span></i></a></span></div>
        <hr class="modal-hr">
        <div id="calendar-on-day">
        </div>
    </div>
</div>

<div class="container">
    <div id="block-settings">
        <div style="font-size:18pt;">Размер шрифта: <span id="smallFontSize" style="font-size:16pt;margin-right:10px;cursor:pointer;">А</span><span id="middleFontSize" style="font-size:20pt;margin-right:10px;cursor:pointer;">А</span><span id="bigFontSize" style="font-size:22pt;cursor:pointer;">А</span></div>
        <div style="font-size:18pt;">Интервал между букв: <span id="interval-standard">стандартный,</span> <span id="interval-big">большой</span></div>
        <div style="font-size:18pt;">Выбор цветовой гаммы: <span class="color-preview" style="background-color:#000;margin-right:10px;"></span><span class="color-preview"></span></div>
    </div>
    <div class="row">
        <div class="col-md-12">

            <div class="pull-left" id="blueCross">
                <a href="/">
                    <img class="pull-left" src='<?= Yii::app()->request->baseUrl ?>/assets/images/blueCross.png'>
                    <div class="pull-right middleFontSize" id="regToDoctorBlack">ЗАПИСЬ К ДОКТОРУ</div>
                </a>
            </div>

            <div class="row">

                <div class="col-md-3 col-md-pull-2 pull-right col-xs-pull-3">
                    <a href="/site/inProcess" style="text-decoration:none;">
                        <div class="order middleFontSize">
                            <img class="phoneIcon" src='<?= Yii::app()->request->baseUrl ?>/assets/images/phone1.png'>
                            Заказать звонок
                        </div>
                    </a>
                </div>
                <div class="col-md-3 pull-right col-md-push-2 col-xs-4 col-xs-push-2" style="padding:0px;margin-right:-40px;text-align:right;">
                    <a href="/site/default">
                        <button id="buttonToNormalVersion" type="button" class="btn btn-default middleFontSize whiteInvertMode">Обычная версия</button>
                    </a>
                    <a>
                        <button id="buttonSettings" type="button" class="btn btn-default middleFontSize whiteInvertMode"><i class="fa fa-caret-right hidden-xs hidden-sm" style="left:55px;"></i>Настройки</button>
                        <?php if(!Yii::app()->user->isGuest){ ?>
                            <div style="font-size:22px;"><a href="/site/logout">Выход</a></div>
                        <?php } ?>
                    </a>
                </div>
                <div class="col-md-3 pull-right">
                    <div class="phoneNumber middleFontSize">8 (495) 777 77 00</div>
                </div>
            </div>
        </div>
    </div>
    <div class="row indent main-menu">
        <div class="col-md-4 col-xs-4">
            <span><a class="middleFontSize" href="/site/personal">Личный кабинет</a></span>
        </div>
        <div class="col-md-4 col-xs-4">
            <?php if(!Yii::app()->user->isGuest){ ?>
            <span><a class="middleFontSize" href="/tickets">Вопрос-Ответ</a></span>
            <?php } else {?>
            <span><a class="middleFontSize" href="/site/personal">Вопрос-Ответ</a></span>
            <?php }?>
        </div>
        <div class="col-md-4 col-xs-4">
            <span><a class="middleFontSize" href="http://vk.com/vrach_help">Онлайн консультация</a></span>
        </div>
    </div>
    <hr>
    <?php
    $submitDoctor = 'none';
    if(isset($_POST['submitDoctor']))
        $submitDoctor = 'inline';
    $submitClinic = 'none';
    if(isset($_POST['submitClinic']))
        $submitClinic = 'inline';
    $submitNurse = 'none';
    if(isset($_POST['submitNurse']))
        $submitNurse = 'inline';
    ?>
    <div class="row indent main-menu" style="margin:0px auto 40px;">
        <div class="col-md-4 col-xs-4">
            <div data-type="doctor" class="btn btn-default search-btn middleFontSize btn-xs-margin-left <?= ($submitDoctor == 'inline') ? 'item-active' : ''; ?>"><i class="fa fa-caret-right hidden-xs hidden-sm" style="left:20px;"></i>Поиск врача</div>
        </div>
        <div class="col-md-4 col-xs-4">
            <div data-type="clinic" class="btn btn-default search-btn middleFontSize <?= ($submitClinic == 'inline') ? 'item-active' : ''; ?>"><i class="fa fa-caret-right hidden-xs hidden-sm" style="left:7px;"></i>Поиск клиники</div>
        </div>
        <div class="col-md-4 col-xs-4">
            <div data-type="nurse" class="btn btn-default search-btn middleFontSize btn-xs-margin-right <?= ($submitNurse == 'inline') ? 'item-active' : ''; ?>"><i class="fa fa-caret-right hidden-xs hidden-sm" style="left:8px;"></i>Поиск сиделки</div>
        </div>
    </div>
    <span id="show-doctor" class="search-form-doctor" style="display:<?= $submitDoctor; ?>">
        <div class="row indent main-menu">
            <div class="col-md-4 col-xs-4">
                <span class="middleFontSize" style="margin-left:15px;">Найдите врача:</span>
            </div>
        </div>
        <? $form = $this->beginWidget('CActiveForm', array(
            'id' => 'search-form-doctor',
            'action' => Yii::app()->request->baseUrl . '/search/',
        ));
        $userModel = new User();
        $stationModel = new Station();
        ?>
        <?= CHtml::hiddenField('type', '') ?>
        <div class="row">
            <div class="col-md-7 col-xs-7" style="left:6%;">
                <input class="main-input" type="text" name="doctor" id="input-search" placeholder="Поиск врача..." style="width:88%;">
            </div>
        </div>

        <div class="row">
            <div class="col-md-7 col-xs-7" style="left:6%;">
                <div style="">
                    <?
                    /*
                    values come from JS
                    */
                    echo $form->dropDownList($userModel, 'country', array(0 => 'Выберите страну', 1 => 'Россия'), array('class' => 'main-input', 'data-alias' => 'country', 'style' => '')); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7 col-xs-7" style="left:6%;">
                <div style="">
                    <?= $form->textField($userModel, 'city', array('class' => 'main-input inputForSearchCity', 'style' => '', 'placeholder' => 'Выберите город', 'autocomplete'=> 'off')); ?>
                    <select class="dropDownCities" type="select" size="5" data-alias="city" style="width:88%;background:none;font-size: 20pt;">
                        <option>Выберите город</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7 col-xs-7" style="left:6%;">
                <div style="">
                    <?= $form->dropDownList($stationModel, 'name', array('0' => 'Выберите станцию метро', 'Нет метро' => 'Нет метро'), array('class' => 'main-input', 'data-alias' => 'metro', 'style' => '')); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-7 col-xs-7" style="left:6%;">
                <?php
                $specializations = Specialization::model()->findAll();
                $listSpecs = array();
                foreach($specializations as $spec){
                    $listSpecs[$spec['id']] = $spec['name'];
                }
                $listSpecs = array_merge(array('' => 'Выбрать специальность'), $listSpecs);
                ?>
                <?= CHtml::dropDownList(
                    'specialization',
                    isset($_POST['specialization']) ? $_POST['specialization'] : '',
                    $listSpecs,
                    array('class' => 'main-input', 'id'=>'input-speciality')
                ) ?>
                <!--
                <select class="main-input" type="select" name="speciality" id="input-speciality">
                    <option>Выберите специальность</option>
                    <option>first el</option>
                    <option>second el</option>
                    <option>third el</option>
                </select>
                -->
            </div>
        </div>
        <!--
        <div class="row" style="font-size:22px;">
            <div class="col-md-7 col-md-push-1 col-xs-7 col-xs-push-1">
                <span id="age-region">Выбрать врача по возрастному приёму:</span>
            </div>
            <div class="col-md-3">
                <select class="main-input2" type="select" name="age-region" id="input-age-region" style="margin-top:0px;">
                    <option>Взрослые</option>
                    <option>first el</option>
                    <option>second el</option>
                    <option>third el</option>
                </select>
            </div>
        </div>
        -->
        <div class="row indent main-menu" style="margin:20px auto 30px;">
            <div class="col-md-4 col-xs-4">
                <button type="submit" name="submitDoctor" class="btn btn-default search-btn btn-xs-margin-left" value="Найти" style="margin-bottom:10px;"><i class="fa fa-caret-right" style="left:20px;"></i>Найти</button>
            </div>
        </div>
        <? $this->endWidget(); ?>
    </span>
    <span id="show-clinic" class="search-form-clinic" style="display:<?= $submitClinic; ?>">
        <div class="row indent main-menu">
            <div class="col-md-4 col-xs-4">
                <span class="middleFontSize" style="margin-left:15px;">Найдите клинику:</span>
            </div>
        </div>
        <? $form = $this->beginWidget('CActiveForm', array(
            'id' => 'search-form-clinic',
            'action' => Yii::app()->request->baseUrl . '/search/',
        ));
        $userModel = new User();
        $stationModel = new Station();
        ?>
        <?= CHtml::hiddenField('type', '') ?>
        <div class="row">
            <div class="col-md-7 col-xs-7" style="left:6%;">
                <input class="main-input" type="text" name="clinic" id="input-search" placeholder="Поиск клиники...">
            </div>
        </div>
        <div class="row">
            <div class="col-md-7 col-xs-7" style="left:6%;">
                <div style="">
                    <?= $form->dropDownList($userModel, 'country', array(0 => 'Выберите страну', 1 => 'Россия'), array('class' => 'main-input', 'data-alias' => 'country', 'style' => '')); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7 col-xs-7" style="left:6%;">
                <div style="">
                    <?= $form->textField($userModel, 'city', array('class' => 'main-input inputForSearchCity', 'style' => '', 'placeholder' => 'Выберите город', 'autocomplete'=> 'off')); ?>
                    <select class="dropDownCities" type="select" size="5" data-alias="city" style="width:88%;background:none;font-size: 20pt;">
                        <option>Выберите город</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7 col-xs-7" style="left:6%;">
                <div style="">
                    <?= $form->dropDownList($stationModel, 'name', array('0' => 'Выберите станцию метро', 'Нет метро' => 'Нет метро'), array('class' => 'main-input', 'data-alias' => 'metro', 'style' => '')); ?>
                </div>
            </div>
        </div>
        <div class="row indent main-menu" style="margin:20px auto 30px;">
            <div class="col-md-4 col-xs-4">
                <button type="submit" name="submitClinic" class="btn btn-default search-btn btn-xs-margin-left" value="Найти" style="margin-bottom:10px;"><i class="fa fa-caret-right" style="left:20px;"></i>Найти</button>
            </div>
        </div>
        <? $this->endWidget(); ?>
    </span>
    <span id="show-nurse" class="search-form-nurse" style="display:<?= $submitNurse; ?>">
        <div class="row indent main-menu">
            <div class="col-md-4 col-xs-4">
                <span class="middleFontSize" style="margin-left:15px;">Найдите сиделку:</span>
            </div>
        </div>
        <? $form = $this->beginWidget('CActiveForm', array(
            'id' => 'search-form-nurse',
            'action' => Yii::app()->request->baseUrl . '/search/',
        ));
        $userModel = new User();
        $stationModel = new Station();
        ?>
        <?= CHtml::hiddenField('type', '') ?>
        <div class="row">
            <div class="col-md-7 col-xs-7" style="left:6%;">
                <input class="main-input" type="text" name="nurse" id="input-search" placeholder="Поиск сиделки...">
            </div>
        </div>

        <div class="row">
            <div class="col-md-7 col-xs-7" style="left:6%;">
                <div style="">
                    <?= $form->dropDownList($userModel, 'country', array(0 => 'Выберите страну', 1 => 'Россия'), array('class' => 'main-input', 'data-alias' => 'country', 'style' => '')); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7 col-xs-7" style="left:6%;">
                <div style="">
                    <?= $form->textField($userModel, 'city', array('class' => 'main-input inputForSearchCity', 'style' => '', 'placeholder' => 'Выберите город', 'autocomplete'=> 'off')); ?>
                    <select class="dropDownCities" type="select" size="5" data-alias="city" style="width:88%;background:none;font-size: 20pt;">
                        <option>Выберите город</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7 col-xs-7" style="left:6%;">
                <div style="">
                    <?= $form->dropDownList($stationModel, 'name', array('0' => 'Выберите станцию метро', 'Нет метро' => 'Нет метро'), array('class' => 'main-input', 'data-alias' => 'metro', 'style' => '')); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-7" style="left:6%;">
                <span id="Nurse_residence">
                    <input id="Nurse_residence_0" type="radio" name="Nurse[residence]" value="0" checked>
                    <label class="indent" for="Nurse_residence_0">Без проживания</label>
                    <br>
                    <input id="Nurse_residence_1" type="radio" name="Nurse[residence]" value="1">
                    <label class="indent" for="Nurse_residence_1">С проживанием</label>
                </span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7" style="left:6%;margin-top:10px;">
                <span id="Nurse_food">
                    <input id="Nurse_food_0" type="radio" name="Nurse[food]" value="0" checked>
                    <label class="indent" for="Nurse_food_0">Без приготовления еды</label>
                    <br>
                    <input id="Nurse_food_1" type="radio" name="Nurse[food]" value="1">
                    <label class="indent" for="Nurse_food_1">С приготовлением еды</label>
                </span>
            </div>
        </div>

        <div class="row indent main-menu" style="margin:20px auto 30px;">
            <div class="col-md-4 col-xs-4">
                <button type="submit" name="submitNurse" class="btn btn-default search-btn btn-xs-margin-left" value="Найти" style="margin-bottom:10px;"><i class="fa fa-caret-right" style="left:20px;"></i>Найти</button>
            </div>
        </div>
        <? $this->endWidget(); ?>
    </span>

    <?= $content; ?>

    <div class="row footer">
        <div class="col-md-4 col-md-push-1">
            <!--<div style="font-size: 22pt;">Новости</div>-->
            <div class="policy middleFontSize">Единый полис ОМС</div>
            <div class="footer-date middleFontSize" style="font-size: 11pt;">25 декабря 2015г.</div>
        </div>
    </div>
</div>
<script type="text/javascript" src='<?= Yii::app()->request->baseUrl ?>/assets/scripts/jquery-ui.js'></script>
<script type="text/javascript" src='<?= Yii::app()->request->baseUrl ?>/assets/bootstrap/js/bootstrap.js'></script>
<script type="text/javascript" src='<?= Yii::app()->request->baseUrl ?>/assets/scripts/main2.js'></script>
<script type="text/javascript" src='<?= Yii::app()->request->baseUrl ?>/assets/scripts/globalSearch.js'></script>
<script>
    /*
    $('[name="submitDoctor"]').click(function(){
        $('#input-search').attr('name','doctor');
    });
    $('[name="submitClnic"]').click(function(){
        $('#input-search').attr('name','clinic');
    });
    $('[name="submitNurse"]').click(function(){
        $('#input-search').attr('name','nurse');
    });
    */
    $('.main-menu [data-type="doctor"]').click(function(){
        $('#show-doctor').show();
        $('#show-clinic').hide();
        $('#show-nurse').hide();
        $('.item-active').removeClass('item-active');
        $(this).addClass('item-active');
    });
    $('.main-menu [data-type="clinic"]').click(function(){
        $('#show-doctor').hide();
        $('#show-clinic').show();
        $('#show-nurse').hide();
        $('.item-active').removeClass('item-active');
        $(this).addClass('item-active');
    });
    $('.main-menu [data-type="nurse"]').click(function(){
        $('#show-doctor').hide();
        $('#show-clinic').hide();
        $('#show-nurse').show();
        $('.item-active').removeClass('item-active');
        $(this).addClass('item-active');
    });
</script>
</body>
</html>