<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 27.10.2015
 * Time: 12:12
 */
if($dataProvider)
$this->widget('zii.widgets.CListView', array(
    'id' => 'list',
    'dataProvider' => $dataProvider,
    'enablePagination' => true,
    'ajaxUpdate' => true,
    'beforeAjaxUpdate' => 'function(){ $("#preload").css("display", "block"); }',
    'afterAjaxUpdate' => 'function(){ $("#preload").css("display", "none"); }',
    'template' => '{items}{pager}',
    'loadingCssClass' => '',
//    'pager' => array(
//        'class' => 'CLinkPager',
//        'header' => '',
//        'maxButtonCount' => 3,
//        'cssFile' => Yii::app()->request->getBaseUrl(true) . '/css/listview_pager.css',
//    ),
    'itemView' => '_list',
    'summaryText' => '',
    'emptyText' => 'В этой клинике пока нет докторов',
    'htmlOptions' => array(
        'class' => 'col-md-12'
    )
)); ?>