<div class="row">
    <div class="col-md-11 col-md-push-1 col-xs-11 col-xs-push-1">
        <h1 class="middleFontSize" style="cursor:default;text-decoration:none;"><b>Личный кабинет:</b></h1>
    </div>
</div>


<div class="row indent main-menu" style="margin:20px auto 40px;">
    <div class="col-md-4 col-xs-4">
        <a href="/site/registration"><div class="btn btn-default search-btn btn-xs-margin-left middleFontSize"><i class="fa fa-caret-right" style="left:20px;"></i>Регистрация</div></a>
    </div>
    <div class="col-md-4 col-xs-4"></div>
    <div class="col-md-4 col-xs-4">
        <a href="/site/login"><div class="btn btn-default search-btn middleFontSize"><i class="fa fa-caret-right" style="left:20px;"></i>Авторизация</div></a>
    </div>
</div>


<link rel="stylesheet" href="<?= Yii::app()->request->getBaseUrl(true); ?>/assets/admin/bootstrap/css/bootstrap-multiselect.css">
<script src="<?= Yii::app()->request->getBaseUrl(true); ?>/assets/admin/bootstrap/js/bootstrap-multiselect.js"></script>
<script src="//cdn.ckeditor.com/4.5.4/standard/ckeditor.js"></script>
<script type="text/javascript">
    window.onload = function () {
        var inputs = document.querySelectorAll('#user-registration input')
        for (i = 0; i < inputs.length; i++) {
            if ((inputs[i].className.indexOf('button') == -1) && (inputs[i].type != 'radio'))
                inputs[i].value = '';
        }
        /*$('[data-alias=metro]').multiselect({
            includeSelectAllOption: true
        });*/
    }
</script>
