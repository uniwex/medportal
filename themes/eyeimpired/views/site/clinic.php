<?
/*Yii::app()->clientScript->registerScript('clinicDocs',
    "
$('.clinicDoctor').on('click', function(){
    $('#modalClinicDoctors').css('display','block');
    $('#overlayModalClinicDoctors').css('display','block');
    var clinicId = $(this).attr('data-id');
    $.fn.yiiListView.update('list', {
            url: '" . CController::createUrl('search/getDoctors') . "',
            data: 'clinicId='+clinicId
        }
    );
});
$('#overlayModalClinicDoctors').click(function(){
    $('#overlayModalClinicDoctors').css('display','none');
});
");*/
?>
<div id="overlayModalClinicDoctors" class="overlay">
    <div id="modalClinicDoctors" style="display:none;">
        <div class="row form-group">

            <?php
            $this->widget('zii.widgets.CListView', array(
                'id' => 'list',
                'dataProvider' => $dataProvider,
                'enablePagination' => true,
                'ajaxUpdate' => true,
                'beforeAjaxUpdate' => 'function(){ $("#preload").css("display", "block"); }',
                'afterAjaxUpdate' => 'function(){ $("#preload").css("display", "none"); }',
                'template' => '{items}{pager}',
                'loadingCssClass' => '',
//    'pager' => array(
//        'class' => 'CLinkPager',
//        'header' => '',
//        'maxButtonCount' => 3,
//        'cssFile' => Yii::app()->request->getBaseUrl(true) . '/css/listview_pager.css',
//    ),
                'itemView' => '_list',
                'summaryText' => '',
                'emptyText' => 'В этой клинике пока нет докторов',
                'htmlOptions' => array(
                    'class' => 'col-md-12'
                )
            )); ?>
            ?>

        </div>
    </div>
</div>
<div class="overlay" id="overlay-make-vip">
    <div class="modal-window" style="height:145px;">
        <div class="close-button">X</div>
        <div class="form-group row indent" style="font-size:16px;">Выберите тариф:</div>
        <?php $formMakeVip = $this->beginWidget("CActiveForm", array(
            'id' => 'form-make-vip',
            'action' => Yii::app()->request->baseUrl . '/site/buyVip',
            'method' => 'post',
            'enableAjaxValidation' => true,
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => true
            ),
            'htmlOptions' => array(
                'enctype' => 'multipart/form-data',
                'autocomplete' => 'off',
                'onsubmit' => 'return true',
            ),
        )); ?>
        <div class="col-md-12 indent">
            <?php
            $prices = PriceForVip::model()->findAll();
            $ifChecked = false;
            foreach($prices as $price){
                echo "<input value=\"$price->id\" type=\"radio\" name=\"vip-type\" id=\"vip_type_$price->id\"";
                if(!$ifChecked){
                    echo 'checked>';
                    $ifChecked = true;
                }
                else{
                    echo '>';
                }
                echo "<label for=\"vip_type_$price->id\">$price->name</label>";
            }
            ?>
        </div>
        <div class="col-md-12">
            <?= CHtml::submitButton('Купить', array('class' => 'button1 button1reg')); ?>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>

<div class="row">
<div id="postAdvert" class="col-md-4">
    <div class="row">
        <div class="col-md-12">
            <div class="profile-pictureBlock" style="background-image:url('<?= $clinic->getImage($clinic->pic); ?>')">
                <!--<span class="profilePic-bgLayer"></span>-->
            </div>
        </div>
    </div>
    <div class="row text-left">
        <div class="col-md-12 profile-stars">
            <?php
            for ($i = 0; $i < $clinic->rate; $i++) {
                echo Yii::app()->params['goldStarBig'];
            }
            for ($i = 0; $i < 5 - $clinic->rate; $i++) {
                echo Yii::app()->params['grayStarBig'];
            } ?>
        </div>
    </div>
    <? if((!Yii::app()->user->isGuest)) { ?>
        <?php
            $clinicUser = User::model()->findByPk($clinic->idUser);
            if(User::model()->findByPk(Yii::app()->user->id)->id != $clinic->idUser){
            if($clinicUser->money - $clinicUser->tax >= 0) {
        ?>
        <div class="row text-left">
            <div class="col-md-12">
                <div class="button1 middle bg-redColor" style="font-size: 14pt;line-height: 1.7;">
                    <i class="fa fa-plus"></i>Записаться
                </div>
            </div>
        </div>
    <? } } } ?>
    <? if((Yii::app()->user->isGuest)) { ?>
        <div class="row text-left">
            <div class="col-md-12">
                <a href="/site/registration">
                    <div class="button1 middle bg-redColor" style="font-size: 14pt;line-height: 1.7;">
                        <i class="fa fa-plus"></i>Записаться
                    </div>
                </a>
            </div>
        </div>
    <?php } ?>
    <div class="row indent">
        <div class="col-md-12">
            <div class="userInformationHead2">Информация о клинике:</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="userInformationHead1 userInfoMargin"><?= $clinic->getAttributeLabel('specialization') ?>:</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="userInformationText">
                <?= $clinic->specialization; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="userInformationHead1 userInfoMargin">Часы приёма:</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="userInformationText">
                <?= Schedule::getScheduleClinic($clinic->idUser) ?>
            </div>
        </div>
    </div>
    <?php
    $idUser = $clinic->idUser;
    $user = User::model()->findByPk($idUser);

    if(isset(Yii::app()->user->id)){
        if(Yii::app()->user->id == Yii::app()->getRequest()->getParam('id')) {

            if ($user->vipTimeEnd < date('Y-m-d H:i')) {
                ?>
                <input id="button-make-vip" type="button" class="button1 button1reg" value="Купить вип-аккаунт">
            <?php
            } else {
                ?>
                Куплен VIP-аккаунт.<br>Срок окончания действия <?= $user->vipTimeEnd; ?>
            <?php
            }
            ?>
            <br><br>
            <div>Ваш баланс: <?= $user->money; ?> рублей</div>
            <input type="button" class="button1 button1reg" value="Пополнить баланс" onclick="goToPay()">
            <br>
            <br>
            <div>Реферальная ссылка:</div>
            <?php echo "<div><input value=\"" . Yii::app()->getBaseUrl(true) . '/site/registration?idRef=' . Yii::app()->request->getQuery('id') . "\" style=\"width:100%;\" readonly></div>" ?>
        <?php
        } }
    if(Yii::app()->request->getQuery('id') == Yii::app()->user->id){
        $referals = Referral::model()->findAllByAttributes(array('idReferal'=>Yii::app()->user->id));
        if($referals){ ?>
            <br>
            Сетка реферальных отчислений:
            <table class="timetable">
                <tr>
                    <th>Имя пользователя</th>
                    <th>Отчисления</th>
                </tr>
                <?php
                foreach($referals as $referal){
                    echo '<tr><th class="weekend">' . User::model()->findByPk($referal->idReferal)->name . '</th><th class="weekend">'.$referal->money.'</th></tr>';
                }
                ?>
            </table>
        <?php } } ?>
</div>
<div id="moreInfoAboutUser" class="col-md-8">
    <div class="row">
        <div class="col-md-11">
            <div class="userInformationHead2">
                <?= $user->name; ?>
            </div>
        </div>
        <?php if($curProfileId == Yii::app()->user->id){ ?>
            <div class="col-md-1">
                <a href="<?php echo Yii::app()->request->baseUrl . '/edit/' . User::model()->findByPk(Yii::app()->user->id)->type . '/' . Yii::app()->user->id; ?>"><i class="fa fa-pencil-square-o"></i></a>
            </div>
        <?php } ?>
    </div>

    <?php
    $social = Social::model()->findByPk(Yii::app()->getRequest()->getParam('id'));
    $flagSocial = false;
    if(isset($social)) {
        foreach ($social as $key => $value) {
            if ($key != 'idUser')
                if (isset($value) && ($value != '')) {
                    $flagSocial = true;
                    break;
                }
        }
    }

    if($flagSocial == true){
        ?>
        <div id="profile-social-networks" class="row">
            <div class="col-md-12">Я в соц. сетях
                <?php
                foreach($social as $key => $value){
                    if(($key != 'idUser')&&($value != ''))
                        echo '<a href="' . Yii::app()->params['socialNetworks'][$key] . $value . '" target="_blank"><span class="profile-social-icons" style="background: url(/assets/images/icon_'.$key.'.png)"></span></a>';
                }
                ?>
            </div>
        </div>
    <?php } ?>

    <div class="row indent">
        <div class="col-md-12">
            <?php if (isset($clinic)) {
                $station = Station::model()->findByAttributes(array('idUser' => $clinic->idUser));
                if ($station->name != 'Нет метро') {
                    ?>
                    <div class="searchOfMetro iconMetro2">М</div>
                    <span class="userInformationHead1" style="color:#ff6a00;"><?= $station->name; ?></span>
                    <span class="userInformationHead1 text-black" style="margin-left:10px;">г.<?= $station->city; ?></span>
                    <a href="/site/inProcess" class="showOnMap userInformationText">показать на карте</a>
                <?php }
            } ?>
        </div>
    </div>
    <div class="row indent">
        <div class="col-md-12">
            <div class="userInformationHead1 text-black">
                <?= $clinic->description; ?>
            </div>
        </div>
    </div>

    <?php
    $criteria = new CDbCriteria;
    $criteria->order = 'time DESC';
    $review = Review::model()->findByAttributes(array('idUserTo' => $clinic->idUser, 'moderate' => 1), $criteria);
    $reviewCount = Review::model()->count('idUserTo = :idUser AND moderate=1', array('idUser' => $clinic->idUser)) - 1;
    ?>
    <?php
    if ($review) {
        ?>
        <?php if($reviewCount > 0) { ?>
            <div class="row indent">
                <div class="col-md-3">
                    <div class="button1" onclick="showReviews(<?= $clinic->idUser; ?>)" style="width:100%;background-color:#207CAE;">ОТЗЫВЫ</div>
                </div>
            </div>
        <?php
        }
        $reviewDate = $review->time;
        $reviewSeconds = strtotime($reviewDate);
        $reviewYear = date('Y', $reviewSeconds);
        $reviewMonth = date('M', $reviewSeconds);
        $reviewDay = date('d', $reviewSeconds);
        switch ($reviewMonth) {
            case 'Jan':
                $reviewMonth = 'января';
                break;
            case 'Feb':
                $reviewMonth = 'февраля';
                break;
            case 'Mar':
                $reviewMonth = 'марта';
                break;
            case 'Apr':
                $reviewMonth = 'апреля';
                break;
            case 'May':
                $reviewMonth = 'мая';
                break;
            case 'Jun':
                $reviewMonth = 'июня';
                break;
            case 'Jul':
                $reviewMonth = 'июля';
                break;
            case 'Aug':
                $reviewMonth = 'августа';
                break;
            case 'Sep':
                $reviewMonth = 'сентября';
                break;
            case 'Oct':
                $reviewMonth = 'октября';
                break;
            case 'Nov':
                $reviewMonth = 'ноября';
                break;
            case 'Dec':
                $reviewMonth = 'декабря';
                break;
        }
        $totalMark = round(($review->cure + $review->regard + $review->qualification) / 3);
        switch ($totalMark) {
            case 1:
                $totalMark = 'Неуд';
                break;
            case 2:
                $totalMark = 'Удовл';
                break;
            case 3:
                $totalMark = 'Средне';
                break;
            case 4:
                $totalMark = 'Хорошо';
                break;
            case 5:
                $totalMark = 'Отлично';
                break;
        }
        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="review">
                    <div class="row indent">
                        <div class="col-md-3 col-xs-3">
                            <div
                                style="color: #FF6A00;font-family: MyriadCondBold;font-size: 20px;"><?= $totalMark; ?></div>
                        </div>
                        <div class="col-md-3 col-xs-3">
                            <div class="reviewText">
                                Лечение
                                <div class="reviewStars">
                                    <?php
                                    for ($i = 0; $i < $review->cure; $i++) { ?>
                                        <img src=<?= Yii::app()->request->baseUrl . '/assets/images/star1.png' ?>>
                                    <?php } ?>
                                    <?php
                                    for ($i = 0; $i < 5 - $review->cure; $i++) { ?>
                                        <img src=<?= Yii::app()->request->baseUrl . '/assets/images/star2.png' ?>>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-xs-3">
                            <div class="reviewText">
                                Отношение
                                <div class="reviewStars">
                                    <?php
                                    for ($i = 0; $i < $review->regard; $i++) { ?>
                                        <img src=<?= Yii::app()->request->baseUrl . '/assets/images/star1.png' ?>>
                                    <?php } ?>
                                    <?php
                                    for ($i = 0; $i < 5 - $review->regard; $i++) { ?>
                                        <img src=<?= Yii::app()->request->baseUrl . '/assets/images/star2.png' ?>>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-xs-3">
                            <div class="reviewText">
                                Квалификация
                                <div class="reviewStars">
                                    <?php
                                    for ($i = 0; $i < $review->qualification; $i++) { ?>
                                        <img src=<?= Yii::app()->request->baseUrl . '/assets/images/star1.png' ?>>
                                    <?php } ?>
                                    <?php
                                    for ($i = 0; $i < 5 - $review->qualification; $i++) { ?>
                                        <img src=<?= Yii::app()->request->baseUrl . '/assets/images/star2.png' ?>>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row indent">
                        <div class="col-md-3"><?= $review->name; ?>
                            <br><?= $reviewDay . ' ' . $reviewMonth . ' ' . $reviewYear; ?></div>
                        <div class="col-md-9"><?= $review->text; ?></div>
                    </div>
                    <div class="row photoes">
                        <div class="col-md-3 col-md-offset-3 col-xs-offset-3 col-xs-3">
                            <?php
                            $pics = explode("_", $review->pics);
                            $image = '';
                            foreach ($pics as $key) {

                                if (substr($key, -5, 1) == '1') {
                                    $image = Yii::app()->request->baseUrl . '/upload/' . $key;
                                }
                                ?>
                            <?php }
                            if (!$image)
                                $image = Yii::app()->request->baseUrl . '/assets/images/blankPhoto1.png';
                            ?>
                            <img src=<?= $image; ?>>
                        </div>
                        <div class="col-md-3 col-xs-3">
                            <?php
                            $image = '';
                            foreach ($pics as $key) {
                                if (substr($key, -5, 1) == '2') {
                                    $image = Yii::app()->request->baseUrl . '/upload/' . $key;
                                }
                                ?>
                            <?php }
                            if (!$image)
                                $image = Yii::app()->request->baseUrl . '/assets/images/blankPhoto1.png';
                            ?>
                            <img src=<?= $image; ?>>
                        </div>
                        <div class="col-md-3 col-xs-3">
                            <?php
                            $image = '';
                            foreach ($pics as $key) {
                                if (substr($key, -5, 1) == '3') {
                                    $image = Yii::app()->request->baseUrl . '/upload/' . $key;
                                }
                                ?>
                            <?php }
                            if (!$image)
                                $image = Yii::app()->request->baseUrl . '/assets/images/blankPhoto1.png';
                            ?>
                            <img src=<?= $image; ?>>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row indent">
            <div class="col-md-6 pull-right">
                <?php
                if ($reviewCount > 0) { ?>
                    <a class="showOnMap moreReviews" onclick="showReviews(<?= $clinic->idUser; ?>)">Показать
                        ещё <?= $reviewCount; ?> отзывов</a>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
    <?php if (!Yii::app()->user->isGuest) { ?>
        <div class="row">
            <div class="col-md-12">
                <div id="leaveReview" class="review">
                    <?php $formReviewClinic = $this->beginWidget("CActiveForm", array(
                        'id' => 'clinic-review',
                        'action' => Yii::app()->request->baseUrl . '/site/clinic/'.$clinic->idUser,
                        'method' => 'post',
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => true,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                            'validateOnChange' => true,
                        ),
                        'htmlOptions' => array(
                            'enctype' => 'multipart/form-data',
                            'autocomplete' => 'off',

                        ),
                    )); ?>
                    <div class="row">
                        <div class="text-18 blue col-md-6">Оставить отзыв о враче</div>
                        <div class="col-md-6">
                            <?= CHtml::submitButton('Отправить', array('class' => 'button white bg-orange col-md-6')); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <?= $formReviewClinic->textField($reviewModel, 'name', array('class' => 'fieldInput filedInputReview', 'placeholder' => 'Ваше имя')); ?>
                            <?= $formReviewClinic->error($reviewModel, 'name', array('class' => 'text-red')) ?>

                            <div class="leaveReviewStars cureValue form-group">
                                <img>
                                <img>
                                <img>
                                <img>
                                <img>
                                <span>Эффективнось лечения</span>
                            </div>
                            <div class="leaveReviewStars regardValue form-group">
                                <img>
                                <img>
                                <img>
                                <img>
                                <img>
                                <span>Отношение</span>
                            </div>
                            <div class="leaveReviewStars qualificationValue form-group">
                                <img>
                                <img>
                                <img>
                                <img>
                                <img>
                                <span>Квалификация</span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <?= $formReviewClinic->textArea($reviewModel, 'text', array('class' => 'fieldInput leaveReviewText', 'placeholder' => 'Введите текст')); ?>
                            <?= $formReviewClinic->error($reviewModel, 'text', array('class' => 'text-red')) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">Здесь Вы можете дополнить отзыв своими фотографиями клиники</div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-xs-4">
                            <div id="image" class="fieldInput m-auto">
                                <input type="file" name="image1">
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-4">
                            <div id="image" class="fieldInput m-auto">
                                <input type="file" class="fieldInput" name="image2">
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-4">
                            <div id="image" class="fieldInput m-auto">
                                <input type="file" class="fieldInput" name="image3">
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <?= $formReviewClinic->hiddenField($reviewModel, 'pics', array('class' => 'fieldInput')); ?>
                            <?= $formReviewClinic->error($reviewModel, 'pics', array('class' => 'text-red')); ?>
                        </div>
                    </div>
                    <?= $formReviewClinic->hiddenField($reviewModel, 'cure'); ?>
                    <?= $formReviewClinic->error($reviewModel, 'cure', array('class' => 'text-red')) ?>
                    <?= $formReviewClinic->hiddenField($reviewModel, 'regard'); ?>
                    <?= $formReviewClinic->error($reviewModel, 'regard', array('class' => 'text-red')) ?>
                    <?= $formReviewClinic->hiddenField($reviewModel, 'qualification'); ?>
                    <?= $formReviewClinic->error($reviewModel, 'qualification', array('class' => 'text-red')) ?>
                    <?= $formReviewClinic->hiddenField($reviewModel, 'idUserTo', array('value' => $clinic->idUser)); ?>
                    <?php $this->endWidget(); ?>
                    <a href="/tickets/new" class="showOnMap moreReviews"
                       style="font-size:16px;float:none;position:relative;display:block;margin-left:0px;">Пожаловаться
                        Администратору</a>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
</div>
<div id="overlayReviews" class="overlay">
    <div id="modalReviews"></div>
</div>
<script type="text/javascript">
    function goToPay(){
        var fixPath = (location.href.indexOf('localhost') != -1)?'/medportal':'';
        location.href = location.origin + fixPath + '/pay/payment';
    }

    $('.cureValue img').mouseover(function () {
        var index = $(this).index();
        var COUNT_OF_STARS = 5;
        for (var i = 0; i <= COUNT_OF_STARS - 1; i++) {
            $('.cureValue img').eq(i).removeClass('activeStar');
        }
        for (var i = 0; i <= index; i++) {
            $('.cureValue img').eq(i).addClass('activeStar');
        }
        $('#Review_cure').val(index + 1);
    });
    $('.regardValue img').mouseover(function () {
        var index = $(this).index();
        var COUNT_OF_STARS = 5;
        for (var i = 0; i <= COUNT_OF_STARS - 1; i++) {
            $('.regardValue img').eq(i).removeClass('activeStar');
        }
        for (var i = 0; i <= index; i++) {
            $('.regardValue img').eq(i).addClass('activeStar');
        }
        $('#Review_regard').val(index + 1);
    });
    $('.qualificationValue img').mouseover(function () {
        var index = $(this).index();
        var COUNT_OF_STARS = 5;
        for (var i = 0; i <= COUNT_OF_STARS - 1; i++) {
            $('.qualificationValue img').eq(i).removeClass('activeStar');
        }
        for (var i = 0; i <= index; i++) {
            $('.qualificationValue img').eq(i).addClass('activeStar');
        }
        $('#Review_qualification').val(index + 1);
    });
    $('#Review_cure').val('');
    $('#Review_regard').val('');
    $('#Review_qualification').val('');
    $('#Review_name').val('');
    $('#Review_text').val('');
    $('#Review_pics').val('');
    function showReviews(idUser) {
        var fixPath = '';
        if (location.host.indexOf('localhost') != -1)
            fixPath = '/med'
        $('#preload').css('display', 'block');
        $.ajax({
            url: 'http://' + location.host + fixPath + '/site/reviews',
            method: 'POST',
            data: 'idUser=' + idUser,
            success: function (data) {
                var reviews = JSON.parse(data);
                console.log(reviews);
                var htmlReviews = '';
                for (var i = 0; i < reviews.length; i++) {
                    var mark = Math.round((parseInt(reviews[i].cure) + parseInt(reviews[i].regard) + parseInt(reviews[i].qualification)) / 3);
                    console.log(mark);
                    switch (mark) {
                        case 1 :
                            mark = "Неуд";
                            break;
                        case 2 :
                            mark = "Удовл";
                            break;
                        case 3 :
                            mark = "Средне";
                            break;
                        case 4 :
                            mark = "Хорошо";
                            break;
                        case 5 :
                            mark = "Отлично";
                            break;
                    }
                    var time = reviews[i].time;
                    var month = new Date(time * 1000).getMonth() + 1;
                    switch (month) {
                        case 1 :
                            month = "января";
                            break;
                        case 2 :
                            month = "февраля";
                            break;
                        case 3 :
                            month = "марта";
                            break;
                        case 4 :
                            month = "апреля";
                            break;
                        case 5 :
                            month = "мая";
                            break;
                        case 6 :
                            month = "июня";
                            break;
                        case 7 :
                            month = "июля";
                            break;
                        case 8 :
                            month = "августа";
                            break;
                        case 9 :
                            month = "сентября";
                            break;
                        case 10 :
                            month = "октября";
                            break;
                        case 11 :
                            month = "ноября";
                            break;
                        case 12 :
                            month = "декабря";
                            break;
                    }
                    var pics = reviews[i].pics.split('_');
                    htmlReviews += '<div class="review row indent"><div class="row"><div class="col-md-3 col-xs-3"><div style="color: #FF6A00;font-family: MyriadCondBold;font-size: 20px;">';
                    htmlReviews += mark;
                    htmlReviews += '</div></div>';
                    htmlReviews += '<div class="col-md-3 col-xs-3"><div class="userInformationText2 reviewText">Лечение<div class="reviewStars">';
                    for (var j = 0; j < reviews[i].cure; j++) {
                        htmlReviews += '<img src="' + fixPath + '/assets/images/star1.png">';
                    }
                    for (var j = 0; j < 5 - reviews[i].cure; j++) {
                        htmlReviews += '<img src="' + fixPath + '/assets/images/star2.png">';
                    }
                    htmlReviews += '</div></div></div>';
                    htmlReviews += '<div class="col-md-3 col-xs-3"><div class="userInformationText2 reviewText">Отношение<div class="reviewStars">';
                    for (var j = 0; j < reviews[i].regard; j++) {
                        htmlReviews += '<img src="' + fixPath + '/assets/images/star1.png">';
                    }
                    for (var j = 0; j < 5 - reviews[i].regard; j++) {
                        htmlReviews += '<img src="' + fixPath + '/assets/images/star2.png">';
                    }
                    htmlReviews += '</div></div></div>';
                    htmlReviews += '<div class="col-md-3 col-xs-3"><div class="userInformationText2 reviewText">Квалификация<div class="reviewStars">';
                    for (var j = 0; j < reviews[i].qualification; j++) {
                        htmlReviews += '<img src="' + fixPath + '/assets/images/star1.png">';
                    }
                    for (var j = 0; j < 5 - reviews[i].qualification; j++) {
                        htmlReviews += '<img src="' + fixPath + '/assets/images/star2.png">';
                    }
                    htmlReviews += '</div></div></div></div>';
                    htmlReviews += '<div class="row form-group"><div class="col-md-3">' + reviews[i].name + '<br>' + new Date(time * 1000).getDate() + ' ' + month + ' ' + new Date(time * 1000).getFullYear() + '</div><div class="col-md-9">' + reviews[i].text + '</div>';
                    htmlReviews += '</div><div class="row photoes">';
                    var image = '';
                    for (var j = 0; j < pics.length; j++) {
                        if (pics[j].substr(-5, 1) == '1')
                            image = pics[j];
                    }
                    if (image)
                        htmlReviews += '<div class="col-md-4 col-xs-4"><img src="/upload/' + image + '"></div>';
                    else
                        htmlReviews += '<div class="col-md-4 col-xs-4"><img src="/assets/images/blankPhoto1.png"></div>';
                    var image = '';
                    for (var j = 0; j < pics.length; j++) {
                        if (pics[j].substr(-5, 1) == '2')
                            image = pics[j];
                    }
                    if (image)
                        htmlReviews += '<div class="col-md-4 col-xs-4"><img src="/upload/' + image + '"></div>';
                    else
                        htmlReviews += '<div class="col-md-4 col-xs-4"><img src="/assets/images/blankPhoto1.png"></div>';
                    var image = '';
                    for (var j = 0; j < pics.length; j++) {
                        if (pics[j].substr(-5, 1) == '3')
                            image = pics[j];
                    }
                    if (image)
                        htmlReviews += '<div class="col-md-4 col-xs-4"><img src="/upload/' + image + '"></div>';
                    else
                        htmlReviews += '<div class="col-md-4 col-xs-4"><img src="/assets/images/blankPhoto1.png"></div>';
                    htmlReviews += '</div>';
                    htmlReviews += '<div></div></div>';
                }
                htmlReviews += '<div class="row" style="text-align:center;"><div class="button1 lr-paddings bg-orange" onclick="$(\'body\').css(\'overflow\',\'auto\');$(\'#overlayReviews\').css({\'display\':\'none\',\'overflow\':\'hidden\'})">Закрыть</div></div>';
                $('#modalReviews').html(htmlReviews);
            },
            complete: function () {
                $('#preload').css('display', 'none');
                $('#overlayReviews').css({'display': 'block', 'overflow': 'auto'});
                $('body').css({'overflow': 'hidden'});
            }
        });
    }

    $('#button-make-vip').click(function(){
        $('#overlay-make-vip').css('display','block');
    });
    $('.close-button').click(function(){
        $('#overlay-make-vip').css('display','none');
    });
</script>
<script>
    $('.button1.middle.bg-redColor').click(function(){
        $('#modalClinicDoctors').css('display','block');
        $('#overlayModalClinicDoctors').css('display','block');
    });
    $('#overlayModalClinicDoctors').click(function(){
        $('#overlayModalClinicDoctors').css('display','none');
    });
</script>