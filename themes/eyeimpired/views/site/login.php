<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 14.09.2015
 * Time: 13:04
 */
?>
<div class="row">
    <div class="col-md-11 col-md-push-1 col-xs-11 col-xs-push-1">
        <h1 style="cursor:default;text-decoration:none;"><b>Личный кабинет:</b></h1>
    </div>
</div>
<? $form = $this->beginWidget('CActiveForm', array(
    'id' => 'login-form',
    'action' => Yii::app()->request->baseUrl . '/site/login',
    'method' => 'post',
    'enableAjaxValidation' => true,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmmit' => true,
        'validateOnChange' => true
    )
)) ?>
<div class="row indent main-menu" style="margin:25px auto 15px;">
    <div class="col-md-4 col-xs-4">
        <a><button type="button" class="btn btn-default search-btn btn-xs-margin-left"><i class="fa fa-caret-right" style="left:20px;"></i>Авторизация</button></a>
    </div>
</div>
<div class="row">
    <div class="col-md-7 col-xs-7 no-padding" style="left:6%;">
        <div id="error" class="text-red form-group hidden"></div>
        <div style="position:relative;margin-bottom:-10px;">
            <span class="input-label"></span>
            <!--<input class="main-input" type="input" name="doctor" style="padding-left:130px;width:100%!important;">-->
            <? $this->widget('CMaskedTextField', array(
                'model' => $model,
                'attribute' => 'phone',
                'mask' => '+99999999999',
                'placeholder' => '*',
                'htmlOptions' => array(
                    'class' => 'main-input',
                    'placeholder' => 'Телефон*:',
                    'style'=>'padding-left:18px;width:100%!important;'
                )
            )); ?>
            <?= $form->error($model, 'phone') ?>
        </div>
        <div style="position:relative;margin-bottom:-10px;">
            <!--<input class="main-input" type="input" name="doctor" style="padding-left:130px;width:100%!important;">-->
            <?= $form->passwordField($model, 'password', array('class' => 'main-input', 'placeholder' => 'Пароль*:', 'style' => 'padding-left:18px;width:100%!important;')) ?>
            <?= $form->error($model, 'password') ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-7 col-xs-7 login-buttons no-padding" style="left:6%;">
        <div style="position:relative;margin-bottom:-10px;">
            <div class="main-input" type="input" name="doctor" style="width:100%;padding:0px;border:none;margin-top:20px;">
                <!--<button type="button" class="btn btn-default search-btn btn-xs-margin-left" style="width:37%!important;"><i class="fa fa-caret-right" style="left:20px;"></i>Войти</button>-->
                <?= CHtml::ajaxSubmitButton('Войти', Yii::app()->request->baseUrl . '/site/login', array(
                    'success' => 'function(data){
                    if(/Ошибка авторизации/.test(data)) {
                        $("#error").removeClass("hidden");
                        $("#error").html(data);
                    } else if(data == 1) {
                        location.href = "' . Yii::app()->request->getBaseUrl(true) . '";
                    }
                }'
                ), array(
                    'class' => 'btn btn-default search-btn',
                    'style' => 'line-height:1!important;border-radius:5px!important;font-size:20pt!important;color:#FFF!important;background-color:#000;border:none;'
                )) ?>
                <button type="button" class="btn btn-default search-btn btn-xs-margin-left" style="float:right;border:1px solid #000!important;">Забыли пароль</button>
            </div>
        </div>
    </div>
</div>
<? $this->endWidget(); ?>