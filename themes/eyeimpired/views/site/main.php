<?php
if (isset($systemMsg)) { ?>
    <script>
        $(document).ready(function() {
            $('#myModalLabel').html('Регистрация');
            $('#modal-body').html('<?= $systemMsg; ?>');
            $('#myModal').modal();
        })
    </script>
<? } ?>

<div class="row">
    <div class="col-md-11 col-md-push-1 col-xs-11 col-xs-push-1">
        <h1 class="middleFontSize" style="cursor:default;font-weight:700;">Для пациентов:</h1>
        <ul class="middleFontSize">
            <li>1 - Вы можете найти врача и клинику</li>
            <li>2 - Ознакомиться с рейтингом врача и узнать стоимость приёма</li>
            <li>3 - Выбрать врача по параметрам (рейтинг, отзывы, цена приёма)</li>
            <li>4 - Быстро записаться на приём</li>
            <li>5 - Оставить отзыв о приёме</li>
        </ul>
    </div>
</div>
<div class="row indent main-menu" style="margin:20px auto 30px;">
    <div class="col-md-4 col-xs-4">
        <a href="/site/inProcess"><button type="button" class="btn btn-default search-btn middleFontSize btn-xs-margin-left"><i class="fa fa-caret-right" style="left:20px;"></i>Записаться</button></a>
    </div>
    <div class="col-md-4 col-xs-4">
        <a href="/site/inProcess"><button type="button" class="btn btn-default search-btn middleFontSize white-btn"><i class="fa fa-caret-right" style="left:20px;"></i>Регистрация</button></a>
    </div>
    <div class="col-md-4">
    </div>
</div>
<div class="row">
    <div class="col-md-11" style="left:3%;">
        <h1 class="middleFontSize"><img src="/assets/images/medIcon1_bw.png"><a href="/site/inProcess" style="font-weight:700;">Для инвалидов</a></h1>
        <h1 class="middleFontSize"><img src="/assets/images/medIcon2_bw.png"><a href="/site/inProcess" style="font-weight:700;">Транспортировка больных</a></h1>
        <h1 class="middleFontSize"><img src="/assets/images/medIcon3_bw.png"><a href="/site/inProcess" style="font-weight:700;">Помощь на дому</a></h1>
    </div>
</div>