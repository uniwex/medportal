<div class="row">
    <div class="col-md-11 col-md-push-1 col-xs-11 col-xs-push-1">
        <h1 style="cursor:default;text-decoration:none;">Регистрация:</h1>
    </div>
</div>
    <!------------------------------------------------------------------>
    <!------------------------------------------------------------------>
    <?php
    $formPatient = $this->beginWidget("CActiveForm", array(
        'id' => 'patient-registration',
        'action' => Yii::app()->request->baseUrl . '/site/regUser',
        'method' => 'post',
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
        ),
        'htmlOptions' => array(
            'autocomplete' => 'off'
        ),
    )); ?>
    <!--<span class="displayPatient">-->
        <div class="form-group">
            <?= $formPatient->errorSummary($userModel, 'Пожалуйста, исправьте следующие ошибки:', '', array('class' => 'text-red')) ?>
            <?= $formPatient->errorSummary($patientModel, '', '', array('class' => 'text-red')) ?>
            <?= $formPatient->errorSummary($stationModel, '', '', array('class' => 'text-red')) ?>
        </div>
        <!--
        <div class="form-group row">
            <div class="col-md-8">
                <? //echo $formPatient->radioButtonList($userModel, 'type', array('0' => 'Пациент', '1' => 'Клиника', '2' => 'Сиделка', '3' => 'Частный врач'), array('onclick' => 'switchUserType(this)', 'data-ut' => 'patient')); ?>
                <? //echo $formPatient->error($userModel, 'type', array('class' => 'text-red')) ?>
            </div>
        </div>
        -->
        <div class="row">
            <div class="col-md-7 col-xs-7" style="left:6%;">
                <div style="margin-bottom:15px;">
                    <?= $formPatient->textField($userModel, 'name', array('class' => 'main-input', 'placeholder' => 'Ваше имя, фамилия, отчество*:', 'style' => 'width:100%;margin-bottom:0px;')); ?>
                    <?= $formPatient->error($userModel, 'name', array('class' => 'text-red')) ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7 col-xs-7" style="left:6%;">
                <div style="margin-bottom:15px;">
                    <?= $formPatient->textField($userModel, 'email', array('class' => 'main-input', 'placeholder' => 'Ваш e-mail*:', 'style' => 'width:100%;margin-bottom:0px;')); ?>
                    <?= $formPatient->error($userModel, 'email', array('class' => 'text-red')) ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7 col-xs-7" style="left:6%;">
                <div style="margin-bottom:15px;">
                    <?= $formPatient->passwordField($userModel, 'password', array('class' => 'main-input', 'placeholder' => 'Пароль*:', 'style' => 'width:100%;margin-bottom:0px;')); ?>
                    <?= $formPatient->error($userModel, 'password', array('class' => 'text-red')) ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7 col-xs-7" style="left:6%;">
                <div style="margin-bottom:15px;">
                    <?= $formPatient->textField($userModel, 'phone', array('class' => 'main-input', 'placeholder' => 'Телефон*:', 'style' => 'width:100%;margin-bottom:0px;')); ?>
                    <?= $formPatient->error($userModel, 'phone', array('class' => 'text-red')) ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7 col-xs-7" style="left:6%;">
                <div style="margin-bottom:15px;">
                    <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'name' => 'birthday',
                        'model' => $patientModel,
                        'attribute' => 'birthday',
                        'language' => 'ru',
                        'options' => array(
                            'dateFormat' => 'yy-mm-dd',
                            'class' => 'main-input',
                            'showAnim' => 'fold',
                            'style' => 'width:100%;margin-bottom:0px;',
                            'placeholder' => 'Ваша дата рождения*:'
                        ),
                        'htmlOptions' => array(
                            'class' => 'main-input',
                            'id' => 'Patient_birthday',
                            'style' => 'width:100%;margin-bottom:0px;',
                            'placeholder' => 'Ваша дата рождения*:'
                        ),
                    )); ?>
                    <?= $formPatient->error($patientModel, 'birthday', array('class' => 'text-red')); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7 col-xs-7" style="left:6%;">
                <div style="margin-bottom:15px;">
                    <?= $formPatient->dropDownList($userModel, 'country', array(0 => 'Выберите страну', 1 => 'Россия'), array('class' => 'main-input', 'data-alias' => 'country', 'style' => 'width:100%;margin-bottom:0px;')); ?>
                    <?= $formPatient->error($userModel, 'country', array('class' => 'text-red')) ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7 col-xs-7" style="left:6%;">
                <div style="margin-bottom:15px;">
                    <?= $formPatient->textField($userModel, 'city', array('class' => 'main-input inputForSearchCity', 'style' => 'width:100%;margin-bottom:0px;', 'placeholder' => 'Выберите город')); ?>
                    <?= $formPatient->error($userModel, 'city', array('class' => 'text-red')) ?>
                    <select class="dropDownCities" type="select" size="5" data-alias="city" style="width:100%;background:none;">
                        <option>Выберите город</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7 col-xs-7" style="left:6%;">
                <div style="margin-bottom:15px;">
                    <?= $formPatient->dropDownList($stationModel, 'name', array('0' => 'Выберите станцию метро', 'Нет метро' => 'Нет метро'), array('class' => 'main-input', 'data-alias' => 'metro', 'multiple' => 'multiple', 'style' => 'width:100%;background:none;margin-bottom:0px;')); ?>
                    <?= $formPatient->error($stationModel, 'name', array('class' => 'text-red')) ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7 col-xs-7" style="left:6%;">
                <div style="margin-bottom:15px;">
                    <?= $formPatient->textField($patientModel, 'oms', array('class' => 'main-input', 'style' => 'width:100%;margin-bottom:0px;', 'placeholder' => 'ОМС:')); ?>
                    <?= $formPatient->error($patientModel, 'oms', array('class' => 'text-red')) ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7 col-xs-7" style="left:6%;">
                <div style="margin-bottom:15px;">
                    <?= $formPatient->textField($patientModel, 'dms', array('class' => 'main-input', 'style' => 'width:100%;margin-bottom:0px;', 'placeholder' => 'ДМС:')); ?>
                    <?= $formPatient->error($patientModel, 'dms', array('class' => 'text-red')) ?>
                </div>
            </div>
        </div>

        <div class="row indent main-menu" style="margin:25px auto 15px;">
            <div class="col-md-8 col-xs-10">
                <div style="margin-left:44px;text-align:left;font-size:20pt;margin-bottom:10px;padding-left:26px;">На Ваш электронный ящик придёт уведомление о регистрации на сайте</div>
                <a>
                    <button type="submit" name="yt0" class="btn btn-default search-btn btn-xs-margin-left registration-button"><i class="fa fa-caret-right" style="left:20px;line-height:1.5;"></i>Зарегистрироваться</button>
                </a>
            </div>
        </div>

    <!--</span>-->
    <?php $this->endWidget(); ?>

<link rel="stylesheet" href="<?= Yii::app()->request->getBaseUrl(true); ?>/assets/admin/bootstrap/css/bootstrap-multiselect.css">
<script src="<?= Yii::app()->request->getBaseUrl(true); ?>/assets/admin/bootstrap/js/bootstrap-multiselect.js"></script>
<script src="//cdn.ckeditor.com/4.5.4/standard/ckeditor.js"></script>
<script type="text/javascript">
    window.onload = function () {
        var inputs = document.querySelectorAll('#user-registration input')
        for (i = 0; i < inputs.length; i++) {
            if ((inputs[i].className.indexOf('button') == -1) && (inputs[i].type != 'radio'))
                inputs[i].value = '';
        }
        /*$('[data-alias=metro]').multiselect({
            includeSelectAllOption: true
        });*/
    }
</script>
