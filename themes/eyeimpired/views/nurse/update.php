<?php
/* @var $this NurseController */
/* @var $model Nurse */

/*$this->breadcrumbs=array(
	'Nurses'=>array('index'),
	$model->name=>array('view','id'=>$model->idUser),
	'Update',
);*/
?>
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Изменение сиделки <?= $model->name; ?></h3>
            </div>
            <?php $this->renderPartial('_form', array('model' => $model)); ?>
        </div>
    </div>
</div>

