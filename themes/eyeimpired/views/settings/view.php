<?php
/* @var $this SettingsController */
/* @var $model PartnerPercent */
?>

<section class="invoice no-margin">
    <div class="row">
        <div class="col-md-12">
            <h2 class="page-header">Просмотр записи #<?php echo $model->id; ?></h2>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?php $this->widget('zii.widgets.CDetailView', array(
                'data' => $model,
                'htmlOptions' => array(
                    'class' => 'table table-striped'
                ),
                'attributes' => array(
                    'id',
                    'count',
                    'percent',
                ),
            )); ?>
        </div>
    </div>
</section>
