<?php
/* @var $this ClaimController */
/* @var $tickets Ticket */
/* @var $ticket Ticket */
/* @var $model Ticket */
/* @var $reply TicketMessages */

$script = <<<JS
    function(data){
        $('#TicketMessages_message').val('');
        data = JSON.parse(data);
        if(data.status == 'success') {
            $.fn.yiiListView.update('grid', {
                url: '/claim/getMail',
                data: {
                    id: data.id
                }
            })
        }
    }
JS;
?>
<?if (User::isAdmin()) {?>
    <div class="row">
        <div class="col-md-12">
            <? $flash = Yii::app()->user->getFlashes(); ?>
            <?
            $icon = array(
                'success' => 'fa-check',
                'warning' => 'fa-warning',
                'info' => 'fa-info',
                'danger' => 'fa-ban'
            );
            ?>
            <? if ($flash): ?>
                <? foreach ($flash as $key => $value) : ?>
                    <div class="alert alert-<?= $key ?> alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <i class="icon fa <?= $icon[$key] ?>"></i> <?= $value ?>
                    </div>
                <? endforeach; ?>
            <? endif; ?>

            <div class="box box-info">
                <div class="box-header with-border ui-sortable-handle">
                    <h3 class="box-title">Список тикетов</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove">
                            <i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <? $grid = $this->widget('zii.widgets.grid.CGridView', array(
                        'dataProvider' => $model->search(),
                        'filter' => $model,
                        'cssFile' => Yii::app()->request->getBaseUrl(true) . '/assets/admin/css/gridview.css',
                        'itemsCssClass' => 'table no-margin',
                        'htmlOptions' => array(
                            'class' => 'table-responsive'
                        ),
                        'afterAjaxUpdate' => "function() {
                            jQuery('[name=\"Ticket[date]\"]').daterangepicker();
                        }",
                        'summaryText' => 'Показано записей: {start} - {end} из {count}',
                        'emptyText' => 'Результаты не найдены',
                        'columns' => array(
                            'id',
                            'date',
                            array(
                                'name' => 'idUser',
                                'filter' => CHtml::listData(
                                    $tickets,
                                    'idUser',
                                    function ($ticket) {
                                        if (isset($ticket->user->name)) {
                                            return $ticket->user->name;
                                        }
                                    }),
                                'value' => ' User::getName($data->idUser)'
                            ),
                            array(
                                'name' => 'type',
                                'filter' => array('' => 'Любой', 'Вопрос' => 'Вопрос', 'Жалоба' => 'Жалоба'),
                                'value' => 'CHtml::label("$data->type", "", array("class" => $data->type == "Жалоба" ? "label label-warning" : "label label-info"))',
                                'type' => 'raw'
                            ),
                            array(
                                'name' => 'subject',
                                'value' => 'CHtml::link($data->subject, "", array(
                                    "data-toggle" => "modal",
                                    "data-target" => "#myModal",
                                    "class" => "grid-ref",
                                    "data-id" => $data->id,
                                    "onclick" => "viewTicket(this)",
                                ))',
                                'type' => 'raw',
                                'htmlOptions' => array(
                                    "data-html" => true,
                                    "data-trigger" => "",
                                    "data-toggle" => "popover",
                                    "data-container" => "body",
                                    "data-placement" => "left",
                                    "data-content" => "
                                    <div class=\"row\">
                                        <div class=\"col-md-10\">
                                            <p>Нажмите на тему тикета для просмотра истории сообщений</p><a name=\"do-not-show\">Не показывать снова</a>
                                        </div>
                                        <div class=\"col-md-2\">
                                            <i class=\"fa fa-close\" data-dismiss=\"popover\"></i>
                                        </div>
                                    </div>"
                                )
                            ),
                            array(
                                'name' => 'status',
                                'filter' => Yii::app()->params['statusModer'],
                                'value' => 'CHtml::label(Yii::app()->params["statusModer"]["$data->status"], "", array("class" => Yii::app()->params["statusLabel"]["$data->status"]))',
                                'type' => 'raw'
                            ),
                            array(
                                'class' => 'CButtonColumn',
                                'header' => 'Закрыть тикет',
                                'template' => '{close}',
                                'buttons' => array(
                                    'close' => array(
                                        'label' => 'Закрыть',
                                        'url' => 'Yii::app()->createUrl("/claim/close", array("id" => $data->id))',
                                        'options' => array('class' => 'aclose'),
                                        'imageUrl' => Yii::app()->getRequest()->getBaseUrl(true) . "/assets/admin/img/delete.png",
                                    ),
                                ),
                            )
                        ),
                        'pager' => array(
                            'class' => 'CLinkPager',
                            'header' => '',
                            'nextPageLabel' => 'Следующая',
                            'prevPageLabel' => 'Предыдущая'
                        )
                    )); ?>
                </div>
                <div class="box-footer clearfix"></div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">История сообщений</h4>
                </div>
                <div class="modal-body">
                    <div id="preloader" class="text-center" style="display: none">
                        <img src="<?= Yii::app()->request->getBaseUrl(true) ?>/assets/admin/img/preloader.gif" alt="">
                    </div>
                    <? $this->renderPartial('list', array('dataProvider' => $dataProvider), false, false); ?>
                </div>
                <div class="modal-footer">
                    <? $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'ticketMessage',
                        'action' => '/claim/reply',
                        'enableClientValidation' => true,
                        'enableAjaxValidation' => false,
                        'errorMessageCssClass' => 'control-label',
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                            'validateOnChange' => true,
                            'errorCssClass' => 'has-error',
                            'successCssClass' => 'has-success'
                        )
                    )) ?>
                    <div class="input-group">
                        <?= $form->hiddenField($reply, 'idTicket') ?>
                        <?= $form->textField($reply, 'message', array('class' => 'form-control', 'placeholder' => 'Введите сообщение')) ?>
                        <span class="input-group-btn">
                            <?= CHtml::ajaxSubmitButton(
                                'Отправить',
                                array('/claim/reply'),
                                array('success' => $script, 'async' => false),
                                array(
                                    'class' => 'btn btn-primary btn-flat send'
                                )
                            ) ?>
                        </span>
                    </div>
                    <div class="has-error pull-left">
                        <?= $form->error($reply, 'message') ?>
                    </div>
                    <? $this->endWidget() ?>
                </div>
            </div>
        </div>
    </div>
<? } else $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/login'); ?>
    <script>
        function viewTicket(el) {
            var id = $(el).data('id');
            $('#TicketMessages_idTicket').val(id);
            $('#TicketMessages_message').val('');
            $.fn.yiiListView.update('grid', {
                url: '/claim/getMail',
                data: {
                    id: id
                }
            })
        }
    </script>

<?
$script = <<<JS
    var po = $('[data-toggle="popover"]'),
        doNotShow = getCookie('donotshow');

    $('[name="Ticket[date]"]').daterangepicker();

    if(!doNotShow) {
        if(po.length) {
            $(po[0]).popover('show');
            setTimeout(function(){
                $(po[0]).popover('hide');
            }, 5000);
        }
    }

    $('i[data-dismiss]').on('click', function(){
        $(po[0]).popover('hide');
    });

    $('a[name="do-not-show"]').on('click', function(){
        var date = new Date();
        date.setMonth(date.getMonth() + 1);
        setCookie('donotshow', 1, date.getFullYear(), date.getMonth(), date.getDate());
        $(po[0]).popover('hide');
    })

JS;
Yii::app()->clientScript->registerScript('date', $script, CClientScript::POS_READY);
?>