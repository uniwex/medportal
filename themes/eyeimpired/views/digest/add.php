<div id="postAdvert" class="col-md-10">
    <?php $formDigest = $this->beginWidget("CActiveForm", array(
        'id' => 'add-digest',
        'action' => Yii::app()->request->baseUrl . '/digest/saveNew',
        'method' => 'post',
//        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true
        ),
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
            'autocomplete' => 'off',
            'onsubmit' => 'crutchForCKEditor();return true',
        ),
    )); ?>
    <div class="form-group row">
        <div class="col-md-2">
            <?= $formDigest->labelEx($digestModel, 'name') ?>
        </div>
        <div class="col-md-8">
            <?= $formDigest->textField($digestModel, 'name', array('class' => 'fieldInput', 'placeholder' => 'Новая статья')); ?>
            <?= $formDigest->error($digestModel, 'name', array('class' => 'text-red')) ?>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-2">
            Категория
        </div>
        <div class="col-md-8">
            <?= $formDigest->dropDownList($categoryModel, 'name', CHtml::listData(Category::model()->findAll(), 'id', 'name', 'category'), array('class' => 'fieldInput', 'multiple' => 'multiple')); ?>
            <?= $formDigest->error($categoryModel, 'name', array('class' => 'text-red')) ?>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-2">
            <?= $formDigest->labelEx($digestModel, 'text') ?>
        </div>
        <div class="col-md-8">
            <?= $formDigest->textArea($digestModel, 'text', array('class' => 'fieldInput')); ?>
            <?= $formDigest->error($digestModel, 'text', array('class' => 'text-red')) ?>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <?= CHtml::submitButton('Сохранить статью', array('class' => 'button1 button1reg', 'id'=>'digestSubmit')); ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
<script src="//cdn.ckeditor.com/4.5.4/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('Digest[text]');
    var crutchForCKEditor = function(){
        var abc = document.getElementsByTagName('iframe')[0].contentDocument;
        var abc2 = abc.childNodes[1];
        var text = abc2.childNodes[1].childNodes[0].innerHTML;
        if(text != '<br>')
            $('#Digest_text').val(text);
    }
</script>