<?php
$testStars = 4;
if ($data['type'] == 'clinic') {
    $work = 'site/clinic';
    $ru = 'Клиника';
}
if ($data['type'] == "doctor") {
    $work = 'site/doctor';
    $ru = 'Доктор';
}
if ($data['type'] == 'nurse') {
    $work = 'site/nurse';
    $ru = 'Сиделка';
}
if ($data['type'] == 'article') {
    $work = 'articles';
    $ru = 'Статья';
}
?>

<div class="row global-search-row <?php if($data['type'] == 'article') echo 'globalSearch-article'; ?>">
    <div class="col-md-2 no-padding">
        <a href="/<?=$work?>/<?= $data['id'] ?>">
            <?php
            $img = null;
            if (!$data['image']) {
                $img = '/assets/images/articles-overlay.png';
            }
            elseif ($data['image'] == ''){
                $img = '/assets/images/articles-overlay.png';
            }
            elseif ($data['image'] == '/upload/'){
                $img = '/assets/images/articles-overlay.png';
            }
            else
                $img = $data['image'];

            ?>
            <div class="pictureBlock" style="background-image: url('<?= $img; ?>');margin-left:-4px;"></div>
        </a>
    </div>
    <div class="col-md-8">
        <a href="/<?=$work?>/<?= $data['id'] ?>">
            <h2><?= $data['name'] ?></h2>
            <small><?=$ru?></small>
            <!--<p>-->
                <?//$data['description']?>
            <!--</p>-->
        </a>
    </div>
    <div class="col-md-2">
        <?='<div class="ratingStarsView form-group" style="margin-top:30px">'.RatingHelper::renderStars($data['rating']).'</div>'?>
    </div>
</div>
