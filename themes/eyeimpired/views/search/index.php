<?
Yii::app()->clientScript->registerScript('search',
    "
$('.clinicDoctor').on('click', function(){
    $('#modalClinicDoctors').css('display','block');
    $('#overlayModalClinicDoctors').css('display','block');
    var clinicId = $(this).attr('data-id');
    $.fn.yiiListView.update('list', {
            url: '" . CController::createUrl('search/getDoctors') . "',
            data: 'clinicId='+clinicId
        }
    );
});
$('#overlayModalClinicDoctors').click(function(){
    $('#overlayModalClinicDoctors').css('display','none');
});
$('.privateDoctor').on('click', function(){
    var docId = $(this).attr('data-id');
    (location.host != 'localhost') ? location.href='/visit?step=5&docId='+docId : location.href='/medportal/visit?step=5&docId='+docId;
});
$('.nurse').on('click', function(){
    var docId = $(this).attr('data-id');
    location.href='/visit?step=9&docId='+docId;
});
");
?>
<?php if($related == 'clinic') { ?>
    <div id="overlayModalClinicDoctors" class="overlay">
        <div id="modalClinicDoctors" style="display:none;">
            <div class="row form-group">
                <? $this->renderPartial('list', array('dataProvider' => $dataProvider), false, true); ?>
            </div>
        </div>
    </div>
<?php } ?>
    <div id="postAdvert" class="col-md-12 no-padding">
        <? //$this->renderPartial('list', array('dataProviderMenuDoctors' => $dataProviderMenuDoctors), false, true); ?>
<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 26.10.2015
 * Time: 11:57
 */
if (isset($dataProvider2))
    $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $dataProvider2,
        'itemView' => '_indexVip',
        'pager' => array(
            'class' => 'CLinkPager',

        ),
        'htmlOptions' => array(
            'class' => 'col-md-12',
            'id' => 'list-view-vip'
        ),
        'summaryText' => 'VIP',
        'summaryCssClass' => 'summary userInformationHead1 no-margin',
        'emptyText' => '',
        'emptyCssClass' => 'empty userInformationHead1 no-margin',
        'viewData' => array(
            'related' => $related
        )
    ));
?>

<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 26.10.2015
 * Time: 11:57
 */
if ($related == 'doctor') { ?>
    <div class="col-md-4">
        <?= CHtml::label('Возрастной прием', 'typeHuman', array('class' => 'middleFontSize')) ?>
        <?= CHtml::dropDownList(
            'typeHuman',
            isset($_POST['type']) ? $_POST['type'] : '',
            array(
                '' => 'Любой',
                'Все' => 'Все',
                'Дети' => 'Дети',
                'Взрослые' => 'Взрослые'
            ),
            array(
                'class' => 'fieldInput'
            )
        ) ?>
    </div>
<? }

$this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_index',
    'pager' => array(
        'class' => 'CLinkPager',
    ),
    'htmlOptions' => array(
        'class' => 'col-md-12',
        'id' => 'list-view'
    ),
    'summaryText' => '<span class="middleFontSize">Найдено записей</span>: {count}',
    'summaryCssClass' => 'summary userInformationHead1 no-margin',
    'emptyText' => 'Записи не найдены',
    'emptyCssClass' => 'empty userInformationHead1 no-margin',
    'viewData' => array(
        'related' => $related
    )
));
?>
</div>