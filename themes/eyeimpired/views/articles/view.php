<div id="postAdvert" class="col-md-10">
    <div class="row form-group">
        <div class="col-md-3">
            <?php
                $image = Yii::app()->request->baseUrl.'/assets/images/articles-overlay.png';
                if($article->image)
                    if($article->image != '')
                        $image = Yii::app()->request->baseUrl.'/upload/articles/' . $article->image;
            ?>
            <div class="article-img" style="height:180px;background-image:url('<?= $image ?>')"></div>
        </div>
        <div class="col-md-9">
            <div class="row form-group">
                <div class="col-md-10 article-author no-padding">
                    <?php
                    $user = User::model()->findByPk($article->idUser);
                    $name = null;
                    if(isset($user)) {
                        if(User::isAdmin())
                            $name = "Администратор";
                        else
                            $name = $user->name;
                    }
                    else
                        $name = '<пользователь удалён>';
                    echo $name;
                    ?>
                </div>
                <div class="col-md-2 text-r">
                    <?= date('d-m-Y',strtotime($article->time)) ?>
                </div>
                <?php
                $articleModel = Article::model()->findByPk($article->id);
                if (!Yii::app()->user->isGuest) {
                    if (($articleModel->idUser == Yii::app()->user->id) || (User::model()->findByPk(Yii::app()->user->id)->role == 'admin')) {
                        ?>
                        <div class="col-md-1 text-r"><a href="/articles/edit/<?= $article->id; ?>"><i
                                    class="fa fa-pencil-square-o"></i></a>
                            <i class="fa fa-times" onclick="removeArticle(this, 0)" data-id="<?= $article->id; ?>"></i>
                        </div>
                    <?php
                    }
                }
                ?>
            </div>
            <div class="row">
                <div class="col-md-12 no-padding">
                    <?php
                        $categories = "";
                        if($article->idCategory) {
                            $article->idCategory = str_replace("@", "", $article->idCategory);
                            foreach (explode(',',$article->idCategory) as $key) {
                                $categories .= Category::model()->findByPk($key)->name . ',';
                            }
                        }
                        $categories = substr($categories,0,-1);
                    ?>
                    Категория: <?= $categories; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 no-padding" style="font-size:18pt;">
                    <?= $article->name ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $article->text ?>
        </div>
    </div>
</div>
<script src="/assets/scripts/articles.js"></script>