<div id="postAdvert" class="col-md-10 no-padding">
    <? if(!Yii::app()->user->IsGuest) { ?>
        <div class="col-md-12">
            <? echo CHtml::link('Добавить статью',array('articles/add') ,
                array(
                    'style' => 'margin-top:27px;display:block;width:200px;text-decoration:none;color:#fff;float:right;text-align:center;',
                    'class' => 'help-home bg-liteBlue'
                )); ?>
        </div>
    <?php } ?>
<?php
$this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_section',
    'pager' => array(
        'class' => 'CLinkPager',

    ),
    'template'=>"{items}\n{pager}",
    'htmlOptions' => array(
        'class' => 'col-md-12',
        'id' => 'list-view-vip'
    ),
    'summaryCssClass' => 'summary userInformationHead1 no-margin',
    'emptyText' => '',
    'emptyCssClass' => 'empty userInformationHead1 no-margin',
));
?>
</div>