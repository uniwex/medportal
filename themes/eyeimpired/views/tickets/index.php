<?
Yii::app()->clientScript->registerScript('search',
    "
$('#Ticket_date').on('keyup', function(){
    $.fn.yiiListView.update('list', {
            url: '" . CController::createUrl('tickets/') . "',
            data: 'date='+$(\"[id=Ticket_date]\").val()
        }
    )
});
$('#Ticket_date').on('change', function(){
    $.fn.yiiListView.update('list', {
            url: '" . CController::createUrl('tickets/') . "',
            data: 'date='+$(\"[id=Ticket_date]\").val()
        }
    )
});
");
?>

<div id="postAdvert" class="col-md-12" data-type="ticket">
    <div class="row">
        <div class="col-md-11 col-md-push-1 col-xs-11 col-xs-push-1">
            <h1 style="cursor:default;text-decoration:none;margin:30px 0px 35px;">Вопрос-ответ:</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-xs-4">
            <div class="button1 search-btn buttonFilter" style="margin-left:30px;" onclick="location.href='/tickets/new'">
                <i class="fa fa-caret-right hidden-xs hidden-sm" style="left:20px;"></i>Задать вопрос
            </div>
        </div>
        <div class="col-md-4 col-xs-4 no-padding" style="text-align:right;">
            <div id="ticket-label-find">Найти вопрос по дате</div>
        </div>

        <div class="col-md-4 col-xs-4">
            <div class="form-group hidden-inner-label">
                <?= CHtml::activeLabel(Ticket::model(), 'date') ?>
                <?
                $year = date('y');
                $month = date('m');
                $day = date('d');
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => Ticket::model(),
                    'attribute' => 'date',
                    'language' => 'ru',
                    'options' => array(
                        'dateFormat' => 'yy-mm-dd',
                        'showAnim' => 'fold',
                    ),
                    'htmlOptions' => array(
                        'class' => 'fieldInput margin-left-30',
                        'placeholder' => '',
                        'style' => ''
                    ),
                )); ?>
            </div>
        </div>
    </div>
    <div class="row">
        <? $this->renderPartial('list', array('dataProvider' => $dataProvider), false, true); ?>
    </div>
</div>