<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 27.10.2015
 * Time: 15:03
 */
?>
<div id="postAdvert" class="col-md-12" data-type="ticket">
    <div class="row">
        <div class="col-md-11 col-md-push-1 col-xs-11 col-xs-push-1">
            <h1 style="cursor:default;text-decoration:none;margin:30px 0px 35px;">Вопрос-ответ:</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-xs-6">
            <div class="button1 search-btn buttonFilter" style="margin-left:30px;background-color:#FFF!important;color:#000!important;border:1px solid #000;" onclick="location.href='/tickets/new'">
                <i class="fa fa-caret-right hidden-xs hidden-sm" style="left:20px;"></i>Задать вопрос
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <? $form = $this->beginWidget('CActiveForm', array(
                'id' => 'new-form',
                'action' => '/tickets/new',

                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                    'validateOnChange' => true
                ),
                'htmlOptions' => array(
                    'class' => 'indent-left',
                    'style' => 'margin:35px 0px;'
                ),
            )); ?>

            <div class="row form-group">
                <div class="col-md-8">
                    <?= $form->textField($model, 'subject', array('class' => 'fieldInput', 'placeholder'=>'Тема вопроса*', 'style'=>'width:100%')) ?>
                    <?= $form->error($model, 'subject', array('class' => 'text-red')) ?>
                </div>
            </div>

            <div class="row form-group">
                <div class="col-md-8">
                    <?= $form->textArea($model, 'text', array('class' => 'fieldInput', 'placeholder'=>'Задать вопрос*', 'style' => 'width: 100%; height: 100px')) ?>
                    <?= $form->error($model, 'text', array('class' => 'text-red')) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 col-xs-6">
                    <i class="fa fa-caret-right hidden-xs hidden-sm" style="position:absolute;color:rgb(255, 255, 255);z-index:1;top:8px;left:35px;"></i>
                    <?= CHtml::submitButton('Отправить', array('class' => 'button1 search-btn buttonFilter')) ?>
                </div>
            </div>

            <? $this->endWidget(); ?>
        </div>
    </div>
</div>
