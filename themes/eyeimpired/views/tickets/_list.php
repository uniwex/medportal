<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 27.10.2015
 * Time: 12:13
 */
?>

<div class="col-md-12 search-block">
    <div class="row">
        <div class="col-md-7 col-xs-7">
            <div class="userInformationHead2 no-margin">
                <a href="/tickets/view/<?= $data->id ?>"><?= $data->subject ?></a>
            </div>
            <div class="userInformationHead1 no-margin">
                <?= $data->user['name']; ?>
            </div>
            <p style="text-overflow:ellipsis;overflow:hidden!important;white-space:nowrap;"><?= $data->text ?></p>
        </div>
        <div class="col-md-5 col-xs-5 text-right">
            <p><b><?= date('d-m-Y',strtotime($data->date)); ?></b></p>
            <? if(count($data->message)) {
                $lastMess = $data->message[0];
                echo '<p>Дата изменения: <b>'.date('d-m-Y',strtotime($lastMess->messageDate)).'</b></p>';
            } ?>
            <p>Статус: <?= Yii::app()->params['status'][$data->status] ?></p>
        </div>
    </div>
</div>
