<?php
/* @var $this ReviewController */
/* @var $model Review */


?>

<h1><?php echo $model->name; ?></h1>


<? $image = Review::model()->find('id = :id', array(':id' => $model->id));
if ($image->pics != '1') { ?>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border ui-sortable-handle">
                    <h3 class="box-title">Фотографии</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove">
                            <i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <?

                    $pics = explode('_', $image->pics);

                    for ($i = 0; $i < count($pics); $i++) {
                        ?>
                        <div class="col-md-4 col-xs-4 reviewImage">
                            <img class="reviewImageDetail"
                                 src="<?= Yii::app()->request->getBaseUrl(true) ?>/upload/<? echo $pics[$i] ?>">
                        </div>
                        <?
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
<? } ?>

<?
$idUserTo=User::getName($model->idUserTo);
$idUserFrom=User::getName($model->idUserFrom);
?>
<div class="box box-info text-left">
    <?php $this->widget('zii.widgets.CDetailView', array(
        'data' => $model,
        'attributes' => array(
            'id',
            array(
                'name' => 'idUserFrom',
                'value' => "$idUserTo"
            ),
            array(
                'name' => 'idUserTo',
                'value' => "$idUserFrom"
            ),
            'name',
            'text',
            'cure',
            'regard',
            'qualification',
            'time',
            'pics',
            'moderate'
        ),
    )); ?>
</div>
