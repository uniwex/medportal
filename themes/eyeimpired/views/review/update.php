<?php
/* @var $this ReviewController */
/* @var $model Review */

?>
<? $image = Review::model()->find('id = :id', array(':id' => $model->id));
if ($image->pics != '1') { ?>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border ui-sortable-handle">
                    <h3 class="box-title">Фотографии</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove">
                            <i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <?

                    $pics = explode('_', $image->pics);

                    for ($i = 0; $i < count($pics); $i++) {
                        ?>
                        <div class="col-md-4 col-xs-4 reviewImage">
                            <button type="button" id="deleteItem" class="btn btn-danger" name="deleteItem"
                                    value="<? echo $model->id . '_' . $i ?>">Удалить
                            </button>
                            <img class="reviewImageDetail"
                                 src="<?= Yii::app()->request->getBaseUrl(true) ?>/upload/<? echo $pics[$i] ?>">
                        </div>
                        <?
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
<? } ?>
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Изменение комментария <?= $model->name; ?></h3>
            </div>
            <?php $this->renderPartial('_form', array('model' => $model)); ?>
        </div>
    </div>
</div>