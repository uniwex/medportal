<? Yii::app()->clientScript->registerScriptFile(Yii::app()->homeUrl . 'assets/scripts/edit.js', CClientScript::POS_END); ?>

<div class="col-md-10">
    <?php $formDoctor = $this->beginWidget("CActiveForm", array(
        'id' => 'doctor-edit',
        'action' => Yii::app()->request->baseUrl . '/edit/doctor',
        'method' => 'post',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
        ),
        'htmlOptions' => array(
            'autocomplete' => 'off',
            'enctype' => 'multipart/form-data',
            'onsubmit' => 'crutchForDoctorCKEditor();return true'
        ),
    )); ?>
    <span class="userEditDoctor">
        <?= $formDoctor->hiddenField($doctorModel, 'pic') ?>
        <div class="form-group">
            <?= $formDoctor->errorSummary($userModel, 'Пожалуйста, исправьте следующие ошибки:', '', array('class' => 'text-red')) ?>
            <?= $formDoctor->errorSummary($doctorModel, '', '', array('class' => 'text-red')) ?>
        </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formDoctor->labelEx($userModel, 'name') ?>
                </div>
                <div class="col-md-8">
                    <?= $formDoctor->textField($userModel, 'name', array('class' => 'fieldInput')); ?>
                    <?= $formDoctor->error($userModel, 'name', array('class' => 'text-red')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formDoctor->labelEx($userModel, 'email') ?>
                </div>
                <div class="col-md-8">
                    <?= $formDoctor->textField($userModel, 'email', array('class' => 'fieldInput', 'placeholder' => 'sample@mail.ru')); ?>
                    <?= $formDoctor->checkBox($userModel, 'emailEnable', array('id' => 'User_emailEnable_1')); ?>
                    <?= $formDoctor->labelEx($userModel, 'emailEnable', array('for' => 'User_emailEnable_1')); ?>
                    <?= $formDoctor->error($userModel, 'email', array('class' => 'text-red')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formDoctor->labelEx($userModel, 'password') ?>
                </div>
                <div class="col-md-8">
                    <?= $formDoctor->passwordField($userModel, 'password', array('class' => 'fieldInput', 'value' => '')); ?>
                    <?= $formDoctor->error($userModel, 'password', array('class' => 'text-red')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formDoctor->labelEx($userModel, 'phone') ?>
                </div>
                <div class="col-md-8">
                    <?= $formDoctor->textField($userModel, 'phone', array('class' => 'fieldInput', 'placeholder' => '+79000000000')) ?>
                    <?= CHtml::button('Изменить', array('class' => 'btn-change', 'name' => 'changePhone')) ?>
                    <?= $formDoctor->error($userModel, 'phone', array('class' => 'text-red')) ?>
                    <div class="indent-top hidden">
                        <?= CHtml::textField('code', '', array('class' => 'fieldInput', 'placeholder' => 'Введите пароль из SMS')) ?>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formDoctor->labelEx($userModel, 'country') ?>
                </div>
                <div class="col-md-8">
                    <?= $formDoctor->dropDownList(
                        $userModel,
                        'country',
                        array(
                            0 => 'Выберите страну'
                        ),
                        array(
                            'class' => 'fieldInput',
                            'data-alias' => 'country',
                            'options' => array(
                                $userModel->country => array(
                                    'selected' => true
                                )
                            )
                        )
                    ); ?>
                    <?= $formDoctor->error($userModel, 'country', array('class' => 'text-red')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formDoctor->labelEx($userModel, 'city'); ?>
                </div>
                <div class="col-md-4 col-xs-6">
                    <?= $formDoctor->textField($userModel, 'city', array('class' => 'fieldInput inputForSearchCity')); ?>
                    <?= $formDoctor->error($userModel, 'city', array('class' => 'text-red')) ?>
                    <select class="dropDownCities" type="select" size="5" data-alias="city">
                        <option>Выберите город</option>
                    </select>
                </div>
                <div class="col-md-4 col-xs-6">
                    <?= $formDoctor->dropDownList($stationModel, 'name', array('' => 'Выберите станцию метро', 'Нет метро' => 'Нет метро'), array('class' => 'fieldInput', 'data-alias' => 'metro', 'multiple' => true)); ?>
                    <?= $formDoctor->error($stationModel, 'name', array('class' => 'text-red')) ?>
                    <div class="selectButton"></div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formDoctor->labelEx($doctorModel, 'typeHuman') ?>
                </div>
                <div class="col-md-8">
                    <?= $formDoctor->dropDownList($doctorModel, 'typeHuman', array('Дети' => 'Дети', 'Взрослые' => 'Взрослые', 'Все' => 'Все'), array('class' => 'fieldInput')) ?>
                    <?= $formDoctor->error($doctorModel, 'typeHuman', array('class' => 'text-red')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formDoctor->labelEx($doctorModel, 'specialization'); ?>
                </div>
                <div class="col-md-8">
                    <?= $formDoctor->textArea($doctorModel, 'specialization', array('class' => 'fieldInput', 'style' => 'width: 100%; height: 100px')); ?>
                    <i>* Для лучшего форматирования текста начинайте каждый пункт с новой строки</i>
                    <?= $formDoctor->error($doctorModel, 'specialization', array('class' => 'text-red')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formDoctor->labelEx($doctorModel, 'expirience') ?>
                </div>
                <div class="col-md-8">
                    <?= $formDoctor->textArea($doctorModel, 'expirience', array('class' => 'fieldInput', 'style' => 'width: 100%; height: 100px')); ?>
                    <i>* Для лучшего форматирования текста начинайте каждый пункт с новой строки</i>
                    <?= $formDoctor->error($doctorModel, 'expirience', array('class' => 'text-red')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formDoctor->labelEx($doctorModel, 'education') ?>
                </div>
                <div class="col-md-8">
                    <?= $formDoctor->textArea($doctorModel, 'education', array('class' => 'fieldInput', 'style' => 'width: 100%; height: 100px')); ?>
                    <i>* Для лучшего форматирования текста начинайте каждый пункт с новой строки</i>
                    <?= $formDoctor->error($doctorModel, 'education', array('class' => 'text-red')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formDoctor->labelEx($doctorModel, 'idSpecialization') ?>
                </div>
                <div class="col-md-8">
                    <?= $formDoctor->dropDownList($doctorModel, 'idSpecialization', CHtml::listData(Specialization::model()->findAll(), 'id', 'name'), array('class' => 'fieldInput')); ?>
                    <?= $formDoctor->error($doctorModel, 'idSpecialization', array('class' => 'text-red')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formDoctor->labelEx($doctorModel, 'license'); ?>
                </div>
                <div class="col-md-8">
                    <?= $formDoctor->textField($doctorModel, 'license', array('class' => 'fieldInput')); ?>
                    <?= $formDoctor->error($doctorModel, 'license', array('class' => 'text-red')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formDoctor->labelEx($doctorModel, 'merit'); ?>
                </div>
                <div class="col-md-8">
                    <?= $formDoctor->textField($doctorModel, 'merit', array('class' => 'fieldInput')); ?>
                    <?= $formDoctor->error($doctorModel, 'merit', array('class' => 'text-red')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <label>График работы</label>
                </div>
                <div class="col-md-9">
                    <div class="tabTitle">Время приёма</div>
                    <table class="timetable" data-id="0" data-type="doc" onclick="openSchedule(this)">
                        <tr>
                            <th>Пн</th>
                            <th>Вт</th>
                            <th>Ср</th>
                            <th>Чт</th>
                            <th>Пт</th>
                            <th>Сб</th>
                            <th>Вс</th>
                        </tr>
                        <tr>
                            <th class="weekend"><input value="выходной"></th>
                            <th class="weekend"><input value="выходной"></th>
                            <th class="weekend"><input value="выходной"></th>
                            <th class="weekend"><input value="выходной"></th>
                            <th class="weekend"><input value="выходной"></th>
                            <th class="weekend"><input value="выходной"></th>
                            <th class="weekend"><input value="выходной"></th>
                        </tr>
                    </table>
                    <?= $formDoctor->hiddenField($scheduleModel, 'timework', array('id' => 'Schedule_timework_0', 'data-id' => '0', 'data-type' => 'doc')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formDoctor->labelEx($doctorModel, 'description'); ?>
                </div>
                <div class="col-md-8">
                    <?= $formDoctor->textArea($doctorModel, 'description', array('class' => 'fieldInput', 'style' => 'width:100%;height:100px;')); ?>
                    <?= $formDoctor->error($doctorModel, 'description', array('class' => 'text-red')) ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">

                </div>
                <div class="col-md-8">
                    <?= $formDoctor->checkBox($doctorModel, 'enabled', array('id' => 'User_type_1')); ?>
                    <?= $formDoctor->labelEx($doctorModel, 'enabled', array('for' => 'User_type_1')); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formDoctor->labelEx($doctorModel, 'price'); ?>
                </div>
                <div class="col-md-8">
                    <?= $formDoctor->numberField($doctorModel, 'price', array('class' => 'fieldInput')); ?>
                    <?= $formDoctor->error($doctorModel, 'price', array('class' => 'text-red')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formDoctor->labelEx($doctorModel, 'image'); ?>
                </div>
                <div class="col-md-8">
                    <div class="fieldInput" id="image">
                        <?= $formDoctor->fileField($doctorModel, 'image'); ?>
                    </div>
                    <?= $formDoctor->error($doctorModel, 'image', array('class' => 'text-red')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formDoctor->labelEx($socialModel, 'vkontakte'); ?>
                </div>
                <div class="col-md-8">
                    <?= $formDoctor->textField($socialModel, 'vkontakte', array('class' => 'fieldInput')); ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formDoctor->labelEx($socialModel, 'odnoklassniki'); ?>
                </div>
                <div class="col-md-8">
                    <?= $formDoctor->textField($socialModel, 'odnoklassniki', array('class' => 'fieldInput')); ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formDoctor->labelEx($socialModel, 'facebook'); ?>
                </div>
                <div class="col-md-8">
                    <?= $formDoctor->textField($socialModel, 'facebook', array('class' => 'fieldInput')); ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formDoctor->labelEx($socialModel, 'twitter'); ?>
                </div>
                <div class="col-md-8">
                    <?= $formDoctor->textField($socialModel, 'twitter', array('class' => 'fieldInput')); ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formDoctor->labelEx($socialModel, 'instagram'); ?>
                </div>
                <div class="col-md-8">
                    <?= $formDoctor->textField($socialModel, 'instagram', array('class' => 'fieldInput')); ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <?= CHtml::submitButton('Обновить', array('class' => 'button1 button1reg')); ?>
                </div>
            </div>
        </span>
    <?php $this->endWidget(); ?>
</div>
<script src="//cdn.ckeditor.com/4.5.4/standard/ckeditor.js"></script>
<script type="text/javascript">
    //CKEDITOR INITIALIZATION
    //NURSE
    CKEDITOR.replace('Doctor[education]');
    CKEDITOR.replace('Doctor[specialization]');
    CKEDITOR.replace('Doctor[expirience]');
    CKEDITOR.replace('Doctor[description]');
    //call when form submit
    var crutchForDoctorCKEditor = function(){
        var iframesDoctor = $('.userEditDoctor iframe');
        for(var i=0;i<iframesDoctor.length;i++) {
            var text = iframesDoctor[i].contentDocument.childNodes[1].childNodes[1].childNodes[0].innerHTML;
            if (text != '<br>') {
                $(iframesDoctor[i].parentNode.parentNode.parentNode.parentNode).find('textarea').val(text);
            } else
                $(iframesDoctor[i].parentNode.parentNode.parentNode.parentNode).find('textarea').val('');
        }
    }
</script>