<? Yii::app()->clientScript->registerScriptFile(Yii::app()->homeUrl . 'assets/scripts/edit.js', CClientScript::POS_HEAD); ?>
<div class="col-md-10">
    <?php $formClinic = $this->beginWidget("CActiveForm", array(
        'id' => 'clinic-edit',
        'action' => Yii::app()->request->baseUrl . '/edit/clinic',
        'method' => 'post',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true
        ),
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
            'autocomplete' => 'off',
            'onsubmit' => 'crutchForClinicCKEditor();return true'
        ),
    )); ?>
    <span class="userEditClinic">
        <div class="form-group">
            <?= $formClinic->errorSummary($userModel, 'Пожалуйста, исправьте следующие ошибки:', '', array('class' => 'text-red')) ?>
            <?= $formClinic->errorSummary($userModel->clinic, '', '', array('class' => 'text-red')) ?>
            <?= $formClinic->errorSummary($userModel->station, '', '', array('class' => 'text-red')) ?>
        </div>
        <span class="displayClinicStep1">
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($userModel, 'name') ?>
                </div>
                <div class="col-md-8">
                    <?= $formClinic->textField($userModel, 'name', array('class' => 'fieldInput', 'placeholder' => 'Вишневского')); ?>
                    <?= $formClinic->error($userModel, 'name', array('class' => 'text-red')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($userModel->clinic, 'site') ?>
                </div>
                <div class="col-md-8">
                    <?= $formClinic->textField($userModel->clinic, 'site', array('class' => 'fieldInput', 'placeholder' => 'www.fotoditazin.com')); ?>
                    <?= $formClinic->error($userModel->clinic, 'site', array('class' => 'text-red')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($userModel, 'email') ?>
                </div>
                <div class="col-md-8">
                    <?= $formClinic->textField($userModel, 'email', array('class' => 'fieldInput', 'placeholder' => 'sample@mail.ru')); ?>
                    <?= $formClinic->checkBox($userModel, 'emailEnable', array('id' => 'User_emailEnable_2')); ?>
                    <?= $formClinic->labelEx($userModel, 'emailEnable', array('for' => 'User_emailEnable_2')); ?>
                    <?= $formClinic->error($userModel, 'email', array('class' => 'text-red')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($userModel, 'password') ?>
                </div>
                <div class="col-md-8">
                    <?= $formClinic->passwordField($userModel, 'password', array('class' => 'fieldInput', 'value' => '')); ?>
                    <?= $formClinic->error($userModel, 'password', array('class' => 'text-red')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($userModel, 'phone') ?>
                </div>
                <div class="col-md-8">
                    <?= $formClinic->textField($userModel, 'phone', array('class' => 'fieldInput', 'placeholder' => '+79000000000')) ?>
                    <?= CHtml::button('Изменить', array('class' => 'btn-change', 'name' => 'changePhone')) ?>
                    <?= $formClinic->error($userModel, 'phone', array('class' => 'text-red')) ?>
                    <div class="indent-top hidden">
                        <?= CHtml::textField('code', '', array('class' => 'fieldInput', 'placeholder' => 'Введите пароль из SMS')) ?>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($userModel, 'country') ?>
                </div>
                <div class="col-md-8">
                    <?= $formClinic->dropDownList($userModel, 'country', array('0' => 'Выберите страну', 'Россия' => 'Россия'), array('class' => 'fieldInput', 'data-alias' => 'country')); ?>
                    <?= $formClinic->error($userModel, 'country', array('class' => 'text-red')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($userModel, 'city') ?>
                </div>
                <div class="col-md-4 col-xs-6">
                    <?= $formClinic->textField($userModel, 'city', array('class' => 'fieldInput inputForSearchCity')); ?>
                    <?= $formClinic->error($userModel, 'city', array('class' => 'text-red')) ?>
                    <select class="dropDownCities" type="select" size="5" data-alias="city">
                        <option>Выберите город</option>
                    </select>
                </div>
                <div class="col-md-4 col-xs-6">
                    <?= $formClinic->dropDownList($userModel->station, 'name', array('0' => 'Выберите станцию метро', 'Нет метро' => 'Нет метро'), array('class' => 'fieldInput', 'data-alias' => 'metro', 'multiple' => true)); ?>
                    <?= $formClinic->error($userModel->station, 'name', array('class' => 'text-red')) ?>
                    <div class="selectButton"></div>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($userModel->schedule[0], 'График работы клиники') ?>
                </div>
                <div class="col-md-8">
                    <div class="tabTitle">График работы</div>
                    <table class="timetable" data-id="1" data-type="timeclinic" onclick="openSchedule(this)">
                        <tr>
                            <th>Пн</th>
                            <th>Вт</th>
                            <th>Ср</th>
                            <th>Чт</th>
                            <th>Пт</th>
                            <th>Сб</th>
                            <th>Вс</th>
                        </tr>
                        <tr>
                            <th class="weekend"><input value="выходной"></th>
                            <th class="weekend"><input value="выходной"></th>
                            <th class="weekend"><input value="выходной"></th>
                            <th class="weekend"><input value="выходной"></th>
                            <th class="weekend"><input value="выходной"></th>
                            <th class="weekend"><input value="выходной"></th>
                            <th class="weekend"><input value="выходной"></th>
                        </tr>
                    </table>
                    <input data-id="1" data-type="timeclinic" name="Schedule[timeworkClinic]"
                           id="Schedule_timeworkClinic" value="<?= $userModel->schedule[0]->timework ?>"
                           type="hidden"/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($userModel->clinic, 'specialization') ?>
                </div>
                <div class="col-md-8">
                    <?= $formClinic->textArea($userModel->clinic, 'specialization', array('class' => 'fieldInput', 'style' => 'width:100%;height:100px;')); ?>
                    <i>* Для лучшего форматирования текста начинайте каждый пункт с новой строки</i>
                    <?= $formClinic->error($userModel->clinic, 'specialization', array('class' => 'text-red')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($userModel->clinic, 'license') ?>
                </div>
                <div class="col-md-8">
                    <?= $formClinic->textField($userModel->clinic, 'license', array('class' => 'fieldInput')); ?>
                    <?= $formClinic->error($userModel->clinic, 'license', array('class' => 'text-red')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($userModel->clinic, 'merit') ?>
                </div>
                <div class="col-md-8">
                    <?= $formClinic->textField($userModel->clinic, 'merit', array('class' => 'fieldInput')); ?>
                    <?= $formClinic->error($userModel->clinic, 'merit', array('class' => 'text-red')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($userModel->clinic, 'contactAdvanced') ?>
                </div>
                <div class="col-md-8">
                    <?= $formClinic->textField($userModel->clinic, 'contactAdvanced', array('class' => 'fieldInput')); ?>
                    <?= $formClinic->error($userModel->clinic, 'contactAdvanced', array('class' => 'text-red')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($userModel->clinic, 'description') ?>
                </div>
                <div class="col-md-8">
                    <?= $formClinic->textArea($userModel->clinic, 'description', array('class' => 'fieldInput', 'style' => 'width:100%;height:100px;')); ?>
                    <?= $formClinic->error($userModel->clinic, 'description', array('class' => 'text-red')) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($userModel->clinic, 'image') ?>
                </div>
                <div class="col-md-8">
                    <div class="fieldInput" id="image">
                        <?= $formClinic->hiddenField($userModel->clinic, 'pic') ?>
                        <?= $formClinic->fileField($userModel->clinic, 'image'); ?>
                    </div>
                    <?= $formClinic->error($userModel->clinic, 'image', array('class' => 'text-red')) ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($socialModel, 'vkontakte'); ?>
                </div>
                <div class="col-md-8">
                    <?= $formClinic->textField($socialModel, 'vkontakte', array('class' => 'fieldInput')); ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($socialModel, 'odnoklassniki'); ?>
                </div>
                <div class="col-md-8">
                    <?= $formClinic->textField($socialModel, 'odnoklassniki', array('class' => 'fieldInput')); ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($socialModel, 'facebook'); ?>
                </div>
                <div class="col-md-8">
                    <?= $formClinic->textField($socialModel, 'facebook', array('class' => 'fieldInput')); ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($socialModel, 'twitter'); ?>
                </div>
                <div class="col-md-8">
                    <?= $formClinic->textField($socialModel, 'twitter', array('class' => 'fieldInput')); ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <?= $formClinic->labelEx($socialModel, 'instagram'); ?>
                </div>
                <div class="col-md-8">
                    <?= $formClinic->textField($socialModel, 'instagram', array('class' => 'fieldInput')); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <input id="regClinicToStep2" type="button" class="button1 button1reg" value="Перейти к шагу 2">
                    <?= CHtml::submitButton('Обновить', array('class' => 'button1 button1reg')); ?>
                </div>
            </div>
        </span>
        <span class="displayClinicStep2">
            <div class="form-group row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <input id="regClinicToStep1" type="button" class="button1 button1reg" value="Назад к шагу 1">
                </div>
            </div>
            <span id="doctorsInClinic">
                <? for ($i = 0; $i < count($userModel->schedule); $i++) {
                    if (!$userModel->schedule[$i]->spec) continue;
                    ?>
                    <div class="form-doctor">
                        <?= CHtml::hiddenField('hash', md5(Yii::app()->params['salt'].$userModel->schedule[$i]->id)) ?>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <?= $formClinic->labelEx($userModel->schedule[$i], 'name') ?>
                            </div>
                            <div class="col-md-7">
                                <?= $formClinic->textField($userModel->schedule[$i], 'name', array('class' => 'fieldInput', 'name' => 'Schedule[name][]')); ?>
                            </div>
                            <div class="col-md-1">
                                <i data-id="removeDoctor" data-toggle="tooltip" data-placement="top"
                                   title="Удалить врача" class="fa fa-times pull-right" style="cursor: pointer"
                                   onclick="removeDoctor(this)"></i>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <?= $formClinic->labelEx($userModel->schedule[$i]->spec, 'name') ?>
                            </div>
                            <div class="col-md-8">
                                <?= $formClinic->dropDownList(
                                    $userModel->schedule[$i]->spec,
                                    'name',
                                    CHtml::listData(Specialization::model()->findAll(), 'id', 'name'),
                                    array(
                                        'class' => 'fieldInput',
                                        'name' => 'Specialization[name][]',
                                        'options' => array(
                                            $userModel->schedule[$i]->spec->id => array('selected' => 'selected')
                                        )
                                    )
                                ); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <?= $formClinic->labelEx($userModel->schedule[$i], 'price') ?>
                            </div>
                            <div class="col-md-8">
                                <?= $formClinic->numberField($userModel->schedule[$i], 'price', array('class' => 'fieldInput', 'name' => 'Schedule[price][]')); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <?= $formClinic->labelEx($userModel->schedule[$i], 'График работы') ?>
                            </div>
                            <div class="col-md-8">
                                <div class="tabTitle">Время приёма</div>
                                <table class="timetable" data-id="<?= $i ?>" data-type="clinic"
                                       onclick="openSchedule(this)">
                                    <tr>
                                        <th>Пн</th>
                                        <th>Вт</th>
                                        <th>Ср</th>
                                        <th>Чт</th>
                                        <th>Пт</th>
                                        <th>Сб</th>
                                        <th>Вс</th>
                                    </tr>
                                    <tr>
                                        <th class="weekend"><input value="выходной"></th>
                                        <th class="weekend"><input value="выходной"></th>
                                        <th class="weekend"><input value="выходной"></th>
                                        <th class="weekend"><input value="выходной"></th>
                                        <th class="weekend"><input value="выходной"></th>
                                        <th class="weekend"><input value="выходной"></th>
                                        <th class="weekend"><input value="выходной"></th>
                                    </tr>
                                </table>
                                <? //= $formClinic->hiddenField($userModel->schedule[$i]->timework, 'timework', array('id' => 'Schedule_timework_'.$i , 'data-id' => $i, 'data-type' => 'doc')) ?>
                                <input data-id="<?= $i ?>" data-type="clinic" name="Schedule[timework][]"
                                       value="<?= $userModel->schedule[$i]->timework ?>"
                                       id="Schedule_timework_<?= $i ?>"
                                       type="hidden"/>
                            </div>
                        </div>
                    </div>
                <? } ?>
                <div class="form-group row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <input class="button1 btnAddDoctor" type="button" value="Добавить ещё врача">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <?= CHtml::submitButton('Обновить', array('class' => 'button1 button1reg')); ?>
                    </div>
                </div>
            </span>
        </span>
    </span>
    <?php $this->endWidget(); ?>
</div>
<script>
    $(document).ready(function () {
        $('i[data-id="removeDoctor"]').tooltip();
    })
</script>
<script src="//cdn.ckeditor.com/4.5.4/standard/ckeditor.js"></script>
<script type="text/javascript">
    //CKEDITOR INITIALIZATION
    //CLINIC
    CKEDITOR.replace('Clinic[specialization]');
    //call when form submit
    var crutchForClinicCKEditor = function(){
        var iframesClinic = $('.userEditClinic iframe');
        for(var i=0;i<iframesClinic.length;i++) {
            var text = iframesClinic[i].contentDocument.childNodes[1].childNodes[1].childNodes[0].innerHTML;
            if (text != '<br>') {
                $(iframesClinic[i].parentNode.parentNode.parentNode.parentNode).find('textarea').val(text);
            } else
                $(iframesClinic[i].parentNode.parentNode.parentNode.parentNode).find('textarea').val('');
        }
    }
</script>