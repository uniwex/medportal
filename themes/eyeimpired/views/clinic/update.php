<?php
/* @var $this ClinicController */
/* @var $model Clinic */

/*$this->breadcrumbs = array(
    'Clinics' => array('index'),
    $model->name => array('view', 'id' => $model->idUser),
    'Update',
);*/

//$this->menu = array(
//    array('label' => 'List Clinic', 'url' => array('index')),
//    array('label' => 'Create Clinic', 'url' => array('create')),
//    array('label' => 'View Clinic', 'url' => array('view', 'id' => $model->idUser)),
//    array('label' => 'Manage Clinic', 'url' => array('admin')),
//);
?>
    <form action="/clinic/UpdateDetail" method="post">
        <div class="col-md-12 createGood">
            <div class="box box-primary">
                <div class="box-header with-border">
                </div>
                <div class="box-body">
                    <? $doctor = Schedule::model()->findAll('idUser = :id and type = :type', array(':id' => $model->idUser, ':type' => 'clinicDoc')); ?>

                    <div class="form-group">
                        <div class="col-md-12 checklist">
                            <?= CHtml::dropDownList(
                                'specialization', '',
                                array_merge(
                                    CHtml::listData(
                                        $doctor, 'id', 'name',
                                        function ($Schedule) {
                                            if (isset($Schedule->spec->name)) {
                                                return $Schedule->spec->name;
                                            }
                                        }
                                    )
                                ),
                                array(
                                    'id' => 'lstFruits',
                                    'multiple' => true
                                )
                            ) ?>
                            <input type="button" id="btnSelected" class="btn btn-primary" value="Выбрать"/>
                            <a href="#myModal" id="addDoctor" class="btn btn-info" data-toggle="modal">+Добавить
                                врача</a>
                        </div>
                        <form action="/clinic/UpdateDetail" method="post">
                            <div id='test'></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <form action="/clinic/AddDoctor" method="post">
        <div id="myModal" class="modal fade">
            <div class="modal-dialog addDoctorModal">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Добавление врача</h4>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12">
                            в разработке

<!--                            <div class="col-md-12 str">-->
<!--                                <div class="col-md-4 text-center">ФИО<br><input type="text" data-detail-day="" name="name[]"-->
<!--                                                             class="form-control">-->
<!--                                </div>-->
<!--                                --><?// $specialization = specialization::model()->findAll(); ?>
<!--                                -->
<!--                                <div class="col-md-4 text-center">Специальность<br>--><?//= CHtml::dropDownList(
//                                        'specialization[]', '',
//                                        array_merge(
//                                            CHtml::listData(
//                                                $specialization, 'id',
//                                                function ($Schedule) {
//                                                    if (isset($Schedule->name)) {
//                                                        return $Schedule->name;
//                                                    }
//                                                }
//                                            )
//                                        ),
//                                        array(
//                                            'id' => 'listSpec',
//                                            'multiple' => false
//                                        )
//                                    ) ?><!--</div>-->
<!--                                <div class="col-md-2 text-center">Цена<br><input type="text" data-detail-day="" name="price[]"-->
<!--                                                             class="form-control"></div>-->
<!--                                <input type="hidden"  name="id" class="form-control" value="--><?//=$model->idUser?><!--">-->
<!--                                <div class="col-md-2 text-center">Время<br><input type="text" data-detail-day="" name="timeSize[]"-->
<!--                                                             class="form-control"></div>-->
<!--                                <div title="Добавить врача" data-id-plus-doctorAdd=""  data-toggle="tooltip" class="col-md-12 text-center"><i class="fa fa-plus red"></i></div>-->
<!--                            </div>-->
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
<?php $this->renderPartial('_form', array('model' => $model)); ?>