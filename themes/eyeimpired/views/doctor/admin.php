<?php
/* @var $this DoctorController */
/* @var $model Doctor */
?>
<div class="row">
    <div class="col-md-12">

        <div class="box box-info">
            <div class="box-header with-border ui-sortable-handle">
                <h3 class="box-title">Список врачей</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <?php $this->widget('zii.widgets.grid.CGridView', array(
                    'id' => 'doctor-grid',
                    'dataProvider' => $model->search(),
                    'filter' => $model,
                    'cssFile' => Yii::app()->request->getBaseUrl(true) . '/assets/admin/css/gridview.css',
                    'itemsCssClass' => 'table no-margin',
                    'htmlOptions' => array(
                        'class' => 'table-responsive'
                    ),
                    'summaryText' => 'Показано записей: {start} - {end} из {count}',
                    'emptyText' => 'Результаты не найдены',
                    'columns' => array(
                        array(
                            'name' => 'pic',
                            'type' => 'image',
                            'value' => '"/upload/".$data->pic',
                            'htmlOptions' => array('class' => 'imgGood'),
                            'filter' => false,
                        ),
                        'name',
                        array(
                            'name' => 'idSpecialization',
                            'filter' => CHtml::listData(
                                $specializations,
                                'id',
                                'name'
                            ),
                            'value' => '$data->getSpecialization->name'
                        ),
                        array(
                            'name' => 'typeHuman',
                            'filter' => array(
                                '' => 'Любые',
                                'Все' => 'Все',
                                'Дети' => 'Дети',
                                'Взрослые' => 'Взрослые'
                            )
                        ),
                        /*
                        'expirience',
                        'license',
                        'description',
                        'merit',
                        'specialization',
                        'education',
                        'price',
                        'rate',
                        'isPhototherapy',
                        'pic',
                        */
                        array(
                            'header' => 'Операции',
                            'class' => 'CButtonColumn',
                        ),
                    ),
                )); ?>
            </div>
            <div class="box-footer clearfix"></div>
        </div>
    </div>
</div>