<?php
/* @var $this DoctorController */
/* @var $data Doctor */
?>

<div class="view">

    <b><?php echo CHtml::encode($data->getAttributeLabel('idUser')); ?>:</b>
    <?php echo CHtml::link(CHtml::encode($data->idUser), array('view', 'id' => $data->idUser)); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('idSpecialization')); ?>:</b>
    <?php echo CHtml::encode($data->idSpecialization); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
    <?php echo CHtml::encode($data->name); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('expirience')); ?>:</b>
    <?php echo CHtml::encode($data->expirience); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('license')); ?>:</b>
    <?php echo CHtml::encode($data->license); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
    <?php echo CHtml::encode($data->description); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('merit')); ?>:</b>
    <?php echo CHtml::encode($data->merit); ?>
    <br/>

    <?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('specialization')); ?>:</b>
	<?php echo CHtml::encode($data->specialization); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('education')); ?>:</b>
	<?php echo CHtml::encode($data->education); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('price')); ?>:</b>
	<?php echo CHtml::encode($data->price); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rate')); ?>:</b>
	<?php echo CHtml::encode($data->rate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('typeHuman')); ?>:</b>
	<?php echo CHtml::encode($data->typeHuman); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('isPhototherapy')); ?>:</b>
	<?php echo CHtml::encode($data->isPhototherapy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pic')); ?>:</b>
	<?php echo CHtml::encode($data->pic); ?>
	<br />

	*/ ?>

</div>