<?php
/* @var $this DoctorController */
/* @var $dataProvider CActiveDataProvider */

?>

<h1>Doctors</h1>

<?php $this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_view',
)); ?>
