<?php
/* @var $this TestController */
/* @var $data Article */
?>
<div class="row article">
    <div class="col-md-2 col-xs-2 no-padding">
        <?php
        $image = Yii::app()->request->baseUrl.'/assets/images/articles-overlay.png';
        if($data->image)
            if($data->image != '')
                $image = Yii::app()->request->baseUrl.'/upload/articles/' . $data->image;
        ?>
        <div class="article-img" style="background-image:url('<?= $image; ?>')"></div>
    </div>
    <div class="col-md-10 col-xs-10">
        <div class="row">
            <div class="col-md-11 col-xs-11 article-author"><? ?></div>
            <div class="col-md-1 col-xs-1 article-author"></div>
        </div>
        <div class="row form-group">
            <a href="/articles/<?=$data->id?>">
                <div class="col-md-10 col-xs-10 article-head no-padding" style="padding-left: 14px;">
                    <?= $data->name ?>
                </div>
                <div class="col-md-2 col-xs-2 text-r no-padding"><?= date('d-m-Y',strtotime($data->time)) ?></div>
            </a>
            <?php
                $articleModel = Article::model()->findByPk($data->id);
                $idUser = 0;
                if(isset(Yii::app()->user->id)) {
                    $idUser = Yii::app()->user->id;
                    if(($articleModel->idUser == $idUser)||( User::model()->findByPk($idUser)->role == 'admin')) {
                        ?>
                        <div class="col-md-1 col-xs-1 text-r">
                            <a href="/articles/edit/<?= $data->id; ?>"><i class="fa fa-pencil-square-o"></i></a>
                            <i class="fa fa-times" onclick="removeArticle(this, 0)" data-id="<?= $data->id; ?>"></i></div>
                        <?php
                    }
                }
            ?>
        </div>
        <div class="row article-text">
            <div class="col-md-9 col-xs-9 no-padding" style="margin-left: 14px;">
                <?= $data->text ?>
            </div>
            <!--<div class="col-md-2 rating-div" style="text-align: left; margin-left: 18px;" onclick="return false;">-->
                <? //$this->widget('RatingWidget',['elem_id' => $data->id,'elem_type' => 'article','avg' => RatingHelper::getRating($data->id, 'article')]); ?>
            <div class="col-md-2 col-xs-2">
            </div>
        </div>
    </div>
</div>
<script src="/assets/scripts/articles.js"></script>