<link href='<?= Yii::app()->request->baseUrl ?>/assets/styles/rating.css' media="screen" rel="stylesheet"
      type="text/css"/>
<script type="text/javascript" src='<?= Yii::app()->request->baseUrl ?>/assets/scripts/rating.js'></script>
<div id="postAdvert" class="col-md-10">

    <div class="row">
        <?php if (!Yii::app()->user->isGuest) { ?>
            <div class="col-md-9"></div>
            <div class="col-md-3">
                <input class="button1 buttonFilter bg-blue" style="margin:15px" type="button" value="Добавить статью" onclick="addArticle();">
            </div>
        <?php } ?>
    </div>
    <div class="row form-group" style="margin-bottom: -10px;">
        <?php if (Yii::app()->user->id) { ?>
            <div class="col-md-3">
                <p style="text-align: center">Автор статей</p>
            </div>
        <?php } ?>
        <div class="col-md-3">
            <p style="text-align: center">Категория</p>
        </div>
        <div class="col-md-3">
            <p style="text-align: center">Дата</p>
        </div>
        <div class="col-md-3">
            <p style="text-align: center">Сортировать по</p>
        </div>
    </div>
    <div class="row form-group">
        <?php if (Yii::app()->user->id) { ?>
        <div class="col-md-3">
            <?php
            echo CHtml::dropDownList('articleUser', '0', array('0'=>'Любой','1'=>'Только я'), array(
                'id' => 'articleCurrentUser',
                'class' => 'fieldInput',
            ));
            ?>
        </div>
            <!--<div class="col-md-push-1 col-md-2" >
                <input id="articleCurrentUser" type="checkbox" name="articleCurrentUser" value="1">
            </div>-->
        <? } else { ?>
            <div class="col-md-3"></div>
        <? } ?>
        <div class="col-md-3">
            <?php
            echo CHtml::dropDownList('categories', '', CHtml::listData(Category::model()->findAll(), 'id', 'name', 'category'), array(
                'id' => 'inputCategories',
                'class' => 'fieldInput',
                'prompt' => 'Не выбрана'
            ));
            ?>
        </div>
        <div class="col-md-3">
            <?php
            /*$year = date('Y');
            $month = date('m');
            $day = date('d');*/
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'name' => 'birthday',
                'model' => $patientModel,
                'attribute' => 'birthday',
                'language' => 'ru',
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                    'class' => 'fieldInput',
                    'showAnim' => 'fold',
                ),
                'htmlOptions' => array(
                    'class' => 'fieldInput',
                    'id' => 'dateArticle',
                ),
            )); ?>
        </div>
        <div class="col-md-3">
            <?php
            echo CHtml::dropDownList('sorting', '0', array('0'=>'По дате','1'=>'Рейтингу (убывание)','2'=>'Рейтингу (возрастание)'), array(
                'id' => 'inputSorting',
                'class' => 'fieldInput',
            ));
            ?>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-5"></div>
        <div class="col-md-2">
            <input class="button1 buttonFilter bg-blue" type="button" value="Применить фильтр" onclick="loadArticles();">
        </div>
        <div class="col-md-5"></div>
    </div>
	<span id="listOfArticles">
	<?php foreach ($articles as $key) {
        $day = date('d', strtotime($key->time));
        $month = date('M', strtotime($key->time));
        $year = date('Y', strtotime($key->time));
        switch ($month) {
            case 'Jan':
                $month = 'января';
                break;
            case 'Feb':
                $month = 'февраля';
                break;
            case 'Mar':
                $month = 'марта';
                break;
            case 'Apr':
                $month = 'апреля';
                break;
            case 'May':
                $month = 'мая';
                break;
            case 'Jun':
                $month = 'июня';
                break;
            case 'Jul':
                $month = 'июля';
                break;
            case 'Aug':
                $month = 'августа';
                break;
            case 'Sep':
                $month = 'сентября';
                break;
            case 'Oct':
                $month = 'октября';
                break;
            case 'Nov':
                $month = 'ноября';
                break;
            case 'Dec':
                $month = 'декабря';
                break;
        }
        $user = User::model()->findByPk($key->idUser);
        $name = $user->name;
        ?>
        <div class="row article" id="article<?= $key->id; ?>">
            <div class="col-md-3" data-id="<?= $key->id; ?>" onclick="showArticle(event, this)">
                <?php if ($key->image) $image = '/upload/articles/' . $key->image; else $image = Yii::app()->request->baseUrl . '/assets/images/blankPhoto1.png'; ?>
                <img class="article-img" src="<?= $image; ?>">
            </div>
            <div class="col-md-9">
                <div class="row" data-id="<?= $key->id; ?>" onclick="showArticle(event, this)">
                    <div class="col-md-11 article-author"><?php echo $name; ?></div>
                    <div class="col-md-1 article-author"><?php if ($key->idUser == Yii::app()->user->id) { ?><i
                            class="fa fa-pencil-square-o" data-id="<?= $key->id; ?>"
                            onclick="editArticle(event, this)"></i><i data-id="<?= $key->id; ?>" class="fa fa-times lml"
                                                                      onclick="removeArticle(event, this)"></i><?php } ?>
                    </div>
                </div>
                <div class="row form-group" data-id="<?= $key->id; ?>" onclick="showArticle(event, this)">
                    <div class="col-md-8 article-head no-padding"><?php echo $key->name; ?></div>
                    <div class="col-md-4 text-r"><?php echo $day . ' ' . $month . ' ' . $year; ?></div>
                </div>
                <div class="row article-text">
                    <div class="col-md-8" data-id="<?= $key->id; ?>" onclick="showArticle(event, this)">
                        <?php echo $key->text; ?>
                    </div>
                    <div class="col-md-4 rating-div">
                        <? $this->widget('RatingWidget', ['elem_id' => $key->id, 'elem_type' => 'article', 'avg' => RatingHelper::getRating($key->id, 'article')]) ?>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
	</span>
</div>
<input id="userId" type="hidden" value=<?= Yii::app()->user->id; ?>>
<script>

    function showArticle(event, el) {
        var id = el.getAttribute('data-id');
        location.href = '/articles/' + id;
    }

    function addArticle() {
        location.href = '/articles/add';
    }

    function editArticle(event, el) {
        event = event || window.event;
        event.stopPropagation();
        var id = el.getAttribute('data-id');
        location.href = '/articles/edit/' + id;
    }

    function removeArticle(event, el) {
        event = event || window.event;
        event.stopPropagation();
        var id = el.getAttribute('data-id');
        $('#preload').css('display', 'block');
        $.ajax({
            url: '/articles/remove/' + id,
            method: 'GET',
            success: function (response) {
                $('#article' + id).fadeOut();
            },
            error: function (data) {
                console.log(data);
            },
            complete: function () {
                $('#preload').css('display', 'none');
            }
        });
    }

    function loadArticles() {
        var userId = $('#userId').val();
        var idCategory = $('#inputCategories').val();
        var articleDate = $('#dateArticle').val();
        var sorting = $('#inputSorting').val();
        if (($('#articleCurrentUser').val() == 0) || ($('#userId').val() == ''))
           userId = -1;
        //console.log(userId + "  -  "+ articleDate + "   -   " + idCategory + "    -    "+sorting);
        if (!idCategory)
            idCategory = -1;
        $('#preload').css('display', 'block');
        $.ajax({
            url: '/articles/getArticles',
            method: 'POST',
            data: {'idCategory':idCategory,'userId':userId,'articleDate':articleDate,'sorting':sorting},
            success: function (responce) {
                $('#listOfArticles').html('');
                if (!responce) {
                    return;
                }
                var data = JSON.parse(responce);
                var html = '';
                if (data.length == 0) {
                    html += '<div class="row">';
                    html += '<div class="col-md-12 text-center article-head">Ничего не найдено</div>';
                    html += '</div>';
                }
                for (var i = 0; i < data.length; i++) {
                    var articleDate = data[i].time;
                    articleDate = articleDate.split(' ')[0];
                    var articleDateYear = articleDate.split('-')[0];
                    var articleDateMonth = articleDate.split('-')[1];
                    var articleDateDay = articleDate.split('-')[2];
                    switch (articleDateMonth) {
                        case '01':
                            articleMonth = 'января'
                            break;
                        case '02':
                            articleMonth = 'февраля'
                            break;
                        case '03':
                            articleMonth = 'марта'
                            break;
                        case '04':
                            articleMonth = 'апреля'
                            break;
                        case '05':
                            articleMonth = 'мая'
                            break;
                        case '06':
                            articleMonth = 'июня'
                            break;
                        case '07':
                            articleMonth = 'июля'
                            break;
                        case '08':
                            articleMonth = 'августа'
                            break;
                        case '09':
                            articleMonth = 'сентября'
                            break;
                        case '10':
                            articleMonth = 'октября'
                            break;
                        case '11':
                            articleMonth = 'ноября'
                            break;
                        case '12':
                            articleMonth = 'декабря'
                            break;
                    }
                    html += '<div class="row article" id="article' + data[i].id + '" data-id="' + data[i].id + '" onclick="showArticle(event, this)">';
                    html += '<div class="col-md-3">';
                    var pic = '/assets/images/blankPhoto1.png';
                    if (data[i].image != '')
                        pic = '/upload/articles/' + data[i].image;
                    html += '<img class="article-img" src="' + pic + '">';
                    html += '</div>';
                    html += '<div class="col-md-9">';
                    html += '<div class="row">';
                    html += '<div class="col-md-11 article-author">' + data[i].author + '</div>';
                    if ($('#userId').val() == data[i].idUser)
                        html += '<div class="col-md-1 article-author"><i class="fa fa-pencil-square-o" data-id="' + data[i].id + '" onclick="editArticle(event, this)"></i><i data-id="' + data[i].id + '" class="fa fa-times lml" onclick="removeArticle(event, this)"></i></div>';
                    else
                        html += '<div class="col-md-1 article-author"></div>';
                    html += '</div>';
                    html += '<div class="row form-group">';
                    html += '<div class="col-md-8 article-head no-padding">' + data[i].name + '</div>';
                    html += '<div class="col-md-4 text-r">' + articleDateDay + ' ' + articleMonth + ' ' + articleDateYear + '</div>';
                    html += '</div>';
                    html += '<div class="row article-text">' + data[i].text + '</div>';
                    html += '</div>';
                    html += '</div>';
                }
                $('#listOfArticles').html(html);

            },
            error: function (data) {
                console.log(data);
            },
            complete: function () {
                $('#preload').css('display', 'none');
            }
        });
    }
</script>