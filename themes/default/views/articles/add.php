<div id="postAdvert" class="col-md-10">
    <?php $formArticle = $this->beginWidget("CActiveForm", array(
        'id' => 'add-article',
        'action' => Yii::app()->request->baseUrl . '/articles/saveNew',
        'method' => 'post',
//        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true
        ),
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
            'autocomplete' => 'off',
            'onsubmit' => 'crutchForCKEditor();return true',
        ),
    )); ?>
    <div class="form-group row">
        <div class="col-md-2">
            <?= $formArticle->labelEx($articleModel, 'name') ?>
        </div>
        <div class="col-md-8">
            <?= $formArticle->textField($articleModel, 'name', array('class' => 'fieldInput', 'placeholder' => 'О сайте')); ?>
            <?= $formArticle->error($articleModel, 'name', array('class' => 'text-red')) ?>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-2">
            <?= $formArticle->labelEx($articleModel, 'image'); ?>
        </div>
        <div class="col-md-8">
            <div id="image" class="fieldInput">
                <?= $formArticle->fileField($articleModel, 'image'); ?>
            </div>
            <?= $formArticle->labelEx($articleModel, 'Размер картинки не может превышать 5 MB') ?>
            <?= $formArticle->error($articleModel, 'image', array('class' => 'text-red')) ?>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-2">
            Категория
        </div>
        <div class="col-md-8">
            <?= $formArticle->dropDownList($categoryModel, 'name', CHtml::listData(Category::model()->findAll(), 'id', 'name', 'category'), array('class' => 'fieldInput', 'multiple' => 'multiple')); ?>
            <?= $formArticle->error($categoryModel, 'name', array('class' => 'text-red')) ?>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-2">
            <?= $formArticle->labelEx($articleModel, 'text') ?>
        </div>
        <div class="col-md-8">
            <?= $formArticle->textArea($articleModel, 'text', array('class' => 'fieldInput')); ?>
            <?= $formArticle->error($articleModel, 'text', array('class' => 'text-red')) ?>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <?= CHtml::submitButton('Сохранить статью', array('class' => 'button1 button1reg')); ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
<script src="//cdn.ckeditor.com/4.5.4/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('Article[text]');
    var crutchForCKEditor = function(){
        var abc = document.getElementsByTagName('iframe')[0].contentDocument;
        var abc2 = abc.childNodes[1];
        var text = abc2.childNodes[1].childNodes[0].innerHTML;
        if(text != '<br>')
            $('#Article_text').val(text);
    }
</script>