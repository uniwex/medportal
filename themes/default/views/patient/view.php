<?php
/* @var $this PatientController */
/* @var $model Patient */

?>

<h1><?= User::model()->findByPk($model->idUser)->name; ?></h1>
<div class="box box-info text-left">
    <?php $this->widget('zii.widgets.CDetailView', array(
        'data' => $model,
        'attributes' => array(
            'idUser',
            'birthday',
            'oms',
            'dms',
        ),
    )); ?>
</div>
