<?php
/* @var $model ScienceWorks */

$this->breadCrumbs = array(
    'Научные работы'=>array('index'),
    'Изменение',
);
//
//$this->menu=array(
//array('label'=>'List ScienceWorks', 'url'=>array('index')),
//array('label'=>'Manage ScienceWorks', 'url'=>array('admin')),
//);
//?>
<div id="postAdvert" class="col-md-10" data-type="ticket">
    <div class="row">
        <div class="col-md-12 userInformationHead1 no-margin">
            Изменение работы <?=$model->name?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php $this->renderPartial('_form', array('model' => $model,'edit_flag'=>1)); ?>
        </div>
    </div>
</div>
