<?php
/* @var $this ScienceController */
/* @var $model ScienceWorks */
/* @var $form CActiveForm */
$submit_button_text = 'Создать';
?>
<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'science-works-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'errorMessageCssClass' => 'control-label',
    'clientOptions' => array(
        'validateOnChange' => true,
        'validateOnSubmit' => true,
        'errorCssClass' => 'has-error',
        'successCssClass' => 'has-success'
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<div class="row form-group">
    <div class="col-md-2">
        <?= $form->labelEx($model, 'name') ?>
    </div>
    <div class="col-md-8">
        <?= $form->textField($model, 'name', array('class' => 'fieldInput')) ?>
        <?= $form->error($model, 'name', array('class' => 'text-red')) ?>
    </div>
</div>

<div class="row form-group">
    <div class="col-md-2">
        <?= $form->labelEx($model, 'description') ?>
    </div>
    <div class="col-md-8">
        <?= $form->textArea($model, 'description', array('class' => 'fieldInput', 'style' => 'width: 100%; height: 100px')) ?>
        <?= $form->error($model, 'description', array('class' => 'text-red')) ?>
    </div>
</div>

<!--<div class="form-group row">
            <div class="col-md-2">
                <? /*= $form->labelEx($model, 'link') */ ?>
            </div>
            <div class="col-md-8">
                <div class="fieldInput marginForInput">
                    <? /*= $form->fileField($model, 'link'); */ ?>
                </div>
                <? /*= $form->labelEx($model, '* Размер не может превышать 5 MB', array('class'=>'marginForInput')) */ ?>
                <? /*= $form->error($model, 'link', array('class' => 'text-red marginForInput')) */ ?>
            </div>
        </div>-->


<div class="row form-group">
    <div class="col-md-2">
        <?= $form->labelEx($model, 'link') ?>
    </div>
    <div class="col-md-8">
        <?= $form->fileField($model, 'link', array('class' => 'fieldInput no-load-preview', 'style' => 'opacity:100')) ?>
        <?= $form->error($model, 'link', array('class' => 'text-red')) ?>
    </div>
</div>

<?if (isset($edit_flag)):?>
    <? $submit_button_text = 'Изменить'; ?>
    <div class="form-group row">
        <div class="col-md-2">
            <?= $form->labelEx($model, 'picture') ?>
        </div>
        <div class="col-md-8">
            <div class="fieldInput marginForInput" id="image">
                <?= $form->fileField($model, 'picture'); ?>
            </div>
            <?= $form->labelEx($model, '* Размер картинки не может превышать 5 MB', array('class'=>'marginForInput')) ?>
            <?= $form->error($model, 'image', array('class' => 'text-red marginForInput')) ?>
        </div>
    </div>
<?endif;?>

<div class="row form-group">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <?= CHtml::submitButton($submit_button_text, array('class' => 'button1 button1reg')) ?>
    </div>
</div>


<?php $this->endWidget(); ?>

<!-- form -->
