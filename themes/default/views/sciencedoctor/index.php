<div class="container-fluid">

    <div class="scienceworks">
        <div class="row">
            <p style="height: 34px;"><a href="/sciencedoctor/create" class="button1 buttonFilter bg-liteBlue" style="margin-left: 60px;width:170px;text-decoration: none;color:#FFF;height:37px;padding:5px 16px;background-color:#456BA5;text-align:left;font-size:18px;">Добавить работу</a></p>
            <?php
            $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $dataProvider,
                'itemView' => '_view',
                'enablePagination'=>false,
                'template'=>'{items} {pager}',
            ));
            ?>
            <div class="col-md-9">

                <?php
                $this->widget('CLinkPager', array(
                    'header' => '',
                    'pages' => $pages,
                    'cssFile' => '/assets/styles/science-pager.css',
                    'firstPageLabel' => '<<',
                    'prevPageLabel' => '<',
                    'nextPageLabel' => '>',
                    'lastPageLabel' => '>>',
                ));?>
            </div>
        </div>
    </div>
</div>