<div class="col-md-3 col-sm-3 col-lg-3 col-xs-3 science-block" >
    <div class="science-block-align">
        <div class="science-img">
            <a href="/scienceworks/<?=$data->id?>"><img src="/upload/<?=$data->picture?>" width="154" height="220" alt=""></a>
        </div>
        <p><b><?=substr($data->name, 0, 87)?><?if(strlen($data->name)>87) echo '...';?></b></p>
        <p><?=User::model()->findByPk($data->idAuthor)->name?></p>
        <p>
            <a href="/scienceworks/download/id/<?=$data->id?>">Скачать</a> / <a href="/upload/<?=$data->link?>">Читать online</a>
            <br>
            <a href="/sciencedoctor/update/?id=<?=$data->id?>">Изменить</a> / <a href="/sciencedoctor/delete/?id=<?=$data->id?>">Удалить</a>
        </p>
    </div>
</div>