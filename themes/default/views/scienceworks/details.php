<div id="science-detail" class="container-fluid">
	<div class="col-md-9">
		<p id="science-details-header"><?=$model->name?></p>
		<div class="row">
			<div class="col-md-3">
				<img src="/upload/<?=$model->picture?>" width="154" height="220" alt="">
			</div>
			<div class="col-md-6">
				<p><b>Описание:</b> <?=$model->description?></p>
				<div class="dotted"></div>
				<p><b>Авторы:</b> <?=User::model()->findByPk($model->idAuthor)->name?></p>
				<div class="dotted"></div>
				<p><a href="/scienceworks/download/id/<?=$model->id?>">Скачать с нашего сервера</a> / <a href="/upload/<?=$model->link?>">Читать online</a></p>
				<div class="dotted"></div>
			</div>
		</div>
	</div>
</div>