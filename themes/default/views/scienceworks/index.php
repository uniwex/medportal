
<div class="container-fluid">
	<div class="scienceworks">
		<div class="row">
			<?php
			$this->widget('zii.widgets.CListView', array(
				'dataProvider' => $dataProvider,
				'itemView' => '_view',
				'enablePagination'=>false,
				'template'=>'{items} {pager}',
			));
			?>
			<div class="col-md-9">
				<?php
				$this->widget('CLinkPager', array(
					'header' => '',
					'pages' => $pages,
					'cssFile' => '/assets/styles/science-pager.css',
					'firstPageLabel' => '<<',
					'prevPageLabel' => '<',
					'nextPageLabel' => '>',
					'lastPageLabel' => '>>',
				));?>
			</div>
		</div>
	</div>
</div>