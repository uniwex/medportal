<div id="postAdvert" class="col-md-10">
    <div class="row form-group">
        <div class="col-md-3">
            <?php
            if($medbook->image) $image = Yii::app()->request->baseUrl.'/upload/medbook/' . $medbook->image; else $image = Yii::app()->request->baseUrl.'/assets/images/blankPhoto1.png';
            ?>
            <img class="article-img" src="<?= $image; ?>">
        </div>
        <div class="col-md-9">
            <div class="row form-group">
                <div class="col-md-6 article-author">
                    <?php
                    echo 'Администратор';
                    ?>
                </div>
                <div class="col-md-6">
                    <?= $medbook->time ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 no-padding">
                    <?php
                    $categories = "";
                    if($medbook->idCategory) {
                        foreach (explode(',',$medbook->idCategory) as $key) {
                            $categories .= Category::model()->findByPk($key)->name . ',';
                        }
                    }
                    $categories = substr($categories,0,-1);
                    ?>
                    Категория: <?= $categories; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 article-head no-padding">
                    <?= $medbook->name ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $medbook->text ?>
        </div>
    </div>
</div>