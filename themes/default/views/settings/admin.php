<?php
/* @var $this SettingsController */
/* @var $model PartnerPercent */
?>
<div class="row">
    <div class="col-md-12">

        <div class="box box-info">
            <div class="box-header with-border ui-sortable-handle">
                <h3 class="box-title">Таблица вознаграждений</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <? $this->widget('zii.widgets.grid.CGridView', array(
                    'dataProvider' => $model->search(),
                    'filter' => $model,
                    'cssFile' => Yii::app()->request->getBaseUrl(true) . '/assets/admin/css/gridview.css',
                    'itemsCssClass' => 'table no-margin',
                    'htmlOptions' => array(
                        'class' => 'table-responsive'
                    ),
                    'summaryText' => 'Показано записей: {start} - {end} из {count}',
                    'emptyText' => 'Результаты не найдены',
                    'columns' => array(
                        'id',
                        'count',
                        'percent',
                        array(
                            'class' => 'CButtonColumn',
                            'header' => 'Операции'
                        ),
                    ),
                )); ?>
            </div>
            <div class="box-footer clearfix"></div>
        </div>
    </div>
</div>