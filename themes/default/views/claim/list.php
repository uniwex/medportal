<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 26.11.2015
 * Time: 13:27
 */
$beforeAjaxUpdate = <<<JS
    function(){
        $('#preloader').css('display', 'block');
        $('#grid').css('display', 'none');
    }
JS;

$afterAjaxUpdate = <<<JS
    function(){
        var empty = $('span.empty').length;
        $('#preloader').css('display', 'none');
        $('#grid').css('display', 'block');
    }
JS;

$i = 0;
Yii::app()->session['counter'] = $i;
$list = $this->widget('zii.widgets.CListView', array(
    'id' => 'grid',
    'ajaxUpdate' => true,
    'dataProvider' => $dataProvider,
    'beforeAjaxUpdate' => $beforeAjaxUpdate,
    'afterAjaxUpdate' => $afterAjaxUpdate,
    'itemView' => '_message',
    'itemsCssClass' => 'direct-chat-messages',
    'summaryText' => 'Показано сообщений: {start} - {end} из {count}',
    'emptyText' => 'История сообщений пуста',
    'htmlOptions' => array(
        'class' => 'direct-chat-primary'
    ),
    'pager' => array(
        'class' => 'CLinkPager',
        'header' => '',
        'nextPageLabel' => 'Следующая',
        'prevPageLabel' => 'Предыдущая'
    )
));