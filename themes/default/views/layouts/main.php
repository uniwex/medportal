<?php
if (!Yii::app()->user->isGuest) {
    if (User::model()->findByPk(Yii::app()->user->id) == false) {
        $this->redirect("/site/logout");
    }
}
?>
<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Сайт по поиску врачей</title>

    <link href='<?= Yii::app()->request->baseUrl ?>/assets/styles/jquery-ui.css' media="screen" rel="stylesheet"
          type="text/css"/>
    <link href='<?= Yii::app()->request->baseUrl ?>/assets/bootstrap/css/bootstrap.css' media="screen" rel="stylesheet"
          type="text/css"/>
    <link href='<?= Yii::app()->request->baseUrl ?>/assets/styles/main.css' media="screen" rel="stylesheet"
          type="text/css"/>
    <link rel="shortcut icon"
          href=<?= '"' . Yii::app()->request->baseUrl . "/assets/images/favicon.ico\""; ?>type="image/x-icon">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <? Yii::app()->getClientScript()->registerCoreScript('jquery'); ?>
</head>
<body>
<div id="design"></div>
<!--модальное окно-->
<div id="modal-add-time" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content" style="padding-left:20px;">
            <div class="modal-body">
                <h3>Добавить время</h3>

                <p>Введите страну:</p>
                <?php $allCountries = AllRegionsHelper::getCountries(); ?>
                <select id="time-city" class="form-control"
                        style="width:265px;margin-bottom:15px;border:1px solid #AAA;" value="">
                    <?php
                    foreach ($allCountries as $item) {
                        echo '<option data-id="' . $item->id . '">' . $item->rusName . '</option>';
                    }
                    ?>
                </select>

                <div id="time-country">
                    <p>Введите город:</p>
                    <!--<input id="time-country-input" class="form-control">-->
                    <select id="time-country-select" class="form-control"
                            style="width:265px;margin-bottom:15px;border:1px solid rgb(170, 170, 170);">
                    </select>
                </div>
            </div>
            <input id="time-button-get" type="button" value="Добавить время">

            <div class="modal-footer" style="margin-right: 18px;">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div id="modal-citytimes-warning" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content" style="padding-left:20px;">
            <div class="modal-body">
                <p>Можно добавить не более 5 городов</p>
            </div>
            <div class="modal-footer" style="margin-right: 18px;">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div id="preload" class="overlay" style="z-index:9999;">
    <div id="spinner"></div>
</div>
<div class="overlay" id="overlaySchedule">
    <div class="modal-window">
        <div class="modal-content">
            <div class="modal-table">
                <table id="schedule">
                    <tr>
                        <th>Пн</th>
                        <th>Вт</th>
                        <th>Ср</th>
                        <th>Чт</th>
                        <th>Пт</th>
                        <th>Сб</th>
                        <th>Вс</th>
                    </tr>
                    <tr>
                        <th>с <input autocomplete="off" type="text"><br>до
                            <input autocomplete="off" type="text"></th>
                        <th>с <input autocomplete="off" type="text"><br>до
                            <input autocomplete="off" type="text"></th>
                        <th>с <input autocomplete="off" type="text"><br>до
                            <input autocomplete="off" type="text"></th>
                        <th>с <input autocomplete="off" type="text"><br>до
                            <input autocomplete="off" type="text"></th>
                        <th>с <input autocomplete="off" type="text"><br>до
                            <input autocomplete="off" type="text"></th>
                        <th>с <input autocomplete="off" type="text"><br>до
                            <input autocomplete="off" type="text"></th>
                        <th>с <input autocomplete="off" type="text"><br>до
                            <input autocomplete="off" type="text"></th>
                    </tr>
                </table>
            </div>
            <div class="modal-btn">
                <input type="button" class="button1" value="+ время" id="addTime">
                <input type="button" class="button1 disabled" disabled="disabled" value="- время" id="removeTime">
                <input type="button" class="button1" value="Сохранить" id="saveSchedule" onclick="SaveSchedule()">
                <input type="button" class="button1" value="Закрыть"
                       onclick="$('#overlaySchedule').css('display', 'none');">
            </div>
        </div>
    </div>
</div>

<div class="overlay" id="overlay-calendar">
    <div class="modal-window">
        <div class="close-button">X</div>
        <?
        if (!Yii::app()->user->isGuest) {
            if (User::model()->findByPk(Yii::app()->user->id)->type == 'patient') { ?>
                <div class="calendar-appointment">
                <span>
                    <a href="<?= Yii::app()->request->baseUrl . "/visit" ?>">
                        <i class="fa fa-calendar-plus-o"><span
                                style="font-family: MyriadProRegular;font-weight: 100;font-size: 12pt;">Записаться к врачу</span></i>
                    </a>
                </span>
                </div>
            <?php }
        }
        ?>

        <div class="calendar-appointment"><span><a href="<?= Yii::app()->request->baseUrl . "/note" ?>"><i
                        class="fa fa-calendar-plus-o"><span
                            style="font-family: MyriadProRegular;font-weight: 100;font-size: 12pt;">Создать заметку</span></i></a></span>
        </div>
        <hr class="modal-hr">
        <div id="calendar-on-day">
        </div>
    </div>
</div>

<nav class="navbar navbar-inverse" role="navigation">
    <div class="container" style="padding-left:2px;margin-top:7px;">
        <div class="navbar-header" style="margin-right:0px;">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <!--div class="hidden-md hidden-xs hidden-sm phone-header col-md-3 col-lg-3 no-padding" style="max-width:220px;">
            <span id="firstSite">Первый сайт по поиску врачей</span>
        </div-->
        <div class="collapse navbar-collapse col-md-9 col-lg-9 pull-right" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right" id="navMenu">
                <li><a href="/site/eyeimpired" id="eyeimpired" class="text-black">Слабовидящим</a></li>
                <li class="navColor hidden-sm"><a href="http://vk.com/vrach_help">Онлайн консультация</a></li>
                <li class="navColor"><a href='<?= Yii::app()->request->baseUrl ?>/digest'>Дайджест соцсетей</a></li>
                <li class="navColor"><a href="/forum">Форум</a></li>
                <li class="navColor"><a href='<?= Yii::app()->request->baseUrl ?>/polls'>Опросы</a></li>
                <? if (Yii::app()->user->id) { ?>
                    <li class="navColor"><a href="/tickets">Вопрос-ответ</a></li>
                <?php } ?>
                <?php if (User::isAdmin()) { ?>
                    <li><a class="navColor" href="/clinic/admin">Админ.панель</a></li>
                <?php } ?>
                <?php
                if (!Yii::app()->user->id) {
                    ?>
                    <li><a class="navColor" href='<?= Yii::app()->request->baseUrl ?>/site/registration'>Регистрация</a>
                    </li>
                    <li><a class="navColor" href="<?= Yii::app()->request->baseUrl ?>/site/login">Вход</a></li>
                    <?php
                } else {
                ?>
                <?php if (User::model()->findByPk(Yii::app()->user->id)->type == 'nurse') { ?>
                    <li><a id="toggle-user-menu" style="line-height:1.6;padding-top:1px;">Личный кабинет<i
                                class="fa fa-caret-down"></i></a>
                        <ul id="user-menu" style="display:none;">
                            <li><a href="<?= Yii::app()->request->baseUrl ?>/site/">Заявки</a></li>
                            <li>
                                <a href="<?= Yii::app()->request->baseUrl . '/site/' . User::model()->findByPk(Yii::app()->user->id)->type . '/' . Yii::app()->user->id; ?>">Настройки</a>
                            </li>
                            <li><a href="<?= Yii::app()->request->baseUrl ?>/site/">Заметки</a></li>
                            <li>
                                <a href="<?= Yii::app()->request->baseUrl . '/site/' ?>">Пригласить коллегу</a>
                            </li>
                            <li style="margin-top: 8px;"><a style="color:black;"
                                                            href="<?= Yii::app()->request->baseUrl ?>/site/logout/">Выйти</a>
                            </li>
                        </ul>
                    </li>

                <?php } else if (User::model()->findByPk(Yii::app()->user->id)->type == 'clinic') { ?>
                <li><a id="toggle-user-menu" style="line-height:1.6;padding-top:1px;">Личный кабинет<i
                            class="fa fa-caret-down"></i></a>
                    <ul id="user-menu" style="display:none;">

                        <li>
                            <a href="<?= Yii::app()->request->baseUrl . '/site/' . User::model()->findByPk(Yii::app()->user->id)->type . '/' . Yii::app()->user->id; ?>">Настройки</a>
                        </li>
                        <li><a href="<?= Yii::app()->request->baseUrl ?>/site/">Заметки</a></li>
                        <listyle
                        ="margin-top: 8px;" ><a style="color:black;"
                                                href="<?= Yii::app()->request->baseUrl ?>/site/logout/">Выйти</a>
                </li>
            </ul>
            </li>

            <?php } else if (User::model()->findByPk(Yii::app()->user->id)->type == 'doctor') { ?>
                <li><a id="toggle-user-menu" style="line-height:1.6;padding-top:1px;">Личный кабинет<i
                            class="fa fa-caret-down"></i></a>
                    <ul id="user-menu" style="display:none;">
                        <li><a href="<?= Yii::app()->request->baseUrl ?>/site/">Записи на прием</a></li>
                        <li>
                            <a href="<?= Yii::app()->request->baseUrl . '/site/' . User::model()->findByPk(Yii::app()->user->id)->type . '/' . Yii::app()->user->id; ?>">Настройки</a>
                        </li>
                        <li><a href="<?= Yii::app()->request->baseUrl ?>/site/">Заметки</a></li>
                        <li><a href="<?= Yii::app()->request->baseUrl . '/sciencedoctor/' ?>">Мои работы</a></li>
                        <li>
                            <a href="<?= Yii::app()->request->baseUrl . '/site/' ?>">Пригласить коллегу</a>
                        </li>
                        <li style="margin-top: 8px;"><a style="color:black;"
                                                        href="<?= Yii::app()->request->baseUrl ?>/site/logout/">Выйти</a>
                        </li>
                    </ul>
                </li>

            <?php } else if (User::model()->findByPk(Yii::app()->user->id)->type == 'patient') { ?>
                <li><a id="toggle-user-menu" style="line-height:1.6;padding-top:1px;">Личный кабинет<i
                            class="fa fa-caret-down"></i></a>
                    <ul id="user-menu" style="display:none;">
                        <li><a href="<?= Yii::app()->request->baseUrl ?>/site/">Медицинская карта</a></li>
                        <!--<li><a href="<? /*= Yii::app()->request->baseUrl . '/site/' . User::model()->findByPk(Yii::app()->user->id)->type . '/' . Yii::app()->user->id; */ ?>">Настройки</a></li>-->
                        <li><a href="<?= Yii::app()->request->baseUrl ?>/site/">Заметки</a></li>
                        <li>
                            <a href="<?= Yii::app()->request->baseUrl . '/site/' ?>">Пригласить врача</a>
                        </li>
                        <li style="margin-top: 8px;"><a style="color:black;"
                                                        href="<?= Yii::app()->request->baseUrl ?>/site/logout/">Выйти</a>
                        </li>

                    </ul>
                </li>
            <?php } else { ?>
                <li><a style="color:black" href="<?= Yii::app()->request->baseUrl ?>/site/logout">Выйти</a></li>
            <?php } ?>
            <?php
            }
            ?>
            </ul>
        </div>
    </div>
</nav>

<div class="container">
    <div class="row">
        <div class="col-md-12" style="padding-left:5px;">

            <div class="pull-left" id="blueCross">
                <a href="/">
                    <img class="pull-left" src='<?= Yii::app()->request->baseUrl ?>/assets/images/blueCross.png'>

                    <div class="pull-right" id="regToDoctor"><span style="letter-spacing:-0.5px;">ЗАПИСЬ К </span><span
                            style="letter-spacing:0.5px;">ДОКТОРУ</span></div>
                </a>
            </div>

            <div class="pull-right" id="contacts">
                <div class="row" style="margin-right:-20px;">
                    <a href="/site/inProcess" style="text-decoration:none;">
                        <div class="order">
                            <img class="phoneIcon" src='<?= Yii::app()->request->baseUrl ?>/assets/images/phone1.png'>
                            Заказать звонок
                        </div>
                    </a>

                    <div class="phoneNumber">8 (495) 777 77 00</div>
                    <!--<div class="language">Eng</div>-->
                </div>
                <div class="row">
                    <input class="pull-right" id="siteSearch" placeholder="Поиск по сайту..">

                    <div id="language" class="rus">
                        <span>Rus</span>
                        <ul id="ul-language" style="display:none;">
                            <li data-img="/assets/images/ru_flag.png">Rus</li>
                            <li data-img="/assets/images/eng_flag.png">Eng</li>
                            <li data-img="/assets/images/chn_flag.png">Chn</li>
                        </ul>
                    </div>
                    <span id="searchButton"><i class="fa fa-search"></i></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row indent search">
        <div class="col-md-2 col-xs-2">
            <div id="clock">
                <div style="height:37px;">
                    <button>Изменить</button>
                    <span class="title">Часы</span>
                    <img src="/assets/images/plus1.png">
                </div>
                <?php
                $worldtimes = $this->widget('WorldtimeWidget')->run();
                //
                //                    $worldtimes = array(
                //                        array('id'=>1,'city'=>'Ch','country'=>'R','time'=>'00:00','dayOfWeek'=>'monday'),
                //                        array('id'=>2,'city'=>'Moscow','country'=>'R','time'=>'00:00','dayOfWeek'=>'monday'),
                //                        array('id'=>3,'city'=>'Ch','country'=>'R','time'=>'00:00','dayOfWeek'=>'monday'),
                //                        array('id'=>4,'city'=>'Moscow','country'=>'R','time'=>'00:00','dayOfWeek'=>'monday'),
                //                        array('id'=>5,'city'=>'Ch','country'=>'R','time'=>'00:00','dayOfWeek'=>'monday')
                //                    );

                foreach ($worldtimes as $item) {
                    ?>
                    <div data-id="<?= $item['id']; ?>" class="item">
                        <i class="fa fa-times"></i>

                        <div class="city">
                            <span><?= $item['city']; ?></span>
                            <br>
                            <span><?= $item['country']; ?></span>
                        </div>
                        <div class="clock">
                            <span><?= $item['time']; ?></span>
                            <br>
                            <span><?= $item['dayOfWeek']; ?></span>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="col-md-8 col-xs-10 wrap-search-form">
            <ul class="doctors-search-tabs">
                <li class="active pane-doctor-lg pane-doctor-md pane-doctor-sm"><a href="#searchDoctor"
                       data-toggle="tab">Поиск врача</a></li>
                <li class="pane-clinic-lg pane-clinic-md pane-clinic-sm"><a href="#searchClinic" data-toggle="tab">Поиск
                        клиники</a></li>
                <li class="pane-nurse-lg pane-nurse-md pane-nurse-sm"><a href="#searchNurse" data-toggle="tab">Поиск
                        сиделки</a></li>
            </ul>
            <? $form = $this->beginWidget('CActiveForm', array(
                'id' => 'search-form',
                'action' => Yii::app()->request->baseUrl . '/search/',
            )) ?>
            <?= CHtml::hiddenField('type', '') ?>
            <div class="tab-content">
                <div class="tab-pane active" id="searchDoctor">
                    <input type="text" name="doctor" placeholder="Поиск врача...">
                    <input type="submit" name="submitDoctor" style="width:120px;margin-right:10px;" value="Найти">
                </div>

                <div class="tab-pane" id="searchClinic">
                    <input type="text" name="clinic" placeholder="Поиск клиники...">
                    <input type="submit" name="submitClinic" style="width:120px;margin-right:10px;" value="Найти">
                </div>

                <div class="tab-pane" id="searchNurse">
                    <input type="text" name="nurse" placeholder="Поиск сиделки...">
                    <input type="submit" name="submitNurse" style="width:120px;margin-right:10px;" value="Найти">
                </div>

                <div class="row search-detail">
                    <div class="col-md-5 col-xs-6 col-sm-5">
                        <div class="form-group">
                            <label class="control-label">Найдите врача в другой стране</label>
                            <?php
                            $userModel = new User();
                            echo $form->dropDownList($userModel, 'country', array(0 => 'Россия', 1 => 'Россия'), array('class' => 'form-control', 'data-alias' => 'country', 'style' => ''));
                            ?>
                        </div>
                    </div>

                    <div class="col-md-5 col-xs-6 col-sm-5 col-sm-push-1 col-md-push-0 right-block-find">
                        <div class="form-group">
                            <label class="control-label" style="margin-left: 75px;">Найдите врача в своем городе</label>
                            <?
                            echo $form->textField($userModel, 'city', array('class' => 'form-control inputForSearchCity', 'style' => 'width:92%;font-size:14pt;border-radius:10px;height:32px;margin-left: 68px;margin-top:2px;border:none;background-image:url(/assets/images/plus1.png);background-position:98% center;background-repeat:no-repeat;', 'value' => 'Москва')); ?>
                            <select class="dropDownCities mainPage-search" type="select" size="19" data-alias="city"
                                    style="left: 54px;width:84%;background:none;background-color:#FFF;color:#555555;font-size:14pt;margin-top:0px;position:absolute;border:1px solid #000;background-image:none !important;">
                                <option>Выберите город</option>
                            </select>

                            <div class="height visible-xs visible-sm"></div>
                        </div>
                    </div>

                    <div class="col-md-2 text-center no-padding right-metro">
                        <div class="form-group">
                            <ul>
                                <li><a href="#"
                                       onclick="$('#stationModal').modal('show')"
                                       data-id="metro"
                                       title="Поиск по метро">М</a></li>
                                <!--                                <li><a href="#" data-id="map" data-toggle="tooltip" data-placement="bottom"-->
                                <!--                                       title="Поиск по карте">К</a></li>-->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="stationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="modalLabel">Выберите станцию метро</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row" data-empty="metro" style="display: none">
                                <div class="col-md-12">
                                    <p>Для этого города нет станций метро</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4" data-col="0"></div>
                                <div class="col-md-4" data-col="1"></div>
                                <div class="col-md-4" data-col="2"></div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Сохранить изменения
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <? $this->endWidget(); ?>
        </div>
        <div class="hidden-sm hidden-xs col-md-2" id="calendar">
            <?php
            $year = date('Y');
            $month = date('m');
            ?>
            <span id="calendar-month" style="display:none;"><?= $month; ?></span>
            <h4 class="text-center"><a id="calendar-link" href="<?= Yii::app()->request->baseUrl . '/events'; ?>">Предстоящие
                    события</a></h4>
            <hr>
            <div class="month"><span id="calendar-month-left" style="padding-left:0px;">«</span><span
                    id="calendar-month-name"><?php $month = date('m');
                    switch ($month) {
                        case 1:
                            echo "Январь";
                            break;
                        case 2:
                            echo "Февраль";
                            break;
                        case 3:
                            echo "Март";
                            break;
                        case 4:
                            echo "Апрель";
                            break;
                        case 5:
                            echo "Май";
                            break;
                        case 6:
                            echo "Июнь";
                            break;
                        case 7:
                            echo "Июль";
                            break;
                        case 8:
                            echo "Август";
                            break;
                        case 9:
                            echo "Сентябрь";
                            break;
                        case 10:
                            echo "Октябрь";
                            break;
                        case 11:
                            echo "Ноябрь";
                            break;
                        case 12:
                            echo "Декабрь";
                            break;
                    }
                    ?></span><span id="calendar-year"><?= date('Y'); ?></span><span id="calendar-month-right">»</span>
            </div>
            <table class="week">
                <tr>
                    <td>Пн</td>
                    <td>Вт</td>
                    <td>Ср</td>
                    <td>Чт</td>
                    <td>Пт</td>
                    <td class="weekend">Сб</td>
                    <td class="weekend">Вс</td>
                </tr>
            </table>
            <span id="calendar-content">
            <?php
            //GENERATING CALENDAR
            $beginOfMonth = strtotime($year . "-" . $month . "-01");
            $firstDayOfWeek = date('N', $beginOfMonth);
            $daysFirstWeek = 8 - $firstDayOfWeek;
            $lastDayOfMonth = date('t', strtotime($year . "-" . $month . "-01"));
            $endOfMonth = strtotime($year . "-" . $month . "-" . $lastDayOfMonth);
            $daysLastWeek = date('N', $endOfMonth);
            $countFullWeeks = ($lastDayOfMonth - $daysFirstWeek - $daysLastWeek) / 7;
            $day = 1;

            $criteria = new CDbCriteria;
            $userId = Yii::app()->user->id;
            if (!Yii::app()->user->isGuest) {

                $userType = User::model()->findByPk(Yii::app()->user->id)->type;
                $events = array();
                //get data from database
                if ($userType == 'patient') {
                    $criteria->condition = 'idOwner=:idOwner';
                    $criteria->params = array(':idOwner' => Yii::app()->user->id);
                    $events = Event::model()->findAll($criteria);
                }
                if ($userType == 'doctor') {
                    $criteria->condition = '(idOwner=:idOwner OR idExecuter=:idExecuter) AND type<>:type';
                    $criteria->params = array(':idOwner' => Yii::app()->user->id, ':idExecuter' => Yii::app()->user->id, 'type' => 'nurse');
                    $events = Event::model()->findAll($criteria);
                }
                if ($userType == 'nurse') {
                    $criteria->condition = '(idOwner=:idOwner OR idExecuter=:idExecuter) AND type<>:type';
                    $criteria->params = array(':idOwner' => Yii::app()->user->id, ':idExecuter' => Yii::app()->user->id, 'type' => 'schedule');
                    $events = Event::model()->findAll($criteria);
                }
                if ($userType == 'clinic') {
                    $doctors = Schedule::model()->findAllByAttributes(array('idUser' => Yii::app()->user->id));
                    foreach ($doctors as $doctor) {
                        $criteria->condition = '(idOwner=:idOwner OR idExecuter=:idExecuter) AND type=:type';
                        $criteria->params = array(':idOwner' => $doctor->id, ':idExecuter' => $doctor->id, 'type' => 'schedule');
                        $events = array_merge(Event::model()->findAll($criteria), $events);
                    }
                    $criteria->condition = 'idOwner=:idOwner AND type=:type';
                    $criteria->params = array(':idOwner' => Yii::app()->user->id, 'type' => 'note');
                    $events = array_merge(Event::model()->findAll($criteria), $events);
                }
            }
            //display calendar
            echo '<table class="days"><tr>';
            //first week
            $tempI;
            for ($i = 1; $i <= 7; $i++) {
                if ($i >= $firstDayOfWeek) {
                    $weekend = "";
                    $wrote = false;//if number in calendar already highlighted
                    if ($i > 5)
                        $weekend = " weekend";
                    $class = "class=\"";
                    if ($userId) {
                        foreach ($events as $key) {
                            if ($key->type != 'nurse') {
                                ($day < 10) ? $tempI = '0' . $day : $tempI = $day;
                                if (($key->eventDate == $year . "-" . $month . "-" . $tempI) && ($wrote == false)) {
                                    $wrote = true;
                                    $class .= "busyDay";
                                }
                            }
                            if ($key->type == 'nurse') {
                                $beginDateSeconds = strtotime($key->eventDate);
                                $endDateSeconds = strtotime($key->eventDateFinish);
                                $curDateSeconds = strtotime($year . "-" . $month . "-" . $day);
                                if (($beginDateSeconds <= $curDateSeconds) && ($curDateSeconds <= $endDateSeconds) && ($wrote == false)) {
                                    $wrote = true;
                                    $class .= "busyDay";
                                }
                            }
                        }
                    }
                    $class .= $weekend . " calendar-day\"";
                    echo '<td ' . $class . '>' . $day . '</td>';
                    $day++;
                } else {
                    echo '<td></td>';
                }
            }
            echo '</tr></table>';
            $tempDay = $day;
            //full weeks
            for ($i = 1; $i <= $countFullWeeks; $i++) {
                echo '<table class="days"><tr>';
                for ($j = 1; $j <= 7; $j++) {
                    $weekend = "";
                    $wrote = false;//if number in calendar already highlighted
                    if ($j > 5)
                        $weekend = " weekend";
                    $class = "class=\"";
                    if ($userId) {
                        foreach ($events as $key) {
                            if ($key->type != 'nurse') {
                                ($day < 10) ? $tempI = '0' . $day : $tempI = $day;
                                if (($key->eventDate == $year . "-" . $month . "-" . $tempI) && ($wrote == false)) {
                                    $class .= "busyDay";
                                    $wrote = true;
                                }
                            }
                            if ($key->type == 'nurse') {
                                $beginDateSeconds = strtotime($key->eventDate);
                                $endDateSeconds = strtotime($key->eventDateFinish);
                                $curDateSeconds = strtotime($year . "-" . $month . "-" . $day);
                                if (($beginDateSeconds <= $curDateSeconds) && ($curDateSeconds <= $endDateSeconds) && ($wrote == false)) {
                                    $class .= "busyDay";
                                    $wrote = true;
                                }
                            }
                        }
                    }
                    $class .= $weekend . " calendar-day\"";
                    echo '<td ' . $class . '>' . $day . '</td>';
                    $day++;
                }
                echo '</tr></table>';
            }
            //last week
            echo '<table class="days"><tr>';
            for ($i = 1; $i <= 7; $i++) {
                if ($i <= $daysLastWeek) {
                    $weekend = "";
                    $wrote = false;//if number in calendar already highlighted
                    if ($i > 5)
                        $weekend = " weekend";
                    $class = "class=\"";
                    if ($userId) {
                        foreach ($events as $key) {
                            if ($key->type != 'nurse') {
                                ($day < 10) ? $tempI = '0' . $day : $tempI = $day;
                                if (($key->eventDate == $year . "-" . $month . "-" . $tempI) && ($wrote == false)) {
                                    $class .= "busyDay";
                                    $wrote = true;
                                }
                            }
                            if ($key->type == 'nurse') {
                                $beginDateSeconds = strtotime($key->eventDate);
                                $endDateSeconds = strtotime($key->eventDateFinish);
                                $curDateSeconds = strtotime($year . "-" . $month . "-" . $day);
                                if (($beginDateSeconds <= $curDateSeconds) && ($curDateSeconds <= $endDateSeconds) && ($wrote == false)) {
                                    $class .= "busyDay";
                                    $wrote = true;
                                }
                            }
                        }
                    }
                    $class .= $weekend . " calendar-day\"";
                    echo '<td ' . $class . '>' . $day . '</td>';
                    $day++;
                } else {
                    echo '<td></td>';
                }
            }
            echo '</tr></table>';
            ?>
            </span>
        </div>
    </div>
    <div class="row indent" style="margin-bottom: 18px;">
        <div class="col-md-2 col-xs-2 specialty">
            <span>Специальность</span>
        </div>
        <div class="col-md-7 col-xs-6 breadCrumbs">
            <?
            if (!(isset($this->breadCrumbsHome))) {
                $this->widget('zii.widgets.CBreadcrumbs', array(

                    'homeLink' => CHtml::link('Главная','/site/main'),//$breadCrumbsHome,
                    'links' => $this->breadCrumbs
                ));
            } else
                echo '<div class="breadcrumbs">' . $this->breadCrumbsHome . '</div>';
            ?>
            <div class="forPrint"><img src="/assets/images/icons/23.png"
                                       style="margin-right:7px;width:12px;height:14px;">Версия для печати
            </div>
        </div>
        <div id="wrap-social" class="col-md-3 col-xs-4 pull-right">
            <div class="pull-left social" style="margin-right:-5px;">
                <a href="<?= "http://vk.com/" . Yii::app()->params['socialNetLink']; ?>"><img class="arrowRight"
                                                                                              src="<?= Yii::app()->request->baseUrl ?>/assets/images/icon_vkontakte.png"></a>
                <a href="<?= "http://odnoklassniki.ru/" . Yii::app()->params['socialNetLink']; ?>"><img
                        class="arrowRight"
                        src="<?= Yii::app()->request->baseUrl ?>/assets/images/icon_odnoklassniki.png"></a>
                <a href="<?= "http://facebook.com/" . Yii::app()->params['socialNetLink']; ?>"><img class="arrowRight"
                                                                                                    src="<?= Yii::app()->request->baseUrl ?>/assets/images/icon_facebook.png"></a>
                <a href="<?= "http://twitter.com/" . Yii::app()->params['socialNetLink']; ?>"><img class="arrowRight"
                                                                                                   src="<?= Yii::app()->request->baseUrl ?>/assets/images/icon_twitter.png"></a>
                <a href="<?= "http://instagram.com/" . Yii::app()->params['socialNetLink']; ?>"><img class="arrowRight"
                                                                                                     src="<?= Yii::app()->request->baseUrl ?>/assets/images/icon_instagram.png"></a>
                <a href="<?= "http://linkedin.com/" . Yii::app()->params['socialNetLink']; ?>"><img class="arrowRight"
                                                                                                    src="<?= Yii::app()->request->baseUrl ?>/assets/images/linkedin1.png"></a>
            </div>
        </div>
    </div>

    <div id="baseBlock" class="row indent" style="margin-right:-30px;">
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 menuSpeciality indent" style="">
            <ul>
                <li><a href="/search?specialization=45">Терапевт</a></li>
                <li><a href="/search?specialization=33">Онколог</a></li>
                <li><a href="/search?specialization=43">Стоматолог</a></li>
                <li><a href="/search?specialization=49">Хирург</a></li>
                <li><a href="/search?specialization=13">Дерматолог</a></li>
                <li><a href="/search?specialization=31">ЛОР</a></li>
                <li><a href="/search?specialization=11">Гинеколог</a></li>
                <li><a href="/search?specialization=47">Уролог</a></li>
                <li><a href="/search?specialization=26">Невролог</a></li>
                <li><a href="/search?specialization=36">Педиатр</a></li>
            </ul>
            <div class="entireList indent">
                <div id="allSpec" class="text-center" style="padding:2px 0px;"><p>Все специальности <i class="fa fa-caret-down all-main-caret"></i></p></div>
            </div>
            <script>
                $('#allSpec').on("click", function(){
                    location.href='/specializations';
                });
            </script>
            <div class="col-md-2 col-xs-2 specialty">
                <span>Заболевания</span>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 menuSpeciality" id="diseaseList">
                <ul>
                    <li><a href="/disease/view/553">Сахарный диабет</a></li>
                    <li><a href="/disease/view/120">Гипертония</a></li>
                    <li><a href="/disease/view/228">Инсульт</a></li>
                    <li><a href="/disease/view/909">Бессонница</a></li>
                    <li><a href="/disease/view/230">Инфаркт</a></li>
                    <li><a href="/disease/view/816">Аллергия</a></li>
                    <li><a href="/disease/direction/17">Онкология</a></li>
                    <li><a href="/disease/view/503">Псориаз</a></li>
                    <li><a href="/disease/view/166">Дисбактериоз</a></li>
                    <li><a href="/disease/view/63">Гастрит</a></li>
                </ul>
                <div id="allDiseases" class="entireList indent">
                    <div class="text-center" style="padding:2px 0px;"><p>Все заболевания <i class="fa fa-caret-down all-main-caret"></i></p></div>
                </div>
                <script>
                    $('#allDiseases').on("click", function(){
                        location.href='/disease';
                    });
                </script>
                <div class="help-home">
                    Помощь на дому
                </div>
            </div>

        </div>
        <?= $content; ?>
    </div>

    <div id="partner-pre" class="row partner">
        <div class="col-md-2 contacts-md">
            <a href="/site/inProcess">
                <img class="img-left" src="<?= Yii::app()->request->baseUrl ?>/assets/images/icons/21.png">
                <span class="our-partners">Контакты</span>
            </a>
        </div>
    </div>
    <div id="partner-post" class="row partner">
        <div class="col-md-2 col-xs-10 col-md-push-0">
            <a href="/site/inProcess">
                <img class="img-left" src="<?= Yii::app()->request->baseUrl ?>/assets/images/icons/22.png">
                <span class="our-partners">Наши партнёры</span>
            </a>
            <a href="/site/inProcess">
                <img class="img-left img-site-help"
                     src="<?= Yii::app()->request->baseUrl ?>/assets/images/icons/23.jpg">
                <span class="our-partners site-help">Помощь по сайту</span>
            </a>
        </div>
        <div class="col-md-10 col-xs-12 col-md-push-0" style="padding-left:30px;">
            <div class="row">
                <div class="col-md-3 col-xs-6 rows-partners" style="padding-right: 0px;">
                    <a href="http://normoprotein.ru/">
                        <h5 class="desc-partner">Лечебное питание для больниц:</h5>

                        <div class="row" style="margin-top:15px;">
                            <div class="col-md-6">
                                <img src="<?= Yii::app()->request->baseUrl ?>/assets/images/banner1.png">
                            </div>
                            <div class="col-md-6" style="padding:0px;margin-left:0px;padding-left:10px;">
                                <img style="height:30px;width:auto;"
                                     src="<?= Yii::app()->request->baseUrl ?>/assets/images/banner1_1.png">

                                <div class="partners-text">СБКС "НОРМОПРОТЕИН"</div>
                                <a href="http://normoprotein.ru/" class="partner-link">normoprotein.ru</a>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 rows-partners" style="padding-right: 0px;">
                    <a href="http://фотодитазин.рф">
                        <h5 class="desc-partner">Фотодинамическая терапия:</h5>

                        <div class="row" style="margin-top:15px;">
                            <div class="col-md-6">
                                <img src="<?= Yii::app()->request->baseUrl ?>/assets/images/banner2.png">
                            </div>
                            <div class="col-md-6" style="padding:0px;margin-left:0px;padding-left:10px;">
                                <img style="height:35px;width:auto;"
                                     src="<?= Yii::app()->request->baseUrl ?>/assets/images/banner2_1.png">

                                <div class="partners-text">Иннованция в онкологии</div>
                                <a href="http://фотодитазин.рф" class="partner-link">фотодитазин.рф</a>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 rows-partners" style="padding-right: 0px;">
                    <a href="http://markagoda.ru/">
                        <h5 class="desc-partner">Добровольная сертификация:</h5>

                        <div class="row" style="margin-top:15px;">
                            <div class="col-md-6">
                                <img src="<?= Yii::app()->request->baseUrl ?>/assets/images/banner3.png">
                            </div>
                            <div class="col-md-6" style="padding:0px;margin-left:0px;padding-left:10px;">
                                <img style="height:25px;width:auto;"
                                     src="<?= Yii::app()->request->baseUrl ?>/assets/images/banner3_1.png">

                                <div class="partners-text">Космецевтика спец.питание<br>БАД</div>
                                <a href="http://markagoda.ru/" class="partner-link">markagoda.ru</a>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-xs-6 rows-partners" style="padding-right: 0px;">
                    <a href="https://legal.report">
                        <h5 class="desc-partner">Юридическое издание:</h5>

                        <div class="row legal-report">
                            <div class="col-md-6">
                                <img src="<?= Yii::app()->request->baseUrl ?>/assets/images/banner4.png">
                            </div>
                            <div class="col-md-6" style="padding:0px;margin-left:0px;padding-left:10px;">
                                <img id="legal-news"
                                     src="<?= Yii::app()->request->baseUrl ?>/assets/images/banner4_1.png">

                                <div class="partners-text">Полезные<br>правовые<br>новости</div>
                                <a href="http://legal.report" class="partner-link">legal.report</a>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript" src='<?= Yii::app()->request->baseUrl ?>/assets/scripts/jquery-ui.js'></script>
<script type="text/javascript" src='<?= Yii::app()->request->baseUrl ?>/assets/bootstrap/js/bootstrap.js'></script>
<script type="text/javascript" src='<?= Yii::app()->request->baseUrl ?>/assets/scripts/main2.js'></script>
<script type="text/javascript" src='<?= Yii::app()->request->baseUrl ?>/assets/scripts/globalSearch.js'></script>
</body>
</html>