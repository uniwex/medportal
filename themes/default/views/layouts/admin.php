<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Админка</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <? Yii::app()->clientScript->registerCoreScript('jquery'); ?>
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet"
          href="<?= Yii::app()->request->getBaseUrl(true); ?>/assets/admin/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= Yii::app()->request->getBaseUrl(true); ?>/assets/admin/bootstrap/css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= Yii::app()->request->getBaseUrl(true); ?>/assets/admin/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet"
          href="<?= Yii::app()->request->getBaseUrl(true); ?>/assets/admin/dist/css/skins/_all-skins.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet"
          href="<?= Yii::app()->request->getBaseUrl(true); ?>/assets/admin/plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet"
          href="<?= Yii::app()->request->getBaseUrl(true); ?>/assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <link rel="stylesheet"
          href="<?= Yii::app()->request->getBaseUrl(true); ?>/assets/admin/bootstrap/css/bootstrap-multiselect.css">
    <link rel="stylesheet"
          href="<?= Yii::app()->request->getBaseUrl(true); ?>/assets/styles/adminPanel.css">
    <!--  user style  -->
    <link rel="stylesheet" href="<?= Yii::app()->request->getBaseUrl(true); ?>/assets/admin/css/styles.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>A</b>D</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Admin</b></span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">

            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>

            </a>

            <div class="navbar-custom-menu">
                <div class="col-md-12 but">
                    <a href="<?= Yii::app()->request->getBaseUrl(true) ?>/Site/logout"
                       class="btn btn-danger square-btn-adjust pull-right">Выход</a>
                </div>
                <ul class="nav navbar-nav">

                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">

                        <ul class="dropdown-menu">
                            <!-- User image -->

                            <!-- Menu Body -->
                            <li class="user-body">
                                <div class="row">
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Followers</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Sales</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Friends</a>
                                    </div>
                                </div>
                                <!-- /.row -->
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="#" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <a href="#" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">


            </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-users"></i> <span>Пользователи</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/user/admin"><i class="fa fa-cog"></i> Список пользователей</a></li>
                        <li><a href="/user/create"><i class="fa fa-plus"></i> Добавить пользвателя</a></li>
                    </ul>
                </li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-user-md"></i> <span>Частные врачи</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/doctor/admin"><i class="fa fa-cog"></i> Список врачей</a></li>
                        <li><a href="/doctor/create"><i class="fa fa-plus"></i> Добавить врача</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-medkit"></i> <span>Сиделки</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/nurse/admin"><i class="fa fa-cog"></i> Список сиделок</a></li>
                        <li><a href="/nurse/create"><i class="fa fa-plus"></i> Добавить сиделку</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-male"></i> <span>Пациент</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/patient/admin"><i class="fa fa-cog"></i> Список пациентов</a></li>
                        <li><a href="/patient/create"><i class="fa fa-plus"></i> Добавить пациента</a></li>
                    </ul>
                </li>
                <li class="treeview" data-li="clinic">
                    <a href="#">
                        <i class="fa fa-hospital-o"></i> <span>Клиники</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li data-li="view-clinic">
                            <a href="/clinic/admin"><i class="fa fa-circle-o"></i>Список клиник</a>
                        </li>
                        <li data-li="create-clinic">
                            <a href="/clinic/create"><i class="fa fa-plus"></i>Добавление клиники</a>
                        </li>
                        <li class="treeview" data-li="clinic">
                            <a href="#">
                                <i class="fa fa-hospital-o"></i> <span>Режим работы</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li data-li="view-clinic">
                                    <a href="/visitingHours/admin"><i class="fa fa-circle-o"></i>Список режимов</a>
                                </li>
                                <li data-li="create-clinic">
                                    <a href="/visitingHours/create"><i class="fa fa-plus"></i>Добавление режима</a>
                                </li>
                            </ul>
                        </li>
                        <li class="treeview" data-li="clinic">
                            <a href="#">
                                <i class="fa fa-hospital-o"></i> <span>Доп. инфо</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li data-li="view-clinic">
                                    <a href="/addInfo/admin"><i class="fa fa-circle-o"></i>Список информации</a>
                                </li>
                                <li data-li="create-clinic">
                                    <a href="/addInfo/create"><i class="fa fa-plus"></i>Добавление информации</a>
                                </li>
                            </ul>
                        </li>
                        <li class="treeview" data-li="clinic">
                            <a href="#">
                                <i class="fa fa-hospital-o"></i> <span>Фото/видео</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li data-li="view-clinic">
                                    <a href="/clinicMedia/admin"><i class="fa fa-circle-o"></i>Список фото/видео</a>
                                </li>
                                <li data-li="create-clinic">
                                    <a href="/clinicMedia/create"><i class="fa fa-plus"></i>Добавление фото/видео</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-comments-o"></i> <span>Отзывы</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/review/admin"><i class="fa fa-cog"></i> Список отзывов</a></li>
                        <li><a href="/review/create"><i class="fa fa-plus"></i> Добавить отзыв</a></li>
                    </ul>
                </li>
                <li class="treeview" data-li="statistics">
                    <a href="#">
                        <i class="fa fa-bar-chart-o"></i> <span>Статистика</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li data-li="general"><a href="/statistics/general"><i class="fa fa-circle-o"></i>Общая статистика</a></li>
                        <li data-li="pay"><a href="/statistics/pay"><i class="fa fa-circle-o"></i>Статистика оплат</a></li>
                        <li data-li="referral"><a href="/statistics/referral"><i class="fa fa-circle-o"></i>Статистика РС</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-pie-chart"></i> <span>Реферальная система</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/settings/admin"><i class="fa fa-cog"></i> Настройка РС</a></li>
                        <li><a href="/settings/create"><i class="fa fa-plus"></i> Добавить запись</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="/claim"><i class="fa fa-ticket"></i> <span>Тикеты</span></a>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-pie-chart"></i> <span>Научные работы</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/science/admin"><i class="fa fa-cog"></i> Список работ</a></li>
                        <li><a href="/science/create"><i class="fa fa-plus"></i> Добавить работу</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-pie-chart"></i> <span>Сравнительный анализ</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/lightArticle/admin"><i class="fa fa-cog"></i> Список статей</a></li>
                        <li><a href="/lightArticle/create"><i class="fa fa-plus"></i> Добавить статью</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-pie-chart"></i> <span>Опросы</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/pollsManage/admin"><i class="fa fa-cog"></i> Список опросов</a></li>
                        <li><a href="/pollsManage/create"><i class="fa fa-plus"></i> Добавить опрос</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-pie-chart"></i> <span>Розыгрыши</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/quiz/admin"><i class="fa fa-cog"></i> Список розыгрышей</a></li>
                        <li><a href="/quiz/create"><i class="fa fa-plus"></i> Добавить розыгрыш</a></li>
                        <li><a href="/winners/admin"><i class="fa fa-cog"></i> Список победителей</a></li>
                        <li><a href="/winners/create"><i class="fa fa-plus"></i> Добавить победителя</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-pie-chart"></i> <span>Инфохаб</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/infohub/admin"><i class="fa fa-cog"></i> Слайдеры</a></li>
                        <li><a href="/infohub/create"><i class="fa fa-plus"></i> Добавить новый</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-pie-chart"></i> <span>Спец. проект</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/specialProject/admin"><i class="fa fa-cog"></i> Просмотр</a></li>
                        <li><a href="/specialProject/create"><i class="fa fa-plus"></i> Добавить новый</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-comments-o"></i> <span>Новости</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/newsManage/admin"><i class="fa fa-cog"></i> Список новостей</a></li>
                        <li><a href="/newsManage/create"><i class="fa fa-plus"></i> Добавить новость</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-hospital-o"></i> <span>Заболевания</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/diseasesManage/admin"><i class="fa fa-cog"></i> Список заболеваний</a></li>
                        <li><a href="/diseasesManage/create"><i class="fa fa-plus"></i> Добавить новое</a></li>
                        <li>
                            <a href="#">
                                <i class="fa fa-hospital-o"></i> <span>Направления</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/directionsDiseases/admin"><i class="fa fa-cog"></i> Список направлений</a></li>
                                <li><a href="/directionsDiseases/create"><i class="fa fa-plus"></i> Добавить новое</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>

        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <section class="content-header">
            <h1><?= $this->nameController ?>
                <small>Администрирование</small>
            </h1>
            <? $this->widget('zii.widgets.CBreadcrumbs', array(
                'links' => $this->breadCrumbs,
                'tagName' => 'ol',
                'homeLink' => '<li>' . CHtml::link('На сайт', Yii::app()->request->getBaseUrl(true)) . '</li>',
                'inactiveLinkTemplate' => '<li>{label}</li>',
                'activeLinkTemplate' => '<li class="active"><a href="{url}">{label}</a></li>',
                'separator' => '',
                'htmlOptions' => array(
                    'class' => 'breadcrumb'
                )
            )); ?>
        </section>
        <!-- Main content -->
        <section class="content">
            <? echo $content; ?>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    //	$.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="<?= Yii::app()->request->getBaseUrl(true); ?>/assets/admin/bootstrap/js/bootstrap.min.js"></script>
<!-- daterangepicker -->
<script src="<?= Yii::app()->request->getBaseUrl(true); ?>/assets/admin/plugins/daterangepicker/moment.js"></script>
<script
    src="<?= Yii::app()->request->getBaseUrl(true); ?>/assets/admin/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script
    src="<?= Yii::app()->request->getBaseUrl(true); ?>/assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= Yii::app()->request->getBaseUrl(true); ?>/assets/admin/dist/js/app.min.js"></script>
<script src="<?= Yii::app()->request->getBaseUrl(true); ?>/assets/admin/dist/js/pages/dashboard.js"></script>
<script src="<?= Yii::app()->request->getBaseUrl(true); ?>/assets/admin/dist/js/demo.js"></script>
<script src="<?= Yii::app()->request->getBaseUrl(true); ?>/assets/admin/bootstrap/js/bootstrap-multiselect.js"></script>
<script src="<?= Yii::app()->request->getBaseUrl(true); ?>/assets/admin/bootstrap/js/jquery.maskedinput.js"></script>
<script src="<?= Yii::app()->request->getBaseUrl(true); ?>/assets/admin/js/scripts.js"></script>
</body>

<script type="text/javascript">
    $(function () {
        $('#lstFruits').multiselect({
            includeSelectAllOption: true
        });
        $('#btnSelected').click(function () {
            $("div.wekDay").remove();
            var selected = $("#lstFruits option:selected");
            var message = "";
            selected.each(function () {
                message += $(this).val() + "_";
            });
            var URL = 'http://' + window.location.hostname;
            $.ajax({
                url: URL + '/clinic/DoctorUpdate',
                data: 'id=' + message,
                success: function (data) {
                    result = JSON.parse(data);
                    $("#test").empty();
                    var n=0;
                    $.each(result, function (key, value) {
                        var count = value.length;
                        var countmas = value[0].length;
                        var theDiv = document.createElement('div');  // создать новый тег div
                        theDiv.innerHTML =

                            '<div class="col-xs-3 text-center name_stolb">Имя</div>' +
                            '<div class="col-xs-3 text-center name_stolb">Специализация</div>' +
                            '<div class="col-xs-3 text-center name_stolb">Цена</div>' +
                            '<div class="col-xs-3 text-center name_stolb">Время</div>' +
                            '<div class="col-xs-3 text-center doctor"><input  name="name[]" class="form-control" value="' + value[7]['doctor'].name + '"></div>' +
                            '<div class="col-xs-3 text-center doctor"><input name="specializationDoctor[]" type="text" class="form-control" value="' + value[7]['doctor'].specialization + '"></div>' +
                            '<div class="col-xs-3 text-center doctor"><input  name="price[]" type="text" class="form-control" value="' + value[7]['doctor'].price + '"></div>' +
                            '<div class="col-xs-3 text-center doctor"><input name="timeSize[]" type="text" class="form-control" value="' + value[7]['doctor'].timeSize + '"></div>' +
                            '<input type="hidden"  name="id[]" class="form-control" value="' + value[7]['doctor'].id + '">' +
                            '<div class="col-xs-3 text-center wekDay text-center">Понедельник</div>' +
                            '<div class="col-xs-3 text-center wekDay text-center">Вторник</div>' +
                            '<div class="col-xs-3 text-center wekDay text-center">Среда</div>' +
                            '<div class="col-xs-3 text-center wekDay text-center">Четверг</div>' +
                            '<div class="col-xs-3 text-center wekDay text-center">Пятница</div>' +
                            '<div class="col-xs-3 text-center wekDay text-center">Суббота</div>' +

                            '<div class="col-xs-3 text-center wekDay text-center">Воскресенье</div>';
                        document.getElementById('test').appendChild(theDiv);
                        for (i = 0; i < countmas; i++) {

                            $("#test").append('<div class="row" data-i="'+i+'"  data-doctor="' + value[7]['doctor'].id + '"></div>');

                            for (j = 0; j < count; j++) {
                                var div = document.querySelector('#btnSelected');
                                if (!value[j]['doctor']) {
                                    $("[data-doctor="+ value[7]['doctor'].id + "][data-i="+i+"]").append('<div class="col-xs-1 wekDay"><input data-detail-day="' + value[7]['doctor'].id + '" type="text" name="day'+value[7]['doctor'].id+'[]" class="form-control" value="' + value[j][i] + '"></div>');
                                }
                            }
                            $("[data-doctor="+ value[7]['doctor'].id + "][data-i="+i+"]").append('<div title="Удалить"  data-toggle="tooltip" class="col-md-1 text-center"><i class="fa fa-trash red" data-detail-id="'+value[7]['doctor'].id+'_'+i+ '"></i></div></div>');

                        }
                        $("#test").append('<div title="Добавить расписание" data-id-plus="'+n+'" data-toggle="tooltip" class="col-md-12 text-center"><i class="fa fa-plus blue"></i></div>');
                        n++;
                    });
                    $("#test").append(' <div class="col-md-12 text-center"> <button type="submit"  class="btn btn-primary text-center">Сохранить</button></div>');

                }
            });
        });

    });
</script>
<script type="text/javascript">
    window.onclick = function (e) {
        e = e || event;
        var target = e.target || e.srcElement;
        if (target.getAttribute('data-detail-id')) {
            var id = $(target).attr('data-detail-id');
            var URL = 'http://' + window.location.hostname;
            $.ajax({
                url: URL + '/clinic/DeleteDoctor',
                data: $('form').serialize()+'&idDelete=' + id,
                success: function (data) {
                    $(target).parent().parent().remove();
                    console.log(target);
                }
            })
        }
        if ($(target).hasClass('blue')) {
            var index = $(target.parentNode).attr('data-id-plus');
            console.log(target);
            var parent = $(target).parent().parent();
            var parentClone = $(parent.find('.row')[index]).clone();
            console.log(parentClone);
            $(parent.find('.form-control')[index]).val('');
            $(target).before(parentClone);
        }
        if ($(target).hasClass('red')) {
            var index = $(target.parentNode).attr('data-id-plus-doctorAdd');
            console.log(target);
            var parent = $(target).parent().parent();
            var parentClone = $(parent.find('.str')[index]).clone();
            console.log(parentClone);
            $(parent.find('.form-control')[index]).val('');
            $(target).before(parentClone);
        }

    };
    $('#listSpec').multiselect({
        includeSelectAllOption: true
    });
    $('[name=deleteItem]').click(function(){
        var id= $(this).val();
        var URL = 'http://'+window.location.hostname;
        $.ajax({
            url: URL+'/review/DeleteItem',
            data: 'id='+id,
            success: function (data) {
                $('[id=deleteItem][value='+id+']').parent().remove();
                console.log(this);
            }
        })
    })
</script>


</html>
