<?php
if(!Yii::app()->user->isGuest) {
    if(User::model()->findByPk(Yii::app()->user->id) == false){
        $this->redirect("/site/logout");
    }
}
?>
<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Сайт по поиску врачей</title>

    <link href='<?= Yii::app()->request->baseUrl ?>/assets/styles/jquery-ui.css' media="screen" rel="stylesheet"
          type="text/css"/>
    <link href='<?= Yii::app()->request->baseUrl ?>/assets/bootstrap/css/bootstrap.css' media="screen" rel="stylesheet"
          type="text/css"/>
    <link href='<?= Yii::app()->request->baseUrl ?>/assets/styles/main.css' media="screen" rel="stylesheet"
          type="text/css"/>
    <link rel="shortcut icon" href=<?= '"'.Yii::app()->request->baseUrl . "/assets/images/favicon.ico\""; ?> type="image/x-icon" >
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <? Yii::app()->getClientScript()->registerCoreScript('jquery'); ?>
</head>
<body>

<div class="container">

    <div id="baseBlock" class="row indent" style="margin-right:-30px;">
        <?= $content; ?>
    </div>

</div>
<script type="text/javascript" src='<?= Yii::app()->request->baseUrl ?>/assets/scripts/jquery-ui.js'></script>
<script type="text/javascript" src='<?= Yii::app()->request->baseUrl ?>/assets/bootstrap/js/bootstrap.js'></script>
<script type="text/javascript" src='<?= Yii::app()->request->baseUrl ?>/assets/scripts/main2.js'></script>
<script type="text/javascript" src='<?= Yii::app()->request->baseUrl ?>/assets/scripts/globalSearch.js'></script>
</body>
</html>