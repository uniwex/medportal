<!DOCTYPE html>

<html>
<head>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
	<title>Indi Gold</title>
	<!---->
	<link rel="stylesheet" type="text/css" href=<?php echo Yii::app()->request->baseUrl.'/assets/datepicker/css/datepicker.css'?>>
	<!---->
	<link rel="stylesheet" type="text/css" media="screen" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href=<?php echo Yii::app()->request->baseUrl.'/assets/styles/datetimepicker.css'?>>
	<link rel="stylesheet" type="text/css" href=<?php echo Yii::app()->request->baseUrl.'/assets/styles/index.css'?>>
	<script type="text/javascript" src="//code.jquery.com/jquery-2.1.1.min.js"></script>
	<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
	<script src=<?php echo Yii::app()->request->baseUrl.'/assets/scripts/datetimepicker.js'?>></script>
	<script type="text/javascript">
			var birdNumber = 0;
			var toggleModal1 = function(){
				$('#modal-window');

				if(birdNumber == 1){
					$('#modal-window .modal-head').html('МОЛОДАЯ ИНДЕЙКА');
					$('#modal-window form').css('display', 'none');
					$('#descrForBird').html('Стоимость молодой тушки 4000 руб. Живой вес тушки 2-3кг.<br><br><br>Партия подрастает.Заказать молодую индейку можно будет с 01.09.15 (на сайте появится объявление)<br>Спасибо');
					$('#size').val('Молодая');
					changeQuantity(birdNumber);
				}
				else{
					$('#modal-window form').css('display', 'block');
				}
				if(birdNumber == 2){
					$('#modal-window .modal-head').html('СРЕДНЯЯ ИНДЕЙКА');
					$('#descrForBird').html('Стоимость 5000 руб. Живой вес тушки 5-6кг.');
					$('#size').val('Средняя');
					changeQuantity(birdNumber);
				}
				if(birdNumber == 3){
					$('#modal-window .modal-head').html('КРУПНАЯ ИНДЕЙКА');
					$('#descrForBird').html('Стоимость 6000 руб. Живой вес тушки 7-8кг.');
					$('#size').val('Крупная');
					changeQuantity(birdNumber);
				}

				var overlay = document.getElementById('layout');
				var modalWindow = document.getElementById('modal-window');
				if((modalWindow.style.display == 'none')||(modalWindow.style.display == '')){
					overlay.style.display = 'block';
					modalWindow.style.display = 'block';
				}
				else{
					overlay.style.display = 'none';
					modalWindow.style.display = 'none';
				}
				
			}

			var changeQuantity = function(){
				var price = 0;
				var total = 0;
				if(birdNumber == 1)
					price = 4000;
				if(birdNumber == 2)
					price = 5000;
				if(birdNumber == 3)
					price = 6000;

				var quantity = document.getElementById('quantity').value.replace(/[^0-9.]/g,'');
				if(quantity == '')
					quantity = 0;
				total = quantity * price;
				$('#totalPrice').html(total);
				$('#hiddenTotalPrice').val(total);
			}

			var toggleModalAgreement = function(){
				var overlay = document.getElementById('layout');
				var modalWindow1 = document.getElementById('modal-window');
				var modalWindowAgreement = document.getElementById('modal-agreement');
				if((modalWindowAgreement.style.display == 'none')||(modalWindowAgreement.style.display == '')){
					modalWindow1.style.display = 'none';
					modalWindowAgreement.style.display = 'block';
				}
				else{
					modalWindow1.style.display = 'block';
					modalWindowAgreement.style.display = 'none';
				}
				
			}

			var iAgree = function(){
				$('#agree').prop('checked',true);
			}

			var toggleDelivery = function(parm){
				if(parm == 0){
					$('#userStreet').css('display','none');
					$('#userFlat').css('display','none');
					$('#userFloor').css('display','none');
					$('#size').css('display','none');
					$('#quantity').css('margin','0px 0px 0px 14%');
					$('#address-text').html('Количество');
				}
				else{
					$('#userStreet').css('display','inline');
					$('#userFlat').css('display','inline');
					$('#userFloor').css('display','inline');
					$('#size').css('display','inline');
					$('#quantity').css('margin','0px 0px 0px 2%');
					$('#address-text').html('Адрес доставки');
				}
			}

			var checkFields = function(){
				if(!$('#agree').prop('checked')){
					alert('Перед покупкой Вы должны прочитать соглашение');
					return;
				}
				var userName = $('#userName').val();
				if(userName == ''){
					alert('Введите имя');
					return;
				}
				var reg = /^((8|\+7)[\- ]?)?(\(?\d{3,4}\)?[\- ]?)?[\d\- ]{5,10}$/;
				var userPhone = $('#userPhone').val();
				if(userPhone == ''){
					alert('Введите телефон');
					return;
				}
				if(!reg.test(userPhone)){
					alert('Неверный формат телефона');
					return;
				}
				var quantity = $('#quantity').val();
				if(quantity == ''){
					alert('Введите количество тушек');
					return;
				}
				reg = /^\d+$/;
				if(!reg.test(quantity)){
					alert('В поле "Количество" должны быть только цифры');
					return;
				}
				if(quantity == 0){
					alert('Количество должно быть больше нуля');
					return;
				}
				if($('#radioDelivery').prop('checked')){
					var userStreet = $('#userStreet').val();
					if(userStreet == ''){
						alert('Введите название улицы');
						return;
					}
					var userFloor = $('#userFloor').val();
					if(userFloor == ''){
						alert('Введите этаж');
						return;
					}
					reg = /^\d+$/;
					if(!reg.test(userFloor)){
						alert('В поле "Этаж" должны быть только цифры');
						return;
					}
					var userFlat = $('#userFlat').val();
					if(userFlat == ''){
						alert('Введите номер квартиры');
						return;
					}
					reg = /^\d+$/;
					if(!reg.test(userFlat)){
						alert('В поле "Номер квартиры" должны быть только цифры');
						return;
					}
				}
				return true;
			}
		
	</script>
</head>
<body>
	<?php echo $content; ?>
	<div id="layout"></div>
	<div id="modal-window" class="modal-window">
		<div class="modal-head">ОСУЩЕСТВИТЬ ПОКУПКУ</div>
		<img class="modal-close" src=<?php echo Yii::app()->request->baseUrl.'/assets/images/button-close.png'?> onclick="toggleModal1()">
			<div class="modal-text" id="descrForBird" style="width:80%;text-align:center;">
				текст
			</div>
		<!--/pay/payment-->
		<form method="POST" action=<?php echo Yii::app()->request->baseUrl.'/pay/payment'?> onsubmit="if(!checkFields())return false;">
			<input id="userName" class="modal-input1" name="name" placeholder="Имя">
			<input id="userPhone" class="modal-input1" name="phoneNumber" placeholder="Номер телефона">
			<div class="modal-text" id="address-text">Адрес доставки</div>

				<input id="userStreet" class="modal-input2" name="userStreet" placeholder="Улица">
				<input id="quantity" name="quantity" onKeyUp="changeQuantity()" class="modal-input2" placeholder="Кол - во штук" style="margin: 0px 0px 0px 11px;" value="1">
				<input id="userFlat" class="modal-input3" name="userFlat" placeholder="Квартира" style="margin-left:100px;margin-top:10px;margin-right:4px;">
				<input id="userFloor" class="modal-input3" name="userFloor" placeholder="Этаж">
				<input id="size" name="size" class="modal-input2" placeholder="Размер тушки" style="margin: 0px 0px 0px 2%;">
				<br>

			<div id="for-radio">
				<input id="radioSelf" type="radio" name="typeDelivery" value="pickup" onclick="toggleDelivery(0)">
				<label for="radioSelf" onclick="toggleDelivery(0)">Самовывоз</label>
				<input id="radioDelivery" type="radio" name="typeDelivery" value="delivery" onclick="toggleDelivery(1)" style="margin-left:20px;" checked>
				<label for="radioDelivery" onclick="toggleDelivery(1)">Доставка</label>

				<input id="radioNotCash" type="radio" name="typeCash" value="notCash" style="margin-left:50px;" checked>
				<label for="radioNotCash">Б/н оплата</label>
				<input id="radioCash" type="radio" name="typeCash" value="cash" style="margin-left:5px;">
				<label for="radioCash">Оплата наличными</label>
			</div>
			<div class="modal-text" style="text-align:center;margin-top:10px;">Итого:</div>
			<div id="point"></div>
			<div class="modal-text" style="text-align:center;margin-top:10px;font-size: 22px;"><span id="totalPrice">0</span> руб.</div>
			<div class="agreement">
				<input type="checkbox" id="agree">Я согласен с <span onclick="toggleModalAgreement();">правилами</span>
			</div>
			<input type="hidden" id="hiddenTotalPrice" name="sum">
			<input id="submit" class="modal-ok" type="submit" value="ОТПРАВИТЬ ЗАКАЗ">
		</form>
	</div>
	<div id="modal-agreement" class="modal-window">
		<div class="modal-head">ПРАВИЛА ПОКУПКИ/ПРОДАЖИ</div>
		<img class="modal-close" src=<?php echo Yii::app()->request->baseUrl.'/assets/images/button-close.png'?> onclick="toggleModalAgreement()">
		<div class="modal-agreement-text">
			Покупатель согласен приобретать продукцию домашнего хозяйства как у частного лица.<br>
			Вся продукция имеет ветеринарную проверку с соответствующими документами
			Доставка осуществляется в указанные сроки, в случае невозможности доставки по вине покупателя, он самостоятельно заберет птицу в замороженном виде с фермы, самовывозом и обязан оплатить доставку на месте.<br>
			В случае изменения сроков доставки или самовывоза сообщить об этом не позднее чем за 24 часа, в ином случае птица будет заморожена.<br>
			Мы выставляем размер тушек живым весом т.е. взвешиваем птицу перед забоем, по факту вес будет отличаться в меньшую сторону после обработки.<br>
			<br>
			Уважаемые покупатели! Доставка заказов осуществляется с 12.00 до 20.00.<br>
			Доставка осуществляется в течение трёх дней с момента оформления заказа.<br>
			Если Вы хотите сделать заказ заранее на какой то конкретный день, то об этом необходимо сообщить оператору при подтверждении заказа по телефону.<br>
			Сумма минимального заказа составляет 4000 рублей.<br>
			При заказе стоимостью более 8000 руб. доставка по Санкт-Петербургу осуществляется бесплатно.<br>
			Стоимость доставки по Санкт-Петербургу в пределах КАД – 250 руб.<br>
			Стоимость доставки по Санкт-Петербургу и Ленинградской области за пределами КАД - 250 руб. +20 руб. за 1 км.<br>
			Оплата доставки производится наличным платежом курьеру.<br>
			Забой птицы, ветеринарный осмотр и упаковка товара осуществляется непосредственно перед доставкой.<br>
			100% предоплата безналичным переводом с сайта при заказе или оплата наличными (так же 100% предоплата), место передачи денег согласовывается по телефону с оператором<br>
			Мы стремимся сделать доставку максимально быстрой и удобной для всех наших клиентов.<br>
		</div>
		<!--<input onclick="iAgree()" class="modal-ok" type="button" value="Я ПРОЧИТАЛ СОГЛАШЕНИЕ">-->
	</div>
	<div id="main">
		<div id="logo"></div>
		<div id="hello">
			<img id="substrate2" src=<?php echo Yii::app()->request->baseUrl.'/assets/images/substrate2.png'?>>
			<div class="text-head" style="margin-bottom:10px;">
				ЗДРАВСТВУЙТЕ
			</div>
			<div class="text-content">
				ПРЕДЛАГАЕМ ВАШЕМУ ВНИМАНИЮ ВЫСОКОКАЧЕСТВЕННУЮ ДОМАШНЮЮ ИНДЕЙКУ!<br>
				ВЫРАЩЕННУЮ С БОЛЬШОЙ ЗАБОТОЙ И ЛЮБОВЬЮ!<br>
				В ЭКОЛОГИЧЕСКИ ЧИСТОМ РАЙОНЕ ЛЕНИНГРАДСКОЙ ОБЛАСТИ!
			</div>
		</div>
		<div id="substrate1">
			<!--<div id="ask">ЗАДАТЬ ВОПРОС</div>-->
			<img class="shadow2" style="margin-left: -32px;" src=<?php echo Yii::app()->request->baseUrl.'/assets/images/shadow-left2.png'?>>
			<img class="shadow2" style="margin-left: 752px;" src=<?php echo Yii::app()->request->baseUrl.'/assets/images/shadow-right2.png'?>>
			<div class="text-head text-head2" style="margin-bottom:10px;">
				О ПРОДУКТЕ:
			</div>
			<div class="text-content" style="margin-bottom:10px;color:rgb(99,82,82);">
				Все особи выращены на домашнем хозяйственном участке в деревне!<br>
				Птица находится на естественном выгуле и получает<br>
				полноценное питание без УСКОРИТЕЛЕЙ РОСТА, АНТИБИОТИКОВ<br>
				и прочих вредных добавок. Поэтому мясо нашей индейки<br>
				не только вкусное и полезное, но также экологически чистое,<br>
				натуральное и без ГМО!
			</div>
		</div>
		<div id="substrate3">
			<div class="text-head text-head2" style="margin-top:30px;color:#FFF;">
				ВЫ МОЖЕТЕ ПРИОБРЕСТИ НАШУ<br>
				ПРОДУКЦИЮ ЗДЕСЬ И СЕЙЧАС:
			</div>
		</div>
		<div id="birds">
			<div class="bird">
				<div class="bird-name">МОЛОДАЯ<br>ИНДЕЙКА</div>
				<img id="bird1-pic" src=<?php echo Yii::app()->request->baseUrl.'/assets/images/bird1.png'?>></img>
				<div class="button-buy" onclick="birdNumber=1;toggleModal1(1)"><i>КУПИТЬ</i></div>
			</div>
			<div class="bird">
				<div class="bird-line" style="margin-left: -17px;"></div>
				<div class="bird-line" style="margin-left: 168px;"></div>
				<div class="bird-name" style="color:rgb(191,13,13);">СРЕДНЯЯ<br>ИНДЕЙКА</div>
				<img id="bird2-pic" src=<?php echo Yii::app()->request->baseUrl.'/assets/images/bird-gold.png'?>></img>
				<div class="button-buy" onclick="birdNumber=2;toggleModal1()"><i>КУПИТЬ</i></div>
			</div>
			<div class="bird">
				<div class="bird-name" style="color:rgb(191,13,13);">КРУПНАЯ<br>ИНДЕЙКА</div>
				<img id="bird3-pic" src=<?php echo Yii::app()->request->baseUrl.'/assets/images/bird-gold.png'?>></img>
				<div class="button-buy" onclick="birdNumber=3;toggleModal1()"><i>КУПИТЬ</i></div>
			</div>
		</div>
		<img id="venzel-bottom" src=<?php echo Yii::app()->request->baseUrl.'/assets/images/venzel-bottom.png'?>>
		<div id="main-block"></div>
		<div id="block-for-shadow">
			<div id="shadow-left"></div>
			<div id="shadow-right"></div>
			<div id="tracery-left"></div>
			<div id="tracery-right"></div>
		</div>
		<img id="tracery" src=<?php echo Yii::app()->request->baseUrl.'/assets/images/tracery1.png'?>>
	</div>
	<!-- BEGIN JIVOSITE CODE {literal} -->
	<script type='text/javascript'>
	(function(){ var widget_id = 'hLa9LiiTEK';
	var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
	<!-- {/literal} END JIVOSITE CODE -->
</body>
<script type="text/javascript">
$(function () {
    $('#radioDelivery').prop('checked');
});
</script>
</html>