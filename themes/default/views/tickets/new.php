<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 27.10.2015
 * Time: 15:03
 */
?>
<div id="postAdvert" class="col-md-10" data-type="ticket">
    <div class="row">
        <div class="col-md-12 userInformationHead1 no-margin">
            Новый тикет
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <? $form = $this->beginWidget('CActiveForm', array(
                'id' => 'new-form',
                'action' => '/tickets/new',

                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                    'validateOnChange' => true
                )
            )); ?>
            <!--
            <div class="row form-group">
                <div class="col-md-2">
                    <?// $form->labelEx($model, 'type') ?>
                </div>
                <div class="col-md-8">
                    <?// $form->dropDownList($model, 'type', array('Вопрос' => 'Вопрос', 'Жалоба' => 'Жалоба'), array('class' => 'fieldInput')) ?>
                </div>
            </div>
            -->

            <div class="row form-group">
                <div class="col-md-2">
                    <?= $form->labelEx($model, 'subject') ?>
                </div>
                <div class="col-md-8">
                    <?= $form->textField($model, 'subject', array('class' => 'fieldInput')) ?>
                    <?= $form->error($model, 'subject', array('class' => 'text-red')) ?>
                </div>
            </div>

            <div class="row form-group">
                <div class="col-md-2">
                    <?= $form->labelEx($model, 'text') ?>
                </div>
                <div class="col-md-8">
                    <?= $form->textArea($model, 'text', array('class' => 'fieldInput', 'style' => 'width: 100%; height: 100px')) ?>
                    <?= $form->error($model, 'text', array('class' => 'text-red')) ?>
                </div>
            </div>

            <div class="row form-group">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <?= CHtml::submitButton('Создать', array('class' => 'button1 button1reg')) ?>
                </div>
            </div>

            <? $this->endWidget(); ?>
        </div>
    </div>
</div>
