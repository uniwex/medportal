<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 27.10.2015
 * Time: 14:00
 */
?>

<div class="row search-block">
    <div class="col-md-8">
        <div class="userInformationHead1 no-margin">
            <?php
            $display = "";
            (isset($data->user)) ? $display = $data->user->name : $display = '<пользователь удалён>';
            echo $display;
            ?>
        </div>
    </div>
    <div class="col-md-4 text-right">
        <p>Дата: <b><?= $data->messageDate ?></b></p>
    </div>
    <div class="col-md-12">
        <p class="ticketText"><?= $data->message ?></p>
    </div>
</div>
