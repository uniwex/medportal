<?
Yii::app()->clientScript->registerScript('search',
    "
$('#Ticket_date').on('keyup', function(){
    $.fn.yiiListView.update('list', {
            url: '" . CController::createUrl('tickets/') . "',
            data: 'date='+$(\"[id=Ticket_date]\").val()
        }
    )
});
$('#Ticket_date').on('change', function(){
    $.fn.yiiListView.update('list', {
            url: '" . CController::createUrl('tickets/') . "',
            data: 'date='+$(\"[id=Ticket_date]\").val()
        }
    )
});
");
?>

<div id="postAdvert" class="col-md-10" data-type="ticket">
    <div class="row">
        <!--
        <div class="col-md-3 col-xs-6">
            <div class="form-group">
                <?// CHtml::activeLabel(Ticket::model(), 'type') ?>
                <?// CHtml::activeDropDownList(Ticket::model(), 'type', array('' => 'Любой', 'Вопрос' => 'Вопрос', 'Жалоба' => 'Жалоба'), array('class' => 'fieldInput')); ?>
            </div>
        </div>
        -->
        <div class="col-md-3 col-xs-6">
            <div class="form-group hidden-inner-label">
                <?= CHtml::activeLabel(Ticket::model(), 'date') ?>
                <?
                $year = date('y');
                $month = date('m');
                $day = date('d');
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => Ticket::model(),
                    'attribute' => 'date',
                    'language' => 'ru',
                    'options' => array(
                        'dateFormat' => 'yy-mm-dd',
                        'showAnim' => 'fold',
                    ),
                    'htmlOptions' => array(
                        'class' => 'fieldInput',
                        'placeholder' => 'Дата создания'
                    ),
//                    'value' => $year . '-' . $month . '-' . $day
                )); ?>
            </div>
        </div>
        <!--
        <div class="col-md-3 col-xs-6 text-center top-padding">
            <input class="button1 buttonFilter bg-blue" type="button" id="filter" value="Фильтровать">
        </div>
        -->
        <div class="col-md-9 col-xs-6">
            <input class="button1 buttonFilter bg-liteBlue" style="width:200px;text-decoration: none;color:#FFF;float:right;height:37px;padding:5px 16px;background-color:#456BA5;text-align:left;font-size:18px;" type="button" value="Задать вопрос" onclick="location.href='/tickets/new'">
        </div>
    </div>
    <div class="row">
        <? $this->renderPartial('list', array('dataProvider' => $dataProvider), false, true); ?>
    </div>
</div>