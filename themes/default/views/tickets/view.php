<div id="postAdvert" class="col-md-10" data-type="ticket">
    <div class="row">
        <div class="col-md-12 userInformationHead1 no-margin">
            <?= $ticket->subject ?> - <?= Yii::app()->params['status'][$ticket->status] ?>
        </div>
    </div>

    <? $this->widget('zii.widgets.CListView', array(
        'id' => 'list',
        'dataProvider' => $dataProvider,
        'enablePagination' => true,
        'ajaxUpdate' => true,
        'beforeAjaxUpdate' => 'function(){ $("#preload").css("display", "block"); }',
        'afterAjaxUpdate' => 'function(){ $("#preload").css("display", "none"); }',
        'template' => '{items}{pager}',
        'loadingCssClass' => '',
//    'pager' => array(
//        'class' => 'CLinkPager',
//        'header' => '',
//        'maxButtonCount' => 3,
//        'cssFile' => Yii::app()->request->getBaseUrl(true) . '/css/listview_pager.css',
//    ),
        'itemView' => '_view',
        'summaryText' => '',
        'emptyText' => '',
        'htmlOptions' => array(
            'class' => 'col-md-12'
        )
    )); ?>

    <? if($ticket->status != 2) { ?>
    <div class="row">
        <?php $formMessage = $this->beginWidget("CActiveForm", array(
            'id' => 'add-message',
            'action' => Yii::app()->request->baseUrl . '/tickets/view/'.$ticket->id,
            'method' => 'post',
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => true
            ),
//            'htmlOptions' => array(
//                'enctype' => 'multipart/form-data',
//                'autocomplete' => 'off',
//                'onsubmit' => 'return true',
//            ),
        )); ?>
        <div class="col-md-6">
            <div class="form-group">
                <?= $formMessage->labelEx($model, 'message') ?>
                <?= $formMessage->textArea($model, 'message', array('class' => 'fieldInput', 'style' => 'width: 100%')); ?>
                <?= $formMessage->error($model, 'message', array('class' => 'text-red')) ?>
                <?= CHtml::submitButton('Отправить', array('class' => 'button1 button1reg')); ?>
            </div>
        </div>
        <? if(User::isAdmin()) { ?>
        <div class="col-md-6">
            <div class="form-group">
                <?= CHtml::checkBox('close') ?>
                <?= CHtml::label('Закрыть тикет', 'close') ?>
            </div>
        </div>
        <? } ?>
        <?php $this->endWidget(); ?>
    </div>
    <? } ?>
</div>