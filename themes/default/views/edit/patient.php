    <? Yii::app()->clientScript->registerScriptFile(Yii::app()->homeUrl . 'assets/scripts/edit.js', CClientScript::POS_HEAD); ?>
    <div class="col-md-10">
        <?php $formPatient = $this->beginWidget("CActiveForm", array(
            'id' => 'patient-edit',
            'action' => Yii::app()->request->baseUrl . '/edit/patient',
            'method' => 'post',
            'enableAjaxValidation' => true,
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => true,
            ),
            'htmlOptions' => array(
                'autocomplete' => 'off'
            ),
        )); ?>
        <div class="form-group">
            <?= $formPatient->errorSummary($userModel, 'Пожалуйста, исправьте следующие ошибки:', '', array('class' => 'text-red')) ?>
            <?= $formPatient->errorSummary($patientModel, '', '', array('class' => 'text-red')) ?>
            <?= $formPatient->errorSummary($stationModel, '', '', array('class' => 'text-red')) ?>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formPatient->labelEx($patientModel, 'name') ?>
            </div>
            <div class="col-md-8">
                <?= $formPatient->textField($patientModel, 'name', array('class' => 'fieldInput', 'placeholder' => 'Иванов И.И.')); ?>
                <?= $formPatient->error($patientModel, 'name', array('class' => 'text-red')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formPatient->labelEx($userModel, 'email') ?>
            </div>
            <div class="col-md-8">
                <?= $formPatient->textField($userModel, 'email', array('class' => 'fieldInput', 'placeholder' => 'sample@mail.ru')); ?>
                <?= $formPatient->checkBox($userModel, 'emailEnable', array('id' => 'User_emailEnable_4')); ?>
                <?= $formPatient->labelEx($userModel, 'emailEnable', array('for' => 'User_emailEnable_4')) ?>
                <?= $formPatient->error($userModel, 'email', array('class' => 'text-red')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formPatient->labelEx($userModel, 'password') ?>
            </div>
            <div class="col-md-8">
                <?= $formPatient->passwordField($userModel, 'password', array('class' => 'fieldInput', 'value' => '')); ?>
                <?= $formPatient->error($userModel, 'password', array('class' => 'text-red')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formPatient->labelEx($userModel, 'phone') ?>
            </div>
            <div class="col-md-8">
                <?= $formPatient->textField($userModel, 'phone', array('class' => 'fieldInput', 'placeholder' => '+79000000000')) ?>
                <?= CHtml::button('Изменить', array('class' => 'btn-change', 'name' => 'changePhone')) ?>
                <?= $formPatient->error($userModel, 'phone', array('class' => 'text-red')) ?>
                <div class="indent-top hidden">
                    <?= CHtml::textField('code', '', array('class' => 'fieldInput', 'placeholder' => 'Введите пароль из SMS')) ?>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formPatient->labelEx($patientModel, 'birthday') ?>
            </div>
            <div class="col-md-8">
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'name' => 'birthday',
                    'model' => $patientModel,
                    'attribute' => 'birthday',
                    'language' => 'ru',
                    'options' => array(
                        'dateFormat' => 'yy-mm-dd',
                        'class' => 'fieldInput',
                        'showAnim' => 'fold',
                    ),
                    'htmlOptions' => array(
                        'class' => 'fieldInput',
                        'id' => 'Patient_birthday',
                    ),
                )); ?>
                <?= $formPatient->error($patientModel, 'birthday', array('class' => 'text-red')); ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formPatient->labelEx($userModel, 'country') ?>
            </div>
            <div class="col-md-8">
                <?= $formPatient->dropDownList($userModel, 'country', array('0' => 'Выберите страну', 'Россия' => 'Россия'), array('class' => 'fieldInput', 'data-alias' => 'country')); ?>
                <?= $formPatient->error($userModel, 'country', array('class' => 'text-red')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formPatient->labelEx($userModel, 'city') ?>
            </div>
            <div class="col-md-4 col-xs-6">
                <?= $formPatient->textField($userModel, 'city', array('class' => 'fieldInput inputForSearchCity')); ?>
                <?= $formPatient->error($userModel, 'city', array('class' => 'text-red')) ?>
                <select class="dropDownCities" type="select" size="5" data-alias="city">
                    <option>Выберите город</option>
                </select>
            </div>
            <div class="col-md-4 col-xs-6">
                <?= $formPatient->dropDownList($stationModel, 'name', array('0' => 'Выберите станцию метро', 'Нет метро' => 'Нет метро'), array('class' => 'fieldInput', 'data-alias' => 'metro', 'multiple' => true, 'style' => 'margin-left:10px;padding-top:3px;padding-bottom:4px;')); ?>
                <?= $formPatient->error($stationModel, 'name', array('class' => 'text-red')) ?>
                <div class="selectButton"></div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formPatient->labelEx($patientModel, 'oms') ?>
            </div>
            <div class="col-md-8">
                <?= $formPatient->textField($patientModel, 'oms', array('class' => 'fieldInput')); ?>
                <?= $formPatient->error($patientModel, 'oms', array('class' => 'text-red')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formPatient->labelEx($patientModel, 'dms') ?>
            </div>
            <div class="col-md-8">
                <?= $formPatient->textField($patientModel, 'dms', array('class' => 'fieldInput')); ?>
                <?= $formPatient->error($patientModel, 'dms', array('class' => 'text-red')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <?= CHtml::submitButton('Обновить', array('class' => 'button1 button1reg')); ?>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>