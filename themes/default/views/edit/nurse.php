<? Yii::app()->clientScript->registerScriptFile(Yii::app()->homeUrl . 'assets/scripts/edit.js', CClientScript::POS_HEAD); ?>
<div class="col-md-10">
    <?php $formNurse = $this->beginWidget("CActiveForm", array(
        'id' => 'nurse-edit',
        'action' => Yii::app()->request->baseUrl . '/edit/nurse',
        'method' => 'post',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
        ),
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
            'autocomplete' => 'off',
            'onsubmit' => 'crutchForNurseCKEditor();return true'
        ),
    )); ?>
    <span class="userEditDoctor">
        <?= $formNurse->hiddenField($nurseModel, 'pic') ?>
        <div class="form-group">
            <?= $formNurse->errorSummary($userModel, 'Пожалуйста, исправьте следующие ошибки:', '', array('class' => 'text-red')) ?>
            <?= $formNurse->errorSummary($nurseModel, '', '', array('class' => 'text-red')) ?>
            <?= $formNurse->errorSummary($stationModel, '', '', array('class' => 'text-red')) ?>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($nurseModel, 'name') ?>
            </div>
            <div class="col-md-8">
                <?= $formNurse->textField($nurseModel, 'name', array('class' => 'fieldInput', 'placeholder' => 'Иванов И.И.')); ?>
                <?= $formNurse->error($nurseModel, 'name', array('class' => 'text-red')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($userModel, 'email') ?>
            </div>
            <div class="col-md-8">
                <?= $formNurse->textField($userModel, 'email', array('class' => 'fieldInput', 'placeholder' => 'sample@mail.ru')); ?>
                <?= $formNurse->checkBox($userModel, 'emailEnable', array('id' => 'User_emailEnable_3')); ?>
                <?= $formNurse->labelEx($userModel, 'emailEnable', array('for' => 'User_emailEnable_3')) ?>
                <?= $formNurse->error($userModel, 'email', array('class' => 'text-red')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($userModel, 'password') ?>
            </div>
            <div class="col-md-8">
                <?= $formNurse->passwordField($userModel, 'password', array('class' => 'fieldInput', 'value' => '')); ?>
                <?= $formNurse->error($userModel, 'password', array('class' => 'text-red')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($userModel, 'phone') ?>
            </div>
            <div class="col-md-8">
                <?= $formNurse->textField($userModel, 'phone', array('class' => 'fieldInput', 'placeholder' => '+79000000000')) ?>
                <?= CHtml::button('Изменить', array('class' => 'btn-change', 'name' => 'changePhone')) ?>
                <?= $formNurse->error($userModel, 'phone', array('class' => 'text-red')) ?>
                <div class="indent-top hidden">
                    <?= CHtml::textField('code', '', array('class' => 'fieldInput', 'placeholder' => 'Введите пароль из SMS')) ?>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($userModel, 'country') ?>
            </div>
            <div class="col-md-8">
                <?= $formNurse->dropDownList($userModel, 'country', array('0' => 'Выберите страну', 'Россия' => 'Россия'), array('class' => 'fieldInput', 'data-alias' => 'country')); ?>
                <?= $formNurse->error($userModel, 'country', array('class' => 'text-red')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($userModel, 'city') ?>
            </div>
            <div class="col-md-4 col-xs-6">
                <?= $formNurse->textField($userModel, 'city', array('class' => 'fieldInput inputForSearchCity')); ?>
                <?= $formNurse->error($userModel, 'city', array('class' => 'text-red')) ?>
                <select class="dropDownCities" type="select" size="5" data-alias="city">
                    <option>Выберите город</option>
                </select>
            </div>
            <div class="col-md-4 col-xs-6">
                <?= $formNurse->dropDownList($stationModel, 'name', array('0' => 'Выберите станцию метро', 'Нет метро' => 'Нет метро'), array('class' => 'fieldInput', 'data-alias' => 'metro', 'multiple' => true, 'style' => 'padding-top:3px;padding-bottom:4px;')); ?>
                <?= $formNurse->error($stationModel, 'name', array('class' => 'text-red')) ?>
                <div class="selectButton"></div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($nurseModel, 'education') ?>
            </div>
            <div class="col-md-8">
                <?= $formNurse->textArea($nurseModel, 'education', array('class' => 'fieldInput', 'style' => 'width: 100%; height: 100px')); ?>
                <i>* Для лучшего форматирования текста начинайте каждый пункт с новой строки</i>
                <?= $formNurse->error($nurseModel, 'education', array('class' => 'text-red')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($nurseModel, 'specialization') ?>
            </div>
            <div class="col-md-8">
                <?= $formNurse->textArea($nurseModel, 'specialization', array('class' => 'fieldInput', 'style' => 'width: 100%; height: 100px')); ?>
                <i>* Для лучшего форматирования текста начинайте каждый пункт с новой строки</i>
                <?= $formNurse->error($nurseModel, 'specialization', array('class' => 'text-red')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($nurseModel, 'expirience') ?>
            </div>
            <div class="col-md-8">
                <?= $formNurse->textArea($nurseModel, 'expirience', array('class' => 'fieldInput', 'style' => 'width: 100%; height: 100px')); ?>
                <i>* Для лучшего форматирования текста начинайте каждый пункт с новой строки</i>
                <?= $formNurse->error($nurseModel, 'expirience', array('class' => 'text-red')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($nurseModel, 'categorySick') ?>
            </div>
            <div class="col-md-8">
                <?= $formNurse->textField($nurseModel, 'categorySick', array('class' => 'fieldInput')); ?>
                <?= $formNurse->error($nurseModel, 'categorySick', array('class' => 'text-red')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($nurseModel, 'residence') ?>
            </div>
            <div class="col-md-8">
                <?= $formNurse->radioButtonList($nurseModel, 'residence', array(0 => 'Без проживания', 1 => 'С проживанием')); ?>
                <?= $formNurse->error($nurseModel, 'residence', array('class' => 'text-red')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($nurseModel, 'food') ?>
            </div>
            <div class="col-md-8">
                <?= $formNurse->radioButtonList($nurseModel, 'food', array(0 => 'Без приготовления еды', 1 => 'С приготовлением еды')); ?>
                <?= $formNurse->error($nurseModel, 'food', array('class' => 'text-red')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($nurseModel, 'priceDay') ?>
            </div>
            <div class="col-md-8">
                <?= $formNurse->numberField($nurseModel, 'priceDay', array('class' => 'fieldInput')); ?>
                <?= $formNurse->error($nurseModel, 'priceDay', array('class' => 'text-red')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($nurseModel, 'priceMonth') ?>
            </div>
            <div class="col-md-8">
                <?= $formNurse->numberField($nurseModel, 'priceMonth', array('class' => 'fieldInput')); ?>
                <?= $formNurse->error($nurseModel, 'priceMonth', array('class' => 'text-red')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($nurseModel, 'description') ?>
            </div>
            <div class="col-md-8">
                <?= $formNurse->textArea($nurseModel, 'description', array('class' => 'fieldInput', 'style' => 'width: 100%; height: 100px')); ?>
                <?= $formNurse->error($nurseModel, 'description', array('class' => 'text-red')) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($nurseModel, 'image') ?>
            </div>
            <div class="col-md-8">
                <?= $formNurse->fileField($nurseModel, 'image', array('class' => 'fieldInput')); ?>
                <?= $formNurse->error($nurseModel, 'image', array('class' => 'text-red')) ?>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($socialModel, 'vkontakte') ?>
            </div>
            <div class="col-md-8">
                <?= $formNurse->textField($socialModel, 'vkontakte', array('class' => 'fieldInput')); ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($socialModel, 'odnoklassniki') ?>
            </div>
            <div class="col-md-8">
                <?= $formNurse->textField($socialModel, 'odnoklassniki', array('class' => 'fieldInput')); ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($socialModel, 'facebook') ?>
            </div>
            <div class="col-md-8">
                <?= $formNurse->textField($socialModel, 'facebook', array('class' => 'fieldInput')); ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($socialModel, 'twitter') ?>
            </div>
            <div class="col-md-8">
                <?= $formNurse->textField($socialModel, 'twitter', array('class' => 'fieldInput')); ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <?= $formNurse->labelEx($socialModel, 'instagram') ?>
            </div>
            <div class="col-md-8">
                <?= $formNurse->textField($socialModel, 'instagram', array('class' => 'fieldInput')); ?>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <?= CHtml::submitButton('Обновить', array('class' => 'button1 button1reg')); ?>
            </div>
        </div>
    </span>
    <?php $this->endWidget(); ?>
</div>
<script src="//cdn.ckeditor.com/4.5.4/standard/ckeditor.js"></script>
<script type="text/javascript">
    //CKEDITOR INITIALIZATION
    //NURSE
    CKEDITOR.replace('Nurse[education]');
    CKEDITOR.replace('Nurse[specialization]');
    CKEDITOR.replace('Nurse[expirience]');
    CKEDITOR.replace('Nurse[description]');
    //call when form submit
    var crutchForNurseCKEditor = function(){
        var iframesNurse = $('.userEditNurse iframe');
        for(var i=0;i<iframesNurse.length;i++) {
            var text = iframesNurse[i].contentDocument.childNodes[1].childNodes[1].childNodes[0].innerHTML;
            if (text != '<br>') {
                $(iframesNurse[i].parentNode.parentNode.parentNode.parentNode).find('textarea').val(text);
            } else
                $(iframesNurse[i].parentNode.parentNode.parentNode.parentNode).find('textarea').val('');
        }
    }
</script>