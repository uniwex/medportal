<?php
/* @var $this TestController */
/* @var $dataProvider CActiveDataProvider */

?>
<link href='<?= Yii::app()->request->baseUrl ?>/assets/styles/rating.css' media="screen" rel="stylesheet"
      type="text/css"/>
<script type="text/javascript" src='<?= Yii::app()->request->baseUrl ?>/assets/scripts/rating.js'></script>
<? Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
$.fn.yiiListView.update('digestlistview', {
//this entire js section is taken from admin.php. w/only this line diff
data: $(this).serialize()
});
return false;
});
");
?>
<div id="postAdvert" class="col-md-10 col-xs-10">
<?php  $this->renderPartial('_search',array(
    'model'=>$model,
)); ?>

<span id="listOfDigest">
<?php
$dataProvider->sort->defaultOrder='time DESC';
$this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_view',
    'id'=>'digestlistview',
    'template'=>'{sorter}{items}{pager}',
    /*
    'sortableAttributes' => array(
        'time',
        'avg_rating'
    ),
    */
));
?>
</span>
</div>