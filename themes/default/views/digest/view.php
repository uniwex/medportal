<div id="postAdvert" class="col-md-10 articleDescription">
    <div class="row form-group no-margin">
        <div class="col-md-12">
            <div class="row form-group">
                <div class="col-md-10 article-author">
                    <?php
                    echo 'Администратор';
                    ?>
                </div>
                <div class="col-md-2 text-r no-padding">
                    <?= date('d-m-Y',strtotime($digest->time)) ?>
                </div>
                <?php
                if (!Yii::app()->user->isGuest) {
                    if (User::model()->findByPk(Yii::app()->user->id)->role == 'admin') {
                        ?>
                        <div class="col-md-1 text-r"><a href="/digest/edit/<?= $digest->id; ?>"><i
                                    class="fa fa-pencil-square-o"></i></a>
                            <i class="fa fa-times" onclick="removeArticle(this, 1)" data-id="<?= $digest->id; ?>"></i>
                        </div>
                    <?php
                    }
                }
                ?>
            </div>
            <div class="row">
                <div class="col-md-12 no-padding">
                    <?php
                    $categories = "";
                    if($digest->idCategory) {
                        foreach (explode(',',$digest->idCategory) as $key) {
                            $categories .= Category::model()->findByPk($key)->name . ',';
                        }
                    }
                    $categories = substr($categories,0,-1);
                    ?>
                    Категория: <?= $categories; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 no-padding" style="font-size:18pt;">
                    <?= $digest->name ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $digest->text ?>
        </div>
    </div>
</div>
<script src="/assets/scripts/articles.js"></script>