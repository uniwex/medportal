<?php
/* @var $this TestController */
/* @var $model Article */
/* @var $form CActiveForm */
?>

<div class="wide form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    )); ?>
    <div class="row form-group">
        <!-- condition is for do not display fields which in this condition -->
        <?php if(false){ ?>
            <div class="col-md-3">
                <p>Дата с</p>
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'name' => 'dateStart',
                    'model' => $model,
                    'attribute' => 'dateStart',
                    'language' => 'ru',
                    'options' => array(
                        'dateFormat' => 'yy-mm-dd',
                        'class' => 'fieldInput',
                        'showAnim' => 'fold',
                    ),
                    'htmlOptions' => array(
                        'class' => 'fieldInput',
                        'id' => 'dateStart',
                    ),
                )); ?>
            </div>
            <div class="col-md-3">
                <p>Рейтинг с</p>
                <? echo CHtml::activeDropDownList($model, 'minRatingFilter', array(0 => '0', 1 => '1', 2 => '2', 3 => '3', 4 => '4'), array(
                    'class' => 'fieldInput',
                )); ?>
            </div>
            <div class="col-md-3">
                <p>Категория</p>
                <?php
                echo CHtml::activeDropDownList($model, 'id_category', CHtml::listData(Category::model()->findAll(), 'id', 'name', 'category'), array(
                    'class' => 'fieldInput',
                    'prompt' => 'Не выбрана',
                ));
                ?>
            </div>
        <?php } ?>
        <? if(!Yii::app()->user->IsGuest) {
            if(User::model()->findByPk(Yii::app()->user->id)->role == 'admin'){ ?>
            <div class="col-md-12">
                <? echo CHtml::link('Добавить дайджест',array('digest/add') ,
                    array(
                        'style' => 'margin-top:27px;display:block;width:200px;text-decoration:none;color:#fff;float:right;padding:5px 16px;background-color:#456BA5;',
                        'class' => 'help-home'
                    )); ?>
            </div>
        <?php } } ?>
    </div>
    <div class="row">
        <?php if(false) { ?>
        <div class="col-md-3">
            <p>Дата по</p>
            <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'name' => 'dateEnd',
                'model' => $model,
                'attribute' => 'dateEnd',
                'language' => 'ru',
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                    'class' => 'fieldInput',
                    'showAnim' => 'fold',
                ),
                'htmlOptions' => array(
                    'class' => 'fieldInput',
                    'id' => 'dateEnd',
                ),
            )); ?>
        </div>
        <div class="col-md-3">
            <p>Рейтинг по</p>
            <? echo CHtml::activeDropDownList($model, 'maxRatingFilter', array(1 => '1', 2 => '2', 3 => '3', 4 => '4', 5 => '5'), array(
                'class' => 'fieldInput',
            )); ?>
        </div>
    <?php if (!Yii::app()->user->isGuest && Yii::app()->user->id) { ?>
        <div class="col-md-3">
            <p>Автор дайджеста</p>
            <?php
            echo CHtml::activeDropDownList($model, 'onlyMe', array('0' => 'Любой', '1' => 'Только я'), array(
                'class' => 'fieldInput',
            ));
            ?>
        </div>
    <? } ?>
    <? if(!Yii::app()->user->IsGuest) { ?>
        <div class="col-md-3">
            <?php } else { ?>
            <div class="col-md-3 col-md-push-3">
                <?php } ?>
                <?php echo CHtml::submitButton('Поиск',
                    array(
                        'style' => 'font-family:MyriadProRegular;margin-top:20px;border:medium none;width:200px;float:right;text-align:center;font-size:18px;',
                        'class' => 'help-home bg-liteRed'
                    )); ?>
            </div>
            <?php } ?>
        </div>
        <?php $this->endWidget(); ?>
    </div>

