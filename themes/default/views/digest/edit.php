<div id="postAdvert" class="col-md-10">
    <?php $formDigest = $this->beginWidget("CActiveForm", array(
        'id' => 'edit-digest',
        'action' => Yii::app()->request->baseUrl . '/digest/update',
        'method' => 'post',
//        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true
        ),
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
            'autocomplete' => 'off',
//            'onsubmit' => 'return true',
        ),
    )); ?>
    <div class="form-group row">
        <div class="col-md-2">
            <?= $formDigest->labelEx($digestModel, 'name') ?>
        </div>
        <div class="col-md-8">
            <?= $formDigest->textField($digestModel, 'name', array('class' => 'fieldInput', 'placeholder' => 'О сайте')); ?>
            <?= $formDigest->error($digestModel, 'name', array('class' => 'text-red')) ?>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-2">
            Категория
        </div>
        <div class="col-md-8">
            <?php
            array($digestModel->idCategory => array('selected'=>true));
            $arrayIdCategories = explode(',',$digestModel->idCategory);
            $arrayCategories = array();
            for($i=0;$i<count($arrayIdCategories);$i++){
                $arrayCategories[$arrayIdCategories[$i]] = array('selected'=>true);
            }
            ?>
            <?= $formDigest->dropDownList($categoryModel, 'name', CHtml::listData(Category::model()->findAll(), 'id', 'name', 'category'), array('class' => 'fieldInput', 'multiple'=>'multiple' , 'options' => $arrayCategories)); ?>
            <?= $formDigest->error($categoryModel, 'name', array('class' => 'text-red')) ?>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-2">
            <?= $formDigest->labelEx($digestModel, 'text') ?>
        </div>
        <div class="col-md-8">
            <?= $formDigest->textArea($digestModel, 'text', array('class' => 'fieldInput')); ?>
            <?= $formDigest->error($digestModel, 'text', array('class' => 'text-red')) ?>
        </div>
    </div>
    <?= $formDigest->hiddenField($digestModel, 'id', array('class' => 'fieldInput', 'value'=>$digestModel->id)); ?>
    <div class="form-group row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <?= CHtml::submitButton('Сохранить статью', array('class' => 'button1 button1reg')); ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
<script src="//cdn.ckeditor.com/4.5.4/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('Digest[text]');
</script>