<?php
/* @var $this NurseController */
/* @var $dataProvider CActiveDataProvider */

/*$this->breadcrumbs=array(
	'Nurses',
);*/

$this->menu=array(
array('label'=>'Create Nurse', 'url'=>array('create')),
array('label'=>'Manage Nurse', 'url'=>array('admin')),
);
?>

<h1>Nurses</h1>

<?php $this->widget('zii.widgets.CListView', array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
