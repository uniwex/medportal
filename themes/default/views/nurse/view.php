<?php
/* @var $this NurseController */
/* @var $model Nurse */

/*$this->breadcrumbs=array(
	'Nurses'=>array('index'),
	$model->name,
);*/

$this->menu = array(
    array('label' => 'List Nurse', 'url' => array('index')),
    array('label' => 'Create Nurse', 'url' => array('create')),
    array('label' => 'Update Nurse', 'url' => array('update', 'id' => $model->idUser)),
    array('label' => 'Delete Nurse', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->idUser), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage Nurse', 'url' => array('admin')),
);
?>

<h1><?php echo $model->name; ?></h1>
<div class="box box-info text-left">
    <?php $this->widget('zii.widgets.CDetailView', array(
        'data' => $model,
        'attributes' => array(
            array(
                'type'=>'raw',
                'value'=> CHtml::image(Yii::app()->request->baseUrl.'/upload/'.$model->pic,'', array('style'=>'width: 30%;')),
            ),
            'idUser',
            'name',
            array(
                'label' => 'Опыт работы',
                'type'=>'raw',
                'value'=> strip_tags($model->expirience),
            ),
            'description',
            array(
                'label' => 'Опыт работы',
                'type'=>'raw',
                'value'=> strip_tags($model->specialization),
            ),
            array(
                'label' => 'Опыт работы',
                'type'=>'raw',
                'value'=> strip_tags($model->education),
            ),
            'priceHour',
            'priceDay',
            'priceMonth',
            'rate',
            'residence',
            'food',
            'categorySick',

        ),
    )); ?>
</div>
