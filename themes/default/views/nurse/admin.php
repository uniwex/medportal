<?php
/* @var $this NurseController */
/* @var $model Nurse */

//$this->breadcrumbs=array(
//	'Nurses'=>array('index'),
//	'Manage',
//);

/*$this->menu=array(
array('label'=>'List Nurse', 'url'=>array('index')),
array('label'=>'Create Nurse', 'url'=>array('create')),
);*/

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$('#nurse-grid').yiiGridView('update', {
data: $(this).serialize()
});
return false;
});
");
?>

<!--<h1>Manage Nurses</h1>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
    or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>
-->
<?php /*echo CHtml::link('Advanced Search', '#', array('class' => 'search-button')); */?><!--
<div class="search-form" style="display:none">
    <?php /*$this->renderPartial('_search', array(
        'model' => $model,
    )); */?>
</div><!-- search-form -->

<div class="row">
    <div class="col-md-12">

        <div class="box box-info">
            <div class="box-header with-border ui-sortable-handle">
                <h3 class="box-title">Список сиделок</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <?php $this->widget('zii.widgets.grid.CGridView', array(
                    'id' => 'nurse-grid',
                    'dataProvider' => $model->search(),
                    'filter' => $model,
                    'cssFile' => Yii::app()->request->getBaseUrl(true) . '/assets/admin/css/gridview.css',
                    'itemsCssClass' => 'table no-margin',
                    'htmlOptions' => array(
                        'class' => 'table-responsive'
                    ),
                    'summaryText' => 'Показано записей: {start} - {end} из {count}',
                    'emptyText' => 'Результаты не найдены',
                    'columns' => array(
                        'idUser',
                        array(
                            'name' => 'pic',
                            'type' => 'image',
                            'value' => '"/upload/".$data->pic',
                            'htmlOptions' => array('class' => 'imgGood'),
                        ),
                        'name',
                        'expirience',
                        'description',
                        'specialization',
                        'education',
                        /*
                        'priceDay',
                        'priceMonth',
                        'rate',
                        'residence',
                        'food',
                        'categorySick',
                        'pic',
                        */
                        array(
                            'class' => 'CButtonColumn',
                        ),
                    ),
                )); ?>
            </div>
            <div class="box-footer clearfix"></div>
        </div>
    </div>
</div>
