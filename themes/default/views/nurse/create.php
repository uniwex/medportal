<?php
/* @var $this NurseController */
/* @var $model Nurse */

/*$this->breadcrumbs=array(
	'Nurses'=>array('index'),
	'Create',
);*/
?>
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Создание сиделки <?= $model->idUser; ?></h3>
            </div>
            <?php $this->renderPartial('_form', array('model' => $model)); ?>
        </div>
    </div>
</div>