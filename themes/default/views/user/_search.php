<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

                    <div class="row">
            <?php echo $form->label($model,'id'); ?>
            <?php echo $form->textField($model,'id'); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'name'); ?>
            <?php echo $form->textArea($model,'name',array('rows'=>6, 'cols'=>50)); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'phone'); ?>
            <?php echo $form->textField($model,'phone',array('size'=>60,'maxlength'=>255)); ?>
        </div>

                            <div class="row">
            <?php echo $form->label($model,'money'); ?>
            <?php echo $form->textField($model,'money'); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'city'); ?>
            <?php echo $form->textField($model,'city',array('size'=>60,'maxlength'=>255)); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'country'); ?>
            <?php echo $form->textField($model,'country',array('size'=>60,'maxlength'=>255)); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'email'); ?>
            <?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255)); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'emailEnable'); ?>
            <?php echo $form->textField($model,'emailEnable'); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'type'); ?>
            <?php echo $form->textField($model,'type',array('size'=>0,'maxlength'=>0)); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'dateRegistration'); ?>
            <?php echo $form->textField($model,'dateRegistration'); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'vipFinish'); ?>
            <?php echo $form->textField($model,'vipFinish'); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'status'); ?>
            <?php echo $form->textField($model,'status',array('size'=>0,'maxlength'=>0)); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'restoreCode'); ?>
            <?php echo $form->textField($model,'restoreCode',array('size'=>32,'maxlength'=>32)); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'isConfirm'); ?>
            <?php echo $form->textField($model,'isConfirm'); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'role'); ?>
            <?php echo $form->textField($model,'role',array('size'=>5,'maxlength'=>5)); ?>
        </div>

        <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->