<?php
/* @var $this ReviewController */
/* @var $model Review */
/* @var $form CActiveForm */
?>

<div class="box-body">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'review-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    )); ?>



    <?php echo $form->errorSummary($model); ?>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'idUser'); ?>
        <?php echo $form->dropDownList($model, 'idUser', CHtml::listData(User::model()->findAll(), 'id', 'name'), array('class' => 'form-control')); ?>
        <?php echo $form->error($model, 'idUser'); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'idUserTo'); ?>
        <?php echo $form->dropDownList($model, 'idUserTo', CHtml::listData(User::model()->findAll(), 'id', 'name'), array('class' => 'form-control')); ?>
        <?php echo $form->error($model, 'idUserTo'); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'name'); ?>
        <?php echo $form->textArea($model, 'name', array('class' => 'form-control')); ?>
        <?php echo $form->error($model, 'name'); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'text'); ?>
        <?php echo $form->textArea($model, 'text', array('class' => 'form-control')); ?>
        <?php echo $form->error($model, 'text'); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'cure'); ?>
        <?php echo $form->textField($model, 'cure', array('class' => 'form-control')); ?>
        <?php echo $form->error($model, 'cure'); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'regard'); ?>
        <?php echo $form->textField($model, 'regard', array('class' => 'form-control')); ?>
        <?php echo $form->error($model, 'regard'); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'qualification'); ?>
        <?php echo $form->textField($model, 'qualification', array('class' => 'form-control')); ?>
        <?php echo $form->error($model, 'qualification'); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'time'); ?>
        <?php echo $form->textField($model, 'time', array('class' => 'form-control')); ?>
        <?php echo $form->error($model, 'time'); ?>
    </div>
    <div class="form-group">
        <?php echo $form->labelEx($model, 'moderate'); ?>
        <?php echo $form->dropDownList($model, 'moderate', array(0 => 'Не проверен',1 => 'Проверен'), array('class' => 'form-control'));  ?>
        <?php echo $form->error($model, 'moderate'); ?>
    </div>


    <div class="box-footer">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array('class' => 'btn btn-primary pull-right')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->