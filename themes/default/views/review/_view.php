<?php
/* @var $this ReviewController */
/* @var $data Review */
?>

<div class="view">

    	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idUserFrom')); ?>:</b>
	<?php echo CHtml::encode($data->idUserFrom); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idUserTo')); ?>:</b>
	<?php echo CHtml::encode($data->idUserTo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('text')); ?>:</b>
	<?php echo CHtml::encode($data->text); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cure')); ?>:</b>
	<?php echo CHtml::encode($data->cure); ?>
	<br />
	<b><?php echo CHtml::encode($data->getAttributeLabel('moderate')); ?>:</b>
	<?php echo CHtml::encode($data->moderate); ?>
	<br />
	<b><?php echo CHtml::encode($data->getAttributeLabel('regard')); ?>:</b>
	<?php echo CHtml::encode($data->regard); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('qualification')); ?>:</b>
	<?php echo CHtml::encode($data->qualification); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('time')); ?>:</b>
	<?php echo CHtml::encode($data->time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pics')); ?>:</b>
	<?php echo CHtml::encode($data->pics); ?>
	<br />

	*/ ?>

</div>