<?php
/* @var $this ScienceController */
/* @var $dataProvider CActiveDataProvider */

$this->breadCrumbs=array(
	'Science Works',
);

$this->menu=array(
array('label'=>'Create ScienceWorks', 'url'=>array('create')),
array('label'=>'Manage ScienceWorks', 'url'=>array('admin')),
);
?>

<h1>Science Works</h1>

<?php $this->widget('zii.widgets.CListView', array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
