<?php
/* @var $this ScienceController */
/* @var $model ScienceWorks */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'science-works-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'errorMessageCssClass' => 'control-label',
    'clientOptions' => array(
        'validateOnChange' => true,
        'validateOnSubmit' => true,
        'errorCssClass' => 'has-error',
        'successCssClass' => 'has-success'
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
    <div class="row">
        <div class="col-md-6 createGood">
            <div class="box box-primary">
                <div class="box-header with-border">
                </div>
                <div class="box-body">
                    <?php echo $form->errorSummary($model); ?>

                    <div class="form-group">
                    <?php echo $form->labelEx($model,'name'); ?><br>
                    <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
                    <?php echo $form->error($model,'name'); ?>
                    </div>

                    <div class="form-group">
                    <?php echo $form->labelEx($model,'description'); ?><br>
                    <?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
                    <?php echo $form->error($model,'description'); ?>
                    </div>

                    <div class="form-group">
                    <?php echo $form->labelEx($model,'idAuthor'); ?><br>
                    <?php echo $form->textField($model,'idAuthor'); ?>
                    <?php echo $form->error($model,'idAuthor'); ?>
                    </div>

                    <div class="form-group">
                    <?php echo $form->labelEx($model,'link'); ?><br>
                    <?php echo $form->fileField($model,'link',array('class' => 'form-control')); ?>
                    <?php echo $form->error($model,'link'); ?>
                    </div>

                    <div class="form-group">
                    <?php echo $form->labelEx($model,'picture'); ?><br>
                    <?php echo $form->fileField($model,'picture',array('class' => 'form-control')); ?>
                    <?php echo $form->error($model,'picture'); ?>
                    </div>

                    <div class="row buttons text-center">
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить всё', array('class' => 'btn btn-primary save')); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
<!-- form -->
