<?php
    /* @var $this ScienceController */
    /* @var $model ScienceWorks */

$this->breadCrumbs=array(
	'Научные работы'=>array('admin'),
	$model->name=>array('view','id'=>$model->id),
	'Изменить',
);

//    $this->menu=array(
//    array('label'=>'List ScienceWorks', 'url'=>array('index')),
//    array('label'=>'Create ScienceWorks', 'url'=>array('create')),
//    array('label'=>'View ScienceWorks', 'url'=>array('view', 'id'=>$model->id)),
//    array('label'=>'Manage ScienceWorks', 'url'=>array('admin')),
//    );
//    ?>


<?php $this->renderPartial('_form', array('model'=>$model)); ?>