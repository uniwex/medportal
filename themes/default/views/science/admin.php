<?php
/* @var $this ScienceController */
/* @var $model ScienceWorks */

$this->breadCrumbs=array(
	'Научные работы'=>array('admin'),
	'Список',
);


?>


<p>
    При поиске вы можете использоать знаки(<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
    or <b>=</b>) .
</p>


<div class="row">
	<div class="col-md-12">

		<div class="box box-info">
			<div class="box-header with-border ui-sortable-handle">
				<h3 class="box-title">Список клиник</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove">
						<i class="fa fa-times"></i></button>
				</div>
			</div>
			<div class="box-body">
			<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'science-works-grid',
			'dataProvider'=>$model->search(),
			'filter'=>$model,
			'cssFile' => Yii::app()->request->getBaseUrl(true) . '/assets/admin/css/gridview.css',
			'itemsCssClass' => 'table no-margin',
			'htmlOptions' => array(
				'class' => 'table-responsive'
			),
			'summaryText' => 'Показано записей: {start} - {end} из {count}',
			'emptyText' => 'Результаты не найдены',
			'columns'=>array(
					'id',
					array(
						'name' => 'picture',
						'type' => 'image',
						'value' => '"/upload/".$data->picture',
						'htmlOptions' => array('class' => 'imgGood'),
					),
					'name',
					'description',
					'idAuthor',
					array(
						//'class'=>'CLinkColumn',
						'name'=>'link',
						'type'=>'raw',
						//'labelExpression'=>'$data->link',
						'value'=>'CHtml::link(\'pdf\',Yii::app()->request->getBaseUrl(true) .\'/upload/\'.$data->link)',
					),


			array(
			'class'=>'CButtonColumn',
			),
			),
			)); ?>
			</div>
		</div>
	</div>
</div>