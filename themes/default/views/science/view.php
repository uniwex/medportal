<?php
/* @var $this ScienceController */
/* @var $model ScienceWorks */

$this->breadCrumbs=array(
	'Научные работы'=>array('admin'),
	$model->name,
);

$this->menu=array(
array('label'=>'List ScienceWorks', 'url'=>array('index')),
array('label'=>'Create ScienceWorks', 'url'=>array('create')),
array('label'=>'Update ScienceWorks', 'url'=>array('update', 'id'=>$model->id)),
array('label'=>'Delete ScienceWorks', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage ScienceWorks', 'url'=>array('admin')),
);
?>

<h1>View ScienceWorks #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
'data'=>$model,
'attributes'=>array(
		'id',
		'name',
		'description',
		'idAuthor',
		'link',
		'picture',
),
)); ?>
