<?php
/* @var $this DoctorController */
/* @var $model Doctor */
/* @var $form CActiveForm */
?>

<div class="form adminEditDoctor">

<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'doctor-form',

    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'errorMessageCssClass' => 'control-label',
    'clientOptions' => array(
        'validateOnChange' => true,
        'validateOnSubmit' => true,
        'errorCssClass' => 'has-error',
        'successCssClass' => 'has-success'
    ),
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
        'onsubmit' => 'crutchForDoctorCKEditor();return true',
    ),
)); ?>

    <div class="box-body">

        <?php echo $form->errorSummary($model, 'Пожалуйста, исправьте следующие ошибки:', '', array('class' => 'callout callout-danger')); ?>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'idUser'); ?>
            <?php echo $form->dropDownList($model, 'idUser', CHtml::listData(User::model()->findAll('type = :id', array(':id' => 'doctor')), 'id', 'name'), array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'idUser'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'idSpecialization'); ?>
            <?php echo $form->dropDownList($model, 'idSpecialization', CHtml::listData(Specialization::model()->findAll(), 'id', 'name'), array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'idSpecialization'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'name'); ?>
            <?php echo $form->textField($model, 'name', array( 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'name'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'expirience'); ?>
            <?php echo $form->textArea($model, 'expirience', array('rows' => 6, 'cols' => 50, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'expirience'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'license'); ?>
            <?php echo $form->textField($model, 'license', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'license'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'description'); ?>
            <?php echo $form->textArea($model, 'description', array('rows' => 6, 'cols' => 50, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'description'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'merit'); ?>
            <?php echo $form->textArea($model, 'merit', array('rows' => 6, 'cols' => 50, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'merit'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'specialization'); ?>
            <?php echo $form->textArea($model, 'specialization', array('rows' => 6, 'cols' => 50, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'specialization'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'education'); ?>
            <?php echo $form->textArea($model, 'education', array('rows' => 6, 'cols' => 50, 'class' => 'form-control')); ?>
            <?php echo $form->error($model, 'education'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'enabled'); ?>
            <?php echo $form->dropDownList($model, 'enabled', array(0 => 'Не активен', 1 => 'Активен',), array('class' => 'form-control')); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'price'); ?>
            <?php echo $form->textField($model, 'price', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'price'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'timeSize'); ?>
            <?php echo $form->textField($model, 'timeSize', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'timeSize'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'rate'); ?>
            <?php echo $form->dropDownList($model, 'rate', array(0 => '0',1 => '1', 2 => '2', 3 => '3',4 => '4',5 => '5'), array('class' => 'form-control'));  ?>
            <?php echo $form->error($model, 'rate'); ?>
        </div>

        <div id="clinicName" class="form-group">
            <?php echo $form->labelEx($model,'idClinic'); ?><br>
            <?php echo $form->dropDownList($model,'idClinic',$clinic, array('class' => 'form-control')); ?>
            <?php echo $form->error($model,'idClinic'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'typeHuman'); ?>
            <?php echo $form->dropDownList($model, 'typeHuman', array(1 => 'Дети', 2 => 'Взрослые', 3 => 'Все'), array('class' => 'form-control'));  ?>
            <?php echo $form->error($model, 'typeHuman'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'isPhototherapy'); ?>
            <?php echo $form->dropDownList($model, 'isPhototherapy', array(0 => 'Не практикует', 1 => 'Практикует'), array('class' => 'form-control'));  ?>
            <?php echo $form->error($model, 'isPhototherapy'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'image'); ?>
            <?php echo $form->fileField($model, 'image', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'image'); ?>
        </div>


        <div class="box-footer">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array('class' => 'btn btn-primary pull-right')); ?>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>
<script src="//cdn.ckeditor.com/4.5.4/standard/ckeditor.js"></script>
<script type="text/javascript">
    //CKEDITOR INITIALIZATION
    //NURSE
    CKEDITOR.replace('Doctor[education]');
    CKEDITOR.replace('Doctor[specialization]');
    CKEDITOR.replace('Doctor[expirience]');
    //call when form submit
    var crutchForDoctorCKEditor = function(){
        var iframesDoctor = $('.adminEditDoctor iframe');
        for(var i=0;i<iframesDoctor.length;i++) {
            var text = iframesDoctor[i].contentDocument.childNodes[1].childNodes[1].childNodes[0].innerHTML;
            if (text != '<br>') {
                $(iframesDoctor[i].parentNode.parentNode.parentNode.parentNode).find('textarea').val(text);
            } else
                $(iframesDoctor[i].parentNode.parentNode.parentNode.parentNode).find('textarea').val('');
        }
    }
</script>