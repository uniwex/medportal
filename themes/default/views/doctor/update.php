<?php
/* @var $this DoctorController */
/* @var $model Doctor */

?>

<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Изменение врача <?= $model->name; ?></h3>
            </div>
            <?php $this->renderPartial('_form', array('model' => $model, 'clinic'=>$clinic)); ?>
        </div>
    </div>
</div>

