<div class="col-md-7 col-xs-7 auth">
    <div class="row enter">
        <div class="col-md-5">
    <? $form = $this->beginWidget('CActiveForm', array(
        'id' => 'login-form',
        'action' => Yii::app()->request->baseUrl . '/site/login',
        'method' => 'post',
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmmit' => true,
            'validateOnChange' => true
        )
    )) ?>
    <h3>Вход</h3>
    <div id="error" class="text-red form-group hidden"></div>
    <div class="form-group">
        <?= $form->label( $model, 'phone' ); ?>
        <? $this->widget('CMaskedTextField', array(
            'model' => $model,
            'attribute' => 'phone',
            'mask' => '+99999999999',
            'placeholder' => '*',
            'htmlOptions' => array(
                'class' => 'form-control',
                /*'placeholder' => 'Телефон *'*/
            )
        )); ?>
        <?= $form->error($model, 'phone') ?>
    </div>

    <div class="form-group">
        <?= $form->label( $model, 'password' ); ?>
        <?= $form->passwordField($model, 'password', array('class' => 'form-control', /*'placeholder' => 'Пароль *'*/)) ?>
        <?= $form->error($model, 'password') ?>
    </div>
    <div class="row pre-button">
        <div class="col-md-7">
            <?= CHtml::checkBox('keepSystem', true, ['value' => 'keepSystem']) ?>
            <?= CHtml::label('Оставаться в системе', 'keepSystem', ['class' => 'keepSystem'])?>
        </div>
        <div class="col-md-5">
        <a name="restore" href="<?= Yii::app()->request->getBaseUrl(true) ?>/site/restore">Забыли пароль?</a>
        </div>
    </div>
    <div class="form-group">
        <?= CHtml::ajaxSubmitButton('Войти', Yii::app()->request->baseUrl . '/site/login', array(
            'success' => 'function(data){
                    if(/Ошибка авторизации/.test(data)) {
                        $("#error").removeClass("hidden");
                        $("#error").html(data);
                    } else if(data == 1) {
                        location.href = "' . Yii::app()->request->getBaseUrl(true) . '";
                    }
                }'
        ), array('class' => 'btn btn-primary btn-lg')) ?>

    </div>
    <? $this->endWidget(); ?>
        </div>
        <div class="col-md-1 vr">
            <hr noshade>
        </div>

        <div class="col-md-6 reg">
            <h4>Впервые на нашем сайте? </h4>
            <span>Начните прямо сейчас. Просто сделайте это!</span>
            <div class="reg-link">
            <a href="<?= Yii::app()->request->getBaseUrl(true) ?>/site/registration">Зарегистрироваться</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 soc">
            <span>
            Или войдите, используя аккаунт социальной сети:
            </span>
            <div class="row soc-icons">
                <div class="col-md-1"><a href="http://vk.com" target="_blank"><img src="/assets/images/icons/vkontakte-logo.png"></a></div>
                <div class="col-md-1"><a href="http://ok.ru" target="_blank"><img src="/assets/images/icons/odnoklassniki-logo.png"></a></div>
                <div class="col-md-1"><a href="http://fb.com" target="_blank"><img src="/assets/images/icons/facebook-logo.png"></a></div>

            </div>
        </div>
    </div>

</div>

