<?php
if (isset($systemMsg)) { ?>
    <script>
        $(document).ready(function() {
            $('#myModalLabel').html('Регистрация');
            $('#modal-body').html('<?= $systemMsg; ?>');
            $('#myModal').modal();
        })
    </script>
<? } ?>

<div id="postAdvert" class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
    <div id="banners-main" class="col-lg-12 col-md-12 no-padding">
        <div class="col-lg-8 col-md-8 no-padding">
            <div id="carousel" class="carousel slide" data-interval="3000" data-ride="carousel">
                <div id="head-cab-carousel" class="carousel-inner main-carousel">
                    <?foreach ($infohub as $key => $value) {?>
                        <div class="item <?if ($key == '0') {echo 'active';}?>">
                            <a class="infohub-link" href="<?=$value->link?>" target="_blank"></a>
                            <div class="infohub-bg">
                                <p class="infohub-header"><?=$value->header?></p>
                                <?=$value->description?>
                            </div>
                            <img src="/upload/infohub/<?=$value->pic?>">
                        </div>
                    <?}?>
                </div>
            </div>
        </div>
        <div id="spec-project" class="col-lg-4 col-md-4">
            <a id="spec-project-link" href="<?=$specProject->link?>"></a>
            <img id="spec-project-bg" src="/upload/infohub/<?=$specProject->pic?>">
        </div>
    </div>
    <div class="infoBlock col-xs-12 indent col-lg-6 col-lg-push-0 col-sm-6 col-md-6" data-type="task">
        <img src="/assets/images/infoblock2.png">
        <h4>Регистрация пациентов</h4>
        <ul>
            <li>1 - Вы можете найти врача и клинику</li>
            <li>2 - Узнать стоимость приёма и рейтинг врача</li>
            <li>3 - Выбрать врача по параметрам</li>
            <li>4 - Быстро записаться на приём</li>
            <li>5 - Оставить отзыв о приёме</li>
        </ul>
        <button class="orange">Записаться</button>
    </div>

    <div class="infoBlock col-xs-12 col-lg-6 indent col-sm-6 col-md-6" data-type="task">
        <img src="/assets/images/infoblock1.png">
        <h4>Регистрация врачей, клиники, сиделки</h4>
        <ul>
            <li>1 - Зарегистрируйтесь</li>
            <li>2 - Разместите своё объявление</li>
            <li>3 - Заполните свой профиль</li>
            <li>4 - Оцените свой приём</li>
            <li>5 - Продвиньте своё объявление в поиске</li>
        </ul>
        <button class="blue">Подать объявление</button>
    </div>

    <div class="col-xs-12 col-md-12 col-sm-12 no-padding" style="margin: 13px 0px;">
        <img src="/assets/images/chart.png" style="float:left;"><a href="/relative"><span class="analysis">Сравнительный анализ лечения по странам</span></a>
    </div>
    <div class="col-xs-12 col-md-12 col-sm-12 no-padding">
        <h4 id="news">Новости</h4>
    </div>
    <?foreach ($news as $value) {?>
        <div class="col-xs-12 col-md-12 col-sm-12 no-padding">
            <p class="news">
                <span><?php echo Yii::app()->dateFormatter->format('d MMMM yyyy', $value->date);?></span> <?=$value->header;?><br>
            </p>
            <div class="news-desc">
                <?=$value->text;?>
            </div>
        </div>
    <?}?>
    <div class="col-xs-12 col-md-12 col-sm-12 no-padding">
        <a href="/news"><span class="news-more">Подробнее</span></a>
    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel"></h4>
                </div>
                <div class="modal-body" id="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>

</div>
<?= $this->renderPartial('rightMenu') ?>
