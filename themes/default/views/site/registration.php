<div class="col-md-7 col-xs-7 auth">
    <div class="row enter">
        <div class="col-md-6">
            <? $form = $this->beginWidget('CActiveForm', array(
                'id' => 'login-form',
                'action' => Yii::app()->request->baseUrl . '/site/regUser',
                'method' => 'post',
                'enableAjaxValidation' => true,
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmmit' => true,
                    'validateOnChange' => true
                )
            )) ?>
            <div class="row">
                <div class="col-md-8">
                    <h3>Регистрация</h3>
                </div>
                <div class="col-md-4">
                    <?= $form->dropDownList(
                        $model,
                        'type',
                        ['' => 'Кто вы?', '0' => 'Пациент', '1' => 'Клиника', '2' => 'Сиделка', '3' => 'Частный врач'],
                        [
                            'class' => 'user-level',
                            'options' => ['' => ['selected' => 'selected'], '0' => ['selected' => '']]
                        ]
                    ) ?>
                    <?= $form->error($model, 'type') ?>
                </div>
            </div>
            <div id="error" class="text-red form-group hidden"></div>
            <div class="form-group">
                <?=$form->label( $model, 'name' ); ?>
                <?=$form->textField($model, 'name', ['class' => 'form-control'])?>
                <?= $form->error($model, 'name') ?>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->label( $model, 'phone' ); ?>
                        <? $this->widget('CMaskedTextField', array(
                            'model' => $model,
                            'attribute' => 'phone',
                            'mask' => '+79999999999',
                            'placeholder' => '_',
                            'htmlOptions' => array(
                                'class' => 'form-control',
                                /*'placeholder' => 'Телефон *'*/
                            )
                        )); ?>
                        <?= $form->error($model, 'phone') ?>
                    </div>
                    <div class="col-md-6">
                        <?=$form->label( $model, 'email' ); ?>
                        <?=$form->textField($model, 'email', ['class' => 'form-control'])?>
                        <?= $form->error($model, 'email') ?>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <?= $form->label( $model, 'password' ); ?>
                <?= $form->passwordField($model, 'password', array('class' => 'form-control', /*'placeholder' => 'Пароль *'*/)) ?>
                <?= $form->error($model, 'password') ?>
            </div>

            <div class="form-group">
                <?/*= CHtml::ajaxSubmitButton('Зарегистрироваться', Yii::app()->request->baseUrl . '/site/regUser', array(
                    'success' => 'function(data){
                    if(data == 1) {
                        location.href = "' . Yii::app()->request->getBaseUrl(true) . '";
                    }
                }'
                ), array('class' => 'btn btn-primary btn-lg reg')) */?>
                <?= CHtml::submitButton('Зарегистрироваться', array('class' => 'btn btn-primary btn-lg reg')); ?>
            </div>
            <? $this->endWidget(); ?>
        </div>
        <div class="col-md-1 vr">
            <hr noshade>
        </div>

        <div class="col-md-5 reg ">

            <div class="to-login"><span>Уже есть учётная запись?</span></div>
            <div class="login-link">
                <a href="<?= Yii::app()->request->getBaseUrl(true) ?>/site/login">Войти</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 soc">
            <span>
            Или зарегистрируйтесь, используя аккаунт социальной сети:
            </span>
            <div class="row soc-icons">
                <div class="col-md-1"><a href="http://vk.com" target="_blank"><img src="/assets/images/icons/vkontakte-logo.png"></a></div>
                <div class="col-md-1"><a href="http://ok.ru" target="_blank"><img src="/assets/images/icons/odnoklassniki-logo.png"></a></div>
                <div class="col-md-1"><a href="http://fb.com" target="_blank"><img src="/assets/images/icons/facebook-logo.png"></a></div>

            </div>
        </div>
    </div>

</div>

