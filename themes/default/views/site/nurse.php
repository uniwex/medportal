<div class="overlay" id="overlay-make-vip">
    <div class="modal-window" style="height:145px;">
        <div class="close-button">X</div>
        <div class="form-group row indent" style="font-size:16px;">Выберите тариф:</div>
        <?php $formMakeVip = $this->beginWidget("CActiveForm", array(
            'id' => 'form-make-vip',
            'action' => Yii::app()->request->baseUrl . '/site/buyVip',
            'method' => 'post',
            'enableAjaxValidation' => true,
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => true
            ),
            'htmlOptions' => array(
                'enctype' => 'multipart/form-data',
                'autocomplete' => 'off',
                'onsubmit' => 'return true',
            ),
        )); ?>
        <div class="col-md-12 indent">
            <?php
            $prices = PriceForVip::model()->findAll();
            $ifChecked = false;
            foreach($prices as $price){
                echo "<input value=\"$price->id\" type=\"radio\" name=\"vip-type\" id=\"vip_type_$price->id\"";
                if(!$ifChecked){
                    echo 'checked>';
                    $ifChecked = true;
                }
                else{
                    echo '>';
                }
                echo "<label for=\"vip_type_$price->id\">$price->name</label>";
            }
            ?>
        </div>
        <div class="col-md-12">
            <?= CHtml::submitButton('Купить', array('class' => 'button1 button1reg')); ?>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>

<div id="postAdvert" class="col-md-3 col-sm-3">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="profile-pictureBlock" style="background-image:url('<?= $nurse->getImage($nurse->pic); ?>')">
                <!--<span class="profilePic-bgLayer"></span>-->
            </div>
        </div>
    </div>
    <div class="row text-left">
        <div class="col-md-12 col-sm-12 profile-stars">
            <?
            for ($i = 0; $i < $nurse->rate; $i++) {
                echo Yii::app()->params['goldStarBig'];
            }
            for ($i = 0; $i < 5 - $nurse->rate; $i++) {
                echo Yii::app()->params['grayStarBig'];
            } ?>
        </div>
    </div>
    <? if((!Yii::app()->user->isGuest)) { ?>
        <?php
            $nurseUser = User::model()->findByPk($nurse->idUser);
            if(User::model()->findByPk(Yii::app()->user->id)->id != $nurse->idUser){
            if($nurseUser->money - $nurseUser->tax >= 0) {
        ?>
    <div class="row">
        <div class="col-md-12 text-left">
            <a href="/visit?step=9&docId=<?= $nurse->idUser; ?>">
                <div class="button1 middle" style="background-color:#EE7600;font-size: 14pt;line-height: 1.7;">
                    <i class="fa fa-plus"></i>Записаться
                </div>
            </a>
        </div>
    </div>
    <div class="row indent">
        <div class="col-md-12 text-left">
            <a href="/site/inProcess">
                <div class="button1 middle" style="font-size:12pt;margin-top:10px;line-height:2;background-color:#82BC1B;">
                    Онлайн-консультация
                </div>
            </a>
        </div>
    </div>
    <? } } } ?>
    <? if((Yii::app()->user->isGuest)) { ?>
        <div class="row text-left">
            <div class="col-md-12">
                <a href="/site/registration">
                    <div class="button1 middle bg-redColor" style="font-size: 14pt;line-height: 1.7;">
                        <i class="fa fa-plus"></i>Записаться
                    </div>
                </a>
            </div>
        </div>
    <?php } ?>
    <div class="row indent">
        <div class="col-md-12">
            <div class="userInformationHead2">Информация о сиделке:</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 userInformationHead1 userInfoMargin">
            <?= $nurse->getAttributeLabel('specialization'); ?>:
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 userInformationText userInfoMargin">
            <?= $nurse->specialization; ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 userInformationHead1 userInfoMargin">
            <?= $nurse->getAttributeLabel('education'); ?>:
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 userInformationText userInfoMargin">
            <?= $nurse->education; ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 userInformationHead1 userInfoMargin">
            <?= $nurse->getAttributeLabel('expirience'); ?>:
        </div>
    </div>
    <div class="userInformationText userInfoMargin indent">
        <?= $nurse->expirience; ?>
    </div>
    <?php
    $idUser = $nurse->idUser;
    $user = User::model()->findByPk($idUser);

    if(isset(Yii::app()->user->id)){
        if(Yii::app()->user->id == Yii::app()->getRequest()->getParam('id')) {

            if ($user->vipTimeEnd < date('Y-m-d H:i')) {
                ?>
                <input id="button-make-vip" type="button" class="button1 button1reg" value="Сделать вип-аккаунт">
            <?php
            } else {
                ?>
                <br>
                Куплен VIP-аккаунт.<br>Срок окончания действия <?= $user->vipTimeEnd; ?>
            <?php
            }
            ?>
            <br><br>
            <div>Ваш баланс: <?= $user->money; ?> рублей</div>
            <input type="button" class="button1 button1reg" value="Пополнить баланс" onclick="goToPay()">
            <br>
            <br>
            <div>Реферальная ссылка:</div>
            <?php echo "<div><input value=\"" . Yii::app()->getBaseUrl(true) . '/site/registration?idRef=' . Yii::app()->request->getQuery('id') . "\" style=\"width:100%;\" readonly></div>" ?>
        <?php
        } }
    if(Yii::app()->request->getQuery('id') == Yii::app()->user->id){
        $referals = Referral::model()->findAllByAttributes(array('idReferal'=>Yii::app()->user->id));
        if($referals){ ?>
            <br>
            Сетка реферальных отчислений:
            <table class="timetable">
                <tr>
                    <th>Имя пользователя</th>
                    <th>Отчисления</th>
                </tr>
                <?php
                foreach($referals as $referal){
                    echo '<tr><th class="weekend">' . User::model()->findByPk($referal->idReferal)->name . '</th><th class="weekend">'.$referal->money.'</th></tr>';
                }
                ?>
            </table>
        <?php } } ?>
</div>

<div id="moreInfoAboutUser" class="col-md-7 col-sm-7">
    <div class="row">
        <div class="userInformationHead2 col-md-11">
            <?= $user->name; ?>
        </div>
        <?php if($curProfileId == Yii::app()->user->id){ ?>
            <div class="col-md-1">
                <a href="<?php echo Yii::app()->request->baseUrl . '/edit/' . User::model()->findByPk(Yii::app()->user->id)->type . '/' . Yii::app()->user->id; ?>"><i class="fa fa-pencil-square-o"></i></a>
            </div>
        <?php } ?>
    </div>

    <?php
    $social = Social::model()->findByPk(Yii::app()->getRequest()->getParam('id'));
    $flagSocial = false;
    if(isset($social)) {
        foreach ($social as $key => $value) {
            if ($key != 'idUser')
                if (isset($value) && ($value != '')) {
                    $flagSocial = true;
                    break;
                }
        }
    }

    if($flagSocial == true){
        ?>
        <div class="row">
            <div class="col-md-12">
                <div id="profile-social-networks">
                Я в соц. сетях
                <?php
                foreach($social as $key => $value){
                    if(($key != 'idUser')&&($value != ''))
                        echo '<a href="' . Yii::app()->params['socialNetworks'][$key] . $value . '" target="_blank"><span class="profile-social-icons" style="background: url(/assets/images/icon_'.$key.'.png)"></span></a>';
                }
                ?>
                </div>
            </div>
        </div>
    <?php } ?>

    <div class="row">
        <div class="userInformationText2 col-md-12">
            <?= $nurse->description; ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="userInformationHead1 text-black" style="margin-top:10px;">Цена за час:
                <span class="userInfoPrice">
                    <span>
                        <?= ($nurse->priceHour == 0 ) ? 'Не указана' : $nurse->priceHour . ' р'; ?>
                    </span>
                </span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="userInformationHead1 text-black">Цена за день:
                <span class="userInfoPrice">
                    <span>
                        <?= ($nurse->priceDay == 0 ) ? 'Не указана' : $nurse->priceDay . ' р'; ?>
                    </span>
                </span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="userInformationHead1 text-black" style="margin-bottom:10px;">Цена за месяц:
                <span class="userInfoPrice">
                    <span>
                        <?= ($nurse->priceMonth == 0 ) ? 'Не указана' : $nurse->priceMonth . ' р'; ?>
                    </span>
                </span>
            </div>
        </div>
    </div>

    <?php
    $criteria = new CDbCriteria;
    $criteria->order = 'time DESC';
    $review = Review::model()->findByAttributes(array('idUserTo' => $nurse->idUser, 'moderate'=>1), $criteria);
    $reviewCount = Review::model()->count('idUserTo = :idUser AND moderate=1', array('idUser' => $nurse->idUser)) - 1;
    ?>
    <?php
    if ($review) {
        ?>
        <?php if($reviewCount > 0) { ?>
            <div class="row indent">
                <div class="col-md-12">
                    <div class="button1" onclick="showReviews(<?= $nurse->idUser; ?>)" style="width:150px;background-color:#207CAE;">ОТЗЫВЫ</div>
                </div>
            </div>
        <?php
        }
        $reviewDate = $review->time;
        $reviewSeconds = strtotime($reviewDate);
        $reviewYear = date('Y', $reviewSeconds);
        $reviewMonth = date('M', $reviewSeconds);
        $reviewDay = date('d', $reviewSeconds);
        switch ($reviewMonth) {
            case 'Jan':
                $reviewMonth = 'января';
                break;
            case 'Feb':
                $reviewMonth = 'февраля';
                break;
            case 'Mar':
                $reviewMonth = 'марта';
                break;
            case 'Apr':
                $reviewMonth = 'апреля';
                break;
            case 'May':
                $reviewMonth = 'мая';
                break;
            case 'Jun':
                $reviewMonth = 'июня';
                break;
            case 'Jul':
                $reviewMonth = 'июля';
                break;
            case 'Aug':
                $reviewMonth = 'августа';
                break;
            case 'Sep':
                $reviewMonth = 'сентября';
                break;
            case 'Oct':
                $reviewMonth = 'октября';
                break;
            case 'Nov':
                $reviewMonth = 'ноября';
                break;
            case 'Dec':
                $reviewMonth = 'декабря';
                break;
        }
        $totalMark = round(($review->cure + $review->regard + $review->qualification) / 3);
        switch ($totalMark) {
            case 1:
                $totalMark = 'Неуд';
                break;
            case 2:
                $totalMark = 'Удовл';
                break;
            case 3:
                $totalMark = 'Средне';
                break;
            case 4:
                $totalMark = 'Хорошо';
                break;
            case 5:
                $totalMark = 'Отлично';
                break;
        }
        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="review">
                    <div class="row">
                        <div class="col-md-3 col-xs-3">
                            <div
                                style="color: #FF6A00;font-family: MyriadCondBold;font-size: 20px;"><?= $totalMark; ?></div>
                        </div>

                        <div class="col-md-3 col-xs-3">
                            <div class="userInformationText2 reviewText">
                                <?= $review->getAttributeLabel('cure') ?>
                                <div class="reviewStars">
                                    <? for ($i = 0; $i < 5; $i++) { ?>
                                        <? if($i > $review->cure - 1) { ?>
                                            <img src=<?= Yii::app()->request->baseUrl . '/assets/images/star2.png' ?>>
                                        <? } else { ?>
                                            <img src=<?= Yii::app()->request->baseUrl . '/assets/images/star1.png' ?>>
                                        <?php }
                                    } ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-xs-3">
                            <div class="userInformationText2 reviewText">
                                <?= $review->getAttributeLabel('regard') ?>
                                <div class="reviewStars">
                                    <? for ($i = 0; $i < 5; $i++) { ?>
                                        <? if($i > $review->regard - 1) { ?>
                                            <img src=<?= Yii::app()->request->baseUrl . '/assets/images/star2.png' ?>>
                                        <? } else { ?>
                                            <img src=<?= Yii::app()->request->baseUrl . '/assets/images/star1.png' ?>>
                                        <?php }
                                    } ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-xs-3">
                            <div class="userInformationText2 reviewText">
                                <?= $review->getAttributeLabel('qualification') ?>
                                <div class="reviewStars">
                                    <? for ($i = 0; $i < 5; $i++) { ?>
                                        <? if($i > $review->qualification - 1) { ?>
                                            <img src=<?= Yii::app()->request->baseUrl . '/assets/images/star2.png' ?>>
                                        <? } else { ?>
                                            <img src=<?= Yii::app()->request->baseUrl . '/assets/images/star1.png' ?>>
                                        <?php }
                                    } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3"><?= $review->name; ?>
                            <br><?= $reviewDay . ' ' . $reviewMonth . ' ' . $reviewYear; ?></div>
                        <div class="col-md-9"><?= $review->text; ?></div>
                    </div>
                </div>
                <?php
                if ($reviewCount > 0) { ?>
                    <a class="showOnMap moreReviews" onclick="showReviews(<?= $nurse->idUser; ?>)">Показать
                        ещё <?= $reviewCount; ?> отзывов</a>
                <?php } ?>
            </div>
        </div>
    <?php } ?>

    <?php if (Yii::app()->user->id) { ?>
        <div class="row">
            <div class="col-md-12">
                <div id="leaveReview" class="review">
                    <?php $formReviewNurse = $this->beginWidget("CActiveForm", array(
                        'id' => 'nurse-review',
                        'action' => Yii::app()->request->baseUrl . '/site/nurse/' . $nurseId,
                        'method' => 'post',
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => true,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                            'validateOnChange' => true,
                        ),
                        'htmlOptions' => array(
                            'enctype' => 'multipart/form-data',
                            'autocomplete' => 'off'
                        ),
                    )); ?>
                    <div class="row">
                        <div class="text-18 blue col-md-6">Оставить отзыв о сиделке</div>
                        <div class="col-md-6">
                            <?= CHtml::submitButton('Отправить', array('class' => 'button white bg-orange col-md-6')); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <?= $formReviewNurse->textField($reviewModel, 'name', array('class' => 'fieldInput filedInputReview', 'placeholder' => 'Ваше имя')); ?>
                            <?= $formReviewNurse->error($reviewModel, 'name', array('class' => 'text-red')) ?>

                            <div class="leaveReviewStars cureValue form-group">
                                <img>
                                <img>
                                <img>
                                <img>
                                <img>
                                <span>Эффективнось лечения</span>
                            </div>
                            <div class="leaveReviewStars regardValue form-group">
                                <img>
                                <img>
                                <img>
                                <img>
                                <img>
                                <span>Отношение</span>
                            </div>
                            <div class="leaveReviewStars qualificationValue form-group">
                                <img>
                                <img>
                                <img>
                                <img>
                                <img>
                                <span>Квалификация</span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <?= $formReviewNurse->textArea($reviewModel, 'text', array('class' => 'fieldInput leaveReviewText', 'placeholder' => 'Введите текст')); ?>
                            <?= $formReviewNurse->error($reviewModel, 'text', array('class' => 'text-red')) ?>
                        </div>
                    </div>
                    <?= $formReviewNurse->hiddenField($reviewModel, 'cure'); ?>
                    <?= $formReviewNurse->error($reviewModel, 'cure', array('class' => 'text-red')) ?>
                    <?= $formReviewNurse->hiddenField($reviewModel, 'regard'); ?>
                    <?= $formReviewNurse->error($reviewModel, 'regard', array('class' => 'text-red')) ?>
                    <?= $formReviewNurse->hiddenField($reviewModel, 'qualification'); ?>
                    <?= $formReviewNurse->error($reviewModel, 'qualification', array('class' => 'text-red')) ?>
                    <?= $formReviewNurse->hiddenField($reviewModel, 'pics', array('value' => '1')); ?>
                    <?= $formReviewNurse->hiddenField($reviewModel, 'idUserTo', array('value' => $nurse->idUser)); ?>
                    <?php $this->endWidget(); ?>
                    <a href="/tickets/new" class="showOnMap moreReviews"
                       style="font-size:16px;float:none;position:relative;display:block;margin-left:0px;">Пожаловаться
                        Администратору</a>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
<div id="overlayReviews" class="overlay">
    <div id="modalReviews"></div>
</div>

<? if (isset($com) && $com == 1) { ?>
    <script>
        $(document).ready(function () {
            $('#myModalLabel').html('Отзыв отправлен!');
            $('#modal-body').html('Ваш отзыв успешно отправлен.');
            $('#myModal').modal();
        })
    </script>
<? } elseif (isset($com) && $com == 0) { ?>
    <script>
        $(document).ready(function () {
            $('#myModalLabel').html('Что - то пошло не так..');
            $('#modal-body').html('Попробуйте оставить отзыв позднее.');
            $('#myModal').modal();
        })
    </script>
<? } ?>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body" id="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function goToPay(){
        var fixPath = (location.href.indexOf('localhost') != -1)?'/medportal':'';
        location.href = location.origin + fixPath + '/pay/payment';
    }

    $('.cureValue img').mouseover(function () {
        var index = $(this).index();
        var COUNT_OF_STARS = 5;
        for (var i = 0; i <= COUNT_OF_STARS - 1; i++) {
            $('.cureValue img').eq(i).removeClass('activeStar');
        }
        for (var i = 0; i <= index; i++) {
            $('.cureValue img').eq(i).addClass('activeStar');
        }
        $('#Review_cure').val(index + 1);
    });
    $('.regardValue img').mouseover(function () {
        var index = $(this).index();
        var COUNT_OF_STARS = 5;
        for (var i = 0; i <= COUNT_OF_STARS - 1; i++) {
            $('.regardValue img').eq(i).removeClass('activeStar');
        }
        for (var i = 0; i <= index; i++) {
            $('.regardValue img').eq(i).addClass('activeStar');
        }
        $('#Review_regard').val(index + 1);
    });
    $('.qualificationValue img').mouseover(function () {
        var index = $(this).index();
        var COUNT_OF_STARS = 5;
        for (var i = 0; i <= COUNT_OF_STARS - 1; i++) {
            $('.qualificationValue img').eq(i).removeClass('activeStar');
        }
        for (var i = 0; i <= index; i++) {
            $('.qualificationValue img').eq(i).addClass('activeStar');
        }
        $('#Review_qualification').val(index + 1);
    });
    $('#Review_cure').val('');
    $('#Review_regard').val('');
    $('#Review_qualification').val('');
    $('#Review_name').val('');
    $('#Review_text').val('');

    function showReviews(idUser) {
        var fixPath = '';
        if (location.host.indexOf('localhost') != -1)
            fixPath = '/med'
        $('#preload').css('display', 'block');
        $.ajax({
            url: 'http://' + location.host + fixPath + '/site/reviews',
            method: 'POST',
            data: 'idUser=' + idUser,
            success: function (data) {
                var reviews = JSON.parse(data);
                console.log(reviews);
                var htmlReviews = '';
                for (var i = 0; i < reviews.length; i++) {
                    var mark = Math.round((parseInt(reviews[i].cure) + parseInt(reviews[i].regard) + parseInt(reviews[i].qualification)) / 3);
                    console.log(mark);
                    switch (mark) {
                        case 1 :
                            mark = "Неуд";
                            break;
                        case 2 :
                            mark = "Удовл";
                            break;
                        case 3 :
                            mark = "Средне";
                            break;
                        case 4 :
                            mark = "Хорошо";
                            break;
                        case 5 :
                            mark = "Отлично";
                            break;
                    }
                    var time = reviews[i].time;
                    var month = new Date(time * 1000).getMonth() + 1;
                    switch (month) {
                        case 1 :
                            month = "января";
                            break;
                        case 2 :
                            month = "февраля";
                            break;
                        case 3 :
                            month = "марта";
                            break;
                        case 4 :
                            month = "апреля";
                            break;
                        case 5 :
                            month = "мая";
                            break;
                        case 6 :
                            month = "июня";
                            break;
                        case 7 :
                            month = "июля";
                            break;
                        case 8 :
                            month = "августа";
                            break;
                        case 9 :
                            month = "сентября";
                            break;
                        case 10 :
                            month = "октября";
                            break;
                        case 11 :
                            month = "ноября";
                            break;
                        case 12 :
                            month = "декабря";
                            break;
                    }
                    htmlReviews += '<div class="review row indent"><div class="row"><div class="col-md-3 col-xs-3"><div style="color: #FF6A00;font-family: MyriadCondBold;font-size: 20px;">';
                    htmlReviews += mark;
                    htmlReviews += '</div></div>';
                    htmlReviews += '<div class="col-md-3 col-xs-3"><div class="userInformationText2 reviewText">Лечение<div class="reviewStars">';
                    for (var j = 0; j < reviews[i].cure; j++) {
                        htmlReviews += '<img src="' + fixPath + '/assets/images/star1.png">';
                    }
                    for (var j = 0; j < 5 - reviews[i].cure; j++) {
                        htmlReviews += '<img src="' + fixPath + '/assets/images/star2.png">';
                    }
                    htmlReviews += '</div></div></div>';
                    htmlReviews += '<div class="col-md-3 col-xs-3"><div class="userInformationText2 reviewText">Отношение<div class="reviewStars">';
                    for (var j = 0; j < reviews[i].regard; j++) {
                        htmlReviews += '<img src="' + fixPath + '/assets/images/star1.png">';
                    }
                    for (var j = 0; j < 5 - reviews[i].regard; j++) {
                        htmlReviews += '<img src="' + fixPath + '/assets/images/star2.png">';
                    }
                    htmlReviews += '</div></div></div>';
                    htmlReviews += '<div class="col-md-3 col-xs-3"><div class="userInformationText2 reviewText">Квалификация<div class="reviewStars">';
                    for (var j = 0; j < reviews[i].qualification; j++) {
                        htmlReviews += '<img src="' + fixPath + '/assets/images/star1.png">';
                    }
                    for (var j = 0; j < 5 - reviews[i].qualification; j++) {
                        htmlReviews += '<img src="' + fixPath + '/assets/images/star2.png">';
                    }
                    htmlReviews += '</div></div></div></div>';
                    htmlReviews += '<div class="row"><div class="col-md-3">' + reviews[i].name + '<br>' + new Date(time * 1000).getDate() + ' ' + month + ' ' + new Date(time * 1000).getFullYear() + '</div><div class="col-md-9">' + reviews[i].text + '</div>';
                    htmlReviews += '</div><div></div></div>';
                }
                htmlReviews += '<div class="row" style="text-align:center;"><div class="button1 lr-paddings bg-orange" onclick="$(\'body\').css(\'overflow\',\'auto\');$(\'#overlayReviews\').css({\'display\':\'none\',\'overflow\':\'hidden\'})">Закрыть</div></div>';
                $('#modalReviews').html(htmlReviews);
            },
            complete: function () {
                $('#preload').css('display', 'none');
                $('#overlayReviews').css({'display': 'block', 'overflow': 'auto'});
                $('body').css({'overflow': 'hidden'});
            }
        });
    }

    $('#button-make-vip').click(function(){
        $('#overlay-make-vip').css('display','block');
    });
    $('.close-button').click(function(){
        $('#overlay-make-vip').css('display','none');
    });
</script>