<?php
/* @var $this ClinicController */
/* @var $model Clinic */
/* @var $form CActiveForm */
?>

<div class="form adminEditClinic">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'clinic-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'errorMessageCssClass' => 'control-label',
        'clientOptions' => array(
            'validateOnChange' => true,
            'validateOnSubmit' => true,
            'errorCssClass' => 'has-error',
            'successCssClass' => 'has-success'
        ),
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>
    <?php echo $form->errorSummary($model, 'Пожалуйста, исправьте следующие ошибки:', '', array('class' => 'callout callout-danger')); ?>

    <div class="col-md-6 createGood">
        <div class="box box-primary">
            <div class="box-header with-border">
            </div>

            <div class="box-body">
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'name'); ?><br>
                    <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 255)); ?>
                    <?php echo $form->error($model, 'name'); ?>
                </div>

                <div class="form-group">
                    <?php echo $form->labelEx($model, 'specialization'); ?><br>
                    <?php echo $form->textArea($model, 'specialization', array('rows' => 2, 'cols' => 60)); ?>
                    <?php echo $form->error($model, 'specialization'); ?>
                </div>

                <div class="form-group">
                    <?php echo $form->labelEx($model, 'contact'); ?><br>
                    <?php echo $form->textField($model, 'contact', array('size' => 60, 'maxlength' => 255)); ?>
                    <?php echo $form->error($model, 'contact'); ?>
                </div>

                <div class="form-group">
                    <?php echo $form->labelEx($model, 'site'); ?><br>
                    <?php echo $form->textField($model, 'site', array('size' => 60, 'maxlength' => 255)); ?>
                    <?php echo $form->error($model, 'site'); ?>
                </div>

                <div class="form-group">
                    <?php echo $form->labelEx($model, 'license'); ?><br>
                    <?php echo $form->textField($model, 'license', array('size' => 60, 'maxlength' => 255)); ?>
                    <?php echo $form->error($model, 'license'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 createGood">
        <div class="box box-primary">
            <div class="box-header with-border">
            </div>

            <div class="box-body">
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'description'); ?><br>
                    <?php echo $form->textArea($model, 'description', array('rows' => 2, 'cols' => 60)); ?>
                    <?php echo $form->error($model, 'description'); ?>
                </div>

                <div class="form-group">
                    <?php echo $form->labelEx($model, 'merit'); ?><br>
                    <?php echo $form->textArea($model, 'merit', array('rows' => 2, 'cols' => 60)); ?>
                    <?php echo $form->error($model, 'merit'); ?>
                </div>

                <div class="form-group">
                    <?php echo $form->labelEx($model, 'contactAdvanced'); ?><br>
                    <?php echo $form->textArea($model, 'contactAdvanced', array('rows' => 2, 'cols' => 60)); ?>
                    <?php echo $form->error($model, 'contactAdvanced'); ?>
                </div>

                <div class="form-group">
                    <?php echo $form->labelEx($model, 'rate'); ?><br>
                    <?php echo $form->dropDownList($model, 'rate', array(0 => '0',1 => '1', 2 => '2', 3 => '3',4 => '4',5 => '5'), array('class' => 'form-control'));  ?>
                    <?php echo $form->error($model, 'rate'); ?>
                </div>

                <!-- <div class="form-group">
                    <?php /*echo $form->labelEx($model, 'pic'); */ ?><br>
                    <?php /*echo $form->textArea($model, 'pic', array('rows' => 2, 'cols' => 50)); */ ?>
                    <?php /*echo $form->error($model, 'pic'); */ ?>
                </div>-->
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'image'); ?>
                    <?php echo $form->fileField($model, 'image', array('class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'image'); ?>
                </div>

                <div class="form-group">
                    <?php echo $form->labelEx($model, 'virtual'); ?><br>
                    <?php echo $form->textField($model, 'virtual', array('size' => 60, 'maxlength' => 255)); ?>
                    <?php echo $form->error($model, 'virtual'); ?>
                </div>

                <div class="form-group">
                    <?php echo $form->labelEx($model, 'panorama'); ?><br>
                    <?php echo $form->textField($model, 'panorama', array('size' => 60, 'maxlength' => 255)); ?>
                    <?php echo $form->error($model, 'panorama'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row buttons text-center">

        <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить всё', array('class' => 'btn btn-primary save')); ?>
    </div>

    <?php $this->endWidget(); ?>


</div>
<!-- form -->
<script src="//cdn.ckeditor.com/4.5.4/standard/ckeditor.js"></script>
<script type="text/javascript">
    //CKEDITOR INITIALIZATION
    //NURSE
    CKEDITOR.replace('Clinic[specialization]');
    //call when form submit
    var crutchForDoctorCKEditor = function(){
        var iframesClinic = $('.adminEditDoctor iframe');
        for(var i=0;i<iframesClinic.length;i++) {
            var text = iframesClinic[i].contentDocument.childNodes[1].childNodes[1].childNodes[0].innerHTML;
            if (text != '<br>') {
                $(iframesClinic[i].parentNode.parentNode.parentNode.parentNode).find('textarea').val(text);
            } else
                $(iframesClinic[i].parentNode.parentNode.parentNode.parentNode).find('textarea').val('');
        }
    }
</script>