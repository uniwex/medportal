<?php

?>
<div class="box box-info text-left">
    <h1><?php echo $model->name; ?></h1>

    <?php $this->widget('zii.widgets.CDetailView', array(
        'data' => $model,

        'attributes' => array(
            array(
                'type'=>'raw',
                'value'=> CHtml::image(Yii::app()->request->baseUrl.'/upload/'.$model->pic,'', array('style'=>'width: 30%;')),
            ),
            'idUser',
            'name',
            array(
                'label' => 'Опыт работы',
                'type'=>'raw',
                'value'=> strip_tags($model->specialization),
            ),
            'description',
            'contact',
            'site',
            'license',
            'merit',
            'contactAdvanced',
            'rate',
        ),

    )); ?>
</div>

<div class="row">
    <div class="col-md-12">
        <?
        $doctor = Schedule::model()->findAll('idUser = :id and type = :type', array(':id' => $model->idUser, ':type' => 'clinicDoc'));
        if (isset($doctor)) {
            ?>

            <?
            for ($i = 0;
                 $i < count($doctor);
                 $i++) {
                ?>


                <div class="box box-info">
                    <div class="box-header with-border ui-sortable-handle">
                        <h3 class="box-title"><?= $doctor[$i]['name'] ?></h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-xs-3 text-center name_stolb">ФИО</div>
                        <div class="col-xs-3 text-center name_stolb">Специальность</div>
                        <div class="col-xs-3 text-center name_stolb">Цена</div>
                        <div class="col-xs-3 text-center name_stolb">Длительность</div>
                        <div class="col-xs-3 text-center"><?= $doctor[$i]['name'] ?></div>
                        <div class="col-xs-3 text-center"><?
                            $specialisation = Specialization::model()->find('id = :id', array(':id' => $doctor[$i]['specialization']));
                            if (isset($specialisation)) {
                                echo $specialisation->name;
                            }
                            ?></div>
                        <div class="col-xs-3 text-center"><?= $doctor[$i]['price'] ?> рублей</div>
                        <div class="col-xs-3 text-center"><?= $doctor[$i]['timeSize'] ?> минут</div>
                        <div class="class col-md-12 note"> Расписание приемов</div>
                        <table class="table table-bordered">
                            <tr>
                                <th>Понедельник</th>
                                <th>Втрник</th>
                                <th>Среда</th>
                                <th>Четверг</th>
                                <th>Пятница</th>
                                <th>Суббота</th>
                                <th>Воскресенье</th>
                            </tr>
                            <tr>
                                <td class="text-center"><?= $doctor[$i]['monday'] ?></td>
                                <td class="text-center"><?= $doctor[$i]['tuesday'] ?></td>
                                <td class="text-center"><?= $doctor[$i]['wednesday'] ?></td>
                                <td class="text-center"><?= $doctor[$i]['thursday'] ?></td>
                                <td class="text-center"><?= $doctor[$i]['friday'] ?></td>
                                <td class="text-center"><?= $doctor[$i]['saturday'] ?></td>
                                <td class="text-center"><?= $doctor[$i]['sunday'] ?></td>
                            </tr>
                        </table>
                    </div>
                </div>

                <?
            }
            ?>

            <?
        } else {
            echo 'Врачей нет';
        }
        ?>

    </div>
</div>