<?php
/* @var $this ClinicController */
/* @var $data Clinic */
?>

<div class="view">

    <b><?php echo CHtml::encode($data->getAttributeLabel('idUser')); ?>:</b>
    <?php echo CHtml::link(CHtml::encode($data->idUser), array('view', 'id' => $data->idUser)); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
    <?php echo CHtml::encode($data->name); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('specialization')); ?>:</b>
    <?php echo CHtml::encode($data->specialization); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('contact')); ?>:</b>
    <?php echo CHtml::encode($data->contact); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('site')); ?>:</b>
    <?php echo CHtml::encode($data->site); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('license')); ?>:</b>
    <?php echo CHtml::encode($data->license); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
    <?php echo CHtml::encode($data->description); ?>
    <br/>

    <?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('merit')); ?>:</b>
	<?php echo CHtml::encode($data->merit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contactAdvanced')); ?>:</b>
	<?php echo CHtml::encode($data->contactAdvanced); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rate')); ?>:</b>
	<?php echo CHtml::encode($data->rate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pic')); ?>:</b>
	<?php echo CHtml::encode($data->pic); ?>
	<br />

	*/ ?>

</div>