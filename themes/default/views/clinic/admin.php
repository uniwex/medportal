<?php


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$('#clinic-grid').yiiGridView('update', {
data: $(this).serialize()
});
return false;
});
");
?>

<div class="row">
    <div class="col-md-12">

        <div class="box box-info">
            <div class="box-header with-border ui-sortable-handle">
                <h3 class="box-title">Список клиник</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <?php $this->widget('zii.widgets.grid.CGridView', array(
                    'id' => 'clinic-grid',
                    'dataProvider' => $model->search(),
                    'filter' => $model,
                    'cssFile' => Yii::app()->request->getBaseUrl(true) . '/assets/admin/css/gridview.css',
                    'itemsCssClass' => 'table no-margin',
                    'htmlOptions' => array(
                        'class' => 'table-responsive'
                    ),
                    'summaryText' => 'Показано записей: {start} - {end} из {count}',
                    'emptyText' => 'Результаты не найдены',
                    'columns' => array(
                        'idUser',
                        array(
                            'name' => 'pic',
                            'type' => 'image',
                            'value' => '"/upload/".$data->pic',
                            'htmlOptions' => array('class' => 'imgGood'),
                        ),
                        'name',
                        'specialization',
                        'contact',
                        'site',
                        'license',
                        /*
                        'description',
                        'merit',
                        'contactAdvanced',
                        'rate',
                        'pic',
                        */
                        array(
                            'class' => 'CButtonColumn',
                        ),
                    ),

                    'summaryText' => 'Показано {count} записей из {end}',
                )); ?>
            </div>
        </div>
    </div>
</div>