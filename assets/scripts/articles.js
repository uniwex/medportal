//function that remove arcticle
//type - 0(plain articles), 1(digest)
function removeArticle(el, type){
    if(!type)
        type = 0;
    articleId = $(el).data('id');
    var url = '/articles/remove';
    if(type == 1)
        url = '/digest/remove';
    if(confirm('Удалить статью?')){
        $.ajax({
            url: url,
            method: 'POST',
            data: 'id=' + articleId,
            success: function () {
                if(type == 0)
                    document.location = '/articles';
                if(type == 1)
                    document.location = '/digest';
            },
            error: function(data){
                console.log(data);
            }
        });
    }
}