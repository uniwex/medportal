$(function () {
    var id = $('[id*=pic]').val();

    getTimeWork();
    $('#image').css({'background-image': 'url(../../upload/' + id + ')', 'background-size': 'cover'});

    // Смена телефона
    $('[name="changePhone"]').on('click', function(){
        var code = $('#code'),
            phone = $('#User_phone'),
            error = $('#User_phone_em_'),
            div = code.parent();
        error.html('').css('display', 'none');
        if(div.hasClass('hidden')) {
            $.ajax({
                url: '/edit/changePhone',
                data: {
                    phone: phone.val()
                },
                success: function (data) {
                    data = JSON.parse(data);
                    div.removeClass('hidden');
                }
            })
        }
        else {
            $.ajax({
                url: '/edit/changePhone',
                data: {
                    code: code.val()
                },
                success: function (data) {
                    data = JSON.parse(data);
                    if(data.status == 'fail') {
                        error.html(data.msg).css('display', 'block');
                    }
                    code.val('');
                    div.addClass('hidden');
                }
            })
        }
    })
});
var currentId, currentType;

// График работы
function getTimeWork() {
    var timeWork,
        timeRange,
        num = 0,
        k = 0,
        count = $('[id^=Schedule_timework]').length;

    for (k; k < count; k++) {
        var tr = $('.timetable:eq(' + k + ') tr'),
            tbody = $('.timetable:eq(' + k + ') tbody'),
            dataId = tr.parent().parent()[0].dataset.id,
            dataType = tr.parent().parent()[0].dataset.type;

        if ($('input[data-id=' + dataId + '][data-type=' + dataType + ']').val()) {
            timeWork = JSON.parse($('input[data-id=' + dataId + '][data-type=' + dataType + ']').val());
            num = 0;
            for (var i = 0; i < timeWork.length; i++) {
                if (i > 0) tbody.append('<tr></tr>');
                for (var j = 0; j < timeWork[i].length; j++) {
                    if (i == 0) {
                        if (timeWork[i][j][0] == '' && timeWork[i][j][1] == '') timeRange = 'выходной';
                        else timeRange = 'c <input value="' + timeWork[i][j][0] + '"><br>до <input value="' + timeWork[i][j][1] + '">';
                        $($(tr.eq(1)).find('th')[num]).html(timeRange);
                        num++;
                    } else {
                        if (timeWork[i][j][0] == '' && timeWork[i][j][1] == '') timeRange = 'выходной';
                        else timeRange = 'c <input value="' + timeWork[i][j][0] + '"><br>до <input value="' + timeWork[i][j][1] + '">';
                        $('.timetable:eq(' + k + ') tr:last').append('<th>' + timeRange + '</th>');
                    }
                }
            }
        }
    }
}

// Открываем расписание
function openSchedule(el) {
    currentId = $(el)[0].dataset.id;
    currentType = $(el)[0].dataset.type;

    var timeWork = JSON.parse($('input[data-id=' + currentId + '][data-type=' + currentType + ']').val()),
        tr = $('#schedule tr'),
        tbody = $('#schedule tbody'),
        countTr = $('#schedule tbody').find('tr').length,
        num = 0,
        isEmpty = true;

    if (countTr - 1 < timeWork.length) {
        for (var i = 0; i < timeWork.length; i++) {
            if (i > 0) tbody.append('<tr></tr>');
            for (var j = 0; j < timeWork[i].length; j++) {
                if (i == 0) {
                    if (timeWork[i][j][0] != '' && timeWork[i][j][1] != '') {
                        $($(tr.eq(1)).find('input')[num]).val(timeWork[i][j][0]);
                        num++;
                        $($(tr.eq(1)).find('input')[num]).val(timeWork[i][j][1]);
                        num++;
                    } else num += 2;
                } else {
                    $('#schedule tr:last').append('<th>c <input type="text" value="' + timeWork[i][j][0] + '"><br>до <input type="text" value="' + timeWork[i][j][1] + '"></th>');
                }
            }
        }
    } else {
        for(i = 0; i < timeWork.length; i++) {
            for(j = 0; j < timeWork[i].length; j++) {
                if (timeWork[i][j][0] != '' && timeWork[i][j][1] != '') {
                    $($(tr.eq(1)).find('input')[num]).val(timeWork[i][j][0]);
                    num++;
                    $($(tr.eq(1)).find('input')[num]).val(timeWork[i][j][1]);
                    num++;
                    isEmpty = false;
                } else {
                    $($(tr.eq(1)).find('input')[num]).val('');
                    num++;
                    $($(tr.eq(1)).find('input')[num]).val('');
                    num++;
                }
                if(j == timeWork[i].length - 1 && isEmpty) $(tr.eq(1)).find('input').each(function(id) { $(this).val('') })
            }
        }
    }
    while (timeWork.length < countTr - 1) {
        $('#schedule tr:last').remove();
        countTr--;
    }
    $('#overlaySchedule').css('display', 'block');
}

//  Удаление врача
function removeDoctor(el) {
    var result = confirm('Вы действительно хотите удалить этого врача?'),
        count = $('.form-doctor').length;

    if (result) {
        if(count > 1) {
            var form = $(el).parent().parent().parent(),
                hash = form.find('#hash').val();

            if(hash) {
                $.ajax({
                    url: '/edit/delete',
                    type: 'POST',
                    data: {
                        hash: hash
                    },
                    success: function (data) {
                        form.remove();
                    }
                })
            } else form.remove();
        } else alert('Вы не можете удалить единственного врача');
    }
}