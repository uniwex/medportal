$(function () {

    var globalIdCountry,
        allDoctor = $('table[data-id][data-type="clinic"]').length,
        switchMenu = false,
        isLoadCountry = false,
        isLoadCity = false,
        isLoadMetro = false,
        listMetro,
        radioNodes = document.querySelectorAll('#doctor-registration input[type="radio"]'),
        defaultSchedule = '[[["",""],["",""],["",""],["",""],["",""],["",""],["",""]]]',
        fixPath = (location.host == "localhost") ? '/medportal' : '',
        url = 'http://' + location.host + fixPath + '/site/getCalendar';

    setHiddenSchedule(defaultSchedule);

    if (location.href.indexOf('localhost') != -1) fixPath = '/medportal';

    /*$('.specialty').click(function () {
        var menuSpeciality = $('.menuSpeciality')[0];
        var postAdvert = $('#postAdvert');
        var profDisease = $('#profdisease');
        var parapharmaceutic = $('#parapharmaceutic');
        var auth = $('.auth');
        var registration = $('#registration');
        var moreInfoAboutUser = $('#moreInfoAboutUser');

        if(menuSpeciality.style.display == 'block'){
            menuSpeciality.style = 'display:none!important';

            profDisease.removeClass('col-lg-10').removeClass('col-md-10').removeClass('col-xs-10').removeClass('col-sm-10')
                       .addClass('col-lg-12').addClass('col-md-12').addClass('col-xs-12').addClass('col-sm-12')
                       .css({'margin-right':'0px','padding':'0px'});

            parapharmaceutic.removeClass('col-lg-10').removeClass('col-md-10').removeClass('col-xs-10').removeClass('col-sm-10')
                .addClass('col-lg-12').addClass('col-md-12').addClass('col-xs-12').addClass('col-sm-12')
                .css({'margin-right':'0px','padding':'0px'});

            if(postAdvert.hasClass('col-md-10')) {
                postAdvert.removeClass('col-md-10').removeClass('col-xs-10').addClass('col-md-12').addClass('col-xs-12');//.css('padding-left','0px');
                if(postAdvert.hasClass('no-padding')){
                    $('#postAdvert > div').css('padding-left','0px');
                }
                if(!postAdvert.hasClass('no-padding')){
                    postAdvert.css('padding-left','0px');
                }
            }
            if(postAdvert.hasClass('col-md-8')) {
                postAdvert.removeClass('col-lg-8').removeClass('col-md-8').removeClass('col-xs-8').removeClass('col-sm-8').addClass('col-lg-10').addClass('col-md-10').addClass('col-xs-10').addClass('col-sm-10');
                if(!postAdvert.hasClass('no-padding')){
                    postAdvert.css('padding-left','0px');
                }
            }
            if(postAdvert.hasClass('col-md-3')) {
                if(postAdvert.hasClass('no-padding')){
                    $('#postAdvert > div').css('padding-left','0px');
                }
                if(!postAdvert.hasClass('no-padding')){
                    postAdvert.css('padding-left','0px');
                }
            }
            auth.css('margin-left','0px');
            if(registration.hasClass('col-md-8')) {
                registration.removeClass('col-md-8').removeClass('col-xs-8').addClass('col-md-10').addClass('col-xs-10');
                if(registration.hasClass('no-padding')){
                    $('#postAdvert > div').css('padding-left','0px');
                }
                if(!registration.hasClass('no-padding')){
                    registration.css('padding-left','0px');
                }
            }
            if(moreInfoAboutUser.hasClass('col-md-7'))
                moreInfoAboutUser.removeClass('col-md-7').removeClass('col-xs-7').addClass('col-md-9').addClass('col-xs-9');
        } else {
            menuSpeciality.style = 'display:block';

            profDisease.removeClass('col-lg-12').removeClass('col-md-12').removeClass('col-xs-12').removeClass('col-sm-12')
                .addClass('col-lg-10').addClass('col-md-10').addClass('col-xs-10').addClass('col-sm-10')
                .css({'margin-right':'-12px','padding':'0px 20px 0px 10px'});

            parapharmaceutic.removeClass('col-lg-12').removeClass('col-md-12').removeClass('col-xs-12').removeClass('col-sm-12')
                .addClass('col-lg-10').addClass('col-md-10').addClass('col-xs-10').addClass('col-sm-10')
                .css({'margin-right':'-12px','padding':'0px 20px 0px 10px'});

            if(postAdvert.hasClass('col-md-12')) {
                postAdvert.removeClass('col-md-12').removeClass('col-xs-12').addClass('col-md-10').addClass('col-xs-10');//.css('padding-left','15px');
                if(postAdvert.hasClass('no-padding')){
                    $('#postAdvert > div').css('padding-left','15px');
                }
                if(!postAdvert.hasClass('no-padding')){
                    postAdvert.css('padding-left','15px');
                }
            }
            if(postAdvert.hasClass('col-md-10')) {
                postAdvert.removeClass('col-lg-10').removeClass('col-md-10').removeClass('col-xs-10').removeClass('col-sm-10').addClass('col-lg-8').addClass('col-md-8').addClass('col-xs-8').addClass('col-sm-8');
                if(!postAdvert.hasClass('no-padding')){
                    postAdvert.css('padding-left','15px');
                }
            }
            if(postAdvert.hasClass('col-md-3')) {
                if(postAdvert.hasClass('no-padding')){
                    $('#postAdvert > div').css('padding-left','15px');
                }
                if(!postAdvert.hasClass('no-padding')){
                    postAdvert.css('padding-left','15px');
                }
            }
            auth.css('margin-left','15px');
            if(registration.hasClass('col-md-10')) {
                registration.removeClass('col-md-10').removeClass('col-xs-10').addClass('col-md-8').addClass('col-xs-8');
                if(registration.hasClass('no-padding')){
                    $('#postAdvert > div').css('padding-left','15px');
                }
                if(!registration.hasClass('no-padding')){
                    registration.css('padding-left','15px');
                }
            }
            if(moreInfoAboutUser.hasClass('col-md-9'))
                moreInfoAboutUser.removeClass('col-md-9').removeClass('col-xs-9').addClass('col-md-7').addClass('col-xs-7');
        }
    });*/

    selectCountries();
    if(isLoadCountry) selectCities();
    if(isLoadCity) selectMetro();
    $('[data-alias="country"]').on('change', function() {
        $('#User_city').val('');
        $('#Station_name optgroup').empty();
        selectCities(false, $(this).parent().parent().parent().parent().parent().attr('class'));
    });

    //
    //$('#User_city').on('click', function() {
    //    if(isLoadCity) $('.dropDownCities').css('display', 'block');
    //});

    $('a[data-id="map"]').tooltip();
    $('a[data-id="metro"]')
        .tooltip()
        .on('click', function () {
            var city = $('#city').val(),
                warning = $('[data-empty="metro"]');
            if (city) {
                warning.css('display', 'none');
                $.ajax({
                    url: '/site/getmetro',
                    data: {
                        city: city
                    },
                    success: function (data) {
                        if(data != '') {
                            data = JSON.parse(data);
                            var col = 0,
                                formGroup = function(key, el) {
                                    return '<div class="form-group checkbox">' +
                                        '<label for="metro' + key + '">' +
                                        '<input type="checkbox" id="metro' + key + '" value="' + el + '" name="metro[]">' + el +
                                        '</label>' +
                                        '</div>'
                                };
                            $.each($('[data-col]'), function(key, el){
                                $(el).empty();
                            });
                            $.each(data, function (key, el) {
                                $('[data-col=' + col + ']').append(formGroup(key, el));
                                if(col == 2) col = 0;
                                else col++;
                            })
                        }
                        else warning.css('display', 'block');
                    }
                })
            }
            else warning.css('display', 'block');
        });

    $('.inputForSearchCity').on('click blur', function () {
        $('.dropDownCities').css('display', 'block');
        selectCities(true,$(this).parent().parent().parent().attr('class'));
    });

    $('#reg-city').on('click blur', function () {
        $('#reg-city-choose').css('display', 'block');
        selectCities(true,$(this).parent().parent().parent().attr('class'));
    });

    $('#reg-city-choose').click(function () {
        var city = $(this).val(),
            idCity = $('#reg-city-choose :selected').attr('data-cityid');
        $('#reg-city').val(city).attr('data-cityid', idCity);
        $(this).css('display', 'none');
        var $parentNode = $(this).parent().parent().parent();
        if($parentNode.attr('class').indexOf('step') != -1)
            $parentNode = $parentNode.parent();
        if(city) selectMetro($parentNode.attr('class'));
    })

    $('.dropDownCities').click(function () {
        var city = $(this).val(),
            idCity = $('.dropDownCities :selected').attr('data-cityid');
        $('.inputForSearchCity').val(city).attr('data-cityid', idCity);
        $(this).css('display', 'none');
        var $parentNode = $(this).parent().parent().parent();
        if($parentNode.attr('class').indexOf('step') != -1)
            $parentNode = $parentNode.parent();
        if(city) selectMetro($parentNode.attr('class'));
    })
    .blur(function () {
        $(this).css('display', 'none');
    });

    $('input[type="file"]').change(function () {
        if (!$(this).hasClass('no-load-preview')) {
            readURL(this);
        }
    });

    $('#regClinicToStep1').click(function () {
        $('.displayClinicStep1').css('display', 'block');
        $('.displayClinicStep2').css('display', 'none');
    });
    $('#regClinicToStep2').click(function () {
        $('.displayClinicStep1').css('display', 'none');
        $('.displayClinicStep2').css('display', 'block');
    });

    $('.btnAddDoctor').click(function () {
        allDoctor++;

        var pathname = location.pathname,
            fioInput = $($('[for="Schedule_name"]').parent().parent()[0]).clone(),
            specializationSelect = $($('[for="Schedule_specialization"][data-spec="clinic"]').parent().parent()[0]).clone(),
            priceInput = $($('[for="Schedule_price"]').parent().parent()[0]).clone(),
            timeSize = $($('[for="Schedule_timeSize"]').parent().parent()[0]).clone(),
            timeWork =
                '<div class="form-group row">' +
                '<div class="col-md-2">' +
                '<label for="Schedule_График_работы">График работы</label>' +
                '</div>' +
                '<div class="col-md-8">' +
                '<div class="tabTitle">Время приёма</div>' +
                '<table class="timetable" data-id="' + allDoctor + '" data-type="clinic" onclick="openSchedule(this)">' +
                '<tr>' +
                '<th>Пн</th>' +
                '<th>Вт</th>' +
                '<th>Ср</th>' +
                '<th>Чт</th>' +
                '<th>Пт</th>' +
                '<th>Сб</th>' +
                '<th>Вс</th>' +
                '</tr>' +
                '<tr>' +
                '<th class="weekend"><input value="выходной"></th>' +
                '<th class="weekend"><input value="выходной"></th>' +
                '<th class="weekend"><input value="выходной"></th>' +
                '<th class="weekend"><input value="выходной"></th>' +
                '<th class="weekend"><input value="выходной"></th>' +
                '<th class="weekend"><input value="выходной"></th>' +
                '<th class="weekend"><input value="выходной"></th>' +
                '</tr>' +
                '</table>' +
                '<input data-id="' + allDoctor + '" data-type="clinic" value="' + escapeHtml(defaultSchedule) + '" name="Schedule[timework][]" id="Schedule_timework" type="hidden" />' +
                '</div>' +
                '</div>';

        fioInput.find('input').val('');
        priceInput.find('input').val('');
        specializationSelect.find('select').val(1);
        timeSize.find('input').val('');
        $('.form-doctor:last').after('<div class="form-doctor"></div>');
        $('.form-doctor:last').append(fioInput).append(specializationSelect).append(timeSize).append(priceInput).append(timeWork);
    });

    $('#addTime').click(function () {
        var tr =
                '<tr>' +
                '<th>' +
                'с <input autocomplete="off" type="text"><br>до <input autocomplete="off" type="text">' +
                '</th>' +
                '<th>' +
                'с <input autocomplete="off" type="text"><br>до <input autocomplete="off" type="text">' +
                '</th>' +
                '<th>' +
                'с <input autocomplete="off" type="text"><br>до <input autocomplete="off"type="text">' +
                '</th>' +
                '<th>' +
                'с <input autocomplete="off" type="text"><br>до <input autocomplete="off" type="text">' +
                '</th>' +
                '<th>' +
                'с <input autocomplete="off" type="text"><br>до <input autocomplete="off" type="text">' +
                '</th>' +
                '<th>' +
                'с <input autocomplete="off" type="text"><br>до <input autocomplete="off" type="text">' +
                '</th>' +
                '<th>' +
                'с <input autocomplete="off" type="text"><br>до <input autocomplete="off" type="text">' +
                '</th>' +
                '</tr>',
            table = $('#schedule');

        table.find('tbody').append(tr);
        if (table.find('tr').length > 2) $('#removeTime').removeClass('disabled').removeAttr('disabled');
    })

    $('#removeTime')
        .click(function () {
            var table = $('#schedule'),
                len;
            if (table.find('tr').length > 2) {
                table.find('tr').eq(-1).remove();
                len = table.find('tr').length;
                if (len <= 2) $(this).attr('disabled', 'disabled').addClass('disabled');
            }
            else $(this).attr('disabled', 'disabled').addClass('disabled');
        })
        .attr('disabled', 'disabled').addClass('disabled');

    $('#calendar-month-left').click(function () {
        getCalendar(-1);
    });
    $('#calendar-month-right').click(function () {
        getCalendar(1);
    });

    //click on close button in modal
    $('#overlay-calendar .close-button').click(function () {
        $('#overlay-calendar').css('display', 'none');
    });

    //click on cell in calendar
    $('#calendar-content').click(function (e) {
        var target = e.target;
        if (target.className.indexOf('busyDay') != -1) {
            var year = $('#calendar-year').html();
            var month = $('#calendar-month').html();
            var day = $(target).html();
            var fixPath = (location.host == "localhost") ? '/medportal' : '';
            var url = 'http://' + location.host + fixPath + '/site/getDayEvents';
            $.ajax({
                url: url,
                method: 'POST',
                data: 'year=' + year + '&month=' + month + '&day=' + day,
                success: function (responce) {
                    var data = JSON.parse(responce);
                    $('#calendar-on-day').html(data.html);
                    $('#overlay-calendar').css('display', 'block');
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }
    });

    //remove event
    $('#calendar-on-day').click(function (e) {
        var target = e.target;
        if (target.className.indexOf('event-remove') != -1) {
            var eventId = target.getAttribute('data-id');
            var isRemove = confirm("Удалить событие?");
            if (isRemove) {
                var fixPath = (location.host == "localhost") ? '/medportal' : '';
                var url = 'http://' + location.host + fixPath + '/site/removeEvent';
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: 'eventId=' + eventId,
                    success: function (responce) {
                        if (responce == 'ok') {
                            alert('Событие удалено');
                            $('#overlay-calendar').css('display', 'none');
                            getCalendar(0);
                        }
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
            }
        }
    });

    $('#toggle-user-menu').click(function () {
        $('#user-menu').show();
    });

    $('#user-menu').mouseleave(function () {
        $(this).hide();
    });

    /*
    $('.menuSpeciality > ul > li').click(function () {
        var $it = $(this);
        if ($it.children('ul').hasClass('hidden')) {
            $it.addClass('text-red');
            $it.children('ul').removeClass('hidden');
            $it.addClass('text-red');
            $it.find('a').addClass('text-red');
        }
        else {
            $it.children('ul').addClass('hidden');
            $it.removeClass('text-red');
            $it.find('a').removeClass('text-red');
        }
    });
    */

    $('.specialty').click(function(){
        return;
        $('.menuSpeciality > ul').css('height', '608px');
    });

    $('select#typeHuman').on('change', function(){
       $('input[type=hidden][id=type]').val($(this).val());
    });

    //get new month
    function getCalendar(index) {
        var year = parseInt($('#calendar-year').html());
        var month = parseInt($('#calendar-month').html());
        month = month + index;
        if (month > 12) {
            month = 1;
            year += 1;
        }
        if (month < 1) {
            month = 12;
            year -= 1;
        }
        $.ajax({
            url: url,
            method: 'POST',
            data: 'year=' + year + '&month=' + month,
            success: function (responce) {
                var data = JSON.parse(responce);
                $('#calendar-content').html(data.html);
                $('#calendar-year').html(data.year);
                $('#calendar-month').html(data.month);
                $('#calendar-link').attr('href', 'http://' + location.host + fixPath + '/events?year=' + data.year + '&month=' + data.month);
                $('#calendar-month-name').html(data.monthName);
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function selectCountries() {
        isLoadCountry = false;
        $.ajax({
            url: 'http://' + location.host + fixPath + '/site/getCountries',
            async: false,
            success: function (data) {
                var response = JSON.parse(data),
                    option = '<option value="0">Выберите страну</option>',
                    select = $('[data-alias="country"]'),
                    countries;
                if (response) {

                    countries = response.response.items;
                    for (var i = 0; i < countries.length; i++) {
                        option += '<option data-countryId="' + countries[i].id + '" value="' + countries[i].title + '">' + countries[i].title + '</option>';
                    }

                    select.html(option);
                    if(response.country) $('#User_country [value="' + response.country + '"]').attr('selected', true);
                    if(response.station) listMetro = response.station;
                    isLoadCountry = true;
                    $('#User_country').val('Россия');
                }
                else selectCountries();
            },
            error: function (data) {
                console.log('AJAX: Error selectCountries()');
                console.log(data);
            }
        })
    }

    //typeName is index of user type in order of display
    function selectCities(async, typeName) {
        if(!typeName)
            typeName = '';
        else
            if(typeName.indexOf(' ') != -1)
                typeName = '';
        else
            typeName = '.' + typeName;
        /*var idCountry = $('#User_country option:selected').attr('data-countryid'),
            inputCity = $('#User_city');*/
        var idCountry = $(typeName+' [data-alias="country"] option:selected').attr('data-countryid'),
            inputCity = $(typeName+' .inputForSearchCity');

        if(async === undefined) async = false;

        isLoadCity = false;
        if (idCountry) {
            $('#preload').css('display', 'block');
            $.ajax({
                url: 'http://' + location.host + fixPath + '/site/getCityByIdInVk',
                data: { idCountry: idCountry, queryString: inputCity.val() },
                async: async,
                success: function (data) {
                    var response = JSON.parse(data).response,
                        option = '<optgroup label="Выберите город">',
                        select = $(typeName+' [data-alias="city"]'),
                        cities;

                    if (response) {
                        cities = response.items;
                        for (var i = 0; i < cities.length; i++) {
                            if(!cities[i].area) {
                                if (!cities[i].area && cities[i].title == inputCity.val()) inputCity.attr('data-cityId', cities[i].id);
                                option += '<option value="' + cities[i].title + '" data-cityId="' + cities[i].id + '">' + cities[i].title + '</option>';
                            }
                        }
                        option += '</optgroup>';
                        select.html(option);
                        isLoadCity = true;
                    }
                    else alert('Нет ответа');
                },
                error: function (data) {
                    console.log('AJAX: Error selectCities()');
                    console.log(data);
                },
                complete: function () {
                    $('#preload').css('display', 'none');
                }
            });
        }
    }

    //typeName is index of user type in order of display
    function selectMetro(typeName) {
        //var idCity = $('#User_city').attr('data-cityId');
        //debugger;
        var idCity = $('.'+typeName+' .inputForSearchCity').attr('data-cityId');
        isLoadMetro = false;
        if(idCity) {
            $.ajax({
                url: '/site/getStationsByCityId',
                data: { cityId: idCity },
                success: function (data) {
                    data = JSON.parse(data);
                    var selectOptions = '<optgroup label="Выберите станцию метро">';
                    if(data.length > 0) {
                        var metroLines = data,
                            station;
                        for (var i = 0; i < metroLines.length; i++) {
                            //for (var j = 0; j < metroLines[i].stations.length; j++) {
                                station = metroLines[i];
                                selectOptions += '<option value="' + station + '">' + station + '</option>';
                            //}
                        }
                        selectOptions += '</optgroup>';
                        $('.' +typeName+' [data-alias="metro"]').html(selectOptions);
                        if(listMetro) {
                            for(i = 0; i < listMetro.length; i++) {
                                $('.' +typeName+' [value="' + listMetro[i] + '"]').attr('selected', 1)
                            }
                        }
                        isLoadMetro = true;
                    }
                    else {
                        selectOptions += '<option value="0">Нет метро</option></optgroup>';
                        $('.' +typeName+' [data-alias="metro"]').html(selectOptions);
                        isLoadMetro = true;
                    }
                }
            })
        }
    }

    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onloadend = function (e) {
                $(input).parent().css('background-image', 'url(' + e.target.result + ')');
                if ($('#Review_pics'))
                    $('#Review_pics').val('1');
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    //hide li if it lower than button
    /*$('.menuSpeciality ul li').each(function(){ if($(this).offset().top+$(this).height() > $('.menuSpeciality .entireList').offset().top){$(this).hide()} });*/

    var ulHeightDefault = '450';
    var menuFlag = 0;
    /*$('.menuSpeciality ul').attr('data-flag',0);
    var liCountDefault = 26;
    $('.menuSpeciality ul li').each(
        function(){
            if($(this).index() >= liCountDefault)
                $(this).css({'display':'none'});
        }
    );
    $('.entireList.indent').click(function(){
        if($('.menuSpeciality ul').attr('data-flag') == 0){
            $('.menuSpeciality ul').attr('data-flag',1);
            $('.menuSpeciality ul li').each(
                function(){
                    $(this).css({'display':'block'});
                }
            );
        } else {
            if ($('.menuSpeciality ul').attr('data-flag') == 1) {
                $('.menuSpeciality ul').attr('data-flag', 0);
                $('.menuSpeciality ul li').each(
                    function(){
                        if($(this).index() >= liCountDefault)
                            $(this).css({'display':'none'});
                    }
                );
            }
        }
    });*/

    /*$('.specialty').click();
    //if we on main page
    if(
        (location.pathname == '/')
        ||(location.pathname == '/medcatalog/profdisease')
        ||(location.pathname == '/medcatalog/parapharmaceutic')
    )
        $('.specialty').click();*/

    $('.ulBox h3').click(function(){
        if($(this).next().css('display') == 'none') {
            $('.ulBox ul').slideUp();
            $(this).next().slideToggle();
        } else {
            $('.ulBox ul').slideUp();
        }
    });

    $('#clock img').click(function(){
        if($('#clock .item').length == 5){
            $('#modal-citytimes-warning').modal('show');
        } else {
            $('#modal-add-time').modal('show');
        }
    });

    $('#clock button').click(function(){
        $('#clock .item i').toggle();
    });

    $('#clock .item i').click(function(){
        var id = $(this).parent().attr('data-id');
        $('#clock .item[data-id="'+id+'"]').remove();
        $.ajax({
            url: '/site/removeCityTime',
            method: 'POST',
            data: 'idTimeCity=' + id,
            success: function (responce) {
                console.log(responce);
            },
            error: function (data) {
                console.log(data);
            }
        });
    });

    $('#time-button-get').click(function(){
        if(($('#time-city').val() == '')||($('#time-country-select').val() == '')) {
            alert('Введите название города и страны');
            return;
        }
        $('#clock .item i').hide();
        $('#preload').show();
        $.ajax({
            url: '/site/addCityTime',
            method: 'POST',
            data: 'time-city=' + $('#time-city').val() + '&time-country=' + $('#time-country-select').val(),
            success: function (responce) {
                //console.log(responce);
                var data = JSON.parse(responce);
                if(data.msg == 'fail')
                    alert('Неверно указан город или страна');
                if((data.msg == 'ok')&&(data.time == '')){
                    alert('Неверно указан город или страна');
                } else
                {
                    if (data.msg == 'ok') {
                        var minutes;
                        if($('#clock .item').length > 0) {
                            var firstClock = $('#clock .item').eq(0);
                            var clock = firstClock.find('.clock span').eq(0).html();
                            minutes = clock.split(':')[1];
                        }

                        var html = '';
                        html += '<div class="item" data-id="'+data.id+'">';
                        html += '<i class="fa fa-times"></i>';
                        html += '<div class="city">';
                        html += '<span>' + data.city + '</span>';
                        html += '<br>';
                        html += '<span>' + data.country + '</span>';
                        html += '</div>';
                        html += '<div class="clock">';
                        var newTime = data.time;
                        var newHours = newTime.split(':')[0];
                        var newMinutes = minutes || newTime.split(':')[1];
                        data.time = newHours + ':' + newMinutes;
                        html += '<span>' + data.time + '</span>';
                        html += '<br>';
                        html += '<span>' + data.dayOfWeek + '</span>';
                        html += '</div>';
                        html += '</div>';
                        $('#clock').append(html);
                        $('#modal-add-time').modal('hide');
                    }
                }
            },
            error: function (data) {
                console.log(data);
            },
            complete: function(data){
                $('#preload').hide();
            }
        });
    });

    setInterval(function(){
        $('#clock .item').each(function(){
            var clock = $(this).find('.clock span').eq(0).html();
            var hours = clock.split(':')[0];
            var minutes = clock.split(':')[1];
            minutes = parseInt(minutes) + 1;
            if((minutes + "").length == 1)
                minutes = '0' + minutes;
            if(parseInt(minutes) == 60) {
                hours = parseInt(hours) + 1;
                if((hours + "").length == 1)
                    hours = '0' + hours;
                minutes = '00';
            }
            var time = hours + ':' + minutes;
            $(this).find('.clock span').eq(0).html(time);
        });
    }, 60000);

    $('.doctors-search-tabs li:nth-of-type(1) a').click(function(){
        $('.doctors-search-tabs li')[1].style.borderRadius = '0px 37px 0px 0px';
        $('.doctors-search-tabs li')[1].style.zIndex = '1';
    });
    $('.doctors-search-tabs li:nth-of-type(2) a').click(function(){
        $('.doctors-search-tabs li')[1].style.borderRadius = '37px 37px 0px 0px';
        $('.doctors-search-tabs li')[1].style.zIndex = '2';
    });
    $('.doctors-search-tabs li:nth-of-type(3) a').click(function(){
        $('.doctors-search-tabs li')[1].style.borderRadius = '37px 0px 0px 0px';
        $('.doctors-search-tabs li')[1].style.zIndex = '2';
    });

    $('#language').click(function(){
        $('#ul-language').toggle();
    });

    $('#ul-language li').click(function(){
        $('#language').removeClass('eng').removeClass('chn').removeClass('rus');
        if($(this).index() == 0)
            $('#language').addClass('rus');
        if($(this).index() == 1)
            $('#language').addClass('eng');
        if($(this).index() == 2)
            $('#language').addClass('chn');
        $('#language span').html($(this).html());
    });

    function getCityForTime() {
        var timeCityIndex = $('#time-city')[0].selectedIndex;
        var timeCityId = $('#time-city option').eq(timeCityIndex).data('id');
        var timeCityName = $('#time-city option').eq(timeCityIndex).html();
        var inputCity = $('#time-country-input');
        //console.log(timeCityIndex + ' ' + timeCityId + ' ' + timeCityName + ' ' + inputCity);
        //return;
        $('#preload').css('display', 'block');
        $.ajax({
            url: 'http://' + location.host + fixPath + '/site/getCityById',
            data: {idCountry: timeCityId},
            async: 'async',
            success: function (data) {
                var response = JSON.parse(data),
                    option = '<option>Не выбран</option>',
                    select = $('#time-country-select'),
                    cities;
                if (response) {
                    cities = response;
                    for (var i = 0; i < cities.length; i++) {
                        //if (!cities[i].area && cities[i].title == inputCity.val()) inputCity.attr('data-cityId', cities[i].id);
                        option += '<option value="' + cities[i].name + '" data-cityId="' + cities[i].id + '">' + cities[i].name + '</option>';
                    }
                    //option += '</optgroup>';
                    select.html(option);
                    select.show();
                    isLoadCity = true;
                }
            },
            error: function (data) {
                console.log('AJAX: Error selectCities()');
                console.log(data);
            },
            complete: function () {
                $('#preload').css('display', 'none');
            }
        });
    }

    $('#time-city').change(function(){
        $('#time-country-input').val('');
        getCityForTime();
    });

    /*
    $('#time-country-input').click(function(){
        getCityForTime();
    });
    $('#time-country-input').change(function(){
        getCityForTime();
    });
    $('#time-country-input').keyup(function(){
        getCityForTime();
    });
    $('#time-country-select').change(function(){
        $('#time-country-input').val($(this).val());
        $(this).hide();
    });
    */

    $('#time-city').val('');

    $(window).click(function(event){
        event = event || window.event;
        var target = event.target || event.srcElement;
        if((target.tagName == 'I')&&(target.className == 'fa fa-times')&&(target.parentNode.className == 'item')){
            var clockId = $(target.parentNode).attr('data-id');
            $('#clock .item[data-id="'+clockId+'"]').remove();
            $.ajax({
                url: '/site/removeCityTime',
                method: 'POST',
                data: 'idTimeCity=' + clockId,
                success: function (responce) {
                    console.log(responce);
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }
    })

});

/*var clipMenuSpeciality = function(isShowFullMenu) {
    var flag = false;
    var listTop = $($('.entireList')[0]).offset().top;
    if(!isShowFullMenu) {
        $('.menuSpeciality>ul>li').each(function () {
            if ($(this).offset().top > listTop) {
                flag = true;
            }
            if (flag == true) {
                $('.menuSpeciality>ul>li').eq($(this).index() - 1).addClass('hidden');
                $(this).addClass('hidden');
            }
        });
    }
    if(isShowFullMenu) {
        $('.menuSpeciality>ul>li').each(function () {
            $('.menuSpeciality>ul>li').eq($(this).index() - 1).removeClass('hidden');
            $($(this).removeClass('hidden'));
        });
    }
}*/

var switchUserType = function (el) {
        if (el.value == 3) {
            $('.displayDoctor').css('display', 'inline');
            $('.displayClinic').css('display', 'none');
            $('.displayNurse').css('display', 'none');
            $('.displayPatient').css('display', 'none');
            var radioNodes = document.querySelectorAll('#doctor-registration input[type="radio"]');
            radioNodes[el.value].click();
            radioNodes = document.querySelectorAll('#clinic-registration input[type="radio"]');
            radioNodes[el.value].click();
            radioNodes = document.querySelectorAll('#nurse-registration input[type="radio"]');
            radioNodes[el.value].click();
            radioNodes = document.querySelectorAll('#patient-registration input[type="radio"]');
            radioNodes[el.value].click();
        }
        if (el.value == 1) {
            $('.displayDoctor').css('display', 'none');
            $('.displayClinic').css('display', 'inline');
            $('.displayNurse').css('display', 'none');
            $('.displayPatient').css('display', 'none');
            var radioNodes = document.querySelectorAll('#doctor-registration input[type="radio"]');
            radioNodes[el.value].click();
            radioNodes = document.querySelectorAll('#clinic-registration input[type="radio"]');
            radioNodes[el.value].click();
            radioNodes = document.querySelectorAll('#nurse-registration input[type="radio"]');
            radioNodes[el.value].click();
            radioNodes = document.querySelectorAll('#patient-registration input[type="radio"]');
            radioNodes[el.value].click();
        }
        if (el.value == 2) {
            $('.displayDoctor').css('display', 'none');
            $('.displayClinic').css('display', 'none');
            $('.displayNurse').css('display', 'inline');
            $('.displayPatient').css('display', 'none');
            var radioNodes = document.querySelectorAll('#doctor-registration input[type="radio"]');
            radioNodes[el.value].click();
            radioNodes = document.querySelectorAll('#clinic-registration input[type="radio"]');
            radioNodes[el.value].click();
            radioNodes = document.querySelectorAll('#nurse-registration input[type="radio"]');
            radioNodes[el.value].click();
            radioNodes = document.querySelectorAll('#patient-registration input[type="radio"]');
            radioNodes[el.value].click();
        }
        if (el.value == 0) {
            $('.displayDoctor').css('display', 'none');
            $('.displayClinic').css('display', 'none');
            $('.displayNurse').css('display', 'none');
            $('.displayPatient').css('display', 'inline');
            var radioNodes = document.querySelectorAll('#doctor-registration input[type="radio"]');
            radioNodes[el.value].click();
            radioNodes = document.querySelectorAll('#clinic-registration input[type="radio"]');
            radioNodes[el.value].click();
            radioNodes = document.querySelectorAll('#nurse-registration input[type="radio"]');
            radioNodes[el.value].click();
            radioNodes = document.querySelectorAll('#patient-registration input[type="radio"]');
            radioNodes[el.value].click();
        }
    },
    currentId,
    currentType;

function Schedule(el) {
    $('#overlaySchedule').css('display', 'block');
    currentId = $(el)[0].dataset.id;
    currentType = $(el)[0].dataset.type;
    if (currentType == 'timeclinic') {
        $('#addTime').attr('disabled', 'disabled').addClass('disabled');
        $('#removeTime').attr('disabled', 'disabled').addClass('disabled');
    } else {
        $('#addTime').removeAttr('disabled').removeClass('disabled');
    }
}

function SaveSchedule() {

    var days = new Array,
        tab = $('#schedule'),
        timetable = $('.timetable[data-id="' + currentId + '"][data-type="' + currentType + '"]'),
        timetableTr =
            '<tr>' +
            '<th class="weekend">выходной</th>' +
            '<th class="weekend">выходной</th>' +
            '<th class="weekend">выходной</th>' +
            '<th class="weekend">выходной</th>' +
            '<th class="weekend">выходной</th>' +
            '<th class="weekend">выходной</th>' +
            '<th class="weekend">выходной</th>' +
            '</tr>',
        lenTr = tab.find('tr').length, // count row in #schedule
        lenTr2 = lenTr - 2,
        lenTrTimeTable = timetable.find('tr').length - 2,
        item = 0,
        deleteTr = true,
        from, to, result;

    for (var j = 1; j < lenTr; j++) {
        days.push(j);
        days[j] = [];
        if (lenTr > 2 && lenTrTimeTable < lenTr2) {
            timetable.find('tbody').append(timetableTr);
            lenTr2--;
        }
        for (var i = 0; i < 7; i++) {
            days[j].push(i);
            days[j][i] = [];
            days[j][i].push($($('#schedule input[type=text]')[item]).val());
            from = $($('#schedule input[type=text]')[item]).val();
            item++;
            days[j][i].push($($('#schedule input[type=text]')[item]).val());
            to = $($('#schedule input[type=text]')[item]).val();
            item++;
            if (from && to) {
                $(timetable.find('tr').eq(j).find('th')[i]).html('c <input value="' + from + '"><br>до <input value="' + to + '">');
                deleteTr = false;
            } else if (!from || !to) {
                $(timetable.find('tr').eq(j).find('th')[i]).html('выходной');
                days[j][i][days[j][i].length - 1] = '';
                days[j][i][days[j][i].length - 2] = '';
            }
            if (i == 6 && deleteTr) {
                if ($('#schedule tr').length > 2 && $('.timetable tr').length > 2) {
                    $('#schedule tr:last, .timetable tr:last').remove();
                    days.pop();
                }
            }
        }
        deleteTr = true;
    }
    if (lenTrTimeTable > lenTr2) {
        while (lenTrTimeTable != lenTr2) {
            timetable.find('tr').eq(-1).remove();
            lenTrTimeTable = timetable.find('tr').length - 2;
        }
    }

    for (i = 0; i < days.length; i++) {
        if (!$.isArray(days[i])) {
            days.splice(i, 1);
            i--;
        }
    }

    result = JSON.stringify(days);
    $('input[id^=Schedule_timework][data-id="' + currentId + '"][data-type="' + currentType + '"]').val(result);
    $('#overlaySchedule').css('display', 'none');
}

function escapeHtml(text) {
    var map = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#039;'
    };

    return text.replace(/[&<>"']/g, function (m) {
        return map[m];
    });
}

// Открываем расписание
function openSchedule(el) {
    currentId = $(el)[0].dataset.id;
    currentType = $(el)[0].dataset.type;

    var timeWork = JSON.parse($('input[data-id=' + currentId + '][data-type=' + currentType + ']').val()),
        tr = $('#schedule tr'),
        tbody = $('#schedule tbody'),
        countTr = $('#schedule tbody').find('tr').length,
        num = 0,
        isEmpty = true;

    if (countTr - 1 < timeWork.length) {
        for (var i = 0; i < timeWork.length; i++) {
            if (i > 0) tbody.append('<tr></tr>');
            for (var j = 0; j < timeWork[i].length; j++) {
                if (i == 0) {
                    if (timeWork[i][j][0] != '' && timeWork[i][j][1] != '') {
                        $($(tr.eq(1)).find('input')[num]).val(timeWork[i][j][0]);
                        num++;
                        $($(tr.eq(1)).find('input')[num]).val(timeWork[i][j][1]);
                        num++;
                    } else num += 2;
                } else {
                    $('#schedule tr:last').append('<th>c <input type="text" value="' + timeWork[i][j][0] + '"><br>до <input type="text" value="' + timeWork[i][j][1] + '"></th>');
                }
            }
        }
    } else {
        for (i = 0; i < timeWork.length; i++) {
            for (j = 0; j < timeWork[i].length; j++) {
                if (timeWork[i][j][0] != '' && timeWork[i][j][1] != '') {
                    $($(tr.eq(1)).find('input')[num]).val(timeWork[i][j][0]);
                    num++;
                    $($(tr.eq(1)).find('input')[num]).val(timeWork[i][j][1]);
                    num++;
                    isEmpty = false;
                } else {
                    $($(tr.eq(1)).find('input')[num]).val('');
                    num++;
                    $($(tr.eq(1)).find('input')[num]).val('');
                    num++;
                }
                if (j == timeWork[i].length - 1 && isEmpty) $(tr.eq(1)).find('input').each(function (id) {
                    $(this).val('')
                })
            }
        }
    }
    while (timeWork.length < countTr - 1) {
        $('#schedule tr:last').remove();
        countTr--;
    }
    $('#overlaySchedule').css('display', 'block');
}

// Устанавливаем расписание по умолчанию, если нет value
function setHiddenSchedule(schedule) {
    var clinic = $('#Schedule_timeworkClinic'), // Расписание клиники
        clinicDoc = $('#Schedule_timework[data-type="clinic"]'), // Расписание врачей в клинике
        doc = $('#Schedule_timework[data-type="doc"]'), // Расписание соло врачей
        type = [clinic, clinicDoc, doc];

    for (var i = 0; i < type.length; i++) {
        for (var j = 0; j < type[i].length; j++) {
            if (!$(type[i][j]).val()) $(type[i][j]).val(schedule);
        }
    }
}

$('#addToFavourites').click(function(){
    addToFavourites();
});

function addToFavourites(){
    if (window.sidebar) { // Mozilla Firefox Bookmark
        window.sidebar.addPanel(location.href,document.title,"");
    } else if(window.external) { // IE Favorite
        window.external.AddFavorite(location.href,document.title); }
    else if(window.opera && window.print) { // Opera Hotlist
        this.title=document.title;
        return true;
    }
}

$('#buttonSettings').click(function(e){
    $('#block-settings').toggle();
});

$('#smallFontSize').click(function(){
    $('.middleFontSize').removeClass('middleFontSize').addClass('smallFontSize');
    $('.bigFontSize').removeClass('bigFontSize').addClass('smallFontSize');
    $('#block-settings').css('top', '55px');
});
$('#middleFontSize').click(function(){
    $('.smallFontSize').removeClass('smallFontSize').addClass('middleFontSize');
    $('.bigFontSize').removeClass('bigFontSize').addClass('middleFontSize');
    $('#block-settings').css('top', '65px');
});
$('#bigFontSize').click(function(){
    $('.smallFontSize').removeClass('smallFontSize').addClass('bigFontSize');
    $('.middleFontSize').removeClass('middleFontSize').addClass('bigFontSize');
    $('#block-settings').css('top', '70px');
});

var invertMode = 'white';

$('.color-preview').eq(0).click(function(){
    if(invertMode == 'white') {
        $('#regToDoctorBlack').css('color', '#FFF');
        $('body').css('background-color', '#000');
        $('.phoneNumber').css('color', '#FFF');
        $('.order').css('color', '#FFF');
        $('#buttonToNormalVersion').removeClass('whiteInvertMode').addClass('blackInvertMode');
        $('#buttonSettings').removeClass('whiteInvertMode').addClass('blackInvertMode');
        $('.main-menu span a').css('color', '#FFF');
        $('h1').css('color', '#FFF');
        $('h1 a').css('color', '#FFF');
        $('ul li').css('color', '#FFF');
        $('.policy').css('color', '#FFF');
        $('.footer-date').css('color', '#FFF');
        $('#show-doctor span').css('color', '#FFF');
        $('#show-clinic span').css('color', '#FFF');
        $('#show-nurse span').css('color', '#FFF');
        $('label[for="typeHuman"]').css('color', '#FFF');
        $('.summary').css('color', '#FFF');
        $('.search-block').css('border', '2px solid #FFF');
        $('.search-block .search-description').css('color', '#FFF');
        $('.search-block .userInformationHead1 a').css('color', '#FFF');
        $('.priem').css('color', '#FFF');
        $('#moreInfoAboutUser').css('color', '#FFF');
        $('#moreInfoAboutUser .userInformationHead1').css('color', '#FFF');
        $('#moreInfoAboutUser .userInformationHead2').css('color', '#FFF');
        $('#postAdvert').css('color', '#FFF');
        $('#postAdvert .userInformationHead1').css('color', '#FFF');
        $('#postAdvert .userInformationHead2').css('color', '#FFF');
        $('#postAdvert .userInformationText').css('color', '#FFF');
        invertMode = 'black';
    }
});
$('.color-preview').eq(1).click(function(){
    if(invertMode == 'black') {
        $('#regToDoctorBlack').css('color', '#000');
        $('body').css('background-color', '#FFF');
        $('.phoneNumber').css('color', '#000');
        $('.order').css('color', '#000');
        $('#buttonToNormalVersion').removeClass('blackInvertMode').addClass('whiteInvertMode');
        $('#buttonSettings').removeClass('blackInvertMode').addClass('whiteInvertMode');
        $('.main-menu span a').css('color', '#000');
        $('h1').css('color', '#000');
        $('h1 a').css('color', '#000');
        $('ul li').css('color', '#000');
        $('.policy').css('color', '#000');
        $('.footer-date').css('color', '#000');
        $('#show-doctor span').css('color', '#000');
        $('#show-clinic span').css('color', '#000');
        $('#show-nurse span').css('color', '#000');
        $('label[for="typeHuman"]').css('color', '#000');
        $('.summary').css('color', '#000');
        $('.search-block').css('border', '2px solid #000');
        $('.search-block .search-description').css('color', '#000');
        $('.search-block .userInformationHead1 a').css('color', '#000');
        $('.priem').css('color', '#000');
        $('#moreInfoAboutUser').css('color', '#000');
        $('#moreInfoAboutUser .userInformationHead1').css('color', '#000');
        $('#moreInfoAboutUser .userInformationHead2').css('color', '#000');
        $('#postAdvert').css('color', '#000');
        $('#postAdvert .userInformationHead1').css('color', '#000');
        $('#postAdvert .userInformationHead2').css('color', '#000');
        $('#postAdvert .userInformationText').css('color', '#000');
        invertMode = 'white';
    }
});
$('#interval-standard').click(function(){
    $('body').css('letter-spacing','0px');
});
$('#interval-big').click(function(){
    $('body').css('letter-spacing','1px');
});

$(document).mouseup(function (e){
    var container = $('#block-settings');
    var button = $('#buttonSettings');
    if (!container.is(e.target)&&container.has(e.target).length === 0&&!button.is(e.target)){
        container.hide();
    }
});