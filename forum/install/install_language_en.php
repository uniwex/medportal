<?php
/*======================================================================*\
|| #################################################################### ||
|| # vBulletin 3.8.7 Patch Level 2
|| # ---------------------------------------------------------------- # ||
|| # Copyright ©2000–2011 vBulletin Solutions Inc. Все права защищены # ||
|| # --------------- VBULLETIN НЕ БЕСПЛАТНЫЙ ПРОДУКТ ---------------- # ||
|| #                  Русский перевод сделал zCarot                   # ||
|| #################################################################### ||
\*======================================================================*/

error_reporting(E_ALL & ~E_NOTICE & ~8192);

// перемещено из installcore сюда
$stylevar = array(
	'textdirection' => 'ltr',
	'left' => 'left',
	'right' => 'right',
	'languagecode' => 'ru',
	'charset' => 'utf-8'
);

$authenticate_phrases['install_title'] = 'Установочный скрипт';
$authenticate_phrases['new_installation'] = 'Новая установка';
$authenticate_phrases['enter_system'] = 'Вход в систему установки';
$authenticate_phrases['enter_cust_num'] = 'Пожалуйста, введите ваш клиентский номер';
$authenticate_phrases['customer_number'] = 'Клиентский номер';
$authenticate_phrases['cust_num_explanation'] = 'Это номер, с помощью которого вы входите в зону участников';
$authenticate_phrases['cust_num_success'] = 'Клиентский номер введён правильно.';
$authenticate_phrases['redirecting'] = 'Перемещение...';

$phrasetype['global'] = 'ОСНОВНЫЕ';
$phrasetype['cpglobal'] = 'Контрольная панель. Основное';
$phrasetype['cppermission'] = 'Права и разрешения';
$phrasetype['forum'] = 'Связанное с форумом';
$phrasetype['calendar'] = 'Календарь';
$phrasetype['attachment_image'] = 'Вложения / картинки';
$phrasetype['style'] = 'Управление стилями';
$phrasetype['logging'] = 'Управление записями';
$phrasetype['cphome'] = 'Контрольная панель. Главная страница';
$phrasetype['promotion'] = 'Управление повышениями';
$phrasetype['user'] = 'Управление участниками (основное)';
$phrasetype['help_faq'] = 'Система справки';
$phrasetype['sql'] = 'Управление SQL';
$phrasetype['subscription'] = 'Управление подписками';
$phrasetype['language'] = 'Управление языками';
$phrasetype['bbcode'] = 'Управление BB кодами';
$phrasetype['stats'] = 'Управление статистикой';
$phrasetype['diagnostics'] = 'Управление диагностикой';
$phrasetype['maintenance'] = 'Управление обслуживанием';
$phrasetype['profile'] = 'Управление полями профиля';
$phrasetype['cprofilefield'] = 'Другие поля профиля';
$phrasetype['thread'] = 'Управление темами';
$phrasetype['timezone'] = 'Временные зоны';
$phrasetype['banning'] = 'Управление блокировками';
$phrasetype['reputation'] = 'Репутация';
$phrasetype['wol'] = 'Кто на Форуме';
$phrasetype['threadmanage'] = 'Управление темами';
$phrasetype['pm'] = 'Личные Сообщения';
$phrasetype['cpuser'] = 'Контрольная панель. Управление участниками';
$phrasetype['accessmask'] = 'Маски доступа'; 
$phrasetype['cron'] = 'Запланированные задачи';
$phrasetype['moderator'] = 'Модераторы';
$phrasetype['cpoption'] = 'Контрольная панель. Опции';
$phrasetype['cprank'] = 'Контрольная панель. Звания участников';
$phrasetype['cpusergroup'] = 'Контрольная панель. Группы пользователей';
$phrasetype['holiday'] = 'Праздники';
$phrasetype['posting'] = 'Сообщения';
$phrasetype['poll'] = 'Опросы';
$phrasetype['fronthelp'] = 'Контекстная помощь';
$phrasetype['register'] = 'Регистрация';
$phrasetype['search'] = 'Поиск';
$phrasetype['showthread'] = 'Отображение темы';
$phrasetype['postbit'] = 'Части сообщений';
$phrasetype['forumdisplay'] = 'Отображение ';
$phrasetype['messaging'] = 'Сообщения';
$phrasetype['plugins'] = 'Система модулей';
$phrasetype['inlinemod'] = 'Встроенное управление';
$phrasetype['front_end_error'] = 'Сообщения об ошибках';
$phrasetype['front_end_redirect'] = 'Перемещения';
$phrasetype['email_body'] = 'Электронные письма. Сообщение';
$phrasetype['email_subj'] = 'Электронные письма. Тема';
$phrasetype['vbulletin_settings'] = 'Основные настройки';
$phrasetype['cp_help'] = 'Контрольная панель. Текст помощи';
$phrasetype['faq_title'] = 'Справка. Заголовки';
$phrasetype['faq_text'] = 'Справка. Текст';
$phrasetype['stop_message'] = 'Контрольная панель. Сообщение об ошибках';
$phrasetype['reputationlevel'] = 'Уровни репутации';
$phrasetype['infraction'] = 'Нарушения пользователей';
$phrasetype['infractionlevel'] = 'Уровни нарушений пользователей';
$phrasetype['notice'] = 'Важные сообщения';
$phrasetype['prefix'] = 'Префиксы тем';
$phrasetype['prefixadmin'] = 'Префиксы тем. Управление';
$phrasetype['album'] = 'Альбомы';
$phrasetype['hvquestion'] = 'Вопросы для отделения людей от роботов';
$phrasetype['socialgroups'] = 'Социальные группы';

#####################################
# разнообразные фразы               #
#####################################

$customphrases['cprofilefield']['field1_title'] = 'Биография';
$customphrases['cprofilefield']['field1_desc']  = 'Немного о себе';
$customphrases['cprofilefield']['field2_title'] = 'Местоположение';
$customphrases['cprofilefield']['field2_desc']  = 'Где вы проживаете';
$customphrases['cprofilefield']['field3_title'] = 'Интересы';
$customphrases['cprofilefield']['field3_desc']  = 'Ваши хобби и т.д.';
$customphrases['cprofilefield']['field4_title'] = 'Чем занимаетесь';
$customphrases['cprofilefield']['field4_desc']  = 'Кем работаете';

$customphrases['reputationlevel']['reputation1']  = 'презирают в этих краях';
$customphrases['reputationlevel']['reputation2']  = 'может только надеется на улучшение';
$customphrases['reputationlevel']['reputation3']  = 'имеет немного плохого в прошлом';
$customphrases['reputationlevel']['reputation4']  = 'пока не определено';
$customphrases['reputationlevel']['reputation5']  = 'на пути к лучшему';
$customphrases['reputationlevel']['reputation6']  = 'скоро придёт к известности';
$customphrases['reputationlevel']['reputation7']  = '- весьма и весьма положительная личность';
$customphrases['reputationlevel']['reputation8']  = 'как роза среди колючек';
$customphrases['reputationlevel']['reputation9']  = '- очень-очень хороший человек';
$customphrases['reputationlevel']['reputation10'] = '- луч света в тёмном царстве';
$customphrases['reputationlevel']['reputation11'] = '- это имя известно всем';
$customphrases['reputationlevel']['reputation12'] = '- просто великолепная личность';
$customphrases['reputationlevel']['reputation13'] = 'за этого человека можно гордиться';
$customphrases['reputationlevel']['reputation14'] = 'обеспечил(а) себе прекрасное будущее';
$customphrases['reputationlevel']['reputation15'] = 'репутация неоспорима';

$customphrases['infractionlevel']['infractionlevel1_title'] = 'Реклама и спам';
$customphrases['infractionlevel']['infractionlevel2_title'] = 'Оскорбление других участников';
$customphrases['infractionlevel']['infractionlevel3_title'] = 'Нарушение правил форума';
$customphrases['infractionlevel']['infractionlevel4_title'] = 'Матерная речь';

#####################################
# фразы для импорта системы         #
#####################################
$vbphrase['importing_language'] = 'Импорт языков';
$vbphrase['importing_style'] = 'Импорт стилей';
$vbphrase['importing_admin_help'] = 'Импорт помощи администратору';
$vbphrase['importing_settings'] = 'Импорт настроек';
$vbphrase['please_wait'] = 'Пожалуйста, подождите';
$vbphrase['language'] = 'Язык';
$vbphrase['master_language'] = 'ОСНОВНОЙ ЯЗЫК';
$vbphrase['admin_help'] = 'Помощь администратору';
$vbphrase['style'] = 'Стиль';
$vbphrase['styles'] = 'Стили';
$vbphrase['settings'] = 'Настройки';
$vbphrase['master_style'] = 'ОСНОВНОЙ СТИЛЬ';
$vbphrase['templates'] = 'Шаблоны';
$vbphrase['css'] = 'CSS';
$vbphrase['stylevars'] = 'Переменные стиля';
$vbphrase['replacement_variables'] = 'Замещаемые переменные';
$vbphrase['controls'] = 'Управление';
$vbphrase['rebuild_style_information'] = 'Перестройка информации стиля';
$vbphrase['updating_style_information_for_each_style'] = 'Обновление информации о стиле для каждого из них';
$vbphrase['updating_styles_with_no_parents'] = 'Обновление стилей установленных с отсутствием предыдущей информации';
$vbphrase['updated_x_styles'] = 'Обновлено стилей: %1$s';
$vbphrase['no_styles_needed_updating'] = 'Стили не нуждаются в обновлении';
$vbphrase['yes'] = 'Да';
$vbphrase['no'] = 'Нет';

#####################################
# фразы глобального обновления      #
#####################################
$vbphrase['refresh'] = 'Обновление';
$vbphrase['vbulletin_message'] = 'Сообщение vBulletin';
$vbphrase['create_table'] = 'Создание таблицы %1$s';
$vbphrase['remove_table'] = 'Удаление таблицы %1$s';
$vbphrase['alter_table'] = 'Изменение таблицы %1$s';
$vbphrase['update_table'] = 'Обновление таблицы %1$s';
$vbphrase['upgrade_start_message'] = "<p>Этот скрипт обновит ваш установленный vBulletin до версии <b>" . VERSION . "</b></p>\n<p>Нажмите кнопку 'Дальше' для продолжения.</p>";
$vbphrase['upgrade_wrong_version'] = "<p>Ваша версия vBulletin не совместима с версией этого скрипта (версия <b>" . PREV_VERSION . "</b>).</p>\n<p>Пожалуйста, убедитесь, что вы запустили верный скрипт.</p>\n<p>Если вы уверены, что скрипт, который вы желаете запустить тот, то <a href=\"" . THIS_SCRIPT . "?step=1\">нажмите здесь</a>.</p>";
$vbphrase['file_not_found'] = 'Ой-ой, ./install/%1$s не существует!';
$vbphrase['importing_file'] = 'Импорт %1$s';
$vbphrase['ok'] = 'Ok';
$vbphrase['query_took'] = 'Запрос на выполнение занял %1$s секунд(ы).';
$vbphrase['done'] = 'Выполнено';
$vbphrase['proceed'] = 'Продолжить';
$vbphrase['reset'] = 'Сбросить';
$vbphrase['vbulletin_copyright'] = 'vBulletin v' . VERSION . '<br />Copyright &copy;2000 - ' . date('Y') . ', vBulletin Solutions, Inc<br /> Перевод сделал zCarot';
$vbphrase['vbulletin_copyright_orig'] = $vbphrase['vbulletin_copyright'];
$vbphrase['xml_error_x_at_line_y'] = 'Ошибка XML: %1$s в строке %2$s';
$vbphrase['default_data_type'] = 'Вставка данных в %1$s';
$vbphrase['processing_complete_proceed'] = 'Действие завершено - продолжаем';
#####################################
# фразы обновления основы           #
#####################################

$installcore_phrases['php_version_too_old'] = 'vBulletin ' . VERSION . ' требуется версия PHP 4.3.3 или выше. Ваша версия PHP - ' . PHP_VERSION . ', пожалуйста, попросите вашего хостера обновить PHP.';
$installcore_phrases['mysql_version_too_old'] = 'vBulletin ' . VERSION . ' требуется версия MySQL 4.0.16 или выше. Вы используете MySQL %1$s, пожалуйста, попросите вашего хостера обновить MySQL.';
$installcore_phrases['need_xml'] = 'vBulletin ' . VERSION . ' требуется доступ к функциям XML в PHP. Пожалуйста, попросите вашего хостера включить его.';
$installcore_phrases['need_mysql'] = 'vBulletin ' . VERSION . ' требуется доступ к функциям MySQL в PHP. Пожалуйста, попросите вашего хостера включить его.';
$installcore_phrases['need_config_file'] = 'Пожалуйста, убедитесь, что вы ввели значения в config.php.new и переименовали его в config.php.';
$installcore_phrases['step_x_of_y'] = ' (действие %1$d из %2$d)';
$installcore_phrases['vb3_install_script'] = 'Установочный скрипт vBulletin ' . VERSION . ' ';
$installcore_phrases['may_take_some_time'] = '<a href="mailto:">(Пожалуйста, проявите терпение, т.к. некоторые действия могут занять много времени)</a>';
$installcore_phrases['step_title'] = 'Действие %1$d: %2$s';
$installcore_phrases['batch_complete'] = 'Группа действий завершена! Нажмите на кнопку справа, если вас не перенаправят автоматически.';
$installcore_phrases['next_batch'] = ' Дальше';
$installcore_phrases['next_step'] = 'Дальше (%1$d/%2$d)';
$installcore_phrases['click_button_to_proceed'] = 'Нажмите кнопку справа для продолжения.';
$installcore_phrases['page_x_of_y'] = 'Страница %1$d из %2$d';
$installcore_phrases['eaccelerator_too_old'] = 'eAccelerator для PHP должен быть обновлен до версии 0.9.3 или выше. ';
$installcore_phrases['apc_too_old'] = 'На вашем сервере установлена старая версия <a href="http://pecl.php.net/package/APC/">Alternative PHP Cache</a> (APC), которая не совместима с vBulletin. Вам следует обновить APC как минимум до 3.0.0.';
$installcore_phrases['mmcache_not_supported'] = 'Turck MMCache устарела в eAccelerator и не корректно работает с vBulletin.';
$installcore_phrases['dbname_is_mysql'] = 'Имя базы данных, указанное в параметре <code>$config[\'Database\'][\'dbname\']</code> файла <em>includes/config.php</em>, не может быть "<strong>mysql</strong>", так как это зарезервированное имя для баз данных.<br />Данное исключение было добавлено для предотвращения возможных повреждений.';

#####################################
# фразы install.php                 #
#####################################
$install_phrases['steps'] = array(
        1  => 'проверка конфигурации',
        2  => 'связь с базой данных',
        3  => 'создание таблиц',
        4  => 'изменение таблиц',
        5  => 'вставка данных по умолчанию',
        6  => 'импортирование языка',
        7  => 'импортирование стиля',
        8  => 'импортирование справки администратора',
        9  => 'получение некоторых настроек по умолчанию',
        10 => 'импорт настроек по умолчанию',
        11 => 'получение данных пользователя',
        12 => 'установка данных по умолчанию',
        13 => 'установка завершена'
);
$install_phrases['welcome'] = '<p style="font-size:10pt"><b>Добро пожаловать в vBulletin версии 3.8</b></p>
        <p>Вы находитесь в оболочке для установки.</p>
        <p>При нажатии кнопки <b>[Дальше]</b> начнётся процесс установки форума в вашу базу данных.</p>
        <p>Чтобы предотвратить возможные ошибки обозревателя в течение выполнения этого скрипта, мы настоятельно рекомендуем выключить все дополнительные панели вашего обозревателя, такие, как панель <b>Google</b>.</p>';
$install_phrases['turck'] = '<p><strong>Turck MMCache</strong> был обнаружен на вашем сервере.  <strong>Turck MMCache</strong> не поддерживается в последних версиях PHP и может вызывать проблемы с vBulletin. Мы рекомендуем выключить его.</p>';
$install_phrases['cant_find_config'] = 'Мы не смогли найти файл \'includes/config.php\' , пожалуйста, убедитесь, что он существует.';
$install_phrases['cant_read_config'] = 'Мы не смогли прочитать содержимое файла \'includes/config.php\' , пожалуйста, проверьте права доступа.';
$install_phrases['config_exists'] = 'Файл конфигурации существует и читаем.';
$install_phrases['attach_to_db'] = 'Попытка связи с базой данных';
$install_phrases['no_db_found_will_create'] = 'База данных не найдена, попытка создания.';
$install_phrases['attempt_to_connect_again'] = 'Попытка связаться снова.';
$install_phrases['database_functions_not_detected'] = 'Выбранный тип базы данных \'%1$s\' не обнаружен в вашей сборке PHP.';
$install_phrases['unable_to_create_db'] = 'Невозможно создать базу данных, пожалуйста проверьте название базы данных в файле \'includes/config.php\' или попросите вашего хостера создать базу данных.';
$install_phrases['database_creation_successful'] = 'Создание базы данных завершилось успешно!';
$install_phrases['connect_failed'] = 'Связь не произошла: неожиданная ошибка из базы данных.';
$install_phrases['db_error_num'] = 'Номер ошибки: %1$s';
$install_phrases['db_error_desc'] = 'Описание ошибки: %1$s';
$install_phrases['check_dbserver'] = 'Пожалуйста, убедитесь, что база данных и сервер правильно сконфигурированы, и попробуйте снова.';
$install_phrases['connection_succeeded'] = 'Связь успешно установлена! База данных уже существует.';
$install_phrases['vb_installed_maybe_upgrade'] = 'Вы уже устанавливали vBulletin; желаете ли вы <a href="upgrade.php">обновить</a> его?';
$install_phrases['wish_to_empty_db'] = 'Нажмите здесь, если вы желаете воспользоваться возможностью очистки текущего содержимого вашей базы данных.';
$install_phrases['no_connect_permission'] = 'Невозможно связаться с вашей базой данных, т.к. у вас нет прав соединяться с сервером. Пожалуйста, подтвердите значения, введённые в файле \'includes/config.php\' .';
$install_phrases['reset_database'] = 'Очистить базу данных';
$install_phrases['delete_tables_instructions'] = '<p>Ниже находится полный список таблиц, найденных в вашей базе данных. Таблицы, которые относятся к форуму, уже выбраны. Возможно, есть ещё таблицы, которые, вероятно, также относятся к форуму, они выделены в списке.</p>
<p style="font-size:12pt">При нажатии кнопки <em>Удалить выбранные таблицы</em> все выбранные таблицы и их содержимое будет <strong>полностью удалено из базы данных без возможности восстановления</strong>.</p>
<p><a href="install.php?step=2">Нажмите здесь, если вы желаете вернуться к процессу установки без удаления каких-либо таблиц.</a></p>
<p>vBulletin, vBulletin Solutions, Inc и zCаrot не отвечают за возможную потерю каких-либо данных, произошедшую в результате удаления таблиц из базы данных.</p>';
$install_phrases['select_deselect_all_tables'] = 'Установка / отмена выбора на все таблицы';
$install_phrases['delete_selected_tables'] = 'Удалить выбранные таблицы';
$install_phrases['mysql_strict_mode'] = 'MySQL запущен в строгом режиме. Вы можете продолжать, но некоторые области vBulletin не будут правильно функционировать. <em>Настоятельно рекомендуется</em> установить <code>$config[\'Database\'][\'force_sql_mode\']</code> на <code>true</code> в вашем файле includes/config.php. <br /><br /><code>$config[\'Database\'][\'force_sql_mode\'] = true';
$install_phrases['resetting_db'] = 'Очищение базы данных...';
$install_phrases['succeeded'] = 'успешно';
$install_phrases['script_reported_errors'] = 'Скрипт сообщает об ошибках при установке таблиц. Продолжайте только, если вы уверены, что они не существенны.';
$install_phrases['errors_were'] = 'Ошибки были:';
$install_phrases['tables_setup'] = 'Таблицы успешно установлены.';
$install_phrases['general_settings'] = 'Основные Настройки';
$install_phrases['bbtitle'] = '<b>Название</b> <dfn>Название форума. Появляется в заголовке каждой страницы.</dfn>';
$install_phrases['hometitle'] = '<b>Имя главного сайта</b> <dfn>Название вашего главного сайта. Отображается внизу каждой страницы.</dfn>';
$install_phrases['bburl'] = '<b>URL форума</b> <dfn>URL (без последнего "/") форума. Например, <em>http://www.example.com/forum</em></dfn>';
$install_phrases['homeurl'] = '<b>URL главного сайта</b> <dfn>URL вашего главного сайта. Отображается внизу каждой страницы.</dfn>';
$install_phrases['webmasteremail'] = '<b>Адрес электронной почты веб-мастера</b> <dfn>Email технического администратора (веб-мастера) сайта.</dfn>';
$install_phrases['cookiepath'] = '<b>Путь для Cookies</b> <dfn>Путь для сохранения Cookies. Если у вас более одного форума на одном и том же домене, то вам необходимо указать отдельные директории для каждого из форумов. Если форум один, просто оставьте как есть / .<br /><br />Предполагаемые значения для этого параметра доступны в ниспадающем меню. Если у вас есть хорошая причина указать другие параметры, то поставьте галочку и введите своё значение в поле.<br /><br />Помните, что ваш путь должен <b>всегда</b> заканчиваться на /; например \'/forum/\', \'/vb/\' и т.п.<br /><br /><span class="modlasttendays">Если вы введёте неверное значение, то не сможете зайти на форум.</span></dfn>';
$install_phrases['cookiedomain'] = '<b>Домен Cookies</b> <dfn>Этот параметр устанавливает домен, к которому вы хотите сделать привязку Сookies. Самая частая причина исправления этого параметра - наличие двух разных ссылок на ваш форум, например example.com и forum.example.com. Чтобы разрешить пользователям оставаться на форуме, зайдя по другому адресу, вам следует установить здесь <b>.vashsajt.com</b> (помните, что домен начинается с <b>точки</b>).<br /><br />Предполагаемые значения для этого параметра доступны в ниспадающем меню. Если у вас есть хорошая причина указать другие параметры, то поставьте галочку и введите своё значение в поле.<br /><br /><span class="modlasttendays">Лучше пропустить этот пункт, так как если вы введёте неверное значение, то не сможете зайти на форум.</span></dfn>';
$install_phrases['suggested_settings'] = 'Предполагаемые значения';
$install_phrases['custom_setting'] = 'Другое значение';
$install_phrases['use_custom_setting'] = 'Использовать другое значение (укажите его ниже)';
$install_phrases['blank'] = '(пусто)';
$install_phrases['fill_in_for_admin_account'] = 'Пожалуйста, заполните форму ниже, чтобы настроить доступ администратора';
$install_phrases['username'] = 'Имя пользователя';
$install_phrases['password'] = 'Пароль';
$install_phrases['confirm_password'] = 'Подтверждение пароля';
$install_phrases['email_address'] = 'Адрес электронной почты';
$install_phrases['complete_all_data'] = 'Вы заполнили не все поля.<br /><br />Пожалуйста, нажмите кнопку \'Дальше\' для возврата назад и заполните всё.';
$install_phrases['password_not_match'] = 'Поля \'Пароль\' и \'Подтверждение пароля\' не совпадают!<br /><br />Пожалуйста, нажмите кнопку \'Дальше\' для возврата назад и исправьтесь.';
$install_phrases['admin_added'] = 'Администратор добавлен';
$install_phrases['install_complete'] = '<p>Вы успешно установили vBulletin 3.8<br />
        <br />
        <font size="+1"><b>ВЫ ДОЛЖНЫ УДАЛИТЬ СЛЕДУЮЩИЕ ФАЙЛЫ ДО ТОГО, КАК ПРОДОЛЖИТЬ:</b><br />
        install/install.php<br />
        install/init.php</font><br />
        <br />
        Когда вы это сделаете, вы можете зайти в вашу контрольную панель.
        <br />
        Контрольная панель находится <b><a href="../%1$s/index.php">здесь</a></b>';

$install_phrases['alter_table_type_x'] = 'Изменение ' . TABLE_PREFIX . '%1$s в тип %2$s';
$install_phrases['default_calendar'] = 'Обычный календарь';
$install_phrases['category_title'] = 'Главная категория';
$install_phrases['category_desc'] = 'Описание главной категории';
$install_phrases['forum_title'] = 'Главный раздел';
$install_phrases['forum_desc'] = 'Описание главного раздела';
$install_phrases['posticon_1'] = 'Сообщение';
$install_phrases['posticon_2'] = 'Стрелка';
$install_phrases['posticon_3'] = 'Лампочка';
$install_phrases['posticon_4'] = 'Восклицание';
$install_phrases['posticon_5'] = 'Вопрос';
$install_phrases['posticon_6'] = 'Счастье';
$install_phrases['posticon_7'] = 'Радость';
$install_phrases['posticon_8'] = 'Злость';
$install_phrases['posticon_9'] = 'Печаль';
$install_phrases['posticon_10'] = 'Смех';
$install_phrases['posticon_11'] = 'Смущение';
$install_phrases['posticon_12'] = 'Подмигивание';
$install_phrases['posticon_13'] = 'Плохо';
$install_phrases['posticon_14'] = 'Хорошо';
$install_phrases['generic_avatars'] = 'Основные аватары';
$install_phrases['generic_smilies'] = 'Основные смайлы';
$install_phrases['generic_icons'] = 'Основные иконки';
// должно соответствовать содержимому vbulletin-language.xml
$install_phrases['master_language_title'] = 'English (EN)';
$install_phrases['master_language_langcode'] = 'en';
$install_phrases['master_language_charset'] = 'UTF-8';
$install_phrases['master_language_decimalsep'] = '.';
$install_phrases['master_language_thousandsep'] = ',';
$install_phrases['default_style'] = 'Обычный стиль';
$install_phrases['smilie_smile'] = 'Радость';
$install_phrases['smilie_embarrass'] = 'Смущение';
$install_phrases['smilie_grin'] = 'Смех';
$install_phrases['smilie_wink'] = 'Подмигивание';
$install_phrases['smilie_tongue'] = 'Язычок';
$install_phrases['smilie_cool'] = 'Счастье';
$install_phrases['smilie_roll'] = 'Сарказм';
$install_phrases['smilie_mad'] = 'Злость';
$install_phrases['smilie_eek'] = 'Удивление';
$install_phrases['smilie_confused'] = 'Непонимание';
$install_phrases['smilie_frown'] = 'Печаль';
$install_phrases['socialgroups_uncategorized'] = 'Вне категорий';
$install_phrases['socialgroups_uncategorized_description'] = 'Социальные группы вне категорий';
$install_phrases['usergroup_guest_title'] = 'Не зарегистрированные / не вошедшие';
$install_phrases['usergroup_guest_usertitle'] = 'Гость';
$install_phrases['usergroup_registered_title'] = 'Зарегистрированные';
$install_phrases['usergroup_activation_title'] = 'Ожидающие подтверждения по электронной почте';
$install_phrases['usergroup_coppa_title'] = '(COPPA) Ожидающие проверки';
$install_phrases['usergroup_super_title'] = 'Супер-модераторы';
$install_phrases['usergroup_super_usertitle'] = 'Супер-модератор';
$install_phrases['usergroup_admin_title'] = 'Администраторы';
$install_phrases['usergroup_admin_usertitle'] = 'Администратор';
$install_phrases['usergroup_mod_title'] = 'Модераторы';
$install_phrases['usergroup_mod_usertitle'] = 'Модератор';
$install_phrases['usergroup_banned_title'] = 'Заблокированные';
$install_phrases['usergroup_banned_usertitle'] = 'Заблокирован';
$install_phrases['usertitle_jnr'] = 'Новичок';
$install_phrases['usertitle_mbr'] = 'Пользователь';
$install_phrases['usertitle_snr'] = 'Местный';

/*======================================================================*\
|| #################################################################### ||
|| #                  Русский перевод  сделал zCarot                  # ||
|| # CVS: $RCSfile$ - $Revision: 39862 $
|| #################################################################### ||
\*======================================================================*/
?>