<?php
/*======================================================================*\
|| #################################################################### ||
|| # vBulletin 3.8.7 Patch Level 2
|| # ---------------------------------------------------------------- # ||
|| # Copyright ©2000–2011 vBulletin Solutions Inc. Все права защищены # ||
|| # --------------- VBULLETIN НЕ БЕСПЛАТНЫЙ ПРОДУКТ ---------------- # ||
|| #                  Русский перевод сделал zCarot                   # ||
|| #################################################################### ||
\*======================================================================*/

error_reporting(E_ALL & ~E_NOTICE & ~8192);

// перемещено из upgradecore сюда
$stylevar = array(
	'textdirection' => 'ltr',
	'left' => 'left',
	'right' => 'right',
	'languagecode' => 'ru',
	'charset' => 'utf-8'
);

$authenticate_phrases['upgrade_title'] = 'Система обновления';
$authenticate_phrases['enter_system'] = 'Вход в систему обновления';
$authenticate_phrases['enter_cust_num'] = 'Пожалуйста, введите ваш клиентский номер';
$authenticate_phrases['customer_number'] = 'Клиентский номер';
$authenticate_phrases['cust_num_explanation'] = 'Это номер, с помощью которого вы входите в зону участников';
$authenticate_phrases['cust_num_success'] = 'Клиентский номер введён правильно.';
$authenticate_phrases['redirecting'] = 'Перемещение...';

#####################
# фразы upgradecore #
#####################
$upgradecore_phrases['vb3_upgrade_system'] = 'Система обновления vBulletin 3.8';
$upgradecore_phrases['please_login'] = 'Пожалуйста, введите своё:';
$upgradecore_phrases['username'] = '<u>И</u>мя пользователя';
$upgradecore_phrases['password'] = '<u>П</u>ароль';
$upgradecore_phrases['login'] = 'Вход';
$upgradecore_phrases['php_version_too_old'] = 'vBulletin 3.8 требуется версия PHP 4.3.3 или выше. Ваша версия PHP - ' . PHP_VERSION . ', пожалуйста, попросите вашего хостера обновить PHP.';
$upgradecore_phrases['mysql_version_too_old'] = 'vBulletin 3.8 требуется версия MySQL 4.0.16 или выше. Вы используете MySQL %1$s, пожалуйста, попросите вашего хостера обновить MySQL.';
$upgradecore_phrases['ensure_config_exists'] = 'Пожалуйста, убедитесь, что вы создали новую структуру директорий, требуемую  vBulletin 3';
$upgradecore_phrases['mysql_strict_mode'] = 'MySQL запущен в строгом режиме. Вы можете продолжать, но некоторые области vBulletin не будут правильно функционировать. <em>Настоятельно рекомендуется</em> установить <code>$config[\'Database\'][\'force_sql_mode\']</code> на <code>true</code> в вашем файле includes/config.php. <br /><br /><code>$config[\'Database\'][\'force_sql_mode\'] = true';
$upgradecore_phrases['step_x_of_y'] = ' (действие %1$d из %2$d)';
$upgradecore_phrases['unknown'] = 'Неизвестно';
$upgradecore_phrases['file_not_found'] = 'Файл не найден!';
$upgradecore_phrases['xml_file_versions'] = 'Версии файлов XML:';
$upgradecore_phrases['may_take_some_time'] = '(Пожалуйста, проявите терпение, т.к. некоторые действия могут занять много времени)';
$upgradecore_phrases['update_v_number'] = 'Обновление до версии ' . VERSION . '... ';
$upgradecore_phrases['skipping_v_number_update'] = 'Пропускаем обновление номера версии, так как у вас уже есть более новая версия... ';
$upgradecore_phrases['done'] = 'Выполнено';
$upgradecore_phrases['step_title'] = 'Действие %1$d) %2$s';
$upgradecore_phrases['batch_complete'] = 'Группа действий завершена! Нажмите на кнопку справа, если вас не перенаправят автоматически.';
$upgradecore_phrases['next_batch'] = ' Дальше';
$upgradecore_phrases['next_step'] = 'Дальше (%1$d/%2$d)';
$upgradecore_phrases['click_button_to_proceed'] = 'Нажмите кнопку справа для продолжения.';
$upgradecore_phrases['page_x_of_y'] = 'Страница %1$d из %2$d';
$upgradecore_phrases['semicolons_file_intro'] = "СЛЕДУЮЩИЕ ИМЕНА ПОЛЬЗОВАТЕЛЕЙ СОДЕРЖАТ ТОЧКИ С ЗАПЯТОЙ ( ; )\r\nИ *ДОЛЖНЫ* БЫТЬ ИЗМЕНЕНЫ В ПАНЕЛИ УПРАВЛЕНИЯ:";
$upgradecore_phrases['dump_data_to_sql'] = 'Сохранение данных в файл SQL';
$upgradecore_phrases['choose_table_to_dump'] = 'Выберите таблицу для сохранения в файл';
$upgradecore_phrases['dump_tables'] = 'Сохранить таблицу(-ы)';
$upgradecore_phrases['dump_data_to_csv'] = 'Сохранение данных в файл CSV';
$upgradecore_phrases['backup_individual_table'] = 'Копирование <b>индивидуальных таблиц</b>';
$upgradecore_phrases['field_seperator'] = 'Разделители полей';
$upgradecore_phrases['quote_character'] = 'Символ кавычек';
$upgradecore_phrases['show_column_names'] = 'Отображать названия колонок';
$upgradecore_phrases['dump_table'] = 'Сохранить таблицу';
$upgradecore_phrases['vb_db_dump_completed'] = 'СОХРАНЕНИЕ БАЗА ДАННЫХ VBULLETIN ЗАВЕРШЕНО';
$upgradecore_phrases['dump_all_tables'] = '* СОХРАНИТЬ ВСЕ ТАБЛИЦЫ *';
$upgradecore_phrases['dump_database_desc'] = '<p class="smallfont">Отсюда вы можете сделать копию базы данных вашего vBulletin.</p>
                <p class="smallfont">Пожалуйста, помните, что если у вас очень большая база данных, то
                этот скрипт <i>может</i> сделать не полную копию.</p>
                <p class="smallfont">Для корректной резервной копии, войдите на ваш сервер через Telnet или SSH и используйте
                команду <i>mysqldump</i> в командной строке.</p>';
$upgradecode_phrases['vb_database_backup_system'] = 'Система копирования базы данных vBulletin';
$upgradecore_phrases['backup_after_upgrade'] = '<p class="smallfont">Вы должны сделать резервную копию базы данных до выполнения обновления, если обновление уже завершено - используйте для этого панель администратора.</p>
		<p class="smallfont">Для корректной резервной копии, войдите на ваш сервер через Telnet или SSH и используйте
                команду <i>mysqldump</i> в командной строке.</p>';
$upgradecore_phrases['eaccelerator_too_old'] = 'eAccelerator для PHP должен быть обновлен до версии 0.9.3 или выше. ';
$upgradecore_phrases['apc_too_old'] = 'На вашем сервере установлена старая версия <a href="http://pecl.php.net/package/APC/">Alternative PHP Cache</a> (APC), которая не совместима с vBulletin. Вам следует обновить APC как минимум до 3.0.0.';
$upgradecore_phrases['mmcache_not_supported'] = 'Turck MMCache устарела в eAccelerator и не корректно работает с vBulletin.'; 
$upgradecore_phrases['altering_x_table'] = 'Изменяем таблицу %1$s (%2$d из %3$d)';
$upgradecore_phrases['wrong_bitfield_xml'] = 'Файл includes/xml/bitfield_vbulletin.xml устарел. Пожалуйста, убедитесь, что вы загрузили правильный файл.';

// этот параметр должен содержать только символы, которые отображаются в файловой системе
// он указывает на файл, содержащий неправильные имена пользователей.
$upgradecore_phrases['illegal_user_names'] = 'Illegal User Names.txt';

$phrasetype['global'] = 'ОСНОВНЫЕ';
$phrasetype['cpglobal'] = 'Контрольная панель. Основное';
$phrasetype['cppermission'] = 'Права и разрешения';
$phrasetype['forum'] = 'Связанное с форумом';
$phrasetype['calendar'] = 'Календарь';
$phrasetype['attachment_image'] = 'Вложения / картинки';
$phrasetype['style'] = 'Управление стилями';
$phrasetype['logging'] = 'Управление записями';
$phrasetype['cphome'] = 'Контрольная панель. Главная страница';
$phrasetype['promotion'] = 'Управление повышениями';
$phrasetype['user'] = 'Управление участниками (основное)';
$phrasetype['help_faq'] = 'Система справки';
$phrasetype['sql'] = 'Управление SQL';
$phrasetype['subscription'] = 'Управление подписками';
$phrasetype['language'] = 'Управление языками';
$phrasetype['bbcode'] = 'Управление BB кодами';
$phrasetype['stats'] = 'Управление статистикой';
$phrasetype['diagnostics'] = 'Управление диагностикой';
$phrasetype['maintenance'] = 'Управление обслуживанием';
$phrasetype['profile'] = 'Управление полями профиля';
$phrasetype['cprofilefield'] = 'Другие поля профиля';
$phrasetype['thread'] = 'Управление темами';
$phrasetype['timezone'] = 'Временные зоны';
$phrasetype['banning'] = 'Управление блокировками';
$phrasetype['reputation'] = 'Репутация';
$phrasetype['wol'] = 'Кто на форуме';
$phrasetype['threadmanage'] = 'Управление темами';
$phrasetype['pm'] = 'Личные сообщения';
$phrasetype['cpuser'] = 'Контрольная панель. Управление участниками';
$phrasetype['accessmask'] = 'Маски доступа'; 
$phrasetype['cron'] = 'Запланированные задачи';
$phrasetype['moderator'] = 'Модераторы';
$phrasetype['cpoption'] = 'Контрольная панель. Опции';
$phrasetype['cprank'] = 'Контрольная панель. Звания участников';
$phrasetype['cpusergroup'] = 'Контрольная панель. Группы пользователей';
$phrasetype['holiday'] = 'Праздники';
$phrasetype['posting'] = 'Сообщения';
$phrasetype['poll'] = 'Опросы';
$phrasetype['fronthelp'] = 'Соответствующая помощь';
$phrasetype['register'] = 'Регистрация';
$phrasetype['search'] = 'Поиск';
$phrasetype['showthread'] = 'Отображение темы';
$phrasetype['postbit'] = 'Части сообщений';
$phrasetype['forumdisplay'] = 'Отображение ';
$phrasetype['messaging'] = 'Сообщения';
$phrasetype['plugins'] = 'Система модулей';
$phrasetype['inlinemod'] = 'Встроенное управление';
$phrasetype['front_end_error'] = 'Сообщения об ошибках';
$phrasetype['front_end_redirect'] = 'Перемещения';
$phrasetype['email_body'] = 'Электронные письма. Сообщение';
$phrasetype['email_subj'] = 'Электронные письма. Тема';
$phrasetype['vbulletin_settings'] = 'Основные настройки';
$phrasetype['cp_help'] = 'Контрольная панель. Текст помощи';
$phrasetype['faq_title'] = 'Справка. Заголовки';
$phrasetype['faq_text'] = 'Справка. Текст';
$phrasetype['stop_message'] = 'Контрольная панель. Сообщение об ошибках';
$phrasetype['reputationlevel'] = 'Уровни репутации';
$phrasetype['infraction'] = 'Нарушения пользователей';
$phrasetype['infractionlevel'] = 'Уровни нарушений пользователей';
$phrasetype['notice'] = 'Важные сообщения';
$phrasetype['prefix'] = 'Префиксы тем';
$phrasetype['prefixadmin'] = 'Префиксы тем. Управление';
$phrasetype['album'] = 'Альбомы';
$phrasetype['hvquestion'] = 'Вопросы для отделения людей от роботов';
$phrasetype['socialgroups'] = 'Социальные группы';

#############################
# фразы для импорта системы #
#############################
$vbphrase['please_wait'] = 'Пожалуйста, подождите';
$vbphrase['language'] = 'Язык';
$vbphrase['master_language'] = 'ОСНОВНОЙ ЯЗЫК';
$vbphrase['admin_help'] = 'Помощь администратору';
$vbphrase['style'] = 'Стиль';
$vbphrase['styles'] = 'Стили';
$vbphrase['settings'] = 'Настройки';
$vbphrase['master_style'] = 'ОСНОВНОЙ СТИЛЬ';
$vbphrase['templates'] = 'Шаблоны';
$vbphrase['css'] = 'CSS';
$vbphrase['stylevars'] = 'Переменные стиля';
$vbphrase['replacement_variables'] = 'Замещаемые переменные';
$vbphrase['controls'] = 'Управление';
$vbphrase['rebuild_style_information'] = 'Перестройка информации стиля';
$vbphrase['updating_style_information_for_each_style'] = 'Обновление информации о стиле для каждого из них';
$vbphrase['updating_styles_with_no_parents'] = 'Обновление стилей установленных с отсутствием предыдущей информации';
$vbphrase['updated_x_styles'] = 'Обновлено стилей: %1$s';
$vbphrase['no_styles_needed_updating'] = 'Стили не нуждаются в обновлении';
$vbphrase['yes'] = 'Да';
$vbphrase['no'] = 'Нет';

################################
# фразы глобального обновления #
################################
$vbphrase['vbulletin_message'] = 'Сообщение vBulletin';
$vbphrase['create_table'] = 'Создание таблицы %1$s';
$vbphrase['remove_table'] = 'Удаление таблицы %1$s';
$vbphrase['alter_table'] = 'Изменение таблицы %1$s';
$vbphrase['update_table'] = 'Обновление таблицы %1$s';
$vbphrase['upgrade_start_message'] = "<p>Этот скрипт обновит ваш форум до версии <b>" . VERSION . "</b></p>\n<p>Нажмите на кнопку 'Дальше' для продолжения.</p>";
$vbphrase['upgrade_wrong_version'] = "<p>Ваша версия vBulletin не совместима с версией этого скрипта (версия <b>" . PREV_VERSION . "</b>).</p>\n<p>Пожалуйста, убедитесь, что вы запустили верный скрипт.</p>\n<p>Если вы уверены, что скрипт, который вы желаете запустить тот, то <a href=\"" . THIS_SCRIPT . "?step=1\">нажмите здесь</a>.</p>";
$vbphrase['file_not_found'] = 'Ой-ой, ./install/%1$s не существует!';
$vbphrase['importing_file'] = 'Импорт %1$s';
$vbphrase['ok'] = 'Ok';
$vbphrase['query_took'] = 'Запрос на выполнение занял %1$s секунд(ы).';
$vbphrase['done'] = 'Выполнено';
$vbphrase['proceed'] = 'Продолжить';
$vbphrase['reset'] = 'Сбросить';
$vbphrase['alter_table_step_x'] = 'Изменение таблицы %1$s (%2$d из %3$d)';
$vbphrase['vbulletin_copyright'] = 'vBulletin v' . VERSION . '<br />Copyright &copy;2000 - ' . date('Y') . ', vBulletin Solutions, Inc<br /> Перевод сделал zCarot';
$vbphrase['vbulletin_copyright_orig'] = $vbphrase['vbulletin_copyright'];
$vbphrase['processing_complete_proceed'] = 'Обработка завершена - Продолжаем';
$vbphrase['xml_error_x_at_line_y'] = 'Ошибка XML: %1$s в строке %2$s';
$vbphrase['apply_critical_template_change_to_x'] = 'Выполнение важных изменений в шаблоне %1$s стиля %2$s';

###########################
# фразы upgrade_300b3.php #
###########################

$upgrade_phrases['upgrade_300b3.php']['steps'] = array(
        1  => 'создание новых таблиц vBulletin 3',
        2  => 'обновление шаблонов и выполнение изменяющих запросов',
        3  => 'обновление календаря',
        4  => 'изменение таблицы forum',
        5  => 'обновление информации о последнем сообщении на форуме',
        6  => 'обновление личных сообщений',
        7  => 'обновление пользователей',
        8  => 'изменение таблицы user (часть 1)',
        9  => 'изменение таблицы user (часть 2)',
        10 => 'обновление таблицы announcements',
        11 => 'изменение таблиц аватаров/смайлов/иконок',
        12 => 'изменение таблицы attachments',
        13 => 'обновление вложений #1',
        14 => 'обновление вложений #2',
        15 => 'обновление записей о редактировании сообщений',
        16 => 'обновление таблицы тем и сообщений',
        17 => 'обновление сообщений для поддержки просмотра тем',
        18 => 'изменение различных таблиц #1',
        19 => 'изменение различных таблиц #2',
        20 => 'обновление системы BB кодов',
        21 => 'изменение таблицы usergroup',
        22 => 'обновление прав форума',
        23 => 'обновление прав модератора',
        24 => 'вставка групп фраз',
        25 => 'вставка назначенных заданий',
        26 => 'обновление настроек (часть 1)',
        27 => 'обновление настроек (часть 2)',
        28 => 'импортирование vbulletin-language.xml',
        29 => 'импортирование vbulletin-adminhelp.xml',
        30 => 'изменение таблицы Style и удаление таблицы replacementset',
        31 => 'изменение таблицы template',
        32 => 'заполнение репутации пользователей',
        33 => 'создание стилей vB3, основанных на существующих стилях vB2',
        34 => 'перевод замещаемых переменных vB2 в замещаемые переменные, переменные стиля и CSS шаблонов vB3',
        35 => 'перенос старых шаблонов в ссылаемые на них стили',
        36 => 'удаление таблицы излишних стилей и очистка процесса перевода',
        37 => 'импортирование vbulletin-style.xml',
        38 => 'построение информации стиля',
        39 => 'импортирование справки',
        40 => 'проверка на неправильные имена пользователя',
        41 => 'импортирование настроек и очистка',
        42 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);
$upgrade_phrases['upgrade_300b3.php']['tableprefix_not_empty'] = '$config[\'Database\'][\'tableprefix\'] не пустой!';
$upgrade_phrases['upgrade_300b3.php']['tableprefix_not_empty_fix'] = "В файле config.php \$config['Database']['tableprefix'] должен быть пустым для процесса обновления.";
$upgrade_phrases['upgrade_300b3.php']['welcome'] = '<p style="font-size:10pt"><b>Добро пожаловать в vBulletin версии 3.8.</b></p>
        <p>Вы обновляете свой форум, поэтому он был автоматически выключен.</p>
        <p>При нажатии кнопки <b>[Дальше]</b>, начнётся процесс установки форума в вашу базу данных \'<i>%1$s</i>\'.</p>
        <p>Чтобы предотвратить возможные ошибки обозревателя в течение выполнения этого скрипта, мы настоятельно рекомендуем выключить все дополнительные панели вашего обозревателя, такие как панель <b>Google</b>.</p>
        <p>Рекомендуется сделать копию всех записей вашей базы данных до продолжения.<br /><a href="upgrade_300b3.php?step=backup"><b>Нажмите здесь, если вы желаете сделать копию вашей базы данных</b></a>.</p>';
$upgrade_phrases['upgrade_300b3.php']['safe_mode_warning'] = 'PHP запущен в safe mode. В связи с этим мы не можем установить таймаут для ваших скриптов. Это особенно важно, если вы будете делать бекап базы данных и произойдет ошибка.';
$upgrade_phrases['upgrade_300b3.php']['upgrade_already_run'] = 'Мы обнаружили, что вы уже пытались запустить скрипт обновления. Вы не сможете продолжить до тех пор, пока не преобразуете базу данных в vB 2.2.x/2.3.x .';
$upgrade_phrases['upgrade_300b3.php']['moving_maxloggedin_datastore'] = 'Перенос специального шаблона maxloggedin в новую  таблицу datastore';
$upgrade_phrases['upgrade_300b3.php']['new_datastore_values'] = 'Создание новых значений datastore';
$upgrade_phrases['upgrade_300b3.php']['removing_special_templates'] = 'Удаление специальных шаблонов из таблицы template';
$upgrade_phrases['upgrade_300b3.php']['removing_orphan_pms'] = 'Удаляем личные сообщения с отсутствием отправителя';
$upgrade_phrases['upgrade_300b3.php']['rename_calendar_events'] = 'Переименовываем calendar_events в event';
$upgrade_phrases['upgrade_300b3.php']['altering_x_table'] = 'Изменение таблицы %1$s (%2$d из %3$d)';
$upgrade_phrases['upgrade_300b3.php']['droping_event_date'] = 'Удаление даты события из таблицы event';
$upgrade_phrases['upgrade_300b3.php']['changing_subject_to_title'] = 'Изменение "subject" на "title"';
$upgrade_phrases['upgrade_300b3.php']['creating_pub_calendar'] = 'Создание общего календаря';
$upgrade_phrases['upgrade_300b3.php']['creating_priv_calendar'] = 'Создание личного календаря';
$upgrade_phrases['upgrade_300b3.php']['moving_pub_events'] = 'Перемещение общих событий в общий календарь';
$upgrade_phrases['upgrade_300b3.php']['moving_priv_events'] = 'Перемещение личных событий в личный календарь';
$upgrade_phrases['upgrade_300b3.php']['drop_public_field'] = 'Удаление поля public из таблицы event';
$upgrade_phrases['upgrade_300b3.php']['convert_forum_options'] = 'Конвертирование хранения опций форума в bitfield';
$upgrade_phrases['upgrade_300b3.php']['dropping_option_fields'] = 'Удаление полей опций (%1$d из %2$d)';
$upgrade_phrases['upgrade_300b3.php']['resetting_styleids'] = 'Восстановление styleids к обычному стилю форума';
$upgrade_phrases['upgrade_300b3.php']['updating_forum_child_lists'] = 'Обновление списков под-форумов';
$upgrade_phrases['upgrade_300b3.php']['updating_counters_for_x'] = 'Обновление счетчиков для форума <i>%1$s</i>';
$upgrade_phrases['upgrade_300b3.php']['updating_lastpost_info_for_x'] = 'Обновление информации о последнем сообщений для форума <i>%1$s</i>';
$upgrade_phrases['upgrade_300b3.php']['converting_priv_msg_x'] = 'Конвертирование личных сообщений, %1$s';
$upgrade_phrases['upgrade_300b3.php']['insert_priv_msg_txt_from_x'] = 'Вставка текста личных сообщений от <i>%1$s</i>';
$upgrade_phrases['upgrade_300b3.php']['insert_priv_msg_from_x_to_x'] = 'Вставка текста личных сообщений от <i>%1$s</i> к <i>%2$s</i>';
$upgrade_phrases['upgrade_300b3.php']['update_priv_msg_multiple_recip'] = 'Обновление личных сообщений для отображения многочисленных получателей';
$upgrade_phrases['upgrade_300b3.php']['insert_priv_msg_receipts'] = 'Вставка получателей личных сообщений';
$upgrade_phrases['upgrade_300b3.php']['dropping_vb2_pm_table'] = 'Удаление таблицы личных сообщений vBulletin 2';
$upgrade_phrases['upgrade_300b3.php']['alter_user_table_for_vb3_pm'] = 'Изменение таблицы user для поддержки системы личных сообщений vB3';
$upgrade_phrases['upgrade_300b3.php']['alter_user_table_vb3_password'] = 'Изменение таблицы user для поддержки системы паролей vB3';
$upgrade_phrases['upgrade_300b3.php']['priv_msg_import_complete'] = 'Импорт личных сообщений завершен';
$upgrade_phrases['upgrade_300b3.php']['upgrading_users_x'] = 'Обновление пользователей, %1$s';
$upgrade_phrases['upgrade_300b3.php']['found_x_users'] = 'Обнаружено %1$d пользователей для этой группы действий...';
$upgrade_phrases['upgrade_300b3.php']['updating_priv_messages_for_x'] = 'Обновление всех личных сообщений для пользователя <i>%1$s</i>';
$upgrade_phrases['upgrade_300b3.php']['inserting_user_details_usertextfield'] = 'Вставка деталей в таблицу <i>usertextfield</i>';
$upgrade_phrases['upgrade_300b3.php']['user_upgrades_complete'] = 'Обновление пользователей завершено.';
$upgrade_phrases['upgrade_300b3.php']['updating_user_table_options'] = 'Обновление опций таблицы user';
$upgrade_phrases['upgrade_300b3.php']['drop_user_option_fields'] = 'Удаление полей опций user (%1$d из 3)';
$upgrade_phrases['upgrade_300b3.php']['update_access_masks'] = 'Обновление масок доступа пользователей';
$upgrade_phrases['upgrade_300b3.php']['convert_new_birthday_format'] = 'Конвертирование дней рождений в новый формат';
$upgrade_phrases['upgrade_300b3.php']['insert_admin_perms_admin_table'] = 'Вставка прав администратора в таблицу administrator';
$upgrade_phrases['upgrade_300b3.php']['updating_announcements'] = 'Обновление вложений';
$upgrade_phrases['upgrade_300b3.php']['announcement_x'] = 'Вложение: %1$s';
$upgrade_phrases['upgrade_300b3.php']['add_index_avatar_table'] = 'Добавление index в таблицу avatar';
$upgrade_phrases['upgrade_300b3.php']['move_avatars_to_category'] = 'Перемещение аватаров в категорию "Основные аватары"';
$upgrade_phrases['upgrade_300b3.php']['move_icons_to_category'] = 'Перемещение иконок сообщений в категорию "Основные иконки"';
$upgrade_phrases['upgrade_300b3.php']['move_smilies_to_category'] = 'Перемещение смайлов в категорию "Основные смайлы"';
$upgrade_phrases['upgrade_300b3.php']['update_avatars_per_page'] = 'Постраничное обновление аватаров';
$upgrade_phrases['upgrade_300b3.php']['updating_attachments'] = 'Обновление вложений...';
$upgrade_phrases['upgrade_300b3.php']['attachment_x'] = 'Вложение: %1$d';
$upgrade_phrases['upgrade_300b3.php']['remove_orphan_attachments'] = 'Удаление вложений без автора';
$upgrade_phrases['upgrade_300b3.php']['populating_attachmenttype_table'] = 'Заполнение таблицы типов вложений';
$upgrade_phrases['upgrade_300b3.php']['updating_editpost_log'] = 'Обновление записей о редактировании сообщений, %1$s';
$upgrade_phrases['upgrade_300b3.php']['found_x_posts'] = 'Обнаружено %1$d сообщений для этой группы действий...';
$upgrade_phrases['upgrade_300b3.php']['post_x'] = 'Сообщение: %1$d';
$upgrade_phrases['upgrade_300b3.php']['post_editlog_complete'] = 'Обновление записей о редактировании сообщений завершено.';
$upgrade_phrases['upgrade_300b3.php']['steps_may_take_several_minutes'] = 'Помните, что эти действия могут занять <b>несколько минут</b> до завершения при наличии большого числа сообщений в вашей базе данных.';
$upgrade_phrases['upgrade_300b3.php']['altering_post_table'] = 'Изменение таблицы post...';
$upgrade_phrases['upgrade_300b3.php']['altering_thread_table'] = 'Изменение таблицы thread...';
$upgrade_phrases['upgrade_300b3.php']['inserting_moderated_threads'] = 'Вставка проверяемых тем';
$upgrade_phrases['upgrade_300b3.php']['inserting_moderated_posts'] = 'Вставка проверяемых сообщений';
$upgrade_phrases['upgrade_300b3.php']['update_posts_support_threaded'] = 'Обновление сообщений для поддержки просмотра тем, %1$s';
$upgrade_phrases['upgrade_300b3.php']['found_x_threads'] = 'Обнаружен %1$d тем для этой группы действий...';
$upgrade_phrases['upgrade_300b3.php']['threaded_update_complete'] = 'Обновление просмотра тем завершено.';
$upgrade_phrases['upgrade_300b3.php']['emptying_search'] = 'Очистка индексирования поиска';
$upgrade_phrases['upgrade_300b3.php']['emptying_wordlist'] = 'Очистка списка слов';
$upgrade_phrases['upgrade_300b3.php']['remove_bbcodes_hardcoded_now'] = 'Удаление BB кодов, которые теперь считаются слишком сложными ([B], [I], [U], [FONT={option}], [SIZE={option}], [COLOR={option}])';
$upgrade_phrases['upgrade_300b3.php']['inserting_quote_bbcode'] = 'Вставка BB кода [QUOTE=\'<i>Имя пользователя</i>\']----[/QUOTE]';
$upgrade_phrases['upgrade_300b3.php']['select_banned_groups'] = 'Пожалуйста, выберите все группы пользователей, содержащие заблокированных пользователей';
$upgrade_phrases['upgrade_300b3.php']['explain_banned_groups'] = "В vBulletin 3, группы заблокированных пользователей должны быть указаны специфически.<br /><br />\nЕсли у вас есть такие группы, то отметьте их здесь.";
$upgrade_phrases['upgrade_300b3.php']['user_groups'] = 'Группы пользователей:';
$upgrade_phrases['upgrade_300b3.php']['update_some_usergroup_titles'] = 'Обновление названий некоторых групп пользователей';
$upgrade_phrases['upgrade_300b3.php']['updating_usergroup_permissions'] = 'Обновление прав групп пользователей';
$upgrade_phrases['upgrade_300b3.php']['usergroup_x'] = 'Группа пользователей: <i>%1$s</i>';
$upgrade_phrases['upgrade_300b3.php']['updating_usergroups'] = 'Обновление групп пользователей';
$upgrade_phrases['upgrade_300b3.php']['updating_generic_options'] = 'Обновление основных опций групп пользователей';
$upgrade_phrases['upgrade_300b3.php']['updating_usergroup_calendar'] = 'Обновление прав календаря для групп пользователей';
$upgrade_phrases['upgrade_300b3.php']['creating_priv_calendar_perms'] = 'Создание прав личного календаря';
$upgrade_phrases['upgrade_300b3.php']['removing_orhpan_forum_perms'] = 'Удаление прав форума без данного форума';
$upgrade_phrases['upgrade_300b3.php']['backup_forum_perms'] = 'Резервное копирование прав форума';
$upgrade_phrases['upgrade_300b3.php']['drop_old_forumperms'] = 'Удаление таблицы старых прав форума';
$upgrade_phrases['upgrade_300b3.php']['usergroup_x_forum_y'] = 'Группа пользователей: <i>%1$s</i> в форуме <i>%2$s</i>';
$upgrade_phrases['upgrade_300b3.php']['reinsert_forum_perms'] = 'Вставка прав форума в новом формате';
$upgrade_phrases['upgrade_300b3.php']['remove_forum_perms_backup'] = 'Удаление резервной копии прав форума';
$upgrade_phrases['upgrade_300b3.php']['updating_moderator_perms'] = 'Обновление прав модератора';
$upgrade_phrases['upgrade_300b3.php']['moderator_x_forum_y'] = 'Модератор <i>%1$s</i> в разделе <u>%2$s</u>';
$upgrade_phrases['upgrade_300b3.php']['deleted_not_needed'] = 'удаление - больше не требуется.';
$upgrade_phrases['upgrade_300b3.php']['insert_phrase_groups'] = 'Вставка групп фраз';
$upgrade_phrases['upgrade_300b3.php']['inserting_task_x'] = 'Вставка задание %1$d';
$upgrade_phrases['upgrade_300b3.php']['scheduling_x'] = 'Выполнение %1$s';
$upgrade_phrases['upgrade_300b3.php']['update_setting_group_x'] = 'Обновление настроек группы: <i>%1$s</i>';
$upgrade_phrases['upgrade_300b3.php']['update_settings_within_x'] = 'Обновление настроек в группе: <i>%1$s</i>';
$upgrade_phrases['upgrade_300b3.php']['insert_phrases_nonstandard_groups'] = 'Вставка фраз для нестандартных настроек группы';
$upgrade_phrases['upgrade_300b3.php']['insert_phrases_nonstandard_settings'] = 'Вставка фраз для нестандартных настроек';
$upgrade_phrases['upgrade_300b3.php']['saving_your_settings'] = 'Сохранение ваших настроек...';
$upgrade_phrases['upgrade_300b3.php']['building_lang_x'] = 'Построение языка: %1$s';
$upgrade_phrases['upgrade_300b3.php']['language_imported_sucessfully'] = 'Язык импортирован успешно!';
$upgrade_phrases['upgrade_300b3.php']['ahelp_imported_sucessfully'] = 'Помощь администратору импортирована успешно!';
$upgrade_phrases['upgrade_300b3.php']['renaming_style_table'] = 'Переименование таблицы <b>style</b>';
$upgrade_phrases['upgrade_300b3.php']['removing_default_templates'] = 'Удаление ОБЫЧНЫХ шаблонов (они будут замещены позже)';
$upgrade_phrases['upgrade_300b3.php']['updating_template_format'] = 'Обновление шаблонов к новому формату...';
$upgrade_phrases['upgrade_300b3.php']['updating_template_x'] = 'Обновление шаблона: <i>%1$s</i>';
$upgrade_phrases['upgrade_300b3.php']['populating_reputation_levels'] = 'Заполнение таблицы уровней репутации пользователей';
$upgrade_phrases['upgrade_300b3.php']['set_reputation_to_neutral'] = 'Установка всем пользователям нейтральной репутации';
$upgrade_phrases['upgrade_300b3.php']['bbtitle_vb3_style'] = 'Стиль vBulletin 3: %1$s';
$upgrade_phrases['upgrade_300b3.php']['please_read_txt'] = 'Пожалуйста, внимательно прочтите этот текст!';
$upgrade_phrases['upgrade_300b3.php']['replacement_upgrade_desc'] = '<p><b>ЗАМЕЧАНИЕ:</b></p>
                <p>Система попытается перевести ваши замещаемые переменные vBulletin 2 как, например, <b>{firstaltcolor}</b>
                в настройки, чтобы они работали с vBulletin 3. В этом случае ваши обычные замещаемые переменные будут
                переведены в переменные стиля и настройки CSS vBulletin 3.</p>
                <p>Этот скрипт запустится для каждого вашего стиля vBulletin 2 styles и создаст соответствующий новый стиль vBulletin 3.</p>
                <p>Ниже находится список стилей, которые будут переведены с версии vBulletin 2 и должны будут сразу же
                готовы к использованию как стили vBulletin 3.</p>';
$upgrade_phrases['upgrade_300b3.php']['create_vb3_style_x'] = "Создание стиля vBulletin 3: <b>'%1\$s'</b>";
$upgrade_phrases['upgrade_300b3.php']['template_upgrade_desc'] = '<p><b>ЗАМЕЧАНИЕ:</b></p>
                <p>Шаблоны vBulletin 3 существенно отличаются от тех, которые использовались в  vBulletin 2. Поэтому все шаблоны,
                которые вы создали, будут, по существу, бесполезны в системе vBulletin 3.</p>
                <p>Однако, при настройке ваших шаблонов для vBulletin 3, вы можете ссылаться на ваши шаблоны vBulletin 2.
                Поэтому система создаст сейчас отдельные стили, содержащие ваши измененные шаблоны vBulletin 2.</p>
                <p>Эти стили будут <i>бесполезными</i> для vBulletin 3, а создаются они просто для вашего удобства.</p>';
$upgrade_phrases['upgrade_300b3.php']['create_vb2_refernce_style'] = "Создание стиля, ссылающегося на ваши настройки шаблонов <b>'%1\$s'</b>";
$upgrade_phrases['upgrade_300b3.php']['x_old_custom_templates'] = 'Старый измененный шаблон %1$s';
$upgrade_phrases['upgrade_300b3.php']['insert_styles_vb3_table'] = 'Вставка стиля в таблицу стилей vB3';
$upgrade_phrases['upgrade_300b3.php']['updating_style_parent_list'] = 'Обновление списка родительских стилей';
$upgrade_phrases['upgrade_300b3.php']['updating_user_to_new_style'] = 'Обновление пользователей для использования нового стиля';
$upgrade_phrases['upgrade_300b3.php']['settings_imported_sucessfully'] = 'Настройки импортированы успешно!';
$upgrade_phrases['upgrade_300b3.php']['translate_replacement_to_stylevars'] = 'Перевод замещаемых переменных в переменные стиля';
$upgrade_phrases['upgrade_300b3.php']['no_value_to_translate'] = 'нет ни одного значения для перевода в переменные стиля для этого стиля';
$upgrade_phrases['upgrade_300b3.php']['translating_replacement_to_css'] = 'Перевод замещаемых переменных в CSS';
$upgrade_phrases['upgrade_300b3.php']['body_bg_color_x'] = 'цвет фона body: <b>%1$s</b>';
$upgrade_phrases['upgrade_300b3.php']['body_text_color_x'] = 'цвет текста body: <b>%1$s</b>';
$upgrade_phrases['upgrade_300b3.php']['margin_width_x'] = 'ширина поля: <b>%1$s</b>';
$upgrade_phrases['upgrade_300b3.php']['link_color_x'] = 'цвет ссылки: <b>%1$s</b>';
$upgrade_phrases['upgrade_300b3.php']['hover_link_color_x'] = 'цвет нажатой ссылки: <b>%1$s</b>';
$upgrade_phrases['upgrade_300b3.php']['page_bg_color_x'] = 'цвет фона страницы: <b>%1$s</b>';
$upgrade_phrases['upgrade_300b3.php']['page_text_color_x'] = 'цвет текста страницы: <b>%1$s</b>';
$upgrade_phrases['upgrade_300b3.php']['table_border_color_x'] = 'цвет границ таблиц: <b>%1$s</b>';
$upgrade_phrases['upgrade_300b3.php']['category_strip_bg_color'] = 'цвет фона полосы категорий: <b>%1$s</b>';
$upgrade_phrases['upgrade_300b3.php']['category_strip_text_color'] = 'цвет текста полосы категорий: <b>%1$s</b>';
$upgrade_phrases['upgrade_300b3.php']['tbl_head_bg_color_x'] = 'цвет фона заголовка таблиц: <b>%1$s</b>';
$upgrade_phrases['upgrade_300b3.php']['tbl_head_text_color_x'] = 'цвет заголовка таблиц: <b>%1$s</b>';
$upgrade_phrases['upgrade_300b3.php']['first_alt_color_x'] = 'первый альтернативный цвет: <b>%1$s</b>';
$upgrade_phrases['upgrade_300b3.php']['second_alt_color_x'] = 'второй альтернативный цвет: <b>%1$s</b>';
$upgrade_phrases['upgrade_300b3.php']['normal_font_size'] = 'размер нормального шрифта: <b>%1$s</b>';
$upgrade_phrases['upgrade_300b3.php']['normal_font_family'] = 'нормальный шрифт: <b>%1$s</b>';
$upgrade_phrases['upgrade_300b3.php']['normal_font_color'] = 'цвет нормального шрифта: <b>%1$s</b>';
$upgrade_phrases['upgrade_300b3.php']['small_font_size'] = 'размер маленького шрифта: <b>%1$s</b>';
$upgrade_phrases['upgrade_300b3.php']['small_font_family'] = 'маленький шрифт: <b>%1$s</b>';
$upgrade_phrases['upgrade_300b3.php']['small_font_color'] = 'цвет маленького шрифта: <b>%1$s</b>';
$upgrade_phrases['upgrade_300b3.php']['highlight_font_size'] = 'размер выделенного шрифта: <b>%1$s</b>';
$upgrade_phrases['upgrade_300b3.php']['highlight_font_family'] = 'выделенный шрифт: <b>%1$s</b>';
$upgrade_phrases['upgrade_300b3.php']['highlight_font_color'] = 'цвет выделенного шрифтаr: <b>%1$s</b>';
$upgrade_phrases['upgrade_300b3.php']['time_color_x'] = 'цвет времени: <b>%1$s</b>';
$upgrade_phrases['upgrade_300b3.php']['no_replacements_to_translate'] = 'нет ни одного значения для перевода в CSS для этого стиля';
$upgrade_phrases['upgrade_300b3.php']['translating_remaining_replacements'] = 'Перевод остальных замещаемых переменных в замещаемые переменные vB3';
$upgrade_phrases['upgrade_300b3.php']['no_remaining_replacement_vars'] = 'больше не осталось замещаемых переменных для этого стиля';
$upgrade_phrases['upgrade_300b3.php']['translate_vb2_style_settings'] = 'Перевод настроек стиля vB2';
$upgrade_phrases['upgrade_300b3.php']['add_css_headinclude_to_extra'] = 'Добавление данных CSS из шаблона headinclude template для этого стиля в раздел CSS \'extra\'';
$upgrade_phrases['upgrade_300b3.php']['found_css_data'] = 'Найдены и добавлены данные CSS из шаблона headinclude';
$upgrade_phrases['upgrade_300b3.php']['no_css_data_found'] = 'Данные CSS в этом стиле не найдены в шаблоне headinclude';
$upgrade_phrases['upgrade_300b3.php']['no_headinclude_found'] = 'Шаблон headinclude не найден для этого стиля';
$upgrade_phrases['upgrade_300b3.php']['insert_style_settings'] = 'Вставка настроек стиля в базу данных';
$upgrade_phrases['upgrade_300b3.php']['moving_template_x_to_style_x'] = "Перемещение шаблонов из templateset '%1\$s' в стиль '%2\$s'";
$upgrade_phrases['upgrade_300b3.php']['importing_faq_entries'] = 'Импорт справки';
$upgrade_phrases['upgrade_300b3.php']['follow_users_contain_semicolons'] = 'Следующие имена пользователей содержат точки с запятой, и <b>должны</b> быть изменены в панели управления:';
$upgrade_phrases['upgrade_300b3.php']['download_semicolon_users'] = 'Мы рекомендуем использовать <a href="%1$s"><b>эту ссылку</b></a>, чтобы скачать список неправильных имен пользователей для дальнейшего исправления.';
$upgrade_phrases['upgrade_300b3.php']['no_illegal_users_found'] = 'Неправильные имена пользователей не найдены.';
$upgrade_phrases['upgrade_300b3.php']['remove_old_settings_storage'] = 'Удаление сохраненных старых опций стиля';
$upgrade_phrases['upgrade_300b3.php']['salt_admin_x'] = 'Шифрование пароля для администратора: <b>%1$s</b>';
$upgrade_phrases['upgrade_300b3.php']['build_forum_and_usergroup_cache'] = 'Построение форума и кэша групп пользователей... ';
$upgrade_phrases['upgrade_300b3.php']['upgrade_complete'] = "Вы только что успешно обновили форум до vBulletin 3.<br />
<br />
        <font size=\"+1\"><b>ВЫ ДОЛЖНЫ УДАЛИТЬ СЛЕДУЮЩИЕ ФАЙЛЫ ДО ТОГО, КАК ПРОДОЛЖИТЬ:</b><br />
        install/install.php<br />
        install/init.php</font><br />
        <br />
        После того как вы это сделаете, нажмите кнопку 'Продолжить' для продолжения.<br />
        <br />
        Помните, что ваш форум сейчас <b>выключен</b><br />
        <br />
        <b>Вам необходимо перестроить ваш индекс поиска и статистику, как только вы попадете в панель управления.</b><br />
        <br />
        <b>Замечание: Обновление еще не завершено. Вы сейчас используете версию " . VERSION . ".
        Нажмите '<i>Продолжить</i>' для продолжения обновления до новейшей версии.</b>";

$upgrade_phrases['upgrade_300b3.php']['public'] = 'Общий';
$upgrade_phrases['upgrade_300b3.php']['public_calendar'] = 'Общий календарь';
$upgrade_phrases['upgrade_300b3.php']['private'] = 'Личный';
$upgrade_phrases['upgrade_300b3.php']['private_calendar'] = 'Личный календарь';
$upgrade_phrases['upgrade_300b3.php']['deleted_user'] = 'Удаленный пользователь';
$upgrade_phrases['upgrade_300b3.php']['standard_avatars'] = 'Основные аватары';
$upgrade_phrases['upgrade_300b3.php']['standard_icons'] = 'Основные смайлы';
$upgrade_phrases['upgrade_300b3.php']['standard_smilies'] = 'Основные иконки';
$upgrade_phrases['upgrade_300b3.php']['avatar_setting_title'] = 'Аватаров на страницу';
$upgrade_phrases['upgrade_300b3.php']['avatar_setting_desc'] = 'Как много аватаров вы желаете отображать на странице в \\\'Редактирование аватаров\\\' и в редакторе профиля?';
$upgrade_phrases['upgrade_300b3.php']['registered_user'] = 'Зарегистрированный';
// должно соответствовать содержимому vbulletin-language.xml
$upgrade_phrases['upgrade_300b3.php']['master_language_title'] = 'English (EN)';
$upgrade_phrases['upgrade_300b3.php']['master_language_langcode'] = 'en';
$upgrade_phrases['upgrade_300b3.php']['master_language_charset'] = 'UTF-8';
$upgrade_phrases['upgrade_300b3.php']['master_language_decimalsep'] = '.';
$upgrade_phrases['upgrade_300b3.php']['master_language_thousandsep'] = ',';
$upgrade_phrases['upgrade_300b3.php']['master_language_just_created'] = 'Создание русского (RU) языка';
$upgrade_phrases['upgrade_300b3.php']['settinggroups'] = array(
                'Turn Your vBulletin on and off' => 'onoff',
                'General Settings' => 'general',
                'Contact Details' => 'contact',
                'Posting Code allowances (vB code / HTML / etc)' => 'postingallow',
                'Forums Home Page Options' => 'forumhome',
                'User and registration options' => 'user',
                'Memberlist options' => 'memberlist',
                'Thread display options' => 'showthread',
                'Forum Display Options' => 'forumdisplay',
                'Search Options' => 'search',
                'Email Options' => 'email',
                'Date / Time options' => 'datetime',
                'Edit Options' => 'editpost',
                'IP Logging Options' => 'ip',
                'Floodcheck Options' => 'floodcheck',
                'Banning Options' => 'banning',
                'Private Messaging Options' => 'pm',
                'Censorship Options' => 'censor',
                'HTTP Headers and output' => 'http',
                'Version Info' => 'version',
                'Templates' => 'templates',
                'Load limiting options' => 'loadlimit',
                'Polls' => 'poll',
                'Avatars' => 'avatar',
                'Attachments' => 'attachment',
                'Custom User Titles' => 'usertitle',
                'Upload Options' => 'upload',
                'Who\'s Online' => 'online',
                'Language Options' => 'OLDlanguage',
                'Spell Check' => 'OLDspellcheck',
                'Calendar' => 'OLDcalendar'
        );
$upgrade_phrases['upgrade_300b3.php']['vb2_default_style_title'] = 'Default';
$upgrade_phrases['upgrade_300b3.php']['new_vb2_default_style_title'] = 'Обычный vBulletin 2';
$upgrade_phrases['upgrade_300b3.php']['cron_birthday'] = 'Поздравление по электронной почте с Днём Рождения';
$upgrade_phrases['upgrade_300b3.php']['cron_thread_views'] = 'Просмотр тем';
$upgrade_phrases['upgrade_300b3.php']['cron_user_promo'] = 'Продвижение пользователей';
$upgrade_phrases['upgrade_300b3.php']['cron_daily_digest'] = 'Обзор дня';
$upgrade_phrases['upgrade_300b3.php']['cron_weekly_digest'] = 'Обзор недели';
$upgrade_phrases['upgrade_300b3.php']['cron_activation'] = 'Напоминание об активации по электронной почте';
$upgrade_phrases['upgrade_300b3.php']['cron_subscriptions'] = 'Подписки';
$upgrade_phrases['upgrade_300b3.php']['cron_hourly_cleanup'] = 'Ежечасная очистка #1';
$upgrade_phrases['upgrade_300b3.php']['cron_hourly_cleaup2'] = 'Ежечасная очистка #2';
$upgrade_phrases['upgrade_300b3.php']['cron_attachment_views'] = 'Просмотр вложений';
$upgrade_phrases['upgrade_300b3.php']['cron_unban_users'] = 'Восстановление временно заблокированных участников';
$upgrade_phrases['upgrade_300b3.php']['cron_stats_log'] = 'Записи статистики за день';
$upgrade_phrases['upgrade_300b3.php']['reputation_-999999'] = 'презирают в этих краях';
$upgrade_phrases['upgrade_300b3.php']['reputation_-50'] = 'может только надеется на улучшение';
$upgrade_phrases['upgrade_300b3.php']['reputation_-10'] = 'имеет немного плохого в прошлом';
$upgrade_phrases['upgrade_300b3.php']['reputation_0'] = 'пока не определено';
$upgrade_phrases['upgrade_300b3.php']['reputation_10'] = 'на пути к лучшему';
$upgrade_phrases['upgrade_300b3.php']['reputation_50'] = 'скоро придёт к известности';
$upgrade_phrases['upgrade_300b3.php']['reputation_150'] = '- весьма и весьма положительная личность';
$upgrade_phrases['upgrade_300b3.php']['reputation_250'] = 'как роза среди колючек';
$upgrade_phrases['upgrade_300b3.php']['reputation_350'] = '- очень-очень хороший человек';
$upgrade_phrases['upgrade_300b3.php']['reputation_450'] = '- луч света в тёмном царстве';
$upgrade_phrases['upgrade_300b3.php']['reputation_550'] = '- это имя известно всем';
$upgrade_phrases['upgrade_300b3.php']['reputation_650'] = '- просто великолепная личность';
$upgrade_phrases['upgrade_300b3.php']['reputation_1500'] = 'за этого человека можно гордиться';
$upgrade_phrases['upgrade_300b3.php']['reputation_1000'] = 'обеспечил(а) себе прекрасное будущее';
$upgrade_phrases['upgrade_300b3.php']['reputation_2000'] = 'репутация неоспорима';
$upgrade_phrases['upgrade_300b3.php']['upgrade_from_vb2_not_supported'] = '
	<p>Невозможно напрямую обновить форум с версии 2.х.х до 3.8.х и более старших из-за несовместимости в структуре базы данных.</p>
	<p>Если у вас на данный момент vBulletin 2 и вы желаете обновиться до самой новой версии vBulletin, то, пожалуйста, скачайте vBulletin 3.6.</p>
	<p>Распакуйте файлы из скачанного архива и загрузите их на сервер, с заменой уже загруженных файлов. После этого запустите процесс обновления.</p>
	<p>После завершения процесса обновления до 3.6.x, скачайте самую новую версию vBulletin и загрузите её на сервер с заменой существующих файлов. После этого опять запустите процесс обновления.</p>
';

###########################
# фразы upgrade_300b4.php #
###########################

$upgrade_phrases['upgrade_300b4.php.php']['steps'] = array(
        1 => 'изменение схемы базы данных',
        2 => 'добавление и удаление данных',
        3 => 'обновление типа кэша вложений',
        4 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);
$upgrade_phrases['upgrade_300b4.php.php']['default_avatar_category'] = 'Вставка обычной категории аватаров';
$upgrade_phrases['upgrade_300b4.php.php']['insert_into_whosonline'] = "Вставка кэша поисковых машин Кто на форуме в %1$sdatastore";
$upgrade_phrases['upgrade_300b4.php.php']['delete_redundant_cron'] = 'Удаление лишнего назначенного задания';
$upgrade_phrases['upgrade_300b4.php.php']['attachment_cache_rebuilt'] = 'Перестройка кэша вложений';
$upgrade_phrases['upgrade_300b4.php.php']['generic_avatars'] = 'Основные аватары';

###########################
# фразы upgrade_300b5.php #
###########################

$upgrade_phrases['upgrade_300b5.php.php']['steps'] = array(
        1 => 'изменение схемы базы данных',
        2 => 'изменение таблиц thread / post',
        3 => 'изменение настроек',
        4 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);
$upgrade_phrases['upgrade_300b5.php.php']['alter_post_title'] = 'Изменения названия колонки %1$spost до VARCHAR(250)<br /><i>(это может занять несколько минут для больших форумов)';
$upgrade_phrases['upgrade_300b5.php.php']['alter_thread_title'] = 'Изменения названия колонки %1$sthread до VARCHAR(250)<br /><i>(это может занять несколько минут для больших форумов)';
$upgrade_phrases['upgrade_300b5.php.php']['disabled_timeout_admin'] = 'Успешное выключение настройки "Таймаут сессии администратора".';
$upgrade_phrases['upgrade_300b5.php.php']['timeout_admin_not_changed'] = 'Настройка "Таймаут сессии администратора" не изменена.';
$upgrade_phrases['upgrade_300b5.php.php']['change_setting_value'] = 'Изменить значение настройки?';
$upgrade_phrases['upgrade_300b5.php.php']['proceed'] = ' Дальше ';
$upgrade_phrases['upgrade_300b5.php.php']['setting_info'] = '<b>Выключить настройку "Таймаут сессии администратора"?</b> ' .
                                        '<dfn>Эта опция добавляет безопасности, но может затруднить вход в панель управления администратора.<br />' .
                                        'Если у вас были проблемы с входом в панель управления или вы используете прокси ' .
                                        '(как прокси серверы AOL\'а), то вам следует выключить эту опцию (выберите "Да").</dfn>';
$upgrade_phrases['upgrade_300b5.php.php']['no_change_needed'] = 'Определение следует ли изменять значение ... Нет, это не нужно.';

###########################
# фразы upgrade_300b6.php #
###########################

$upgrade_phrases['upgrade_300b6.php.php']['steps'] = array(
        1 => 'добавление index в таблицу thread',
        2 => 'изменение схемы базы данных',
        3 => 'изменение данных подписки',
        4 => 'переименование некоторых шаблонов и смайлов для нового редактора WYSIWYG',
        5 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_300b6.php.php']['alter_thread_table'] = 'Изменение таблицы %1$sthread ...<br /><i>(это может занять несколько минут для больших форумов)';
$upgrade_phrases['upgrade_300b6.php.php']['remove_avatar_cache'] = 'Удаление кэша аватаров';
$upgrade_phrases['upgrade_300b6.php.php']['update_userban'] = 'Обновление ежечасного снятия блокировки для запуска в 15 минут каждый час (т.е. время должно быть xx:15)';
$upgrade_phrases['upgrade_300b6.php.php']['subscription_active'] = 'Установка активных подписок';
$upgrade_phrases['upgrade_300b6.php.php']['rename_old_template'] = 'Переименование шаблона <i>%1$s</i> в <i>%2$s</i>';
$upgrade_phrases['upgrade_300b6.php.php']['delete_vbcode_color'] = 'Удаление шаблона <i>vbcode_color_options</i> (цвета теперь задаются в файле clientscript/vbulletin_editor.js)';
$upgrade_phrases['upgrade_300b6.php.php']['smilie_fixes'] = "Улучшение названий смайлов)";

###########################
# фразы upgrade_300b7.php #
###########################

$upgrade_phrases['upgrade_300b7.php.php']['steps'] = array(
        1 => 'различные обновления #1',
        2 => 'изменение таблицы user',
        3 => 'важное замечание об изменениях bb кодов',
        4 => 'изменение системы bb кодов',
        5 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_300b7.php.php']['redundant_stylevars'] = "Удаление лишних переменных стиля из таблицы " . TABLE_PREFIX . "template";
$upgrade_phrases['upgrade_300b7.php.php']['renaming_some_templates'] = 'Переименование некоторых шаблонов';
$upgrade_phrases['upgrade_300b7.php.php']['ban_removal_fix'] = 'Обновление ежечасного снятия блокировки для запуска в 15 минут каждый час (т.е. время должно быть xx:15). Исправление ошибок предыдущего скрипта';
$upgrade_phrases['upgrade_300b7.php.php']['promotion_lastrun_fix'] = 'Восстановление времени последнего запуска для возврата продвижений до 8-го сентября для процесса восстановления';
$upgrade_phrases['upgrade_300b7.php.php']['default_charset'] = 'Установка обычного charset для языка на <i>ISO-8859-1</i>.';
$upgrade_phrases['upgrade_300b7.php.php']['comma_var_names'] = 'Удаление лишних запятых в фразах имен переменных';
$upgrade_phrases['upgrade_300b7.php.php']['delete_quote_email_bbcode'] = 'Удаление BB кодов [quote] и [email] - они слишком сложны';
$upgrade_phrases['upgrade_300b7.php.php']['bbcode_update'] = "<h3>Важное замечание...</h3>" .
         "<p>Следующее действие этого скрипта обновления <b>удалит</b> определения BB кодов <i>[quote]</i>, <i>[quote=Имя_Пользователя]</i>, <i>[email]</i>, и <i>[email=адрес]</i>, так как они теперь считаются слишком сложно написанными и управляются шаблоном.</p>" .
         "<p>Если вы сделали страницу HTML, генерирующую эти теги, то мы предлагаем посетить <a href=\"../$admincpdir/bbcode.php?$session[sessionurl]\" target=\"_blank\" title=\"Открыть управление BB кодами в новом окне\">Управление BB кодами</a> и убедится, в значениях вашего HTML.</p>" .
         "<p>Когда скрипт завершится, вы сможете изменить шаблон <b>bbcode_quote</b>, чтобы получить те же результаты, что вы и сделали самостоятельно.</p>" .
         "<p>Нажмите кнопку 'Дальше' для перехода к следующему действию скрипта обновления.</p>";

##########################
# фразы upgrade_300g.php #
##########################

$upgrade_phrases['upgrade6.php']['steps'] = array(
        1 => 'различные обновления #1',
        2 => 'изменения языка и фраз',
        3 => 'платные подписки',
        4 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade6.php']['remove_duplicate_templates'] = 'Удаление дублированных шаблонов в стиле ...';
$upgrade_phrases['upgrade6.php']['done'] = 'Выполнено!';
$upgrade_phrases['upgrade6.php']['rename_searchindex_postindex'] = "Переименование таблицы " . TABLE_PREFIX . "searchindex в " . TABLE_PREFIX . "postindex";
$upgrade_phrases['upgrade6.php']['removing_redundant_index_phrase'] = "Удаление лишнего index в " . TABLE_PREFIX . "phrase";
$upgrade_phrases['upgrade6.php']['holiday_to_phrasetype'] = "Добавление праздника в " . TABLE_PREFIX . "phrasetype table";
$upgrade_phrases['upgrade6.php']['moving_holiday_type'] = "Перемещение существующих праздников в их новый phrasetype";
$upgrade_phrases['upgrade6.php']['adding_x_to_phrasetype'] = 'Добавление %1$s в таблицу ' . TABLE_PREFIX . 'phrasetype';
$upgrade_phrases['upgrade6.php']['update_invalid_birthdays'] = "Обновление существующих дней рождений";
$upgrade_phrases['upgrade6.php']['step_already_run'] = 'Действие уже запущено.';
$upgrade_phrases['upgrade6.php']['updating_subscription_expiry_times'] = 'Обновление продолжительности подписок.';

############################
# фразы upgrade_300rc1.php #
############################

$upgrade_phrases['upgrade_300rc1.php']['steps'] = array(
        1 => 'различные обновления #1',
        2 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_300rc1.php']['alter_reputation_negative'] = "Изменение продвижения репутации для разрешения негативных записей";
$upgrade_phrases['upgrade_300rc1.php']['phrase_varname_case_sens'] = "Создание регистрозависимых фраз имен переменных";
$upgrade_phrases['upgrade_300rc1.php']['add_faq_entry'] = 'Добавление справки';

############################
# фразы upgrade_300rc2.php #
############################

$upgrade_phrases['upgrade_300rc2.php']['steps'] = array(
        1 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

############################
# фразы upgrade_300rc3.php #
############################

$upgrade_phrases['upgrade_300rc3.php']['steps'] = array(
        1 => 'исправление некоторых ошибок таблиц',
        2 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_300rc3.php']['click_here_auto_redirect'] = 'Нажмите здесь, если вас не переадресовали.';
$upgrade_phrases['upgrade_300rc3.php']['not_latest_files'] = 'Вы загрузили не все новейшие файлы!<br /><br /><b>Пожалуйста, загрузите последнюю версию adminfunctions_profilefield.php в директорию includes и обновите страницу.</b>';
$upgrade_phrases['upgrade_300rc3.php']['fix_sortorder'] = "Исправление поля sortorder в таблице " . TABLE_PREFIX . "search";
$upgrade_phrases['upgrade_300rc3.php']['fix_logdateoverride'] = "Исправление поля logdateoverride в таблице " . TABLE_PREFIX . "language";
$upgrade_phrases['upgrade_300rc3.php']['fix_filesize_customavatar'] = "Добавление поля filesize в таблицу " . TABLE_PREFIX . "customavatar";
$upgrade_phrases['upgrade_300rc3.php']['fix_filesize_customprofile'] = "Добавление поля filesize в таблицу " . TABLE_PREFIX . "customprofilepic";
$upgrade_phrases['upgrade_300rc3.php']['populate_avatar_filesize'] = 'Заполнение полей filesize аватаров.';
$upgrade_phrases['upgrade_300rc3.php']['populate_profile_filesize'] = 'Заполнение полей filesize фотографий.';

############################
# фразы upgrade_300rc4.php #
############################

$upgrade_phrases['upgrade_300rc4.php']['steps'] = array(
        1 => 'исправление некоторых ошибок таблиц',
        2 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_300rc4.php']['increase_storage_dateoverride'] = "Увеличение размера поля dateoverride в таблице " . TABLE_PREFIX . "language";
$upgrade_phrases['upgrade_300rc4.php']['increase_storage_timeoverride'] = "Увеличение размера поля timeoverride в таблице " . TABLE_PREFIX . "language";
$upgrade_phrases['upgrade_300rc4.php']['increase_storage_registereddateoverride'] = "Увеличение размера поля registereddateoverride в таблице " . TABLE_PREFIX . "language";
$upgrade_phrases['upgrade_300rc4.php']['increase_storage_calformat1override'] = "Увеличение размера поля calformat1override в таблице " . TABLE_PREFIX . "language";
$upgrade_phrases['upgrade_300rc4.php']['increase_storage_calformat2override'] = "Увеличение размера поля calformat2override в таблице " . TABLE_PREFIX . "language";
$upgrade_phrases['upgrade_300rc4.php']['increase_storage_logdateoverride'] = "Увеличение размера поля logdateoverride в таблице " . TABLE_PREFIX . "language";
$upgrade_phrases['upgrade_300rc4.php']['adding_calendar_mardi_gras'] = "Изменение таблицы " . TABLE_PREFIX . "calendar для поддержки новых предопределенных праздников: Mardi Gras и Corpus Christi. Вам следует включить эти праздники в управлении календарями.";

########################
# фразы upgrade300.php #
########################

$upgrade_phrases['upgrade_300.php']['steps'] = array(
        1 => 'исправление некоторых ошибок таблиц',
        2 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_300.php']['make_reputation_signed'] = 'Изменения типа колонки reputation на signed integer';
$upgrade_phrases['upgrade_300.php']['add_birthday_search'] = 'Добавление поля поиска дней рождений';
$upgrade_phrases['upgrade_300.php']['add_index_birthday_search'] = 'Добавление index в поле поиска дней рождений';
$upgrade_phrases['upgrade_300.php']['populate_birhtday_search'] = 'Заполнение поля поиска дней рождений';

#########################
# фразы upgrade_301.php #
#########################

$upgrade_phrases['upgrade_301.php']['steps'] = array(
        1 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

#########################
# фразы upgrade_302.php #
#########################

$upgrade_phrases['upgrade_302.php']['steps'] = array(
        1 => 'разнообразные изменения таблиц 1/4',
        2 => 'разнообразные изменения таблиц 2/4',
        3 => 'разнообразные изменения таблиц 3/4',
        4 => 'разнообразные изменения таблиц 4/4',
        5 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_302.php']['drop_pmpermissions'] = 'Удаление поля redundant';
$upgrade_phrases['upgrade_302.php']['add_thumbnail_filesize'] = 'Добавление поля размера миниатюр для таблицы attachment';
$upgrade_phrases['upgrade_302.php']['change_profilefield'] = 'Увеличение поля статуса пользователя';
$upgrade_phrases['upgrade_302.php']['update_genericpermissions'] = 'Добавление основных прав \'Может видеть скрытые поля профиля\'';
$upgrade_phrases['upgrade_302.php']['alter_poll_table'] = 'Добавление поля lastvote в ' . TABLE_PREFIX . 'poll';
$upgrade_phrases['upgrade_302.php']['alter_user_table'] = 'Добавление поддержки длинных адресов электронной почты в ' . TABLE_PREFIX . 'user';
$upgrade_phrases['upgrade_302.php']['add_rss_faq'] = 'Добавление записи о RSS в ' . TABLE_PREFIX . 'faq';
$upgrade_phrases['upgrade_302.php']['add_notes'] = 'Добавление поля notes в ' . TABLE_PREFIX . 'administrator';
$upgrade_phrases['upgrade_302.php']['add_cpsession_table'] = 'Добавление таблицы cpsession';
$upgrade_phrases['upgrade_302.php']['fix_blank_charset'] = 'Добавление обычного charset';

#########################
# фразы upgrade_303.php #
#########################

$upgrade_phrases['upgrade_303.php']['steps'] = array(
        1 => 'разнообразные изменения таблиц 1/1',
        2 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_303.php']['note'] = '<p>Если у вас включены миниатюры, вы храните вложения в файлах и не перестроили миниатюры после обновления до 3.0.2, то сделайте это после обновления, так как ваши миниатюры не будут работать пока вы это не сделаете.</p>';
$upgrade_phrases['upgrade_303.php']['rebuild_usergroupcache'] = 'Перестройка Usergroupcache';

################################
# фразы upgrade_304 - 3015.php #
################################

$upgrade_phrases['upgrade_304.php']['steps'] =& $upgrade_phrases['upgrade_301.php']['steps'];
$upgrade_phrases['upgrade_305.php']['steps'] =& $upgrade_phrases['upgrade_301.php']['steps'];
$upgrade_phrases['upgrade_306.php']['steps'] =& $upgrade_phrases['upgrade_301.php']['steps'];

$upgrade_phrases['upgrade_307.php']['steps'] =& $upgrade_phrases['upgrade_301.php']['steps'];
$upgrade_phrases['upgrade_307.php']['update_calendarpermissions'] = 'Изменение прав календаря';
$upgrade_phrases['upgrade_307.php']['import_birthdaydatecut_option'] = 'Импортирование старого datecut дней рождений';

$upgrade_phrases['upgrade_308.php']['steps'] =& $upgrade_phrases['upgrade_303.php']['steps'];
$upgrade_phrases['upgrade_309.php']['steps'] =& $upgrade_phrases['upgrade_301.php']['steps'];
$upgrade_phrases['upgrade_3010.php']['steps'] =& $upgrade_phrases['upgrade_301.php']['steps'];
$upgrade_phrases['upgrade_3011.php']['steps'] =& $upgrade_phrases['upgrade_301.php']['steps'];
$upgrade_phrases['upgrade_3012.php']['steps'] =& $upgrade_phrases['upgrade_301.php']['steps'];
$upgrade_phrases['upgrade_3013.php']['steps'] =& $upgrade_phrases['upgrade_301.php']['steps'];
$upgrade_phrases['upgrade_3014.php']['steps'] =& $upgrade_phrases['upgrade_301.php']['steps'];
$upgrade_phrases['upgrade_3015.php']['steps'] =& $upgrade_phrases['upgrade_301.php']['steps'];

###########################
# фразы upgrade_350b1.php #
###########################

$upgrade_phrases['upgrade_350b1.php']['steps'] = array(
	1 => 'разнообразные изменения таблиц 1/6',
	2 => 'разнообразные изменения таблиц 2/6',
	3 => 'разнообразные изменения таблиц 3/6',
	4 => 'разнообразные изменения таблиц 4/6',
	5 => 'разнообразные изменения таблиц 5/6',
	6 => 'разнообразные изменения таблиц 6/6',
	7 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_350b1.php']['update_forumpermissions'] = 'Изменение прав разделов';
$upgrade_phrases['upgrade_350b1.php']['support_multiple_products'] = 'Добавление поддержки множества продуктов';
$upgrade_phrases['upgrade_350b1.php']['update_adminpermissions'] = 'Изменение прав администратора';
$upgrade_phrases['upgrade_350b1.php']['cron_event_reminder'] = 'Напоминание события';

###########################
# фразы upgrade_350b2.php #
###########################

$upgrade_phrases['upgrade_350b2.php']['steps'] = array(
	1 => 'разнообразные изменения таблиц  1/1',
	2 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

###########################
# фразы upgrade_350b3.php #
###########################

$upgrade_phrases['upgrade_350b3.php']['steps'] = array(
	1 => 'конвертирование различных настроек',
	2 => 'обновление системы платежей',
	3 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_350b3.php']['translating_allowvbcodebuttons'] = 'Конвертирование $vbulletin->options[\'allowvbcodebuttons\'] и $vbulletin->options[\'quickreply\'] в новый интегрированный параметр $vbulletin->options[\'editormodes\']';
$upgrade_phrases['upgrade_350b3.php']['translating_quickreply'] = 'Конвертирование $vbulletin->options[\'quickreply\'] и $vbulletin->options[\'quickreplyclick\'] в новый интегрированный параметр $vbulletin->options[\'quickreply\'].';
$upgrade_phrases['upgrade_350b3.php']['converting_phrases_x_of_y'] = 'Конвертирование фраз (шаг %1$d из %2$d)';
$upgrade_phrases['upgrade_350b3.php']['paymentapi_data'] = 'Вставка данных по умолчанию о платежных системах';
$upgrade_phrases['upgrade_350b1.php']['note'] = '<p><h3>Вы храните аватары в файловой системе.  vB 3.5.0 теперь поддерживает хранение фотографий в файловой системе, причём используются раздельные настройки для обоих типов изображений.  После окончания обновления вам следует зайти в панель администратора и переместить все ваши аватары обратно в базу данных с помощью <em>Аватары->Тип хранения изображений</em>. После завершения процесса пройдите опять туда же, и переместите изображения обратно в файловую систему.  Теперь и аватары и фотографии хранятся в файловой системе. Если вы не сделаете это, то фотографии ваших пользователей не будут отображаться.</h3></p>';

###########################
# фразы upgrade_350b4.php #
###########################

$upgrade_phrases['upgrade_350b4.php']['steps'] = array(
	1 => 'разнообразные изменения таблиц 1/1',
	2 => 'обновление системы платежей',
	3 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_350b4.php']['adding_payment_api_x_settings'] = 'Добавление опций платежной системы %1$s';
$upgrade_phrases['upgrade_350b4.php']['invert_moderate_permission'] = 'Инвертирование разрешения \'Всегда проверять сообщения этой группы\'';

############################
# фразы upgrade_350rc1.php #
############################

$upgrade_phrases['upgrade_350rc1.php']['steps'] = array(
	1 => 'установка системы управления продуктами',
	2 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_350rc1.php']['control_panel_hook_support'] = 'Добавление поддержки хаков в контрольной панели';

############################
# фразы upgrade_350rc2.php #
############################

$upgrade_phrases['upgrade_350rc2.php']['steps'] = array(
	1 => 'обновление полей product',
	2 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

############################
# фразы upgrade_350rc3.php #
############################

$upgrade_phrases['upgrade_350rc3.php']['steps'] = array(
	1 => 'разнообразные изменения таблиц',
	2 => 'изменения таблицы thread',
	3 => 'изменения таблицы post',
	4 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_350rc3.php']['please_wait_message'] = 'Следующий шаг может занять много времени. Будьте терпеливы.';
$upgrade_phrases['upgrade_350rc3.php']['updating_payment_api_x_settings'] = 'Обновление настроек системы оплаты %1$s';

#########################
# фразы upgrade_350.php #
#########################

$upgrade_phrases['upgrade_350.php']['steps'] = array(
	1 => 'разнообразные изменения таблиц',
	2 => 'изменения таблицы thread',
	3 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

#########################
# фразы upgrade_351.php #
#########################

$upgrade_phrases['upgrade_351.php']['steps'] = array(
    1 => 'разнообразные изменения таблиц',
    2 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_351.php']['delete_vb_product'] = 'Удаление vBulletin из таблицы product.';

#########################
# фразы upgrade_352.php #
#########################

$upgrade_phrases['upgrade_352.php']['steps'] =& $upgrade_phrases['upgrade_351.php']['steps'];

$upgrade_phrases['upgrade_352.php']['adding_skype_field'] = 'Добавление поля Skype в таблицу user';

#########################
# фразы upgrade_353.php #
#########################

$upgrade_phrases['upgrade_353.php']['steps'] =& $upgrade_phrases['upgrade_351.php']['steps'];

$upgrade_phrases['upgrade_353.php']['remove_352_xss_plugin'] = 'Удаляем модуль-исправление XSS для 3.5.2, если установлен';

###############################
# фразы upgrade_354 - 355.php #
###############################

$upgrade_phrases['upgrade_354.php']['steps'] =& $upgrade_phrases['upgrade_351.php']['steps'];
$upgrade_phrases['upgrade_355.php']['steps'] =& $upgrade_phrases['upgrade_301.php']['steps'];

###########################
# фразы upgrade_360b1.php #
###########################

$upgrade_phrases['upgrade_360b1.php']['steps'] = array(
	1 => 'большие изменения таблиц (1/2)',
	2 => 'большие изменения таблиц (2/2)',
	3 => 'разнообразные изменения таблиц',
	4 => 'разнообразные изменения таблиц',
	5 => 'разнообразные изменения таблиц',
	6 => 'разнообразные изменения таблиц',
	7 => 'права подписей',
	8 => 'нарушения',
	9 => 'обновления платных подписок',
	10 => 'обновления прав супер-модераторов',
	11 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_360b1.php']['please_wait_message'] = 'Следующий шаг может занять много времени. Будьте терпеливы. Если же вы уверены, что ничего не происходит, то можете обновить страницу.';

$upgrade_phrases['upgrade_360b1.php']['updating_cron'] = 'Обновление назначенных заданий';
$upgrade_phrases['upgrade_360b1.php']['updating_subscriptions'] = 'Обновление подписок';
$upgrade_phrases['upgrade_360b1.php']['updating_holidays'] = 'Обновление праздников';
$upgrade_phrases['upgrade_360b1.php']['updating_profilefields'] = 'Обновление других полей профиля';
$upgrade_phrases['upgrade_360b1.php']['updating_reputationlevels'] = 'Обновление уровней репутации';
$upgrade_phrases['upgrade_360b1.php']['invert_banned_flag'] = "Инвертирование отметки 'Это группа 'Заблокированных' '";
$upgrade_phrases['upgrade_360b1.php']['install_canignorequota_permission'] = "Установка разрешения 'Может игнорировать предел'";
$upgrade_phrases['upgrade_360b1.php']['infractionlevel1_title'] = 'Реклама и спам';
$upgrade_phrases['upgrade_360b1.php']['infractionlevel2_title'] = 'Оскорбление других участников';
$upgrade_phrases['upgrade_360b1.php']['infractionlevel3_title'] = 'Нарушение правил форума';
$upgrade_phrases['upgrade_360b1.php']['infractionlevel4_title'] = 'Матерная речь';
$upgrade_phrases['upgrade_360b1.php']['rssposter_title'] = 'Размещение RSS';
$upgrade_phrases['upgrade_360b1.php']['infractions_title'] = 'Очистка нарушений';
$upgrade_phrases['upgrade_360b1.php']['ccbill_title'] = 'Проверка возврата счёта по кредитной карте';

$upgrade_phrases['upgrade_360b1.php']['rename_post_parsed'] = "Переименование таблицы " . TABLE_PREFIX . "post_parsed на " . TABLE_PREFIX . "postparsed";

$upgrade_phrases['upgrade_360b1.php']['updating_thread_redirects'] = 'Обновление перенаправлений тем';

$upgrade_phrases['upgrade_360b1.php']['super_moderator_x_updated'] = 'Супер-модератор \'%1$s\' обновлён';

$upgrade_phrases['upgrade_360b1.php']['lastpostid_notice'] = '<strong>Чтобы окончательно завершить обновление, то есть обновить информацию о темах и разделах, вы <em>должны</em> самостоятельно обновить счётчики в панели администратора.</strong>';

###########################
# фразы upgrade_360b2.php #
###########################

$upgrade_phrases['upgrade_360b2.php']['steps'] = array(
	1 => 'разнообразные изменения таблиц',
	2 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

###########################
# фразы upgrade_360b3.php #
###########################

$upgrade_phrases['upgrade_360b3.php']['steps'] = array(
	1 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);


$upgrade_phrases['upgrade_360b4.php']['steps'] =& $upgrade_phrases['upgrade_360b2.php']['steps'];
$upgrade_phrases['upgrade_360rc1.php']['steps'] =& $upgrade_phrases['upgrade_360b2.php']['steps'];
$upgrade_phrases['upgrade_360rc2.php']['steps'] =& $upgrade_phrases['upgrade_360b2.php']['steps'];
$upgrade_phrases['upgrade_360rc3.php']['steps'] =& $upgrade_phrases['upgrade_360b2.php']['steps'];
$upgrade_phrases['upgrade_360.php']['steps'] =& $upgrade_phrases['upgrade_360b2.php']['steps'];
$upgrade_phrases['upgrade_361.php']['steps'] =& $upgrade_phrases['upgrade_360b2.php']['steps'];

$upgrade_phrases['upgrade_361.php']['rename_podcasturl'] = "Переименование таблицы " . TABLE_PREFIX . "podcasturl в " . TABLE_PREFIX . "podcastitem";

$upgrade_phrases['upgrade_362.php']['steps'] =& $upgrade_phrases['upgrade_360b3.php']['steps'];
$upgrade_phrases['upgrade_363.php']['steps'] =& $upgrade_phrases['upgrade_360b3.php']['steps'];

$upgrade_phrases['upgrade_364.php']['steps'] =& $upgrade_phrases['upgrade_360b2.php']['steps'];
$upgrade_phrases['upgrade_365.php']['steps'] =& $upgrade_phrases['upgrade_360b3.php']['steps'];
$upgrade_phrases['upgrade_366.php']['steps'] =& $upgrade_phrases['upgrade_360b2.php']['steps'];
$upgrade_phrases['upgrade_367.php']['steps'] =& $upgrade_phrases['upgrade_360b3.php']['steps'];
$upgrade_phrases['upgrade_368.php']['steps'] =& $upgrade_phrases['upgrade_360b3.php']['steps'];

$upgrade_phrases['upgrade_370b2.php']['steps'] = array(
	1 => 'создание таблиц',
	2 => 'большие изменения таблиц',
	3 => 'разнообразные изменения таблиц',
	4 => 'вставка данных по умолчанию',
	5 => 'перестроение списка пользователей',
	6 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_370b2.php']['build_userlist'] = '<p>Перестроение списка пользователей %1$s</p>';
$upgrade_phrases['upgrade_370b2.php']['build_userlist_complete'] = '<p>Перестроение списка пользователей завершено</p>';

$upgrade_phrases['upgrade_370b3.php']['steps'] = array(
	1 => 'разнообразные изменения таблиц',
	2 => 'изменение значений по умолчанию в таблицах',
	3 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_370b4.php']['steps'] = array(
	1 => 'разнообразные изменения таблиц',
	2 => 'обновление данных таблиц',
	3 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_370b5.php']['steps'] = array(
	1 => 'разнообразные изменения таблиц',
	2 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_370b6.php']['steps'] = array(
	1 => 'разнообразные изменения таблиц',
	2 => 'вставка новой справки по форуму',
	3 => 'Удалить старую справку по форуму?',
	4 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_370b6']['inserting_vb37_faq_structure'] = 'Вставка новой структуры справки vBulletin 3.7.0';
$upgrade_phrases['upgrade_370b6']['delete_selected_faq_items'] = 'Удалить выбранные элементы справки';
$upgrade_phrases['upgrade_370b6']['delete_faq_description'] = '
	В предыдущем шаге было добавлено новое дерево справки по форуму.
	Это означает, что в данный момент у вас есть как старое, так и новое дерево справки.<br />
	<br />
	Ниже находится список всех элементов справки старого дерева.
	Если вы никогда не изменили ни один из элементов, то оставьте их все отмеченными для удаления.
	Если вы добавляли новые элементы справки самостоятельно или изменяли какие-либо из существующих элементов, то, вероятно, вы желаете сохранить их, поэтому вам следует снять отметку с тех элементов, которые <b>не</b> следует удалять.<br />
	<br />
	Когда вы закончите выбор, нажмите на кнопку <strong>' . $upgrade_phrases['upgrade_370b6']['delete_selected_faq_items'] . '</strong>, находящуюся внизу списка.
	Если ни один элемент не будет выбран, то ничего не будет удалено, но при этом процесс обновления продолжится.
';
$upgrade_phrases['upgrade_370b6']['reset_selection'] = 'Отметить всё';
$upgrade_phrases['upgrade_370b6']['selected_faq_items_deleted'] = 'Выбранные элементы справки были удалены.';

$upgrade_phrases['upgrade_370rc1.php']['steps'] = $upgrade_phrases['upgrade_370b5.php']['steps'];

$upgrade_phrases['upgrade_370rc2.php']['steps'] = array(
	1 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_370rc3.php']['dropping_old_table_x'] = 'Удаление устаревшей таблицы %1$s';
$upgrade_phrases['upgrade_370rc3.php']['steps'] = array(
	1 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_370rc4.php']['steps'] = array(
	1 => 'важное изменение шаблонов',
	2 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_370rc4.php']['token_added_x_templates'] = 'Добавление обязательного маркера безопасности в %1$s шаблонов';

$upgrade_phrases['upgrade_370.php']['steps'] = array(
	1 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_371.php']['steps'] = array(
	1 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_380a2.php']['steps'] = array(
	1 => 'создание таблиц',
	2 => 'разнообразные изменения таблиц',
	3 => 'преобразование сообщений внутри социальных групп',
	4 => 'обновление прав пользователей',
	5 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_380a2.php']['alter_index_on_x'] = 'Изменяем index в таблице %1$s';
$upgrade_phrases['upgrade_380a2.php']['create_index_on_x'] = 'Создаём index в таблице %1$s';
$upgrade_phrases['upgrade_380a2.php']['creating_default_group_category'] = 'Создаём категории по умолчанию для социальных групп';
$upgrade_phrases['upgrade_380a2.php']['fulltext_index_on_x'] = 'Создаём fulltext index в таблице %1$s';
$upgrade_phrases['upgrade_380a2.php']['convert_messages_to_discussion'] = 'Преобразуем сообщения социальных групп в дискуссии';
$upgrade_phrases['upgrade_380a2.php']['set_discussion_titles'] = 'Устанавливаем заголовки для дискуссий';
$upgrade_phrases['upgrade_380a2.php']['update_last_post'] = 'Обновляем информацию о последнем сообщении в дискуссии';
$upgrade_phrases['upgrade_380a2.php']['update_discussion_counters'] = 'Обновляем счётчики дискуссий';
$upgrade_phrases['upgrade_380a2.php']['update_group_message_counters'] = 'Обновляем счётчики сообщений социальных групп';
$upgrade_phrases['upgrade_380a2.php']['update_group_discussion_counters'] = 'Обновляем счётчики дискуссий социальных групп';
$upgrade_phrases['upgrade_380a2.php']['uncategorized'] = 'Вне категорий';
$upgrade_phrases['upgrade_380a2.php']['uncategorized_description'] = 'Социальные группы вне категорий';
$upgrade_phrases['upgrade_380a2.php']['move_groups_to_default_category'] = 'Перемещаем социальные группы в одну категорию (Вне категорий)';
$upgrade_phrases['upgrade_380a2.php']['updating_profile_categories'] = 'Добавляем настройки ограничения доступа к категориям полей профиля';
$upgrade_phrases['upgrade_380a2.php']['update_hv_options'] = 'Обновляем настройки отделения людей от роботов';
$upgrade_phrases['upgrade_380a2.php']['update_album_update_counters'] = 'Обновляем счётчики альбомов';
$upgrade_phrases['upgrade_380a2.php']['granting_permissions'] = 'Предоставляем доступ к новым функциям';

$upgrade_phrases['upgrade_380b1.php']['steps'] = array(
	1 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_380b2.php']['steps'] = array(
	1 => 'разнообразные изменения таблиц',
	2 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_380b3.php']['steps'] = array(
	1 => 'разнообразные изменения таблиц',
	2 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_380b4.php']['steps'] = array(
	1 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_380rc1.php']['rebuild_event_cache'] = 'Перестраиваем кэш событий';

$upgrade_phrases['upgrade_380rc1.php']['steps'] = array(
	1 => 'перестроение кэша',
	2 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_380rc2.php']['updating_mail_ssl_setting'] = 'Обновляем настройки SSL для почты';
$upgrade_phrases['upgrade_380rc2.php']['steps'] = array(
	1 => 'обновление настроек',
	2 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_380.php']['steps'] = array(
	1 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_381.php']['steps'] = array(
	1 => 'разнообразные изменения таблиц',
	2 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_382.php']['steps'] = array(
	1 => 'разнообразные изменения таблиц',
	2 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_383.php']['steps'] = array(
	1 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_384.php']['steps'] = array(
	1 => 'разнообразные изменения таблиц',
	2 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_385.php']['steps'] = array(
	1 => 'разнообразные изменения таблиц',
	2 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_386.php']['steps'] = array(
	1 => 'разнообразные изменения таблиц',
	2 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_387b1.php']['steps'] = array(
	1 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

$upgrade_phrases['upgrade_387.php']['steps'] = array(
	1 => 'обновление vBulletin до версии ' . VERSION . ' завершено!'
);

##########################
# фразы finalupgrade.php #
##########################

$upgrade_phrases['finalupgrade.php']['steps'] = array(
    1 => 'импорт новейших опций',
    2 => 'импорт новейшей помощи администратору',
    3 => 'импорт новейшего языка',
    4 => 'импорт новейшего стиля',
	5 => 'всё выполнено',
);

$upgrade_phrases['finalupgrade.php']['upgrade_start_message'] = "<p>Этот скрипт обновит ваши шаблоны, настройки, язык и помощь администратору до последней версии.</p>\n<p>Нажмите кнопку 'Дальше' для продолжения.</p>";
$upgrade_phrases['finalupgrade.php']['upgrade_version_mismatch'] = '<p>Ваш vBulletin (%1$s) не похож на тот, который вы скачали (%2$s).</p>
		<p>Это означает, что обновление было неудачным. Убедитесь, что вы загрузили последние файлы и <a href="upgrade.php">нажмите сюда</a>, чтобы попытаться снова.</p>
		<p>Если вы действительно желаете продолжить с частичным обновлением, то <a href="finalupgrade.php?step=1">нажмите здесь</a>.</p>';

/*======================================================================*\
|| #################################################################### ||
|| #                  Русский перевод  сделал zCarot                  # ||
|| # CVS: $RCSfile$ - $Revision: 41030 $
|| #################################################################### ||
\*======================================================================*/
?>